# DT Line Control - CLI

## Building

Logged at `pcrpct01`:

Clone it:
```
git clone ssh://git@gitlab.cern.ch:7999/cmsrpconline/dt_line_control.git
```

Build `ptypes`:
```
cd ThrDT/ptypes-2.1.1
make clean
make
cd ../..
```


Build `ccbdb`:

```
cd ThrDT/ccbcmds/ccbdb
make clean
make 
cd ../../..
```

Build `ccbcmds`:
```
mkdir -p ThrDT/ccbcmds/lib
cd ThrDT/ccbcmds/lib
ln -s ../../ptypes-2.1.1/so/libptypes.so
cd ../cmd
make clean
make
cd ../../..
```


Compile `rpc_write` and `rpc_read`:

```
cd ThrDT
export  LD_LIBRARY_PATH=LD_LIBRARY_PATH_LD:`pwd`/ccbcmds/lib:`pwd`/ccbcmds/ccbdb:`pwd`/ptypes-2.1.1/so

g++ -o rpc_write rpc_write.cc -D'__USE_PTYPES__' -D'__USE_CCBDB__'   -I ccbcmds/inc -L ccbcmds/lib -lccbcmds -lpthread -lxml2
g++ -o rpc_read rpc_read.cc  -D'__USE_PTYPES__' -D'__USE_CCBDB__' -I ccbcmds/inc -L ccbcmds/lib -lccbcmds -lpthread -lxml2
```

## Running the tool

Inside `ThrDT`, this will, for example, set `VTh = 400 mV` for all chambers in W+1 Far.

```
./set_rb+1_far 400
```