export  LD_LIBRARY_PATH=LD_LIBRARY_PATH_LD:~/DT/ThrDT/ccbcmds2/lib:~/DT/ThrDT/ccbcmds2/ccbdb
g++ -o rpc_write rpc_write.cc -D'__USE_PTYPES__' -D'__USE_CCBDB__'   -I ccbcmds2/inc -L ccbcmds2/lib -lccbcmds -lpthread -lxml2
g++ -o rpc_read rpc_read.cc  -D'__USE_PTYPES__' -D'__USE_CCBDB__' -I ccbcmds2/inc -L ccbcmds2/lib -lccbcmds -lpthread -lxml2

echo "W+2 RB3 sect6 (RB3+ and RB3- just in case) "
./rpc_read 10.176.60.228 26 7 0x20 0x04 
./rpc_read 10.176.60.228 26 7 0x40 0x04 
./rpc_read 10.176.60.228 26 7 0x20 0x05 
./rpc_read 10.176.60.228 26 7 0x40 0x05 

#echo "W+2 RB1out sect11 FW "
#./rpc_read 10.176.60.228 47 6 0x20 0x04 
#./rpc_read 10.176.60.228 47 6 0x40 0x04 

echo "W+2 RB2out sect6 FW (due to FEB3, Vth2=0) "
./rpc_read 10.176.60.228 25 6 0x20 0x04 
./rpc_read 10.176.60.228 25 6 0x40 0x04 

echo "W0 RB2 out 1 BW"
./rpc_read 10.176.60.232 2 6 0x20 0x05 
./rpc_read 10.176.60.232 2 6 0x40 0x05 

#echo "W0 RB2 out 12 FW&BW"
#./rpc_read 10.176.60.232 52 6 0x20 0x04 
#./rpc_read 10.176.60.232 52 6 0x40 0x04 
#./rpc_read 10.176.60.232 52 6 0x20 0x05 
#./rpc_read 10.176.60.232 52 6 0x40 0x05 

echo "W0 RB2 out 7 FW"
./rpc_read 10.176.60.232 29 6 0x20 0x04 
./rpc_read 10.176.60.232 29 6 0x40 0x04 

echo "W0 RB2 in 3 FW (due to FEB1, Vth2=0) "
./rpc_read 10.176.60.232 11 7 0x20 0x04 
./rpc_read 10.176.60.232 11 7 0x40 0x04 

echo "W-1 RB1 in Sect3 Back"
./rpc_read 10.176.60.234 10 7 0x20 0x05 
./rpc_read 10.176.60.234 10 7 0x40 0x05 

echo " W-1 RB2 in Sect4 Centr "
./rpc_read 10.176.60.234 15 7 0x20 0x05 
./rpc_read 10.176.60.234 15 7 0x40 0x05 

#echo " W-1 RB1 out Sect4 FW"
#./rpc_read 10.176.60.234 14 6 0x20 0x04 
#./rpc_read 10.176.60.234 14 6 0x40 0x04 

echo "W-1 RB2 in Sect11 Back "
./rpc_read 10.176.60.234 48 7 0x20 0x05 
./rpc_read 10.176.60.234 48 7 0x40 0x05 

echo "W-2 RB1 out sect5 Forw "
./rpc_read 10.176.60.236 19 6 0x20 0x04   
./rpc_read 10.176.60.236 19 6 0x40 0x04  

echo "W-2 RB2 in Sect 5 Back "
./rpc_read 10.176.60.236 20 7 0x20 0x05 
./rpc_read 10.176.60.236 20 7 0x40 0x05 

echo "W-2 RB2 in Sect 2 FW (due to FEB3)"
./rpc_read 10.176.60.236 6 7 0x20 0x04 
./rpc_read 10.176.60.236 6 7 0x40 0x04 

