#! /bin/tcsh -f
# set the correct environmnet and run "main" in gdb

# base path
set base_path = `cd ..; pwd`

# execute program "main"
env LD_LIBRARY_PATH="${base_path}/lib:${LD_LIBRARY_PATH}" gdb ${base_path}/exe/main
#env LD_LIBRARY_PATH=${base_path}/lib gdb ${base_path}/exe/main
