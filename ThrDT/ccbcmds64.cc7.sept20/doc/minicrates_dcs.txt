1. CCB server

 Before sending commands to the minicrates the CCB SERVER has to be started on
 dtfed0.
 Check first if the process - called ccbd - is already running: connect to
 dtfed0.cmsdaqpreseries (you have to pass through cmsdaquser0.cern.ch, there
 is no direct connection to dtfed0) and type "ps -C ccbd".

 If the process is not running, start it by going in /home/dtdev and executing
 the command ./startdcs.

 If you have to restart the server, first kill the running one. Get the
 process ID with "pstree -p | grep ccbd", kill first the "sh" and then the
 "ccbd" process.

 NB The command ./startdcs requires that you have the privilege to "sudo" to
 root.
 To check whether you are allowed to do this or not, try "sudo -s"; you
 should become root without being prompted for the password.
 If this doesn't happen, contact DCS people to modify your account.

2. Check status

 Open a shell on dtfed0; then type the command "mc_status <ccbid>";
 the variable <ccbid> is the ccbid used by the ccbserver.
 The status will be then printed on stout.
 You can get the <ccbid> with the command "mc_map <wheel> <sector> <station>".
 Conversely, you can get <wheel>, <sector>, <station> with the command
 "mc_map <ccbid>". In order to use this command you have to access the Oracle 
 DB and thus to add "export LD_LIBRARY_PATH=/opt/instantclient_10_2" in your
 .bashrc file.

3. Temperature monitoring

 Open a shell on dtfed0; then type the command "mc_temp <ccbid>"; this
 will start a monitor of the temperature. Every 30 seconds the temperatures
 of all the minicrate boards will be printed on stdout.

4. Configuration

 Open a shell on dtfed0; then type the command "mc_config <ccbid> <filename>";
 the configuration files are currently in /home/dtdev/configs/.
 The result of the configuration will be printed on stout.
 For the time being it is recommended to configure only a chamber at once.

 The command "mc_threshold <ccbid>" allows you to set front-end bias, threshold
 and width.

5. Other commands

 mc_pwr <ccbid> <pwr_id> <off/on>: switch off/on single boards on a minicrate;
 type "mc_pwr" on a shell for the help.

 ttc_fine_delay <ccbid> <channel> <delay>: set TTC fine delay; type
 "ttc_fine_delay" on a shell for the help.
