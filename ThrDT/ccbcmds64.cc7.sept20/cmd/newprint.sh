#! /bin/tcsh -f
# Create some lines of "printf" and append them to prova.txt

set lista= (Vccin_min Vccin_max Vddin_min Vddin_max Sb_Vcc_min Sb_Vcc_max Sb_Vdd_min Sb_Vdd_max)

set tipo = "f"
set strutt = "mc_status"
set virgolette='"'

foreach nome (${lista})
  set stringa1="printf(${virgolette}${nome}: %${tipo}\\n${virgolette},${strutt}.${nome});"
  set stringa2="printf(${virgolette}${nome}: %${tipo}\\n${virgolette},${strutt}.${nome}[ 0],result.${nome}[ 1],result.${nome}[ 2]);"
  set stringa3="printf(${virgolette}${nome}: %${tipo} <-> %${tipo}\\n${virgolette},m${strutt}->${nome},M${strutt}->${nome});"

  set stringa4="for (i=0;i<;++i)"
  set stringa5="printf(${virgolette}${nome}[ %d]: %${tipo} <-> %${tipo}\\n${virgolette},i,m${strutt}->${nome}[ i],M${strutt}->${nome}[ i]);"


  set stringa6="sprintf(out,${virgolette}%s${nome}: %${tipo}\\n${virgolette},out,${strutt}.${nome});"

#  echo ${stringa1} >> prova.txt

#  echo ${stringa2} >> prova.txt

#  echo ${stringa3} >> prova.txt

#  echo ${stringa4} >> prova.txt
#  echo ${stringa5} >> prova.txt

  echo ${stringa6} >> prova.txt
end
