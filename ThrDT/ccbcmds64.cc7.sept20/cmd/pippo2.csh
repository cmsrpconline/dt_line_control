#!/bin/tcsh -f

set code='0xA6'
set name='Find Sensors'
set cmnd='Cccb::find_sensors'
set numb='b.29'
set virg='"'

echo "${numb}.1 ${cmnd}()"
echo "  Sends the ${virg}${name}${virg} (${code}) command to the CCB and decode the reply."
echo
echo "${numb}.2 ${cmnd}_reset()"
echo "  Resets the results of the ${code} command."
echo
echo "${numb}.3 int ${cmnd}_decode(unsigned char *rdstr)"
echo "  Decodes reply of the ${code} command. The returned value is the number of"
echo "decoded bytes."
echo
echo "${numb}.4 ${cmnd}_print()"
echo "  Print to standard out the results of ${code} command."
