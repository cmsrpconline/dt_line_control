Summary: Library and binaries for CMS DT slow-control
Name: ccbcmds
Version: 2.7.1
Release: 1
Copyright: GPL
Group: Utility
Packager: Andrea Parenti <parenti@pd.infn.it>
Source: http://www.pd.infn.it/~parenti/dtdcs/
Provides: libccbcmds.so
Requires: ccbdb >= 2.7

%description
The classes in the library are used to access the CCB, code/decode commands, access the DB.
In particular the class dtdcs provide functionalities for a drift tube DCS.
Some binaries for DT control are installed in /usr/local/bin.

%build
%install
%check
%clean
%pre
%preun
#nothing to be done

%post
ln -fs /usr/lib/libccbcmds.so.2.7.1 /usr/lib/libccbcmds.so

%postun
rm -f /usr/lib/libccbcmds.so

%files
# libraries
/usr/lib/libccbcmds.so.2.7.1
# header files
/usr/include/ccbcmds/apfunctions.h
/usr/include/ccbcmds/Cbti.h
/usr/include/ccbcmds/Cccb.h
/usr/include/ccbcmds/Cccbheader.h
/usr/include/ccbcmds/Ccmn_cmd.h
/usr/include/ccbcmds/Ccommand.h
/usr/include/ccbcmds/Cconf.h
/usr/include/ccbcmds/Cconfupdate.h
/usr/include/ccbcmds/Cdef.h
/usr/include/ccbcmds/Cdtdcs.h
/usr/include/ccbcmds/Cfe.h
/usr/include/ccbcmds/Ci2c.h
/usr/include/ccbcmds/Clog.h
/usr/include/ccbcmds/Cmisc.h
/usr/include/ccbcmds/Cref.h
/usr/include/ccbcmds/Crob.h
/usr/include/ccbcmds/Ctdc.h
/usr/include/ccbcmds/Ctraco.h
/usr/include/ccbcmds/Ctrb.h
/usr/include/ccbcmds/Ctsm.h
/usr/include/ccbcmds/Ctss.h
/usr/include/ccbcmds/Cttcrx.h
# binaries
/usr/local/bin/dbquery
/usr/local/bin/dsptoieee32
#/usr/local/bin/dt_checkconf
/usr/local/bin/dt_config
/usr/local/bin/dt_monitor
/usr/local/bin/dt_robreset
/usr/local/bin/dt_run_in_progress
#/usr/local/bin/dt_temp
/usr/local/bin/ieee32todsp
#/usr/local/bin/insert_ccbmap
/usr/local/bin/insert_ccbref
/usr/local/bin/insert_configcmds
/usr/local/bin/insert_dtpartition
/usr/local/bin/mc_autosetlink
/usr/local/bin/mc_checktdc
/usr/local/bin/mc_config
/usr/local/bin/mc_dump_config
/usr/local/bin/mc_generic_command
/usr/local/bin/mc_loadconf
/usr/local/bin/mc_map
/usr/local/bin/mc_mask_fe_channel
/usr/local/bin/mc_padc_monitor
/usr/local/bin/mc_pwr
/usr/local/bin/mc_readroberror
/usr/local/bin/mc_restart
/usr/local/bin/mc_restoreconf
/usr/local/bin/mc_robreset
/usr/local/bin/mc_saveconf
/usr/local/bin/mc_status
/usr/local/bin/mc_stop_on_boot
/usr/local/bin/mc_temp
/usr/local/bin/mc_test
/usr/local/bin/mc_threshold
/usr/local/bin/ttc_fine_delay
/usr/local/bin/update_ccbid
