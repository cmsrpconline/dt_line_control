#include <ccbcmds/Cbti.h>
#include <ccbcmds/apfunctions.h>

#include <iostream>
#include <cstdio>


/*****************************************************************************/
// BTI class
/*****************************************************************************/

// Class constructor; arguments: pointer_to_command_object
// Author: A.Parenti, Dec01 2006
Cbti::Cbti(Ccommand* ppp) : Ccmn_cmd(ppp) {
  read_BTI_reset(); // Reset read_bti struct
  read_BTI_6b_reset(); // Reset read_bti_6b struct
  write_BTI_reset(); // Reset write_bti struct
  write_BTI_6b_reset(); // Reset write_bti_6b struct
  enable_BTI_reset(); // Reset enable_bti struct
  load_BTI_emulation_reset(); // Reset load_bti_emul struct
  start_BTI_emulation_reset(); // Reset start_bti_emul struct
  new_start_BTI_emulation_reset(); // Reset new_start_bti_emul struct
  compare_BTI_register_reset(); // Reset comp_bti_register struct
}

/****************************/
/* Override ROB/TDC methods */
/****************************/

void Cbti::read_TDC(char tdc_brd, char tdc_chip) {}
int Cbti::read_TDC_decode(unsigned char *rdstr) {return 0;}
void Cbti::read_TDC_print() {}

void Cbti::status_TDC(char tdc_brd, char tdc_chip) {}
void Cbti::status_TDC_print() {}

void Cbti::read_ROB_error() {}
int Cbti::read_ROB_error_decode(unsigned char *rdstr) {return 0;}
void Cbti::read_ROB_error_print() {}

/*****************/
/* Configure BTI */
/*****************/

// Configure a BTI, reading from a "myconf" object
// Author: A.Parenti, Sep02 2005
// Modified: AP, Apr10 2006
// Modified: AP, Nov17 2006 (Added CRC)
short Cbti::config_BTI(Cconf *myconf, char bti_brd, char bti_chip) {
  short conferr, confstatus, confcrcfe,confcrcdaq,confcrctrig;;

  if (this==NULL) return 0; // Object deleted -> return

// BTI error is BIT0 of conferr
  conferr = config_mc(myconf,CBTI,bti_brd,bti_chip,&confstatus,&confcrcfe,&confcrcdaq,&confcrctrig);

  if ((conferr&0x1)!=0) {
    config_BTI_result.error = true;
    config_BTI_result.crc=0;
    return 1;
  }
  else {
    config_BTI_result.error = false;
    config_BTI_result.crc=confcrctrig;
    return 0;
  }
}

// Print to stdout the result of BTI configuration
// Author: A.Parenti, Sep02 2005
// Modified: AP, Nov17 2006
void Cbti::config_BTI_print() {
  if (this==NULL) return; // Object deleted -> return

  if (config_BTI_result.sent>0) {
    printf("Configure BTI: %d lines sent (%d successful)\n",config_BTI_result.sent,config_BTI_result.sent-config_BTI_result.bad);
    printf("               CRC=%#hX\n",config_BTI_result.crc);
    printf("               Error=%d\n",config_BTI_result.error);
  }
}


// Retrive BTI config from CCB, dump it to file
// Author: A.Parenti, Oct30 2006
// Modified: AP, Nov02 2006
void Cbti::read_BTI_config(char *filename) {
  char brd,chip;
  unsigned char cmdline[34];
  char strcmd[103];
  int i;
  std::ofstream outfile(filename,std::ofstream::app);

  if (this==NULL) return; // Object deleted -> return

  printf("Read BTI config (0x54).\n");

  if (outfile.is_open()) {
// BTI, 0x54
    for (brd=0;brd<=7;++brd)
      for (chip=0;chip<=31;++chip) {
        printf("Brd:%d Chip:%d ...",brd,chip);

        read_BTI_6b(brd,chip);

        if (read_bti_6b.code1==0x5A) {
          cmdline[0]=0x54;
          cmdline[1]=brd;
          cmdline[2]=chip;
          mystrcpy(cmdline+3,read_bti_6b.conf,14);
          mystrcpy(cmdline+17,read_bti_6b.testin,17);

          strcpy(strcmd,"");
          sprintf(strcmd,"%02X",cmdline[0]);
          for (i=1;i<34;++i)
            sprintf(strcmd,"%s %02X",strcmd,cmdline[i]);
          outfile << strcmd << '\n';
          outfile.flush();
        }

        printf("done!\n");
      }

    outfile.close();
  } else
    printf("Impossible to open file '%s'.\n",filename);

}


/*****************************/
/* "Read BTI" (0x19) command */
/*****************************/

// Send "Read BTI" (0x19) command and decode reply
// Author: A. Parenti, Jan06 2005
// Modified: AP, Mar24 2005
// Note: Tested at Cessy, Oct05 2006
void Cbti::read_BTI(char bti_brd, char bti_chip) {

  const unsigned char command_code=0x19;
  unsigned char command_line[3];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=bti_brd;
  command_line[2]=bti_chip;

  read_BTI_reset(); // Reset read_bti struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,3,READ_BTI_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = read_BTI_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
   read_bti.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nRead BTI (0x19): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

  mywait();

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset read_bti struct
// Author: A.Parenti Aug03 2006
// Modified: AP, Dec06 2006
void Cbti::read_BTI_reset() {
  int i;

  if (this==NULL) return; // Object deleted -> return

  read_bti.code1=0;
  for (i=0;i<11;++i)
    read_bti.conf[i]=0;
  for (i=0;i<13;++i)
    read_bti.testin[i] = read_bti.testout[i] = 0;

  read_bti.code2=read_bti.narg=0;
  read_bti.ccbserver_code=-5;

  read_bti.msg="";
}


// Decode "Read BTI" (0x19) reply
// Author: A. Parenti, Jan29 2005
// Modified: AP, Dec06 2006
int Cbti::read_BTI_decode(unsigned char *rdstr) {
  const unsigned char right_reply=0x1A, command_code=0x19;
  unsigned char code2;
  int i, idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure

  idx = rstring(rdstr,idx,&read_bti.code1);
  code2 = rdstr[idx];

// Fill-up the "result" structure
  if (read_bti.code1==right_reply) {
    for (i=0;i<11;++i)
      idx = rstring(rdstr,idx,read_bti.conf+i);
    for (i=0;i<13;++i)
      idx = rstring(rdstr,idx,read_bti.testin+i);
    for (i=0;i<13;++i)
      idx = rstring(rdstr,idx,read_bti.testout+i);
  } else if (read_bti.code1==CCB_OXFC && code2==command_code) {
    idx = rstring(rdstr,idx,&read_bti.code2);
    idx = rstring(rdstr,idx,&read_bti.narg);
  }

// Fill-up the error code
  if (read_bti.code1==CCB_BUSY_CODE) // ccb is busy
    read_bti.ccbserver_code = -1;
  else if (read_bti.code1==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    read_bti.ccbserver_code = 1;
  else if (read_bti.code1==right_reply) // ccb: right reply
    read_bti.ccbserver_code = 0;
  else if (read_bti.code1==CCB_OXFC && code2==command_code) // ccb: right reply
    read_bti.ccbserver_code = 0;
  else // wrong ccb reply
    read_bti.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  read_bti.msg = "\nRead BTI (0x19)\n";

  if (read_bti.ccbserver_code!=0) {// Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(read_bti.ccbserver_code));
    read_bti.msg+=tmpstr;
  } else if (read_bti.code1==right_reply) {
    sprintf(tmpstr,"code: %#2X\n", read_bti.code1); read_bti.msg+=tmpstr;
    for (i=0;i<11;++i) {
      sprintf(tmpstr,"conf[%i]: %#2X\n",i,read_bti.conf[i]); read_bti.msg+=tmpstr;
    }
    for (i=0;i<13;++i) {
      sprintf(tmpstr,"testin[%i]: %#2X\n",i,read_bti.testin[i]); read_bti.msg+=tmpstr;
    }
    for (i=0;i<13;++i) {
      sprintf(tmpstr,"testout[%i]: %#2X\n",i,read_bti.testout[i]); read_bti.msg+=tmpstr;
    }
  } else {
    sprintf(tmpstr,"code1: %#2X\n", read_bti.code1); read_bti.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n", read_bti.code2); read_bti.msg+=tmpstr;
    sprintf(tmpstr,"Wrong argument. narg: %d\n", read_bti.narg); read_bti.msg+=tmpstr;
  }

  return idx;
}


// Print result of "Read BTI" (0x19) to stdout
// Author: A. Parenti, Jan06 2005
// Modified: AP, Dec06 2006
void Cbti::read_BTI_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",read_bti.msg.c_str());
}


/**********************************/
/* "Read BTI 6bit" (0x59) command */
/**********************************/

// Send "Read BTI 6bit" (0x59) command and decode reply
// Author: A. Parenti, Dec13 2004
// Modified: AP, Mar24 2005
// Note: Tested at Cessy, Oct05 2006
void Cbti::read_BTI_6b(char bti_brd, char bti_chip) {

  const unsigned char command_code=0x59;
  unsigned char command_line[3];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=bti_brd;
  command_line[2]=bti_chip;

  read_BTI_6b_reset(); // Reset read_bti_6b struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,3,READ_BTI_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = read_BTI_6b_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    read_bti_6b.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nRead BTI 6bit (0x59): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset read_bti_6b struct
// Author: A.Parenti Aug03 2006
// Modified: AP, Dec06 2006
void Cbti::read_BTI_6b_reset() {
  int i;

  if (this==NULL) return; // Object deleted -> return

  read_bti_6b.code1=0;
  for (i=0;i<14;++i)
    read_bti_6b.conf[i]=0;
  for (i=0;i<17;++i)
    read_bti_6b.testin[i] = read_bti_6b.testout[i] = 0;

  read_bti_6b.code2=read_bti_6b.narg=0;
  read_bti_6b.ccbserver_code=-5;

  read_bti_6b.msg="";
}


// Decode "Read BTI 6bit" (0x59) reply
// Author: A. Parenti, Jan29 2005
// Modified: AP, Dec06 2006
int Cbti::read_BTI_6b_decode(unsigned char *rdstr) {
  const unsigned char right_reply=0x5A, command_code=0x59;
  unsigned char code2;
  int i, idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure

  idx = rstring(rdstr,idx,&read_bti_6b.code1);
  code2 = rdstr[idx];

// Fill-up the "result" structure
  if (read_bti_6b.code1==right_reply) {
    for (i=0;i<14;++i)
      idx = rstring(rdstr,idx,read_bti_6b.conf+i);
    for (i=0;i<17;++i)
      idx = rstring(rdstr,idx,read_bti_6b.testin+i);
    for (i=0;i<17;++i)
      idx = rstring(rdstr,idx,read_bti_6b.testout+i);
  } else if (read_bti_6b.code1==CCB_OXFC && code2==command_code) {
    idx = rstring(rdstr,idx,&read_bti_6b.code2);
    idx = rstring(rdstr,idx,&read_bti_6b.narg);
  }

// Fill-up the error code
  if (read_bti_6b.code1==CCB_BUSY_CODE) // ccb is busy
    read_bti_6b.ccbserver_code = -1;
  else if (read_bti_6b.code1==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    read_bti_6b.ccbserver_code = 1;
  else if (read_bti_6b.code1==right_reply) // ccb: right reply
    read_bti_6b.ccbserver_code = 0;
  else if (read_bti_6b.code1==CCB_OXFC && code2==command_code) // ccb: right reply
    read_bti_6b.ccbserver_code = 0;
  else // wrong ccb reply
    read_bti_6b.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  read_bti_6b.msg = "\nRead BTI 6bit (0x59)\n";

  if (read_bti_6b.ccbserver_code!=0) {// Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(read_bti_6b.ccbserver_code)); read_bti_6b.msg+=tmpstr;
  } else if (read_bti_6b.code1==right_reply) {
    sprintf(tmpstr,"code: %#2X\n", read_bti_6b.code1); read_bti_6b.msg+=tmpstr;
    for (i=0;i<14;++i) {
      sprintf(tmpstr,"conf[%i]: %#2X\n",i,read_bti_6b.conf[i]); read_bti_6b.msg+=tmpstr;
    }
    for (i=0;i<17;++i) {
      sprintf(tmpstr,"testin[%i]: %#2X\n",i,read_bti_6b.testin[i]); read_bti_6b.msg+=tmpstr;
    }
    for (i=0;i<17;++i) {
      sprintf(tmpstr,"testout[%i]: %#2X\n",i,read_bti_6b.testout[i]); read_bti_6b.msg+=tmpstr;
    }
  } else {
    sprintf(tmpstr,"code1: %#2X\n", read_bti_6b.code1); read_bti_6b.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n", read_bti_6b.code2); read_bti_6b.msg+=tmpstr;
    sprintf(tmpstr,"Wrong argument. narg: %d\n", read_bti_6b.narg); read_bti_6b.msg+=tmpstr;
  }

  return idx;
}

// Print result of "Read BTI 6bit" (0x59) to stdout
// Author: A. Parenti, Dec13 2004
// Modified: AP, Dec06 2006
void Cbti::read_BTI_6b_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s", read_bti_6b.msg.c_str());
}



/******************************/
/* "Write BTI" (0x14) command */
/******************************/

// Send "Write BTI" (0x14) command and decode reply
// Author: A. Parenti, Jan06 2005
// Modified: AP, Mar24 2005
// Note: Tested at LNL, Apr29 2005
void Cbti::write_BTI(char bti_brd, char bti_chip, unsigned char conf[11], unsigned char testin[13]) {

  const unsigned char command_code=0x14;
  unsigned char command_line[27];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=bti_brd;
  command_line[2]=bti_chip;
  mystrcpy(command_line+3,conf,11);
  mystrcpy(command_line+14,testin,13);

  write_BTI_reset(); // Reset write_bti struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,27,WRITE_BTI_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = write_BTI_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    write_bti.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nWrite BTI (0x14): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}

// Reset write_bti struct
// Author: A.Parenti Aug03 2006
// Modified: AP, Dec06 2006
void Cbti::write_BTI_reset() {
  if (this==NULL) return; // Object deleted -> return

  write_bti.code1 = write_bti.code2 = 0;
  write_bti.result = 0;

  write_bti.ccbserver_code=-5;

  write_bti.msg="";
}


// Decode "Write BTI" (0x14) reply
// Author: A. Parenti, Jan29 2005
// Modified: AP, Dec12 2006
int Cbti::write_BTI_decode(unsigned char* rdstr) {
  const unsigned char command_code=0x14, right_reply=CCB_OXFC;
  int idx;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  write_bti.code1  = rdstr[0];
  write_bti.code2  = rdstr[1];

// Fill-up the "result" structure
  write_bti.result = rdstr[2];

  idx=3;

// Fill-up the error code
  if (write_bti.code1==CCB_BUSY_CODE) // ccb is busy
    write_bti.ccbserver_code = -1;
  else if (write_bti.code1==CCB_UNKNOWN1 && write_bti.code2==CCB_UNKNOWN2) // ccb: unknown command
    write_bti.ccbserver_code = 1;
  else if (write_bti.code1==right_reply && write_bti.code2==command_code) // ccb: right reply
    write_bti.ccbserver_code = 0;
  else // wrong ccb reply
    write_bti.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

   write_bti.msg = "\nWrite BTI (0x14)\n";

  if (write_bti.ccbserver_code!=0) {// Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(write_bti.ccbserver_code)); write_bti.msg+=tmpstr;
  } else {

    sprintf(tmpstr,"code1: %#2X\n", write_bti.code1); write_bti.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n", write_bti.code2); write_bti.msg+=tmpstr;

    switch (write_bti.result) {
    case 0:
      write_bti.msg+="Result: Verify error\n";
      break;
    case 1:
      write_bti.msg+="Result: Write successful\n";
      break;
    case -1:
      write_bti.msg+="Result: Parameter error\n";
      break;
    }
  }

  return idx;
}


// Print results of "Write BTI" (0x14) command to standard output
// Author:  A.Parenti Jan06 2005
// Modifid: AP, Dec06 2006
void Cbti::write_BTI_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",write_bti.msg.c_str());
}


/**********************************/
/* "Write BTI 6bit" (0x54) command */
/**********************************/

// Send "Write BTI 6bit" (0x54) command and decode reply
// Author: A. Parenti, Dec14 2004
// Modified: AP, Mar24 2005
// Note: Tested at LNL, Apr29 2005
void Cbti::write_BTI_6b(char bti_brd, char bti_chip, unsigned char conf[14], unsigned char testin[17]) {

  const unsigned char command_code=0x54;
  unsigned char command_line[34];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=bti_brd;
  command_line[2]=bti_chip;
  mystrcpy(command_line+3,conf,14);
  mystrcpy(command_line+17,testin,17);

  write_BTI_6b_reset(); // Reset write_bti_6b struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,34,WRITE_BTI_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = write_BTI_6b_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    write_bti_6b.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nWrite BTI 6bit (0x54): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}

// Reset write_bti_6b struct
// Author: A.Parenti Aug03 2006
// Modified: AP, Dec06 2006
void Cbti::write_BTI_6b_reset() {
  if (this==NULL) return; // Object deleted -> return

  write_bti_6b.code1 = write_bti_6b.code2 = 0;
  write_bti_6b.result = 0;

  write_bti_6b.ccbserver_code=-5;

  write_bti_6b.msg="";
}

// Decode "Write BTI 6bit" (0x54) reply
// Author: A. Parenti, Jan29 2005
// Modified: AP, Dec12 2006
int Cbti::write_BTI_6b_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x54, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  write_bti_6b.code1  = rdstr[0];
  write_bti_6b.code2  = rdstr[1];

// Fill-up the "result" structure
  write_bti_6b.result = rdstr[2];

  idx=3;

// Fill-up the error code
  if (write_bti_6b.code1==CCB_BUSY_CODE) // ccb is busy
    write_bti_6b.ccbserver_code = -1;
  else if (write_bti_6b.code1==CCB_UNKNOWN1 && write_bti_6b.code2==CCB_UNKNOWN2) // ccb: unknown command
    write_bti_6b.ccbserver_code = 1;
  else if (write_bti_6b.code1==right_reply && write_bti_6b.code2==command_code) // ccb: right reply
    write_bti_6b.ccbserver_code = 0;
  else // wrong ccb reply
    write_bti_6b.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  write_bti_6b.msg = "\nWrite BTI 6bit (0x54)\n";

  if (write_bti_6b.ccbserver_code!=0) {// Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(write_bti_6b.ccbserver_code)); write_bti_6b.msg+=tmpstr;
  } else {

    sprintf(tmpstr,"code1: %#2X\n", write_bti_6b.code1); write_bti_6b.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n", write_bti_6b.code2); write_bti_6b.msg+=tmpstr;

    switch (write_bti_6b.result) {
    case 0:
      write_bti_6b.msg+="Result: Verify error\n";
      break;
    case 1:
      write_bti_6b.msg+="Result: Write successful\n";
      break;
    case -1:
      write_bti_6b.msg+="Result: Parameter error\n";
      break;
    }
  }

  return idx;
}


// Print results of "Write BTI 6bit" (0x54) command to standard output
// Author:  A.Parenti Dec14 2004
// Modified: AP, Dec06 2006
void Cbti::write_BTI_6b_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",write_bti_6b.msg.c_str());
}


/*******************************/
/* "Enable BTI" (0x51) command */
/*******************************/

// Send "Enable BTI" (0x51) command and decode reply
// Author: A. Parenti, Jan07 2005
// Modified: AP, Mar24 2005
// Note: Tested at LNL, Apr29 2005
void Cbti::enable_BTI(char bti_brd, short inner_msk, short outer_msk) {

  const unsigned char command_code=0x51;
  unsigned char command_line[7];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  wstring((short)bti_brd,1,command_line);
  wstring(inner_msk,3,command_line);
  wstring(outer_msk,5,command_line);

  enable_BTI_reset(); // Reset enable_bti struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,7,ENABLE_BTI_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = enable_BTI_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    enable_bti.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nEnable BTI (0x51): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset enable_bti struct
// Author: A.Parenti Aug03 2006
// Modified: AP, Dec06 2006
void Cbti::enable_BTI_reset() {
  if (this==NULL) return; // Object deleted -> return

  enable_bti.code1 = enable_bti.code2 = 0;
  enable_bti.result = 0;

  enable_bti.ccbserver_code=-5;

  enable_bti.msg="";
}


// Decode "Enable BTI" (0x51) reply
// Author: A. Parenti, Jan29 2005
// Modified: AP, Dec12 2006
int Cbti::enable_BTI_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x51, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  enable_bti.code1  = rdstr[0];
  enable_bti.code2  = rdstr[1];

// Fill-up the "result" structure
  enable_bti.result = rdstr[2];

  idx=3;

// Fill-up the error code
  if (enable_bti.code1==CCB_BUSY_CODE) // ccb is busy
    enable_bti.ccbserver_code = -1;
  else if (enable_bti.code1==CCB_UNKNOWN1 && enable_bti.code2==CCB_UNKNOWN2) // ccb: unknown command
    enable_bti.ccbserver_code = 1;
  else if (enable_bti.code1==right_reply && enable_bti.code2==command_code) // ccb: right reply
    enable_bti.ccbserver_code = 0;
  else // wrong ccb reply
    enable_bti.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

   enable_bti.msg = "\nEnable BTI (0x51)\n";

  if (enable_bti.ccbserver_code!=0) {// Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(enable_bti.ccbserver_code)); enable_bti.msg+=tmpstr;
  } else {

    sprintf(tmpstr,"code1: %#2X\n", enable_bti.code1); enable_bti.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n", enable_bti.code2); enable_bti.msg+=tmpstr;

    switch (enable_bti.result) {
    case 0:
      enable_bti.msg+="Result: Verify error\n";
      break;
    case 1:
      enable_bti.msg+="Result: Write successful\n";
      break;
    case -1:
      enable_bti.msg+="Result: Parameter error\n";
      break;
    }
  }

  return idx;
}


// Print results of "Enable BTI" (0x51) command to standard output
// Author:  A.Parenti Jan07 2005
// Modified: AP, Dec06 2006
void Cbti::enable_BTI_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",enable_bti.msg.c_str());
}


/*********************************************/
/* "Load BTI emulation trace" (0x52) command */
/*********************************************/

// Send "Load BTI emulation trace" (0x52) command and decode reply
// Author: A. Parenti, Jan16 2005
// Modified: AP, Mar24 2005
// Note: Tested at LNL, Apr29 2005
void Cbti::load_BTI_emulation(char bti_brd, char bti_chip, short trace[9]) {

  const unsigned char command_code=0x52;
  unsigned char command_line[21];
  int i, send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=bti_brd;
  command_line[2]=bti_chip;
  for (i=0;i<9;++i)
    wstring(trace[i],3+2*i,command_line);

  load_BTI_emulation_reset(); // Reset load_bti_emul struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,21,LOAD_BTI_EMUL_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = load_BTI_emulation_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    load_bti_emul.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nLoad BTI emulation trace (0x52): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset load_bti_emul struct
// Author: A.Parenti Aug03 2006
// Modified: AP, Dec06 2006
void Cbti::load_BTI_emulation_reset() {
  if (this==NULL) return; // Object deleted -> return

  load_bti_emul.code1 = load_bti_emul.code2 = 0;
  load_bti_emul.result = 0;

  load_bti_emul.ccbserver_code=-5;

  load_bti_emul.msg="";
}


// Decode "Load BTI emulation trace" (0x52) reply
// Author: A. Parenti, Jan29 2005
int Cbti::load_BTI_emulation_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x52, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  load_bti_emul.code1  = rdstr[0];
  load_bti_emul.code2  = rdstr[1];

// Fill-up the "result" structure
  load_bti_emul.result = rdstr[2];

  idx=3;

// Fill-up the error code
  if (load_bti_emul.code1==CCB_BUSY_CODE) // ccb is busy
    load_bti_emul.ccbserver_code = -1;
  else if (load_bti_emul.code1==CCB_UNKNOWN1 && load_bti_emul.code2==CCB_UNKNOWN2) // ccb: unknown command
    load_bti_emul.ccbserver_code = 1;
  else if (load_bti_emul.code1==right_reply && load_bti_emul.code2==command_code) // ccb: right reply
    load_bti_emul.ccbserver_code = 0;
  else // wrong ccb reply
    load_bti_emul.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

   load_bti_emul.msg = "\nLoad BTI emulation trace (0x52)\n";

  if (load_bti_emul.ccbserver_code!=0) {// Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(load_bti_emul.ccbserver_code)); load_bti_emul.msg+=tmpstr;
  } else {

    sprintf(tmpstr,"code1: %#2X\n", load_bti_emul.code1); load_bti_emul.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n", load_bti_emul.code2); load_bti_emul.msg+=tmpstr;

    switch (load_bti_emul.result) {
    case 0:
      sprintf(tmpstr,"Result: Verify error\n"); load_bti_emul.msg+=tmpstr;
      break;
    case 1:
      sprintf(tmpstr,"Result: Write successful\n"); load_bti_emul.msg+=tmpstr;
      break;
    case -1:
      sprintf(tmpstr,"Result: Parameter error\n"); load_bti_emul.msg+=tmpstr;
      break;
    }
  }

  return idx;
}


// Print results of "Load BTI emulation trace" (0x52) command to standard output
// Author:  A.Parenti Jan16 2005
// Modified: AP, Aug24 2006
void Cbti::load_BTI_emulation_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",load_bti_emul.msg.c_str());
}


/****************************************/
/* "Start BTI emulation" (0x53) command */
/****************************************/

// Send "Start BTI emulation" (0x53) command and decode reply
// Author: A. Parenti, Jan16 2005
// Modified: AP, Mar24 2005
// Note: Tested at LNL, Apr29 2005
void Cbti::start_BTI_emulation(char bti_brd, char bti_chip) {

  const unsigned char command_code=0x53;
  unsigned char command_line[3];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=bti_brd;
  command_line[2]=bti_chip;

  start_BTI_emulation_reset(); // Reset start_bti_emul struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,3,START_BTI_EMUL_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = start_BTI_emulation_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    start_bti_emul.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nStart BTI emulation (0x53): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset start_bti_emul struct
// Author: A.Parenti Aug03 2006
// Modified: AP, Dec06 2006
void Cbti::start_BTI_emulation_reset() {
  if (this==NULL) return; // Object deleted -> return

  start_bti_emul.code1 = start_bti_emul.code2 = 0;
  start_bti_emul.result = 0;

  start_bti_emul.ccbserver_code=-5;

  start_bti_emul.msg="";
}


// Decode "Start BTI emulation" (0x53) reply
// Author: A. Parenti, Jan29 2005
// Modified: AP, Dec12 2006
int Cbti::start_BTI_emulation_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x53, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  start_bti_emul.code1  = rdstr[0];
  start_bti_emul.code2  = rdstr[1];

// Fill-up the "result" structure
  start_bti_emul.result = rdstr[2];

  idx=3;

// Fill-up the error code
  if (start_bti_emul.code1==CCB_BUSY_CODE) // ccb is busy
    start_bti_emul.ccbserver_code = -1;
  else if (start_bti_emul.code1==CCB_UNKNOWN1 && start_bti_emul.code2==CCB_UNKNOWN2) // ccb: unknown command
    start_bti_emul.ccbserver_code = 1;
  else if (start_bti_emul.code1==right_reply && start_bti_emul.code2==command_code) // ccb: right reply
    start_bti_emul.ccbserver_code = 0;
  else // wrong ccb reply
    start_bti_emul.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  start_bti_emul.msg = "\nStart BTI emulation (0x53)\n";

  if (start_bti_emul.ccbserver_code!=0) {// Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(start_bti_emul.ccbserver_code)); start_bti_emul.msg+=tmpstr;
  } else {

    sprintf(tmpstr,"code1: %#2X\n", start_bti_emul.code1); start_bti_emul.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n", start_bti_emul.code2); start_bti_emul.msg+=tmpstr;

    switch (start_bti_emul.result) {
    case 1:
      start_bti_emul.msg+="Result: Write successful\n";
      break;
    case 0:
      start_bti_emul.msg+="Result: Parameter error\n";
      break;
    }
  }

  return idx;
}



// Print results of "Start BTI emulation" (0x53) command to standard output
// Author:  A.Parenti Jan16 2005
// Modified: AP, Dec06 2006
void Cbti::start_BTI_emulation_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",start_bti_emul.msg.c_str());
}

/********************************************/
/* "New Start BTI emulation" (0x79) command */
/********************************************/

// Send "New Start BTI emulation" (0x79) command and decode reply
// Author: A. Parenti, Jan16 2005
// Modified: AP, Mar24 2005
// Note: Tested at LNL, Apr29 2005
void Cbti::new_start_BTI_emulation(char bti_brd, int btimsk) {

  const unsigned char command_code=0x79;
  unsigned char command_line[6];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=bti_brd;
  wstring(btimsk,2,command_line);

  new_start_BTI_emulation_reset(); // Reset new_start_bti_emul struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,6,START_BTI_EMUL_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = new_start_BTI_emulation_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    new_start_bti_emul.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nNew Start BTI emulation (0x79): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset new_start_bti_emul struct
// Author: A.Parenti Aug03 2006
// Modified: AP, Dec06 2006
void Cbti::new_start_BTI_emulation_reset() {
  if (this==NULL) return; // Object deleted -> return

  new_start_bti_emul.code1 = new_start_bti_emul.code2 = 0;
  new_start_bti_emul.result = 0;

  new_start_bti_emul.ccbserver_code=-5;

  new_start_bti_emul.msg="";
}


// Decode "New Start BTI emulation" (0x79) reply
// Author: A. Parenti, Jan29 2005
// Modified: AP, Dec12 2006
int Cbti::new_start_BTI_emulation_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x79, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  new_start_bti_emul.code1  = rdstr[0];
  new_start_bti_emul.code2  = rdstr[1];

// Fill-up the "result" structure
  new_start_bti_emul.result = rdstr[2];

  idx=3;

// Fill-up the error code
  if (new_start_bti_emul.code1==CCB_BUSY_CODE) // ccb is busy
    new_start_bti_emul.ccbserver_code = -1;
  else if (new_start_bti_emul.code1==CCB_UNKNOWN1 && new_start_bti_emul.code2==CCB_UNKNOWN2) // ccb: unknown command
    new_start_bti_emul.ccbserver_code = 1;
  else if (new_start_bti_emul.code1==right_reply && new_start_bti_emul.code2==command_code) // ccb: right reply
    new_start_bti_emul.ccbserver_code = 0;
  else // wrong ccb reply
    new_start_bti_emul.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

   new_start_bti_emul.msg = "\nNew Start BTI emulation (0x79)\n";

  if (new_start_bti_emul.ccbserver_code!=0) {// Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(new_start_bti_emul.ccbserver_code)); new_start_bti_emul.msg+=tmpstr;
  } else {

    sprintf(tmpstr,"code1: %#2X\n", new_start_bti_emul.code1); new_start_bti_emul.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n", new_start_bti_emul.code2); new_start_bti_emul.msg+=tmpstr;

    switch (new_start_bti_emul.result) {
    case 1:
      new_start_bti_emul.msg+="Result: Write successful\n";
      break;
    case 0:
      new_start_bti_emul.msg+="Result: Parameter error\n";
      break;
    }
  }

  return idx;
}


// Print results of "New Start BTI emulation" (0x79) command to standard output
// Author:  A.Parenti Jan16 2005
// Modified: AP, Dec06 2006
void Cbti::new_start_BTI_emulation_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",new_start_bti_emul.msg.c_str());
}



/*****************************************/
/* "Compare BTI register" (0x9C) command */
/*****************************************/

// Send "Compare BTI register" (0x9C) command and decode reply
// Author: A. Parenti, Sep05 2005
// Modified: AP, Dec07 2006
void Cbti::compare_BTI_register(char bti_brd, char bti_chip) {

  const unsigned char command_code=0x9C;
  unsigned char command_line[3];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

  mc_firmware_version(); // Get firmware version
  compare_BTI_register_reset(); // Reset comp_bti_register struct
  if (HVer<1 || (HVer==1 && LVer<11)) { // Firmware < v1.11
    char tmpstr[100]; 
    sprintf(tmpstr,"Attention: command %#X supported after v1.11 (actual: v%d.%d)\n",command_code,HVer,LVer);
    comp_bti_register.msg=tmpstr;
    std::cout << tmpstr;
    return;
  }

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=bti_brd;
  command_line[2]=bti_chip;

// Send the command line
  send_command_result = cmd_obj->send_command(command_line,3,COMP_BTI_REGISTER_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = compare_BTI_register_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    comp_bti_register.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nCompare BTI register (0x9C): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset comp_bti_register struct
// Author: A.Parenti Aug03 2006
// Modified: AP, Dec06 2006
void Cbti::compare_BTI_register_reset() {
  int i;

  if (this==NULL) return; // Object deleted -> return

  comp_bti_register.code=0;
  for (i=0;i<11;++i)
    comp_bti_register.conf[i]=0;
  for (i=0;i<13;++i)
    comp_bti_register.testin[i]=0;

  comp_bti_register.ccbserver_code=-5;

  comp_bti_register.msg="";
}


// Decode "Compare BTI register" (0x9C) reply
// Author: A. Parenti, Sep05 2005
// Modified: AP, Dec06 2006
int Cbti::compare_BTI_register_decode(unsigned char *rdstr) {
  const unsigned char right_reply=0x9D;
  unsigned char code2;
  int idx=0, i;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&comp_bti_register.code);
  code2  = rdstr[1];

// Fill-up the "result" structure
  for (i=0;i<11;++i)
    idx = rstring(rdstr,idx,comp_bti_register.conf+i);
  for (i=0;i<13;++i)
    idx = rstring(rdstr,idx,comp_bti_register.testin+i);

// Fill-up the error code
  if (comp_bti_register.code==CCB_BUSY_CODE) // ccb is busy
    comp_bti_register.ccbserver_code = -1;
  else if (comp_bti_register.code==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    comp_bti_register.ccbserver_code = 1;
  else if (comp_bti_register.code==right_reply) // ccb: right reply
    comp_bti_register.ccbserver_code = 0;
  else // wrong ccb reply
    comp_bti_register.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

   comp_bti_register.msg = "\nCompare BTI register (0x9C)\n";

  if (comp_bti_register.ccbserver_code!=0) {// Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(comp_bti_register.ccbserver_code)); comp_bti_register.msg+=tmpstr;
  } else {

    sprintf(tmpstr,"code: %#2X\n", comp_bti_register.code); comp_bti_register.msg+=tmpstr;
    for (i=0;i<11;++i) {
      sprintf(tmpstr,"conf[%d]: %#2X\n", i, comp_bti_register.conf[i]); comp_bti_register.msg+=tmpstr;
    }
    for (i=0;i<13;++i) {
      sprintf(tmpstr,"testin[%d]: %#2X\n", i, comp_bti_register.testin[i]); comp_bti_register.msg+=tmpstr;
    }
  }

  return idx;
}


// Print results of "Compare BTI register" (0x9C) command to standard output
// Author:  A.Parenti Sep05 2005
// Modified: AP, Dec06 2006
void Cbti::compare_BTI_register_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",comp_bti_register.msg.c_str());
}

