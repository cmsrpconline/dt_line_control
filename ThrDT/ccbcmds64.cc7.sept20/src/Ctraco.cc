#include <ccbcmds/Ctraco.h>
#include <ccbcmds/apfunctions.h>

#include <iostream>
#include <cstdio>

/*****************************************************************************/
// TRACO class
/*****************************************************************************/


// Class constructor; arguments: pointer_to_command_object
// Author: A.Parenti, Dec01 2006
Ctraco::Ctraco(Ccommand* ppp) : Ccmn_cmd(ppp) {
  read_TRACO_reset(); // Reset read_traco struct
  write_TRACO_reset(); // Reset write_traco struct
  write_TRACO_LUTS_reset(); // Reset write_traco_luts struct
  write_MC_LUTS_reset(); // Reset write_mc_luts struct
  preload_LUTS_reset(); // Reset preload_luts struct
  load_LUTS_reset(); // Reset load_luts struct
  read_LUTS_reset(); // Reset read_luts struct
  read_LUTS_param_reset(); // Reset read_luts_param struct
  read_MC_LUTS_param_reset(); // Reset read_luts_param struct
}


/****************************/
/* Override ROB/TDC methods */
/****************************/

void Ctraco::read_TDC(char tdc_brd, char tdc_chip) {}
int Ctraco::read_TDC_decode(unsigned char *rdstr) {return 0;}
void Ctraco::read_TDC_print() {}

void Ctraco::status_TDC(char tdc_brd, char tdc_chip) {}
void Ctraco::status_TDC_print() {}

void Ctraco::read_ROB_error() {}
int Ctraco::read_ROB_error_decode(unsigned char *rdstr) {return 0;}
void Ctraco::read_ROB_error_print() {}

/*******************/
/* Configure TRACO */
/*******************/

// Configure a TRACO, reading from a "myconf" object
// Author: A.Parenti, Sep02 2005
// Modified: AP, Apr10 2006
// Modified: AP, Nov17 2006 (Added CRC) 
short Ctraco::config_TRACO(Cconf *myconf, char traco_brd, char traco_chip) {
  short conferr, confstatus, configcrcfe,configcrcdaq,configcrctrig;
  if (this==NULL) return 0; // Object deleted -> return

// TRACO error is BIT1 of conferr
  conferr = config_mc(myconf,CTRACO,traco_brd,traco_chip,&confstatus,&configcrcfe,&configcrcdaq,&configcrctrig);

  if (((conferr>>1)&0x1)!=0) {
    config_TRACO_result.error = true;
    config_TRACO_result.crc=0;
    return 1;
  }
  else {
    config_TRACO_result.error = false;
    config_TRACO_result.crc=configcrctrig;
    return 0;
  }
}

// Print to stdout the result of TRACO configuration
// Author: A.Parenti, Sep02 2005
// Modified: AP, Nov17 2006
void Ctraco::config_TRACO_print() {
  if (this==NULL) return; // Object deleted -> return

  if (config_TRACO_result.sent>0) {
    printf("Configure TRACO: %d lines sent (%d successful)\n",config_TRACO_result.sent,config_TRACO_result.sent-config_TRACO_result.bad);
    printf("                 CRC=%#hX\n",config_TRACO_result.crc);
    printf("                 Error=%d\n",config_TRACO_result.error);
  }
}


// Retrive TRACO config from CCB, dump it to file
// Author: A.Parenti, Oct30 2006
void Ctraco::read_TRACO_config(char *filename) {
  char brd,chip;
  unsigned char cmdline[41];
  char strcmd[124];
  int i;
  std::ofstream outfile(filename,std::ofstream::app);

  if (this==NULL) return; // Object deleted -> return

  printf("Read TRACO config (0x15).\n");

  if (outfile.is_open()) {
// TRACO, 0x15
    for (brd=0;brd<=5;++brd)
      for (chip=0;chip<=3;++chip) {
        printf("Brd:%d Chip:%d ...",brd,chip);

        read_TRACO(brd,chip);

        if (read_traco.code1==0x1C) {
          cmdline[0]=0x15;
          cmdline[1]=brd;
          cmdline[2]=chip;
          mystrcpy(cmdline+3,read_traco.conf,10);
          mystrcpy(cmdline+13,read_traco.testin,28);

          strcpy(strcmd,"");
          sprintf(strcmd,"%02X",cmdline[0]);
          for (i=1;i<41;++i)
            sprintf(strcmd,"%s %02X",strcmd,cmdline[i]);
          outfile << strcmd << '\n';
          outfile.flush();
        }

        printf("done!\n");
      }

    outfile.close();
  } else
    printf("Impossible to open file '%s'.\n",filename);

}


/*****************/
/* Configure LUT */
/*****************/

// Configure a LUT, reading from a "myconf" object
// Author: A.Parenti, Sep02 2005
// Modified: AP, Apr10 2006
// Modified: AP, Nov17 2006 (Added CRC) 
short Ctraco::config_LUT(Cconf *myconf, char lut_brd, char lut_chip) {
  short conferr, confstatus, confcrcfe,confcrcdaq,confcrctrig;
  if (this==NULL) return 0; // Object deleted -> return

// LUT error is BIT2 of conferr
  conferr = config_mc(myconf,CLUT,lut_brd,lut_chip,&confstatus,&confcrcfe,&confcrcdaq,&confcrctrig);

  if (((conferr>>2)&0x1)!=0) {
    config_LUT_result.error = true;
    config_LUT_result.crc=0;
    return 1;
  }
  else {
    config_LUT_result.error = false;
    config_LUT_result.crc=confcrctrig;
    return 0;
  }
}

// Print to stdout the result of LUT configuration
// Author: A.Parenti, Sep02 2005
// Modified: AP, Nov17 2006
void Ctraco::config_LUT_print() {
  if (this==NULL) return; // Object deleted -> return

  if (config_LUT_result.sent>0) {
    printf("Configure LUT: %d lines sent (%d successful)\n",config_LUT_result.sent,config_LUT_result.sent-config_LUT_result.bad);
    printf("               CRC=%#hX\n",config_LUT_result.crc);
    printf("               Error=%d\n",config_LUT_result.error);
  }
}


// Retrive LUT config from CCB, dump it to file
// Author: A.Parenti, Oct30 2006
// Modified: AP, Dec01 2006
void Ctraco::read_LUT_config(char *filename) {
  char brd,chip;
  unsigned char cmdline[19];
  char strcmd[58];
  int i;
  std::ofstream outfile(filename,std::ofstream::app);

  if (this==NULL) return; // Object deleted -> return

  printf("Read LUT config (0x3E).\n");

  if (outfile.is_open()) {
// LUT, 0x3E
    for (brd=0;brd<=5;++brd)
      for (chip=0;chip<=3;++chip) {
        printf("Brd:%d Chip:%d ...",brd,chip);

        read_LUTS_param(brd,chip);

        if (read_luts_param.code1==0x89) {
          cmdline[0]=0x3E;
          cmdline[1]=brd;
          cmdline[2]=chip;
          wstring(read_luts_param.shift,3,cmdline);
          wstring(read_luts_param.ST,5,cmdline);
          wstring(read_luts_param.d,7,cmdline);
          wstring(read_luts_param.Xc,11,cmdline);
          wstring(read_luts_param.Xn,15,cmdline);

          strcpy(strcmd,"");
          sprintf(strcmd,"%02X",cmdline[0]);
          for (i=1;i<19;++i)
            sprintf(strcmd,"%s %02X",strcmd,cmdline[i]);
          outfile << strcmd << '\n';
          outfile.flush();
        }

        printf("done!\n");
      }

// LUT, 0xA8
    read_MC_LUTS_param();
    if (read_mc_lut_param.code1==0xA9) {
      cmdline[0]=0xA8;
      wstring(read_mc_lut_param.ST,1,cmdline);
      wstring(read_mc_lut_param.d,3,cmdline);
      wstring(read_mc_lut_param.Xcn,7,cmdline);
      wstring(read_mc_lut_param.wheel_sign,11,cmdline);

      strcpy(strcmd,"");
      sprintf(strcmd,"%02X",cmdline[0]);
      for (i=1;i<12;++i)
        sprintf(strcmd,"%s %02X",strcmd,cmdline[i]);
      outfile << strcmd << '\n';
      outfile.flush();
    }

    outfile.close();
  } else
    printf("Impossible to open file '%s'.\n",filename);

}

/*******************************/
/* "Read TRACO" (0x1B) command */
/*******************************/

// Send "Read TRACO" (0x1B) command and decode reply
// Author: A. Parenti, Dec18 2004
// Modified: AP, Mar24 2005
// Note: Tested at Cessy, Oct05 2006
void Ctraco::read_TRACO(char traco_brd, char traco_chip) {
  const unsigned char command_code=0x1B;
  unsigned char command_line[3];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=traco_brd;
  command_line[2]=traco_chip;

  read_TRACO_reset(); // Reset read_traco struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,3,READ_TRACO_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = read_TRACO_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    read_traco.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nRead TRACO (0x1B): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset read_traco struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec07 2006
void Ctraco::read_TRACO_reset() {
  int i;

  if (this==NULL) return; // Object deleted -> return

  read_traco.code1=0;
  for (i=0;i<10;++i)
    read_traco.conf[i]=0;
  for (i=0;i<28;++i)
    read_traco.testin[i]=0;
  for (i=0;i<22;++i)
    read_traco.testout[i]=0;

  read_traco.code2=read_traco.narg=0;
  read_traco.ccbserver_code=-5;
  read_traco.msg="";
}


// Decode "Read TRACO" (0x1B) reply
// Author: A. Parenti, Feb08 2005
// Modified: AP, Dec07 2006
int Ctraco::read_TRACO_decode(unsigned char *rdstr) {
  const unsigned char right_reply=0x1C, command_code=0x1B;
  unsigned char code2;
  int i, idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&read_traco.code1);
  code2 = rdstr[idx];

// Fill-up the "result" structure
  if (read_traco.code1==right_reply) {
    for (i=0;i<10;++i)
      idx = rstring(rdstr,idx,read_traco.conf+i);
    for (i=0;i<28;++i)
      idx = rstring(rdstr,idx,read_traco.testin+i);
    for (i=0;i<22;++i)
      idx = rstring(rdstr,idx,read_traco.testout+i);
  } else if (read_traco.code1==CCB_OXFC && code2==command_code) {
    idx = rstring(rdstr,idx,&read_traco.code2);
    idx = rstring(rdstr,idx,&read_traco.narg);
  }

// Fill-up the error code
  if (read_traco.code1==CCB_BUSY_CODE) // ccb is busy
    read_traco.ccbserver_code = -1;
  else if (read_traco.code1==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    read_traco.ccbserver_code = 1;
  else if (read_traco.code1==right_reply) // ccb: right reply
    read_traco.ccbserver_code = 0;
  else if (read_traco.code1==CCB_OXFC && code2==command_code) // ccb: right reply
    read_traco.ccbserver_code = 0;
  else // wrong ccb reply
    read_traco.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  read_traco.msg="\nRead TRACO (0x1B)\n";

  if (read_traco.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(read_traco.ccbserver_code)); read_traco.msg+=tmpstr;
  } else {
    if (read_traco.code1==right_reply) {
      sprintf(tmpstr,"code: %#2X\n",read_traco.code1); read_traco.msg+=tmpstr;
      for (i=0;i<10;++i) {
        sprintf(tmpstr,"conf[%i]: %#2X\n",i,read_traco.conf[i]); read_traco.msg+=tmpstr;
      }
      for (i=0;i<28;++i) {
        sprintf(tmpstr,"testin[%i]: %#2X\n",i,read_traco.testin[i]); read_traco.msg+=tmpstr;
      }
      for (i=0;i<22;++i) {
        sprintf(tmpstr,"testout[%i]: %#2X\n",i,read_traco.testout[i]); read_traco.msg+=tmpstr;
      }
    } else {
      sprintf(tmpstr,"code1: %#2X\n",read_traco.code1); read_traco.msg+=tmpstr;
      sprintf(tmpstr,"code2: %#2X\n",read_traco.code2); read_traco.msg+=tmpstr;
      sprintf(tmpstr,"Wrong argument. narg: %d\n", read_traco.narg); read_traco.msg+=tmpstr;
    }
  }

  return idx;
}



// Print result of "Read TRACO" (0x1B) to stdout
// Author: A. Parenti, Dec18 2004
// Modified: AP, Dec07 2006
void Ctraco::read_TRACO_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",read_traco.msg.c_str());
}


/********************************/
/* "Write TRACO" (0x15) command */
/********************************/

// Send "Write TRACO" (0x15) command and decode reply
// Author: A. Parenti, Dec18 2004
// Note: Tested at LNL, Mar09 2005
// Modified: AP, Mar24 2005
void Ctraco::write_TRACO(char traco_brd, char traco_chip, unsigned char conf[10], unsigned char testin[28]) {
  const unsigned char command_code=0x15;
  unsigned char command_line[41];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=traco_brd;
  command_line[2]=traco_chip;
  mystrcpy(command_line+3,conf,10);
  mystrcpy(command_line+13,testin,28);

  write_TRACO_reset(); // Reset write_traco struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,41,WRITE_TRACO_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = write_TRACO_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    write_traco.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nWrite TRACO (0x15): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset write_traco struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec07 2006
void Ctraco::write_TRACO_reset() {
  if (this==NULL) return; // Object deleted -> return

  write_traco.code1=write_traco.code2=write_traco.result=0;

  write_traco.ccbserver_code=-5;
  write_traco.msg="";
}


// Decode "Write TRACO" (0x15) reply
// Author: A. Parenti, Feb08 2005
// Modified: AP, Dec12 2006
int Ctraco::write_TRACO_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x15, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  write_traco.code1  = rdstr[0];
  write_traco.code2  = rdstr[1];

// Fill-up the "result" structure
  write_traco.result = rdstr[2];

  idx=3;

// Fill-up the error code
  if (write_traco.code1==CCB_BUSY_CODE) // ccb is busy
    write_traco.ccbserver_code = -1;
  else if (write_traco.code1==CCB_UNKNOWN1 && write_traco.code2==CCB_UNKNOWN2) // ccb: unknown command
    write_traco.ccbserver_code = 1;
  else if (write_traco.code1==right_reply && write_traco.code2==command_code) // ccb: right reply
    write_traco.ccbserver_code = 0;
  else // wrong ccb reply
    write_traco.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  write_traco.msg="\nWrite TRACO (0x15)\n";
  if (write_traco.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(write_traco.ccbserver_code)); write_traco.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n", write_traco.code1); write_traco.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n", write_traco.code2); write_traco.msg+=tmpstr;

    switch (write_traco.result) {
    case 0:
      write_traco.msg+="Result: Verify error\n";
      break;
    case 1:
      write_traco.msg+="Result: Write successful\n";
      break;
    case -1:
      write_traco.msg+="Result: Parameter error\n";
      break;
    }
  }

  return idx;
}


// Print results of "Write TRACO" (0x15) command to standard output
// Author:  A.Parenti Dec18 2004
// Modified: AP, Dec07 2006
void Ctraco::write_TRACO_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",write_traco.msg.c_str());
}


/*************************************/
/* "Write TRACO LUTS" (0x3E) command */
/*************************************/

// Send "Write TRACO LUTS" (0x3E) command and decode reply
// Author: A. Parenti, Jan03 2005
// Modified: AP, Dec07 2006
void Ctraco::write_TRACO_LUTS(char traco_brd, char traco_chip, short shift, short ST, float d, float Xc, float Xn) {
  const unsigned char command_code=0x3E;
  unsigned char command_line[19];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

  mc_firmware_version(); // Get firmware version
  write_TRACO_LUTS_reset(); // Reset write_traco_luts struct
  if (HVer>1 || (HVer==1 && LVer>17)) { // Firmware > v1.17
    char tmpstr[100];
    sprintf(tmpstr,"Attention: command %#X supported until v1.17 (actual: v%d.%d)\n",command_code,HVer,LVer);
    write_traco_luts.msg=tmpstr;
    std::cout << tmpstr;
    return;
  }

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=traco_brd;
  command_line[2]=traco_chip;
  wstring(shift,3,command_line);
  wstring(ST,5,command_line);
  wstring(d,7,command_line);
  wstring(Xc,11,command_line);
  wstring(Xn,15,command_line);

// Send the command line
  send_command_result = cmd_obj->send_command(command_line,19,WRITE_TRACO_LUTS_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = write_TRACO_LUTS_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    write_traco_luts.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nWrite TRACO LUTS (0x3E): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset write_traco_luts struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec07 2006
void Ctraco::write_TRACO_LUTS_reset() {
  if (this==NULL) return; // Object deleted -> return

  write_traco_luts.code1=write_traco_luts.code2=write_traco_luts.result=0;

  write_traco_luts.ccbserver_code=-5;
  write_traco_luts.msg="";
}


// Decode "Write TRACO LUTS" (0x3E) reply
// Author: A. Parenti, Feb08 2005
// Modified: AP, Dec12 2006
int Ctraco::write_TRACO_LUTS_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x3E, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  write_traco_luts.code1  = rdstr[0];
  write_traco_luts.code2  = rdstr[1];

// Fill-up the "result" structure
  write_traco_luts.result = rdstr[2];

  idx=3;

// Fill-up the error code
  if (write_traco_luts.code1==CCB_BUSY_CODE) // ccb is busy
    write_traco_luts.ccbserver_code = -1;
  else if (write_traco_luts.code1==CCB_UNKNOWN1 && write_traco_luts.code2==CCB_UNKNOWN2) // ccb: unknown command
    write_traco_luts.ccbserver_code = 1;
  else if (write_traco_luts.code1==right_reply && write_traco_luts.code2==command_code) // ccb: right reply
    write_traco_luts.ccbserver_code = 0;
  else // wrong ccb reply
    write_traco_luts.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];


  write_traco_luts.msg="\nWrite TRACO LUTS (0x3E)\n";

  if (write_traco_luts.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(write_traco_luts.ccbserver_code)); write_traco_luts.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n", write_traco_luts.code1); write_traco_luts.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n", write_traco_luts.code2); write_traco_luts.msg+=tmpstr;

    switch (write_traco_luts.result) {
    case 0:
      write_traco_luts.msg+="Result: Verify error\n";
      break;
    case 1:
      write_traco_luts.msg+="Result: Write successful\n";
      break;
    case -1:
      write_traco_luts.msg+="Result: Parameter error\n";
      break;
    case -2:
      write_traco_luts.msg+="Result: Run in progress. LUTs weren't written on TRACO.\n";
      break;
    }
  }

  return idx;
}


// Print results of "Write TRACO LUTS" (0x3E) command to standard output
// Author:  A.Parenti Jan03 2005
// Modified: AP, Dec07 2006
void Ctraco::write_TRACO_LUTS_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",write_traco_luts.msg.c_str());
}


/*****************************************/
/* "Write minicrate LUTS" (0xA8) command */
/*****************************************/

// Send "Write minicrate LUTS" (0xA8) command and decode reply
// Author: A. Parenti, Nov30 2006
// Modified: AP, Dec07 2006
void Ctraco::write_MC_LUTS(short ST, float d, float Xcn, short wheel_sign) {
  const unsigned char command_code=0xA8;
  unsigned char command_line[12];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

  mc_firmware_version(); // Get firmware version
  write_MC_LUTS_reset(); // Reset write_mc_luts struct
  if (HVer<1 || (HVer==1 && LVer<18)) { // Firmware < v1.18
    char tmpstr[100];
    sprintf(tmpstr,"Attention: command %#X supported from v1.18 (actual: v%d.%d)\n",command_code,HVer,LVer);
    write_mc_luts.msg=tmpstr;
    std::cout << tmpstr;
    return;
  }

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  wstring(ST,1,command_line);
  wstring(d,3,command_line);
  wstring(Xcn,7,command_line);
  wstring(wheel_sign,11,command_line);

// Send the command line
  send_command_result = cmd_obj->send_command(command_line,12,WRITE_MC_LUTS_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = write_MC_LUTS_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    write_mc_luts.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nWrite MC LUTS (0xA8): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset write_mc_luts struct
// Author: A.Parenti, Nov30 2006
// Modified: AP, Dec07 2006
void Ctraco::write_MC_LUTS_reset() {
  if (this==NULL) return; // Object deleted -> return

  write_mc_luts.code1=write_mc_luts.code2=write_mc_luts.result=0;

  write_mc_luts.ccbserver_code=-5;
  write_mc_luts.msg="";
}


// Decode "Write minicrate LUTS" (0xA8) reply
// Author: A. Parenti, Nov30 2006
// Modified: AP, Dec12 2006
int Ctraco::write_MC_LUTS_decode(unsigned char *rdstr) {
  const unsigned char command_code=0xA8, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  write_mc_luts.code1  = rdstr[0];
  write_mc_luts.code2  = rdstr[1];

// Fill-up the "result" structure
  write_mc_luts.result = rdstr[2];

  idx=3;

// Fill-up the error code
  if (write_mc_luts.code1==CCB_BUSY_CODE) // ccb is busy
    write_mc_luts.ccbserver_code = -1;
  else if (write_mc_luts.code1==CCB_UNKNOWN1 && write_mc_luts.code2==CCB_UNKNOWN2) // ccb: unknown command
    write_mc_luts.ccbserver_code = 1;
  else if (write_mc_luts.code1==right_reply && write_mc_luts.code2==command_code) // ccb: right reply
    write_mc_luts.ccbserver_code = 0;
  else // wrong ccb reply
    write_mc_luts.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  write_mc_luts.msg="\nWrite MC LUTS (0xA8)\n";

  if (write_mc_luts.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(write_mc_luts.ccbserver_code)); write_mc_luts.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n", write_mc_luts.code1); write_mc_luts.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n", write_mc_luts.code2); write_mc_luts.msg+=tmpstr;

    if (write_mc_luts.result==0) {
      write_mc_luts.msg+="Result: Success\n";
    }
    else if (write_mc_luts.result==-2) {
      write_mc_luts.msg+="Result: Run in progress. LUTs weren't written on TRACO.\n";
    }
    else if (write_mc_luts.result>0) {
      sprintf(tmpstr,"Result: Write error (%#06X)\n",write_mc_luts.result); write_mc_luts.msg+=tmpstr;
    }
  }

  return idx;
}


// Print results of "Write minicrate LUTS" (0xA8) command to standard output
// Author:  A.Parenti Nov30 2006
// Modified: AP, Dec07 2006
void Ctraco::write_MC_LUTS_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",write_mc_luts.msg.c_str());
}



/*********************************/
/* "Preload LUTS" (0x4D) command */
/*********************************/

// Send "preload LUTS" (0x4D) command and decode reply
// Author: A. Parenti, Jan05 2005
// Modified: AP, Mar24 2005
// Tested at PD, Mar29 2005
void Ctraco::preload_LUTS(char traco_brd, char traco_chip, char ang, short addr, short data[64]) {
  const unsigned char command_code=0x4D;
  unsigned char command_line[134];
  int i, send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=traco_brd;
  command_line[2]=traco_chip;
  command_line[3]=ang;
  wstring(addr,4,command_line);
  for (i=0;i<64;++i)
    wstring(data[i],6+2*i,command_line);

  preload_LUTS_reset(); // Reset preload_luts struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,134,PRELOAD_LUTS_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = preload_LUTS_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    preload_luts.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nPreload LUTS (0x4D): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset preload_luts struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec07 2006
void Ctraco::preload_LUTS_reset() {
  if (this==NULL) return; // Object deleted -> return

  preload_luts.code1=preload_luts.code2=preload_luts.result=0;

  preload_luts.ccbserver_code=-5;
  preload_luts.msg="";
}


// Decode "preload LUTS" (0x4D) reply
// Author: A. Parenti, Feb08 2005
// Modified: AP, Dec12 2006
int Ctraco::preload_LUTS_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x4D, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  preload_luts.code1  = rdstr[0];
  preload_luts.code2  = rdstr[1];

// Fill-up the "result" structure
  preload_luts.result = rdstr[2];

  idx=3;

// Fill-up the error code
  if (preload_luts.code1==CCB_BUSY_CODE) // ccb is busy
    preload_luts.ccbserver_code = -1;
  else if (preload_luts.code1==CCB_UNKNOWN1 && preload_luts.code2==CCB_UNKNOWN2) // ccb: unknown command
    preload_luts.ccbserver_code = 1;
  else if (preload_luts.code1==right_reply && preload_luts.code2==command_code) // ccb: right reply
    preload_luts.ccbserver_code = 0;
  else // wrong ccb reply
    preload_luts.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  preload_luts.msg="\nPreload LUTS (0x4D)\n";

  if (preload_luts.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(preload_luts.ccbserver_code)); preload_luts.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",preload_luts.code1); preload_luts.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",preload_luts.code2); preload_luts.msg+=tmpstr;

    switch (preload_luts.result) {
    case 1:
      preload_luts.msg+="Result: preload successful\n";
      break;
    default:
      preload_luts.msg+="Result: preload error\n";
    }
  }

  return idx;
}


// Print results of "Preload LUTS" (0x4D) command to standard output
// Author:  A.Parenti Jan05 2005
// Modified: AP, Dec07 2006
void Ctraco::preload_LUTS_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",preload_luts.msg.c_str());
}


/******************************/
/* "Load LUTS" (0x4E) command */
/******************************/

// Send "load LUTS" (0x4E) command and decode reply
// Author: A. Parenti, Jan05 2005
// Modified: AP, Mar24 2005
// Tested at PD, Mar29 2005
void Ctraco::load_LUTS(char traco_brd, char traco_chip) {
  const unsigned char command_code=0x4E;
  unsigned char command_line[3];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=traco_brd;
  command_line[2]=traco_chip;

  load_LUTS_reset(); // Reset load_luts struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,3,LOAD_LUTS_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = load_LUTS_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    load_luts.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nLoad LUTS (0x4E): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset load_luts struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec07 2006
void Ctraco::load_LUTS_reset() {
  if (this==NULL) return; // Object deleted -> return

  load_luts.code1=load_luts.code2=load_luts.result=0;

  load_luts.ccbserver_code=-5;
  load_luts.msg="";
}


// Decode "load LUTS" (0x4E) reply
// Author: A. Parenti, Feb08 2005
// Modified: AP, Dec12 2006
int Ctraco::load_LUTS_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x4E, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  load_luts.code1  = rdstr[0];
  load_luts.code2  = rdstr[1];

// Fill-up the "result" structure
  load_luts.result = rdstr[2];

  idx=3;

// Fill-up the error code
  if (load_luts.code1==CCB_BUSY_CODE) // ccb is busy
    load_luts.ccbserver_code = -1;
  else if (load_luts.code1==CCB_UNKNOWN1 && load_luts.code2==CCB_UNKNOWN2) // ccb: unknown command
    load_luts.ccbserver_code = 1;
  else if (load_luts.code1==right_reply && load_luts.code2==command_code) // ccb: right reply
    load_luts.ccbserver_code = 0;
  else // wrong ccb reply
    load_luts.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  load_luts.msg="\nLoad LUTS (0x4E)\n";

  if (load_luts.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(load_luts.ccbserver_code)); load_luts.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",load_luts.code1); load_luts.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",load_luts.code2); load_luts.msg+=tmpstr;

    switch (load_luts.result) {
    case 0:
      load_luts.msg+="Result: Verify error\n";
      break;
    case 1:
      load_luts.msg+="Result: load successful\n";
      break;
    case -1:
      load_luts.msg+="Result: Parameter error\n";
      break;
    }
  }

  return idx;
}


// Print results of "Load LUTS" (0x4E) command to standard output
// Author:  A.Parenti Jan05 2005
// Modified: AP, Dec07 2006
void Ctraco::load_LUTS_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",load_luts.msg.c_str());
}



/************************************/
/* "Read Traco Luts" (0x86) command */
/************************************/

// Send "Read traco luts" (0x86) command and decode reply
// Author: A. Parenti, Jan10 2005
// Modified: AP, Mar24 2005
// Note: Tested at Cessy, Oct05 2006
void Ctraco::read_LUTS(char traco_brd, char traco_chip, char ang, short addr, char size) {
  const unsigned char command_code=0x86;
  unsigned char command_line[7];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// Check "size"
  if (size>RD_TRACO_LUT_MAX)
    size=RD_TRACO_LUT_MAX;
  if (size<RD_TRACO_LUT_MIN)
    size=RD_TRACO_LUT_MIN;

// create the command line
  command_line[0]=command_code;
  command_line[1]=traco_brd;
  command_line[2]=traco_chip;
  command_line[3]=ang;
  wstring(addr,4,command_line);
  command_line[6]=size;

  read_LUTS_reset(); // Reset read_luts struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,7,READ_LUTS_TIMEOUT);

  if (send_command_result>=0) { // alright
    read_luts.size=size;
    idx = read_LUTS_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    read_luts.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nRead TRACO LUTS (0x86): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset read_luts struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec07 2006
void Ctraco::read_LUTS_reset() {
  int i;

  if (this==NULL) return; // Object deleted -> return

  read_luts.size=0;
  read_luts.code1=0;
  for (i=0;i<64;++i)
    read_luts.data[i]=0;

  read_luts.code2=read_luts.error=0;
  read_luts.ccbserver_code=-5;
  read_luts.msg="";
}


// Decode "Read traco luts" (0x86) reply
// Author: A. Parenti, Feb08 2005
// Modified: AP, Dec07 2006
int Ctraco::read_LUTS_decode(unsigned char *rdstr) {
  const unsigned right_reply=0x87, command_code=0x86;
  unsigned char code2;
  int i, idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&read_luts.code1);
  code2 = rdstr[idx];

// Fill-up the "result" structure
  if (read_luts.code1==right_reply) {
    for (i=0;i<read_luts.size;++i)
      idx = rstring(rdstr,idx,read_luts.data+i);
  } else if (read_luts.code1==CCB_OXFC && code2==command_code) {
    idx = rstring(rdstr,idx,&read_luts.code2);
    idx = rstring(rdstr,idx,&read_luts.error);
  }

// Fill-up the error code
  if (read_luts.code1==CCB_BUSY_CODE) // ccb is busy
    read_luts.ccbserver_code = -1;
  else if (read_luts.code1==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    read_luts.ccbserver_code = 1;
  else if (read_luts.code1==right_reply) // ccb: right reply
    read_luts.ccbserver_code = 0;
  else if (read_luts.code1==CCB_OXFC && code2==command_code) // ccb: right reply
    read_luts.ccbserver_code = 0;
  else // wrong ccb reply
    read_luts.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  read_luts.msg="\nRead TRACO LUTS (0x86)\n";

  if (read_luts.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(read_luts.ccbserver_code)); read_luts.msg+=tmpstr;
  } else {
    if (read_luts.code1==right_reply) {
      sprintf(tmpstr,"code: %#2X\n", read_luts.code1); read_luts.msg+=tmpstr;
      for (i=0;i<read_luts.size;++i) {
        sprintf(tmpstr,"data[%d]: %#4X\n", i, (unsigned short)read_luts.data[i]); read_luts.msg+=tmpstr;
      }
    } else {
      sprintf(tmpstr,"code1: %#2X\n", read_luts.code1); read_luts.msg+=tmpstr;
      sprintf(tmpstr,"code2: %#2X\n", read_luts.code2); read_luts.msg+=tmpstr;
      sprintf(tmpstr,"Wrong argument. error: %d\n", read_luts.error); read_luts.msg+=tmpstr;
    }
  }

  return idx;
}


// Print results of "Read Traco Luts" (0x86) command to standard output
// Author:  A.Parenti Jan10 2005
// Modified: AP, Dec07 2006
void Ctraco::read_LUTS_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",read_luts.msg.c_str());
}

/***********************************************/
/* "Read Traco Luts parameters" (0x88) command */
/***********************************************/

// Send "Read traco LUTs parameters" (0x88) command and decode reply
// Author: A. Parenti, Jan10 2005
// Modified: AP, Mar24 2005
// Note: Tested at Cessy, Oct05 2006
// Modified: AP, Nov30 2006
void Ctraco::read_LUTS_param(char traco_brd, char traco_chip) {
  const unsigned char command_code=0x88;
  unsigned char command_line[3];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

  mc_firmware_version(); // Get firmware version
  read_LUTS_param_reset(); // Reset read_luts_param struct
  if (HVer>1 || (HVer==1 && LVer>17)) { // Firmware >= v1.17
    printf("Attention: command %#X supported until v1.17 (actual: v%d.%d)\n",command_code,HVer,LVer);
    return;
  }

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=traco_brd;
  command_line[2]=traco_chip;

// Send the command line
  send_command_result = cmd_obj->send_command(command_line,3,READ_LUTS_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = read_LUTS_param_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    read_luts_param.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nRead TRACO LUTS parameters(0x88): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset read_luts_param struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec07 2006
void Ctraco::read_LUTS_param_reset() {
  if (this==NULL) return; // Object deleted -> return

  read_luts_param.code1=0;
  read_luts_param.shift=read_luts_param.ST=0;
  read_luts_param.d=read_luts_param.Xc=read_luts_param.Xn=0;

  read_luts_param.code2=read_luts_param.error=0;
  read_luts_param.ccbserver_code=-5;
  read_luts_param.msg="";
}


// Decode "read traco LUTs parameters" (0x88) reply
// Author: A. Parenti, Feb08 2005
// Modified: AP, Dec07 2006
int Ctraco::read_LUTS_param_decode(unsigned char *rdstr) {
  const unsigned char right_reply=0x89, command_code=0x88;
  unsigned char code2;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&read_luts_param.code1);
  code2 = rdstr[idx];

// Fill-up the "result" structure
  if (read_luts_param.code1==right_reply) {
    idx = rstring(rdstr,idx,&read_luts_param.shift);
    idx = rstring(rdstr,idx,&read_luts_param.ST);
    idx = rstring(rdstr,idx,&read_luts_param.d);
    idx = rstring(rdstr,idx,&read_luts_param.Xc);
    idx = rstring(rdstr,idx,&read_luts_param.Xn);
  } else if (read_luts_param.code1==CCB_OXFC && code2==command_code) {
    idx = rstring(rdstr,idx,&read_luts_param.code2);
    idx = rstring(rdstr,idx,&read_luts_param.error);
  }

// Fill-up the error code
  if (read_luts_param.code1==CCB_BUSY_CODE) // ccb is busy
    read_luts_param.ccbserver_code = -1;
  else if (read_luts_param.code1==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    read_luts_param.ccbserver_code = 1;
  else if (read_luts_param.code1==right_reply) // ccb: right reply
    read_luts_param.ccbserver_code = 0;
  else if (read_luts_param.code1==CCB_OXFC && code2==command_code) // ccb: right reply
    read_luts_param.ccbserver_code = 0;
  else // wrong ccb reply
    read_luts_param.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  read_luts_param.msg="\nRead TRACO LUTS parameters(0x88)\n";

  if (read_luts_param.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(read_luts_param.ccbserver_code)); read_luts_param.msg+=tmpstr;
  } else {
    if (read_luts_param.code1==right_reply) {
      sprintf(tmpstr,"code: %#2X\n", read_luts_param.code1); read_luts_param.msg+=tmpstr;
      sprintf(tmpstr,"shift: %#4X\n", read_luts_param.shift); read_luts_param.msg+=tmpstr;
      sprintf(tmpstr,"ST: %#4X\n", read_luts_param.ST); read_luts_param.msg+=tmpstr;
      sprintf(tmpstr,"d: %f\n", read_luts_param.d); read_luts_param.msg+=tmpstr;
      sprintf(tmpstr,"Xc: %f\n", read_luts_param.Xc); read_luts_param.msg+=tmpstr;
      sprintf(tmpstr,"Xn: %f\n", read_luts_param.Xn); read_luts_param.msg+=tmpstr;
    } else {
      sprintf(tmpstr,"code1: %#2X\n", read_luts_param.code1); read_luts_param.msg+=tmpstr;
      sprintf(tmpstr,"code2: %#2X\n", read_luts_param.code2); read_luts_param.msg+=tmpstr;
      sprintf(tmpstr,"Wrong argument. error: %d\n", read_luts_param.error); read_luts_param.msg+=tmpstr;
    }
  }

  return idx;
}


// Print results of "Read Traco Luts parameters" (0x88) command to standard output
// Author:  A.Parenti Jan10 2005
// Modified: AP, Dec07 2006
void Ctraco::read_LUTS_param_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",read_luts_param.msg.c_str());
}


/***************************************************/
/* "Read minicrate LUT parameters" (0xA9) command */
/***************************************************/

// Send "Read minicrate LUT parameters" (0xA9) command and decode reply
// Author: A. Parenti, Nov30 2006
void Ctraco::read_MC_LUTS_param() {
  const unsigned char command_code=0xA9;
  unsigned char command_line[1];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

  mc_firmware_version(); // Get firmware version
  read_MC_LUTS_param_reset(); // Reset read_luts_param struct
  if (HVer>1 || (HVer==1 && LVer<18)) { // Firmware < v1.18
    printf("Attention: command %#X supported from v1.18 (actual: v%d.%d)\n",command_code,HVer,LVer);
    return;
  }

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;

// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1,READ_LUTS_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = read_MC_LUTS_param_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    read_mc_lut_param.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nRead minicrate LUTS parameters (0xA9): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset read_luts_param struct
// Author: A.Parenti, Nov30 2006
// Modified: AP, Dec07 2006
void Ctraco::read_MC_LUTS_param_reset() {
  if (this==NULL) return; // Object deleted -> return

  read_mc_lut_param.code1=0;
  read_mc_lut_param.ST=0;
  read_mc_lut_param.d=read_mc_lut_param.Xcn=0;
  read_mc_lut_param.wheel_sign=0;

  read_mc_lut_param.ccbserver_code=-5;
  read_mc_lut_param.msg="";
}


// Decode "read traco LUTs parameters" (0xA9) reply
// Author: A. Parenti, Nov30 2006
// Modified: AP, Dec07 2006
int Ctraco::read_MC_LUTS_param_decode(unsigned char *rdstr) {
  const unsigned char right_reply=0xAA;
  unsigned char code2;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&read_mc_lut_param.code1);
  code2 = rdstr[idx];

// Fill-up the "result" structure
  idx = rstring(rdstr,idx,&read_mc_lut_param.ST);
  idx = rstring(rdstr,idx,&read_mc_lut_param.d);
  idx = rstring(rdstr,idx,&read_mc_lut_param.Xcn);
  idx = rstring(rdstr,idx,&read_mc_lut_param.wheel_sign);

// Fill-up the error code
  if (read_mc_lut_param.code1==CCB_BUSY_CODE) // ccb is busy
    read_mc_lut_param.ccbserver_code = -1;
  else if (read_mc_lut_param.code1==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    read_mc_lut_param.ccbserver_code = 1;
  else if (read_mc_lut_param.code1==right_reply) // ccb: right reply
    read_mc_lut_param.ccbserver_code = 0;
  else // wrong ccb reply
    read_mc_lut_param.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  read_mc_lut_param.msg="\nRead minicrate LUTS parameters (0xA9)\n";

  if (read_mc_lut_param.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(read_mc_lut_param.ccbserver_code)); read_mc_lut_param.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code: %#2X\n",read_mc_lut_param.code1); read_mc_lut_param.msg+=tmpstr;
    sprintf(tmpstr,"ST: %#4X\n",read_mc_lut_param.ST); read_mc_lut_param.msg+=tmpstr;
    sprintf(tmpstr,"d: %f\n",read_mc_lut_param.d); read_mc_lut_param.msg+=tmpstr;
    sprintf(tmpstr,"Xcn: %f\n",read_mc_lut_param.Xcn); read_mc_lut_param.msg+=tmpstr;
    sprintf(tmpstr,"wheel sign: %d\n",read_mc_lut_param.wheel_sign); read_mc_lut_param.msg+=tmpstr;
  }

  return idx;
}


// Print results of "Read Traco Luts parameters" (0xA9) command to standard output
// Author:  A.Parenti Nov30 2005
// Modified: AP, Dec07 2006
void Ctraco::read_MC_LUTS_param_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",read_mc_lut_param.msg.c_str());
}

