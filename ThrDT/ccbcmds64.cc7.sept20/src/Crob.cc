#include <ccbcmds/Crob.h>
#include <ccbcmds/apfunctions.h>

#ifdef __USE_CCBDB__ // DB access enabled
  #include <ccbdb/Cccbstatus.h>    // support for cmsdtdb DB
#endif

#include <iostream>
#include <cstdio>

/*****************************************************************************/
// ROB class
/*****************************************************************************/


// Class constructor; arguments: pointer_to_command_object
// Author: A.Parenti, Dec01 2006
// Modified: AP, May10 2007
Crob::Crob(Ccommand* ppp) : Ccmn_cmd(ppp) {
  status_ROB_reset(); // Reset status_rob struct
  read_ROB_pwr_reset(); // Reset read_rob_pwr struct
  reset_ROB_reset(); // Reset reset_rob struct
  run_BIST_test_reset(); // Reset run_bist_test struct
  read_out_ROB_reset(); // Reset read_out_rob struct
  set_CPU_ck_delay_reset(); // Reset set_cpu_ck_delay struct
  disable_temp_test_reset(); // Reset rob_temp_test struct
}


/*********************/
/* Database commands */
/*********************/

// Check ROB status and log it in the DB.
// Author: A.Parenti, May16 2007
// Modified: AP, Oct30 2007
void Crob::status_ROB_to_db(Cref *Pcref) {
  if (this==NULL) return; // Object deleted -> return

  status_ROB(Pcref); // Check ROB status.

#ifdef __USE_CCBDB__ // DB access enabled

  ccbstatus *myCcbstatus;
  if (cmd_obj->dtdbobj==NULL) {
    myCcbstatus=new ccbstatus; // Create a new connection to DB
//    printf("ccbstatus: new connection\n");
  } else {
    myCcbstatus=new ccbstatus(cmd_obj->dtdbobj); // Use an existing connection to DB
//    printf("ccbstatus: existing connection\n");
  }

  if (!status_rob.ccbserver_error) {
    int i;
    float rob_temp[7];
    for (i=0;i<7;++i)
      rob_temp[i]=mc_temp.temp[i];
    myCcbstatus->rob_insert(cmd_obj->read_ccb_id(),read_rob_error.state,rob_temp,read_rob_pwr.Vcc,read_rob_pwr.Vdd,read_rob_pwr.current);
  }

  delete myCcbstatus;
#else
  nodbmsg();
#endif
}


/************************/
/* Override TDC methods */
/************************/

void Crob::read_TDC(char tdc_brd, char tdc_chip) {}
int Crob::read_TDC_decode(unsigned char *rdstr) {return 0;}
void Crob::read_TDC_print() {}

void Crob::status_TDC(char tdc_brd, char tdc_chip) {}
void Crob::status_TDC_print() {}

/*****************/
/* Configure ROB */
/*****************/

// Configure a ROB, reading from a "myconf" object
// Author: A.Parenti, Sep02 2005
// Modified: AP, Apr10 2006
// Modified: AP, Nov17 2006 (Added CRC) 
short Crob::config_ROB(Cconf *myconf) {
  short conferr, confstatus, confcrcfe,confcrcdaq,confcrctrig;

  if (this==NULL) return 0; // Object deleted -> return

// ROB error is BIT6 of conferr
  conferr = config_mc(myconf,CROB,-1,-1,&confstatus,&confcrcfe,&confcrcdaq,&confcrctrig);

  if (((conferr>>6)&0x1)!=0) {
    config_ROB_result.error = true;
    config_ROB_result.crc=0;
    return 1;
  }
  else {
    config_ROB_result.error = false;
    config_ROB_result.crc=confcrcdaq;
    return 0;
  }
}

// Print to stdout the result of ROB configuration
// Author: A.Parenti, Sep02 2005
// Modified: AP, Nov17 2006
void Crob::config_ROB_print() {
  if (this==NULL) return; // Object deleted -> return

  if (config_ROB_result.sent>0) {
    printf("Configure ROB: %d lines sent (%d successful)\n",config_ROB_result.sent,config_ROB_result.sent-config_ROB_result.bad);
    printf("               CRC=%#hX\n",config_ROB_result.crc);
    printf("               Error=%d\n",config_ROB_result.error);
  }
}


/*********************/
/* Check ROB status  */
/*********************/

// Check ROB status.
// Author: A. Parenti, May11 2007
// Modified: AP, Oct10 2007
void Crob::status_ROB(Cref *Pcref) {
  if (this==NULL) return; // Object deleted -> return

  char brdmsk[7]; // Mask of existing ROBs
  int i;

  status_ROB_reset(); // Reset status_rob struct

  get_rob_mask(brdmsk); // Read brdmsk

  MC_temp(); // Check ROB temp's
  read_ROB_pwr(); // Check ROB pwr
  read_ROB_error(); // Read ROB error

// Read Error
  if (mc_temp.ccbserver_code!=0 || read_rob_pwr.ccbserver_code!=0 || read_rob_error.ccbserver_code!=0) {
    status_rob.ccbserver_error = true;
    status_rob.errmsg+="CCBSERVER error\n";
    return;
  }

// Fill-up message
  char tmpstr[100];

  status_rob.stmsg="\nROB Status\n";

  status_rob.xmlmsg+="<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
  status_rob.xmlmsg+="<rob_status>\n";

  if (HVer>1 || (HVer==1 && LVer>=10)){ // Firmware >= v1.10
    for (i=0;i<7;++i) {
    if (brdmsk[i]!=0) { // ROB exists
      sprintf(tmpstr,"ROB[%d] state: %#X\n",i,read_rob_error.state[i]); status_rob.stmsg+=tmpstr;
      if (read_rob_error.state[i]!=0xFF) { // ROB error
        sprintf(tmpstr,"  ROB[%d]: state error\n",i); status_rob.errmsg+=tmpstr;
        sprintf(tmpstr,"  <ErrMsg>ROB[%d] state=%#X</ErrMsg>\n",i,read_rob_error.state[i]); status_rob.xmlmsg+=tmpstr;
        if (Pcref->ref_status.errmask[81]) status_rob.error=true;
      }
		}
    }
  } else { // Firmware < v1.10
    sprintf(tmpstr,"ROB status: %#X\n",read_rob_error.status); status_rob.stmsg+=tmpstr;
    if (read_rob_error.status!=0xFF) { // ROB error
      sprintf(tmpstr,"  ROB status error\n"); status_rob.errmsg+=tmpstr;
      sprintf(tmpstr,"  <ErrMsg>ROB status=%#X</ErrMsg>\n",read_rob_error.status); status_rob.xmlmsg+=tmpstr;
      status_rob.error=true;
    }
  }

  for (i=0;i<7;++i) {
    if (brdmsk[i]!=0) { // ROB exists
      sprintf(tmpstr,"ROB[%d] temp: %.1f C\n",i,mc_temp.temp[i]); status_rob.stmsg+=tmpstr;
      if (mc_temp.temp[i]<Pcref->ref_rob.RobTempMin[i] || mc_temp.temp[i]>Pcref->ref_rob.RobTempMax[i]) { // temperature error
        sprintf(tmpstr,"  ROB[%d]: temp error\n",i); status_rob.errmsg+=tmpstr;
        sprintf(tmpstr,"  <ErrMsg>ROB%d temp=%.1f</ErrMsg>\n",i,mc_temp.temp[i]); status_rob.xmlmsg+=tmpstr;
        if (Pcref->ref_status.errmask[101]) status_rob.error=true;
      }

      sprintf(tmpstr,"ROB[%d] Vcc: %.2f V\n",i,read_rob_pwr.Vcc[i]); status_rob.stmsg+=tmpstr;
      if (read_rob_pwr.Vcc[i]<Pcref->ref_rob.RobVccMin[i] || read_rob_pwr.Vcc[i]>Pcref->ref_rob.RobVccMax[i]) { // Vcc error
        sprintf(tmpstr,"  ROB[%d]: Vcc error\n",i); status_rob.errmsg+=tmpstr;
        sprintf(tmpstr,"  <ErrMsg>ROB%d Vcc=%.2f</ErrMsg>\n",i,read_rob_pwr.Vcc[i]); status_rob.xmlmsg+=tmpstr;
        if (Pcref->ref_status.errmask[121]) status_rob.error=true;
      }

      sprintf(tmpstr,"ROB[%d] Vdd: %.2f V\n",i,read_rob_pwr.Vdd[i]); status_rob.stmsg+=tmpstr;
      if (read_rob_pwr.Vdd[i]<Pcref->ref_rob.RobVddMin[i] || read_rob_pwr.Vdd[i]>Pcref->ref_rob.RobVddMax[i]) { // Vdd error
        sprintf(tmpstr,"  ROB[%d]: Vdd error\n",i); status_rob.stmsg+=tmpstr;
        sprintf(tmpstr,"  <ErrMsg>ROB%d Vdd=%.2f</ErrMsg>\n",i,read_rob_pwr.Vdd[i]); status_rob.xmlmsg+=tmpstr;
        if (Pcref->ref_status.errmask[82]) status_rob.error=true;
      }

        sprintf(tmpstr,"ROB[%d] I: %.2f A\n",i,read_rob_pwr.current[i]); status_rob.stmsg+=tmpstr;
      if (read_rob_pwr.current[i]<Pcref->ref_rob.RobIMin[i] || read_rob_pwr.current[i]>Pcref->ref_rob.RobIMax[i]) { // current error
        sprintf(tmpstr,"  ROB[%d]: I error\n",i); status_rob.errmsg+=tmpstr;
        sprintf(tmpstr,"  <ErrMsg>ROB%d I=%.2f</ErrMsg>\n",i,read_rob_pwr.current[i]); status_rob.xmlmsg+=tmpstr;
        if (Pcref->ref_status.errmask[141]) status_rob.error=true;
      }
    }
  }

  if (status_rob.error) {
    status_rob.stmsg +="Status: error\n";
    status_rob.xmlmsg+="  <status>error</status>\n";
  } else {
    status_rob.stmsg +="Status: ok\n";
    status_rob.xmlmsg+="  <status>ok</status>\n";
  }

  status_rob.xmlmsg+="</rob_status>\n";

  return;
}

// Reset status_rob struct
// Author: A.Parenti, May11 2007
// Modified: AP, Oct08 2007
void Crob::status_ROB_reset() {
  if (this==NULL) return; // Object deleted -> return

  status_rob.error = status_rob.warning = false;
  status_rob.ccbserver_error=false;
  status_rob.stmsg="";
  status_rob.errmsg="";
  status_rob.xmlmsg="";
}


// Print result of "ROB Status" to stdout
// Author: A. Parenti, May10 2007
// Modified: AP, Oct08 2007
void Crob::status_ROB_print() {
  if (this==NULL) return; // Object deleted -> return

// Set font color
  if (status_rob.error) std::cout << REDBKG; // Error
  else if (status_rob.warning) std::cout << YLWBKG; // Warning

  printf("%s",status_rob.stmsg.c_str());
  printf("Messages:\n%s",status_rob.errmsg.c_str());

// Unset font color
  if (status_rob.error || status_rob.warning)
    std::cout << STDBKG << std::endl; // Unset font color
}


/*********************************/
/* "Read ROB pwr" (0x5B) command */
/*********************************/

// Send "Read ROB pwr" (0x5B) command and decode reply
// Author: A. Parenti, Dec21 2004
// Note: Tested at LNL, Mar09 2005
// Modified: AP, Mar24 2005
void Crob::read_ROB_pwr() {
  const unsigned char command_code=0x5B;
  unsigned char command_line[1];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;

  read_ROB_pwr_reset(); // Reset read_rob_pwr struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1,READ_ROB_PWR_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = read_ROB_pwr_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    read_rob_pwr.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nRead ROB pwr (0x5B): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset read_rob_pwr struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec07 2006
void Crob::read_ROB_pwr_reset() {
  int i;

  if (this==NULL) return; // Object deleted -> return

  read_rob_pwr.code=0;
  for (i=0;i<7;++i)
    read_rob_pwr.Vcc[i]=read_rob_pwr.Vdd[i]=read_rob_pwr.current[i]=0;

  read_rob_pwr.ccbserver_code=-5;
  read_rob_pwr.msg="";
}


// Decode "Read ROB pwr" (0x5B) reply
// Author: A. Parenti, Feb04 2005
// Modified: AP, Dec07 2006
int Crob::read_ROB_pwr_decode(unsigned char *rdstr) {
  const unsigned char right_reply=0x5C;
  unsigned char code2;
  int i, idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&read_rob_pwr.code);
  code2 = rdstr[idx];

// Fill-up the "result" structure
  for (i=0;i<7;++i)
    idx = rstring(rdstr,idx,read_rob_pwr.Vcc+i);
  for (i=0;i<7;++i)
    idx = rstring(rdstr,idx,read_rob_pwr.Vdd+i);
  for (i=0;i<7;++i)
    idx = rstring(rdstr,idx,read_rob_pwr.current+i);

// Fill-up the error code
  if (read_rob_pwr.code==CCB_BUSY_CODE) // ccb is busy
    read_rob_pwr.ccbserver_code = -1;
  else if (read_rob_pwr.code==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    read_rob_pwr.ccbserver_code = 1;
  else if (read_rob_pwr.code==right_reply) // ccb: right reply
    read_rob_pwr.ccbserver_code = 0;
  else // wrong ccb reply
    read_rob_pwr.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  read_rob_pwr.msg="\nRead ROB pwr (0x5B)\n";

  if (read_rob_pwr.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(read_rob_pwr.ccbserver_code)); read_rob_pwr.msg+=tmpstr;
  } else {

    sprintf(tmpstr,"code: %#2X\n", read_rob_pwr.code); read_rob_pwr.msg+=tmpstr;
    for (i=0;i<7;++i) {
      sprintf(tmpstr,"Vcc[%i]: %.2f (V)\n",i,read_rob_pwr.Vcc[i]); read_rob_pwr.msg+=tmpstr;
    }
    for (i=0;i<7;++i) {
      sprintf(tmpstr,"Vdd[%i]: %.2f (V)\n",i,read_rob_pwr.Vdd[i]); read_rob_pwr.msg+=tmpstr;
    }
    for (i=0;i<7;++i) {
      sprintf(tmpstr,"current[%i]: %.3f (A)\n",i,read_rob_pwr.current[i]); read_rob_pwr.msg+=tmpstr;
    }
  }

  return idx;
}


// Print result of "Read ROB pwr" (0x5B) to stdout
// Author: A. Parenti, Dec21 2004
// Modified: AP, Dec07 2006
void Crob::read_ROB_pwr_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",read_rob_pwr.msg.c_str());
}


/******************************/
/* "Reset ROB" (0x32) command */
/******************************/

// Send "Reset ROB" (0x32) command and decode reply
// Author: A. Parenti, Dec21 2004
// Note: Tested at LNL, Mar09 2005
// Modified: Mar24 2005
void Crob::reset_ROB() {
  const unsigned char command_code=0x32;
  unsigned char command_line[1];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;

  reset_ROB_reset(); // Reset reset_rob struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1,RESET_ROB_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = reset_ROB_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    reset_rob.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nReset ROB (0x32): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset reset_rob struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec07 2006
void Crob::reset_ROB_reset() {
  if (this==NULL) return; // Object deleted -> return

  reset_rob.code1=reset_rob.code2=0;

  reset_rob.ccbserver_code=-5;
  reset_rob.msg="";
}


// Decode "Reset ROB" (0x32) reply
// Author: A. Parenti, Feb04 2005
// Modified: AP, Dec07 2006
int Crob::reset_ROB_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x32, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&reset_rob.code1);
  idx = rstring(rdstr,idx,&reset_rob.code2);

// Fill-up the error code
  if (reset_rob.code1==CCB_BUSY_CODE) // ccb is busy
    reset_rob.ccbserver_code = -1;
  else if (reset_rob.code1==CCB_UNKNOWN1 && reset_rob.code2==CCB_UNKNOWN2) // ccb: unknown command
    reset_rob.ccbserver_code = 1;
  else if (reset_rob.code1==right_reply && reset_rob.code2==command_code) // ccb: right reply
    reset_rob.ccbserver_code = 0;
  else // wrong ccb reply
    reset_rob.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  reset_rob.msg="\nReset ROB (0x32)\n";

  if (reset_rob.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(reset_rob.ccbserver_code)); reset_rob.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",reset_rob.code1); reset_rob.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",reset_rob.code2); reset_rob.msg+=tmpstr;
  }

  return idx;
}


// Print result of "Reset ROB" (0x32) to stdout
// Author: A. Parenti, Dec21 2004
// Modified: AP, Dec07 2006
void Crob::reset_ROB_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",reset_rob.msg.c_str());
}


/**********************************/
/* "Run BIST test" (0x41) command */
/**********************************/

// Send "Run BIST test" (0x41) command and decode reply
// Author: A. Parenti, Dec22 2004
// Modified: A.Parenti, Mar24 2005
// Note: Tested at Cessy, Oct05 2006
void Crob::run_BIST_test(char this_brd) {
  const unsigned char command_code=0x41;
  unsigned char command_line[2];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
    command_line[0]=command_code;
    command_line[1]=this_brd;

  run_BIST_test_reset(); // Reset run_bist_test struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,2,RUN_BIST_TEST_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = run_BIST_test_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    run_bist_test.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nRun BIST test (0x41): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset run_bist_test struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec07 2006
void Crob::run_BIST_test_reset() {
  int i,j;

  if (this==NULL) return; // Object deleted -> return

  run_bist_test.code1=0;
  for (i=0;i<4;++i)
    for (j=0;j<2;++j)
      run_bist_test.bist[i][j]=0;

  run_bist_test.code2=run_bist_test.narg=0;
  run_bist_test.ccbserver_code=-5;
  run_bist_test.msg="";
}


// Decode "run BIST test" (0x41) reply
// Author: A. Parenti, Feb04 2005
// Modified: AP, Dec07 2006
int Crob::run_BIST_test_decode(unsigned char *rdstr) {
  const unsigned char right_reply=0x42, command_code=0x41;
  unsigned char code2, tmpbyte;
  int i, j, idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&run_bist_test.code1);
  code2 = rdstr[idx];

// Fill-up the "result" structure
  if (run_bist_test.code1==right_reply) {
    for (i=0;i<4;++i)
      for (j=0;j<2;++j) {
        idx = rstring(rdstr,idx,&tmpbyte);
        run_bist_test.bist[i][j] = tmpbyte;
      }
  } else if (run_bist_test.code1==CCB_OXFC && code2==command_code) {
    idx = rstring(rdstr,idx,&run_bist_test.code2);
    idx = rstring(rdstr,idx,&run_bist_test.narg);
  }

// Fill-up the error code
  if (run_bist_test.code1==CCB_BUSY_CODE) // ccb is busy
    run_bist_test.ccbserver_code = -1;
  else if (run_bist_test.code1==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    run_bist_test.ccbserver_code = 1;
  else if (run_bist_test.code1==right_reply) // ccb: right reply
    run_bist_test.ccbserver_code = 0;
  else if (run_bist_test.code1==CCB_OXFC && code2==command_code) // ccb: right reply
    run_bist_test.ccbserver_code = 0;
  else // wrong ccb reply
    run_bist_test.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  run_bist_test.msg="\nRun BIST test (0x41)\n";

  if (run_bist_test.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(run_bist_test.ccbserver_code)); run_bist_test.msg+=tmpstr;
  } else {

    if (run_bist_test.code1==right_reply) {
      sprintf(tmpstr,"code: %#2X\n", run_bist_test.code1); run_bist_test.msg+=tmpstr;
      for (i=0;i<4;++i)
        for (j=0;j<2;++j) {
          sprintf(tmpstr,"bist[%d][%d]: %#2X\n",i,j,run_bist_test.bist[i][j]); run_bist_test.msg+=tmpstr;
        }
    } else {
      sprintf(tmpstr,"code1: %#2X\n",run_bist_test.code1); run_bist_test.msg+=tmpstr;
      sprintf(tmpstr,"code2: %#2X\n",run_bist_test.code2); run_bist_test.msg+=tmpstr;
      sprintf(tmpstr,"Wrong argument. narg: %d\n",run_bist_test.narg); run_bist_test.msg+=tmpstr;
    }
  }

  return idx;
}


// Print result of "Run BIST test" (0x41) to stdout
// Author: A. Parenti, Dec22 2004
// Modified: AP, Dec07 2006
void Crob::run_BIST_test_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",run_bist_test.msg.c_str());
}


/*********************************/
/* "Read out ROB" (0x6C) command */
/*********************************/

// Send "Read out ROB" (0x6C) command and decode reply
// Author: A. Parenti, Dec22 2004
// Modified: AP, Mar24 2005
// Note: Tested at Cessy, Oct05 2006
void Crob::read_out_ROB(char this_brd) {
  const unsigned char command_code=0x6C;
  unsigned char command_line[2];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]= command_code;
  command_line[1]= this_brd;

  read_out_ROB_reset(); // Reset read_out_rob struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,2,READ_OUT_ROB_TIMEOUT);

  if (send_command_result>=0) { // alright
    read_out_rob.size=(char)((cmd_obj->data_read_len-2)/sizeof(int));
    idx = read_out_ROB_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    read_out_rob.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nRead out ROB (0x6C): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset read_out_rob struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec07 2006
void Crob::read_out_ROB_reset() {
  int i;

  if (this==NULL) return; // Object deleted -> return

  read_out_rob.code1=read_out_rob.more_data=0;
  for (i=0;i<240;++i)
    read_out_rob.readout[i]=0;
  read_out_rob.size=0;

  read_out_rob.code2=read_out_rob.error=0;
  read_out_rob.ccbserver_code=-5;
  read_out_rob.msg="";
}


// Decode "Read out ROB" (0x6C) reply
// Author: A. Parenti, Feb04 2005
// Modified: AP, Dec07 2006
int Crob::read_out_ROB_decode(unsigned char *rdstr) {
  const unsigned char right_reply=0x6D, command_code=0x6C;
  unsigned char code2;
  int i, idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&read_out_rob.code1);
  code2 = rdstr[idx];

// Fill-up the "result" structure
  if (read_out_rob.code1==right_reply) {
    idx = rstring(rdstr,idx,&read_out_rob.more_data);

    for (i=0;i<read_out_rob.size;++i)
      idx = rstring(rdstr,idx,read_out_rob.readout+i);
  } else if (read_out_rob.code1==CCB_OXFC && code2==command_code) {
    idx = rstring(rdstr,idx,&read_out_rob.code2);
    idx = rstring(rdstr,idx,&read_out_rob.error);
  }

// Fill-up the error code
  if (read_out_rob.code1==CCB_BUSY_CODE) // ccb is busy
    read_out_rob.ccbserver_code = -1;
  else if (read_out_rob.code1==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    read_out_rob.ccbserver_code = 1;
  else if (read_out_rob.code1==right_reply) // ccb: right reply
    read_out_rob.ccbserver_code = 0;
  else if (read_out_rob.code1==CCB_OXFC && code2==command_code) // ccb: right reply
    read_out_rob.ccbserver_code = 0;
  else // wrong ccb reply
    read_out_rob.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  read_out_rob.msg="\nRead out ROB (0x6C)\n";

  if (read_out_rob.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(read_out_rob.ccbserver_code)); read_out_rob.msg+=tmpstr;
  } else {

    if (read_out_rob.code1==right_reply) {
      sprintf(tmpstr,"code: %#2X\n",read_out_rob.code1); read_out_rob.msg+=tmpstr;
      sprintf(tmpstr,"more_data: %#2X\n",read_out_rob.more_data); read_out_rob.msg+=tmpstr;
      for (i=0;i<read_out_rob.size;++i) {
        sprintf(tmpstr,"readout[%d]: %#2X\n",i,read_out_rob.readout[i]); read_out_rob.msg+=tmpstr;
      }
    } else {
      sprintf(tmpstr,"code1: %#2X\n",read_out_rob.code1); read_out_rob.msg+=tmpstr;
      sprintf(tmpstr,"code2: %#2X\n",read_out_rob.code2); read_out_rob.msg+=tmpstr;
      sprintf(tmpstr,"Wrong argument. error: %d\n",read_out_rob.error); read_out_rob.msg+=tmpstr;
    }
  }


  return idx;
}


// Print result of "Read out ROB" (0x6C) to stdout
// Author: A. Parenti, Dec22 2004
// Modified: AP, Dec07 2006
void Crob::read_out_ROB_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",read_out_rob.msg.c_str());
}


/*************************************/
/* "Set cpu-ck delay" (0x26) command */
/*************************************/

// Send "Set cpu-ck delay" (0x26) command and decode reply
// Input: delay (unit 150ps)
// Author: A. Parenti, Jan06 2005
// Note: Tested at LNL, Mar09 2005
// Modified: AP, Mar24 2005

void Crob::set_CPU_ck_delay(char delay) {
  const unsigned char command_code=0x26;
  unsigned char command_line[2];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]= command_code;
  command_line[1]= delay;

  set_CPU_ck_delay_reset(); // Reset set_cpu_ck_delay struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,2,SET_CPU_CK_DELAY_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = set_CPU_ck_delay_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    set_cpu_ck_delay.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nSet CPU-Ck delay (0x26): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset set_cpu_ck_delay struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec07 2006
void Crob::set_CPU_ck_delay_reset() {
  if (this==NULL) return; // Object deleted -> return

  set_cpu_ck_delay.code1=set_cpu_ck_delay.code2=0;

  set_cpu_ck_delay.ccbserver_code=-5;
  set_cpu_ck_delay.msg="";
}


// Decode "Set cpu-ck delay" (0x26) reply
// Author: A. Parenti, Feb07 2005
// Modified: AP, Dec07 2006
int Crob::set_CPU_ck_delay_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x26, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&set_cpu_ck_delay.code1);
  idx = rstring(rdstr,idx,&set_cpu_ck_delay.code2);

// Fill-up the error code
  if (set_cpu_ck_delay.code1==CCB_BUSY_CODE) // ccb is busy
    set_cpu_ck_delay.ccbserver_code = -1;
  else if (set_cpu_ck_delay.code1==CCB_UNKNOWN1 && set_cpu_ck_delay.code2==CCB_UNKNOWN2) // ccb: unknown command
    set_cpu_ck_delay.ccbserver_code = 1;
  else if (set_cpu_ck_delay.code1==right_reply && set_cpu_ck_delay.code2==command_code) // ccb: right reply
    set_cpu_ck_delay.ccbserver_code = 0;
  else // wrong ccb reply
    set_cpu_ck_delay.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  set_cpu_ck_delay.msg="\nSet CPU-Ck delay (0x26)\n";

  if (set_cpu_ck_delay.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(set_cpu_ck_delay.ccbserver_code)); set_cpu_ck_delay.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",set_cpu_ck_delay.code1); set_cpu_ck_delay.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",set_cpu_ck_delay.code2); set_cpu_ck_delay.msg+=tmpstr;
  }

  return idx;
}


// Print result of "Set cpu-ck delay" (0x26) to stdout
// Author: A. Parenti, Jan06 2005
// Modified: AP, Dec07 2006
void Crob::set_CPU_ck_delay_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",set_cpu_ck_delay.msg.c_str());
}


/*********************************************/
/* "Disable temperature test" (0x83) command */
/*********************************************/

// Send "Disable temperature test" (0x83) command and decode reply
// Author: A. Parenti, Jan15 2005
// Note: Tested at LNL, Mar09 2005
// Modified: AP, Mar24 2005
void Crob::disable_temp_test(char disable) {
  const unsigned char command_code=0x83;
  unsigned char command_line[2];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=disable;

  disable_temp_test_reset(); // Reset rob_temp_test struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,2,ROB_DISABLE_TEMP_TEST_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = disable_temp_test_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    rob_temp_test.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nDisable temperature test (0x83): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset rob_temp_test struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec07 2006
void Crob::disable_temp_test_reset() {
  if (this==NULL) return; // Object deleted -> return

  rob_temp_test.code1=rob_temp_test.code2=0;

  rob_temp_test.ccbserver_code=-5;
  rob_temp_test.msg="";
}


// Decode "Disable temperature test" (0x83) reply
// Author: A. Parenti, Feb03 2005
// Modified: AP, Dec07 2006
int Crob::disable_temp_test_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x83, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&rob_temp_test.code1);
  idx = rstring(rdstr,idx,&rob_temp_test.code2);

// Fill-up the error code
  if (rob_temp_test.code1==CCB_BUSY_CODE) // ccb is busy
    rob_temp_test.ccbserver_code = -1;
  else if (rob_temp_test.code1==CCB_UNKNOWN1 && rob_temp_test.code2==CCB_UNKNOWN2) // ccb: unknown command
    rob_temp_test.ccbserver_code = 1;
  else if (rob_temp_test.code1==right_reply && rob_temp_test.code2==command_code) // ccb: right reply
    rob_temp_test.ccbserver_code = 0;
  else // wrong ccb reply
    rob_temp_test.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  rob_temp_test.msg="\nDisable temperature test (0x83)\n";

  if (rob_temp_test.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(rob_temp_test.ccbserver_code)); rob_temp_test.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",rob_temp_test.code1); rob_temp_test.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",rob_temp_test.code2); rob_temp_test.msg+=tmpstr;
  }

  return idx;
}


// Print result of "Disable temperature test" (0x83) to stdout
// Author: A. Parenti, Jan15 2005
// Modified: AP, Dec07 2006
void Crob::disable_temp_test_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",rob_temp_test.msg.c_str());
}
