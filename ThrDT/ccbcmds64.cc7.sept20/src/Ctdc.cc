#include <ccbcmds/Ctdc.h>
#include <ccbcmds/apfunctions.h>

#include <iostream>
#include <cstdio>

/*****************************************************************************/
// TDC class
/*****************************************************************************/

// Class constructor; arguments: pointer_to_command_object
// Author: A.Parenti, Dec01 2006
Ctdc::Ctdc(Ccommand* ppp) : Ccmn_cmd(ppp) {
  write_TDC_reset(); // Reset write_tdc struct
  write_TDC_control_reset(); // Reset write_tdc_control struct
}

/*****************/
/* Configure TDC */
/*****************/

// Configure a TDC, reading from a "myconf" object
// Author: A.Parenti, Sep10 2005
// Modified: AP, Apr10 2006
// Modified: AP, Nov17 2006 (Added CRC) 
short Ctdc::config_TDC(Cconf *myconf, char tdc_brd, char tdc_chip) {
  short conferr, confstatus, confcrcfe,confcrcdaq,confcrctrig;
  if (this==NULL) return 0; // Object deleted -> return

// TDC error is BIT5 of conferr
  conferr = config_mc(myconf,CTDC,tdc_brd,tdc_chip,&confstatus,&confcrcfe,&confcrcdaq,&confcrctrig);

  if (((conferr>>5)&0x1)!=0) {
    config_TDC_result.error = true;
    config_TDC_result.crc=0;
    return 1;
  }
  else {
    config_TDC_result.error = false;
    config_TDC_result.crc=confcrcdaq;
    return 0;
  }
}


// Print to stdout the result of TDC configuration
// Author: A.Parenti, Sep10 2005
// Modified: AP, Nov17 2006
void Ctdc::config_TDC_print() {
  if (this==NULL) return; // Object deleted -> return

  if (config_TDC_result.sent>0) {
    printf("Configure TDC: %d lines sent (%d successful)\n",config_TDC_result.sent,config_TDC_result.sent-config_TDC_result.bad);
    printf("               CRC=%#hX\n",config_TDC_result.crc);
    printf("               Error=%d\n",config_TDC_result.error);
  }
}


// Retrive TDC config from CCB, dump it to file
// Author: A.Parenti, Oct30 2006
void Ctdc::read_TDC_config(char *filename) {
  char brd,chip;
  unsigned char cmdline[89];
  char strcmd[268];
  int i;
  std::ofstream outfile(filename,std::ofstream::app);

  if (this==NULL) return; // Object deleted -> return

  printf("Read TDC config (0x44).\n");

  if (outfile.is_open()) {
// TDC, 0x44
    for (brd=0;brd<=6;++brd)
      for (chip=0;chip<=3;++chip) {
        printf("Brd:%d Chip:%d ...",brd,chip);

        read_TDC(brd,chip);

        if (read_tdc.code1==0x22) {
          cmdline[0]=0x44;
          cmdline[1]=brd;
          cmdline[2]=chip;
          mystrcpy(cmdline+3,read_tdc.setup,81);
          mystrcpy(cmdline+84,read_tdc.setup,5);

          strcpy(strcmd,"");
          sprintf(strcmd,"%02X",cmdline[0]);
          for (i=1;i<89;++i)
            sprintf(strcmd,"%s %02X",strcmd,cmdline[i]);
          outfile << strcmd << '\n';
          outfile.flush();
        }

        printf("done!\n");
      }

    outfile.close();
  } else
    printf("Impossible to open file '%s'.\n",filename);

}


/******************************/
/* "write TDC" (0x44) command */
/******************************/

// Send "write TDC" (0x44) command and decode reply
// Author: A. Parenti, Dec20 2004
// Modified: AP, Oct07 2006
void Ctdc::write_TDC(char tdc_brd, char tdc_chip, unsigned char setup[81], unsigned char control[5]) {
  bool ErTdcCnf;
  const unsigned char command_code=0x44;
  unsigned char command_line[89], default_delay, tmp_delay;
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// Read original CPU-Ck delay
  MC_read_status();
  default_delay = mc_status.CpuCkDelay;

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=tdc_brd;
  command_line[2]=tdc_chip;
  mystrcpy(command_line+3,setup,81);
  mystrcpy(command_line+84,control,5);

  write_TDC_reset(); // Reset write_tdc struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,89,WRITE_TDC_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = write_TDC_decode(cmd_obj->data_read); // decode reply
  }
  else // copy error code
    write_tdc.ccbserver_code = send_command_result; // Copy error code

  if (strncmp((char *)cmd_obj->data_read,"\x45",1)!=0) // wrong reply
    ErTdcCnf=true;
  else
    ErTdcCnf=false;

// unlock "cmd_mutex" for status_TDC
  cmd_obj->cmdunlock();

// Check TDC status
  status_TDC(tdc_brd,tdc_chip);

  if (status_tdc.error || ErTdcCnf) { // TDC error!
    unsigned char tmp_cmd[2];

// lock again "cmd_mutex"
    cmd_obj->cmdlock();

// Set a new delay
    if (default_delay<40 || default_delay>120) // if delay < 6ns or > 18ns
      tmp_delay = 80; // set delay = 12 ns 
    else if (default_delay<80) // if 6ns < delay < 12ns
      tmp_delay += 40; // set delay += 6 ns 
    else // if 12ns < delay < 18ns
      tmp_delay -= 40; // set delay -= 6 ns

    tmp_cmd[0] = 0x26;
    tmp_cmd[1] = tmp_delay;
    cmd_obj->send_command(tmp_cmd,2,TIMEOUT_DEFAULT); // set delay

    write_TDC_reset(); // Reset write_tdc struct
// Try again to configure TDC
    send_command_result = cmd_obj->send_command(command_line,89,WRITE_TDC_TIMEOUT);

    if (send_command_result>=0) { // alright
      idx = write_TDC_decode(cmd_obj->data_read); // decode reply
    }
    else // copy error code
      write_tdc.ccbserver_code = send_command_result;

    if (strncmp((char *)cmd_obj->data_read,"\x45",1)!=0) // wrong reply
      ErTdcCnf=true;
    else
      ErTdcCnf=false;

// unlock "cmd_mutex", for status_TDC
    cmd_obj->cmdunlock();

// Check again TDC status
    status_TDC(tdc_brd,tdc_chip);

// Set CPU-Ck delay to the original value
    tmp_cmd[0] = 0x26;
    tmp_cmd[1] = default_delay;
    cmd_obj->send_command(tmp_cmd,2,TIMEOUT_DEFAULT);
  }

  if (_verbose_)
    printf("\nWrite TDC (0x44): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

}


// Reset write_tdc struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec06 2006
void Ctdc::write_TDC_reset() {
  int i;

  if (this==NULL) return; // Object deleted -> return

  write_tdc.code1=0;
  for (i=0;i<8;++i)
    write_tdc.status[i]=0;

  write_tdc.code2=write_tdc.narg=0;
  write_tdc.ccbserver_code=-5;

  write_tdc.msg="";
}


// Decode "write TDC" (0x44) reply
// Author: A. Parenti, Feb03 2005
// Modified: AP, Dec06 2006
int Ctdc::write_TDC_decode(unsigned char *rdstr) {
  const unsigned char right_reply=0x45, command_code=0x44;
  unsigned char code2;
  int i, idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&write_tdc.code1);
  code2  = rdstr[idx];

// Fill-up the "result" structure
  if (write_tdc.code1==right_reply) {
    for (i=0;i<8;++i)
      idx = rstring(rdstr,idx,write_tdc.status+i);
  } else if (write_tdc.code1==CCB_OXFC && code2==command_code) {
    idx = rstring(rdstr,idx,&write_tdc.code2);
    idx = rstring(rdstr,idx,&write_tdc.narg);
  }

// Fill-up the error code
  if (write_tdc.code1==CCB_BUSY_CODE) // ccb is busy
    write_tdc.ccbserver_code = -1;
  else if (write_tdc.code1==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    write_tdc.ccbserver_code = 1;
  else if (write_tdc.code1==right_reply) // ccb: right reply
    write_tdc.ccbserver_code = 0;
  else if (write_tdc.code1==CCB_OXFC && code2==command_code) // ccb: right reply
    write_tdc.ccbserver_code = 0;
  else // wrong ccb reply
    write_tdc.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  write_tdc.msg="\nWrite TDC (0x44)\n";

  if (write_tdc.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(write_tdc.ccbserver_code)); write_tdc.msg+=tmpstr;
  } else {

    if (write_tdc.code1==right_reply) {
      sprintf(tmpstr,"code: %#2X\n", write_tdc.code1); write_tdc.msg+=tmpstr;
      for (i=0;i<8;++i) {
        sprintf(tmpstr,"status[%i]: %#2X\n",i,write_tdc.status[i]); write_tdc.msg+=tmpstr;
      }
    } else {
      sprintf(tmpstr,"code1: %#2X\n", write_tdc.code1); write_tdc.msg+=tmpstr;
      sprintf(tmpstr,"code2: %#2X\n", write_tdc.code2); write_tdc.msg+=tmpstr;
      sprintf(tmpstr,"Wrong argument. narg: %d\n", write_tdc.narg); write_tdc.msg+=tmpstr;
    }
  }

  return idx;
}


// Print results of "write TDC" (0x44) command to standard output
// Author:  A.Parenti Dec20 2004
// Modified: AP, Dec06 2006
void Ctdc::write_TDC_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",write_tdc.msg.c_str());
}



/**************************************/
/* "write TDC control" (0x18) command */
/**************************************/

// Send "write TDC control" (0x18) command and decode reply
// Author: A. Parenti, Dec20 2004
// Note: Tested at LNL, Mar09 2005
//  Modified: AP, Oct07 2006
void Ctdc::write_TDC_control(char tdc_brd, char tdc_chip, unsigned char control[5]) {
  bool ErTdcCnf;
  const unsigned char command_code=0x18;
  unsigned char command_line[8], default_delay, tmp_delay;
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// Read original CPU-Ck delay
  MC_read_status();
  default_delay = mc_status.CpuCkDelay;

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=tdc_brd;
  command_line[2]=tdc_chip;
  mystrcpy(command_line+3,control,5);

  write_TDC_control_reset(); // Reset write_tdc_control struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,8,WRITE_TDC_CONTROL_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = write_TDC_control_decode(cmd_obj->data_read); // decode reply
  }
  else // copy error code
    write_tdc_control.ccbserver_code = send_command_result; // Copy error code

  if (strncmp((char *)cmd_obj->data_read,"\xfc\x18\x1",1)!=0) // wrong reply
    ErTdcCnf=true;
  else
    ErTdcCnf=false;

// unlock "cmd_mutex" for status_TDC
  cmd_obj->cmdunlock();

// Check TDC status
  status_TDC(tdc_brd,tdc_chip);

  if (status_tdc.error || ErTdcCnf) { // TDC error!
    unsigned char tmp_cmd[2];

// lock again "cmd_mutex"
    cmd_obj->cmdlock();

// Set a new delay
    if (default_delay<40 || default_delay>120) // if delay < 6ns or > 18ns
      tmp_delay = 80; // set delay = 12 ns 
    else if (default_delay<80) // if 6ns < delay < 12ns
      tmp_delay += 40; // set delay += 6 ns 
    else // if 12ns < delay < 18ns
      tmp_delay -= 40; // set delay -= 6 ns

    tmp_cmd[0] = 0x26;
    tmp_cmd[1] = tmp_delay;
    cmd_obj->send_command(tmp_cmd,2,TIMEOUT_DEFAULT); // set delay

    write_TDC_control_reset(); // Reset write_tdc_control struct
// Try again to configure TDC
    send_command_result = cmd_obj->send_command(command_line,8,WRITE_TDC_CONTROL_TIMEOUT);

    if (send_command_result>=0) { // alright
      idx = write_TDC_control_decode(cmd_obj->data_read); // decode reply (before calling other commands)
    }
    else // copy error code
      write_tdc_control.ccbserver_code = send_command_result;// Copy error code

    if (strncmp((char *)cmd_obj->data_read,"\xfc\x18\x1",1)!=0) // wrong reply
      ErTdcCnf=true;
    else
      ErTdcCnf=false;

// unlock "cmd_mutex", for status_TDC
    cmd_obj->cmdunlock();

// Check again TDC status
    status_TDC(tdc_brd,tdc_chip);

// Set CPU-Ck delay to the original value
    tmp_cmd[0] = 0x26;
    tmp_cmd[1] = default_delay;
    cmd_obj->send_command(tmp_cmd,2,TIMEOUT_DEFAULT);
  }

  if (_verbose_)
    printf("\nWrite TDC control (0x18): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);
}


// Reset write_tdc_control struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec06 2006
void Ctdc::write_TDC_control_reset() {
  if (this==NULL) return; // Object deleted -> return

  write_tdc_control.code1=write_tdc_control.code2=write_tdc_control.result=0;

  write_tdc_control.ccbserver_code=-5;
  write_tdc_control.msg="";
}


// Decode "write TDC control" (0x18) reply
// Author: A. Parenti, Feb03 2005
// Modified: AP, Dec12 2006
int Ctdc::write_TDC_control_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x18, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&write_tdc_control.code1);
  idx = rstring(rdstr,idx,&write_tdc_control.code2);

// Fill-up the "result" structure
  idx = rstring(rdstr,idx,&write_tdc_control.result);

// Fill-up the error code
  if (write_tdc_control.code1==CCB_BUSY_CODE) // ccb is busy
    write_tdc_control.ccbserver_code = -1;
  else if (write_tdc_control.code1==CCB_UNKNOWN1 && write_tdc_control.code2==CCB_UNKNOWN2) // ccb: unknown command
    write_tdc_control.ccbserver_code = 1;
  else if (write_tdc_control.code1==right_reply && write_tdc_control.code2==command_code) // ccb: right reply
    write_tdc_control.ccbserver_code = 0;
  else // wrong ccb reply
    write_tdc_control.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  write_tdc_control.msg="\nWrite TDC control (0x18)\n";

  if (write_tdc_control.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(write_tdc_control.ccbserver_code)); write_tdc_control.msg+=tmpstr;
  } else {

    sprintf(tmpstr,"code1: %#2X\n", write_tdc_control.code1); write_tdc_control.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n", write_tdc_control.code2); write_tdc_control.msg+=tmpstr;

    switch (write_tdc_control.result) {
    case 1:
      write_tdc_control.msg+="Result: write successful\n";
      break;
    case 0:
      write_tdc_control.msg+="Result: verify error\n";
      break;
    case -1:
      write_tdc_control.msg+="Result: argument error\n";
      break;
    }
  }

  return idx;
}


// Print results of "write TDC control" (0x18) command to standard output
// Author:  A.Parenti Dec20 2004
// Modified: AP, Dec06 2006
void Ctdc::write_TDC_control_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",write_tdc_control.msg.c_str());
}
