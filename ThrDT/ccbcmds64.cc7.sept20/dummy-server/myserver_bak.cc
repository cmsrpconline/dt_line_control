#include <stdlib.h>
#include <unistd.h>
#include "Ctcpux.h"

#include <ptypes/pasync.h>
#include <pthread.h> // multithreading

#include <iostream>
using namespace std;

#define max_port 10
#define DTIME 100 // Dead time in Server Reply (msec)
#define DEFPORT 18889

int main(int argc, char** argv)
// "Dummy server", A.Parenti, 06Dec2004
{
  int i, port;
  void open_port(int);

  printf("Use: \"myserver [port]\" (default port:%d )\n",DEFPORT);

  if (argc>1)
    port=atoi(argv[1]);
  else
    port = DEFPORT;

  open_port(port);

  return 0;
}


void open_port(int port)
{
  short ccbid;
  int i, n=0, size;
  const int h_len=12;

// Incoming port
  Ctcp myp(port);
  std::cout << "Listening on port: " << port << std::endl;

  while(1)
    try {
      Ctcp clp=myp.Accept(); // Open the socket

      unsigned char header[h_len];
      unsigned char received[243],reply[255];
      int r_len; // reply lenght

      while (1)
      {
        clp.Receive((char *)header,h_len); // receive header
        ccbid = (header[2]<<8)+header[3];
        size  = (header[4]<<24)+(header[5]<<16)+(header[6]<<8)+header[7];

        clp.Receive((char *)received,size-h_len); // receive payload

        cout << "[" << ++n << "] Server, received " << size << " char:";
        pt::psleep(DTIME); // Add some dead time

        for (i=0;i<h_len;++i)
          printf(" %2X",header[i]);
        for (i=0;i+h_len<size;++i)
          printf(" %2X",received[i]);
        cout << endl;

// Header for the reply
        for (i=0;i<h_len;++i)
          reply[i]=header[i];

// Simulates corrupted header
//        reply[0]=0xBC;

// Simulates CCB error/timeout
//        for (i=1;i<=4;++i)
//          reply[h_len-i]=0xFF;

// payload
        if (received[0]==0xea) {
          unsigned char stdstatus[]="\x13\x00\x00\x00\x01\x00\x10\x00\x01\xFF\xEF\xCF\x3F\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x07\xAE\x50\x00\x00\x00\x00\x02\x0C\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x5A\xBC\x00\x03\x7B\xB8\x00\x02\x4F\x8E\x00\x03\x69\x9A\x00\x02\x78\x52\x00\x01\x63\xA8\x00\x02\x78\x52\x00\x01\x63\x84\x00\x02\x50\x95\x00\x03\x50\xCE\x00\x03\x51\x18\x00\x03\x4E\x00\x00\x02\x4D\xDC\x00\x02\x4D\xDC\x00\x02\x53\x60\x00\x03\x53\x84\x00\x02\x5F\xFA\x00\x01\x5F\xFA\x00\x01\x5F\xFA\x00\x01\x66\xE6\xFF\xFB\x66\xE6\xFF\xFB\x66\xE6\xFF\xFB\x46\x80\x00\x02\x01\x6F\x00\xE7\x01\x69\x00\xE1\x01\x6F\x00\xE7\x41\x00\x00\x06\x00\x04\x00\x00\x5A\xBC\x00\x03\x5B\x06\x00\x03\x7B\x70\x00\x02\x7B\xB8\x00\x02\x53\x3C\x00\x03\x53\x84\x00\x03\x53\x60\x00\x02\x53\xA8\x00\x02";
//          r_len=2;
//          reply[h_len]=2;
//          reply[h_len+1]=0xBC;

          for (i=0;i<197;++i)
            reply[h_len+i]=stdstatus[i];

          r_len=197;
          reply[h_len+1]=header[2]; // ccbid
          reply[h_len+2]=header[3]; // ccbid

        }
        else if (received[0]==0x10 || received[0]==0x12) { // MC test
          r_len=1;
          reply[h_len]=0x11;
        }
        else if (received[0]==0x33) { // Read ROB error
          r_len=8;
          reply[h_len]=0x34; // ID code
          for (i=0;i<7;++i)
          reply[h_len+1+i]=0xFF; // ROB ok 
        }
        else if (received[0]==0x43) { // Read TDC
          r_len=99;
          reply[h_len]=0x22; // reply code
          for (i=0;i<98;++i)
            reply[h_len+1+i]=0;
          reply[h_len+98]=0x84; // TDC ID Code
          reply[h_len+97]=0x70; // TDC ID Code
          reply[h_len+96]=0xDA; // TDC ID Code
          reply[h_len+95]=0xCE; // TDC ID Code
// Bytes 87-94 are the status
//          reply[h_len+87]=255;
//          reply[h_len+88]=7;
          reply[h_len+89]=255;
//          reply[h_len+90]=255;
//          reply[h_len+91]=255;
//          reply[h_len+92]=255;
//          reply[h_len+93]=255;
          reply[h_len+94]=255;
        }
        else if (received[0]==0x27){ // WrTTCrx
          r_len=3;
          reply[h_len]=0xfc;
          reply[h_len+1]=0x27;
          reply[h_len+2]=0;
        }
        else if (received[0]==0x28){ // RdTTCrx
          r_len=3;
          reply[h_len]=0x29;
          reply[h_len+1]=0;
          reply[h_len+2]=0;
        }
        else if (received[0]==0xe7){ // Auto LINK set (boot)
          r_len=3;
          reply[h_len]=0xFC;
          reply[h_len+1]=0xE7;
          reply[h_len+2]=1;
        }
        else if (received[0]==0xe4){ // LINK test (boot)
          r_len=0;
        }
        else if (received[0]==0xe6){ // Reset SEU (boot)
          r_len=2;
          reply[h_len]=0xFC;
          reply[h_len+1]=0xE6;
        }
        else if (received[0]==0xe5){ // Link DAC (boot)
          r_len=2;
          reply[h_len]=0xFC;
          reply[h_len+1]=0xE5;
        }
        else if (received[0]==0xe3){ // TTC write (boot)
          r_len=3;
          reply[h_len]=0xFC;
          reply[h_len+1]=0xE3;
          reply[h_len+2]=0;
        }
        else if (received[0]==0xe2){ // TTC read (boot)
          r_len=3;
          reply[h_len]=0xE1;
          reply[h_len+1]=0xFF;
          reply[h_len+2]=0;
        }
        else if (received[0]==0xe0){ // TTC find (boot)
          r_len=3;
          reply[h_len]=0xFC;
          reply[h_len+1]=0xE0;
          reply[h_len+2]=0;
        }
        else if (received[0]==0xF7){ // HD Watchdog restart
          r_len=2;
          reply[h_len]=0xFC;
          reply[h_len+1]=0xF7;
        }
        else if (received[0]==0xA3){ // GetConfigCRC
          r_len=3;
          reply[h_len]=0xA4;
          reply[h_len+1]=reply[h_len+2]=0xFF;
        }
        else {
          r_len=1;
          reply[h_len]=0x3F; // "ccb busy" reply
//          reply[h_len]=0x13; // "mc status" reply
//          reply[h_len]=0x99; // "Wrong" reply
//          r_len=2;
//          reply[h_len]=0xFC; // ccb unknown command
//          reply[h_len+1]=0x00; // ccb unknown command
        }

// reply lenght
        reply[4]=reply[5]=reply[6]=0;
        reply[7]=h_len+r_len;

        cout << "Server, sent back " << clp.Send((char *)reply,h_len+r_len) << " char:";
        for (i=0;i<r_len+h_len;++i)
          printf(" %2X",reply[i]);
        std::cout << std::endl << std::endl;
      }
    }
    catch (tcpExcp error) {}
}
