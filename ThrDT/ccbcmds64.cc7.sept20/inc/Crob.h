// include Crob.h only once
#ifndef __CROB_H__
#define __CROB_H__

// defines classes Ccmn_cmd
#include <ccbcmds/Ccmn_cmd.h>

// reference values class
#include <ccbcmds/Cref.h>

// configurations class
#include <ccbcmds/Cconf.h>

// default timeouts
#define READ_ROB_PWR_TIMEOUT      5000
#define RUN_BIST_TEST_TIMEOUT    15000
#define READ_OUT_ROB_TIMEOUT      5000
#define ROB_DISABLE_TEMP_TEST_TIMEOUT 15000


/*****************************************************************************/
// ROB data structures
/*****************************************************************************/

// Configuration results
struct Sconfig_ROB {
  short sent, bad; // Sent and unsuccessful cmds
  short crc;
  bool error;
};

// Format of return data from "Read ROB pwr" (0x5B) command
struct Sread_ROB_pwr {
  unsigned char code;
  float Vcc[7], Vdd[7], current[7];
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "Reset ROB" (0x32) command,
// also "Disable temperature test" (0x83) command,
// "CPU-Ck Delay" (0x26) command
struct Srob_1 {
  unsigned char code1, code2;
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "Run BIST test" (0x41) command
struct Srun_BIST_test {
  unsigned char code1, bist[4][2];
  unsigned char code2;
  char narg;
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "Read out ROB" (0x6C) command
struct Sread_out_ROB {
  unsigned char code1, more_data;
  int readout[240];
  char size;
  unsigned char code2, error;
  int ccbserver_code;
  std::string msg;
};

// Status ROB results
struct Sstatus_ROB {
  bool error, warning; // error and warning flags
  bool ccbserver_error; // ccbserver error
  std::string stmsg; // ROB status
  std::string errmsg; // error/warning messages
  std::string xmlmsg;
};

/*****************************************************************************/
// ROB class
/*****************************************************************************/

class Crob : public Ccmn_cmd {
 public:
// Class constructor; arguments: pointer_to_command_object
  Crob(Ccommand*);

/* Override TDC methods */
  void read_TDC(char,char);
  int read_TDC_decode(unsigned char*);
  void read_TDC_print();

  void status_TDC(char,char);
  void status_TDC_print();

/* Database commands */
  void status_ROB_to_db(Cref*); // Check ROB status and log it in the DB. Input: Cref_obj

/* HIGH LEVEL COMMANDS */

// Configure the ROB. Input: Cconf_object
  Sconfig_ROB config_ROB_result;
  short config_ROB(Cconf*);
  void config_ROB_print();

// Check ROB status.
  Sstatus_ROB status_rob  ;
  void status_ROB(Cref*); // Input: Cref_obj
  void status_ROB_reset(); // Reset status_rob struct
  void status_ROB_print();

/* LOW LEVEL COMMANDS */

// "Read ROB pwr" (0x5B) command
  Sread_ROB_pwr read_rob_pwr; // Contains command results
  void read_ROB_pwr(); // Send command and decode reply.
  void read_ROB_pwr_reset(); // Reset read_rob_pwr struct
  int read_ROB_pwr_decode(unsigned char*); // Decode reply. Input: data_read
  void read_ROB_pwr_print(); // Print reply to stdout

// "ROB reset" (0x32) command
  Srob_1 reset_rob; // Contains command results
  void reset_ROB(); // Send command and decode reply
  void reset_ROB_reset(); // Reset reset_rob struct
  int reset_ROB_decode(unsigned char*); // Decode reply. Input: data_read
  void reset_ROB_print(); // Print reply to stdout

// "run BIST test" (0x41) command
  Srun_BIST_test run_bist_test; // Contains command results
  void run_BIST_test(char); // Send command and decode reply. Input: brd.
  void run_BIST_test_reset(); // Reset run_bist_test struct
  int run_BIST_test_decode(unsigned char*); // Decode reply. Input: data_read
  void run_BIST_test_print(); // Print reply to stdout

// "Read out ROB" (0x6C) command
  Sread_out_ROB read_out_rob; // Contains command results
  void read_out_ROB(char); // Send command and decode reply. Input: board
  void read_out_ROB_reset(); // Reset read_out_rob struct
  int read_out_ROB_decode(unsigned char*); // Decode reply. Input: data_read
  void read_out_ROB_print(); // Print reply to stdout

// "set CPU-Ck delay" (0x26) command
  Srob_1 set_cpu_ck_delay; // Contains command results
  void set_CPU_ck_delay(char); // Send command and decode reply. Input: delay (unit 150ps)
  void set_CPU_ck_delay_reset(); // Reset set_cpu_ck_delay struct
  int set_CPU_ck_delay_decode(unsigned char*); // Decode reply. Input: data_read
  void set_CPU_ck_delay_print(); // Print reply to stdout

  // "Disable temperature test" (0x83) command
  Srob_1 rob_temp_test; // Contains command results
  void disable_temp_test(char); // Send command and decode reply. Input: disable (if 1 disable TRB/ROB temperature test for 1 hour)
  void disable_temp_test_reset(); // Reset rob_temp_test struct
  int disable_temp_test_decode(unsigned char*); // Decode reply. Input: data_readx
  void disable_temp_test_print(); // Print reply to stdout

};

#endif // __CROB_H__
