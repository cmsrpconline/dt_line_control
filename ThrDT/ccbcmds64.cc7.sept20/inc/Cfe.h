// include Cfe.h only once
#ifndef __CFE_H__
#define __CFE_H__

// defines classes Ccmn_cmd
#include <ccbcmds/Ccmn_cmd.h>

// configurations class
#include <ccbcmds/Cconf.h>

// default timeouts
#define READ_FE_TEMP_TIMEOUT  5000
#define READ_FE_MASK_TIMEOUT  5000
#define SET_SL_MASK_TIMEOUT  15000
#define READ_FE_TMASK_TIMEOUT 5000
#define SET_SL_TMASK_TIMEOUT 15000
#define FE_TEST_TIMEOUT      15000

#define SET_TP_TIMEOUT            15000
#define READ_TP_TIMEOUT            5000
#define TEST_TP_FINEDELAY_TIMEOUT  5000
#define SET_TP_OFFSET_TIMEOUT     15000
#define READ_TP_OFFSET_TIMEOUT     5000

#define TEST_DAC_TIMEOUT       5000
#define SET_CALIB_DAC_TIMEOUT 15000
#define READ_CALIB_DAC_TIMEOUT 5000

#define SET_TP_OFFSET_MAX_MIN 167

/*****************************************************************************/
// Front-End data structures
/*****************************************************************************/

// Configuration results
struct Sconfig_FE {
  short sent, bad; // Sent and unsuccessful cmds
  short crc;
  bool error;
};

// Format of return data from "set FE threshold" (0x35) command
struct Sset_FE_thr {
  unsigned char code1;
  float bias, threshold, adc_bias, adc_threshold;
  unsigned char code2;
  char narg;
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "set FE width" (0x47) command
struct Sset_FE_width {
  unsigned char code;
  float width, adc_width;
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "read FE temperature" (0x36) command
struct Sread_FE_temp {
  unsigned char code;
  short temp[98];
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "read FE mask" (0x38),
// also "Mask FE" (0x3A) and "Mask FE channel" (0x3B).
struct SFE_mask {
  unsigned char code;
  unsigned char MadMsk[3][24][3];
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "Front-ends super layer enable" (0x9A),
// "Front-ends super layer enable Max Temperature" (0x9B),
struct Sfe_1 {
  unsigned char code1, code2, result;
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "Test DACs" (0x68)
struct Stest_dac {
  unsigned char code1, code2;
  unsigned short result;
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "read FE temperature mask" (0x71),
// also "set FE temperature mask" (0x62).
struct SFE_Tmask {
  unsigned char code;
  unsigned char TempMsk[3][24];
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "FE test" (0x4B)
struct SFE_test {
  unsigned char code;
  short Fe_TestI2C[3], SlPresMsk[3];
  int SlBoardPresMsk[3], SlExBoardMsk[3];
  int ccbserver_code;
  std::string msg;
};


// Format of return data from "Test Pulse settings" (0x4A),
// "Relative Test Pulse settings" (0x78), "Set TP Offset" (0x8B),
// "Calibration DAC" (0x6B)
struct Sfe_2 {
  unsigned char code1, code2;
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "Read Test Pulse settings" (0x72),
struct Sread_TP {
  unsigned char code;
  unsigned char coarsedelay1, finedelay1, coarsedelay2, finedelay2, endSqDly;
  float Amplitude1, Amplitude2;
  unsigned char FEDisDly, fineofs1, fineofs2;
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "Test Fine Delay" (0x82),
struct Stest_TP_finedelay {
  unsigned char code1, code2;
  bool CpuCkDelay, TpFineDelay1, TpFineDelay2;
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "Read TP offset" (0x8C),
struct Sread_TP_offset {
  unsigned char code;
  unsigned char TpOffset[8], fineofs[2], min, max;
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "Read calibration DAC" (0x6E)
struct Sread_calib_DAC {
  unsigned char code;
  float bias_gain[3], thr_gain[3], width_gain, bias_offset[3],
    thr_offset[3], width_offset;
  int ccbserver_code;
  std::string msg;
};

/*****************************************************************************/
// Front-End class
/*****************************************************************************/

class Cfe : public Ccmn_cmd {
 public:
// Class constructor; arguments: pointer_to_command_object
  Cfe(Ccommand*);

/* Override ROB/TDC methods */
  void read_TDC(char,char);
  int read_TDC_decode(unsigned char*);
  void read_TDC_print();

  void status_TDC(char,char);
  void status_TDC_print();

  void read_ROB_error();
  int read_ROB_error_decode(unsigned char*);
  void read_ROB_error_print();

/* Database commands */
  void FE_mask_to_db(); // Log FE mask in the DB.

/* HIGH LEVEL COMMANDS */

// Configure the FE. Input: Cconf_object
  Sconfig_FE config_FE_result;
  short config_FE(Cconf*);
  void config_FE_print();

//  Retrive FE config from CCB, dump it to file
  void read_FE_config(char *filename);

/* LOW LEVEL COMMANDS */

// "set FE threshold" (0x35) command
  Sset_FE_thr set_fe_thr;
  void set_FE_thr(short,float,float); // Send command and decode reply. Input: superlayer, bias, threshold
  void set_FE_thr_reset(); // Reset set_fe_thr struct
  int set_FE_thr_decode(unsigned char *); // Decode reply. Input: data_read
  void set_FE_thr_print(); // Print reply to stdout

// "set FE width" (0x47) command
  Sset_FE_width set_fe_width;
  void set_FE_width(float); // Send command and decode reply. Input: width
  void set_FE_width_reset(); // Reset set_fe_width struct
  int set_FE_width_decode(unsigned char *); // Decode reply. Input: data_read
  void set_FE_width_print(); // Print reply to stdout

// "read FE temperature" (0x36) command
  Sread_FE_temp read_fe_temp;
  void read_FE_temp(char,char); // Send command and decode reply. Input: superlayer, options (-1=all, 0=only Tmax-Tmed).
  void read_FE_temp_reset(); // Reset read_fe_temp struct
  int read_FE_temp_decode(unsigned char *); // Decode reply. Input: data_read
  void read_FE_temp_print(); // Print reply to stdout

// "read FE mask" (0x38) command
  SFE_mask read_fe_mask;
  void read_FE_mask(); // Send command and decode reply.
  void read_FE_mask_reset(); // Reset read_fe_mask struct
  int read_FE_mask_decode(unsigned char *); // Decode reply. Input: data_read
  void read_FE_mask_print(); // Print reply to stdout

// "Mask FE" (0x3A) command
  SFE_mask set_fe_mask;
  void set_FE_mask(short,short,unsigned char*); // Send command and decode reply. Input: superlayer, board, mask[3]
  void set_FE_mask_reset(); // Reset set_fe_mask struct
  int set_FE_mask_decode(unsigned char *); // Decode reply. Input: data_read
  void set_FE_mask_print(); // Print reply to stdout

// "Mask FE channel" (0x3B) command
  void set_FE_mask_ch(short,short,short); // Send command and decode reply. Input: superlayer, channel, status
// Write results in <set_fe_mask>. Use set_fe_mask_print() to see them

// "FE super layer enable" (0x9A) command
  Sfe_1 set_sl_mask;
  void set_SL_mask(char,char); // Send command and decode reply. Input: superlayer, enable (if 1 enable all channels)
  void set_SL_mask_reset(); // Reset set_sl_mask struct
  int set_SL_mask_decode(unsigned char *); // Decode reply. Input: data_read
  void set_SL_mask_print(); // Print reply to stdout

// "Read FE temperature mask" (0x71) command
  SFE_Tmask read_fe_Tmask;
  void read_FE_Tmask(); // Send command and decode reply.
  void read_FE_Tmask_reset(); // Reset read_fe_Tmask struct
  int read_FE_Tmask_decode(unsigned char *); // Decode reply. Input: data_read
  void read_FE_Tmask_print(); // Print reply to stdout

// "set FE temperature mask" (0x62) command
  SFE_Tmask set_fe_Tmask;
  void set_FE_Tmask(short,short,short); // Send command and decode reply. Input: superlayer, chip, status
  void set_FE_Tmask_reset(); // Reset set_fe_Tmask struct
  int set_FE_Tmask_decode(unsigned char *); // Decode reply. Input: data_read
  void set_FE_Tmask_print(); // Print reply to stdout

// "FE super layer Tmax enable" (0x9B) command
  Sfe_1 set_sl_Tmask;
  void set_SL_Tmask(char,char); // Send command and decode reply. Input: superlayer, enable (if 1 enable all channels)
  void set_SL_Tmask_reset(); // Reset set_sl_Tmask struct
  int set_SL_Tmask_decode(unsigned char *); // Decode reply. Input: data_read
  void set_SL_Tmask_print(); // Print reply to stdout


// "FE test" (0x4B) command
  SFE_test fe_test;
  void FE_test(); // Send command and decode reply.
  void FE_test(char); // Send command and decode reply. Input: force (=1 to force FEB search in firmware >=1.20)
  void FE_test_reset(); // Reset fe_test struct
  int FE_test_decode(unsigned char *); // Decode reply. Input: data_read
  void FE_test_print(); // Print reply to stdout

/* TEST PULSE */

// "Test Pulse settings" (0x4A) command
  Sfe_2 set_tp;
  void set_TP(char,char,char,char,char,float,float,char,char*); // Send command and decode reply. Input: coarsedelay1, finedelay1, coarsedelay2, finedelay2, endSqDly, Amplitude1, Amplitude2, FEDisDly, fineofs[2]
  void set_TP_reset(); // Reset set_tp struct
  int set_TP_decode(unsigned char *); // Decode reply. Input: data_read
  void set_TP_print(); // Print reply to stdout

// "Relative Test Pulse settings" (0x78) command
  Sfe_2 set_rel_tp;
  void set_rel_TP(float,float,float,char,char); // Send command and decode reply. Input: delay, Amplitude1, Amplitude2, endSqDly, FEDisDly
  void set_rel_TP_reset(); // Reset set_rel_tp struct
  int set_rel_TP_decode(unsigned char *); // Decode reply. Input: data_read
  void set_rel_TP_print(); // Print reply to stdout

// "Read Test Pulse settings" (0x72) command
  Sread_TP read_tp;
  void read_TP(); // Send command and decode reply.
  void read_TP_reset(); // Reset read_tp struct
  int read_TP_decode(unsigned char *); // Decode reply. Input: data_read
  void read_TP_print(); // Print reply to stdout

// "Test Fine Delay" (0x82) command
  Stest_TP_finedelay test_tp_finedelay;
  void test_TP_finedelay(); // Send command and decode reply.
  void test_TP_finedelay_reset(); // Reset test_tp_finedelay struct
  int test_TP_finedelay_decode(unsigned char *); // Decode reply. Input: data_read
  void test_TP_finedelay_print(); // Print reply to stdout

// "Set TP offset" (0x8B) command. (max-min) must be > 167
  Sfe_2 set_tp_offset;
  void set_TP_offset(char*, char*, char, char); // Send command and decode reply. Input: TpOffset[8], fineofs[2], min, max (min-max are used only for firmware>=v1.9)
  void set_TP_offset_reset(); // Reset set_tp_offset struct
  int set_TP_offset_decode(unsigned char *); // Decode reply. Input: data_read
  void set_TP_offset_print(); // Print reply to stdout

// "Read TP offset" (0x8C) command
  Sread_TP_offset read_tp_offset;
  void read_TP_offset(); // Send command and decode reply.
  void read_TP_offset_reset(); // Reset read_tp_offset struct
  int read_TP_offset_decode(unsigned char *); // Decode reply. Input: data_read
  void read_TP_offset_print(); // Print reply to stdout


/* DAC */

// "Test DAC" (0x68) command
  Stest_dac test_dac;
  void test_DAC(); // Send command and decode reply.
  void test_DAC_reset(); // Reset test_dac struct
  int test_DAC_decode(unsigned char *); // Decode reply. Input: data_read
  void test_DAC_print(); // Print reply to stdout


// "Calibration DAC" (0x6B) command
  Sfe_2 set_calib_dac;
  void set_calib_DAC(float*,float*,float,float*,float*,float); // Send command and decode reply. Input: bias_gain[3], thr_gain[3], width_gain, bias_offset[3], thr_offset[3], width_offset
  void set_calib_DAC_reset(); // Reset set_calib_dac struct
  int set_calib_DAC_decode(unsigned char *); // Decode reply. Input: data_read
  void set_calib_DAC_print(); // Print reply to stdout

// "Read Calibration DAC" (0x6E) command
  Sread_calib_DAC read_calib_dac;
  void read_calib_DAC(); // Send command and decode reply.
  void read_calib_DAC_reset(); // Reset read_calib_dac struct
  int read_calib_DAC_decode(unsigned char *); // Decode reply. Input: data_read
  void read_calib_DAC_print(); // Print reply to stdout

};

#endif // __CFE_H__
