// include Ctss.h only once
#ifndef __CTSS_H__
#define __CTSS_H__

// defines classes Ccmn_cmd
#include <ccbcmds/Ccmn_cmd.h>

// configurations class
#include <ccbcmds/Cconf.h>

// default timeouts
#define READ_TSS_TIMEOUT   5000

/*****************************************************************************/
// TSS data structures
/*****************************************************************************/

// Configuration results
struct Sconfig_TSS {
  short sent, bad; // Sent and unsuccessful cmds
  short crc;
  bool error;
  std::string msg;
};

// Format of return data from "read TSS" (0x1D) command
struct Sread_TSS {
  unsigned char code1, conf[7], testin[20], testout[32];
  unsigned char code2;
  char narg;
  int ccbserver_code;
  std::string msg;
};


// Format of return data from "write TSS" (0x16) command
struct Swrite_TSS {
  unsigned char code1, code2;
  char result;
  int ccbserver_code;
  std::string msg;
};


/*****************************************************************************/
// TSS class
/*****************************************************************************/

class Ctss : public Ccmn_cmd {
 public:
// Class constructor; arguments: pointer_to_ccb_object
  Ctss(Ccommand*);

/* Override ROB/TDC methods */
  void read_TDC(char,char);
  int read_TDC_decode(unsigned char*);
  void read_TDC_print();

  void status_TDC(char,char);
  void status_TDC_print();

  void read_ROB_error();
  int read_ROB_error_decode(unsigned char*);
  void read_ROB_error_print();

/* HIGH LEVEL COMMANDS */

// Configure the TSS. Input: Cconf_object, brd
  Sconfig_TSS config_TSS_result;
  short config_TSS(Cconf*,char);
  void config_TSS_print();

//  Retrive TSS config from CCB, dump it to file
  void read_TSS_config(char *filename);

/* LOW LEVEL COMMANDS */

// "read TSS" (0x1D) command
  Sread_TSS read_tss; // contains command results
  void read_TSS(char); // Send command and decode reply. Input: brd.
  void read_TSS_reset(); // Reset read_tss struct
  int read_TSS_decode(unsigned char*); // Decode reply. Input: data_read
  void read_TSS_print(); // print reply to stdout

// "write TSS" (0x16) command
  Swrite_TSS write_tss; // contains command results
  void write_TSS(char brd, unsigned char*, unsigned char*); // Send command and decode reply. Input: brd, conf[7], testin[20]
  void write_TSS_reset(); // Reset write_tss struct
  int write_TSS_decode(unsigned char*); // Decode reply. Input: data_read
  void write_TSS_print(); // Print reply to standard output

};

#endif // __CTSS_H__
