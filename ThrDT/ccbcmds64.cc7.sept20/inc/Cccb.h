// Include Cccb.h only once
#ifndef __CCCB_H__
#define __CCCB_H__

// defines classes Ccmn_cmd
#include <ccbcmds/Ccmn_cmd.h>

// reference values class
#include <ccbcmds/Cref.h>

// some string dimensions
#define SCRIPT_MAX_SIZE         32
#define CUSTOM_DATA_MAX_SIZE   240
#define LINK_TEST_MIN_SIZE       1
#define LINK_TEST_MAX_SIZE     249
#define READ_CPU_ADDR_MAX_SIZE 245

// ccb_server timeouts
#define SAVE_MC_CONF_TIMEOUT  15000
#define LOAD_MC_CONF_TIMEOUT  90000
#define RESTORE_MC_CONF_TIMEOUT      90000
#define EXIT_TIMEOUT          15000
#define RESTART_CCB_TIMEOUT   15000
#define RD_CPU_ADDR_TIMEOUT    5000
#define RUN_IN_PROG_TIMEOUT   15000
#define READ_COMM_ERR_TIMEOUT  5000
#define AUTO_SET_LINK_TIMEOUT 15000
#define READ_LINK_TIMEOUT      5000
#define DISABLE_LINK_MONITOR_TIMEOUT 15000
#define SCRIPT_TIMEOUT        15000
#define AUTO_TRIGGER_TIMEOUT  15000
#define RD_CRATE_SENSOR_ID_TIMEOUT   15000
#define CCBRDY_PULSE_TIMEOUT  15000
#define PIRW_TIMEOUT          15000
#define PIPROG_TIMEOUT        15000
#define DISABLE_SB_CK_TIMEOUT 15000
#define DISABLE_OSC_CK_TIMEOUT       15000
#define WR_CUSTOM_DATA_TIMEOUT       15000
#define RD_CUSTOM_DATA_TIMEOUT       15000
#define LINK_TEST_BOOT_TIMEOUT       15000
#define RESET_SEU_BOOT_TIMEOUT       15000
#define LINK_DAC_BOOT_TIMEOUT        15000
#define FIND_SENSORS_TIMEOUT  15000
#define PROTECT_FLASH_TIMEOUT  5000
#define ERASE_FLASH_TIMEOUT   15000
#define WRITE_FLASH_TIMEOUT    5000

/*****************************************************************************/
// CCB/Minicrate data structures
/*****************************************************************************/

// Format of return data from "Read Communication error" (0xF0) command
struct Smc_read_comm_err {
  unsigned char code, port;
  bool Parity, Framing, Break, Noise, Overrun, Sync, Crc, LoseData, Size,
    TimeOut, Unexpected, BufferOverflow, BufferEmpty, BufferTooSmall;
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "Auto Set Link" (0x74) command,
// also "Read Link Data" (0x76)
struct Slink {
  unsigned char code;
  short Offset, Hyst, Apl, Thr;
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "Script" (0xEE) command
struct Smc_script {
  unsigned char code, size;
  int ccbserver_code;
  std::string msg;
};


// Format of return data from "Restore CFG" (0x96) command,
// "Auto LINK set (boot)" (0xE7)
struct Smc_1 {
  unsigned char code1, code2, result;
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "Exit" (0xA5) command, also
// "Watchdog Reset" (0xF8),
// "Run in Progress" (0x46), "Disable Link Monitor" (0x77),
// "Auto Trigger" (0x70), "CCBRdy Pulse" (0x91), PIRW (0x92), PIPROG (0xAF),
// "Disable SB CK" (0x94), "Disable Ocillator CK" (0x95),
// "Write custom data" (0x97), "LINK test (boot)" (0xE4),
// "Reset SEU (boot)" (0xE6), "Link DAC (boot)" (0xE5)
// "HD Watchdog Reset" (0xF7), "Protect/Unprotect Flash" (0xED),
// "Erase Flash" (0xEB)
struct Smc_2 {
  unsigned char code1, code2;
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "Save MC configuration" (0x40) command
// also "Load MC configuration" (0x61)
struct Smc_3 {
  unsigned char code1, code2;
  char error_old;
  short error;
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "Read MC sensors ID" (0x8F) command
// also "Find Sensors" (0xA6) command
struct Smc_4 {
  unsigned char code, snsr_code[21][8];
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "Read custom data" (0x98) command
struct Srd_custom_data {
  unsigned char code, size, data[CUSTOM_DATA_MAX_SIZE];
  int ccbserver_code;
  std::string msg;
};

// Format return data from "Read From CPU address space" (0xF5)
struct Srd_cpu_addr {
  unsigned char code, page, size, data[READ_CPU_ADDR_MAX_SIZE];
  short addr;
  int ccbserver_code;
  std::string msg;
};

// Format return data from "Write Flash" (0xF3)
struct Swr_flash {
  unsigned char code, error, sector;
  int ccbserver_code;
  std::string msg;
};

// Status MC results
struct Smc_status_check {
  bool mc_program; // true when "minicrate" program is active
  Emcstatus phys_status, logic_status; // Physical & Logical status
  bool switch_off_fe, switch_off_mc; // When true, FE or MC need to be switched off
  std::string msg; // error/warnin messages
  std::string xmlmsg;
};

// Test MC results
struct Smc_test_check {
  Emcstatus Flags, SrvBrd, TrgBrd, RoBrd; // Flags, Server-Board, TRB, ROB status
  std::string msg; // error/warnin messages
  std::string xmlmsg;
};


/*****************************************************************************/
// Cccb class definition
/*****************************************************************************/

class Cccb : public Ccmn_cmd {
 public:
// Class constructor; arguments: pointer_to_command_object
  Cccb(Ccommand*);

/* Override ROB/TDC methods */
  void read_TDC(char,char);
  int read_TDC_decode(unsigned char*);
  void read_TDC_print();

  void status_TDC(char,char);
  void status_TDC_print();

  void read_ROB_error();
  int read_ROB_error_decode(unsigned char*);
  void read_ROB_error_print();

/* Database commands */

  void MC_status_to_db(Cref*); // Log minicrate status and temperature (0xEA+0x3C) in the DB. Input: Cref_obj
  void MC_status_from_db(); // Retrieve last status info from DB

/* HIGH LEVEL COMMANDS */

// Run "Status" command (0xEA) and compare values with a reference
  Smc_status_check mc_status_check;
  void MC_check_status_reset();
  void MC_check_status(Cref*,bool,bool*,Emcstatus*,Emcstatus*,char*); // Input: Cref_obj, disable_qpll_check. Reply: mc_program, phys_status, logic_status, ErrMessage
  void MC_check_status_print();

// Run "Read Test"/"Run Test" command (0x10/0x12) program and compare values with a reference
  struct Smc_test_check mc_test_check;
  void MC_check_test_reset();
  void MC_check_test(Cref*,bool,Emcstatus*,Emcstatus*,Emcstatus*,Emcstatus*,char*); // Input: Cref_obj, run_test. Reply: flag_status, sb_status, trb_status, rob_status, ErrMessage
  void MC_check_test_print();

/* LOW LEVEL COMMANDS */

// "Save MC configuration" (0x40) command
  Smc_3 save_mc_conf; // Contains command results
  void save_MC_conf(); // Send command and decode reply.
  void save_MC_conf_reset(); // Reset save_mc_conf struct
  int save_MC_conf_decode(unsigned char*); // Decode reply. Input: data_read
  void save_MC_conf_print(); // Print reply to stdout

// "Load MC configuration" (0x61) command
  Smc_3 load_mc_conf; // Contains command results
  void load_MC_conf(); // Send command and decode reply.
  void load_MC_conf_reset(); // Reset load_mc_conf struct
  int load_MC_conf_decode(unsigned char*); // Decode reply. Input: data_read
  void load_MC_conf_print(); // Print reply to stdout

// "Restore MC configuration" (0x96) command
  Smc_1 restore_mc_conf; // Contains command results
  void restore_MC_conf(); // Send command and decode reply.
  void restore_MC_conf_reset(); // Reset restore_mc_conf struct
  int restore_MC_conf_decode(unsigned char*); // Decode reply. Input: data_read
  void restore_MC_conf_print(); // Print reply to stdout

// "Exit" (0xA5) command
  Smc_2 exit_mc; // Contains command results
  void exit_MC(); // Send command and decode reply.
  void exit_MC_reset(); // Reset exit_mc struct
  int exit_MC_decode(unsigned char*); // Decode reply. Input: data_read
  void exit_MC_print(); // Print reply to stdout

// "Watchdog Reset" (0xF8) command
  Smc_2 restart_ccb; // Contains command results
  void restart_CCB(); // Send command and decode reply.
  void restart_CCB_reset(); // Reset restart_ccb struct
  int restart_CCB_decode(unsigned char*); // Decode reply. Input: data_read
  void restart_CCB_print(); // Print reply to stdout

// "Read CPU address space" (0xF5) command
  Srd_cpu_addr rd_cpu_addr; // Contains command results
  void read_CPU_addr(char,short,short); // Send command and decode reply. Input: page, addr, size
  void read_CPU_addr_reset(); // Reset rd_cpu_addr
  int read_CPU_addr_decode(unsigned char*); // Decode reply. Input: data_read
  void read_CPU_addr_print(); // Print reply to stdout

// "Run in Progress" (0x46) command
  Smc_2 run_in_prog; // Contains command results
  void run_in_progress(char); // Send command and decode reply. Input: on. If On=1 CCB knows that DAQ is running
  void run_in_progress_reset(); // Reset run_in_prog struct
  int run_in_progress_decode(unsigned char*); // Decode reply. Input: data_read
  void run_in_progress_print(); // Print reply to stdout

// "Read Communication Error" (0xF0) command
  Smc_read_comm_err mc_read_comm_err; // Contains command results
  void read_comm_err(); // Send command and decode reply.
  void read_comm_err_reset(); // Reset mc_read_comm_err struct
  int read_comm_err_decode(unsigned char*); // Decode reply. Input: data_read
  void read_comm_err_print(); // Print reply to stdout

// "Auto Set Link" (0x74) command
  Slink auto_set_link; // Contains command results
  void auto_set_LINK(); // Send command and decode reply.
  void auto_set_LINK_reset(); // Reset auto_set_link struct
  int auto_set_LINK_decode(unsigned char *); // Decode reply. Input: data_read
  void auto_set_LINK_print(); // Print reply to stdout

// "Read Link Data" (0x76) command
  Slink read_link; // Contains command results
  void read_LINK(); // Send command and decode reply.
  void read_LINK_reset(); // Reset read_link struct
  int read_LINK_decode(unsigned char*); // Decode reply. Input: data_read
  void read_LINK_print(); // Print reply to stdout

// "Disable Link Monitor" (0x77) command
  Smc_2 disble_link_mon; // Contains command results
  void disable_LINK_monitor(char); // Send command and decode reply. Input: disable. Disable=1 means that the periodic link test is disabled
  void disable_LINK_monitor_reset(); // Reset disble_link_mon struct
  int disable_LINK_monitor_decode(unsigned char*); // Decode reply. Input: data_read
  void disable_LINK_monitor_print(); // Print reply to stdout

// "Script" (0xEE) command
// e.g. script((char)0,(short)0x200,"mctest") starts minicrate program 
  Smc_script mc_script; // Contains command results
  void script(char, short, char*); // Send command and decode reply. Input: page, addr, arg[n] (n=0-32)
  void script_reset(); // Reset mc_script struct
  int script_decode(unsigned char *); // Decode reply. Input: data_read
  void script_print(); // Print reply to stdout

// "Auto Trigger" (0x70) command
  Smc_2 mc_auto_trigger; // Contains command results
  void auto_trigger(char, char); // Send command and decode reply. Input: en, L1A_delay (en=1 means enable_auto_trigger; L1A is in units of 25ns)
  void auto_trigger_reset(); // Reset mc_auto_trigger struct
  int auto_trigger_decode(unsigned char*); // Decode reply. Input: data_read
  void auto_trigger_print(); // Print reply to stdout

// "Read MC crate sensors ID" (0x8F) command
  Smc_4 mc_crate_sensor; // Contains command results
  void read_crate_sensor_id(); // Send command and decode reply.
  void read_crate_sensor_id_reset(); // Reset mc_crate_sensor struct
  int read_crate_sensor_id_decode(unsigned char*); // Decode reply. Input: data_read
  void read_crate_sensor_id_print();

// "CCBRdy Pulse" (0x91) command
  Smc_2 mc_ccbrdy_pulse; // Contains command results
  void CCBrdy_pulse(); // Send command and decode reply.
  void CCBrdy_pulse_reset(); // Reset mc_ccbrdy_pulse struct
  int CCBrdy_pulse_decode(unsigned char*); // Decode reply. Input: data_read
  void CCBrdy_pulse_print();

// "PIRW" (0x92) command
  Smc_2 mc_pirw; // Contains command results
  void pirw(char); // Send command and decode reply. Input: state
  void pirw_reset(); // Reset mc_pirw struct
  int pirw_decode(unsigned char*); // Decode reply. Input: data_read
  void pirw_print();


// "PIPROG" (0xAF) command
  Smc_2 mc_piprog; // Contains command results
  void piprog(char); // Send command and decode reply. Input: state
  void piprog_reset(); // Reset mc_piprog struct
  int piprog_decode(unsigned char*); // Decode reply. Input: data_read
  void piprog_print();


// "Disable SB CK" (0x94) command
  Smc_2 disable_sb_ck; // Contains command results
  void disable_SB_CK(char); // Send command and decode reply. Input: state (1=disable)
  void disable_SB_CK_reset(); // Reset disable_sb_ck struct
  int disable_SB_CK_decode(unsigned char*); // Decode reply. Input: data_read
  void disable_SB_CK_print();

// "Disable oscillator CK" (0x95) command
  Smc_2 disable_osc_ck; // Contains command results
  void disable_OSC_CK(char); // Send command and decode reply. Input: state (1=disable)
  void disable_OSC_CK_reset(); // Reset disable_osc_ck struct 
  int disable_OSC_CK_decode(unsigned char*); // Decode reply. Input: data_read
  void disable_OSC_CK_print();

// "Write custom data" (0x97) command
  Smc_2 wr_custom_data; // Contains command results
  void write_custom_data(unsigned char*,unsigned char); // Send command and decode reply. Input: data[size], size (<=240)
  void write_custom_data_reset(); // Reset wr_custom_data struct
  int write_custom_data_decode(unsigned char*); // Decode reply. Input: data_read
  void write_custom_data_print();

// "Read custom data" (0x98) command
  Srd_custom_data rd_custom_data; // Contains command results
  void read_custom_data(); // Send command and decode reply.
  void read_custom_data_reset(); // Reset rd_custom_data struct
  int read_custom_data_decode(unsigned char*,int); // Decode reply. Input: data_read, data_read_len
  void read_custom_data_print();

// "HD Watchdog Reset" (0xF7) command
  Smc_2 hd_restart_ccb; // Contains command results
  void hd_restart_CCB(); // Send command and decode reply.
  void hd_restart_CCB_reset(); // Reset restart_ccb struct
  int hd_restart_CCB_decode(unsigned char*); // Decode reply. Input: data_read
  void hd_restart_CCB_print(); // Print reply to stdout

// "Find Sensors" (0xA6) command
  Smc_4 mc_find_sensor; // Contains command results
  void find_sensors(); // Send command and decode reply.
  void find_sensors_reset(); // Reset restart_ccb struct
  int find_sensors_decode(unsigned char*); // Decode reply. Input: data_read
  void find_sensors_print(); // Print reply to stdout

///////////////////////
//// BOOT COMMANDS ////
///////////////////////

// "Auto LINK set (boot)" (0xE7) command
  Smc_1 auto_set_link_boot; // Contains command results
  void auto_set_LINK_boot(short,short); // Send command and decode reply. Input: levL, levH
  void auto_set_LINK_boot_reset(); // Reset auto_set_link_boot struct
  int auto_set_LINK_boot_decode(unsigned char*,int); // Decode reply. Input: data_read, data_read_len
  void auto_set_LINK_boot_print();

// "LINK test (boot)" (0xE4) command
  Smc_2 link_test_boot; // Contains command results
  void LINK_test_boot(char[LINK_TEST_MAX_SIZE],short); // Send command and decode reply. Input: data, size
  void LINK_test_boot_reset(); // Reset link_test_boot struct
  int LINK_test_boot_decode(unsigned char*,int); // Decode reply. Input: data_read, data_read_len
  void LINK_test_boot_print();

// "Reset SEU (boot)" (0xE6) command
  Smc_2 reset_seu_boot; // Contains command results
  void reset_SEU_boot(); // Send command and decode reply.
  void reset_SEU_boot_reset(); // Reset rest_seu_boot struct
  int reset_SEU_boot_decode(unsigned char*,int); // Decode reply. Input: data_read, data_read_len
  void reset_SEU_boot_print();

// "Link DAC (boot)" (0xE5) command
  Smc_2 link_dac_boot; // Contains command results
  void link_DAC_boot(short); // Send command and decode reply. Input: dacvalue
  void link_DAC_boot_reset(); // Reset link_dac_boot struct
  int link_DAC_boot_decode(unsigned char*,int); // Decode reply. Input: data_read, data_read_len
  void link_DAC_boot_print();


// "Protect/Unprotect Flash" (0xED) command
  Smc_2 protect_flash; // Contains command results
  void protect_FLASH(char); // Send command and decode reply. Input: protect (1=protect, 0=unprotect)
  void protect_FLASH_reset(); // Reset protect_flash struct
  int protect_FLASH_decode(unsigned char*,int); // Decode reply. Input: data_read, data_read_len
  void protect_FLASH_print();


// "Erase Flash" (0xEB) command
  Smc_2 erase_flash; // Contains command results
  void erase_FLASH(); // Send command and decode reply.
  void erase_FLASH_reset(); // Reset erase_flash struct
  int erase_FLASH_decode(unsigned char*,int); // Decode reply. Input: data_read, data_read_len
  void erase_FLASH_print();

// "Write Flash" (0xF3) command
  Swr_flash wr_flash; // Contains command results
  void write_FLASH(char,short,unsigned char[128]); // Send command and decode reply. Input: page, addr, data[128]
  void write_FLASH_reset(); // Reset wr_flash struct
  int write_FLASH_decode(unsigned char*,int); // Decode reply. Input: data_read, data_read_len
  void write_FLASH_print();

};

#endif // __CCCB_H__
