// include Ctsm.h only once
#ifndef __CTSM_H__
#define __CTSM_H__

// defines classes Ccmn_cmd
#include <ccbcmds/Ccmn_cmd.h>

// configurations class
#include <ccbcmds/Cconf.h>

// default timeouts
#define READ_TSM_TIMEOUT       5000

/*****************************************************************************/
// TSM data structures
/*****************************************************************************/

// Configuration results
struct Sconfig_TSM {
  short sent, bad; // Sent and unsuccessful cmds
  short crc;
  bool error;
};


// Format of return data from "read TSM" (0x1F) command
struct Sread_TSM {
  unsigned char code, conf_sorter[2], conf_dataup[2], conf_datadown[2];
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "write TSM" (0x17) command
struct Swrite_TSM {
  unsigned char code1, code2;
  char result;
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "TrgOut Select" (0x49) command
struct Strgout_sel {
  unsigned char code1, code2;
  int ccbserver_code;
  std::string msg;
};


/*****************************************************************************/
// TSM class
/*****************************************************************************/

class Ctsm : public Ccmn_cmd {
 public:
// Class constructor; arguments: pointer_to_ccb_object
  Ctsm(Ccommand*);

/* Override ROB/TDC methods */
  void read_TDC(char,char);
  int read_TDC_decode(unsigned char*);
  void read_TDC_print();

  void status_TDC(char,char);
  void status_TDC_print();

  void read_ROB_error();
  int read_ROB_error_decode(unsigned char*);
  void read_ROB_error_print();

/* HIGH LEVEL COMMANDS */

// Configure the TSM. Input: Cconf_object
  Sconfig_TSM config_TSM_result;
  short config_TSM(Cconf*);
  void config_TSM_print();

//  Retrive TSM config from CCB, dump it to file
  void read_TSM_config(char *filename);

/* LOW LEVEL COMMANDS */

// "read TSM" (0x1F) command
  Sread_TSM read_tsm; // Contains command results
  void read_TSM(); // Send command and decode reply.
  void read_TSM_reset(); // Reset read_tsm struct
  int read_TSM_decode(unsigned char*); // Decode reply. Input: data_read
  void read_TSM_print(); // Print reply to stdout

// "write TSM" (0x17) command
  Swrite_TSM write_tsm; // Contains command results
  void write_TSM(unsigned char*, unsigned char*, unsigned char*); // Send command and decode reply. Input: conf_sorter[2], conf_dataup[2], conf_datadown[2]
  void write_TSM_reset(); // Reset write_tsm struct
  int write_TSM_decode(unsigned char*); // Decode reply. Input: data_read
  void write_TSM_print(); // Print reply to standard output

// "TrgOut Select" (0x49) command
  Strgout_sel trgout_sel; // Contains command results
  void TrgOut_select(char,char,char); // Send command and decode reply. Input: EnTrg_phi, EnTrg_theta, HTrg
  void TrgOut_select_reset(); // Reset trgout_sel struct
  int TrgOut_select_decode(unsigned char*); // Decode reply. Input: data_read
  void TrgOut_select_print(); // Print reply to standard output

};

#endif // __CTSM_H__
