#include "Ccmsdtdb.h"
#include <string>
#include <cstdlib>
#include <exception>

#include <string.h>

#define STR_LEN 40

int cmsdtdb::curr_run=0;

// Authors: S.Ventura, A.Parenti, 2006
// Modified: AP, Oct29 2007 (Tested on MySQL/Oracle)
cmsdtdb::cmsdtdb() {
// Initialise variables
  _verbose_ = false;
  //printf("cmsdtdb -> Default constructor");
  connct_time = 0;
  init();

  read_ds(datasource); // Read datasource from config file
  strcpy(user,""); // No username
  strcpy(passwd,""); // No pswd

// Connect to DATABASE
  connect();

// Initialize mutex
  pthread_mutex_init(&dtdb_mutex,NULL);
}


// Authors: S.Ventura, A.Parenti, 2006
// Modified: AP, Oct29 2007 (Tested on MySQL/Oracle)
cmsdtdb::cmsdtdb(char *DataSource, char *UserName, char *Password) {

// Initialise variables
  _verbose_ = false;
  //printf("cmsdtdb -> Explicit datasource constructor");
  connct_time = 0;
  init();

  strcpy(datasource,DataSource);
  strcpy(user,UserName);
  strcpy(passwd,Password);

// Connect to DATABASE
  connect();

// Initialize mutex
  pthread_mutex_init(&dtdb_mutex,NULL);
}


// Author: Andrea Parenti, Jan 2006
// Modified: AP, Jun26 2007
cmsdtdb::~cmsdtdb() {
  disconnect(); // Disconnect from DB
}

void cmsdtdb::verbose_mode(bool in) {
  _verbose_ = in;
}

// Lock the mutex
// Author: A.Parenti, Apr03 2007
void cmsdtdb::dblock() {
  pthread_mutex_lock(&dtdb_mutex);
}

// Unlock the mutex
// Author: A.Parenti, Apr03 2007
void cmsdtdb::dbunlock() {
  pthread_mutex_unlock(&dtdb_mutex);
}


// Submit a SQL query.
// Authors: Sandro Ventura, A.Parenti
// Modified: AP, Oct29 2007
int cmsdtdb::sqlquery(char *mquery, int *res_rows, int *res_cols) {

// Reset query results
  *res_rows = *res_cols = 0;
  resetqueryresults();

  if (!SQL_SUCCEEDED(connct_rc)) {
// Connection isn't open
    printf("Not connected to the database\n",connct_rc);
    return -1;
  }

  query_rc = SQLExecDirect(hstmt,(SQLCHAR*)mquery,strlen(mquery)); // Send SQL query
//  printf("Ccmsdtdb::sqlquery -- query string \"%s\"\n",mquery);

  if (!SQL_SUCCEEDED(query_rc)) { // Query error

    if (++errcount>=DB_MAXERR) { // Too many error: try to reconnect
      disconnect();
      init();
      connect();
      if (SQL_SUCCEEDED(connct_rc)) {
        printf("Reconnected to DB.\n");
        errcount=0;
      } else {
        printf("Reconnection to DB failed.\n");
      }
    }

    printf("QUERY FAILED\n");
    printf("  Query string \"%s\"\n",mquery);
    printf("  Returned code %d\n",query_rc);
    return -1;
  }

  errcount=0; // Successful query: reset error counter

  SQLNumResultCols(hstmt,&query_cols); // Number of read columns
  SQLRowCount(hstmt,&query_rows); // Number of rows. NB: with some drivers, query_rows assumes only 0 or 1 (even if more rows are read)

  *res_cols = query_cols;
  *res_rows = query_rows;

//  printf("Ccmsdtdb::sqlquery -- querydone. Found %d rows and %d cols.\n", query_rows, query_cols);

  return 0;
}

// Fetch a row from query results
// Authors: Sandro Ventura, A. Parenti
// AP: Modified Jan02 2006
int cmsdtdb::fetchrow() {
  if (SQL_SUCCEEDED(query_rc))  {
    return SQLFetch(hstmt);
  }
  return SQL_NO_DATA;
}


// Get a field from the fetched row
// Authors: S. Ventura, A. Parenti
// Tested: AP, Feb02 2006
int cmsdtdb::returnfield(int field, char *resu) {
  SQLLEN cbValue;
  SQLRETURN rc;

  strcpy(resu,""); // Reset "resu"
  field +=1; // ODBC starts counting from 1!!!

  if (SQL_SUCCEEDED(query_rc))  {
    rc = SQLGetData(hstmt, (SQLSMALLINT)(field), SQL_C_CHAR, (SQLCHAR*)resu,
             FIELD_MAX_LENGTH, &cbValue); // Convert field into chars
    resu[(int)cbValue]='\0'; // Add line terminator
    if (SQL_SUCCEEDED(rc))
      return (int)cbValue; // Ok
  }
  return 0; // Error
}


// AP: Tested Jan03 2006
void cmsdtdb::setcurrun(int nrun) {
  curr_run = nrun;
}

// Author: S. Ventura
// AP: Tested Jan03 2006
// Modified: AP, Apr19 2007
int cmsdtdb::insertnewrun(char *runtype, char *runcomment) {
  int da,db;
  if (SQL_SUCCEEDED(connct_rc)) {
    char qstr[256];
    sprintf(qstr,"INSERT INTO tbrun (run,type,runcomment) VALUES ('%d','%s','%s')",curr_run,runtype,runcomment) ;

    dblock(); // lock the mutex
    query_rc = sqlquery(qstr,&da,&db);
    dbunlock(); // unlock the mutex

    if (SQL_SUCCEEDED(query_rc))
      return curr_run;
  }

  return -1;
}


// Connect to DATABASE
// Author: A. Parenti, Jan03 2006
// Modified: AP, Mar29 2007 (Tested on MySQL/Oracle)
void cmsdtdb::connect() {
  char connectstr[256];
  int timeout=DB_TIMEOUT;
  SQLRETURN rc; // return code
  UCHAR info[STR_LEN]; // info string for SQLGetInfo
  SQLSMALLINT cbInfoValue;

  if (SQL_SUCCEEDED(connct_rc)) {
    return; // already connected
  }

  if ((time(NULL)-connct_time) < DB_MIN_RECONNET_TIME) {
    return; // Small time since last connection: return
  } else {
    connct_time=time(NULL); // Set to current time
  }

  if (_verbose_)
    printf("Connecting to database\n");

  try {

//    SQLAllocEnv(&henv); // Deprecated. Use instead:
    SQLAllocHandle(SQL_HANDLE_ENV,SQL_NULL_HANDLE,&henv);
    SQLSetEnvAttr(henv,SQL_ATTR_ODBC_VERSION,(void*)SQL_OV_ODBC3,0); // ODBC3

//    SQLAllocConnect(henv,&hdbc); // Deprecated. Use instead:
    SQLAllocHandle(SQL_HANDLE_DBC, henv, &hdbc);

    SQLSetConnectAttr(hdbc,SQL_LOGIN_TIMEOUT,(SQLPOINTER)(&timeout),0);
    SQLSetConnectAttr(hdbc,SQL_ATTR_CONNECTION_TIMEOUT,(SQLPOINTER)(&timeout),0);

//    rc = SQLConnect(hdbc, (SQLCHAR*)datasource, SQL_NTS,
//      (SQLCHAR*)user, SQL_NTS, (SQLCHAR*)passwd, SQL_NTS);// For ODBC>=3 use:
    if (strlen(user)!=0 && strlen(passwd)!=0) {
      sprintf(connectstr,"DSN=%s; UID=%s; PWD=%s",datasource,user,passwd);
    } else {
      sprintf(connectstr,"DSN=%s",datasource);
    }
    rc = SQLDriverConnect(hdbc,NULL,(SQLCHAR *)connectstr, SQL_NTS,
                          NULL, 0, NULL, SQL_DRIVER_NOPROMPT);

    if (SQL_SUCCEEDED(rc))
//      rc = SQLAllocStmt(hdbc, &hstmt); // Deprecated. Use instead:
      rc = SQLAllocHandle(SQL_HANDLE_STMT,hdbc,&hstmt);
    else { // Get ERROR
      SQLCHAR SQLState[6], MessageText[257];
      SQLSMALLINT TextLength;
      SQLINTEGER SQLCode;
      SQLGetDiagRec(SQL_HANDLE_DBC,hdbc,1,SQLState,&SQLCode,
                    MessageText,256,&TextLength);
      printf("Connection Error: %s\n", MessageText);
    }
  } catch (std::exception& e) {
    printf("Standard exception: %s\n",e.what());
  }

  if (SQL_SUCCEEDED(rc)) {
    if (_verbose_)
      printf("Connected!\n");

// Get DB info
    SQLGetInfo(hdbc,SQL_SERVER_NAME, &info, STR_LEN, &cbInfoValue);
    if (info[0]!=0 && _verbose_) printf("Server: %s\n", info);

    SQLGetInfo(hdbc,SQL_DBMS_NAME, &info, STR_LEN, &cbInfoValue);
    if (info[0]!=0 && _verbose_) printf("DB type: %s\n", info);
// Database type: MySQL or Oracle?
    if (strstr((char*)info,"MySQL")!=NULL)
      dbtype=mysql;
    else if (strstr((char*)info,"Oracle")!=NULL)
      dbtype=oracle;

    SQLGetInfo(hdbc,SQL_DBMS_VER, &info, STR_LEN, &cbInfoValue);
    if (info[0]!=0 && _verbose_) printf("DB version: %s\n", info);
  } else {
    printf("Error in connecting to DB. Returned code: %d\n",rc);
  }

  connct_rc = rc;
}


// Disconnect from DATABASE
// Author: A. Parenti, Jan 2006
// Modified/Tested on Oct29 2007
void cmsdtdb::disconnect() {
  try {
// Free handles: otherwise some memory remains unfreed
    SQLFreeHandle(SQL_HANDLE_STMT,hstmt);
    SQLDisconnect(hdbc); // Needed, otherwise connections are not closed!!!
    SQLFreeHandle(SQL_HANDLE_DBC,hdbc);
    SQLFreeHandle(SQL_HANDLE_ENV,henv);
//
    if (SQL_SUCCEEDED(connct_rc) && _verbose_)
      printf("Disconnected from database\n");

    connct_rc = SQL_NO_DATA;
  } catch (std::exception& e) {
    printf("Standard exception: %s\n",e.what());
  }
}


// Initialise variables
// Author: A. Parenti, Jan 2006
// Modified: AP, Jun26 2007
void cmsdtdb::init() {

  errcount=0; // No errors

// Reset return codes
  connct_rc = SQL_NO_DATA;
  query_rc = SQL_NO_DATA;

// Reset columns
  query_cols = query_rows = 0;

// Reset DB Type
  dbtype = unknown;
// Reset current run
//  curr_run=0;
}

// Read datasource from config file: /etc/ccbdb.conf
// Author: A. Parenti, Apr07 2006
// Modified: AP, Nov13 2006
void cmsdtdb::read_ds(char *datasource) {
  int c;
  std::string content, subcontent;
  FILE *fstream;

  fstream = fopen(DB_CONFIG_FILE, "r") ; // Open the file

  if (fstream!=NULL) { // File has been open
    while ((c=fgetc(fstream))!=EOF) // Loop while extraction from file is possible
      content += (char)c; // Get character from file, put it in <content>
    fclose(fstream); // Close file
  }

// Get datasource name, user name, password
  if (content.size()>0) {
    int i;

    if ((i=content.find("DATASOURCE=",0)) != std::string::npos) {
      subcontent = content.substr(i+11,content.find("\n",i)-i-11);
// Trim white spaces
      subcontent = subcontent.erase(subcontent.find_last_not_of(" ")+1);
      subcontent = subcontent.erase(0,subcontent.find_first_not_of(" "));
//
      strcpy(datasource,subcontent.c_str());
    }
  }
  //printf("Read from /etc/ccbdb.conf\n");
  //printf("DATASOURCE='%s'\n",datasource);
}

// Reset query results
// Author: A. Parenti, Jan04 2006
// Modified/Tested: AP, Oct30 2007
void cmsdtdb::resetqueryresults() {
  query_rc   = SQL_NO_DATA;
  query_cols = query_rows = 0;

// Discard results of the previous query:
  while (SQLMoreResults(hstmt)!=SQL_NO_DATA) {}
//// Following two lines replaced by SQLMoreResults... AP, Oct30 2007
//  SQLFreeHandle(SQL_HANDLE_STMT,hstmt);
//  connct_rc = SQLAllocHandle(SQL_HANDLE_STMT,hdbc,&hstmt);
}

// Cast a variable to char (the function is specific to DB)
// Author: A. Parenti, Jan 2006
char *cmsdtdb::to_char(char *varname) {
  static char strout[LEN1];

  switch(dbtype) {
  case mysql:
    sprintf(strout,"CAST(%s AS CHAR)",varname);
    break;
  case oracle:
    sprintf(strout,"TO_CHAR(%s)",varname);
    break;
  default:
    strcpy(strout,varname);
  }
  return strout;
}


// Get the value of a counter from a table
// Author: A. Parenti, Apr28 2006
// Modified: AP, Apr19 2007
int cmsdtdb::get_counter(char *CounterName, char *TableName){
  char mquer[LEN1], CntStr[20];
  int rows=0, fields=0, resu=0;
  long Cnt=0;

  dblock(); // lock the mutex

  sprintf(mquer,"SELECT MAX(%s) FROM %s",CounterName,TableName);
  resu=sqlquery(mquer,&rows,&fields);
  if (resu==0 && fetchrow()==SQL_SUCCESS) {
    returnfield(0,CntStr);
    sscanf(CntStr,"%d",&Cnt);
    Cnt = (Cnt>0)?Cnt:0; // MAX between Cnt and 0
  }

  dbunlock(); // unlock the mutex
  return Cnt;
}
