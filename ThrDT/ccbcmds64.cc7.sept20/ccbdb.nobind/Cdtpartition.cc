#include "Cdtpartition.h"
#include <cstring>

#define N_MAX_CCB 500

dtpartition::dtpartition() {
  dtdbobj = new cmsdtdb;
  newconn = true;
}

dtpartition::dtpartition(cmsdtdb* thisobj) {
  dtdbobj = thisobj;
  newconn = false;
}

dtpartition::~dtpartition() {
  if (newconn)
    delete dtdbobj;
}


// Select a partition
// AP, Jan05 2006: Tested
int dtpartition::select(char *thepart) {
  strcpy(partname,thepart);
  return 0;
}

int dtpartition::getDefault(char *thepart)
{
char mquer[FIELD_MAX_LENGTH];
  int rows=0, fields=0, resu=0;

  dtdbobj->dblock(); // lock the mutex
  sprintf(mquer,"select partname from dtpartitions,g2lrelations where g2lrelations.defa=1 and partkey=dtpart");
  resu=dtdbobj->sqlquery(mquer,&rows,&fields);

  if (resu==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {

    char myfie[1024];
      dtdbobj->returnfield(0,myfie);
      strcpy(partname,myfie);
      strcpy(thepart,myfie);
     printf("Default partition %s selected\n",thepart);
   dtdbobj->dbunlock(); // unlock the mutex
     return 0;
    }
   printf("No default partition defined!\n");
   dtdbobj->dbunlock(); // unlock the mutex
   return -1;
}

// Create a partition
// Author: AP, Apr28 2006
// Modified: AP, Apr04 2007 (Tested on ODBC: MySQL/Oracle)
int dtpartition::newdtpart(char *thepart, int defa) {
  char mquer[FIELD_MAX_LENGTH];
  int rows=0, fields=0, resu=0;

  if (strlen(thepart)<=0)
    return -1; // Invalid PartName

  dtdbobj->dblock(); // lock the mutex

  strcpy(partname,thepart);

// Return if partition already exists
  sprintf(mquer,"SELECT PartKey FROM dtpartitions WHERE PartName='%s'",partname);
  resu=dtdbobj->sqlquery(mquer,&rows,&fields);
  if (resu==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {
    printf("newdtpart: partition %s already exists!\n",partname);

    dtdbobj->dbunlock(); // unlock the mutex
    return 0;
  }

  sprintf(mquer,"INSERT INTO dtpartitions (Run,PartName,Defa) VALUES ('0','%s','%d')",partname,defa); 
  resu=dtdbobj->sqlquery(mquer,&rows,&fields);

  dtdbobj->dbunlock(); // unlock the mutex
  return resu;
}

// Insert a CCB in a partition
// Author: AP, Feb01 2006 (Tested on ODBC: MySQL-ORACLE)
// Modified: AP, Nov03 2006 (Tested on ODBC: MySQL/Oracle)
int dtpartition::insert(int ccb) {
  char mquer[FIELD_MAX_LENGTH];
  char PKey[20]="";
  int rows=0, fields=0, resu=0;

  dtdbobj->dblock(); // lock the mutex

// Find Partition Key
  sprintf(mquer,"SELECT PartKey FROM dtpartitions WHERE PartName='%s' ORDER BY PartDate DESC",partname); 
  resu = dtdbobj->sqlquery(mquer,&rows,&fields);
  if (resu==0 && dtdbobj->fetchrow()==SQL_SUCCESS)
    dtdbobj->returnfield(0,PKey);
  else {
    dtdbobj->dbunlock(); // unlock the mutex
    return -1;
  }
// Insert CcbID in dtpartition
  if (strlen(PKey)>0) {

    sprintf(mquer,"INSERT INTO dtrelations (PartKey,CcbID) VALUES ('%s','%d')",PKey,ccb); 
    resu = dtdbobj->sqlquery(mquer,&rows,&fields);

    dtdbobj->dbunlock(); // unlock the mutex
    return resu;
  } else {
    dtdbobj->dbunlock(); // unlock the mutex
    return -1;
  }
}

// Insert a CCB in a partition
// Author: AP, Feb01 2006
// Modified: AP, Jul05 2007 (Tested on MySQL/Oracle)
int dtpartition::insert(char *thepart, int nccb, int *ccblist) {
  char mquer[FIELD_MAX_LENGTH];
  int rows=0, fields=0, resu=0;
  int ii;

  select(thepart); // Select partition

  for(ii=0; ii<nccb; ii++) {
    resu+=insert(ccblist[ii]); // Insert each Ccb
  }

  return resu;
}

// Retrieve a list of partitions
// Author: A. Parenti, Feb05 2006
// Modified: S. Ventura, Apr05 2006
// Modified: AP, Oct29 2007 (Tested on MySQL/Oracle)
int dtpartition::retrieveparts(int *npart, char partlist[][32]) {
  int rows=0, fields=0, resu=0;
  *npart=0;

  dtdbobj->dblock(); // lock the mutex

  resu=dtdbobj->sqlquery("SELECT DISTINCT partname FROM dtpartitions  WHERE obso IS NULL OR obso>CURRENT_TIMESTAMP ORDER BY partname",&rows,&fields);
  if (resu==0) {
    char myfie[1024];
    int i=0;
    while (dtdbobj->fetchrow()==SQL_SUCCESS) {
      dtdbobj->returnfield(0,myfie);
      strcpy(&partlist[i][0],myfie);
      ++i;
    }
    *npart=i;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return resu;
}


// Retrieve CCBs forming a partition
// Author: AP, Feb01 2006
// Modified: AP, Jul27 2006 (added new output portlist)
int dtpartition::retrieveccbs(char *thepart, int *nccb, int *ccblist, int *portlist, int *secportlist, char ccbserver[][255]) {
  strcpy(partname,thepart);
  return retrieveccbs(nccb,ccblist,portlist,secportlist,ccbserver);
}


// Retrieve CCBs forming a partition
// Author: AP, Feb02 2006
// Modified: AP, Apr04 2007 (Tested on MySQL/Oracle)
int dtpartition::retrieveccbs(int *nccb, int *ccblist, int *portlist, int *secportlist, char ccbserver[][255]) {
  int i, rows=0, fields=0, resu=0;
  char mquer[LEN2];

  dtdbobj->dblock(); // lock the mutex

  *nccb=0;

// Find Partition
  sprintf(mquer,"SELECT ccbmap.ccbid,ccbmap.port,ccbmap.secport,ccbmap.ccbserver FROM ccbmap,dtrelations,dtpartitions WHERE dtrelations.PartKey=dtpartitions.PartKey AND dtpartitions.PartName='%s' AND ccbmap.ccbid=dtrelations.ccbid ORDER by ccbmap.wheel,ccbmap.sector,ccbmap.station",partname);
  resu=dtdbobj->sqlquery(mquer,&rows,&fields);

// Read CCB ports
  if (resu==0) {
    char myfie[LEN1];

    i=0;
    while (dtdbobj->fetchrow()==SQL_SUCCESS) {
      dtdbobj->returnfield(0,myfie);
      sscanf(myfie,"%d",&(ccblist[i]));

      dtdbobj->returnfield(1,myfie);
      sscanf(myfie,"%d",&(portlist[i]));

      dtdbobj->returnfield(2,myfie);
      sscanf(myfie,"%d",&(secportlist[i]));

      dtdbobj->returnfield(3,ccbserver[i]);

      ++i;
    }
    *nccb=i;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return resu;
}


// Author: AP, Oct10 2007 (For backward compatibility)
int dtpartition::retrieveccbs(char *thepart, int *nccb, int *ccblist, int *portlist, char ccbserver[][255]) {
  strcpy(partname,thepart);
  return retrieveccbs(nccb,ccblist,portlist,ccbserver);
}

// Author: AP, Oct10 2007 (For backward compatibility)
int dtpartition::retrieveccbs(int *nccb, int *ccblist, int *portlist, char ccbserver[][255]) {
  int secportlist[N_MAX_CCB];
  return retrieveccbs(nccb,ccblist,portlist,secportlist,ccbserver);
}

// Author: AP, Jul27 2006 (For backward compatibility)
int dtpartition::retrieveccbs(char *thepart, int *nccb, int *ccblist, int *portlist) {
  strcpy(partname,thepart);
  return retrieveccbs(nccb,ccblist,portlist);
}

// Author: AP, Jul27 2006 (For backward compatibility)
int dtpartition::retrieveccbs(int *nccb, int *ccblist, int *portlist) {
  char ccbserver[N_MAX_CCB][255];
  return retrieveccbs(nccb,ccblist,portlist,ccbserver);
}

// Author: AP, Jul27 2006 (For backward compatibility)
int dtpartition::retrieveccbs(char *thepart, int *nccb, int *ccblist) {
  strcpy(partname,thepart);
  return retrieveccbs(nccb,ccblist);
}

// Author: AP, Jul27 2006 (For backward compatibility)
int dtpartition::retrieveccbs(int *nccb, int *ccblist) {
  int portlist[N_MAX_CCB];
  return retrieveccbs(nccb,ccblist,portlist);
}
