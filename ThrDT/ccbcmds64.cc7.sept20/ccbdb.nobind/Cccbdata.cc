#include "Cccbdata.h"

#include <cstring>

ccbdata::ccbdata() {
  dtdbobj = new cmsdtdb;
  newconn = true;
}

ccbdata::ccbdata(cmsdtdb* thisobj) {
  dtdbobj=thisobj;
  newconn = false;
}

ccbdata::~ccbdata() {
  if (newconn)
    delete dtdbobj;
}


// Author: A.Parenti, Nov03 2006
// Modified: AP, May16 2007 (Tested on Oracle/MySQL)
int ccbdata::insert(int ccbID,int CmdCode,char *data,int datasize) {
  char mquer[FIELD_MAX_LENGTH], mhex[FIELD_MAX_LENGTH];
  int rows=0, fields=0, resu=0;
  int ii;
  
  mhex[0]=0;
  for (ii=0; ii<datasize; ii++)
    sprintf(mhex,"%s%02hhx",mhex,data[ii]);

  sprintf(mquer,"INSERT INTO ccbdata (RunID,ccbID,CmdCode,Data,time) VALUES (%d,%d,%d,'0x%s',CURRENT_TIMESTAMP)",dtdbobj->curr_run,ccbID,CmdCode,mhex);
//  printf("%s\n",mquer);

  dtdbobj->dblock(); // lock the mutex
  resu=dtdbobj->sqlquery(mquer,&rows,&fields);
  dtdbobj->dbunlock(); // unlock the mutex

  return resu;
}

// Author: A.Parenti, Nov03 2006
// Modified: Apr04,2007 (Tested on MySQL/Oracle)
int ccbdata::retrievelast(int ccbID,char *data,char *time) {
  char mquer[FIELD_MAX_LENGTH];
  int rows=0, fields=0, reso;

  dtdbobj->dblock(); // lock the mutex

  sprintf(mquer,"SELECT Data,time FROM ccbdata WHERE ccbID=%d ORDER BY time DESC",ccbID);
//  printf("%s\n",mquer);
  reso = dtdbobj->sqlquery(mquer,&rows,&fields);

  if (reso==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {
    dtdbobj->returnfield(0,data);
    dtdbobj->returnfield(1,time);

    dtdbobj->dbunlock(); // unlock the mutex
    return 1;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return 0;
}


// Author: A.Parenti, Nov03 2006
// Modified: Apr04,2007 (Tested on MySQL/Oracle)
int ccbdata::retrievelast(int ccbID,int CmdCode,char *data,char *time) {
  char mquer[FIELD_MAX_LENGTH];
  int rows=0, fields=0, reso;

  dtdbobj->dblock(); // lock the mutex

  sprintf(mquer,"SELECT Data,time FROM ccbdata WHERE ccbID=%d AND CmdCode=%d ORDER BY time DESC",ccbID,CmdCode);
//  printf("%s\n",mquer);
  reso = dtdbobj->sqlquery(mquer,&rows,&fields);

  if (reso==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {
    dtdbobj->returnfield(0,data);
    dtdbobj->returnfield(1,time);

    dtdbobj->dbunlock(); // unlock the mutex
    return 1;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return 0;
}
