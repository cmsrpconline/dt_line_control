#!/bin/tcsh -f

# Read a PADC pedestal file, create the script to feed it into sql DB
# Author: A.Parenti, Oct10 2008

if (${#argv} != 1) then
  echo "Usage: $0 <PADC pedestal file>"
  echo "The script creates the sql code to fill DB from a PADC pedestal file"
  echo "The pedestal file must have 5 columns like:\n    ped100_HV ped500_HV ped100_FE ped500_FE ccbid"
  exit
endif

if (! -e ${1}) then
  echo "PADC pedestal file $1 doesn't exist"
  exit
endif

set filein=$1
set fileout=`echo $filein | awk '(sub("txt","sql"))'`
set nlines=`wc -l $1 | awk '{print $1}'`

if (-e $fileout) then
  mv -f $fileout $fileout.bak
endif

@ i=1
set AdcCount=9999
set sensHV_Vcc=0
set sensFE_Vcc=0
set PADC_Vcc=0
set PADC_Vdd=0
set PadcDataId=9999

while ($i <= $nlines)
  set rline=`head -$i $filein | tail -1`

# NB pedestals are expressed in mbar, we want them in bar 
  set sens100_HV=`echo $rline | awk '{print $1/1000}'`
  set sens500_HV=`echo $rline | awk '{print $2/1000}'`
  set sens100_FE=`echo $rline | awk '{print $3/1000}'`
  set sens500_FE=`echo $rline | awk '{print $4/1000}'`
#
  set ccbid=`echo $rline | awk '{print $5}'`

  echo INSERT INTO padcdata \(PadcDataId,AdcCount,sens100A_HV,sens100B_HV,sens500_HV,sensHV_Vcc,sens100A_FE,sens100B_FE,sens500_FE,sensFE_Vcc,PADC_Vcc,PADC_Vdd\) VALUES \(${PadcDataId},${AdcCount},${sens100_HV},${sens100_HV},${sens500_HV},${sensHV_Vcc},${sens100_FE},${sens100_FE},${sens500_FE},${sensFE_Vcc},${PADC_Vcc},${PADC_Vdd}\)';'>> $fileout
  echo UPDATE padcdata SET PadcDataId=\(SELECT MAX\(padcrelations.PadcDataId\) FROM padcrelations,ccbmap WHERE padcrelations.PadcId=ccbmap.PadcId AND ccbmap.ccbid=${ccbid}\) WHERE PadcDataId=${PadcDataId}';'>> $fileout
  @ i++
end
