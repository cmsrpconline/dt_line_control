-- WHEEL 0 --
-- AP, last updated Oct10 2007 --

-- SECTOR 1 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 92,0,1,1,0,1092,27);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (376,0,1,2,0,1376,26);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (360,0,1,3,0,1360,51);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (280,0,1,4,0,1280, 7);

-- SECTOR 2 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (204,0,2,1,0,1204,18);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (261,0,2,2,0,1261,29);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (154,0,2,3,0,1154,29);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 31,0,2,4,0,1031, 3);

-- SECTOR 3 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (160,0,3,1,0,1160,30);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (275,0,3,2,0,1275,17);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (199,0,3,3,0,1199,23);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (173,0,3,4,0,1173, 8);

-- SECTOR 4 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (323,0,4,1,0,1323,17);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (103,0,4,2,0,1103,16);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (109,0,4,3,0,1109,47);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (179,0,4,4,0,1179, 4);

-- SECTOR 5 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (112,0,5,1,0,1112,11);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (117,0,5,2,0,1117,14);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (144,0,5,3,0,1144,45);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 73,0,5,4,0,1073, 4);

-- SECTOR 6 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (104,0,6,1,0,1104,28);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (159,0,6,2,0,1159,18);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 98,0,6,3,0,1098,38);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (244,0,6,4,0,1244, 3);

-- SECTOR 7 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (382,0,7,1,0,2382,11);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (156,0,7,2,0,2156,21);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (368,0,7,3,0,2368,49);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (161,0,7,4,0,2161, 3);

-- SECTOR 8 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (120,0,8,1,0,2120, 4);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (203,0,8,2,0,2203, 5);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (251,0,8,3,0,2251,16);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (218,0,8,4,0,2218, 1);

-- SECTOR 9 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (132,0,9,1,0,2132, 2);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (224,0,9,2,0,2224, 1);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (106,0,9,3,0,2106,43);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (372,0,9,4,0,2372, 1);

-- SECTOR 10 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (167,0,10,1,0,2167,23);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (153,0,10,2,0,2153,20);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (169,0,10,3,0,2169,17);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (126,0,10,4,0,2126, 2);

-- SECTOR 11 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (157,0,11,1,0,2157,29);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (190,0,11,2,0,2190,23);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (192,0,11,3,0,2192,32);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (124,0,11,4,0,2124, 3);

-- SECTOR 12 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 93,0,12,1,0,2093, 6);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (210,0,12,2,0,2210, 4);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (200,0,12,3,0,2200,31);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (229,0,12,4,0,2229, 2);

-- SECTOR 13 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (245,0,13,4,0,1245, 3);

-- SECTOR 14 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (121,0,14,4,0,2121, 1);
