#!/bin/tcsh -f

# Read a DAC Calibration file, create the script to feed it into sql DB
# Author: A.Parenti, Jul20 2007
# Modified: 

echo "Usage: $0 [mysql|oracle [<calibration_file>]]"

if ($#argv < 1) then
  echo 'No DB type passed. Using Oracle.'
  set dbtype='oracle'
else
  set dbtype=$1
endif

if ($#argv < 2) then
  echo 'No filename passed on commad line. Using CalDac_DB.txt'
  set fname="CalDac_DB.txt"
else
  set fname=$2
endif

if (!(-e $fname)) then
  echo "File" $fname "doesn't exist. Quitting..."
  exit
endif

if ($dbtype == 'oracle') then
  set month=`ls -l $fname | awk '{print $6}'`
  set mday=`ls -l $fname | awk '{print $7}'`
  set year=`ls -l $fname | awk '{print substr($8,3,2)}'`

  set datetime="${mday}-${month}-${year}"
else if ($dbtype == 'mysql') then
  set datetime=`ls --full-time $fname | awk '{print $6}'`
else
  echo 'Allowed DB types: oracle, mysql. Quitting...'
  exit
endif

set fileout=`echo $fname | awk 'sub("[.].+$",".sql")'`
if (-e $fileout) then
  mv -f $fileout $fileout.bak
endif

set lines=`wc -l ${fname} | awk '{print $1}'`
@ i=1

if (-e $fileout) then
  mv -f $fileout $fileout.bak
endif

while (${i} < ${lines})
  @ i++

  set theline=`head -$i $fname | tail -1`
  set ttcid=`echo $theline |awk '{print $1}' |awk '{gsub("[.].+$",""); print}'`
  set othrs=`echo $theline |awk '{gsub("[\n\r\f]",""); print $2 "," $3 "," $4 "," $5 "," $6 "," $7 "," $8 "," $9 "," $10 "," $11 "," $12 "," $13 "," $14 "," $15}'`

  echo "INSERT INTO daccalibs (TTCid,width_gain,bias_gain1,bias_gain2,bias_gain3,thr_gain1,thr_gain2,thr_gain3,width_ofst,bias_ofst1,bias_ofst2,bias_ofst3,thr_ofst1,thr_ofst2,thr_ofst3,Datetime) VALUES (" $ttcid "," $othrs ",'" $datetime "');" >> $fileout

end
