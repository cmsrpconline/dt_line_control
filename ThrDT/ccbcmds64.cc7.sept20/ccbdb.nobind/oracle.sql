-- This script creates cmsdtdb tables in ORACLE

/* LIST TABLES */
SELECT TABLE_NAME FROM USER_TABLES;

/* DROP TABLES */
--DROP TABLE ccbmap;
--DROP TABLE ccbstatus;
--DROP TABLE ccbdata;
--DROP TABLE refrelations;
--DROP TABLE ccbref;
--DROP TABLE configsets;
--DROP TABLE ccbrelations;
--DROP TABLE configs;
--DROP TABLE cfgautorel;
--DROP TABLE cfgrelations;
--DROP TABLE configcmds;
--DROP TABLE daqconfigs;
--DROP TABLE daqpartition;
--DROP TABLE daqrelations;
--DROP TABLE dcsdaqstatus;
--DROP TABLE dtpartitions;
--DROP TABLE dtrelations;
--DROP TABLE logger;
--DROP TABLE runfiles;
--DROP TABLE tbrun;
--DROP TABLE padcrelations;
--DROP TABLE padcdata;
--DROP TABLE padcstatus;


-- A.P. Aug09 2006
CREATE TABLE ccbmap (
  CcbID NUMBER(7),
  ch_id_ofl NUMBER(7), -- Chamber ID in offline DB
  wheel NUMBER(7) NOT NULL,
  sector NUMBER(7) NOT NULL,
  station NUMBER(7) NOT NULL,
  chamber NUMBER(7) DEFAULT '0',
  minicrate NUMBER(7) DEFAULT '0',
  on_line NUMBER(3) DEFAULT '0' NOT NULL,
  port NUMBER(7) NOT NULL,
  ccbserver VARCHAR2(255) DEFAULT '127.0.0.1' NOT NULL,
  PadcId NUMBER(7) DEFAULT '0',
  cmnt VARCHAR2(255) DEFAULT '',
  CONSTRAINT ccbmap_pk PRIMARY KEY (ccbID),
  CONSTRAINT ccbmap_uq UNIQUE (wheel,sector,station)
--  CONSTRAINT ccbmap_uq2 UNIQUE (port,ccbserver) -- useful or not?
);

-- A.P. Mar21 2006
CREATE TABLE ccbstatus (
  RunID NUMBER(10) NOT NULL,
  ccbID NUMBER(7) NOT NULL,
  statusstruct CLOB NOT NULL,
  statusxml VARCHAR2(1024),
  time TIMESTAMP(0) DEFAULT CURRENT_TIMESTAMP
);
--  time TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP -- not supported by Oracle

-- A.P. Nov03 2006
CREATE TABLE ccbdata (
  RunID NUMBER(10) NOT NULL,
  ccbID NUMBER(7) NOT NULL,
  CmdCode VARCHAR2(12) NOT NULL,
  Data CLOB NOT NULL,
  time TIMESTAMP(0) DEFAULT CURRENT_TIMESTAMP
);

-- AP Apr28 2006
-- SV Jun08 2009
CREATE TABLE refrelations (
  CcbID NUMBER(7),
  bootrefid NUMBER(10) DEFAULT '0',
  statusrefid NUMBER(10) DEFAULT '0',
  testrefid NUMBER(10) DEFAULT '0',
  CONSTRAINT refrelations_pk PRIMARY KEY (CcbID),
  CONSTRAINT refrelations_fk1 FOREIGN KEY(CcbID) REFERENCES CCBMAP(CcbID)
);

-- A.P. Jan18 2006
CREATE TABLE ccbref (
  refid NUMBER(10),
  mctype NUMBER(1) DEFAULT NULL CHECK (mctype BETWEEN 1 AND 7),
  reftype CHAR(6) CHECK (reftype IN ('boot','status','test')),
  refxml1 CLOB,
  refxml2 CLOB,
  refxml3 CLOB,
  cmnt VARCHAR2(255) DEFAULT '',
  CONSTRAINT ccbref_pk PRIMARY KEY (refid)
);

-- AP after Luca Ciano Apr12 2006
CREATE TABLE configsets (
  ConfKey NUMBER(10),
  Defa NUMBER(3) NOT NULL,
  Name VARCHAR2(64) NOT NULL,
  Cmnt CLOB NOT NULL,
  Run NUMBER(10) DEFAULT '0',
  ConfDate TIMESTAMP(0) DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT configsets_pk PRIMARY KEY (ConfKey)
);

-- AP after Luca Ciano Apr28 2006
CREATE TABLE ccbrelations (
  ID NUMBER(10),
  CcbID NUMBER(7) NOT NULL,
  ConfKey NUMBER(10) NOT NULL,
  ConfCcbKey NUMBER(10) NOT NULL,
  CONSTRAINT ccbrelations_pk PRIMARY KEY (ID),
  CONSTRAINT ccbrelations_uq UNIQUE (CcbID,ConfKey)
);

-- A.P. Jan20 2006
CREATE TABLE configs (
  ConfID NUMBER(10),
  ConfName VARCHAR2(64) NOT NULL UNIQUE,
  ConfDate TIMESTAMP(0) DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT configs_pk PRIMARY KEY (ConfID)
);
--   ConfID auto_increment -- Done in ccbdb library
--   confdate ON UPDATE CURRENT_TIMESTAMP -- How to do in Oracle ??

-- Oct23 2006
CREATE TABLE cfgautorel (
  ID NUMBER(10),
  ConfKeyF NUMBER(10) NOT NULL,
  ConfKeyS NUMBER(10) NOT NULL,
  CONSTRAINT cfgautorel_pk PRIMARY KEY  (ID),
  CONSTRAINT cfgautorel_uq UNIQUE (ConfKeyF,ConfKeyS)
);

-- Oct23 2006
CREATE TABLE cfgrelations (
  ID NUMBER(10),
  ConfID NUMBER(10) NOT NULL,
  CmdID NUMBER(20) NOT NULL,
  CONSTRAINT cfgrelations_pk PRIMARY KEY (ID),
  CONSTRAINT cfgrelations_uq UNIQUE (ConfID,CmdID)
);

-- A.P. Nov02 2006
CREATE TABLE configcmds (
  cmdid NUMBER(20),
  CmdName VARCHAR2(32) DEFAULT '',    
  ConfToken VARCHAR2(64) NOT NULL,
  ConfData CLOB NOT NULL,
  CONSTRAINT configcmds_pk PRIMARY KEY (cmdid)
);
--  cmdid AUTO_INCREMENT -- Done in ccbdb library
--  KEY `ConfToken` (`ConfToken`) -- How to do in Oracle ?

-- AP Apr12 2006
CREATE TABLE daqconfigs (
  ID NUMBER(10),
  CmsID NUMBER(11) NOT NULL,
  ConfName VARCHAR2(32) NOT NULL,
  ConfData CLOB NOT NULL,
  CONSTRAINT daqconfigs_pk PRIMARY KEY (ID)
);

-- AP after Luca Ciano Apr12 2006
CREATE TABLE daqpartition (
  DaqPartitionID NUMBER(10),
  PartName VARCHAR2(32) NOT NULL,
  CmsID NUMBER(10) NOT NULL,
  CONSTRAINT daqpartition_pk PRIMARY KEY (DaqPartitionID)
);

-- AP after Luca Ciano Apr12 2006
CREATE TABLE daqrelations (
  ID NUMBER(10),
  ConfKey NUMBER(10) NOT NULL,
  ConfDaqKey NUMBER(10) NOT NULL,
  CONSTRAINT daqrelations_pk PRIMARY KEY (ID)
);

-- A.P. Jan20 2006
CREATE TABLE dcsdaqstatus (
  lastupdate TIMESTAMP(0) DEFAULT CURRENT_TIMESTAMP,
  dtpart VARCHAR2(32) NOT NULL,
  daqpart VARCHAR2(32) NOT NULL,
  dtconfig VARCHAR2(32) NOT NULL,
  status NUMBER(3) DEFAULT '0'
);
--  lastupdate on update CURRENT_TIMESTAMP -- How to do in Oracle ??

-- AP after Luca Ciano Apr17 2006
CREATE TABLE dtpartitions (
  PartKey NUMBER(10),
  Run NUMBER(10) DEFAULT '0',
  PartName VARCHAR2(32) NOT NULL,
  PartDate TIMESTAMP(0) DEFAULT CURRENT_TIMESTAMP,
  Defa NUMBER(3) NOT NULL,
  CONSTRAINT dtpartitions_pk PRIMARY KEY (PartKey),
  CONSTRAINT dtpartitions_uq UNIQUE (PartName)
);

-- AP after Luca Ciano Apr12 2006
CREATE TABLE dtrelations (
  ID NUMBER(10) NOT NULL,
  PartKey NUMBER(10) NOT NULL,
  CcbID NUMBER(10) NOT NULL,
  CONSTRAINT dtrelations_pk PRIMARY KEY (ID),
  CONSTRAINT dtrelations_uq UNIQUE (PartKey,CcbID)
);

-- A.P. Jan20 2006
CREATE TABLE logger (
  msgid NUMBER(20),
  datetime TIMESTAMP(0) DEFAULT CURRENT_TIMESTAMP,
  levl CHAR(4) DEFAULT 'mesg' CHECK (levl IN ('mesg','warn','erro')),
  body CLOB NOT NULL,
  runid NUMBER(10) DEFAULT '0',
  CONSTRAINT logger_pk PRIMARY KEY (msgid)
);
-- "level" doesn't work as a name
--  msgid auto_increment -- How to do in Oracle?
--  datetime on update CURRENT_TIMESTAMP -- How to do in Oracle?
--  KEY datetime (datetime)
--  KEY runid (runid)

-- A.P. Dec23 2005
CREATE TABLE runfiles (
  run NUMBER(10) DEFAULT '0',
  filename VARCHAR2(255) NOT NULL,
  evts NUMBER(10) NOT NULL,
  writerid NUMBER(7) NOT NULL
);
-- KEY run (run)

-- AP after Luca Ciano Apr12 2006
CREATE TABLE tbrun (
  run NUMBER(10) DEFAULT '0',
  ConfKey NUMBER(10) NOT NULL,
  PartKey NUMBER(10) NOT NULL,
  runtimestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  type VARCHAR2(40),
  runcomment VARCHAR2(40),
  CONSTRAINT tbrun_pk PRIMARY KEY (run)
);

-- A.P. Aug01 2006
CREATE TABLE padcrelations (
  PadcId NUMBER(7),
  ManifoldFeId NUMBER(7) NOT NULL,
  ManifoldHvId NUMBER(7) NOT NULL,
  Datetime TIMESTAMP(0),
  PadcDataId NUMBER(7),
  CONSTRAINT padcrelations_pk PRIMARY KEY (PadcId,Datetime),
  CONSTRAINT padcrelations_uq UNIQUE (PadcDataId)
);

-- A.P. Aug01 2006
CREATE TABLE padcdata (
  PadcDataId NUMBER(7),
  AdcCount NUMBER(7),
  sens100A_HV NUMBER,
  sens100B_HV NUMBER,
  sens500_HV NUMBER,
  sensHV_Vcc NUMBER,
  sens100A_FE NUMBER,
  sens100B_FE NUMBER,
  sens500_FE NUMBER,
  sensFE_Vcc NUMBER,
  PADC_Vcc NUMBER,
  PADC_Vdd NUMBER,
  CONSTRAINT padcdata_pk PRIMARY KEY (PadcDataId,AdcCount)
);

-- A.P. Oct09 2006
CREATE TABLE padcstatus (
  padcID NUMBER(7) NOT NULL,
  adccount VARCHAR2(256) NOT NULL,
  statusxml VARCHAR2(1024),
  time TIMESTAMP(0) DEFAULT CURRENT_TIMESTAMP
);


/* DESCRIBE TABLES */
--DESCRIBE ccbmap;
--DESCRIBE ccbstatus;
--DESCRIBE ccbdata;
--DESCRIBE ccbref;
--DESCRIBE refrelations;
--DESCRIBE configs;
--DESCRIBE ccbrelations;
--DESCRIBE configs;
--DESCRIBE cfgautorel;
--DESCRIBE cfgrelations;
--DESCRIBE configcmds;
--DESCRIBE daqconfigs;
--DESCRIBE daqpartition;
--DESCRIBE daqrelations;
--DESCRIBE dcsdaqstatus;
--DESCRIBE dtpartitions;
--DESCRIBE dtrelations;
--DESCRIBE logger;
--DESCRIBE runfiles;
--DESCRIBE tbrun;
--DESCRIBE padcrelations;
--DESCRIBE padcdata;
--DESCRIBE padcstatus;


/* LIST TABLES */
SELECT TABLE_NAME FROM USER_TABLES;
