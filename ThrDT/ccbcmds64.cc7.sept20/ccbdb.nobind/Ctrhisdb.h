#ifndef _trhisdb_
#define _trhisdb_
#include "Ctrhisdb.h"

class trhisdb {

 public:
  trhisdb(); // Create a new connection to DB
  trhisdb(cmsdtdb* dtdbobj); // Use an existing connection to DB
  ~trhisdb();

  int insert(int offset, int counts); // Insert histo entry. Input: offset,counts
  int retrieveBx(int theBx, char *sttime); // Retrieve Bx counts nearest to timestamp. Input: ccbID
  int retrieveOrbit(char *sttime,int *countArr); // Input: ccbID


 private:
  bool newconn; // True if a new connection to DB is open
  cmsdtdb* dtdbobj;
};

#endif
