#ifndef _ccbstatusdb_
#define _ccbstatusdb_
#include "Ccmsdtdb.h"

class ccbstatus {

 public:
  ccbstatus(); // Create a new connection to DB
  ccbstatus(cmsdtdb* dtdbobj); // Use an existing connection to DB
  ~ccbstatus();

  int insert(char *mst); // Insert ccb status in ccbstatus. Input: statusstruct
  int insert(char *mst, char *statusxml); // Input: statusstruct, statusxml
  int retrievelast(int ccbID, char *mst,char *sttime); // Retrieve last ccbstatus. Input: ccbID
  int retrievelast(int ccbID, char *mst, char *statusxml,char *sttime); // Input: ccbID

  int rob_insert(int ccbID, unsigned char rob_error_state[7], float mc_temp_rob[7], float read_rob_pwr_Vcc[7], float read_rob_pwr_Vdd[7], float read_rob_pwr_current[7]); // Insert ROB status in robstatus
  int rob_retrievelast(int ccbID, unsigned char rob_error_state[7], float mc_temp_rob[7], float read_rob_pwr_Vcc[7], float read_rob_pwr_Vdd[7], float read_rob_pwr_current[7],char *sttime); // Retrieve last ROB status. Input: ccbID

 private:
  bool newconn; // True if a new connection to DB is open
  cmsdtdb* dtdbobj;
};

#endif
