#include "Cccbconfdb.h"
//#define __USE_XOPEN // A.P. needed on some linuxes
#include <cstring>
#include <cstdlib>

ccbconfdb::ccbconfdb() {
  dtdbobj = new cmsdtdb;
  ConfKey = 0;
  newconn = true;
}

ccbconfdb::ccbconfdb(cmsdtdb* thisobj) {
  dtdbobj=thisobj;
  ConfKey = 0;
  newconn = false;
}

ccbconfdb::~ccbconfdb() {
  if (newconn)
    delete dtdbobj;
}


// Set <Run> in "configsets.Run" and cmsdtdb obj
// Authors: S.Ventura & A.Parenti
// Modified: AP, Apr18 2007 (Tested on MySQL/Oracle)
int ccbconfdb::updaterun(int nrun) {
  char mquer[LEN2];
  int rows=0, fields=0, resu=0;

  if (ConfKey<=0) return -1;

  dtdbobj->curr_run=nrun; // Set RunNr in cmsdtdb obj

  sprintf(mquer,"UPDATE configsets SET Run='%d' WHERE ConfKey='%d'", nrun, ConfKey);

  dtdbobj->dblock(); // lock the mutex
  resu=dtdbobj->sqlquery(mquer,&rows,&fields);
  dtdbobj->dbunlock(); // unlock the mutex

  return resu;
}

// Select a configuration by using "configsets.Name"
// Authors: S.Ventura & A.Parenti
// Modified: AP, Apr03 2007 (Tested on MySQL/ORACLE)
int ccbconfdb::confselect(char *confname) {
  char mquer[LEN2], myfie[LEN1];
  int rows=0, fields=0, resu=0;

  for(int kk=0; kk<30; kk++)
   {
	   printf("confname %d : %x \n",kk,confname[kk]);
	}
  dtdbobj->dblock(); // lock the mutex

  sprintf(mquer,"SELECT ConfKey FROM configsets WHERE Name='%s' ORDER BY confdate DESC", confname);
  printf("Confselect: doing query with %s \n",mquer);

  resu=dtdbobj->sqlquery(mquer,&rows,&fields);

  printf("Confselect: done query with %s rows %d fields %d\n",mquer,rows,fields);
  if (resu!=0 || dtdbobj->fetchrow() != SQL_SUCCESS) {
    printf("WARNING! No config %s was found!!!\n",confname);


    dtdbobj->dbunlock(); // unlock the mutex
    return (ConfKey=0);
  }

  printf("Config %s selected!!!\n",confname);

  dtdbobj->returnfield(0,myfie);
  sscanf(myfie,"%d",&ConfKey);

  dtdbobj->dbunlock(); // unlock the mutex
  return ConfKey;
}


int ccbconfdb::getDefault(char *theconf,int type)
{
char mquer[FIELD_MAX_LENGTH];
  int rows=0, fields=0, resu=0;

  dtdbobj->dblock(); // lock the mutex
  sprintf(mquer,"SELECT name from CONFIGSETS,g2lrelations,G2LCONFIGSETS where g2lrelations.defa>0 and G2LCONFIGSETS.G2LID=g2lrelations.id AND G2LCONFIGSETS.CSETTYPEID='%d' AND CONFIGSETS.CONFKEY=G2LCONFIGSETS.CONFKEY",type);
  resu=dtdbobj->sqlquery(mquer,&rows,&fields);

  if (resu==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {

    char myfie[1024];
      dtdbobj->returnfield(0,myfie);
      strcpy(theconf,myfie);
     printf("Default conf %s for type %d selected\n",theconf,type);
   dtdbobj->dbunlock(); // unlock the mutex
     return 0;
    }
   strcpy(theconf,"");
   printf("No default conf for type %d defined!\n",type);
   dtdbobj->dbunlock(); // unlock the mutex
   return -1;
}

int ccbconfdb::getConfg2l(char *globkey,char *theconf,int type)
{
char mquer[FIELD_MAX_LENGTH];
  int rows=0, fields=0, resu=0;

  dtdbobj->dblock(); // lock the mutex
  sprintf(mquer,"SELECT name from CONFIGSETS,g2lrelations,G2LCONFIGSETS where g2lrelations.globalkey='%s' and G2LCONFIGSETS.G2LID=g2lrelations.id AND G2LCONFIGSETS.CSETTYPEID='%d' AND CONFIGSETS.CONFKEY=G2LCONFIGSETS.CONFKEY",globkey,type);
  resu=dtdbobj->sqlquery(mquer,&rows,&fields);

  if (resu==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {

    char myfie[1024];
      dtdbobj->returnfield(0,myfie);
      strcpy(theconf,myfie);
     printf("Default conf %s for type %d selected\n",theconf,type);
   dtdbobj->dbunlock(); // unlock the mutex
     return 0;
    }
   strcpy(theconf,"");
   printf("No default conf for type %d defined!\n",type);
   dtdbobj->dbunlock(); // unlock the mutex
   return -1;
}

/*
int ccbconfdb::retrievecrc(char *theconf,short ccbid)
{

  char mquer[FIELD_MAX_LENGTH];
  int rows=0, fields=0, resu=0, merro=0;

  dtdbobj->dblock(); // lock the mutex
	
	for (int csettype=1;csettype<4;csettype++)
	{	
    sprintf(mquer,"SELECT crc,ccb_crc.ccbid,ccbrelations.confkey,ccbrelations.confccbkey,ccb_crc.CSETTYPEID from ccb_crc,g2lrelations,G2LCONFIGSETS,ccbrelations where g2lrelations.globalkey='%s' and G2LCONFIGSETS.G2LID=g2lrelations.id and ccbrelations.confkey=G2LCONFIGSETS.CONFKEY AND CCB_CRC.ccbid='%d' AND CCB_CRC.ccbid=ccbrelations.ccbid and ccb_crc.CSETTYPEID=g2lconfigsets.CSETTYPEID and ccb_crc.CSETTYPEID='%d'",theconf,ccbid,csettype);


	  resu=dtdbobj->sqlquery(mquer,&rows,&fields);

     if (resu==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {

     char myfie[1024];
      dtdbobj->returnfield(0,myfie);
	  if (csettype==1)
		  strcpy(crcfe,myfie);
	  else if (csettype==2) 
		  strcpy(crcdaq,myfie);
	  else if (csettype==3) 
		  strcpy(crctrig,myfie);
	 }
	 else {merro=1;
	 	 break;
	 }
	 }
	
     if (merro)
	 {printf("crc's for glob key %s and ccbid %d  selected %s %s %s\n",theconf,ccbid,crcfe,crcdaq,crctrig);
     dtdbobj->dbunlock(); // unlock the mutex
     return 0;
    }
   printf("No crc's for conf %d ccbid %d defined!\n",theconf,ccbid);
   dtdbobj->dbunlock(); // unlock the mutex
   return -1;

}
*/

int ccbconfdb::retrievecrc(short ccbid,short csettype,char *theconf,char *theconf2,char *theconf3,short *crc)
{
	
	char mquer[FIELD_MAX_LENGTH];
	int rows=0, fields=0, resu=0;
	
	dtdbobj->dblock(); // lock the mutex
	
	if (csettype<3)
	sprintf(mquer,"SELECT crc,ccb_crc.ccbid,ccbrelations.confkey,ccbrelations.confccbkey,ccb_crc.CSETTYPEID from ccb_crc,ccbrelations,configs where configs.confname='%s' and ccb_crc.confid1=configs.confid and ccb_crc.ccbid='%d' and ccb_crc.csettypeid='%d' and ccbrelations.ccbid='%d' and ccbrelations.confccbkey=configs.confid",theconf,ccbid,csettype,ccbid);
	//sprintf(mquer,"SELECT crc,ccb_crc.ccbid,ccbrelations.confkey,ccbrelations.confccbkey,ccb_crc.CSETTYPEID from ccb_crc,ccbrelations,configsets where configsets.name='%s' AND ccbrelations.confkey=configsets.confkey AND CCB_CRC.ccbid='%d' AND CCB_CRC.ccbid=ccbrelations.ccbid and ccb_crc.CSETTYPEID='%d'",theconf,ccbid,csettype);
	else
	{
    char myfie[1024];
    char myfie2[1024];
    char myfie3[1024];

   sprintf(mquer,"select confid from configs where confname='%s' and obso is null",theconf);
   printf(" %s ",mquer);
   resu=dtdbobj->sqlquery(mquer,&rows,&fields);

	if (resu==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {
   	dtdbobj->returnfield(0,myfie);
   }

   sprintf(mquer,"select confid from configs where confname='%s' and obso is null",theconf2);
   printf(" %s ",mquer);
  resu=dtdbobj->sqlquery(mquer,&rows,&fields);
	if (resu==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {
     dtdbobj->returnfield(0,myfie2);
	  }

   sprintf(mquer,"select confid from configs where confname='%s' and obso is null",theconf3);
   printf(" %s ",mquer);
  resu=dtdbobj->sqlquery(mquer,&rows,&fields);
	if (resu==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {
     dtdbobj->returnfield(0,myfie3);
	 }

	 sprintf(mquer,"SELECT crc from ccb_crc  where   ccb_crc.confid1='%s' and ccb_crc.confid2='%s' and ccb_crc.confid3='%s'\
      and ccb_crc.ccbid='%d' and ccb_crc.csettypeid='%d' ",myfie,myfie2,myfie3,ccbid,csettype);
   printf(" %s ",mquer);
	}	
		
		resu=dtdbobj->sqlquery(mquer,&rows,&fields);
		
		if (resu==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {
			
			char myfie[1024];
			dtdbobj->returnfield(0,myfie);
				sscanf(myfie,"%x",crc);

	printf("crc's for mc key %s and ccbid %d  (type %d) selected %x \n",theconf,ccbid,csettype,crc);
	dtdbobj->dbunlock(); // unlock the mutex
	return 0;
}
printf("No crc's for conf %d ccbid %d defined!\n",theconf,ccbid);
crc=0;
dtdbobj->dbunlock(); // unlock the mutex
return -1;

}

int ccbconfdb::storecrc(char *theconf, char *theconf2, char *theconf3, short ccbid,short crc, int type)
{

  char mquer[FIELD_MAX_LENGTH];
  int rows=0, fields=0, resu=0;
    char myfie[1024];
    char myfie2[1024];
    char myfie3[1024];

	  strcpy(myfie,"");
	  strcpy(myfie2,"");
	  strcpy(myfie3,"");

  dtdbobj->dblock(); // lock the mutex
	sprintf(mquer,"select confid from configs where confname='%s' and obso is null",theconf);
   resu=dtdbobj->sqlquery(mquer,&rows,&fields);

   dtdbobj->fetchrow();
	dtdbobj->returnfield(0,myfie);
  printf("select for confname %s with query %s reutrned %d rows\n",theconf,mquer,myfie);
   

   if (type==3)
	{
	sprintf(mquer,"select confid from configs where confname='%s' and obso is null",theconf2);
  resu=dtdbobj->sqlquery(mquer,&rows,&fields);
   dtdbobj->fetchrow();
     dtdbobj->returnfield(0,myfie2);

	sprintf(mquer,"select confid from configs where confname='%s' and obso is null",theconf3);
  resu=dtdbobj->sqlquery(mquer,&rows,&fields);
   dtdbobj->fetchrow();
     dtdbobj->returnfield(0,myfie3);
   }
	else 
	{
	  strcpy(myfie2,"");
	  strcpy(myfie3,"");
	}
			sprintf(mquer,"insert into ccb_crc values (ccb_crc_seq.nextval,'%d','%s','%s','%s','%d','%x')",type,myfie,myfie2,myfie3,ccbid,crc);

     resu=dtdbobj->sqlquery(mquer,&rows,&fields);

			printf("insert into ccb_crc values (ccb_crc_seq.nextval,'%d','%s','%s','%s','%d','%x') resu=%d",type,myfie,myfie2,myfie3,ccbid,crc);
	  
  if (resu==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {
     dtdbobj->dbunlock(); // unlock the mutex
     return 0;
    }
   printf("Errors in crc's store conf %d ccbid %d defined!\n",theconf,ccbid);
   dtdbobj->dbunlock(); // unlock the mutex
   return -1;

}


// Retrieve all conf's from <configsets>
// Author: A.Parenti, Jul26 2005
// Modified: AP, Aug14 2007 (Tested on MySQL/ORACLE)
int ccbconfdb::retrieveconfs(int *howmany,char cmds[][64]) {
  int rows=0, fields=0, resu=0;
  char mquer[LEN2];

  dtdbobj->dblock(); // lock the mutex
  *howmany=0;

  resu = dtdbobj->sqlquery("SELECT DISTINCT(Name) FROM configsets WHERE obso IS NULL OR obso>CURRENT_TIMESTAMP ORDER BY Name",&rows,&fields);
//  resu = dtdbobj->sqlquery("SELECT DISTINCT(Name) FROM configsets ORDER BY Name",&rows,&fields);
  if (resu==0) {
    while (dtdbobj->fetchrow()==SQL_SUCCESS) {
      dtdbobj->returnfield(0,&cmds[*howmany][0]);
      *howmany+=1;
    }

    dtdbobj->dbunlock(); // unlock the mutex
    return 0;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return -1;
}

// Retrieve all keys from <G2L>
int ccbconfdb::retrieveconfsG2L(int *howmany,char cmds[][64]) {
  int rows=0, fields=0, resu=0;
  char mquer[LEN2];

  dtdbobj->dblock(); // lock the mutex
  *howmany=0;

  resu = dtdbobj->sqlquery("SELECT DISTINCT(globalkey) FROM g2lrelations ORDER BY globalkey",&rows,&fields);

  if (resu==0) {
    while (dtdbobj->fetchrow()==SQL_SUCCESS) {
      dtdbobj->returnfield(0,&cmds[*howmany][0]);
      *howmany+=1;
    }

    dtdbobj->dbunlock(); // unlock the mutex
    return 0;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return -1;
}


// Retrieve commands from <configcmds>, selecting by "configsets.Name" and "ccbrelations.CcbID"
// Author: A.Parenti, Aug02 2005
// Modified: AP, Apr03 2007 (Tested on MySQL/Oracle)
int ccbconfdb::retrievecmds(int ccbid, int *howmany) {
  char mquer[LEN2];
  char myfie[1024];
  int rows=0, fields=0, resu=0;

  *howmany=0;

  if (ConfKey<=0) return -1;

  dtdbobj->dblock(); // lock the mutex


  sprintf(mquer,"SELECT COUNT(1) FROM configcmds,cfgrelations,cfg2brkrel,ccbrelations WHERE configcmds.CmdID=cfgrelations.CmdID AND cfg2brkrel.BrkID=cfgrelations.BrkID AND ccbrelations.ConfCcbKey=cfg2brkrel.ConfID AND ccbrelations.CcbID=%d AND ccbrelations.ConfKey=%d",ccbid,ConfKey);
  resu = dtdbobj->sqlquery(mquer,&rows,&fields);

  if (resu==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {
    dtdbobj->returnfield(0,myfie);
    *howmany = (int)strtol(myfie,NULL,10);

    dtdbobj->dbunlock(); // unlock the mutex
    return 0;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return -1;
}


// Autor: AP, Jan04 2006
int ccbconfdb::retrievecmds(int ccbid,int *howmany,char cmds[][512]) {
  retrievecmds(ccbid,howmany,cmds,0x7FFFFFFF);
}


// Author: A.Parenti, Jan04 2006
// Modified: AP, Apr03 2007 (Tested on on MySQL/Oracle)
int ccbconfdb::retrievecmds(int ccbid,int *howmany,char cmds[][512],int nmax) {
  char mquer[LEN2];
  char myfie[1024];
  int rows=0, fields=0, resu=0;
  int kk;

  *howmany=0;
  if (ConfKey<=0) return -1;

  dtdbobj->dblock(); // lock the mutex

  printf("retrievecmds:  retrieving ccb %d confkey %d \n",ccbid,ConfKey);

  sprintf(mquer,"SELECT configcmds.ConfData FROM configcmds,cfgrelations,cfg2brkrel,ccbrelations WHERE configcmds.CmdID=cfgrelations.CmdID AND cfg2brkrel.BrkID=cfgrelations.BrkID AND ccbrelations.ConfCcbKey=cfg2brkrel.ConfID AND ccbrelations.CcbID=%d AND ccbrelations.ConfKey=%d ORDER BY cfgrelations.ID",ccbid,ConfKey);
  resu = dtdbobj->sqlquery(mquer,&rows,&fields);

  if (resu==0) {
    for (kk=0; kk<nmax && dtdbobj->fetchrow()==SQL_SUCCESS; kk++) {
      dtdbobj->returnfield(0,cmds[*howmany]);
      *howmany+=1;
    }

    dtdbobj->dbunlock(); // unlock the mutex
    return 0;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return -1;
}


// Retrieve commands from <configcmds>, selecting by "configsets.Name", "ccbrelations.CcbID", and "cfgbricks.brktype"
// Author: A. Parenti, Mar14 2007
// Modified: AP, Apr03 (Tested on MySQL/Oracle)
int ccbconfdb::retrievecmds(int ccbid,int brktype,int *howmany) {
  char mquer[LEN2];
  char myfie[1024];
  int rows=0, fields=0, resu=0;

  *howmany=0;

  if (ConfKey<=0) return -1;

  dtdbobj->dblock(); // lock the mutex

  sprintf(mquer,"SELECT COUNT(1) FROM configcmds,cfgrelations,cfgbricks,cfg2brkrel,ccbrelations WHERE configcmds.CmdID=cfgrelations.CmdID AND cfgrelations.BrkID=cfgbricks.BrkID AND cfgbricks.BrkID=cfg2brkrel.BrkID AND cfg2brkrel.ConfID=ccbrelations.ConfCcbKey AND ccbrelations.CcbID=%d AND ccbrelations.ConfKey=%d AND cfgbricks.brktype=%d",ccbid,ConfKey,brktype);
  resu = dtdbobj->sqlquery(mquer,&rows,&fields);

  if (resu==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {
    dtdbobj->returnfield(0,myfie);
    *howmany = (int)strtol(myfie,NULL,10);

    dtdbobj->dbunlock(); // unlock the mutex
    return 0;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return -1;
}


// Autor: AP, Mar14 2007
int ccbconfdb::retrievecmds(int ccbid,int brktype,int *howmany,char cmds[][512]) {
  retrievecmds(ccbid,brktype,howmany,cmds,0x7FFFFFFF);
}

// Autor: A.Parenti, Mar14 2007
// Modified: AP, Apr03 2007 (Tested on on MySQL/Oracle)
int ccbconfdb::retrievecmds(int ccbid,int brktype,int *howmany,char cmds[][512],int nmax) {
  char mquer[LEN2];
  char myfie[1024];
  int rows=0, fields=0, resu=0;
  int kk;

  *howmany=0;
  if (ConfKey<=0) return -1;
  // printf("ccbconfdb::retrievecmds with brick type\n");

  dtdbobj->dblock(); // lock the mutex

  if (brktype == 20 ) 
  {
     	sprintf(mquer,"SELECT ccbcmds.ConfData FROM ccbcmds WHERE CcbID=%d AND ord=1 ORDER BY ccbcmds.cmdID",ccbid);
  	resu = dtdbobj->sqlquery(mquer,&rows,&fields);
  }
  else 
  if (brktype == 21 )
  {
        sprintf(mquer,"SELECT ccbcmds.ConfData FROM ccbcmds WHERE CcbID=%d AND ord=2 ORDER BY ccbcmds.cmdID",ccbid);
        resu = dtdbobj->sqlquery(mquer,&rows,&fields);
  }
  else
  {

  	sprintf(mquer,"SELECT configcmds.ConfData FROM configcmds,cfgrelations,cfgbricks,cfg2brkrel,ccbrelations WHERE configcmds.CmdID=cfgrelations.CmdID AND cfgrelations.BrkID=cfgbricks.BrkID AND cfgbricks.BrkID=cfg2brkrel.BrkID AND cfg2brkrel.ConfID=ccbrelations.ConfCcbKey AND ccbrelations.CcbID=%d AND ccbrelations.ConfKey=%d AND cfgbricks.brktype=%d ORDER BY cfgrelations.ID",ccbid,ConfKey,brktype);
  	resu = dtdbobj->sqlquery(mquer,&rows,&fields);
  }

  if (resu==0) {
    for (kk=0; kk<nmax && dtdbobj->fetchrow()==SQL_SUCCESS; kk++) {
      dtdbobj->returnfield(0,cmds[*howmany]);
      *howmany+=1;
    }

    dtdbobj->dbunlock(); // unlock the mutex
    return 0;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return -1;
}


// Retrieve "configs.confname" for the selected "configsets.Name"
// and the desired ccbid
// Author: A.Parenti, Jan26 2007
// Modified: AP, Apr03 2007 (Tested on MySQL/Oracle)
char* ccbconfdb::retrieve_configs_confname(int ccbid) {
  char mquer[LEN2];
  static char myfie[LEN1];
  int rows=0, fields=0, resu=0;

  if (ConfKey<=0) return "";

  dtdbobj->dblock(); // lock the mutex

  sprintf(mquer,"SELECT DISTINCT(configs.ConfName) FROM configs,ccbrelations,configsets WHERE configs.ConfID=ccbrelations.ConfCcbKey AND ccbrelations.ConfKey=%d AND ccbrelations.CcbID=%d",ConfKey,ccbid);
  resu=dtdbobj->sqlquery(mquer,&rows,&fields);

  if (resu!=0 || dtdbobj->fetchrow() != SQL_SUCCESS) {
    dtdbobj->dbunlock(); // unlock the mutex
    return "";
  }

  dtdbobj->returnfield(0,myfie);

  dtdbobj->dbunlock(); // unlock the mutex
  return myfie;
}

// Insert new commands in <configcmds>, update <cfgrelations> and
//   (if needed) <cfgbricks>. <BrkType> is the <cfgbricks.BrkType>
// Author: A.Parenti, Oct26 2006
// Modified: AP, Jul23 2007 (Tested on MySQL/Oracle)
int ccbconfdb::cmdinsert(char *name, char *cmd, Ebrktype brktype) {
  char mquer[FIELD_MAX_LENGTH], mhex[FIELD_MAX_LENGTH];
  char myfie[LEN1];
  char cmdname[32];
  int rows=0, fields=0, resu=0;
  int jj, mtok;

  int brkID, cmdID, cfgID;

  dtdbobj->dblock(); // lock the mutex

// Look for existing <cfgbricks.BrkName>
  sprintf(mquer,"SELECT COUNT(1) FROM cfgbricks WHERE BrkName='%s' AND BrkType=%d",name,brktype);
  resu=dtdbobj->sqlquery(mquer,&rows,&fields);
//  std::cout << mquer << std::endl; // Printout
  if (resu==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {
    dtdbobj->returnfield(0,myfie);
    jj=(int)strtol(myfie,NULL,10);
  } else {
    dtdbobj->dbunlock(); // unlock the mutex
    return -1;
  }

  if (jj==0) { // Create new entry in configs
    sprintf(mquer,"INSERT INTO cfgbricks (BrkName,BrkType) VALUES ('%s',%d)",name,brktype);
//    std::cout << mquer << std::endl; // Printout
    resu=dtdbobj->sqlquery(mquer,&rows,&fields);
    if (resu!=0) {
      dtdbobj->dbunlock(); // unlock the mutex
      return resu; // Error: return!
    }
  }

// Get ConfId
  sprintf(mquer,"SELECT BrkID FROM cfgbricks WHERE BrkName='%s'",name);
//  std::cout << mquer << std::endl; // Printout
  resu=dtdbobj->sqlquery(mquer,&rows,&fields);
  if (resu==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {
    dtdbobj->returnfield(0,myfie);
    brkID=(int)strtol(myfie,NULL,10);
  } else {
    dtdbobj->dbunlock(); // unlock the mutex
    return -1; // Error: return!
  }

  mtok = (unsigned char)cmd[2]; // command token
  mhex[0]=0; // line terminator
  for (jj=0; jj<(unsigned char)cmd[1]; ++jj)
    sprintf(mhex,"%s%02hhx",mhex,cmd[jj]); // Transform command in a string

// Command comment
  switch (mtok) {
  case 0x26:
    strcpy(cmdname,"CPU-CK delay");
    break;
  case 0x54:
    strcpy(cmdname,"Write BTI 6b");
    break;
  case 0x14:
    strcpy(cmdname,"Write BTI 8b");
    break;
  case 0x51:
    strcpy(cmdname,"Enable BTI");
    break;
  case 0x15:
    strcpy(cmdname,"Write TRACO");
    break;
  case 0x3E:
    strcpy(cmdname,"Write TRACO LUTs");
    break;
  case 0xA8:
    strcpy(cmdname,"Write MC LUTs");
    break;
  case 0x4D:
    strcpy(cmdname,"Preload LUTs");
    break;
  case 0x4E:
    strcpy(cmdname,"Load LUTs");
    break;
  case 0x16:
    strcpy(cmdname,"Write TSS");
    break;
  case 0x17:
    strcpy(cmdname,"Write TSM");
    break;
  case 0x49:
    strcpy(cmdname,"Trigger Out Select");
    break;
  case 0x44:
    strcpy(cmdname,"Write TDC");
    break;
  case 0x18:
    strcpy(cmdname,"Write TDC control");
    break;
  case 0x32:
    strcpy(cmdname,"ROB Reset");
    break;
  case 0x35:
    strcpy(cmdname,"Set FE Threshold");
    break;
  case 0x47:
    strcpy(cmdname,"Set FE Width");
    break;
  case 0x3A:
    strcpy(cmdname,"Mask FE");
    break;
  case 0x3B:
    strcpy(cmdname,"Mask FE channel");
    break;
  case 0x62:
    strcpy(cmdname,"Enable FE temperature chip");
    break;
  default:
    strcpy(cmdname,"Other command");
  }

// Is the command already in the DB?
  sprintf(mquer,"SELECT CmdID FROM configcmds WHERE ConfData LIKE '0x%s'",mhex);
  if (dtdbobj->sqlquery(mquer,&rows,&fields)==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {   
    dtdbobj->returnfield(0,myfie);
    cmdID=(int)strtol(myfie,NULL,10);
  } else { // Insert command
    sprintf(mquer,"INSERT INTO configcmds (ConfToken,ConfData,CmdName) VALUES ('%d','0x%s','%s')",mtok,mhex,cmdname);
//  std::cout << mquer << std::endl; // Printout
    if (dtdbobj->sqlquery(mquer,&rows,&fields)!=0) {
      dtdbobj->dbunlock(); // unlock the mutex
      return -1;
    }

    dtdbobj->dbunlock(); // unlock the mutex
    cmdID = dtdbobj->get_counter("cmdid","configcmds"); // Get cmdid
    dtdbobj->dblock(); // relock the mutex
  }

// Insert relation into cfgrelations
  sprintf(mquer,"INSERT INTO cfgrelations (BrkID,CmdID) VALUES ('%d','%d')",brkID,cmdID);
//  std::cout << mquer << std::endl; // Printout
  if (dtdbobj->sqlquery(mquer,&rows,&fields)!=0) {
    dtdbobj->dbunlock(); // unlock the mutex
    return -1;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return 0;
}


// Retrieve confmask.maskxml for the desired ccbid
// Author: A.Parenti, Oct28 2007
int ccbconfdb::retrieve_confmask(int ccbid, char *maskxml) {
  char mquer[LEN2];
  int rows=0, fields=0, resu=0;

  dtdbobj->dblock(); // lock the mutex

  strcpy(maskxml,"");

  sprintf(mquer,"SELECT maskxml FROM confmask WHERE CcbID=%d ORDER BY Datetime DESC",ccbid);
  resu=dtdbobj->sqlquery(mquer,&rows,&fields);

  if (resu!=0 || dtdbobj->fetchrow() != SQL_SUCCESS) {
    dtdbobj->dbunlock(); // unlock the mutex
    return -1;
  }

  dtdbobj->returnfield(0,maskxml);

  dtdbobj->dbunlock(); // unlock the mutex
  return 0;
}



//// AP, Jan03 2006: Tested on ODBC (MySQL-ORACLE)
//int ccbconfdb::appliedtoccb(char *confname, int ccbid) {
//  ConfKey = confselect(confname);
//  return appliedtoccb(ccbid);
//}
//
// AP, Jan03 2006: Tested on ODBC (MySQL-ORACLE)
//int ccbconfdb::appliedtoccb(int ccbid) {
//  char mquer[LEN2];
//  int rows=0, fields=0;
//
//  if (ConfKey<=0) {
//    printf("WARNING! No config was selected!!!\n");
//    return -1;
//  }
//
//  sprintf(mquer,"INSERT INTO runconfigs VALUES ('%d','%d','%d')",
//    curr_run,ccbid,ConfKey);
//
//  return dtdbobj->sqlquery(mquer,&rows,&fields);
//}
