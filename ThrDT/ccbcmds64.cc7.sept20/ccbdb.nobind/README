These classes are used to access the DT database.
They must be compiled and installed before compiling ccbcmds.

Compiling and installing ccbdb
******************************

Prerequisites:
* unixODBC libraries (in particular libodbc.so is used) and include files.
* MySQL-ODBC driver (MyODBC package from http://dev.mysql.com/downloads/),
  if a MySQL DB is used
* Oracle-ODBC driver if a Oracle DB is used.
  An "official" driver from Oracle is supplied with the instant client,
  http://www.oracle.com/technology/tech/oci/instantclient/index.html.
  An open source driver for linux is available at the URL
  http://home.fnal.gov/~dbox/oracle/odbc/, a commercial one is provided by
  Easysoft, http://www.easysoft.com/),
  The last two drivers need libclntsh.so and oci include files from
  Oracle.

How to install and configure ODBC:
1) Install first unixODBC libraries and include files.
2) Install then MySQL or Oracle ODBC libraries and configure unixODBC to use
   them (see details above).

How to compile and install ccbdb:
1) In the current directory (ccbdb) run "make".
2) As root, run "make install" in the same directory.
3) Default datasource: is defined in the configuration file /etc/ccbdb.conf;
   for example
     DATASOURCE=cmsdtdb
   The configuration file is defined in Ccmsdtdb.h (#define DB_CONFIG_FILE ...)

How to uninstall ccbdb:
1) As root, run "make uninstall" in this directory (ccbdb).


How to create the RPM:
1) In the current directory, as root, run "rpmbuild -bb ccbdb.SPEC"


Installing unixODBC
*******************

*** Installation ***
1) Get unixODBC. Home page: http://www.unixodbc.org/, downloads from:
   http://sourceforge.net/projects/unixodbc/
2) Install unixODBC and unixODBC-devel (ie include files). RPMs are available
   at sourceforge.net.

*** Configuration ***
1) /etc/odbcinst.ini file
   It contains a list of drivers and options; the options are driver specific,
   see driver instructions for details.
2) /etc/odbc.ini and ~/.odbc.ini files
   They contain a list of DSN (Data Source Name) and options; the options are
   driver specific, see driver instructions for details.
   The only difference is that the file /etc/odbc.ini is visible to all users,
   ~/.odbc.ini only to one...
3) If you prefer unixODBC supplies the odbcinst command in order to modify the
   odbcints.ini and .odbc.ini files.
4) unixODBC also provides a command-line interface to a datasource: isql


Installing MyODBC
*****************

*** Installation ***
The files are downloadable from MySQL web site: http://dev.mysql.com/downloads/
also as RPM.

*** unixODBC configuration ***
1) Add to odbcinst.ini:
[MySQL ODBC 3.51 Driver]
DRIVER          = /usr/lib/libmyodbc3.so
SETUP           = /usr/lib/libmyodbc3S.so
UsageCount      = 1
2) Add to odbc.ini:
[cmsdtdb]
Driver          = MySQL ODBC 3.51 Driver
Description     = MySQL
SERVER          = icab159.pd.infn.it
PORT            = 3306
USER            = [masked]
Password        = [masked]
Database        = andrea
OPTION          = 3
SOCKET          = ''


Installing Oracle instant client and ODBC
*****************************************
-> The "official" client from Oracle

*** Installation ***
1) I describe here how to get and install the Oracle instant client; it can be
   used freely for software development.
2) From version 10.2.0.2 the software is only distributed as zip files.
   Go to http://www.oracle.com/technology/tech/oci/instantclient/index.html and
   download the kit (basic+odbc+sdk and sqlplus if you like), go in /opt and
   (as root) unzip all.
3) Add /opt/instantclient_[version] to LD_LIBRARY_PATH.
4) You can now use sqlplus to access the database; eg
   "sqlplus [user]/[password]@oracms.cern.ch:10121/OMDS" or

*** ODBC driver Configuration ***
1) You first need to create in /opt/instantclient_[version] (or elsewhere) a
   file tnsnames.ora like this:

OMDS =
  (DESCRIPTION =
    (ADDRESS = (PROTOCOL = TCP)(HOST = oracms.cern.ch)(PORT = 10121))
    (CONNECT_DATA =
      (SERVER = DEDICATED)
      (SERVICE_NAME = OMDS)
    )
  )

   and then to set TNS_ADMIN=/opt/instantclient_[version]
2) You need to set TWO_TASK=OMDS (the connect identifier appearing in the
   tnsnames.ora file).

*** unixODBC configuration ***
1) Add the driver in the odbcinst.ini file:
[Oracle ODBC driver]
Description     = Oracle ODBC driver for Oracle 10g
Driver          = /opt/instantclient_[version]/libsqora.so.10.1
Setup           =
FileUsage       =
CPTimeout       =
CPReuse         =
2) Add the DSN in the odbc.ini file:
[drifttube2]
Driver          = Oracle ODBC driver
Description     = Oracle 10g Driver
USERID          = [masked]
PASSWORD        = [masked]


Installing open source oracle_odbc driver
*****************************************
-> This is an open source linux driver from Dennis Box (dbox@fnal.gov)

*** Installation ***
1) Once libclntsh.so and sdk include files are installed (see "Installing
   Oracle instant client and ODBC"), you need to create
   /opt/instantclient_[version]/oci and link
   /opt/instantclient_[version]/sdk/include to
   /opt/instantclient_[version]/oci/include.
   Then you have to create /opt/instantclient_[version]/lib and link
   /opt/instantclient_[version]/libclntsh.so.[lib_version] to
   /opt/instantclient_[version]/lib/libclntsh.so
2) Download the source or get it from CVS, as described in
   http://home.fnal.gov/~dbox/oracle/odbc/
3) Un-tar somewhere the tar.gz file (if needed):
   "tar -xzf oracle_odbc_driver.[version].tar.gz"
   and go into the oracle_odbc_driver directory.
4) Run "./configure --with-orahome=/opt/instantclient_[version]"
5) Run "make" then (as root) "make install".

*** unixODBC configuration: ***
1) Add the driver in the odbcinst.ini file:
[oracle-driver]
Description = OpenSource Oracle Driver
Driver = /usr/local/lib/liboraodbc.so
Setup =
FileUsage = 1
2) Add the DSN in the odbc.ini file:
[drifttube]
Driver          = oracle-driver
Description     = Easysoft Oracle Driver
Database        = //oracms.cern.ch:10121/OMDS
USER            = [masked]
PASSWORD        = [masked]


Installing Easysoft Oracle-ODBC driver
**************************************
-> This is a linux commercial software! You can register and then download it
   for evaluation from www.easysoft.com

1) Un-tar somewhere the file:
   "tar -xf  odbc-oracle-[version].tar"
2) Install it by typing "./install" as root.
   You will be prompted for some parameters of the Oracle DB (Server, Port,
   Service Name): they are available in the tnsnames.ora file, on the server
   that hosts the DB.
   You will be asked too for Username and Password for accessing the Oracle DB.
   The installation program will install the needed libraries, documentation,
   and will add a DSN to /etc/odbcinst.ini and /etc/odbc.ini unixODBC files.
3) You need to set the environment variable EASYSOFT_ROOT (eg
   /usr/local/easysoft) and to add ${EASYSOFT_ROOT}/lib to the
   LD_LIBRARY_PATH


Creating the Database
*********************

0) Move to the scripts/ subdirectory.

1) The scripts used to create the tables in MySQL and Oracle are mysql.sql,
 and oracle.sql, respectively.
 The DB tables and relations are described in schema.sxi (schema.pdf).

2) In order to populate the PADC tables, I provide a shell script,
 scripts/padclut_to_sql.csh.
 It takes as input a LUT file and create a sql script;
 the LUT file usually has a name of the type LUT_PADC_ID_119_060531_1716.txt,
 where 119 is the PADC_ID, 060531_1716 is date/time in the format
 yymmdd_hhmm.
 If you have unixODBC installed you can execute the script by writing
 cat <file.sql> | isql -b <odbc datasource>.
 With sqlplus you can run the sql script from the sqlplus prompt: "@ file.sql"
 The data are stored in DB with an ID (PadcDataId) equal to 999999, therefore
 you have to change the "PadcDataId" field in the database.
 In any SQL client (like sqlplus, isql) you must type
 "SELECT MAX(PadcDataId+1) FROM padcrelations WHERE PadcDataId!=999999"
 in order to get the first available PadcDataId (let me call it xyz)
 and then "UPDATE padcrelations SET PadcDataId=xyz WHERE PadcDataId=999999",
 "UPDATE padcdata SET PadcDataId=xyz WHERE PadcDataId=999999" in order to
 update the PadcDataId.

3) The 5 scripts wheel-2.sql, wheel-1.sql, wheel0.sql, wheel+1.sql, wheel+2.sql
 fill the ccbmap table.

4) The insert_reference.sql script link the reference value for each chamber
 (ie fill the refrelations table). Check that bootrefid, statusrefid, testrefid
 point to the correct lines in ccbref table.

5) The script insert_ccbgeom.sql fill the ccbgeom table.

Author: A. Parenti
Revision: Oct04 2007
