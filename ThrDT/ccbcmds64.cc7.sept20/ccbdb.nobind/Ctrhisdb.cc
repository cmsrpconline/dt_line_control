#include "Ctrhisdb.h"

#include <cstring>

trhisdb::trhisdb() {
  dtdbobj = new cmsdtdb;
  newconn = true;
}

trhisdb::trhisdb(cmsdtdb* thisobj) {
  dtdbobj = thisobj;
  newconn = false;
}

trhisdb::~trhisdb() {
  if (newconn)
    delete dtdbobj;
}



// Authors: S.Ventura, A.Parenti
// Modified: AP, Apr18 2007 (Tested on ODBC MySQL-ORACLE)
int trhisdb::insert(int theBX,int counts) {
  char mquer[FIELD_MAX_LENGTH], mhex[FIELD_MAX_LENGTH];
  int rows=0, fields=0, resu=0;
  int ii;

  mhex[0]=0;
  for (ii=0; ii<(unsigned char)mst[1]; ii++)
    sprintf(mhex,"%s%02hhx",mhex,mst[ii]);

  sprintf(mquer,"INSERT INTO trighisto (offset,counts,datetime) VALUES ('%d','%d',CURRENT_TIMESTAMP)",theBX,counts);

  dtdbobj->dblock(); // lock the mutex
  resu=dtdbobj->sqlquery(mquer,&rows,&fields);
  dtdbobj->dbunlock(); // unlock the mutex

  return resu;
}


// Authors: S.Ventura, A.Parenti
// Modified: AP, Mar22 2006 (Tested on ODBC MySQL-ORACLE)
int trhisdb::retrieveBX(int theBx, char *sttime) {
  char mquer[LEN2];
  int rows=0, fields=0, reso;

  dtdbobj->dblock(); // lock the mutex
  sprintf(mquer,"select counts from (select offset,counts from trighisto where offset=%d and datetime>%s ) where rownum<=1",theBx,sttime);

  reso=dtdbobj->sqlquery(mquer,&rows,&fields);

  if (reso==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {
   char mst[255];
   dtdbobj->returnfield(0,mst);
	int cou;
	sscanf(mst,"%d",&cou);
   return cou;
	}
	else return -1;
   
}

// Authors: S.Ventura, A.Parenti
// Modified: AP, Apr05 2007 (Tested on ODBC MySQL-ORACLE)
int trhisdb::retrieveOrbit(char *sttime, char *endtime, int *countarr) {
  char mquer[LEN2];
  int rows=0, fields=0, reso;

  dtdbobj->dblock(); // lock the mutex


  sprintf(mquer,"select distinct offset,counts from trighisto where datetime>%s) and rownum<=3600 ",sttime,endtime);
  reso = dtdbobj->sqlquery(mquer,&rows,&fields);

  if (reso==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {
    for (int jj=0;jj<row;jj++)
	 {
	    char mst[255];
       dtdbobj->returnfield(0,mst);
	    int cou;
	    sscanf(mst,"%d",&cou);
		 countarr[jj]=cou;
    }
    dtdbobj->dbunlock(); // unlock the mutex
    return 0;
	 }
    dtdbobj->dbunlock(); // unlock the mutex
    return -1;
	
}


