#include "Cdtalert.h"

#include <cstring>

dtalert::dtalert() {
  dtdbobj = new cmsdtdb;
  newconn = true;
}

dtalert::dtalert(cmsdtdb* thisobj) {
  dtdbobj=thisobj;
  newconn = false;
}

dtalert::~dtalert() {
  if (newconn)
    delete dtdbobj;
}

int dtalert::isStableBeams(char *data){
char mquer[FIELD_MAX_LENGTH];
int rows=0, fields=0, reso;

  dtdbobj->dblock(); // lock the mutex

  //sprintf(mquer,"select VALUE as presentstatus from (select * from cms_beam_cond.lhc_beammode order by diptime desc) where rownum=1;");
  //2012 sprintf(mquer,"select RUNCONTROLBEAMMODE from (select * from cms_beam_cond.BRM_LHCINONEPAGE order by diptime desc) where rownum<2;");
  sprintf(mquer,"select VALUE from (select * from CMS_LHC_BEAM_COND.LHC_BEAMMODE order by diptime desc) where rownum<2;");
//  printf("%s\n",mquer);
  reso = dtdbobj->sqlquery(mquer,&rows,&fields);

  if (reso==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {
    dtdbobj->returnfield(0,data);

    dtdbobj->dbunlock(); // unlock the mutex
///masking    if (strcmp("STABLE BEAMS",data)!=0) 
	if (strcmp("FLAT TOP",data)==0) return 1;
	if (strcmp("SQUEEZE",data)==0) return 1;
	if (strcmp("ADJUST",data)==0) return 1;
	if (strcmp("STABLE BEAMS",data)==0) return 1;
   return 0;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return 0;
}


int dtalert::retrieveAlert(char *data,char *time) {
  char mquer[FIELD_MAX_LENGTH];
  int rows=0, fields=0, reso;

  dtdbobj->dblock(); // lock the mutex

  sprintf(mquer,"select * from (select errkey,rownum r from da_errkeys where alertid in (select id from da_alerts where time_off is null)) where rownum<=1");
//  printf("%s\n",mquer);
  reso = dtdbobj->sqlquery(mquer,&rows,&fields);

  if (reso==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {
    dtdbobj->returnfield(0,data);

    dtdbobj->dbunlock(); // unlock the mutex
    return 1;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return 0;
}


