#ifndef _dtalertdb_
#define _dtalertdb_
#include "Ccmsdtdb.h"

class dtalert {

 public:
  dtalert(); // Create a new connection to DB
  dtalert(cmsdtdb* dtdbobj); // Use an existing connection to DB
  ~dtalert();


//  int isAlert(); 
  int retrieveAlert(char *data,char *time); // Input: ccbID,CmdCode
  int isStableBeams(char *data);

 private:
  bool newconn; // True if a new connection to DB is open
  cmsdtdb* dtdbobj;
};

#endif
