#include <ccbcmds/apfunctions.h>
#include <ccbcmds/Clog.h>
#include <ccbcmds/Ccommand.h>
#include <ccbcmds/Cdef.h>

#include <iostream>
#include <stdio.h>

// Author: A.Parenti, Aug07 2007
int main(int argc, char *argv[]) {
  char c, cmdstr[3*WRITE_DIM]="", *errstr;
  short ccbport;
  int i, errcod;

  unsigned char sntline[WRITE_DIM], rdline[READ_DIM];
  int sntlinelen, rdlinelen;

  Clog *myLog;
  Ccommand *myCmd;

  if (argc < 2) {
    printf("*** Usage: mc_generic_command [ccbport] [commandline]\n");

    printf("ccbport? ");
    scanf("%d",&ccbport);
    getchar(); // catch the '\n'

    printf("Command line? (eg. 35 0 0 60 F 0 1 51 8B FF FB)\n");
    while ((c=getchar()) != '\n')
      sprintf(cmdstr,"%s%c",cmdstr,c);

  }  else {
    sscanf(argv[1],"%d",&ccbport);

    for (i=2;i<argc;++i)
      sprintf(cmdstr,"%s %s",cmdstr,argv[i]);
  }

  printf("Would you like to send the command '%s'? (y/N) ",cmdstr);
  c=getchar();
  if (c!='y' && c!='Y') return 0;

  errstr=new char[1024];
  string_to_array(cmdstr,16,sntline,&sntlinelen); // Convert to ccb format

#ifdef LOGTODB
  myLog = new Clog(); // Log to DB
#else
  myLog = new Clog("log.txt"); // Log to file
#endif
  myCmd = new Ccommand(ccbport,CCBSERVER,18885,myLog);

  myCmd->set_write_log(true); // Switch on logger

  printf("Using server %s:%d\n",myCmd->read_server_name(),myCmd->read_server_port());

  printf("\n\n\n");
  printf("*** Sending command to ccbport: %d\n",ccbport);
  printf("*** Actual time: %s\n",get_time());

  errcod=myCmd->send_command(sntline,sntlinelen,TIMEOUT_DEFAULT);
  errstr=myCmd->read_error_message(errcod);


  printf("CCB reply:");
  for (i=0; i<(myCmd->data_read_len); ++i)
    printf(" %02X",myCmd->data_read[i]);
  putchar('\n');
  printf("Error code: %s\n",errstr);
}
