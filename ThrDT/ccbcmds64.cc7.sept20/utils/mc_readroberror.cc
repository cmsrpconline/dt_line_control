#include <ccbcmds/apfunctions.h>
#include <ccbcmds/Ccommand.h>
#include <ccbcmds/Crob.h>
#include <ccbcmds/Cdef.h>

#include <iostream>
#include <stdio.h>

// Author: A.Parenti, Aug10 2006
// Modified: AP, Jan26 2007
int main(int argc, char *argv[]) {
  short ccbport;
  Clog *myLog;
  Ccommand *myCmd;
  Crob *myRob;

  if (argc < 2) {
    printf("*** Usage: mc_readroberror [ccbport]\n");

    printf("ccbport? ");
    scanf("%d",&ccbport);
  }  else {
    sscanf(argv[1],"%d",&ccbport);
  }


  myLog = new Clog("log.txt"); // Log to file
  myCmd = new Ccommand(ccbport,CCBSERVER,CCBPORT,myLog);
  myRob = new Crob(myCmd); // New ROB object
  
  printf("Using server %s:%d\n",myCmd->read_server_name(),myCmd->read_server_port());

  printf("\n\n\n");
  printf("*** Read ROB error ***\n");
  printf("*** Actual time: %s\n",get_time());
  myRob->read_ROB_error(); // 0x33
  myRob->read_ROB_error_print();

}
