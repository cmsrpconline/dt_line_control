#include <ccbcmds/apfunctions.h>
#include <ccbcmds/Clog.h>
#include <ccbcmds/Ccommand.h>
#include <ccbcmds/Cttcrx.h>

#include <iostream>
#include <stdio.h>

// Author: A.Parenti, Jul07 2006
// Modified: AP, Jan26 2007
int main(int argc, char *argv[]) {
  char channel;
  short ccbport;
  float delay;
  Clog *myLog;
  Ccommand *myCmd;
  Cttcrx *myTtcrx;

  if (argc < 4) {
    printf("*** Usage: ttc_fine_delay [ccbport] [channel] [delay]\n");
    printf("*** channel: 0= TTCrx fine delay1, 1=TTCrx fine delay2\n");
    printf("*** delay: (ns) from 0 to 24.9\n");

    printf("ccbport? ");
    scanf("%d",&ccbport);
    printf("channel? ");
    scanf("%d",&channel);
    printf("delay? ");
    scanf("%f",&delay);
  }  else {
    sscanf(argv[1],"%d",&ccbport);
    sscanf(argv[2],"%d",&channel);
    sscanf(argv[3],"%f",&delay);
  }

#ifdef LOGTODB
  myLog = new Clog(); // Log to DB
#else
  myLog = new Clog("log.txt"); // Log to file
#endif
  myCmd = new Ccommand(ccbport,CCBSERVER,CCBPORT,myLog);
  myTtcrx = new Cttcrx(myCmd);

  myCmd->set_write_log(true); // Switch on logger

  printf("Using server %s:%d\n",myCmd->read_server_name(),myCmd->read_server_port());

  printf("\n\n\n");
  printf("*** Set TTCrx fine delay ***\n");
  printf("*** Actual time: %s\n",get_time());
  myTtcrx->set_TTC_fine_delay(channel,delay); // 0x8E
  myTtcrx->set_TTC_fine_delay_print();

}
