#include <ccbcmds/apfunctions.h>
#include <ccbcmds/Cdef.h>
#include <ccbcmds/Ccommand.h>
#include <ccbcmds/Cccb.h>
#include <ccbcmds/Ci2c.h>
#include <ccbcmds/Clog.h>
#include <ccbcmds/Cref.h>

#ifdef __USE_CCBDB__ // DB access enabled
  #include <ccbdb/Ccmsdtdb.h>
#endif

#include <cstdio>
#include <iostream>
#include <fstream>
#include <ctime>

#define LOWT  15 // Lower temperature limit
#define HIGHT 38 // Higher temperature limit
#define MONITINT  60 // temperature monitoring interval, sec
#define PRESSINT 300 // pressure monitoring interval, sec
#define WRITEINT 300 // temperature logging interval, sec
#define NMAX 250

//#define FILEROOT "/home/dtdqm/temperature/" // directory for logging
#define FILEROOT "/home/parenti/" // directory for logging

// Author: A.Parenti, Aug24 2006
// Modified: AP, Apr16 2007

#ifdef __USE_CCBDB__ // DB access enabled
int main(int argc, char *argv[]) {

  int i, j;
  char TLogFile[256], PLogFile[256]; // Log filenames: max_temperature, press.
  char ATLogFile[256]; // Log filename: all_temperature
  char CcbFName[256]; // CCB list filename
  float ttt;
  std::string CcbFCont; // CcbFName content
  std::fstream TFile, PFile; // (MAX) Temperatures file, Pressure file
  std::fstream ATFile; // (ALL) Temperatures file

// Datetime
  time_t currenttime, wtime, ttime[NMAX], ptime[NMAX];
  time_t dt_ttime, dt_ptime;
  struct tm *timeinfo;
  char datetime[256], tempstr[2500];

// CCB list and status
  char fname[NMAX][256];
  int nccb, ccbid[NMAX], port[NMAX];
  float maxtempRob[NMAX], maxtempTrb[NMAX], maxtempExt[NMAX];
  float pressure_hv[NMAX], pressure_fe[NMAX];
  Clog myLog("/dev/null"); // No ccbcmds log
  Ccommand *myCmd[NMAX];
  Cccb *myCcb[NMAX];
  Ci2c *myI2c[NMAX];
  Cref *myRef[NMAX];

// For PADCs
  bool padc_error;
  short adc_count[NMAX][10];
  float volt_read[10], data_read[10];

  if (argc <2) {
    printf("*** Usage: dt_monitor_noDB <ccblist_file>\n");
    return -1;
  }  else {
    sscanf(argv[1],"%s",&CcbFName);
  }

// Reset
  wtime=dt_ttime=dt_ptime=0;
  for (i=0;i<NMAX;++i)
    ttime[i]=ptime[i]=0;


// Read CCB list

  cmsdtdb mydtdtb;
  std::string::size_type pos1=0, pos2=0;
  std::string pippo;

  CcbFCont=read_file((std::string)CcbFName);

  for (nccb=0; (pos2=CcbFCont.find ("\n",pos1))!=std::string::npos &&
	 nccb<NMAX; nccb++) {
    pippo = CcbFCont.substr(pos1,pos2-pos1);

    sscanf(pippo.c_str(),"%d %d %s",ccbid+nccb,port+nccb,fname[nccb]);

    myCmd[nccb]=new Ccommand(ccbid[nccb],port[nccb],CCBSERVER,CCBPORT,&myLog);
    myCcb[nccb]=new Cccb(myCmd[nccb]); // New CCB object
    myI2c[nccb]=new Ci2c(myCmd[nccb]); // New I2C object
    myRef[nccb]=new Cref(ccbid[nccb],&mydtdtb); // New Cref object

    pos1=pos2+1;
  }

// Get datetime & create LogFiles
  time(&currenttime);
  timeinfo = localtime(&currenttime);
  sprintf(TLogFile,"%sdttemp%d%02d%02d.txt",FILEROOT,timeinfo->tm_year+1900,
	  timeinfo->tm_mon+1,timeinfo->tm_mday);
  sprintf(PLogFile,"%sdtpres%d%02d%02d.txt",FILEROOT,timeinfo->tm_year+1900,
	  timeinfo->tm_mon+1,timeinfo->tm_mday);
  sprintf(ATLogFile,"%sdttempall%d%02d%02d.txt",FILEROOT,
	  timeinfo->tm_year+1900,timeinfo->tm_mon+1,timeinfo->tm_mday);

// Open files for logging
  TFile.open(TLogFile,std::ios::out | std::ios::app); 
  PFile.open(PLogFile,std::ios::out | std::ios::app); 
  ATFile.open(ATLogFile,std::ios::out | std::ios::app); 

  if (!TFile.is_open()) {
    printf("Cannot open file %s for logging.\n",TLogFile);
    return -2;
  }
  if (!PFile.is_open()) {
    printf("Cannot open file %s for logging.\n",PLogFile);
    return -2;
  }
  if (!ATFile.is_open()) {
    printf("Cannot open file %s for logging.\n",ATLogFile);
    return -2;
  }

  printf("Starting temperature/pressure monitor. Please wait.\n");
  while(1) {
// Get datetime
    time(&currenttime);
    timeinfo = localtime(&currenttime);
    sprintf(datetime,"%d/%02d/%02d\t%02d:%02d",timeinfo->tm_year+1900,
            timeinfo->tm_mon+1,timeinfo->tm_mday,timeinfo->tm_hour,
            timeinfo->tm_min);

    for (i=0;i<nccb;++i) {
      if (currenttime-ttime[i]>=MONITINT) {
	ttime[i]=currenttime;

	myCcb[i]->MC_temp(); // Read all MC temperatures

// Reset maxtemp values
        maxtempExt[i]=maxtempTrb[i]=maxtempRob[i]=0.0;

        for (j=0;j<=6;++j) // ROB max temp
	  if (myCcb[i]->mc_temp.temp[j]>maxtempRob[i])
            maxtempRob[i]=myCcb[i]->mc_temp.temp[j];
        for (j=7;j<=14;++j) // TRB max temp
	  if (myCcb[i]->mc_temp.temp[j]>maxtempTrb[i])
            maxtempTrb[i]=myCcb[i]->mc_temp.temp[j];
        for (j=15;j<=20;++j) // Ext sensor max temp
	  if (myCcb[i]->mc_temp.temp[j]>maxtempExt[i])
            maxtempExt[i]=myCcb[i]->mc_temp.temp[j];
      }

      if (currenttime-ptime[i]>=PRESSINT) {
	ptime[i]=currenttime;
        vector_reset(adc_count[i],10); // Reset adc_count
        pressure_hv[i]=pressure_fe[i]=0;

	if (myCcb[i]->mc_temp.code==0x3D) {
	  myI2c[i]->read_PADC(&padc_error,adc_count[i],volt_read);// Read PADCs
	  if (!padc_error) {
	    myI2c[i]->use_PADC_LUT(fname[i],adc_count[i],data_read)==0;
	    pressure_hv[i]=1000*(data_read[0]+data_read[1])/2;
	    pressure_fe[i]=1000*(data_read[4]+data_read[5])/2;
	  }
	}
      }

    }

// Get temperature/pressure "last update" time
    for (i=1,dt_ttime=ttime[0],dt_ptime=ptime[0];i<nccb;++i) {
      dt_ttime=(ttime[i]<dt_ttime)?ttime[i]:dt_ttime;
      dt_ptime=(ptime[i]<dt_ptime)?ptime[i]:dt_ptime;
    }


// Print to stdout
    printf("%s",CLRSCR); // Clear screen
    printf("%s",STDBKG); // Standard BKG
    printf("Current time:            %s\n",get_time());
    printf("Last temperature update: %s",ctime(&dt_ttime));
    printf("Last pressure update:    %s",ctime(&dt_ptime));
    printf("\n");
    printf("**************************************************************\n");
    printf(" CCBID   MAXTEMP   MAXTEMP   MAXTEMP   SENS100_HV   SENS100_FE\n");
    printf("        (ROB,°C)  (TRB,°C)  (EXT,°C)     (mbar)       (mbar)  \n");
    printf("**************************************************************\n");
    for (i=0;i<nccb;++i) {
      printf(" %5d ",ccbid[i]);

// Print max Temperature, ROB
      if (maxtempRob[i]>HIGHT) printf("%s",REDBKG); // Red BKG
      else if (maxtempRob[i]<=LOWT) printf("%s",BLUBKG); // Blue BKG
      else printf("%s",GRNBKG); // Green BKG
      printf("   %6.2f ",maxtempRob[i]);
      printf("%s",STDBKG); // Standard BKG

// Print max Temperature, TRB
      if (maxtempTrb[i]>HIGHT) printf("%s",REDBKG); // Red BKG
      else if (maxtempTrb[i]<=LOWT) printf("%s",BLUBKG); // Blue BKG
      else printf("%s",GRNBKG); // Green BKG
      printf("   %6.2f ",maxtempTrb[i]);
      printf("%s",STDBKG); // Standard BKG

// Print max Temperature, EXT
      if (maxtempExt[i]>HIGHT) printf("%s",REDBKG); // Red BKG
      else if (maxtempExt[i]<=LOWT) printf("%s",BLUBKG); // Blue BKG
      else printf("%s",GRNBKG); // Green BKG
      printf("   %6.2f ",maxtempExt[i]);
      printf("%s",STDBKG); // Standard BKG

// Print Pressure
      printf("      %+3.3f       %+3.3f\n",pressure_hv[i],pressure_fe[i]);
    }

    if (timeinfo->tm_mday != localtime(&wtime)->tm_mday){// Day has changed
      TFile.close(); // Close logging file
      PFile.close(); // Close logging file
      ATFile.close(); // Close logging file
      sprintf(TLogFile,"%sdttemp%d%02d%02d.txt",FILEROOT,
              timeinfo->tm_year+1900,timeinfo->tm_mon+1,timeinfo->tm_mday);
      sprintf(PLogFile,"%sdtpres%d%02d%02d.txt",FILEROOT,
              timeinfo->tm_year+1900,timeinfo->tm_mon+1,timeinfo->tm_mday);
      sprintf(ATLogFile,"%sdttempall%d%02d%02d.txt",FILEROOT,
              timeinfo->tm_year+1900,timeinfo->tm_mon+1,timeinfo->tm_mday);

// Re-open a new file for logging
      TFile.open(TLogFile,std::ios::out | std::ios::app);
      PFile.open(PLogFile,std::ios::out | std::ios::app);
      ATFile.open(ATLogFile,std::ios::out | std::ios::app);
    }

    if (currenttime-wtime>=WRITEINT) { // Write every 300 sec
      wtime=currenttime;

// Write (MAX) Temperature file
      TFile << datetime;
      strcpy(tempstr,"");
      for (i=0;i<nccb;++i) {
        ttt=0.0;
	for (j=0;j<21;++j)
 	  ttt=(myCcb[i]->mc_temp.temp[j]>ttt)?myCcb[i]->mc_temp.temp[j]:ttt;
        sprintf(tempstr,"%s\t%6.2f",tempstr,ttt);
      }
      TFile << tempstr << '\n';
      TFile.flush();

// Write Pressure file
      PFile << datetime;
      strcpy(tempstr,"");
      for (i=0;i<nccb;++i) {
        for (j=0;j<10;++j)
          sprintf(tempstr,"%s\t%d",tempstr,adc_count[i][j]);
      }
      PFile << tempstr << '\n';
      PFile.flush();

// Write (ALL) Temperature file
      ATFile << datetime;
      strcpy(tempstr,"");
      for (i=0;i<nccb;++i)
	for (j=0;j<21;++j)
	  sprintf(tempstr,"%s\t%6.2f",tempstr,myCcb[i]->mc_temp.temp[j]);
      ATFile << tempstr << '\n';
      ATFile.flush();

// Write also status to DB
      for (i=0;i<nccb;++i) {
        myCcb[i]->MC_status_to_db(myRef[i]);
      }
    }

    apsleep(1000);
  }

  TFile.close();
  PFile.close();
  ATFile.close();

  return 0;
}

#else
int main() {
  std::cout << "Nothing done. Please compile with DB support.\n";
}
#endif
