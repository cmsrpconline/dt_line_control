#include <ccbcmds/apfunctions.h>
#include <ccbcmds/Cdef.h>
#include <ccbcmds/Ccommand.h>
#include <ccbcmds/Cccb.h>
#include <ccbcmds/Ci2c.h>
#include <ccbcmds/Clog.h>

#include <cstdio>
#include <iostream>
#include <fstream>
#include <ctime>

#define MONITINT 60 // PADC monitoring interval, sec
#define WRITEINT 60 // PADC logging interval, sec

//#define FILEROOT "/home/daqcms/logs/" // directory for logging
#define FILEROOT "/home/dtdqm/padc/" // directory for logging

// Author: A.Parenti, Feb08 2007
// Modified: AP, Apr19 2007
int main(int argc, char *argv[]) {

  short ccbport, ccbid;
  int i;
  char PLogFile[256]; // PADC data file (name)
  std::fstream PFile; // PADC data file

// Datetime
  time_t currenttime, ptime, wtime;
  struct tm *timeinfo;
  char datetime[256];

  char tempstr[2500];

// For PADCs
  bool padc_error;
  short adc_count[10];
  float volt_read[10];

// Reset
  wtime=ptime=0;

  if (argc<2) {
    printf("Usage: mc_padc_monitor [ccbport]\n");

    printf("ccbport? ");
    scanf("%d",&ccbport);
  } else {
    sscanf(argv[1],"%d",&ccbport);
  }

  Clog *myLog=new Clog("/dev/null"); // No ccbcmds log
  Ccommand *myCmd =new Ccommand(ccbport,CCBSERVER,CCBPORT,myLog);
  Cccb *myCcb=new Cccb(myCmd); // CCB object
  Ci2c *myI2c=new Ci2c(myCmd); // I2C object

// Get ccbid & create datafile name
  myCcb->MC_read_status();
  ccbid=myCcb->mc_status.Ccb_ID;
  if (myCcb->mc_status.code==0x13) {
    sprintf(PLogFile,"%smcpres_ccbid%d.txt",FILEROOT,myCcb->mc_status.Ccb_ID);
  } else {
    printf("Cannot communicate with MC.\n");
    return -1;
  }

// Open files for logging
  PFile.open(PLogFile,std::ios::out | std::ios::app); 
  if (!PFile.is_open()) {
    printf("Cannot open file %s for logging.\n",PLogFile);
    return -2;
  }

  printf("Starting PADC monitor. Please wait.\n");
  while(1) {
// Get datetime
    time(&currenttime);
    timeinfo = localtime(&currenttime);
    sprintf(datetime,"%d/%02d/%02d\t%02d:%02d",timeinfo->tm_year+1900,
            timeinfo->tm_mon+1,timeinfo->tm_mday,timeinfo->tm_hour,
            timeinfo->tm_min);

    if (currenttime-ptime>=MONITINT) {
      ptime=currenttime;
      vector_reset(adc_count,10); // Reset adc_count

      myCcb->MC_read_status();
      if (myCcb->mc_status.code==0x13 && ccbid!=myCcb->mc_status.Ccb_ID) {
// ccb has been changed: exit!
        printf("CCBID has changed (%d->%d): quitting!",ccbid,myCcb->mc_status.Ccb_ID);
	break;
      }

      myI2c->read_PADC(&padc_error,adc_count,volt_read);// Read PADCs
    }

// Print to stdout
    printf("%s",CLRSCR); // Clear screen
    printf("%s",STDBKG); // Standard BKG
    printf("Current time:            %s\n",get_time());
    printf("Last PADC reading:       %s",ctime(&ptime));
    printf("\n");
    printf("**********************************************************************\n");
    printf("CCBID: %d\n",ccbid);
    printf("PADC data: ");
    for (i=0;i<10;++i)
      printf(" %04d ",adc_count[i]);
    printf("\n");
    printf("**********************************************************************\n");

    if (myI2c->led_i2c.ccbserver_code==0 && currenttime-wtime>=WRITEINT) {
// Write every <WRITEINT> sec
      wtime=currenttime;

// Write PADC data
      PFile << datetime;
      strcpy(tempstr,"");
      for (i=0;i<10;++i)
        sprintf(tempstr,"%s\t%d",tempstr,adc_count[i]);

      PFile << tempstr << '\n';
      PFile.flush();
    }

    apsleep(1000);
  }

  PFile.close();
  return 0;
}
