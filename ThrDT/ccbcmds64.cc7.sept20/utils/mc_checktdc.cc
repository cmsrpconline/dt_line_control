#include <ccbcmds/apfunctions.h>
#include <ccbcmds/Ccommand.h>
#include <ccbcmds/Ccmn_cmd.h>
#include <ccbcmds/Cdef.h>

#include <iostream>
#include <stdio.h>

// Author: S.Ventura, Jul13 2006
// Modified: A. Parenti, May10 2007
int main(int argc, char *argv[]) {
  char brd_id, chip;
  short ccbport;
  Clog *myLog;
  Ccommand *myCmd;
  Ccmn_cmd *myCmn;

  char brdmsk[7]; // Mask of existing ROBs

  if (argc < 2) {
    printf("*** Usage: mc_checktdc [ccbport]\n");

    printf("ccbport? ");
    scanf("%d",&ccbport);
  }  else {
    sscanf(argv[1],"%d",&ccbport);
  }

  myLog = new Clog("log.txt"); // Log to file
  myCmd = new Ccommand(ccbport,CCBSERVER,CCBPORT,myLog);
  myCmn = new Ccmn_cmd(myCmd); // New CCB object
  
  printf("Using server %s:%d\n",myCmd->read_server_name(),myCmd->read_server_port());

  printf("\n\n\n");
  printf("*** Check tdc status ***\n");
  printf("*** Actual time: %s\n",get_time());

  myCmn->get_rob_mask(brdmsk);

  for (brd_id=0;brd_id<=6;++brd_id) {
    for (chip=0;chip<=3;++chip){

      if (((brdmsk[brd_id]>>chip)&1)!=1)
        continue; // ROB not existing, skipping.

      printf("Board:%d Chip:%d",brd_id,chip);
      myCmn->status_TDC(brd_id,chip); // 0x43
      myCmn->status_TDC_print();
      printf("\n");
    }
  }
}
