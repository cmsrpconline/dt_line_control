#include <ccbdb/Ccmsdtdb.h>
#include <ccbdb/Cccbmap.h>

#include <iostream>
#include <stdio.h>
#include <Cudpux.h>

#include "/nfshome0/dtdqm/TestCan_v0.6/CuOFCan.h"
#include "/nfshome0/dtdqm/TestCan_v0.6/CuOFBoard.h"
#include "/nfshome0/dtdqm/TestCan_v0.6/canerrors.h"


#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h> 


const std::string canConfFile("/nfshome0/dtdqm/TestCan_v0.6/iocanconf.txt");
const std::string boardConfFile("/nfshome0/dtdqm/TestCan_v0.6/iocanboards.txt");
const std::string adcConfFile("/nfshome0/dtdqm/TestCan_v0.6/iocanmap.txt");

// Author: A.Parenti, Jun29 2006
// Modified: AP, Jan26 2007
int main(int argc, char *argv[]) {
  cmsdtdb *mydb;
  int rows,fields;


/*  if (argc < 2) {
    printf("*** Usage: mc_temp [ccbport]\n");

    printf("ccbport? ");
    scanf("%d",&ccbport);
  }  else
    sscanf(argv[1],"%d",&ccbport);
*/


  mydb = new cmsdtdb();

 /* for (int ii=1;ii<2;ii++)
    for (int jj=0;jj<2;jj++)
	  for(int xx=0x0;xx<0x7d;xx++)
	    for(int me=0;me<4;me++)
		 	for(int on=0;on<8;on++)
			{
			    char mid[128];
			    char myque[1024];
              sprintf(mid,"w%dcuof%db%xm%do%d",ii,jj,xx,me,on);
										   
						printf("mid %s \n",mid);
			         sprintf(myque,"insert into SENSORMAP (ID, NAME, WHEEL, TAG, THR1) values ('%s','%s',%d,'<cuof/>',80)",mid,mid,ii);
						mydb->sqlquery(myque,&rows,&fields);
	       }

*/				

//	dtcan::CuOFCan my( canConfFile, adcConfFile );
	dtcan::CuOFCan my( canConfFile, boardConfFile, adcConfFile  );

  while (true) {
	 printf("Collecting data from CuOF\n");

	 Cudp theTemps(8431);
	 char bufre[1024];
	 char *value;


   std::map< std::string, dtcan::CuOFBoard >::iterator it = my.begin();
   std::map< std::string, dtcan::CuOFBoard >::iterator end = my.end();


         

        char mid[128];
        float mtemp;
        char myque[256];
/*        dtcan::adconet reply;
        for ( size_t i = 0; i < dtcan::CuOFBoard::nmezzanines; ++i ) {
           for ( size_t j = 0; j < dtcan::CuOFBoard::nonets; ++j ) {
             try {
                (it->second).readOnetAdc( i, j, reply );
                std::cout << reply;
*/
			dtcan::adconet reply[dtcan::CuOFBoard::ntotonets];
         try {
			  for ( ; it != end; ++it ) {
		      dtcan::CuOFBoard & thisboard = it->second;
				thisboard.readAllAdc( reply );
				for ( size_t i = 0; i < dtcan::CuOFBoard::ntotonets; ++i ) {
					if ( thisboard.isActiveMezzanine( reply[i] ) )
					{
						std::cout << reply[i];
           			sprintf(mid,"w%dcuof%db%2.2xm%do%d",-1,0,(unsigned int)thisboard.base(),reply[i].mezzanine,reply[i].onet);
           			mtemp=reply[i].temp;
           			printf("mid %s value %s temp %f\n",mid,value,mtemp);
           			sprintf(myque,"INSERT into SENSORTEMP (SENSORID,TEMP,TIME) VALUES ('%s',%f,CURRENT_TIMESTAMP)",mid,mtemp);
           			mydb->sqlquery(myque,&rows,&fields);
			  		}
				 }
				}
         } catch ( const readerror & e ) {
                std::cout << e.what() << std::endl;
              }
			  catch (...) {
			      std::cout << "Some exception occurred..." << std::endl;
					}


    printf("sleeping 1 minute.\n");
    sleep(15);
    printf(".");
    sleep(15);
    printf(".");
    sleep(15);
    printf(".");
    sleep(15);
    printf(".\n");
  }
}

