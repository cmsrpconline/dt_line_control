#include <ccbcmds/apfunctions.h>
#include <ccbcmds/Clog.h>
#include <ccbcmds/Ccommand.h>
#include <ccbcmds/Cccb.h>
#include <ccbcmds/Cdef.h>
#include <ccbdb/Ccmsdtdb.h>
#include <ccbdb/Cccbmap.h>

#include <iostream>
#include <stdio.h>
#include <Cudpux.h>
#include <unistd.h>

// Author: A.Parenti, Jun29 2006
// Modified: AP, Jan26 2007
int main(int argc, char *argv[]) {
  short ccbport;
  Clog *myLog;
  Ccommand *myCmd[10];
  //Ccommand *myCmd;
  Cccb *myCcb;
  char myque[256];
  cmsdtdb *mydb;
  int rows,fields;

  if (argc < 2) {
    printf("*** Usage: mc_temp [ccbport]\n");

    printf("ccbport? ");
    scanf("%d",&ccbport);
  }  else
    sscanf(argv[1],"%d",&ccbport);

  myLog = new Clog("log.txt"); // Log to file
  //myCmd = new Ccommand(ccbport,CCBSERVER,18885,myLog);


  mydb = new cmsdtdb();


#ifndef none
  //myCmd[0] = new Ccommand(ccbport,"10.176.60.136",18885,myLog); //spare
  //myCmd[0] = new Ccommand(ccbport,"10.176.60.236",18885,myLog);
  //2017nov4 YB-2 moved to spare vmepc
  myCmd[0] = new Ccommand(ccbport,"10.176.10.48",18885,myLog);
  //myCmd[0] = new Ccommand(ccbport,"10.176.10.38",18885,myLog);
  //2018-10-25  moved to spare myCmd[1] = new Ccommand(ccbport,"10.176.10.46",18885,myLog);
  myCmd[1] = new Ccommand(ccbport,"10.176.10.38",18885,myLog);
  myCmd[2] = new Ccommand(ccbport,"10.176.10.44",18885,myLog);
  //spare pc myCmd[2] = new Ccommand(ccbport,"10.176.10.38",18885,myLog); //now -11
  myCmd[3] = new Ccommand(ccbport,"10.176.10.42",18885,myLog);
  myCmd[4] = new Ccommand(ccbport,"10.176.10.40",18885,myLog);

  //myCmd[5] = new Ccommand(0x3a29,"10.176.60.136",18885,myLog); //spare
  //i2017nov4 YB-2 moved to spare vmepc
  myCmd[5] = new Ccommand(0x3a29,"10.176.10.48",28880,myLog);
  //2018-10-25 moved to spare myCmd[6] = new Ccommand(0x3a27,"10.176.10.46",28880,myLog);
  myCmd[6] = new Ccommand(0x3a27,"10.176.10.38",28880,myLog);
  myCmd[7] = new Ccommand(0x3a25,"10.176.10.44",28880,myLog);
  //spare pc myCmd[7] = new Ccommand(0x3a25,"10.176.10.38",28880,myLog); //now -11
  myCmd[8] = new Ccommand(0x3a23,"10.176.10.42",28880,myLog);
  myCmd[9] = new Ccommand(0x3a21,"10.176.10.40",28880,myLog);

  for (int ii=0;ii<10;ii++) if (myCmd[ii]<=0 ) printf("errors in cmd constructioni %d\n",ii);

  while (true) {
   for(int whe=0;whe<5;whe++)
  {
    printf("Using server %s:%d\n",myCmd[whe]->read_server_name(),myCmd[whe]->read_server_port());
    //printf("Using server %s:%d\n",myCmd->read_server_name(),myCmd->read_server_port());
    printf("\n\n\n");
    printf("*** Temperature Monitor ***\n");
    printf("*** Actual time: %s\n",get_time());
    //myCcb = new Cccb(myCmd); // New CCB object
   for (int k=0;k<6;k++) {
     printf ("try banktonow %d \n" , k);
    myCcb = new Cccb(myCmd[whe]); // New CCB object
     myCcb->MC_temp(k); // 0x3C
     if (( myCcb->mc_temp.ccbserver_code) < 0)
     {
        delete myCcb;
        myCcb = new Cccb(myCmd[whe+5]);
        myCcb->MC_temp(k); // 0x3C
     }
    //myCcb->MC_temp_print();
     printf("Branch %d:\n",k);

     for (int ii = 0 ; ii<20;ii++)
     {
       printf(" Sensor %s temp:%f\n",myCcb->mc_temp.sensorcodes[ii],myCcb->mc_temp.temp[ii]);
       if (strncmp(myCcb->mc_temp.sensorcodes[ii],"10",2)==0)
     //if ((myCcb->mc_temp.sensorcodes[ii][0]=='1') && (myCcb->mc_temp.sensorcodes[ii][0]=='1'))
       {
        sprintf(myque,"INSERT into SENSORTEMP (SENSORID,TEMP,TIME) VALUES ('%s',%f,CURRENT_TIMESTAMP)",myCcb->mc_temp.sensorcodes[ii],myCcb->mc_temp.temp[ii]);
        mydb->sqlquery(myque,&rows,&fields);
       }
       printf ("iptonow %d \n" , ii);
     }
     printf ("banktonow %d \n" , k);
     delete myCcb;
	  }

	   for (int ll = 0; ll<2; ll++)
	    for (int kk = 0; kk<5; kk++)
	    { 
        char mip[256],low[64];
		  ccbmap myccbmap(mydb);
		   int mpo,msec,msta; 
        if (kk<4) 
		   {
			 msec=ll*6+4;
			 msta=kk+1;
			}
        if (kk==4) 
		  	{		if (ll==0)msec=13;
					else msec=14;
					msta=4;
			}
			mpo=0;
         if (myccbmap.retrieve(2-whe,msec,msta)!=0) {
		      printf("MC not found in DB.\n");
				mpo=-1;
		    }
			 printf("whe %d msec %d msta %d\n",2-whe ,msec , msta );
		  mpo=myccbmap.port;
		  strcpy(mip,"10.176.10.");
        
        sprintf(low,"%d",40 + whe *2);
//	  if (whe==4) strcpy(low,"136"); // to swap with the spare
        strcat(mip,low);
		  printf ("port %d on %s\n",14+kk+(ll*28),mip);
		  if (mpo == -1) break;
	     Ccommand *extCmd = new Ccommand(mpo,mip,18889,myLog);
        myCcb = new Cccb(extCmd); // New CCB object
        myCcb->MC_temp(0); // 0x3C
        for (int ii = 15 ; ii<18;ii++)
        {
           printf(" Sensor %s temp:%f\n",myCcb->mc_temp.sensorcodes[ii],myCcb->mc_temp.temp[ii]);
           if (strncmp(myCcb->mc_temp.sensorcodes[ii],"10",2)==0)
           {
               sprintf(myque,"INSERT into SENSORTEMP (SENSORID,TEMP,TIME) VALUES ('%s',%f,CURRENT_TIMESTAMP)",myCcb->mc_temp.sensorcodes[ii],myCcb->mc_temp.temp[ii]);
               mydb->sqlquery(myque,&rows,&fields);
            }
           printf ("iptonow %d \n" , ii);
        }
       delete myCcb;
       delete extCmd;
       }

   }
	printf("Done with all sensors\n");
/*   for (int ii=0;ii<10;ii++)
	{
		delete myCmd[ii];
	 	printf("deleted myCmd %d\n",ii);
	}
	*/
   /*delete myCmd[0];
   delete myCmd[1];
   delete myCmd[2];
   delete myCmd[3];
   delete myCmd[4];
   delete myCmd[5];
   delete myCmd[6];
   delete myCmd[7];
   delete myCmd[8];
   delete myCmd[9];
	*/


#endif
	 printf("Collecting data from DSS\n");

	 Cudp theTemps(8431);
	 char bufre[1024];
	 char *value;


    theTemps.SendTo("10.176.10.56",10000,"12temp",6);
	 int ret=theTemps.Receive(bufre,512,3);
	 
	 printf("Rec %d Buffer %s \n",ret,bufre);
	 strtok(bufre,"*");
	 if (ret>0) 
	 {
	    int val=0;
	    while (value=strtok(NULL,"*"))
	    {
	     char mid[128];
		  float mtemp;

		  sprintf(mid,"sc%d%s%03d",val/6-2,((val/3)%2)?"down":"up",val%3+1);
		  sscanf(value,"%f",&mtemp);
		  printf("mid %s value %s temp %f\n",mid,value,mtemp);
	     sprintf(myque,"INSERT into SENSORTEMP (SENSORID,TEMP,TIME) VALUES ('%s',%f,CURRENT_TIMESTAMP)",mid,mtemp);
		  mydb->sqlquery(myque,&rows,&fields);
		  val++;
        }	
	 }

    theTemps.SendTo("10.176.10.56",10001,"12relays",8);
    ret=theTemps.Receive(bufre,512,3);

    printf("Rec %d Buffer %s \n",ret,bufre);
    strtok(bufre,"relays");
    if (ret>0)
    {
       int val=0;
       while (value=strtok(NULL,"relays"))
       {
        char mid[128];
        float mtemp;

        sscanf(value,"%f",&mtemp);
        printf("mid %s value %s temp %f\n","relays",value,mtemp);
        sprintf(myque,"INSERT into SENSORTEMP (SENSORID,TEMP,TIME) VALUES ('%s',%f,CURRENT_TIMESTAMP)",mid,mtemp);
        mydb->sqlquery(myque,&rows,&fields);
        val++;
        }
    }
    theTemps.SendTo("10.176.10.56",10001,"12dsslv",8);
    ret=theTemps.Receive(bufre,512,3);

    printf("Rec %d Buffer %s \n",ret,bufre);
    strtok(bufre,"dsslv");
    if (ret>0)
    {
       int val=0;
       while (value=strtok(NULL,"dsslv"))
       {
        char mid[128];
        float mtemp;

        sscanf(value,"%f",&mtemp);
        printf("mid %s value %s temp %f\n","dsslv",value,mtemp);
        sprintf(myque,"INSERT into SENSORTEMP (SENSORID,TEMP,TIME) VALUES ('%s',%f,CURRENT_TIMESTAMP)",mid,mtemp);
        mydb->sqlquery(myque,&rows,&fields);
        val++;
        }
    }

    printf("sleeping 1 minute.\n");
    sleep(15);
    printf(".");
    sleep(15);
    printf(".");
    sleep(15);
    printf(".");
    sleep(15);
    printf(".\n");
  }
}
