#include <iostream>
#include <stdio.h>
#include <string.h>

#ifndef __USE_CCBDB__ // No DB access
int main() {
  std::cout << "Nothing done. Please compile with DB support.\n";
}
#else

#include <ccbdb/Cccbrefdb.h>
#include <ccbcmds/Cdef.h> // Defines DSN
#include <ccbcmds/apfunctions.h>

#define XML_MAX_DIM 9000

// Author: A. Parenti, Jan30 2006
// Modified: AP, Aug13 2007
// Tested on MySQL/Oracle
int main(int argc, char *argv[]) {
  ccbrefdb myccbref;

  char filename[LEN1]="", content[XML_MAX_DIM]="", tmpstr[LEN1]="", c;
  char strrtype[LEN1]="";
  short mctype;
  reftype rtype;
  std::string cmnt="";

  if (argc != 5) {
    printf("*** This program insert an entry in the ccbref table.\n");
    printf("*** Usage: %s [filename mctype reference_type comment]\n",argv[0]);

    printf("Filename? ");
    while ((c=getchar()) != '\n')
      sprintf(filename,"%s%c",filename,c);

    strcpy(tmpstr,"");
    printf("mctype? ");
    while ((c=getchar()) != '\n')
      sprintf(tmpstr,"%s%c",tmpstr,c);
    sscanf(tmpstr,"%d",&mctype);

    printf("Reference type? (a=boot, b=status, c=test, d=rob, e=padc) ");
    while ((c=getchar()) != '\n')
      sprintf(strrtype,"%s%c",strrtype,c);

    printf("Do you want to add a comment? ");
    while ((c=getchar()) != '\n')
      cmnt += c;

  } else {
    strcpy(filename,argv[1]);
    sscanf(argv[2],"%d",&mctype);
    strcpy(strrtype,argv[3]);
    cmnt=argv[4];
  }

  strcpy(content,read_file(filename));
  if (strlen(content)==0) {
    printf("File not existing or empty.\n");
    return -1;
  }

  switch(strrtype[0]) {
  case 'a':
  case 'A':
    rtype=boot;
    break;
  case 'b':
  case 'B':
    rtype=status;
    break;
  case 'c':
  case 'C':
    rtype=test;
    break;
  case 'd':
  case 'D':
    rtype=rob;
    break;
  case 'e':
  case 'E':
    rtype=padc;
    break;
  default:
    printf("Wrong type (%s).\n",strrtype);
    return -1;
  }

//  printf("Filename: '%s'\n",filename);
//  printf("File Content: '%s'\n",content);
//  printf("McType: '%d'\n",mctype);
//  printf("rtype: %d\n",rtype);
//  printf("Comment: '%s'\n",cmnt.c_str());
//
//  return 0;

  myccbref.insert(mctype,rtype,content,(char*)cmnt.c_str());

  return 0;
}

#endif
