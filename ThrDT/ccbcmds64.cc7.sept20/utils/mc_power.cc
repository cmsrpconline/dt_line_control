#include <ccbcmds/apfunctions.h>
#include <ccbcmds/Clog.h>
#include <ccbcmds/Ccommand.h>
#include <ccbcmds/Cdef.h>

#include <iostream>
#include <stdio.h>
#include <string>
#include <fstream>

using namespace std;
// Author: A.Parenti, Aug07 2007
// LG modify to read a text file with delays and send commands:
// writeTTC (address 2) and TTCSkew
int main(int argc, char *argv[]) {
  bool doit = true;
  char c, cmdstr[3*WRITE_DIM]="", *errstr;
  short ccbport;
  int i, errcod, iwr;
  char cfilename[64];
  char cwrred[64];
  unsigned char sntline[WRITE_DIM], rdline[READ_DIM];
  int sntlinelen, rdlinelen;
  
  Clog *myLog;
  Ccommand *myCmd;
  bool user_set = false;
  int user_wh;
  int user_sec = 0; 
  int user_chamb = 0;
  int user_rob = -1;
  string mhost;
  printf("%d\n",argc);
  if (argc < 3) {
    printf("*** Usage: mc_set_clock [filename] {wheel} {sector} {chamber}\n");
    //    printf("*** [write/read] is compulsory\n")
    printf("*** [filename] is compulsory\n");
    printf("*** {wheel}, {sector}, {chamber} are optional\n");
    printf("***  (use them to override enables from file)\n");
    printf("Exiting...\n");
    return 1;
  }  else {
    //    sscanf(argv[1],"%s",cwrrd);
    sscanf(argv[1],"%s",cfilename);
    /*    if (cwrrd != "write" && cwrrd != "read"){
       printf("*** Usage: mc_set_delays [write/read] [filename] {wheel} {sector} {chamber}\n");
       printf("*** [write/read] parameter can be either \"write\" or \"read\" (without \"s)\n");
       printf("  Exiting...\n"); 
       return 2; 
    }
    if (cwrrd == "write") iwr=1; else iwr=0;*/
  }
  if (argc ==3){
    user_set = true;
    sscanf(argv[2],"%d",&user_wh);
  }
  if (argc == 4){
     user_set = true;
     sscanf(argv[2],"%d",&user_wh);
     sscanf(argv[3],"%d",&user_sec);
      }
  if (argc == 5){
    user_set = true;
    sscanf(argv[2],"%d",&user_wh);
    sscanf(argv[3],"%d",&user_sec);
    sscanf(argv[4],"%d",&user_chamb);
  }
  if (argc == 6){
    user_set = true;
    sscanf(argv[2],"%d",&user_wh);
    sscanf(argv[3],"%d",&user_sec);
    sscanf(argv[4],"%d",&user_chamb);
    sscanf(argv[5],"%d",&user_rob);
  }
  // Open the file here
  fstream infile;
  infile.open(cfilename,ios::in);
  if (!infile.is_open()) {
    printf("Error opening file [%s]! Exiting...\n",cfilename);
    return 1;
  }
  else {
    printf("File [%s] successfully opened.\n",cfilename);
  }
  string spass;
  for (int skip=0; skip<7; skip++){
    infile>>spass;
    //cout<<spass<<endl;
  }
  //return 0;
  int wheel, sector, mb, coarse, fine, port, enable;
  const char setcoarsecmd[5] = "27 2";
  const char setskewcmd[3] = "48";
  const char robreset[3] ="32";
  //  const char setckon[6];
  //  if (enable==1) {
  const char setckon[6] = "2A 17";
  //  } 
    //  else {
  const char setckoff[6] = "2A 14";
  //  const char powerrob[9];
//   if (user_rob == 0) {
//     const char powerrob[9] = "2F 15 01";
//   }
//   else if (user_rob == 1) {
//     const char powerrob[9] = "2F 16 01";
//   }
//   else if (user_rob == 2) {
//     const char powerrob[9] = "2F 17 01";
//   }
//   else if (user_rob == 3) {
//     const char powerrob[9] = "2F 18 01";
//   }
//   else if (user_rob == 4) {
//     const char powerrob[9] = "2F 19 01";
//   }
//   else if (user_rob == 5) {
//     const char powerrob[9] = "2F 1A 01";
//   }
  //  const char powerrob[9] = which;
  //  }    
  int iter=0;
  int oldwheel=100;
  // loop until end of file
  while(!infile.eof()){
    infile>>enable>>wheel>>sector>>mb>>coarse>>fine>>port;
    //    printf("%d %d %d %d %d %d %d\n",enable, wheel, sector, mb, coarse, fine, port);
    //    printf("%d %d %d\n",user_wh,user_sec,user_chamb);
    if (wheel<-2 || wheel>2 || coarse<0 || coarse>7) return 0;
    //printf("ITERATION: %d\n",iter);
    bool dothis;
    int enableg = 1;
    if (user_set){
      if (user_sec==0){
	if (wheel==user_wh && enableg==1)  dothis=true; else  dothis=false;
      }
      else{
	if (user_chamb == 0){
	  if ((wheel==user_wh) && (sector==user_sec) && enableg==1) dothis=true; else dothis=false;
	}
	else{
	  if ((wheel==user_wh) && (sector==user_sec) && (mb==user_chamb) && enableg==1) dothis=true; else dothis=false;
	}
      }
    }
    else{
      if (enableg==1) dothis=true; else dothis=false;
    }

    //    dothis = (user_set)? ( (user_sec==0)? (wheel==user_wh):(wheel==user_wh && sector==user_sec)) : (enable == 1);
    //    dothis = true;
    if (dothis){
      if (wheel == -2) mhost="vmepcS1D12-16.cms";
      if (wheel == -1) mhost="vmepcS1D12-17.cms";
      if (wheel == 0) mhost="vmepcS1D12-18.cms";
      if (wheel == 1) mhost="vmepcS1D12-19.cms";
      if (wheel == 2) mhost="vmepcS1D12-20.cms";
      //const char *chost;
      //chost=mhost.c_str();
      char cchost[19];
      sprintf(cchost,"%s",mhost.c_str());

      //      printf("AAAAAAAAAAAAAAAAAAAAA");




//       if (enable==1){
// 	printf("Setting QPLL clock for W%d S%d MB%d: Port %d... ",wheel,sector,mb,port);
//       }
//       else {
// 	printf("Setting TTC clock for W%d S%d MB%d: Port %d... ",wheel,sector,mb,port);
//       }
      coarse = coarse*16+coarse;
      ccbport = port;
      
      // prepare coarse delay command and execute whatever needed
      if (user_rob == -1) {
	//	const char powerrob[9] = "2F 15 01";
	sprintf(cmdstr,"%s","2F FF 01");
      }

      //ROB
      if (user_rob == 0) {
	//	const char powerrob[9] = "2F 15 01";
	sprintf(cmdstr,"%s","2F 15 01");
      }
      if (user_rob == 1) {
	//	const char powerrob[9] = "2F 15 01";
	sprintf(cmdstr,"%s","2F 16 01");
      }
      if (user_rob == 2) {
	//	const char powerrob[9] = "2F 15 01";
	sprintf(cmdstr,"%s","2F 17 01");
      }
      if (user_rob == 3) {
	//	const char powerrob[9] = "2F 15 01";
	sprintf(cmdstr,"%s","2F 18 01");
      }
      if (user_rob == 4) {
	//	const char powerrob[9] = "2F 15 01";
	sprintf(cmdstr,"%s","2F 19 01");
      }
      if (user_rob == 5) {
	//	const char powerrob[9] = "2F 15 01";
	sprintf(cmdstr,"%s","2F 1A 01");
      }
      if (user_rob == 6) {
	//	const char powerrob[9] = "2F 15 01";
	sprintf(cmdstr,"%s","2F 1B 01");
      }

      //TRB
      if (user_rob == 7) {
	//	const char powerrob[9] = "2F 15 01";
	sprintf(cmdstr,"%s","2F 0D 01");
      }
      if (user_rob == 8) {
	//	const char powerrob[9] = "2F 15 01";
	sprintf(cmdstr,"%s","2F 0E 01");
      }
      if (user_rob == 9) {
	//	const char powerrob[9] = "2F 15 01";
	sprintf(cmdstr,"%s","2F 0F 01");
      }
      if (user_rob == 10) {
	//	const char powerrob[9] = "2F 15 01";
	sprintf(cmdstr,"%s","2F 10 01");
      }
      if (user_rob == 11) {
	//	const char powerrob[9] = "2F 15 01";
	sprintf(cmdstr,"%s","2F 11 01");
      }
      if (user_rob == 12) {
	//	const char powerrob[9] = "2F 15 01";
	sprintf(cmdstr,"%s","2F 12 01");
      }
      if (user_rob == 13) {
	//	const char powerrob[9] = "2F 15 01";
	sprintf(cmdstr,"%s","2F 13 01");
      }
      if (user_rob == 14) {
	//	const char powerrob[9] = "2F 15 01";
	sprintf(cmdstr,"%s","2F 14 01");
      }




      //      sprintf(cmdstr,"%s",powerrob);
//       if (enable==1){
// 	sprintf(cmdstr,"%s",setckon);
//       }
//       else {
// 	sprintf(cmdstr,"%s",setckoff);
//       }

      //      printf("command to be sent: %s ",cmdstr);
	errstr=new char[1024];
      string_to_array(cmdstr,16,sntline,&sntlinelen); // Convert to ccb format    
      //#ifdef LOGTODB
      //    myLog = new Clog(); // Log to DB
      //#else
      myLog = new Clog("log.txt"); // Log to file
      //#endif
      if (doit) {
	myCmd = new Ccommand(ccbport,cchost,CCBPORT,myLog);
	myCmd->set_write_log(true); // Switch on logger
      }
      
      // printout of server settings and current time only at first iteration
      if (oldwheel != wheel) {
	oldwheel=wheel;
	printf("\n Using server %s:%d\n",myCmd->read_server_name(),myCmd->read_server_port());
	printf("*** Actual time: %s\n",get_time());
      }
      if (doit){
	errcod=myCmd->send_command(sntline,sntlinelen,TIMEOUT_DEFAULT);
	errstr=myCmd->read_error_message(errcod);
      }

      if (enable==1){
	printf("Setting QPLL clock for W%d S%d MB%d: Port %d... ",wheel,sector,mb,port);
      }
      else {
	printf("Setting TTC clock for W%d S%d MB%d: Port %d... ",wheel,sector,mb,port);
      }

      printf("command to be sent: %s ",cmdstr);

      bool ok =false;
      
      // Check returned data and compare to expected. If error, print immediately, otehrwise set
      // bool state to ok and wait for the second command (fine delay) before printing to screen
      if (doit){
	if ((myCmd->data_read_len == 4) && 
	    (myCmd->data_read[0] == 0xfc && 
	     myCmd->data_read[1] == 0x2f)) { 
	     //	     myCmd->data_read[2] == 0x00)){
	  printf("  ---> OK\n");
	}else{
	  printf("NOT OK!\n");
	  for (i=0; i<(myCmd->data_read_len); ++i)
	    printf(" %02X",myCmd->data_read[i]);
	  putchar('\n');
	  printf("Error code: %s\n",errstr);
	}
      }
      
//       // redo for the second command, fine delay
//       sprintf(cmdstr,"%s %x",setskewcmd,fine);
//       errstr=new char[1024];
//       string_to_array(cmdstr,16,sntline,&sntlinelen); // Convert to ccb format
//       //#ifdef LOGTODB
//       //    myLog = new Clog(); // Log to DB
//       //#else
//       myLog = new Clog("log.txt"); // Log to file
//       //#endif
//       if (doit){
// 	myCmd = new Ccommand(ccbport,cchost,CCBPORT,myLog);
// 	myCmd->set_write_log(true); // Switch on logger
// 	errcod=myCmd->send_command(sntline,sntlinelen,TIMEOUT_DEFAULT);
// 	errstr=myCmd->read_error_message(errcod);
// 	if ((myCmd->data_read_len == 3)&&
// 	    (myCmd->data_read[0] == 0xfc && myCmd->data_read[1] == 0x48 && myCmd->data_read[2] == 0x00)){
// 	  if (ok) printf("OK");
// 	}else{
// 	  ok = false;
// 	  printf("NOT OK!\n");
// 	  printf("CCB reply:");
// 	  for (i=0; i<(myCmd->data_read_len); ++i)
// 	    printf(" %02X",myCmd->data_read[i]);
// 	  putchar('\n');
// 	  printf("Error code: %s\n",errstr);
// 	}
//       }

//  // do the rob reset
//       sprintf(cmdstr,"%s",robreset);
//       errstr=new char[1024];
//       string_to_array(cmdstr,16,sntline,&sntlinelen); // Convert to ccb format
//       //#ifdef LOGTODB
//       //    myLog = new Clog(); // Log to DB
//       //#else
//       myLog = new Clog("log.txt"); // Log to file
//       //#endif
//       if (doit){
// 	myCmd = new Ccommand(ccbport,cchost,CCBPORT,myLog);
// 	myCmd->set_write_log(true); // Switch on logger
// 	errcod=myCmd->send_command(sntline,sntlinelen,TIMEOUT_DEFAULT);
// 	errstr=myCmd->read_error_message(errcod);
// 	if ((myCmd->data_read_len == 2)&&
// 	    (myCmd->data_read[0] == 0xfc && myCmd->data_read[1] == 0x32)){
// 	  if (ok) printf(" ROB Reset OK\n");
// 	}else{
// 	  ok = false;
// 	  printf(" ROB Reset NOT OK!\n");
// 	  printf("CCB reply:");
// 	  for (i=0; i<(myCmd->data_read_len); ++i)
// 	    printf(" %02X",myCmd->data_read[i]);
// 	  putchar('\n');
// 	  printf("Error code: %s\n",errstr);
// 	}
//       }



      //if (!ok) return 1;
      iter++;  
    }
  }
  return 0;  
}
