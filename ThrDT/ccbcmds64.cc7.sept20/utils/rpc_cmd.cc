#include <ccbcmds/apfunctions.h>
#include <ccbcmds/Cdef.h>
#include <ccbcmds/Ccommand.h>
#include <ccbcmds/Cccb.h>
#include <ccbcmds/Ci2c.h>
#include <ccbcmds/Clog.h>

#include <cstdio>
#include <iostream>
#include <fstream>
#include <ctime>


#define PCA9544 0x70 
#define AD7417  0x28
#define ciccio "10.176.60.236"

int main() {

  short ccbid;
  short ctrl_data[I2C_DATA_MAX];
  short err_data[I2C_DATA_MAX];
  
  char mccbsrv[64];

  strcpy(mccbsrv,"127.0.0.1  ");
  // I need to select the Minicrate via Wheel Sector Station

  Clog *myLog=new Clog("ciccio.txt"); // No ccbcmds log

  //  ccbid = 28;
  int ccbport=10;
  std::cout << "Open COnnection on "<<ciccio<<":"<<ccbport
	    <<" for ccb id "<<ccbid
	    <<std::endl;

  //Ccommand *myCmd =new Ccommand(ccbport,mccbsrv,CCBPORT,myLog);
  Ccommand *myCmd = new Ccommand(1,ccbport,"127.0.0.1",18885,myLog);

/*
 if(!myCmd->connect()){

    std::cout <<" No connection "<<std::endl;
    return -1;
  }*/
  
  std::cout << " done it!" <<myCmd<<std::endl;
  Cccb *myCcb=new Cccb(myCmd); // CCB object
  Ci2c *myI2c=new Ci2c(myCmd); // I2C object
  short size;
  // enable mux
  // write on PCA9544 + 7 RB1,2_in RB3,RB4
  // write on PCA9544 + 6 RB1,2_out

//Enabling Slow-Ctrl
  ctrl_data[ 0]=0x0100 + PCA9544+7;//start I2C sequence on Distr. Board U10
  ctrl_data[ 1]=0x0000 + 0x4; // wrI2C:  Enable U10/Ch0
  ctrl_data[ 2]=0x0200 + PCA9544+7;//stop I2C sequence on Distr. Board U10
  size = sizeof(ctrl_data)/sizeof(short);
  myI2c->rpc_I2C(ctrl_data, size);


  ctrl_data[ 0]=0x0100 + PCA9544+1;//start I2C sequence on Distr. Board U4
  ctrl_data[ 1]=0x0000 + 0x4;//  wrI2C:  Enable U4 Forward Eta Partition 
  ctrl_data[ 2]=0x0000 + 0x5;//  wrI2C:  Enable U4 Backward Eta Partition 
  ctrl_data[ 3]=0x0200 + PCA9544+1;//stop I2C sequence on Distr. Board U4
  size = sizeof(ctrl_data)/sizeof(short);
  myI2c->rpc_I2C(ctrl_data, size);

  // Read VTH1 (0x20) and VTH2 (0x040) of the first feb (ifeb=0)
  unsigned char ifeb = 0;
  ctrl_data[ 0]=0x0100 + AD7417+ifeb;//start I2C sequence on first FEB
  ctrl_data[ 1]=0x0000 + 0x01 + (0x20<<8); // Write Addr. Point CONF_REG
  ctrl_data[ 2]=0x0000 + 0x04; //Prepare for ADC Read
  ctrl_data[ 3]=0x0200 + AD7417+ifeb; //stop I2C sequence
  size = sizeof(ctrl_data)/sizeof(short);
  myI2c->rpc_I2C(ctrl_data, size);

  ctrl_data[ 0]=0x0100 + AD7417+ifeb; //start I2C sequence on first FEB
  ctrl_data[ 1]=0x0400; //read
  ctrl_data[ 2]=0x0200 + AD7417+ifeb; //stop I2C sequence
  size = sizeof(ctrl_data)/sizeof(short);
  myI2c->rpc_I2C(ctrl_data, size);

  Si2c_1 res = myI2c->rpc_i2c;
  char err_size = res.size;
  short result = (res.err_data[12]&0x0FFF);
  int nout  = (result & 0xFFC0) >> 6;
  float vdac = nout * 2.5/1.024; /* convert in mV*/
  std::cout <<"Thr = "<<vdac<<" mVolts"<<std::endl;
  myI2c->rpc_I2C_print();
  return 0;
}

