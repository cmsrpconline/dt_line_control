#include <ccbcmds/apfunctions.h>
#include <ccbcmds/Clog.h>
#include <ccbcmds/Ccommand.h>
#include <ccbcmds/Cccb.h>
#include <ccbcmds/Cdef.h>
#include "../ccbdb/Cccbdata.h"

#ifdef __USE_CCBDB__ // DB access enabled
  #include <ccbdb/Ccmsdtdb.h>
#endif

#include <iostream>
#include <stdio.h>

char CCBSERVERm2[15]="10.176.60.236";  
char CCBSERVER0[15]="10.176.60.232";  
char CCBSERVERm1[15]="10.176.60.234";  
char CCBSERVER2[15]="10.176.60.228";  
char CCBSERVER1[15]="10.176.60.230";


int execute( int & ccbid, int & ccbport, cmsdtdb & mydtdtb,
	     char * mccbsrv, Clog * myLog );

// Author: A.Parenti, Jul01 2006
// Modified: AP, Aug30 2007
#ifdef __USE_CCBDB__ // DB access enabled
int main(int argc, char *argv[]) {
  bool mc_program;
  char message[1024];
  int pippo, ccbport, ccbid, secport;

  short wheel = 100;
  unsigned short sector = 100;
  unsigned short station = 100;

  Emcstatus mc_phys_st, mc_logic_st;

  Clog *myLog;
  char mccbsrv[64];


  if (argc==4) {

    sscanf(argv[1],"%d",&pippo);
    wheel=pippo;

    if ( wheel > 3 || wheel < -3 ) {
      printf("Wrong wheel number: %d\n", pippo);
      printf("Usage: mc_checkTDCsStatusCcb [wheel] [sector] [station]\n");
      return -1;
    }

    sscanf(argv[2],"%d",&pippo);
    sector=pippo;

    if ( sector > 12 ) {
      printf("Wrong wheel number: %d\n", pippo);
      printf("Usage: mc_checkTDCsStatusCcb [wheel] [sector] [station]\n");
      return -1;
    }

    sscanf(argv[3],"%d",&pippo);
    station=pippo;

    if ( station > 4 ) {
      printf("Wrong station number: %d\n", pippo);
      printf("Usage: mc_checkTDCsStatusCcb [wheel] [sector] [station]\n");
      return -1;
    }

  } else {
    printf("Usage: mc_checkTDCsStatusCcb [wheel] [sector] [station]\n");

    printf("port? ");
    scanf("%d",&pippo);
    wheel=pippo;

    printf("ccbid? ");
    scanf("%d",&pippo);
    sector=pippo;

    printf("wheel? ");
    scanf("%d",&pippo);
    station=pippo;

  }

  switch ( wheel ) {
  case -2 : sprintf( mccbsrv, "%s", CCBSERVERm2 ); break;
  case -1 : sprintf( mccbsrv, "%s", CCBSERVERm1 ); break;
  case 0 : sprintf( mccbsrv, "%s", CCBSERVER0 ); break;
  case 1 : sprintf( mccbsrv, "%s", CCBSERVER1 ); break;
  case 2 : sprintf( mccbsrv, "%s", CCBSERVER2 ); break;
  default :     printf( "CAZZI!!! %d\n", pippo ); return(-1);
  } 

  // DB
  cmsdtdb mydtdtb;
  ccbdata data( &mydtdtb );
  data.ccbId( wheel, sector, station, ccbid, ccbport, secport );
  printf( "\nCCB %d [port %d] YB%d S%d MB%d\n",
	  ccbid, ccbport, wheel, sector, station );

  // objects
  myLog = new Clog("log.txt"); // Log to file

  if ( execute(ccbid, ccbport, mydtdtb, mccbsrv, myLog) < 0 ) {
    std::cout << "Failed access: trying with secondary port "
	      << secport << std::endl;
    execute(ccbid, secport, mydtdtb, mccbsrv, myLog);
  }

  delete myLog;
  
}



int execute( int & ccbid, int & ccbport, cmsdtdb & mydtdtb,
	     char * mccbsrv, Clog * myLog )
{

  //myCmd = new Ccommand(ccbid,ccbport,mccbsrv,CCBPORT,myLog);
  Ccommand * myCmd =
    new Ccommand(ccbid,ccbport,1000+ccbid,mccbsrv,CCBPORT,myLog);
  Cccb * myCcb = new Cccb(myCmd); // New CCB object
  Ccmn_cmd * myCmn = new Ccmn_cmd(myCmd); // New CCB object

  Cref * myRef = new Cref(ccbid,&mydtdtb); // read reference from DB

  // logs
  printf("Using server %s:%d\n",
	 myCmd->read_server_name(), myCmd->read_server_port());
  printf("\n*** Actual time: %s\n",get_time());

  myCcb->verbose_mode(true);
  myCcb->check_TDCs_status();
  if ( myCcb->status_mc_tdcs.code != 0xBD )
    return -1;


  std::cout << "CODE : " << int(myCcb->status_mc_tdcs.errorcode) << std::endl;
  std::cout << "MSG : " << myCcb->status_mc_tdcs.msg << std::endl;
  std::cout << "XML : " << myCcb->status_mc_tdcs.xmlmsg << std::endl;
  // printf("*** Command return code: %d\n",   myCcb->read_tdc.ccbserver_code);

  unsigned int errors = 0;

  if ( myCcb->status_mc_tdcs.error ) {
    ++errors;
  
    for (unsigned int idx=0 ; idx < 4; ++idx ) {
      for ( unsigned int j=0; j<8 ; ++j ) {

	bool val = takebit( myCcb->status_mc_tdcs.status[3-idx], j );
	
	if ( val ) {
	  ++errors;
	  unsigned int brd_id = j/4+2*idx;
	  unsigned int chip = j%4;
	  myCmn->status_TDC(brd_id,chip); // 0x43
	  myCmn->status_TDC_print();
	  printf( "DEBUG %d %d %d\n", 3-idx, j, val );
	  printf( "ERROR IN ROB%d chip %d message %s \n", 
	  	  brd_id, chip, myCcb->status_tdc.msg.c_str() );
	  // brd_id, chip, j,  myCcb->status_tdc.msg.c_str() );
	  //  }
	}
      }
    }

  }

  // clean up memory
  delete myCmd;
  delete myCcb;
  delete myRef;
  delete myCmn;

  return errors;
}




#else
int main() {
  std::cout << "Nothing done. Please compile with DB support.\n";
}
#endif
