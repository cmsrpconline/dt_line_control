#ifndef _ccbconfdb_
#define _ccbconfdb_
#include "Ccmsdtdb.h"

enum Ebrktype {brk_other=0,brk_bti_phi,brk_bti_theta,brk_traco,brk_traco_luts,brk_tss,brk_tsm,brk_rob,brk_feb_thr,brk_feb_width,brk_feb_mask};

class ccbconfdb {

 public:
  ccbconfdb(); // Create a new connection to DB
  ccbconfdb(cmsdtdb* dtdbobj); // Use an existing connection to DB
  ~ccbconfdb();

  int updaterun(int nrun); // Set <Run> in "configsets.Run" and cmsdtdb obj

  int confselect(char *); // Select a configuration by using "configsets.Name"

  int retrieveconfs(int*,char[][64]); // Retrieve all conf's from <configsets>
// Retrieve commands from <configcmds>, selecting by "configsets.Name" and "ccbrelations.CcbID"

  int retrievecmds(int ccbid,int *howmany);// Retrieve just the number of rows
  int retrievecmds(int ccbid,int *howmany,char cmds[][512]); // Retrieve all command lines
  int retrievecmds(int ccbid,int *howmany,char cmds[][512],int nmax); // Retrieve (at most <n>) command lines

  int retrievecmds(int ccbid,int brktype,int *howmany);// Retrieve just the number of rows
  int retrievecmds(int ccbid,int brktype,int *howmany,char cmds[][512]); // Retrieve all command lines
  int retrievecmds(int ccbid,int brktype,int *howmany,char cmds[][512],int nmax); // Retrieve (at most <n>) command lines

  char* retrieve_configs_confname(int ccbid); // Retrieve "configs.confname" for the selected "configsets.Name" and the desired ccbid

  int cmdinsert(char *name,char *cmd, Ebrktype brktype); // Insert new commands in <configcmds>, update <cfgrelations> and (if needed) <cfgbricks>. <BrkType> is the >cfgbricks.BrkType>

  int retrieve_confmask(int ccbid, char *maskxml); // Retrieve and return confmask.maskxml for the desired ccbid

//  int appliedtoccb(int);
//  int appliedtoccb(char *,int);

 private:
  bool newconn; // True if a new connection to DB is open
  int ConfKey;
  cmsdtdb* dtdbobj;
};

#endif
