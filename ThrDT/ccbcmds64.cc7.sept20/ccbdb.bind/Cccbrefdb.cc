#include "Cccbrefdb.h"
#include <cstring>

#define CLOB_DIM 65536 // max dimension of clob

char strreftype[][7]={"boot","status","test","rob","padc"};

ccbrefdb::ccbrefdb() {
  dtdbobj = new cmsdtdb;
  newconn = true;
}

ccbrefdb::ccbrefdb(cmsdtdb* thisobj) {
  dtdbobj = thisobj;
  newconn = false;
}

ccbrefdb::~ccbrefdb() {
  if (newconn)
    delete dtdbobj;
}

// Insert a new record in ccbref
// Author: A. Parenti, Apr28 2006
// Modified: AP, Jul24 2007 (Tested on MySQL/Oracle)
int ccbrefdb::insert(short mctype, reftype rtype, char *refxml, char *cmnt) {
  //  char mquer[FIELD_MAX_LENGTH];
  //  int rows=0, fields=0;
  char tmpxml[CLOB_DIM+1];
  int resu=0;
  int refid;

  int lenxml = strlen(refxml);

  refid=dtdbobj->get_counter("refid","ccbref")+1;
  if (refid<=0)
    return resu; // return if error

  dtdbobj->dblock(); // lock the mutex

//   sprintf(mquer,"INSERT INTO ccbref (refid,reftype,cmnt) VALUES (%d,'%s','%s')",refid,strreftype[rtype],cmnt);
//   resu=dtdbobj->sqlquery(mquer,&rows,&fields);

  char * insquery = "INSERT INTO ccbref (refid,reftype,cmnt) VALUES (?,?,?)";
  binder bindSetIns [4];
  bindSetIns[0] = binder(SQL_C_LONG, SQL_INTEGER, &refid, 0);
  bindSetIns[1] = binder(SQL_C_CHAR, SQL_CHAR, strreftype[rtype], strlen(strreftype[rtype]));
  bindSetIns[2] = binder(SQL_C_CHAR, SQL_CHAR, cmnt, strlen(cmnt));

  dtdbobj->sqlQueryBind(insquery, bindSetIns, 3);
  resu = dtdbobj->sqlInsert();

  if (resu!=0) {
    dtdbobj->dbunlock(); // unlock the mutex
    return resu; // return if error
  }

  if (rtype==status || rtype==test) { // Status or test reference: write mctype

//     sprintf(mquer,"UPDATE ccbref SET mctype='%d' WHERE refid='%d'",mctype,refid);
//     resu=dtdbobj->sqlquery(mquer,&rows,&fields);

    char * updquery = "UPDATE ccbref SET mctype=? WHERE refid=?";
    binder bindSetUpd [2];
    bindSetUpd[0] = binder(SQL_C_LONG, SQL_INTEGER, &mctype, 0);
    bindSetUpd[1] = binder(SQL_C_LONG, SQL_INTEGER, &refid, 0);
    dtdbobj->sqlQueryBind(updquery, bindSetUpd, 2);
    resu = dtdbobj->sqlUpdate();

    if (resu!=0) {
      dtdbobj->dbunlock(); // unlock the mutex
      return resu; // return if error
    }
  }

// Write first chunk of xml
  strncpy(tmpxml,refxml,CLOB_DIM);
  tmpxml[CLOB_DIM]='\0';
//   sprintf(mquer,"UPDATE ccbref SET refxml1='%s' WHERE refid='%d'",tmpxml,refid);
//   resu=dtdbobj->sqlquery(mquer,&rows,&fields);

  char * updquery1 = "UPDATE ccbref SET refxml1=? WHERE refid=?";
  binder bindSetUpd [2];
  bindSetUpd[0] = binder(SQL_C_CHAR, SQL_CHAR, tmpxml, strlen(tmpxml));
  bindSetUpd[1] = binder(SQL_C_LONG, SQL_INTEGER, &refid, 0);
  dtdbobj->sqlQueryBind(updquery1, bindSetUpd, 2);
  resu = dtdbobj->sqlUpdate();

  if (resu!=0) {
    dtdbobj->dbunlock(); // unlock the mutex
    return resu; // return if error
  }

// Eventually write second chunk of xml
  if (lenxml>CLOB_DIM) {
    strncpy(tmpxml,refxml+CLOB_DIM,CLOB_DIM);
    tmpxml[CLOB_DIM]='\0';
//     sprintf(mquer,"UPDATE ccbref SET refxml2='%s' WHERE refid='%d'",tmpxml,refid);
//     resu=dtdbobj->sqlquery(mquer,&rows,&fields);

    char * updquery2 = "UPDATE ccbref SET refxml2=? WHERE refid=?";
    binder bindSetUpd [2];
    bindSetUpd[0] = binder(SQL_C_CHAR, SQL_CHAR, tmpxml, strlen(tmpxml));
    bindSetUpd[1] = binder(SQL_C_LONG, SQL_INTEGER, &refid, 0);
    dtdbobj->sqlQueryBind(updquery2, bindSetUpd, 2);
    resu = dtdbobj->sqlUpdate();

    if (resu!=0) {
      dtdbobj->dbunlock(); // unlock the mutex
      return resu; // return if error
    }
  }

// Eventually write third chunk of xml
  if (lenxml>2*CLOB_DIM) {
    strncpy(tmpxml,refxml+2*CLOB_DIM,CLOB_DIM);
    tmpxml[CLOB_DIM]='\0';
//     sprintf(mquer,"UPDATE ccbref SET refxml3='%s' WHERE refid='%d'",tmpxml,refid);
//     resu=dtdbobj->sqlquery(mquer,&rows,&fields);

    char * updquery3 = "UPDATE ccbref SET refxml3=? WHERE refid=?";
    binder bindSetUpd [2];
    bindSetUpd[0] = binder(SQL_C_CHAR, SQL_CHAR, tmpxml, strlen(tmpxml));
    bindSetUpd[1] = binder(SQL_C_LONG, SQL_INTEGER, &refid, 0);
    dtdbobj->sqlQueryBind(updquery3, bindSetUpd, 2);
    resu = dtdbobj->sqlUpdate();

    if (resu!=0) {
      dtdbobj->dbunlock(); // unlock the mutex
      return resu; // return if error
    }
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return 0;
}


// Read a record from ccbref. Input: ccbID, rtype.
// Author: A. Parenti, Jan19 2006
// Modified: AP, Sep20 2007 (Tested on MySQL/Oracle)
int ccbrefdb::retrieveref(int ccbID,reftype rtype,char *refxml,char *cmnt) {
   char mquer[LEN2];
//   int rows=0, fields=0;
  int resu=0;
  int refid=0;
  char myfie[FIELD_MAX_LENGTH];

  dtdbobj->dblock(); // lock the mutex

// Reset fields
  strcpy(refxml,"");
  strcpy(cmnt,"");

// Find RefId
//   sprintf(mquer,"SELECT %srefid FROM refrelations WHERE ccbID='%d'",strreftype[rtype],ccbID);
//   resu = dtdbobj->sqlquery(mquer,&rows,&fields);

//   if (resu==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {
//     dtdbobj->returnfield(0,myfie);   
//     sscanf(myfie,"%d",&refid);
//   }

  sprintf(mquer,"SELECT %srefid FROM refrelations WHERE CCBID=?",
	  strreftype[rtype]);
  binder bindSet [1];
  bindSet[0] = binder(SQL_C_LONG, SQL_INTEGER, &ccbID, 0);
  dtdbobj->sqlQueryBind(mquer, bindSet, 1);
  resu = dtdbobj->sqlFetchOne();


  if (resu == SQL_SUCCESS) {
    dtdbobj->returnfield(0,myfie);   
    sscanf(myfie,"%d",&refid);
  }

  if (refid<=0) {
    printf("%s reference for ccbID=%d not found.\n",strreftype[rtype],ccbID);

    dtdbobj->dbunlock(); // unlock the mutex
    return -1;
  }

  /// TOBEFIXED
  //  sprintf(mquer,"SELECT refxml1,refxml2,refxml3,cmnt FROM ccbref WHERE refid='%d'",refid);
  //  resu = dtdbobj->sqlquery(mquer,&rows,&fields);

  char * tmpquery1 =
    "SELECT refxml1,refxml2,refxml3,cmnt FROM ccbref WHERE refid=?";

  bindSet[0] = binder(SQL_C_LONG, SQL_INTEGER, &refid, 0);
  dtdbobj->sqlQueryBind(tmpquery1, bindSet, 1);
  resu = dtdbobj->sqlFetchOne();

  if (resu == SQL_SUCCESS) {
    //  if (resu==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {

    dtdbobj->returnfield(0,myfie);
    strcpy(refxml,myfie);

    dtdbobj->returnfield(1,myfie);
    strcat(refxml,myfie);

    dtdbobj->returnfield(2,myfie);
    strcat(refxml,myfie);

    dtdbobj->returnfield(3,myfie);
    strcpy(cmnt,myfie);


    dtdbobj->dbunlock(); // unlock the mutex
    return 0;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return -1;
}

// Read a record from ccbref. Input: refid.
// Author: A. Parenti, Jan17 2006
// Modified: AP, Jul25 2007 (Tested on MySQL/Oracle)
int ccbrefdb::retrieveref(int refid,short *mctype,reftype *rtype,char *refxml,char *cmnt) {
//   char mquer[LEN2];
//   int rows=0, fields=0;
  int resu=0;

  dtdbobj->dblock(); // lock the mutex

// Reset fields
  *mctype = 0;
  strcpy(refxml,"");
  strcpy(cmnt,"");

// Query
//   sprintf(mquer,"SELECT mctype,reftype,refxml1,refxml2,refxml3,cmnt FROM ccbref WHERE refid='%d'",refid);
//   resu = dtdbobj->sqlquery(mquer,&rows,&fields);

  char * tmpquery =
    "SELECT mctype,reftype,refxml1,refxml2,refxml3,cmnt FROM ccbref WHERE refid==?";

  binder bindSet [1];
  bindSet[0] = binder(SQL_C_LONG, SQL_INTEGER, &refid, 0);
  dtdbobj->sqlQueryBind(tmpquery, bindSet, 1);
  resu = dtdbobj->sqlFetchOne();

  if (resu == SQL_SUCCESS) {
    //  if (resu==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {
    char myfie[FIELD_MAX_LENGTH];

    dtdbobj->returnfield(0,myfie);
    sscanf(myfie,"%d",mctype);

    dtdbobj->returnfield(1,myfie);
    if (strstr(myfie,"boot")!=NULL)
      *rtype=boot;
    else if (strstr(myfie,"status")!=NULL)
      *rtype=status;
    else if (strstr(myfie,"test")!=NULL)
      *rtype=test;
    else if (strstr(myfie,"rob")!=NULL)
      *rtype=rob;
    else if (strstr(myfie,"padc")!=NULL)
      *rtype=padc;

    dtdbobj->returnfield(2,myfie);   
    strcpy(refxml,myfie);

    dtdbobj->returnfield(3,myfie);
    strcat(refxml,myfie);

    dtdbobj->returnfield(4,myfie);
    strcat(refxml,myfie);

    dtdbobj->returnfield(5,myfie);
    strcpy(cmnt,myfie);

    dtdbobj->dbunlock(); // unlock the mutex
    return 0;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return -1;
}

// Retrieve records from ccbref. Input: mctype, rtype.
// Author: A. Parenti, Jan18 2006
// Modified: AP, Jul25 2007 (Tested on MySQL/Oracle)
int ccbrefdb::retrieveref(short mctype, reftype rtype, int *howmany, int *refid, char cmnt[][LEN2]) {
//   char mquer[LEN2];
//   int rows=0, fields=0;
  int resu=0;

  dtdbobj->dblock(); // lock the mutex

// Reset variables
  *howmany = 0;

  if (rtype==status || rtype==test) {
    //    sprintf(mquer,"SELECT refid,cmnt FROM ccbref WHERE reftype='%s' AND mctype='%d' ORDER BY refid",strreftype[rtype],mctype);

    char * tmpquery1 =
      "SELECT refid,cmnt FROM ccbref WHERE reftype=? AND mctype=? ORDER BY refid";
    binder bindSet [2];
    bindSet[0] = binder(SQL_C_CHAR, SQL_CHAR,
			&(strreftype[rtype]), strlen(strreftype[rtype]));
    bindSet[1] = binder(SQL_C_LONG, SQL_INTEGER, &mctype, 0);
    dtdbobj->sqlQueryBind(tmpquery1, bindSet, 2);
    resu = dtdbobj->sqlFetchOne();
  } else {
    //    sprintf(mquer,"SELECT refid,cmnt FROM ccbref WHERE reftype='%s' ORDER BY refid",strreftype[rtype]);
    
    char * tmpquery1 =
      "SELECT refid,cmnt FROM refid,cmnt WHERE reftype=? ORDER BY refid";
    binder bindSet [1];
    bindSet[0] = binder(SQL_C_CHAR, SQL_CHAR,
			&(strreftype[rtype]), strlen(strreftype[rtype]));
    dtdbobj->sqlQueryBind(tmpquery1, bindSet, 1);
    resu = dtdbobj->sqlFetchOne();
  }

  //  resu=dtdbobj->sqlquery(mquer,&rows,&fields);

  if (resu == SQL_SUCCESS) {
    char myfie[LEN1];
    int kk;

    for (kk=0;dtdbobj->fetchrow()==SQL_SUCCESS;++kk) {
      dtdbobj->returnfield(0,myfie);
      sscanf(myfie,"%d",&refid[*howmany]);

      dtdbobj->returnfield(1,cmnt[*howmany]);

      *howmany+=1;
    }

    dtdbobj->dbunlock(); // unlock the mutex
    return 0;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return -1;
}

// Retrieve last record from ccbref. Input: mctype, rtype.
// Author: A. Parenti, Jan18 2006
// Modified: AP, Jul25 2007  2007 (Tested on MySQL/Oracle)
int ccbrefdb::retrievelastref(short mctype, reftype rtype, int *refid, char cmnt[LEN2]) {
//   char mquer[LEN2];
//   int rows=0, fields=0;
  int resu=0;

  dtdbobj->dblock(); // lock the mutex

// Reset values
  *refid = 0;
  strcpy(cmnt,"");

  if (rtype==status || rtype==test) {
    // sprintf(mquer,"SELECT refid,cmnt FROM ccbref WHERE reftype='%s' AND mctype='%d' ORDER BY refid DESC",strreftype[rtype],mctype);
    
    char * tmpquery1 =
      "SELECT refid,cmnt FROM ccbref WHERE reftype=? AND mctype=? ORDER BY refid";
    binder bindSet [2];
    bindSet[0] = binder(SQL_C_CHAR, SQL_CHAR,
			&(strreftype[rtype]), strlen(strreftype[rtype]));
    bindSet[1] = binder(SQL_C_LONG, SQL_INTEGER, &mctype, 0);
    dtdbobj->sqlQueryBind(tmpquery1, bindSet, 2);
    resu = dtdbobj->sqlFetchOne();
  } else {
    // sprintf(mquer,"SELECT refid,cmnt FROM ccbref WHERE reftype='%s' ORDER BY refid DESC",strreftype[rtype]);
    
    char * tmpquery1 =
      "SELECT refid,cmnt FROM ccbref WHERE reftype=? ORDER BY refid DESC";
    binder bindSet [1];
    bindSet[0] = binder(SQL_C_CHAR, SQL_CHAR,
			&(strreftype[rtype]), strlen(strreftype[rtype]));
    dtdbobj->sqlQueryBind(tmpquery1, bindSet, 1);
    resu = dtdbobj->sqlFetchOne();
  }

  //  resu=dtdbobj->sqlquery(mquer,&rows,&fields);

  if (resu==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {
    char myfie[FIELD_MAX_LENGTH];

    dtdbobj->returnfield(0,myfie);
    sscanf(myfie,"%d",refid);

    dtdbobj->returnfield(1,myfie);
    strcpy(cmnt,myfie);

    dtdbobj->dbunlock(); // unlock the mutex
    return 1;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return -1;
}


int ccbrefdb::retrieveErrMask(int ccbID,bool *errmask,int errlen) {
   char mquer[LEN2];
	//   int rows=0, fields=0;
	int resu=0;
	int errid=0;
	char myfie[FIELD_MAX_LENGTH];

	 dtdbobj->dblock(); // lock the mutex



	  sprintf(mquer,"SELECT errid FROM errs2filt WHERE CCBID=?");
	  binder bindSet [1];
	  bindSet[0] = binder(SQL_C_LONG, SQL_INTEGER, &ccbID, 0);
	  dtdbobj->sqlQueryBind(mquer, bindSet, 1);
	  resu = dtdbobj->sqlFetchOne();


	 if (resu == SQL_SUCCESS) {
     char myfie[LEN1];
	    int kk;

	  for (kk=0;dtdbobj->fetchrow()==SQL_SUCCESS;++kk) {
	     dtdbobj->returnfield(0,myfie);
	     sscanf(myfie,"%d",&errid);
        if ((errid>=0) && (errid <errlen))
		      errmask[errid]=false;
	     }

    dtdbobj->dbunlock(); // unlock the mutex
	     return 0;
    }

   printf("Cccbdbref : errors with query...\n");
   dtdbobj->dbunlock(); // unlock the mutex
  return -1;
 }

