#!/bin/tcsh -f

# Read a PADC LUT file, create the script to feed it into sql DB
# Author: A.Parenti, Aug02 2006


if (${#argv} != 1) then
  echo "Usage: $0 <LUT file>"
  echo "Create an sql script to fill DB from a PADC LUT file"
  exit
endif

if (! -e ${1}) then
  echo "PADC LUT file $1 doesn't exist"
  exit
endif


set filein=$1
set fileout=`echo $1 | awk '(sub("txt","sql"))'`

set padcdataid = 999999
set wheel   = `cat ${filein}|grep '$WHEEL'|awk '{gsub("\r",""); print $2}'`
set sector  = `cat ${filein}|grep '$SECTOR' |awk '{gsub("\r",""); print $2}'`
set station = `cat ${filein}|grep '$MB_TYPE'|awk '{gsub("\r",""); print $2}'`
set padcid  = `cat ${filein}|grep '$PADC_ID'|awk '{gsub("\r",""); print $2}'`
set manfeid = `cat ${filein}|grep '$MANIFOLD_FE_ID'|awk '{gsub("\r","");print $2}'`
set manhvid = `cat ${filein}|grep '$MANIFOLD_HV_ID'|awk '{gsub("\r","");print $2}'`
set datetime = `cat ${filein}|grep '$DATE_TIME'|awk '{gsub("\r","");print $2}'`

# Datetime
set yy = `echo $datetime | awk '{print substr($1,1,2)}'`
set mm = `echo $datetime | awk '{print substr($1,3,2)}'`
set dd = `echo $datetime | awk '{print substr($1,5,2)}'`
set hh = `echo $datetime | awk '{print substr($1,8,2)}'`
set mn = `echo $datetime | awk '{print substr($1,10,2)}'`
set ss = "00"

# Datetime (specific to MySQL)
@ myyyy = 2000 + $yy
set mdate = ${myyyy}${mm}${dd}${hh}${mn}${ss}
#echo $mdate
###########

# Datetime (specific to Oracle)
set mlist=(jan feb mar apr may jun jul aug sep oct nov dec)
set omm = $mlist[$mm]
if ($hh <= 12) then
  @ ohh = $hh
  set ampm = "AM"
else
  @ ohh = $hh - 12
  set ampm = "PM"
endif

set odate = "${dd}-${omm}-${yy} ${ohh}.${mn}.${ss} ${ampm}"
#echo $odate
###########

if (-e $fileout) then
  mv -f $fileout $fileout.bak
endif

echo "/* This is specific to MySQL */" >> $fileout
echo INSERT INTO padcrelations \(PadcId,ManifoldFeId,ManifoldHvId,Datetime,PadcDataId\) VALUES \(${padcid},${manfeid},${manhvid},\'${mdate}\',$padcdataid\) ';'>> $fileout

echo "/* This is specific to Oracle */" >> $fileout
echo INSERT INTO padcrelations \(PadcId,ManifoldFeId,ManifoldHvId,Datetime,PadcDataId\) VALUES \(${padcid},${manfeid},${manhvid},\'${odate}\',$padcdataid\) ';'>> $fileout


@ i=1024
while ($i > 0)
  @ adccount = 1024 - $i # ADC COUNT
  set line=`cat ${filein} | tail -1025 | head -1024 | tail -$i | head -1 | awk '{gsub("\r",""); gsub("\t",","); print}'` # CALIBRATIONS
  echo INSERT INTO padcdata \(PadcDataId,AdcCount,sens100A_HV,sens100B_HV,sens500_HV,sensHV_Vcc,sens100A_FE,sens100B_FE,sens500_FE,sensFE_Vcc,PADC_Vcc,PADC_Vdd\) VALUES \($padcdataid,${adccount},${line}\) ';'>> $fileout

  @ i--
end

echo 'After you have filled the DB using this script, remember to change padcrelations.PadcDataId and padcdata.PadcDataId from 999999 to the value returned by "SELECT MAX(PadcDataId+1) FROM padcrelations WHERE PadcDataId!=999999"'
echo 'Use "UPDATE padcrelations SET PadcDataId=xyz WHERE PadcDataId=999999" and "UPDATE padcdata SET PadcDataId=xyz WHERE PadcDataId=999999", where xyz is the number obtained with "SELECT MAX..."'

