#include "Cdaccalib.h"
#include <cstring>

daccalib::daccalib(){
  reset();
  dtdbobj = new cmsdtdb;
  newconn = true;
}

daccalib::daccalib(cmsdtdb* thisobj) {
  reset();
  dtdbobj=thisobj;
  newconn = false;
}

daccalib::~daccalib() {
  if (newconn)
    delete dtdbobj;
}


// Select a TTC, recover DAC calibrations
// Author: A.Parenti, Jul20 2007 (Tested on MySQL/Oracle)
int daccalib::select(int thisttc) {
//   char mquer[LEN2];
//   int rows=0, fields=0;
  int i, resu=0;

  dtdbobj->dblock(); // lock the mutex

  reset(); // Reset values
  TTCid = thisttc;

//   sprintf(mquer,"SELECT width_gain,bias_gain1,bias_gain2,bias_gain3,thr_gain1,thr_gain2,thr_gain3,width_ofst,bias_ofst1,bias_ofst2,bias_ofst3,thr_ofst1,thr_ofst2,thr_ofst3,Datetime FROM daccalibs WHERE TTCid=%d ORDER BY Datetime DESC",thisttc);
//   resu = dtdbobj->sqlquery(mquer,&rows,&fields);
  char * tmpquery =
    "SELECT width_gain,bias_gain1,bias_gain2,bias_gain3,thr_gain1,thr_gain2,thr_gain3,width_ofst,bias_ofst1,bias_ofst2,bias_ofst3,thr_ofst1,thr_ofst2,thr_ofst3,Datetime FROM daccalibs WHERE TTCid=? ORDER BY Datetime DESC";
  binder bindSet [1];
  bindSet[0] = binder(SQL_C_LONG, SQL_INTEGER, &thisttc, 0);
  dtdbobj->sqlQueryBind(tmpquery, bindSet, 1);
  resu = dtdbobj->sqlFetchOne();

  if (resu == SQL_SUCCESS) {
    char myfie[LEN1];

    dtdbobj->returnfield(0,myfie);
    sscanf(myfie,"%f",&width_gain);

    dtdbobj->returnfield(7,myfie);
    sscanf(myfie,"%f",&width_ofst);

    for (i=0;i<3;++i) {
      dtdbobj->returnfield(1+i,myfie);
      sscanf(myfie,"%f",bias_gain+i);

      dtdbobj->returnfield(4+i,myfie);
      sscanf(myfie,"%f",thr_gain+i);

      dtdbobj->returnfield(8+i,myfie);
      sscanf(myfie,"%f",bias_ofst+i);

      dtdbobj->returnfield(11+i,myfie);
      sscanf(myfie,"%f",thr_ofst+i);
    }

    dtdbobj->returnfield(14,Datetime);

    dtdbobj->dbunlock(); // unlock the mutex
    error=false;
    return 0;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  error=true;
  return -1;
}

// Print DAC calibration
// Author: A.Parenti, Jul20 2007
void daccalib::daccalib_print() {
  printf("TTCid: %d\n",TTCid);

  printf("Width Gain: %f\n",width_gain);
  printf("Bias Gain: %f %f %f\n",bias_gain[0],bias_gain[1],bias_gain[2]);
  printf("Thr Gain: %f %f %f\n",thr_gain[0],thr_gain[1],thr_gain[2]);

  printf("Width Offset: %f\n",width_ofst);
  printf("Bias Offset: %f %f %f\n",bias_ofst[0],bias_ofst[1],bias_ofst[2]);
  printf("Thr Offset: %f %f %f\n",thr_ofst[0],thr_ofst[1],thr_ofst[2]);
}

void daccalib::daccalib_print(int thisttc) {
  select(thisttc);
  daccalib_print();
}

// Retrieve DAC calibrations
// Author: A.Parenti, Jul20 2007
int daccalib::retrievedaccalib(float calibrations[14]) {
  int i;

  if (error) {
    for (i=0;i<14;++i)
      calibrations[i]=0;
    return -1;
  }

  calibrations[0]=width_gain;
  calibrations[7]=width_ofst;
  for (i=0;i<3;++i) {
    calibrations[1+i]=bias_gain[i];
    calibrations[4+i]=thr_gain[i];

    calibrations[8+i]=bias_ofst[i];
    calibrations[11+i]=thr_ofst[i];
  }

  return 0;
}


// Retrieve DAC calibrations
// Author: A.Parenti, Jul20 2007
int daccalib::retrievedaccalib(int thisttc, float calibrations[14]) {
  select(thisttc);
  return retrievedaccalib(calibrations);  
}


// Reset the class
// Author: A.Parenti, Jul20 2007
void daccalib::reset() {
  int i;

  error=false;
  strcpy(Datetime,"");
  TTCid=0;
  width_gain=width_ofst=0;
  for (i=0;i<3;++i)
    bias_gain[i]=thr_gain[i]=bias_ofst[i]=thr_ofst[i]=0;
}
