#ifndef _dtlogger_
#define _dtlogger_
#include "Ccmsdtdb.h"

// enum msglevel {mesg='mesg',warn='warn',erro='erro'};
 enum msglevel {mesg='m',warn='w',erro='e'};

class dtlogger {

 public:
  dtlogger(); // Create a new connection to DB
  dtlogger(cmsdtdb* dtdbobj); // Use an existing connection to DB
  ~dtlogger();

  int insert();
  int fetchlast(char *);
  void Tell(char *);
  void Warn(char *);
  void Erro(char *);

 private:
  bool newconn; // True if a new connection to DB is open
  char mbody[FIELD_MAX_LENGTH];
  int mLevel;
  cmsdtdb* dtdbobj;

};

#endif
