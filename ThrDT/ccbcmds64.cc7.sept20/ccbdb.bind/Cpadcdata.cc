#include "Cpadcdata.h"
#include <cstring>

padcdata::padcdata(){
  reset();
  dtdbobj = new cmsdtdb;
  newconn = true;
}

padcdata::padcdata(cmsdtdb* thisobj) {
  reset();
  dtdbobj=thisobj;
  newconn = false;
}

padcdata::~padcdata() {
  if (newconn)
    delete dtdbobj;
}


// Select a PADC
// Author: A.Parenti, Aug03 2006
// Modified: AP, Jul20 2007 (Tested on MySQL/Oracle)
int padcdata::select(int thisccb) {
//   char mquer[LEN2];
//   int rows=0, fields=0;
  int resu=0;

  dtdbobj->dblock(); // lock the mutex

  reset(); // Reset values
  ccbID = thisccb;

//   sprintf(mquer,"SELECT ccbmap.wheel,ccbmap.sector,ccbmap.station,padcrelations.PadcId,padcrelations.ManifoldFeId,padcrelations.ManifoldHvId,padcrelations.Datetime,padcrelations.PadcDataId FROM ccbmap,padcrelations WHERE ccbmap.ccbID='%d' AND padcrelations.padcid=ccbmap.padcid ORDER BY padcrelations.Datetime DESC",thisccb);
//   resu = dtdbobj->sqlquery(mquer,&rows,&fields);

  char * tmpquery =
    "SELECT ccbmap.wheel,ccbmap.sector,ccbmap.station,padcrelations.PadcId,padcrelations.ManifoldFeId,padcrelations.ManifoldHvId,padcrelations.Datetime,padcrelations.PadcDataId FROM ccbmap,padcrelations WHERE ccbmap.ccbID=? AND padcrelations.padcid=ccbmap.padcid ORDER BY padcrelations.Datetime DESC";
  binder bindSet [1];
  bindSet[0] = binder(SQL_C_LONG, SQL_INTEGER, &thisccb, 0);
  dtdbobj->sqlQueryBind(tmpquery, bindSet, 1);
  resu = dtdbobj->sqlFetchOne();


  if (resu == SQL_SUCCESS) {
    char myfie[LEN1];

// Retrieve chamber info from ccbmap
    dtdbobj->returnfield(0,myfie);
    sscanf(myfie,"%d",&wheel);
    dtdbobj->returnfield(1,myfie);
    sscanf(myfie,"%d",&sector);
    dtdbobj->returnfield(2,myfie);
    sscanf(myfie,"%d",&station);

// Retrieve PADC info from padcrelations
    dtdbobj->returnfield(3,myfie);
    sscanf(myfie,"%d",&PadcId);
    dtdbobj->returnfield(4,myfie);
    sscanf(myfie,"%d",&ManifoldFeId);
    dtdbobj->returnfield(5,myfie);
    sscanf(myfie,"%d",&ManifoldHvId);
    dtdbobj->returnfield(6,Datetime);
    dtdbobj->returnfield(7,myfie);
    sscanf(myfie,"%d",&PadcDataId);

    dtdbobj->dbunlock(); // unlock the mutex
    return 0;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return -1;
}

// Print PADC info
// Author: A.Parenti, Aug03 2006
void padcdata::padcinfo() {
  printf("ccbID: %d\n",ccbID);
  printf("wheel: %d\n",wheel);
  printf("sector: %d\n",sector);
  printf("station: %d\n",station);

  printf("PadcId: %d\n",PadcId);
  printf("ManifoldFeId: %d\n",ManifoldFeId);
  printf("ManifoldHvId: %d\n",ManifoldHvId);
  printf("Datetime: %s\n",Datetime);
  printf("PadcDataId: %d\n",PadcDataId);
}

void padcdata::padcinfo(int thisccb) {
  select(thisccb);
  padcinfo();
}

// Retrieve PADC data
// Author: A.Parenti, Aug03 2006
// Modified: AP, Jul02 2007 (Tested on MySQL/Oracle)
int padcdata::retrievepadcdata(short adc_count[10],float data_read[10]) {

  // int rows=0, fields=0;
  char mquer[LEN2];
  char myfie[LEN1];
  char colnames[10][15]={"sens100A_HV","sens100B_HV","sens500_HV","sensHV_Vcc","sens100A_FE","sens100B_FE","sens500_FE","sensFE_Vcc","PADC_Vcc","PADC_Vdd"};
  int resu=0;
  int i;

  for (i=0;i<10;++i) { // Reset data_read
    data_read[i] = 0.0;
  }

  if (PadcDataId<=0)
    return -1;

  dtdbobj->dblock(); // lock the mutex


  binder bindSet [2];
  bindSet[0] = binder(SQL_C_LONG, SQL_INTEGER, &PadcDataId, 0);

  for (i=0;i<10;++i) {

//     sprintf(mquer,"SELECT %s FROM padcdata WHERE PadcDataId='%d' AND AdcCount='%d'",colnames[i],PadcDataId,adc_count[i]);
//     resu += dtdbobj->sqlquery(mquer,&rows,&fields);
//     if (resu==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {
//       dtdbobj->returnfield(0,myfie);
//       sscanf(myfie,"%f",data_read+i);
//     }

    sprintf(mquer,
	    "SELECT %s FROM padcdata WHERE PadcDataId=? AND AdcCount=?",
	    colnames[i]);

    bindSet[1] = binder(SQL_C_LONG, SQL_INTEGER, &(adc_count[i]), 0);

    dtdbobj->sqlQueryBind(mquer, bindSet, 2);
    resu += dtdbobj->sqlFetchOne();
    if (resu==0) {
      dtdbobj->returnfield(0,myfie);
      sscanf(myfie,"%f",data_read+i);
    }

  }

  dtdbobj->dbunlock(); // unlock the mutex
  return resu;
}

// Retrieve PADC data
// Author: A.Parenti, Aug03 2006
// Modified: AP, Apr04 2007 (Tested on MySQL/Oracle)
int padcdata::retrievepadcdata(int thisccb,short adc_count[10],float data_read[10]) {
  int resu;
  int i;

  for (i=0;i<10;++i) { // Reset data_read
    data_read[i] = 0.0;
  }

  if (select(thisccb)==0) { // Ok
    resu=retrievepadcdata(adc_count,data_read);
  }
  else
    resu = -1;

  return resu;
}

// Reset the class
// Author: A. Parenti, Aug03 2006
void padcdata::reset() {
  strcpy(Datetime,"");
  PadcDataId = 0;
  PadcId = ManifoldFeId = ManifoldHvId = 0;
  ccbID = wheel = sector = station = 0;
}
