#ifndef _Ccmsdtdb_
#define _Ccmsdtdb_

#include <stdio.h>
#include <sqlext.h>
#include <pthread.h> // for mutex
#include <ctime>

#define LEN1 256 // some string lengths
#define LEN2 512
//#define FIELD_MAX_LENGTH 4096 // Maximum dimension (bytes) of a DB field
#define FIELD_MAX_LENGTH 65536 // Maximum dimension (bytes) of a DB field

#define DB_CONFIG_FILE "/etc/ccbdb.conf"
#define DB_TIMEOUT 10 // Timeout (sec) for connecting to DB
#define DB_MIN_RECONNET_TIME 30 // Not reconnect more than once every xx sec's
#define DB_MAXERR 10 // After DB_MAXERR errors, disconnect and reconnect to DB

enum reftype {boot, status, test, rob, padc};

struct binder {
  SQLSMALLINT c_type;   // program type for the conversion
  SQLSMALLINT sql_type; // SQL type of the column
  SQLPOINTER  param;    // binding parameter
  SQLUINTEGER size;     // size of the binding parameter
  binder (){};
  binder ( SQLSMALLINT cType, SQLSMALLINT sqlType, SQLPOINTER  parameter,
	   SQLUINTEGER psize=0 )
    : c_type(cType), sql_type(sqlType), param (parameter), size(psize) {};
};


class cmsdtdb {
 public:

  enum EDbTypes {unknown, mysql, oracle} dbtype;
  SQLHENV henv;            // Environment handle
  SQLHDBC hdbc;            // Connection handle
  SQLHSTMT hstmt;          // Statement handle
  SQLRETURN connct_rc;     // Connection return code

  SQLRETURN query_rc;      // Query return code
  SQLSMALLINT query_cols;  // Returned columns
  SQLINTEGER query_rows;   // Returned rows. NB: in some drivers, a "SELECT" statement return query_rows = 0 or -1

  static int curr_run;     // Current run

  cmsdtdb(); // Connect to the default database
  cmsdtdb(char *DataSource, char *UserName, char *Password); // Connect to the specified DB
  ~cmsdtdb(); // Disconnect from DB
  void verbose_mode(bool in);

// Lock/unlock the mutex
  void dblock();
  void dbunlock();

  /// query preparation using binding parameters
  int sqlQueryBind(char *mquery, binder *input=NULL, unsigned int insize = 0);
  /// binded query execution, allows binding output buffers
  int sqlFetchOne( binder *outBindSet=NULL, unsigned int outsize=0);
  /// binded query execution, allows binding output buffers
  int sqlInsert();
  int sqlUpdate();

  int sqlquery(char *mquery, int *res_rows, int *res_cols); // SQL query. NB: with some drivers, for a "SELECT" statement query_rows is set to 0/-1
  int fetchrow(); // Fetch a row from query results
  int returnfield(int field, char *resu); // Get a field from the fetched row

  int get_counter(char *CounterName, char *TableName); // Get the value of a counter from a table

  void setcurrun(int nrun); // Set RunNr
  int insertnewrun(char *runtype, char *runcomment); // Write run info on DB

  void connect(); // Connect to DB
  void disconnect(); // Disconnect from DB


 protected:
  bool _verbose_;

 private:
//  void connect(); // Connect to DB
//  void disconnect(); // Disconnect from DB
  void init(); // Initialise variables
  void read_ds(char *datasource); // Read datasource name from DB_CONFIG_FILE
  void resetqueryresults(); // Reset query results
  pthread_mutex_t dtdb_mutex; // mutex object: avoid conflicts in DB access
  int errcount; // Error counter
  char datasource[64], user[64], passwd[64]; // Connection parameters
  time_t connct_time;

 protected:
  char *to_char(char *varname); // Cast a variable to char (the function is specific to DB)
};

#endif

