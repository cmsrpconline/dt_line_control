#ifndef _ccbdatadb_
#define _ccbdatadb_
#include "Ccmsdtdb.h"

class ccbdata {

 public:
  ccbdata(); // Create a new connection to DB
  ccbdata(cmsdtdb* dtdbobj); // Use an existing connection to DB
  ~ccbdata();

  int insert(int ccbID,int CmdCode,char *data,int datasize); // Input: ccbid,cmdcode,data,datasize

  int retrievelast(int ccbID,char *data,char *time); // Input: ccbID
  int retrievelast(int ccbID,int CmdCode,char *data,char *time); // Input: ccbID,CmdCode
  int ccbId(int wheel, int sector, int station,
	    int & ccbid, int & port, int & secport);

 private:
  bool newconn; // True if a new connection to DB is open
  cmsdtdb* dtdbobj;
};

#endif
