#include "Cccbmap.h"
#include <cstring>

char strreftypem[][7]={"boot","status","test"};

ccbmap::ccbmap() {
  dtdbobj = new cmsdtdb;
  newconn = true;
}

ccbmap::ccbmap(cmsdtdb* thisobj) {
  dtdbobj = thisobj;
  newconn = false;
}

ccbmap::~ccbmap() {
  if (newconn)
    delete dtdbobj;
}


// Author: S.Ventura
// Modified: AP, Oct10 2007
int ccbmap::insert(int ccbID,int wh,int sec,int sta,int cha,int min,int onl,int prt,int sprt,char *serv,char *comm) {
  mId=ccbID;
  wheel=wh;
  sector=sec;
  station=sta;
  chamber=cha;
  minicrate=min;
  online=onl;
  port=prt;
  secport=sprt;
  strcpy(ccbserver,serv);
  strcpy(comment,comm);

  return this->insert();
}

// Author: S.Ventura
// Modified: AP, Oct10 2007 (Tested)
int ccbmap::insert() {
//   char mquer[LEN2];
//   int rows=0, fields=0;
  int resu=0;

  dtdbobj->dblock(); // lock the mutex

//   sprintf(mquer,"INSERT INTO ccbmap (ccbID,wheel,sector,station,chamber,minicrate,on_line,port,secport,ccbserver,cmnt) VALUES ('%d','%d','%d','%d','%d','%d','%d','%d','%d','%s','%s')",mId,wheel,sector,station,chamber,minicrate,online,port,secport,ccbserver,comment);
//   resu = dtdbobj->sqlquery(mquer,&rows,&fields);

  char* insquery = 
    "INSERT INTO ccbmap (ccbID,wheel,sector,station,chamber,minicrate,on_line,port,secport,ccbserver,cmnt) VALUES (?,?,?,?,?,?,?,?,?,?,?)";

  binder bindSetIns [11];
  bindSetIns[0] = binder(SQL_C_LONG, SQL_INTEGER, &mId, 0);
  bindSetIns[1] = binder(SQL_C_LONG, SQL_INTEGER, &wheel, 0);
  bindSetIns[2] = binder(SQL_C_LONG, SQL_INTEGER, &sector, 0);
  bindSetIns[3] = binder(SQL_C_LONG, SQL_INTEGER, &station, 0);
  bindSetIns[4] = binder(SQL_C_LONG, SQL_INTEGER, &chamber, 0);
  bindSetIns[5] = binder(SQL_C_LONG, SQL_INTEGER, &minicrate, 0);
  bindSetIns[6] = binder(SQL_C_LONG, SQL_INTEGER, &online, 0);
  bindSetIns[7] = binder(SQL_C_LONG, SQL_INTEGER, &port, 0);
  bindSetIns[8] = binder(SQL_C_LONG, SQL_INTEGER, &secport, 0);
  bindSetIns[9] = binder(SQL_C_CHAR, SQL_CHAR, ccbserver, strlen(ccbserver));
  bindSetIns[10] = binder(SQL_C_CHAR, SQL_CHAR, comment, strlen(comment));
  dtdbobj->sqlQueryBind(insquery, bindSetIns, 11);
  resu = dtdbobj->sqlInsert();

  if (resu!=0) {
    dtdbobj->dbunlock(); // unlock the mutex
    return resu;
  }

//   sprintf(mquer,"INSERT INTO refrelations (ccbID) VALUES ('%d')",mId);
//   resu = dtdbobj->sqlquery(mquer,&rows,&fields);

  char* insquery2 = "INSERT INTO refrelations (ccbID) VALUES (?)";
  dtdbobj->sqlQueryBind(insquery2, bindSetIns, 1);
  resu = dtdbobj->sqlInsert();

  dtdbobj->dbunlock(); // unlock the mutex
  return resu;
}



// Author: AP, Jan20 2006
// Modified: AP, Apr18 2007 (Tested on Oracle/MySQL)
int ccbmap::setrefs(int ccbID,reftype rtype,int refid) {

//   int rows=0, fields=0
  int resu=0;
  char mquer[LEN2];

  mId = ccbID;

  //  sprintf(mquer,"UPDATE refrelations SET %srefid='%d' WHERE CcbID='%d'",strreftypem[rtype],refid,ccbID);

  sprintf(mquer,"UPDATE refrelations SET %srefid=? WHERE CcbID=?",strreftypem[rtype]);

  binder bindSetUpd [2];
  bindSetUpd[0] = binder(SQL_C_LONG, SQL_INTEGER, &refid, 0);
  bindSetUpd[1] = binder(SQL_C_LONG, SQL_INTEGER, &ccbID, 0);

  dtdbobj->dblock(); // lock the mutex
  //  resu=dtdbobj->sqlquery(mquer,&rows,&fields);
  dtdbobj->sqlQueryBind(mquer, bindSetUpd, 2);
  resu = dtdbobj->sqlUpdate();
  dtdbobj->dbunlock(); // unlock the mutex

  return resu;
}

// Author: AP, Jan20 2006
// Modified: AP, Apr18 2007 (Tested on Oracle/MySQL)
int ccbmap::setrefs(int ccbID,int bootrefid,int statusrefid,int testrefid) {
//   char mquer[LEN2];
//   int rows=0, fields=0;
  int resu=0;

  mId = ccbID;

  //  sprintf(mquer,"UPDATE refrelations SET bootrefid='%d', statusrefid='%d', testrefid='%d' WHERE CcbID='%d'",bootrefid,statusrefid,testrefid,ccbID);

  char * updquery =
    "UPDATE refrelations SET bootrefid=?, statusrefid=?, testrefid=? WHERE CcbID=?";

  binder bindSetUpd [4];
  bindSetUpd[0] = binder(SQL_C_LONG, SQL_INTEGER, &bootrefid, 0);
  bindSetUpd[1] = binder(SQL_C_LONG, SQL_INTEGER, &statusrefid, 0);
  bindSetUpd[2] = binder(SQL_C_LONG, SQL_INTEGER, &testrefid, 0);
  bindSetUpd[3] = binder(SQL_C_LONG, SQL_INTEGER, &ccbID, 0);

  dtdbobj->dblock(); // lock the mutex
  //  resu=dtdbobj->sqlquery(mquer,&rows,&fields);
  dtdbobj->sqlQueryBind(updquery, bindSetUpd, 4);
  resu = dtdbobj->sqlUpdate();
  dtdbobj->dbunlock(); // unlock the mutex

  return resu;
}


// Author: S.Ventura
// Modified: AP, Oct10 2007 (Tested)
int ccbmap::retrieve(int ccbID) {
//   char mquer[LEN2];
//   int rows=0, fields=0;
  int resu=0;

  dtdbobj->dblock(); // lock the mutex
  mId = ccbID;

  //  sprintf(mquer,"SELECT wheel,sector,station,chamber,minicrate,on_line,port,secport,ccbserver,cmnt FROM ccbmap WHERE ccbID='%d'",ccbID);
  //  resu = dtdbobj->sqlquery(mquer,&rows,&fields);

  char * tmpquery =
    "SELECT wheel,sector,station,chamber,minicrate,on_line,port,secport,ccbserver,cmnt FROM ccbmap WHERE ccbID=?";
  binder bindSet [1];
  bindSet[0] = binder(SQL_C_LONG, SQL_INTEGER, &ccbID, 0);
  dtdbobj->sqlQueryBind(tmpquery, bindSet, 1);
  dtdbobj->sqlFetchOne();

  if (resu == SQL_SUCCESS) {
    char myfie[LEN1];

    dtdbobj->returnfield(0,myfie);
    sscanf(myfie,"%d",&wheel);
    dtdbobj->returnfield(1,myfie);
    sscanf(myfie,"%d",&sector);
    dtdbobj->returnfield(2,myfie);
    sscanf(myfie,"%d",&station);
    dtdbobj->returnfield(3,myfie);
    sscanf(myfie,"%d",&chamber);
    dtdbobj->returnfield(4,myfie);
    sscanf(myfie,"%d",&minicrate);
    dtdbobj->returnfield(5,myfie);
    sscanf(myfie,"%d",&online);
    dtdbobj->returnfield(6,myfie);
    sscanf(myfie,"%d",&port);
    dtdbobj->returnfield(7,myfie);
    sscanf(myfie,"%d",&secport);
    dtdbobj->returnfield(8,myfie);
    strcpy(ccbserver,myfie);
    dtdbobj->returnfield(9,myfie);
    strcpy(comment,myfie);

    dtdbobj->dbunlock(); // unlock the mutex
    return 0;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return -1;
}


// Author: S.Ventura
// Modified: AP, Oct10 2006 (Tested)
int ccbmap::retrieve(int this_wheel, int this_sector, int this_station) {
//   char mquer[LEN2];
//   int rows=0, fields=0;
  int resu=0;

  dtdbobj->dblock(); // lock the mutex

//   sprintf(mquer,"SELECT ccbID,chamber,minicrate,on_line,port,secport,ccbserver,cmnt FROM ccbmap WHERE wheel='%d' AND sector='%d' AND station='%d'", this_wheel, this_sector, this_station);
//   resu = dtdbobj->sqlquery(mquer,&rows,&fields);

  char * tmpquery =
    "SELECT ccbID,chamber,minicrate,on_line,port,secport,ccbserver,cmnt FROM ccbmap WHERE wheel=? AND sector=? AND station=?";
  binder bindSet [3];
  bindSet[0] = binder(SQL_C_LONG, SQL_INTEGER, &this_wheel, 0);
  bindSet[1] = binder(SQL_C_LONG, SQL_INTEGER, &this_sector, 0);
  bindSet[2] = binder(SQL_C_LONG, SQL_INTEGER, &this_station, 0);
  dtdbobj->sqlQueryBind(tmpquery, bindSet, 3);
  resu = dtdbobj->sqlFetchOne();

  if (resu == SQL_SUCCESS) {
    char myfie[LEN1];

    wheel = this_wheel;
    sector = this_sector;
    station = this_station; 

    dtdbobj->returnfield(0,myfie);
    sscanf(myfie,"%d",&mId);
    dtdbobj->returnfield(1,myfie);
    sscanf(myfie,"%d",&chamber);
    dtdbobj->returnfield(2,myfie);
    sscanf(myfie,"%d",&minicrate);
    dtdbobj->returnfield(3,myfie);
    sscanf(myfie,"%d",&online);
    dtdbobj->returnfield(4,myfie);
    sscanf(myfie,"%d",&port);
    dtdbobj->returnfield(5,myfie);
    sscanf(myfie,"%d",&secport);
    dtdbobj->returnfield(6,myfie);
    strcpy(ccbserver,myfie);
    dtdbobj->returnfield(7,myfie);
    strcpy(comment,myfie);

    dtdbobj->dbunlock(); // unlock the mutex
    return 0;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return -1;
}

// Author: S.Ventura
// Modified: AP, Apr03 2007 (Tested on MySQL/ORACLE)
int ccbmap::isonline(int ccbID) {
//   char mquer[LEN2];  
//   int rows=0, fields=0;
  int resu=0;

  dtdbobj->dblock(); // lock the mutex
  mId = ccbID;

//   sprintf(mquer,"SELECT on_line FROM ccbmap WHERE ccbID='%d'",ccbID);
//   resu = dtdbobj->sqlquery(mquer,&rows,&fields);

  char * tmpquery = "SELECT on_line FROM ccbmap WHERE ccbID=?";
  binder bindSet [1];
  bindSet[0] = binder(SQL_C_LONG, SQL_INTEGER, &ccbID, 0);
  dtdbobj->sqlQueryBind(tmpquery, bindSet, 1);
  resu = dtdbobj->sqlFetchOne();

  if (resu == SQL_SUCCESS) {
    char myfie[LEN1];
    dtdbobj->returnfield(0,myfie);
    sscanf(myfie,"%d",&online);

    dtdbobj->dbunlock(); // unlock the mutex
    return online;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return -1;
}

// Author: S.Ventura
int ccbmap::setonline(int ccbID) {
  mId=ccbID;
  return setonline();
}

// Author: S.Ventura
// Modified: AP, Apr18 2007 (Tested on MySQL-ORACLE)
int ccbmap::setonline() {
//   char mquer[LEN2];
//   int rows=0, fields=0
  int  resu=0;

  //  sprintf(mquer,"UPDATE ccbmap SET on_line=1 WHERE ccbID='%d'", mId);

  char * updquery = "UPDATE ccbmap SET on_line=1 WHERE ccbID=?";

  binder bindSetUpd [1];
  bindSetUpd[0] = binder(SQL_C_LONG, SQL_INTEGER, &mId, 0);

  dtdbobj->dblock(); // lock the mutex
  //  resu=dtdbobj->sqlquery(mquer,&rows,&fields);
  dtdbobj->sqlQueryBind(updquery, bindSetUpd, 1);
  resu = dtdbobj->sqlUpdate();
  dtdbobj->dbunlock(); // unlock the mutex

  return resu;
}

// Author: S.Ventura
int ccbmap::setoffline(int ccbID) {
  mId=ccbID;
  return setoffline();
}

// Author: S.Ventura
// Modified: AP, Apr18 2007 (Tested on MySQL-ORACLE)
int ccbmap::setoffline() {
//   char mquer[LEN2];
//   int rows=0, fields=0;
  int resu=0;

  //  sprintf(mquer,"UPDATE ccbmap SET on_line=0 WHERE ccbID='%d'", mId);

  char * updquery = "UPDATE ccbmap SET on_line=0 WHERE ccbID=?";

  binder bindSetUpd [1];
  bindSetUpd[0] = binder(SQL_C_LONG, SQL_INTEGER, &mId, 0);

  dtdbobj->dblock(); // lock the mutex
  //  resu=dtdbobj->sqlquery(mquer,&rows,&fields);
  dtdbobj->sqlQueryBind(updquery, bindSetUpd, 1);
  resu = dtdbobj->sqlUpdate();
  dtdbobj->dbunlock(); // unlock the mutex

  return resu;
}


// Author: S.Ventura
int ccbmap::getport(int ccbID) {
  mId=ccbID;
  return getport();
}


// Author: S.Ventura
// Modified: AP, Apr03 2007 (Tested on MySQL-ORACLE)
int ccbmap::getport() {
//   char mquer[LEN2];
//   int rows=0, fields=0;
  int resu=0;

  dtdbobj->dblock(); // lock the mutex

//   sprintf(mquer,"SELECT port FROM ccbmap WHERE ccbID='%d'",mId);
//   resu = dtdbobj->sqlquery(mquer,&rows,&fields);


  char * tmpquery = "SELECT port FROM ccbmap WHERE ccbID=?";
  binder bindSet [1];
  bindSet[0] = binder(SQL_C_LONG, SQL_INTEGER, &mId, 0);
  dtdbobj->sqlQueryBind(tmpquery, bindSet, 1);
  resu = dtdbobj->sqlFetchOne();

  if (resu == SQL_SUCCESS) {
    char myfie[LEN1];

    dtdbobj->returnfield(0,myfie);
    sscanf(myfie,"%d",&port);

    dtdbobj->dbunlock(); // unlock the mutex
    return port;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return -1;
}


// Author: S.Ventura
int ccbmap::setport(int ccbID, int mport) {
  mId=ccbID;
  return setport(mport);
}

// Author: S.Ventura
// Modified: AP, Apr18 2007 (Tested on MySQL/Oracle)
int ccbmap::setport(int mport) {
//   char mquer[LEN2];
//   int rows=0, fields=0;
  int resu=0;

  //  sprintf(mquer,"UPDATE ccbmap SET port=%d WHERE ccbID='%d'",mport,mId);

  char * updquery = "UPDATE ccbmap SET port=? WHERE ccbID=?";

  binder bindSetUpd [2];
  bindSetUpd[0] = binder(SQL_C_LONG, SQL_INTEGER, &mport, 0);
  bindSetUpd[1] = binder(SQL_C_LONG, SQL_INTEGER, &mId, 0);

  dtdbobj->dblock(); // lock the mutex
  //  resu=dtdbobj->sqlquery(mquer,&rows,&fields);
  dtdbobj->sqlQueryBind(updquery, bindSetUpd, 2);
  resu = dtdbobj->sqlUpdate();
  dtdbobj->dbunlock(); // unlock the mutex

  return resu;
}


// Author: A.Parenti, Oct10 2007 (Tested)
int ccbmap::getsecport(int ccbID) {
  mId=ccbID;
  return getport();
}


// Author: A.Parenti, Oct10 2007 (Tested)
int ccbmap::getsecport() {
//   char mquer[LEN2];
//   int rows=0, fields=0;
  int resu=0;

  dtdbobj->dblock(); // lock the mutex

//   sprintf(mquer,"SELECT secport FROM ccbmap WHERE ccbID='%d'",mId);
//   resu = dtdbobj->sqlquery(mquer,&rows,&fields);

  char * tmpquery = "SELECT secport FROM ccbmap WHERE ccbID=?";
  binder bindSet [1];
  bindSet[0] = binder(SQL_C_LONG, SQL_INTEGER, &mId, 0);
  dtdbobj->sqlQueryBind(tmpquery, bindSet, 1);
  resu = dtdbobj->sqlFetchOne();

  if (resu == SQL_SUCCESS) {
    char myfie[LEN1];

    dtdbobj->returnfield(0,myfie);
    sscanf(myfie,"%d",&port);

    dtdbobj->dbunlock(); // unlock the mutex
    return port;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return -1;
}


// Author: A.Parenti, Oct10 2007 (Tested)
int ccbmap::setsecport(int ccbID, int mport) {
  mId=ccbID;
  return setsecport(mport);
}

// Author: A.Parenti, Oct10 2007 (Tested)
int ccbmap::setsecport(int mport) {
//   char mquer[LEN2];
//   int rows=0, fields=0;
  int resu=0;

  //  sprintf(mquer,"UPDATE ccbmap SET secport=%d WHERE ccbID='%d'",mport,mId);

  char * updquery = "UPDATE ccbmap SET secport=? WHERE ccbID=?";

  binder bindSetUpd [2];
  bindSetUpd[0] = binder(SQL_C_LONG, SQL_INTEGER, &mport, 0);
  bindSetUpd[1] = binder(SQL_C_LONG, SQL_INTEGER, &mId, 0);

  dtdbobj->dblock(); // lock the mutex
  //  resu=dtdbobj->sqlquery(mquer,&rows,&fields);
  dtdbobj->sqlQueryBind(updquery, bindSetUpd, 2);
  resu = dtdbobj->sqlUpdate();
  dtdbobj->dbunlock(); // unlock the mutex

  return resu;
}


// Author: A.Parenti, Nov07 2006
int ccbmap::setserver(int ccbID,char *ccbserver) {
  mId=ccbID;
  return setserver(ccbserver);
}


// Author: A.Parenti, Nov07 2006
// Modified: AP, Apr18 2007 (Tested on MySQL/Oracle)
int ccbmap::setserver(char *ccbserver) {
//   char mquer[LEN2];
//   int rows=0, fields=0;
  int resu=0;

  //  sprintf(mquer,"UPDATE ccbmap SET ccbserver='%s' WHERE ccbID='%d'",ccbserver,mId);

  char * updquery = "UPDATE ccbmap SET ccbserver=? WHERE ccbID=?";

  binder bindSetUpd [2];
  bindSetUpd[0] = binder(SQL_C_CHAR, SQL_CHAR, &ccbserver, strlen(ccbserver));
  bindSetUpd[1] = binder(SQL_C_LONG, SQL_INTEGER, &mId, 0);

  dtdbobj->dblock(); // lock the mutex
  //  resu=dtdbobj->sqlquery(mquer,&rows,&fields);
  dtdbobj->sqlQueryBind(updquery, bindSetUpd, 2);
  resu = dtdbobj->sqlUpdate();
  dtdbobj->dbunlock(); // unlock the mutex

  return resu;
}


// Author: A.Parenti, Nov07 2006
int ccbmap::getserver(int ccbID,char *ccbserver) {
  mId=ccbID;
  return getserver(ccbserver);
}


// Author: A.Parenti, Nov07 2006
// Modified: AP, Apr03 2007 (Tested on MySQL-ORACLE)
int ccbmap::getserver(char *ccbserver) {
//   char mquer[LEN2];
//   int rows=0, fields=0;
  int resu=0;

  dtdbobj->dblock(); // lock the mutex

//   sprintf(mquer,"SELECT ccbserver FROM ccbmap WHERE ccbID='%d'",mId);
//   resu = dtdbobj->sqlquery(mquer,&rows,&fields);

  char * tmpquery = "SELECT ccbserver FROM ccbmap WHERE ccbID=?";
  binder bindSet [1];
  bindSet[0] = binder(SQL_C_LONG, SQL_INTEGER, &mId, 0);
  dtdbobj->sqlQueryBind(tmpquery, bindSet, 1);
  resu = dtdbobj->sqlFetchOne();

  if (resu == SQL_SUCCESS) {
    //    char myfie[LEN1];

    dtdbobj->returnfield(0,ccbserver);

    dtdbobj->dbunlock(); // unlock the mutex
    return 0;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return -1;
}


// Retrieve <BTItheta_R>, <BTItheta_z> and <BTItheta_sign> from table ccbgeom
// Author: A.Parenti, Mar01 2007
// Modified: AP, Apr03 2007 (Tested on Oracle/MySQL)
int ccbmap::retrieve_BTItheta(int ccbID, float *R, float *z, int *sign) {
//   char mquer[LEN2];
//   int rows=0, fields=0;
  int resu=0;

  dtdbobj->dblock(); // lock the mutex

  *R = *z = *sign = 0;
  mId=ccbID;

//   sprintf(mquer,"SELECT BTItheta_R,BTItheta_z,BTItheta_sign FROM ccbgeom WHERE ccbID='%d' ORDER by Datetime DESC",mId);
//   resu = dtdbobj->sqlquery(mquer,&rows,&fields);


  char * tmpquery = 
    "SELECT BTItheta_R,BTItheta_z,BTItheta_sign FROM ccbgeom WHERE ccbID=? ORDER by Datetime DESC";
  binder bindSet [1];
  bindSet[0] = binder(SQL_C_LONG, SQL_INTEGER, &mId, 0);
  dtdbobj->sqlQueryBind(tmpquery, bindSet, 1);
  resu = dtdbobj->sqlFetchOne();

  if ( resu == SQL_SUCCESS) {
    char myfie[LEN1];

    dtdbobj->returnfield(0,myfie);
    sscanf(myfie,"%f",R);

    dtdbobj->returnfield(1,myfie);
    sscanf(myfie,"%f",z);

    dtdbobj->returnfield(2,myfie);
    sscanf(myfie,"%d",sign);

    dtdbobj->dbunlock(); // unlock the mutex
    return 0;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return -1;
}


// Insert <BTItheta_R>, <BTItheta_z> and <BTItheta_sign> in table ccbgeom
// Author: A.Parenti, Mar01 2007
// Modified: AP, Apr18 2007 (Tested on Oracle/MySQL)
int ccbmap::insert_BTItheta(int ccbID, float R, float z, int sign) {
//   char mquer[LEN2];
//   int rows=0, fields=0;
  int resu=0;

  mId=ccbID;
  //  sprintf(mquer,"INSERT INTO ccbgeom (ccbID,BTItheta_R,BTItheta_z,BTItheta_sign) VALUES ('%d','%f','%f','%d')",mId,R,z,sign);


  char * insquery = "INSERT INTO ccbgeom (ccbID,BTItheta_R,BTItheta_z,BTItheta_sign) VALUES (?,?,?,?)";
  binder bindSetIns [4];
  bindSetIns[0] = binder(SQL_C_LONG, SQL_INTEGER, &mId, 0);
  bindSetIns[1] = binder(SQL_C_FLOAT, SQL_FLOAT, &R, 0);
  bindSetIns[2] = binder(SQL_C_FLOAT, SQL_FLOAT, &z, 0);
  bindSetIns[3] = binder(SQL_C_LONG, SQL_INTEGER, &sign, 0);

  dtdbobj->dblock(); // lock the mutex
  //  resu=dtdbobj->sqlquery(mquer,&rows,&fields);
  dtdbobj->sqlQueryBind(insquery, bindSetIns, 4);
  resu = dtdbobj->sqlInsert();

  return resu;
}
