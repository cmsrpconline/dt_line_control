#include "Cccbstatus.h"

#include <cstring>

ccbstatus::ccbstatus() {
  dtdbobj = new cmsdtdb;
  newconn = true;
}

ccbstatus::ccbstatus(cmsdtdb* thisobj) {
  dtdbobj = thisobj;
  newconn = false;
}

ccbstatus::~ccbstatus() {
  if (newconn)
    delete dtdbobj;
}


// Authors: S.Ventura, A.Parenti
// Modified: AP, Mar22 2006 (Tested on MySQL/ORACLE)
int ccbstatus::insert(char *mst) {
  return insert(mst,"");
}

// Authors: S.Ventura, A.Parenti
// Modified: AP, Apr18 2007 (Tested on ODBC MySQL-ORACLE)
int ccbstatus::insert(char *mst, char *statusxml) {
  unsigned short mid = ((unsigned char)mst[3]<<8)+(unsigned char)mst[4]; // CCB ID
//   char mquer[FIELD_MAX_LENGTH];
//   int rows=0, fields=0;
  int resu=0;
  char mhex[FIELD_MAX_LENGTH];
  int ii;

  mhex[0]=0;
  for (ii=0; ii<(unsigned char)mst[1]; ii++)
    sprintf(mhex,"%s%02hhx",mhex,mst[ii]);

  //  sprintf(mquer,"INSERT INTO ccbstatus (RunID,ccbID,statusstruct,statusxml,time) VALUES ('%d','%d','0x%s','%s',CURRENT_TIMESTAMP)",dtdbobj->curr_run,mid,mhex,statusxml);

  sprintf(mhex,"0x%s",mhex);
  char * insquery = "INSERT INTO ccbstatus (RunID,ccbID,statusstruct,statusxml,time) VALUES (?,?,?,?,CURRENT_TIMESTAMP)";
  binder bindSetIns [4];
  bindSetIns[0] = binder(SQL_C_LONG, SQL_INTEGER, &(dtdbobj->curr_run), 0);
  bindSetIns[1] = binder(SQL_C_LONG, SQL_INTEGER, &mid, 0);
  bindSetIns[2] = binder(SQL_C_CHAR, SQL_CHAR, mhex, strlen(mhex));
  bindSetIns[3] = binder(SQL_C_CHAR, SQL_CHAR, statusxml, strlen(statusxml));


  dtdbobj->dblock(); // lock the mutex
  //  resu=dtdbobj->sqlquery(mquer,&rows,&fields);
  dtdbobj->sqlQueryBind(insquery, bindSetIns, 4);
  dtdbobj->dbunlock(); // unlock the mutex

  return resu;
}


// Authors: S.Ventura, A.Parenti
// Modified: AP, Mar22 2006 (Tested on ODBC MySQL-ORACLE)
int ccbstatus::retrievelast(int ccbID, char *mst, char *sttime) {
  char statusxml[FIELD_MAX_LENGTH];

  return retrievelast(ccbID,mst,statusxml,sttime);
}

// Authors: S.Ventura, A.Parenti
// Modified: AP, Apr05 2007 (Tested on ODBC MySQL-ORACLE)
int ccbstatus::retrievelast(int ccbID, char *mst, char *statusxml, char *sttime) {
//   char mquer[LEN2];
//   int rows=0, fields=0
  int reso;

  dtdbobj->dblock(); // lock the mutex

// Reset results
  strcpy(mst,"");
  strcpy(statusxml,"");
  strcpy(sttime,"");

  //  sprintf(mquer,"SELECT statusstruct,statusxml,time FROM ccbstatus WHERE ccbID='%d' ORDER BY time DESC",ccbID);
  //  reso = dtdbobj->sqlquery(mquer,&rows,&fields);

  char * tmpquery =
    "SELECT statusstruct,statusxml,time FROM ccbstatus WHERE ccbID=? ORDER BY time DESC";
  binder bindSet [1];
  bindSet[0] = binder(SQL_C_LONG, SQL_INTEGER, &ccbID, 0);
  dtdbobj->sqlQueryBind(tmpquery, bindSet, 1);
  reso = dtdbobj->sqlFetchOne();

  if (reso == SQL_SUCCESS) {
    dtdbobj->returnfield(0,mst);
    dtdbobj->returnfield(1,statusxml);
    dtdbobj->returnfield(2,sttime);

    dtdbobj->dbunlock(); // unlock the mutex
    return 1;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return 0;
}


// Insert ROB status in robstatus
// Author: A.Parenti, May16 2007 (Tested on MySQL/Oracle)
int ccbstatus::rob_insert(int ccbID, unsigned char rob_error_state[7], float mc_temp_rob[7], float read_rob_pwr_Vcc[7], float read_rob_pwr_Vdd[7], float read_rob_pwr_current[7]) {
//   char mquer[512];
//   int rows=0, fields=0;

  char str1[64]="", str2[64]="", str3[64]="", str4[64]="", str5[64]="";
  int resu=0;
  int i;

  for (i=0;i<7;++i) {
    sprintf(str1,"%s %3d",str1,rob_error_state[i]);
    sprintf(str2,"%s %.1f",str2,mc_temp_rob[i]);
    sprintf(str3,"%s %.2f",str3,read_rob_pwr_Vcc[i]);
    sprintf(str4,"%s %.2f",str4,read_rob_pwr_Vdd[i]);
    sprintf(str5,"%s %.2f",str5,read_rob_pwr_current[i]);
  }

  //  sprintf(mquer,"INSERT INTO robstatus (RunID,ccbID,RobErrorState,McTempRob,ReadRobPwrVcc,ReadRobPwrVdd,ReadRobPwrCurrent,time) VALUES ('%d','%d','%s','%s','%s','%s','%s',CURRENT_TIMESTAMP)",dtdbobj->curr_run,ccbID,str1,str2,str3,str4,str5);

  char * insquery = "INSERT INTO robstatus (RunID,ccbID,RobErrorState,McTempRob,ReadRobPwrVcc,ReadRobPwrVdd,ReadRobPwrCurrent,time) VALUES (?,?,?,?,?,?,?,CURRENT_TIMESTAMP)";
  binder bindSetIns [7];
  bindSetIns[0] = binder(SQL_C_LONG, SQL_INTEGER, &(dtdbobj->curr_run), 0);
  bindSetIns[1] = binder(SQL_C_LONG, SQL_INTEGER, &ccbID, 0);
  bindSetIns[2] = binder(SQL_C_CHAR, SQL_CHAR, str1, strlen(str1));
  bindSetIns[3] = binder(SQL_C_CHAR, SQL_CHAR, str2, strlen(str2));
  bindSetIns[4] = binder(SQL_C_CHAR, SQL_CHAR, str3, strlen(str3));
  bindSetIns[5] = binder(SQL_C_CHAR, SQL_CHAR, str4, strlen(str4));
  bindSetIns[6] = binder(SQL_C_CHAR, SQL_CHAR, str5, strlen(str5));

  dtdbobj->dblock(); // lock the mutex
  //  resu=dtdbobj->sqlquery(mquer,&rows,&fields);
  resu = dtdbobj->sqlQueryBind(insquery, bindSetIns, 7);
  dtdbobj->dbunlock(); // unlock the mutex

  return resu;
}

// Retrieve last ROB status. Input: ccbID
// Author: A.Parenti, May16 2007 (Tested on MySQL/Oracle)
int ccbstatus::rob_retrievelast(int ccbID, unsigned char rob_error_state[7], float mc_temp_rob[7], float read_rob_pwr_Vcc[7], float read_rob_pwr_Vdd[7], float read_rob_pwr_current[7],char *sttime) {
//   char mquer[512];
//   int rows=0, fields=0;
  int resu=0;
  char myfie[64];

  dtdbobj->dblock(); // lock the mutex

//   sprintf(mquer,"SELECT RobErrorState,McTempRob,ReadRobPwrVcc,ReadRobPwrVdd,ReadRobPwrCurrent,time FROM robstatus WHERE ccbID=%d ORDER BY time DESC",ccbID);
//   resu=dtdbobj->sqlquery(mquer,&rows,&fields);

  char * tmpquery =
  "SELECT RobErrorState,McTempRob,ReadRobPwrVcc,ReadRobPwrVdd,ReadRobPwrCurrent,time FROM robstatus WHERE ccbID=? ORDER BY time DESC";
  binder bindSet [1];
  bindSet[0] = binder(SQL_C_LONG, SQL_INTEGER, &ccbID, 0);
  dtdbobj->sqlQueryBind(tmpquery, bindSet, 1);
  resu = dtdbobj->sqlFetchOne();


  if (resu == SQL_SUCCESS) {
    dtdbobj->returnfield(0,myfie);
    sscanf(myfie,"%d %d %d %d %d %d %d",rob_error_state,rob_error_state+1,rob_error_state+2,rob_error_state+3,rob_error_state+4,rob_error_state+5,rob_error_state+6);

    dtdbobj->returnfield(1,myfie);
    sscanf(myfie,"%f %f %f %f %f %f %f",mc_temp_rob,mc_temp_rob+1,mc_temp_rob+2,mc_temp_rob+3,mc_temp_rob+4,mc_temp_rob+5,mc_temp_rob+6);

    dtdbobj->returnfield(2,myfie);
    sscanf(myfie,"%f %f %f %f %f %f %f",read_rob_pwr_Vcc,read_rob_pwr_Vcc+1,read_rob_pwr_Vcc+2,read_rob_pwr_Vcc+3,read_rob_pwr_Vcc+4,read_rob_pwr_Vcc+5,read_rob_pwr_Vcc+6);

    dtdbobj->returnfield(3,myfie);
    sscanf(myfie,"%f %f %f %f %f %f %f",read_rob_pwr_Vdd,read_rob_pwr_Vdd+1,read_rob_pwr_Vdd+2,read_rob_pwr_Vdd+3,read_rob_pwr_Vdd+4,read_rob_pwr_Vdd+5,read_rob_pwr_Vdd+6);

    dtdbobj->returnfield(4,myfie);
    sscanf(myfie,"%f %f %f %f %f %f %f",read_rob_pwr_current,read_rob_pwr_current+1,read_rob_pwr_current+2,read_rob_pwr_current+3,read_rob_pwr_current+4,read_rob_pwr_current+5,read_rob_pwr_current+6);

    dtdbobj->returnfield(5,sttime);

    dtdbobj->dbunlock(); // unlock the mutex
    return 0;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return -1;
}
