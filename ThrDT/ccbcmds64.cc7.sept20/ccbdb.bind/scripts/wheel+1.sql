-- WHEEL +1 --
-- AP, last updated Oct10 2007 --

-- SECTOR 1 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (378,1,1,1,0,1378,10);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (248,1,1,2,0,1248,22);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (101,1,1,3,0,1101,40);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (358,1,1,4,0,1358, 6);

-- SECTOR 2 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 75,1,2,1,0,1075,25);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (267,1,2,2,0,1267,15);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (281,1,2,3,0,1281, 8);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (177,1,2,4,0,1177, 2);

-- SECTOR 3 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 30,1,3,1,0,1030, 9);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (184,1,3,2,0,1184,24);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (148,1,3,3,0,1148,25);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (213,1,3,4,0,1213, 2);

-- SECTOR 4 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (155,1,4,1,0,1155, 1);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (277,1,4,2,0,1277, 1);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (278,1,4,3,0,1278, 1);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (223,1,4,4,0,1223, 1);

-- SECTOR 5 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (168,1,5,1,0,1168,16);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (188,1,5,2,0,1188,25);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (252,1,5,3,0,1252,19);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 80,1,5,4,0,1080, 2);

-- SECTOR 6 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (253,1,6,1,0,1253,26);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (181,1,6,2,0,1181,26);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 70,1,6,3,0,1070,12);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (214,1,6,4,0,1214, 2);

-- SECTOR 7 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (176,1,7,1,0,2176,15);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (383,1,7,2,0,2383,28);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (107,1,7,3,0,2107,39);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (374,1,7,4,0,2374, 5);

-- SECTOR 8 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (212,1,8,1,0,2212,27);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (264,1,8,2,0,2264,11);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (258,1,8,3,0,2258,18);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (220,1,8,4,0,2220, 1);

-- SECTOR 9 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (165,1,9,1,0,2165,22);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (150,1,9,2,0,2150,14);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (274,1,9,3,0,2274,22);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (273,1,9,4,0,2273, 2);

-- SECTOR 10 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (240,1,10,1,0,2240,24);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (270,1,10,2,0,2270,13);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (163,1,10,3,0,2163, 9);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (228,1,10,4,0,2228, 1);

-- SECTOR 11 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (166,1,11,1,0,2166,20);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (263,1,11,2,0,2263,16);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (272,1,11,3,0,2272,20);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (186,1,11,4,0,2186, 4);

-- SECTOR 12 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 55,1,12,1,0,2055,21);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (266,1,12,2,0,2266,12);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (149,1,12,3,0,2149,24);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (216,1,12,4,0,2216, 2);

-- SECTOR 13 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (217,1,13,4,0,1217, 2);

-- SECTOR 14 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (185,1,14,4,0,2185, 1);
