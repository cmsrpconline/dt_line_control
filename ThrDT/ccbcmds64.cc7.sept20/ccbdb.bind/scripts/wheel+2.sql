-- WHEEL +2 --
-- AP, last updated Oct12 2007 --

-- SECTOR 1 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (174,2,1,1,0,1174,12);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (256,2,1,2,0,1256,19);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (276,2,1,3,0,1276,15);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (136,2,1,4,0,1136, 5);

-- SECTOR 2 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 26,2,2,1,0,1026, 4);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 44,2,2,2,0,1044, 1);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 40,2,2,3,0,1040, 1);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (271,2,2,4,0,1271, 1);

-- SECTOR 3 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 28,2,3,1,0,1028, 1);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (232,2,3,2,0,1232, 4);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (162,2,3,3,0,1162,11);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (196,2,3,4,0,1196, 1);

-- SECTOR 4 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (283,2,4,1,0,1283, 8);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 76,2,4,2,0,1076, 8);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 65,2,4,3,0,1065, 6);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 72,2,4,4,0,1072, 2);

-- SECTOR 5 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 64,2,5,1,0,1064, 7);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (170,2,5,2,0,1170, 5);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 83,2,5,3,0,1083, 7);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 81,2,5,4,0,1081, 1);

-- SECTOR 6 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 41,2,6,1,0,1041, 5);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 37,2,6,2,0,1037, 2);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 32,2,6,3,0,1032, 5);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 82,2,6,4,0,1082, 1);

-- SECTOR 7 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 94,2,7,1,0,2094,19);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (209,2,7,2,0,2209,27);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (322,2,7,3,0,2322,54);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (146,2,7,4,0,2146, 5);

-- SECTOR 8 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 38,2,8,1,0,2038, 3);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 60,2,8,2,0,2060,10);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (257,2,8,3,0,2257,13);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 36,2,8,4,0,2036, 1);

-- SECTOR 9 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 42,2,9,1,0,2042,14);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 35,2,9,2,0,2035, 3);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (198,2,9,3,0,2198,26);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (183,2,9,4,0,2183, 2);

-- SECTOR 10 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 61,2,10,1,0,2061,13);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 66,2,10,2,0,2066, 6);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 34,2,10,3,0,2034, 3);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 57,2,10,4,0,2057, 1);

-- SECTOR 11 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 68,2,11,1,0,2068,17);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 62,2,11,2,0,2062, 7);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 39,2,11,3,0,2039,10);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (189,2,11,4,0,2189, 1);

-- SECTOR 12 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 29,2,12,1,0,2029, 2);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 74,2,12,2,0,2074, 9);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 63,2,12,3,0,2063, 2);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (182,2,12,4,0,2182, 1);

-- SECTOR 13 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 79,2,13,4,0,1079, 1);

-- SECTOR 14 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (259,2,14,4,0,2259, 2);
