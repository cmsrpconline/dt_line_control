-- WHEEL -1 --
-- AP, last updated Oct10 2007 --

-- SECTOR 1 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (380,-1,1,1,0,1380,28);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (201,-1,1,2,0,1201, 8);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (108,-1,1,3,0,1108,42);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (333,-1,1,4,0,1333, 7);

-- SECTOR 2 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (140,-1,2,1,0,1140,22);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (373,-1,2,2,0,1373,21);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (221,-1,2,3,0,1221,33);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (260,-1,2,4,0,1260, 6);

-- SECTOR 3 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (138,-1,3,1,0,1138, 1);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (238,-1,3,2,0,1238, 1);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (110,-1,3,3,0,1110, 2);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (207,-1,3,4,0,1207, 4);

-- SECTOR 4 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 85,-1,4,1,0,1085,16);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (359,-1,4,2,0,1359,24);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (225,-1,4,3,0,1225,35);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 95,-1,4,4,0,1095, 4);

-- SECTOR 5 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (100,-1,5,1,0,1100,19);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (151,-1,5,2,0,1151,20);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (255,-1,5,3,0,1255,28);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (222,-1,5,4,0,1222, 3);

-- SECTOR 6 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (143,-1,6,1,0,1143,21);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (102,-1,6,2,0,1102,17);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (361,-1,6,3,0,1361,53);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (195,-1,6,4,0,1195, 4);

-- SECTOR 7 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 99,-1,7,1,0,2099,18);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 89,-1,7,2,0,2089,29);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (331,-1,7,3,0,2331,59);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (197,-1,7,4,0,2197, 7);

-- SECTOR 8 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (371,-1,8,1,0,2371,20);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (355,-1,8,2,0,2355,23);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (141,-1,8,3,0,2141,48);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 90,-1,8,4,0,2090, 2);

-- SECTOR 9 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (352,-1,9,1,0,2352,23);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (344,-1,9,2,0,2344,18);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (147,-1,9,3,0,2147,44);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (230,-1,9,4,0,2230, 5);

-- SECTOR 10 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (236,-1,10,1,0,2236, 9);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (118,-1,10,2,0,2118, 7);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (187,-1,10,3,0,2187,27);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (178,-1,10,4,0,2178, 3);

-- SECTOR 11 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (242,-1,11,1,0,2242, 5);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (133,-1,11,2,0,2133, 3);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (233,-1,11,3,0,2233,34);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (208,-1,11,4,0,2208, 4);

-- SECTOR 12 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (128,-1,12,1,0,2128,10);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (105,-1,12,2,0,2105,15);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (114,-1,12,3,0,2114,46);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (246,-1,12,4,0,2246, 2);

-- SECTOR 13 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (135,-1,13,4,0,1135, 3);

-- SECTOR 14 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (206,-1,14,4,0,2206, 3);
