#include "Cdtlogger.h"
#include <cstring>
#include <netinet/in.h>
//#define __USE_XOPEN // A.P. needed on some linuxes
	
dtlogger::dtlogger() {
  mLevel=ntohl(mesg);
  strcpy(mbody,"");
  dtdbobj = new cmsdtdb;
  newconn = true;
}

dtlogger::dtlogger(cmsdtdb* thisobj) {
  mLevel=ntohl(mesg);
  strcpy(mbody,"");
  dtdbobj = thisobj;
  newconn = false;
}

dtlogger::~dtlogger() {
  if (newconn)
    delete dtdbobj;
}


// AP Jan09 2006: Tested on ODBC (MySQL-ORACLE)
void dtlogger::Tell(char *themsg) {
  mLevel=ntohl(mesg);
  strcpy(mbody,themsg);
  insert();
}

// AP Jan09 2006: Tested on ODBC (MySQL-ORACLE)
void dtlogger::Warn(char *themsg) {
  mLevel=ntohl(warn);
  strcpy(mbody,themsg);
  insert();
}

// AP Jan09 2006: Tested on ODBC (MySQL-ORACLE)
void dtlogger::Erro(char *themsg) {
  mLevel=ntohl(erro);
  strcpy(mbody,themsg);
  insert();
}

// Authors: S.Ventura, A.Parenti, Jan09 2006
// Modified: AP, Apr18 2007 (Tested on MySQL-ORACLE)
int dtlogger::insert() {
//   char mquer[FIELD_MAX_LENGTH];
//   int rows=0, fields=0;
  int resu=0;

//   sprintf(mquer,"INSERT INTO logger (levl,body,runid) VALUES ('%4.4s','%s','%d')",&mLevel,mbody,dtdbobj->curr_run);

  char level[10];
  sprintf(level, "%4.4s", &mLevel);
  char * insquery = "INSERT INTO logger (levl,body,runid) VALUES (?,?,?)";
  binder bindSetIns [3];
  bindSetIns[0] = binder(SQL_C_CHAR, SQL_CHAR, level, strlen(level));
  bindSetIns[1] = binder(SQL_C_CHAR, SQL_CHAR, mbody, strlen(mbody));
  bindSetIns[2] = binder(SQL_C_LONG, SQL_INTEGER, &dtdbobj->curr_run, 0);


  dtdbobj->dblock(); // lock the mutex
  //  resu=dtdbobj->sqlquery(mquer,&rows,&fields);
  dtdbobj->sqlQueryBind(insquery, bindSetIns, 3);
  resu = dtdbobj->sqlInsert();
  dtdbobj->dbunlock(); // unlock the mutex

  return resu;
}

// Authors: S.Ventura & A.Parenti
// Modified: AP Apr04 2007 (Tested on MySQL-ORACLE)
int dtlogger::fetchlast(char *msgstr) {
  int rows=0, fields=0, resu=0;

  dtdbobj->dblock(); // lock the mutex

  strcpy(msgstr,"");

  resu=dtdbobj->sqlquery("SELECT datetime,runid,body FROM logger ORDER BY msgid DESC",&rows,&fields);

  if (resu==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {
    char myfie[FIELD_MAX_LENGTH];
    //    struct tm myt;

    dtdbobj->returnfield(0,myfie);
    strcpy(msgstr,myfie);

    strcat(msgstr," Run #");
    dtdbobj->returnfield(1,myfie);
    strcat(msgstr,myfie); 

    strcat(msgstr," Message: "); 
    dtdbobj->returnfield(2,myfie);
    strcat(msgstr,myfie); 

    dtdbobj->dbunlock(); // unlock the mutex
    return 0;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return -1;
}
