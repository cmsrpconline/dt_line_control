#include "Cccbdata.h"

#include <stdlib.h>
#include <cstring>

ccbdata::ccbdata() {
  dtdbobj = new cmsdtdb;
  newconn = true;
}

ccbdata::ccbdata(cmsdtdb* thisobj) {
  dtdbobj=thisobj;
  newconn = false;
}

ccbdata::~ccbdata() {
  if (newconn)
    delete dtdbobj;
}


// Author: A.Parenti, Nov03 2006
// Modified: AP, May16 2007 (Tested on Oracle/MySQL)
int ccbdata::insert(int ccbID,int CmdCode,char *data,int datasize) {
//   char mquer[FIELD_MAX_LENGTH];
//   int rows=0, fields=0;

  char mhex[FIELD_MAX_LENGTH];
  int resu=0;
  int ii;
  
  mhex[0]=0;
  for (ii=0; ii<datasize; ii++)
    sprintf(mhex,"%s%02hhx",mhex,data[ii]);


  //  sprintf(mquer,"INSERT INTO ccbdata (RunID,ccbID,CmdCode,Data,time) VALUES (%d,%d,%d,'0x%s',CURRENT_TIMESTAMP)",dtdbobj->curr_run,ccbID,CmdCode,mhex);
  //  printf("%s\n",mquer);

  sprintf(mhex,"0x%s",mhex);
  char * insquery = "INSERT INTO ccbdata (RunID,ccbID,CmdCode,Data,time) VALUES (?,?,?,?,CURRENT_TIMESTAMP)";
  binder bindSetIns [4];
  bindSetIns[0] = binder(SQL_C_LONG, SQL_INTEGER, &(dtdbobj->curr_run), 0);
  bindSetIns[1] = binder(SQL_C_LONG, SQL_INTEGER, &ccbID, 0);
  bindSetIns[2] = binder(SQL_C_LONG, SQL_INTEGER, &CmdCode, 0);
  bindSetIns[3] = binder(SQL_C_CHAR, SQL_CHAR, mhex, strlen(mhex));


  dtdbobj->dblock(); // lock the mutex
  //  resu=dtdbobj->sqlquery(mquer,&rows,&fields);
  dtdbobj->sqlQueryBind(insquery, bindSetIns, 4);
  resu = dtdbobj->sqlInsert();

  dtdbobj->dbunlock(); // unlock the mutex

  return resu;
}

// Author: A.Parenti, Nov03 2006
// Modified: Apr04,2007 (Tested on MySQL/Oracle)
int ccbdata::retrievelast(int ccbID,char *data,char *time) {
//   char mquer[FIELD_MAX_LENGTH];
//   int rows=0, fields=0;
  int reso;

  dtdbobj->dblock(); // lock the mutex

//   sprintf(mquer,"SELECT Data,time FROM ccbdata WHERE ccbID=%d ORDER BY time DESC",ccbID);
// //  printf("%s\n",mquer);
//   reso = dtdbobj->sqlquery(mquer,&rows,&fields);


  char * tmpquery =
    "SELECT Data,time FROM ccbdata WHERE ccbID=? ORDER BY time DESC";
  binder bindSet [1];
  bindSet[0] = binder(SQL_C_LONG, SQL_INTEGER, &ccbID, 0);
  dtdbobj->sqlQueryBind(tmpquery, bindSet, 1);
  reso = dtdbobj->sqlFetchOne();

  if (reso == SQL_SUCCESS) {
    dtdbobj->returnfield(0,data);
    dtdbobj->returnfield(1,time);

    dtdbobj->dbunlock(); // unlock the mutex
    return 1;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return 0;
}


// Author: A.Parenti, Nov03 2006
// Modified: Apr04,2007 (Tested on MySQL/Oracle)
int ccbdata::retrievelast(int ccbID,int CmdCode,char *data,char *time) {
//   char mquer[FIELD_MAX_LENGTH];
//   int rows=0, fields=0;
  int reso;

  dtdbobj->dblock(); // lock the mutex

//   sprintf(mquer,"SELECT Data,time FROM ccbdata WHERE ccbID=%d AND CmdCode=%d ORDER BY time DESC",ccbID,CmdCode);
// //  printf("%s\n",mquer);
//   reso = dtdbobj->sqlquery(mquer,&rows,&fields);

  char * tmpquery =
    "SELECT Data,time FROM ccbdata WHERE ccbID=? AND CmdCode=? ORDER BY time DESC";
  binder bindSet [2];
  bindSet[0] = binder(SQL_C_LONG, SQL_INTEGER, &ccbID, 0);
  bindSet[1] = binder(SQL_C_LONG, SQL_INTEGER, &CmdCode, 0);
  dtdbobj->sqlQueryBind(tmpquery, bindSet, 2);
  reso = dtdbobj->sqlFetchOne();


  if (reso == SQL_SUCCESS) {
    dtdbobj->returnfield(0,data);
    dtdbobj->returnfield(1,time);

    dtdbobj->dbunlock(); // unlock the mutex
    return 1;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return 0;
}


// Author: G.Codispoti, Apr07 2009
// Modified: Apr07,2009 (Tested on MySQL/Oracle)
int ccbdata::ccbId(int wheel, int sector, int station, int & ccbid,
		   int & port, int & secport)
{
//   char mquer[FIELD_MAX_LENGTH];
//   int rows=0, fields=0;
  int reso = 0;
  char tmp[20];  // ccbID

  dtdbobj->dblock(); // lock the mutex

//   sprintf(mquer,"SELECT Data,time FROM ccbdata WHERE ccbID=%d AND CmdCode=%d ORDER BY time DESC",ccbID,CmdCode);
// //  printf("%s\n",mquer);
//   reso = dtdbobj->sqlquery(mquer,&rows,&fields);

  char * tmpquery =
    "select CCBID,PORT,SECPORT from CCBMAP WHERE WHEEL=? AND SECTOR=? AND STATION=?";
  //AND SECTOR=? AND STATION=?

  binder bindSet [3];
  bindSet[0] = binder(SQL_C_LONG, SQL_INTEGER, &wheel, 0);
  bindSet[1] = binder(SQL_C_LONG, SQL_INTEGER, &sector, 0);
  bindSet[2] = binder(SQL_C_LONG, SQL_INTEGER, &station, 0);
  dtdbobj->sqlQueryBind(tmpquery, bindSet, 3);
  reso = dtdbobj->sqlFetchOne();

  if (reso == SQL_SUCCESS) {
    dtdbobj->returnfield(0,tmp);
    ccbid = atoi( tmp );
    dtdbobj->returnfield(1,tmp);
    port = atoi( tmp );
    dtdbobj->returnfield(2,tmp);
    secport = atoi( tmp );
    dtdbobj->dbunlock(); // unlock the mutex
    return 1;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return 0;
}
