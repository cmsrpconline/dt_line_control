#include "Cpadcstatus.h"

#include <cstring>

padcstatus::padcstatus() {
  dtdbobj = new cmsdtdb;
  newconn = true;
}

padcstatus::padcstatus(cmsdtdb* thisobj) {
  dtdbobj = thisobj;
  newconn = false;
}

padcstatus::~padcstatus() {
  if (newconn)
    delete dtdbobj;
}


// Insert a new entry in padcstatus
// Author: A.Parenti, Oct13 2006
// Modified: AP, Apr04 2007 (Tested on MySQL-Oracle)
int padcstatus::insert(int ccbID,char *mst) {
  return insert(ccbID,mst,"");
}


int padcstatus::insert(int ccbID, char *mst, char *statusxml) {
//   char mquer[FIELD_MAX_LENGTH];
//   int rows=0, fields=0;
  int resu=0;
  int padcId=0;

  dtdbobj->dblock(); // lock the mutex

//   sprintf(mquer,"SELECT PadcId FROM ccbmap WHERE CcbID='%d'",ccbID);
//   resu=dtdbobj->sqlquery(mquer,&rows,&fields);


  char * tmpquery = "SELECT PadcId FROM ccbmap WHERE CcbID=?";
  binder bindSet [1];
  bindSet[0] = binder(SQL_C_LONG, SQL_INTEGER, &ccbID, 0);
  dtdbobj->sqlQueryBind(tmpquery, bindSet, 1);
  resu = dtdbobj->sqlFetchOne();

  if (resu == SQL_SUCCESS) {
    char myfie[LEN1];
    dtdbobj->returnfield(0,myfie);
    sscanf(myfie,"%d",&padcId);
  } else {
    dtdbobj->dbunlock(); // unlock the mutex
    return resu;
  }

  if (padcId!=0) {
//     sprintf(mquer,"INSERT INTO padcstatus (padcID,adccount,statusxml,time) VALUES ('%d','%s','%s',CURRENT_TIMESTAMP)",padcId,mst,statusxml);
//     resu=dtdbobj->sqlquery(mquer,&rows,&fields);

    char * insquery = 
      "INSERT INTO padcstatus (padcID,adccount,statusxml,time) VALUES (?,?,?,CURRENT_TIMESTAMP)";

    binder bindSetIns [3];
    bindSetIns[0] = binder(SQL_C_LONG, SQL_INTEGER, &padcId, 0);
    bindSetIns[1] = binder(SQL_C_CHAR, SQL_CHAR, mst, strlen(mst));
    bindSetIns[2] = binder(SQL_C_CHAR, SQL_CHAR, statusxml, strlen(statusxml));
    dtdbobj->sqlQueryBind(insquery, bindSetIns, 3);
    resu = dtdbobj->sqlInsert();

    dtdbobj->dbunlock(); // unlock the mutex
    return resu;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return -1;
}


// Retrieve last entry in padcstatus
// Author: A.Parenti, Oct13 2006
// Modified: AP, Apr04 2007 (Tested on MySQL-Oracle)
int padcstatus::retrievelast(int ccbID, char *mst, char *sttime) {
  char statusxml[FIELD_MAX_LENGTH];

  return retrievelast(ccbID,mst,statusxml,sttime);
}

int padcstatus::retrievelast(int ccbID, char *mst, char *statusxml, char *sttime) {
  //  char mquer[LEN2];
  //  int rows=0, fields=0
  int resu=0;

  dtdbobj->dblock(); // lock the mutex

// Reset results
  strcpy(mst,"");
  strcpy(statusxml,"");
  strcpy(sttime,"");

//   sprintf(mquer,"SELECT padcstatus.adccount,padcstatus.statusxml,padcstatus.time FROM padcstatus,ccbmap WHERE padcstatus.padcId=ccbmap.PadcId AND ccbmap.ccbID='%d'ORDER BY time DESC",ccbID);
//   resu = dtdbobj->sqlquery(mquer,&rows,&fields);

  char * tmpquery =
    "SELECT padcstatus.adccount,padcstatus.statusxml,padcstatus.time FROM padcstatus,ccbmap WHERE padcstatus.padcId=ccbmap.PadcId AND ccbmap.ccbID=? ORDER BY time DESC";
  binder bindSet [1];
  bindSet[0] = binder(SQL_C_LONG, SQL_INTEGER, &ccbID, 0);
  dtdbobj->sqlQueryBind(tmpquery, bindSet, 1);
  resu = dtdbobj->sqlFetchOne();

  if (resu == SQL_SUCCESS) {
    dtdbobj->returnfield(0,mst);
    dtdbobj->returnfield(1,statusxml);
    dtdbobj->returnfield(2,sttime);

    dtdbobj->dbunlock(); // unlock the mutex
    return 1;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return 0;
}
