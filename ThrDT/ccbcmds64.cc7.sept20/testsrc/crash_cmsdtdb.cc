#include "ccbdb/Ccmsdtdb.h"
#include "iostream"
#include "ccbcmds/apfunctions.h"

#define NNN 500
#define SERVER "127.0.0.1"
#define PORT 18889

void *prova_thr(void *input);

int main() {
  cmsdtdb *myit;

  char str[24];
  int i=0;
  int rows,cols;

// All ok up to 10000 cycles
//  while (++i<NNN) {
//    std::cout << "Step #" << i << std::endl;
//    myit = new cmsdtdb;
//
//    delete myit;
//  }

// All ok up to 10000 cycles...
//  while(++i<NNN)  {
//    std::cout << "Step #" << i << std::endl;
//    myit=new(cmsdtdb);
//
//    myit->sqlquery("select count(1) from ccbmap",&rows,&cols);
//    myit->fetchrow();
//    myit->returnfield(0,str);
//    std::cout << "Query result: " << str << std::endl;
//
//    delete (myit);
//  }

  pthread_t tid[NNN];
  int terr[NNN];
  for (i=0;i<NNN;++i) {

    terr[i]=pthread_create(&(tid[i]),NULL,prova_thr,(void*)(&i));
    printf("Thread %d started. TID=%d Err=%d\n",i,tid[i],terr[i]);
  }

  mywait();

  for (i=0;i<NNN;++i) {
    if (terr[i]==0) pthread_join(tid[i],NULL);
  }

  mywait();
}


void *prova_thr(void *input) {
  int *i=(int*)(input);

  printf("In thread %d\n",*i);

//  myCmd = new Ccommand(*trueccbid,*trueccbid,SERVER,PORT,myLog);
}
