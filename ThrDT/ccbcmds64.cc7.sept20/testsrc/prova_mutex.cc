#include <apfunctions.h>
#include <Clog.h>
#include <Ccommand.h>
#include <Cbti.h>

#include <iostream>

#define CCB_ID 0x1
#define CCB_PORT 0x1
#define SERVER "127.0.0.1"
//#define SERVER "icab149"
#define PORT 18889

// mutex
#include <pasync.h>
USING_PTYPES

// multithreading
#include <pthread.h>

#define NCCB 5

void *prova(void*), *prova2(void*);
mutex myMutex;

int main() {
  pthread_t thr[NCCB];
  int i;

  Clog* myLog = new Clog("log.txt"); // Log to file
  Ccommand* myCmd = new Ccommand(CCB_ID,CCB_PORT,CCB_ID,SERVER,PORT,myLog);

// open threads
//  for (i=0;i<NCCB;++i) pthread_create(&thr[i],NULL,prova,(void *)(&i));
  for (i=0;i<NCCB;++i) pthread_create(&thr[i],NULL,prova2,(void *)(myCmd));

// wait for thread completion
  for (i=0;i<NCCB;++i) pthread_join(thr[i],NULL);

  return 0;
}


void *prova(void *in) {

//  myMutex.lock(); // lock the mutex
  scopelock lock(myMutex); // lock the mutex
  psleep((int)1000); // wait 1 sec
  std::cout << "Prova (" << *(int*)in << ")\n";
//  myMutex.unlock(); // unlock the mutex

}


void *prova2(void *in) {

  Ccommand *myCmd = (Ccommand*)in;
  Cbti myBti = Cbti(myCmd);

  myBti.read_BTI(0,0);
  printf("read_BTI done.\n");
}
