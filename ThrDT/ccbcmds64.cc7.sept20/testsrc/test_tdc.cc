#include <apfunctions.h>
#include <Clog.h>
#include <Ccommand.h>
#include <Ctdc.h>
#include <Cconf.h>

#include <iostream>

#define CCB_ID 0x01
#define SERVER "127.0.0.1"
//#define SERVER "icab149"
#define PORT 18889


int main() {
  const char brd=2, chip=1;
  unsigned char setup[81]={0XDE,0XFF,0X1,0,0X3E,0XE3,0XD,0XF,0XD,0XC8,0XB0,0XAA,0XAA,0XAA,0X92,0X38,0,0X48,0X7C,0X70,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0X1C,0X10,0X20,0X20,0,0,0,0XF8,0XFF,0X7E}, control[5]={0XE5,0XFF,0XFF,0XFF,0X1F};

  Clog *myLog;
  Ccommand *myCmd;
  Ctdc *myTdc;
  Cconf *myConf;
  
// Initialise objects
  myLog = new Clog("log.txt"); // Log to file
//  myLog = new Clog(); // Log to DB
  myCmd = new Ccommand(CCB_ID,SERVER,PORT,myLog);
  myTdc = new Ctdc(myCmd); // New TDC object
  myConf = new Cconf("../config_files/MB1-neg-default.txt"); // config

//  myTdc->verbose_mode(true);
  myCmd->set_write_log(true); // Switch on logger

//  myTdc->MC_read_status(); // check status
//  myTdc->MC_read_status_print();

//  myTdc->config_TDC(myConf,brd,chip);
//  myTdc->config_TDC(myConf,-1,-1);
//  myTdc->config_TDC_print();
//  mywait();
//
//  myTdc->read_TDC_config("pippo.txt");
//  mywait();
//
//  myTdc->status_TDC(brd,chip);
//  myTdc->status_TDC_print();
//  mywait();
//
  myTdc->read_ROB_error();
  myTdc->read_ROB_error_print();
  mywait();
//
//  myTdc->write_TDC(brd,chip,setup,control); // 0x44
//  myTdc->write_TDC_print();
//  mywait();
//  myTdc->write_TDC(99,99,setup,control); // 0x44
//  myTdc->write_TDC_print();
//  mywait();
//
//  myTdc->write_TDC_control(brd,chip,control); // 0x18
//  myTdc->write_TDC_control_print();
//  mywait();
//
//  myTdc->read_TDC(brd,chip); // 0x43
//  myTdc->read_TDC_print();
//  mywait();
//  myTdc->read_TDC(99,99); // 0x43
//  myTdc->read_TDC_print();
//  mywait();
}
