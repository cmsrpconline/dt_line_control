#include <apfunctions.h>
#include <Cccbheader.h>

#include <iostream>

int main() {

  Cccbheader *myHdr;
  unsigned char header[SRV_HDR_LEN],dummy[]={0xea};

  myHdr = new Cccbheader(0x01,5000);

  printf("CCB: %#x Timeout: %d\n",myHdr->read_ccb_id(),myHdr->read_timeout());

  myHdr->set_timeout(5500);
  printf("CCB: %#x New timeout: %d\n",myHdr->read_ccb_id(),myHdr->read_timeout());

  myHdr->make_header(1,header);
  printf("Header 1: "); strprint(header,SRV_HDR_LEN);

  myHdr->make_header(1,7000,header);
  printf("Header 2: "); strprint(header,SRV_HDR_LEN);

  short tmpccb;
  int tmplen, tmptmout;
  myHdr->decode_header(header,&tmpccb,&tmplen,&tmptmout);
  printf("Decoded: CCB=%#x Len=%d TimePut=%d\n",tmpccb,tmplen,tmptmout);

  printf("Prova1. Header? %d\n",myHdr->is_there_header(header));
  printf("Prova2. Header? %d\n",myHdr->is_there_header(dummy));
 
}
