#include <iostream>
using namespace std;

//max returns the maximum of the two elements of type T, where T is a
//class or data type for which operator> is defined.
template <class T>
T maxx(T a, T b)
{
    return a > b ? a : b ;
}

template <class Z>
void ccc(Z a) {
  printf("ccc: %#2X\n",a);
}

int main()
{    
    cout << "max(10, 15) = " << maxx(10, 15) << endl ;
    cout << "max('k', 's') = " << maxx('k', 's') << endl ;
    cout << "max(10.1, 15.2) = " << maxx(10.1, 15.2) << endl ;
    cout << "max(\"Aladdin\", \"Jasmine\") = " << maxx("Aladdin", "Jasmine") << endl ;

    ccc(0x10);
    ccc('a');

    return 0 ;
}
