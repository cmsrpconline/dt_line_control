#include <apfunctions.h>
#include <Clog.h>
#include <Ccommand.h>
#include <Cccb.h>
#include <Ci2c.h>
#include <Cref.h>

#include <ccbdb/Ccmsdtdb.h>

#include <iostream>

#define CCB_ID 263
#define SERVER "127.0.0.1"
#define PORT 18889

int main() {

  bool error;
  char nm1[]="../ref_files/boot.xml",
    nm2[]="../ref_files/status-1.xml",
    nm3[]="../ref_files/test-1.xml",
    nm4[]="../ref_files/robstatus.xml",
    nm5[]="../ref_files/padcstatus.xml";
  int i;

  cmsdtdb dtdbobj; // DB connector

// Initialise objects
  Clog *myLog = new Clog("log.txt"); // Log to file
//  Clog *myLog = new Clog(); // Log to DB
  Ccommand *myCmd = new Ccommand(CCB_ID,SERVER,PORT,myLog);
  Cccb *myCcb = new Cccb(myCmd); // New CCB object
  Ci2c *myI2C = new Ci2c(myCmd); // New I2C object
  Cref *myRef = new Cref(nm1,nm2,nm3,nm4,nm5); // read reference from file

  myCmd->set_write_log(true); // Switch on logger
  myCmd->set_db_connector(&dtdbobj); // Set DB connector

//  myI2C->read_TDC(0,0);
//  myI2C->read_TDC_print();
//  mywait();
//
//  myI2C->status_TDC(0,0);
//  myI2C->status_TDC_print();
//  mywait();
//
//  myI2C->read_ROB_error();
//  myI2C->read_ROB_error_print();
//  mywait();
//
//  myI2C->MC_read_status(); // check status
//  myI2C->MC_read_status_print();
//  mywait();
//
//  myI2C->PWR_on(5,0); // switch off LED
//  myI2C->PWR_on_print();
//  mywait();
//
  char size=4;
  short data[]={1,2,3,4};
//  myI2C->rpc_I2C(data,size); // 0x64
//  myI2C->rpc_I2C_print();
//  mywait();
//  myI2C->led_I2C(data,size); // 0x66
//  myI2C->led_I2C_print();
//  mywait();
//
//  myI2C->read_ADC(); // 0x69
//  myI2C->read_ADC_print();
//  mywait();
//
//  myI2C->test_LED_I2C(&error);
//  printf("Test LED_I2C: Error = %d\n",error);
//  mywait();
//
//  myI2C->test_RPC_I2C(&error);
//  printf("Test RPC_I2C: Error = %d\n",error);
//  mywait();
//
//  myI2C->test_LED(&error);
//  printf("Test LED: Error = %d\n",error);
//  mywait();
//
  char fname[]="/home/parenti/LUT_PADC/LUT_PADC_ID_263_070511_1424.txt";
  short adc_count[10]={616,616,616,573,614,614,612,573,574,639};
//
  float volt_read[10], data_read[10];
  myI2C->read_PADC(&error,adc_count,volt_read);
  printf("Read PADC: Error = %d\n",error);
  for (i=0;i<10;++i)
    printf("adc_count[%d]=%4d volt_read[%d]=%5.3f V data_read[%d]=%5.3f\n",i,adc_count[i],i,volt_read[i],i,data_read[i]);
  mywait();

//  myI2C->use_PADC_LUT(fname,adc_count,data_read);
//  for (i=0;i<10;++i)
//    printf("adc_count[%d]=%4d volt_read[%d]=%5.3f V data_read[%d]=%3.6f\n",i,adc_count[i],i,volt_read[i],i,data_read[i]);
//  mywait();
//

//  myI2C->use_PADC_LUT(adc_count,data_read);
//  for (i=0;i<10;++i)
//    printf("adc_count[%d]=%4d volt_read[%d]=%5.3f V data_read[%d]=%3.6f\n",i,adc_count[i],i,volt_read[i],i,data_read[i]);
//  mywait();
//
//  myI2C->status_PADC(myRef,data_read);
//  printf("%s",myI2C->status_padc.xmlmsg.c_str());
//  printf("%s",myI2C->status_padc.stmsg.c_str());
//  mywait();
//  myI2C->status_PADC_print();
//  mywait();
//
//  myI2C->PADC_status_to_db(myRef,adc_count);
//  myI2C->status_PADC_print();
//  mywait();
}
