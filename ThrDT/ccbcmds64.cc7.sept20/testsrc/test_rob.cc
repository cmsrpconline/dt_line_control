#include <apfunctions.h>
#include <Clog.h>
#include <Ccommand.h>
#include <Crob.h>

#include <iostream>

#define CCB_ID 0x01
#define SERVER "127.0.0.1"
//#define SERVER "icab149"
#define PORT 18889

int main() {

  char brd=0x1;

  char nm1[]="../ref_files/boot.xml",
    nm2[]="../ref_files/status-1.xml",
    nm3[]="../ref_files/test-1.xml",
    nm4[]="../ref_files/robstatus.xml",
    nm5[]="../ref_files/padcstatus.xml";

  Clog *myLog;

// Initialise objects
  myLog = new Clog("log.txt"); // Log to file
//  myLog = new Clog(); // Log to DB
  Ccommand *myCmd = new Ccommand(CCB_ID,SERVER,PORT,myLog);
  Crob *myRob = new Crob(myCmd); // New ROB object
  Cconf *myConf = new Cconf("../config_files/MB1-neg-default.txt"); // config
  Cref *myRef = new Cref(nm1,nm2,nm3,nm4,nm5); // read reference from file

//  myRob->verbose_mode(true); // Switch to verbose mode
  myCmd->set_write_log(true); // Switch on logger

  myRob->status_ROB_to_db(myRef);
  myRob->status_ROB_print();
  mywait();
//
//  myRob->read_TDC(0,0);
//  myRob->read_TDC_print();
//
//  myRob->status_TDC(0,0);
//  myRob->status_TDC_print();
//
//  myRob->MC_read_status(); // check status
//  myRob->MC_read_status_print();
//  mywait();

//  myRob->config_ROB(myConf);
//  mywait();

//  myRef->print_rob_ref();
//  mywait();
//
//  myRob->status_ROB(myRef);
//  printf("%s",myRob->status_rob.xmlmsg.c_str());
//  mywait();
//  myRob->status_ROB_print();
//  mywait();

//  myRob->read_ROB_pwr(); // 0x5B
//  myRob->read_ROB_pwr_print();
//  mywait();
//
//  myRob->reset_ROB(); // 0x32
//  myRob->reset_ROB_print();
//  mywait();
//
//  myRob->read_ROB_error(); // 0x33
//  myRob->read_ROB_error_print();
//  mywait();
//
//  myRob->run_BIST_test(brd); // 0x41
//  myRob->run_BIST_test_print();
//  mywait();
//  myRob->run_BIST_test(99); // 0x41
//  myRob->run_BIST_test_print();
//  mywait();
//
//  myRob->read_out_ROB(brd); // 0x6C
//  myRob->read_out_ROB_print();
//  mywait();
//  myRob->read_out_ROB(99); // 0x6C
//  myRob->read_out_ROB_print();
//  mywait();
//
//  myRob->set_CPU_ck_delay(80); // 0x26
//  myRob->set_CPU_ck_delay_print();
//  mywait();
//
//  myRob->disable_temp_test(1); // 0x83
//  myRob->disable_temp_test_print();
//  mywait();
}
