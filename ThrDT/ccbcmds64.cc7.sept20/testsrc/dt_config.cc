#include <ccbcmds/Cdtdcs.h>
#include <ccbcmds/apfunctions.h>

#include <ccbcmds/Clog.h>
#include <ccbcmds/Ccommand.h>
#include <ccbcmds/Cccb.h>

#include <unistd.h>

int main(int argc, char *argv[]) {
  char partname[PARTNAMESIZE]; // Partition name
  char confname[CONFNAMESIZE]; // Configuration name

  int i, j, nccb, nexcluded;
  int *ccblist = new int[NMAXCCB];
  dtdcs mydtdcs;

  mydtdcs.partSetMonitorTimeInterval(3600,3600); // Set monitoring time

  if (argc!=3) {
    printf("*** Usage: dt_config <partition name> <config name>\n");

// List available partitions
    printf("Listing partitions...\n\n.");
    mydtdcs.partShowAvailable();
    mywait();

//  printf("Listing configurations...\n\n");
    mydtdcs.confShowAvailable();
    mywait();

    return -1;
  }  else {
    sscanf(argv[1],"%s",&partname);
    sscanf(argv[2],"%s",&confname);
  }

// Initialize the partition
  mydtdcs.disableAutoRecover();
  mydtdcs.partInitialize(partname);

  nccb = mydtdcs.partGetNCcb();

// Configure the partition
  mydtdcs.confStart(confname);
  printf("Configuring partition with '%s'\n",mydtdcs.confGetConfName());

// Wait until config is over
  bool cdone=false;
  Econfstatus cstatus;
  while (!cdone) {
    cstatus=mydtdcs.confGetStatus();

    if (cstatus==c_configured) {
      cdone=true;
      printf("Time: %s. Configuration done.\n",get_time());
    } else if (cstatus==c_undef) {
      cdone=true;
      printf("Time: %s. Configuration status undefined.\n",get_time());
    } else if (cstatus==c_configuring) {
      cdone=false;
      printf("Time: %s. Configuration ongoing.\n",get_time());
    } else if (cstatus==c_error) {
      cdone=true;
      printf("Time: %s. Configuration error.\n",get_time());
    }

    if (!cdone) sleep(10);
  }

  return 0;
}
