#include <apfunctions.h>
#include <Clog.h>
#include <Ccommand.h>
#include <Cmisc.h>

#include <iostream>

#define CCB_ID 0x01
#define SERVER "127.0.0.1"
//#define SERVER "icab149"
#define PORT 18889

int main() {

  Clog *myLog;
  Ccommand *myCmd;
  Cmisc *myMisc;

// Initialise objects
  myLog = new Clog("log.txt"); // Log to file
//  myLog = new Clog(); // Log to DB
  myCmd = new Ccommand(CCB_ID,SERVER,PORT,myLog);
  myMisc = new Cmisc(myCmd); // New MISC object

//  myMisc->verbose_mode(true); // Switch to verbose mode

  myCmd->set_write_log(true); // Switch on logger
//
//  myMisc->read_TDC(0,0);
//  myMisc->read_TDC_print();
//  mywait();
//
//  myMisc->status_TDC(0,0);
//  myMisc->status_TDC_print();
//  mywait();
//
//  myMisc->read_ROB_error();
//  myMisc->read_ROB_error_print();
//  mywait();
//
  myMisc->MC_read_status();
  myMisc->MC_read_status_print();
  mywait();
//
//  myMisc->SYNC(); // 0x2B
//  myMisc->SYNC_print();
//  mywait();
//
//  myMisc->SNAP_reset(); // 0x2C
//  myMisc->SNAP_reset_print();
//  mywait();
//
//  myMisc->SOFT_reset(); // 0x2D
//  myMisc->SOFT_reset_print();
//  mywait();
//
//  myMisc->select_L1A_veto(0); // 0x9F
//  myMisc->select_L1A_veto_print();
//  mywait();
//
//  myMisc->clear_TRIGGER_histogram(); // 0xAD
//  myMisc->clear_TRIGGER_histogram_print();
//  mywait();
//
//  myMisc->read_TRIGGER_histogram(1); // 0xB0
//  myMisc->read_TRIGGER_histogram_print();
//  mywait();
//
  myMisc->TRIGGER_frequency(); // 0xB2
  myMisc->TRIGGER_frequency_print();
  mywait();
}
