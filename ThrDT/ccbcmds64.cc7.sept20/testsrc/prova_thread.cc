#include <Clog.h>
#include <Ccommand.h>
#include <Cccb.h>
#include <apfunctions.h>

#include <iostream>

#include <pthread.h> // multithreading
#include <pasync.h> // for psleep()
#include <ptime.h> // timing routines

#define NumCcb 7
#define NumStep 3
#define WriteLog false

#define SERVER "127.0.0.1"

struct info {
  short ccb_id;
  int port;
  int delay;
};

int main() {
  short i;
  void *prova(void *);
  pthread_t thr[NumCcb];
  info *Pcp[NumCcb];

  pt::datetime start, stop;

  for (i=0;i<NumCcb;++i) {
    Pcp[i] = new info;
    Pcp[i]->ccb_id = i+0x1; // CCB ID 
    Pcp[i]->port   = 18889; // TCP port
    Pcp[i]->delay  = 500 + 50*i;
  }

  start = pt::now();

  for (i=0;i<NumCcb;++i) pthread_create(&thr[i],NULL,prova,(void *)(Pcp[i]));

  for (i=0;i<NumCcb;++i) pthread_join(thr[i],NULL);

  stop = pt::now();

  printf("Elapsed time: %d (ms)\n",stop-start);

  return 0;
}

void *prova(void *Ptmp) {
  int i;
  info *inpu=(info*)Ptmp;

  Clog *myLog;
  Ccommand *myCommand;
  Cccb *myCcb;
  Cconf *myConf;

  myLog = new Clog("log.txt");
  myCommand = new Ccommand(inpu->ccb_id,myLog);
  myCcb = new Cccb(myCommand);
  myConf = new Cconf("../config_files/MB1-neg-default.txt"); // New Conf object

  myCommand->set_server(SERVER,inpu->port);

  std::cout << "\nCCB " << myCommand->read_ccb_id() << ". Server set to [" << myCommand->read_server_name() << ":" << myCommand->read_server_port() << "]" << std::endl;

  for (i=0;i<NumStep;++i) {
//  while(1) {
    short confstatus, confcrc;
//    myCcb->MC_read_status(WriteLog);
//    myCcb->MC_read_status_print();
//    myCcb->restart_CCB();
//    myCcb->restart_CCB_print();
    myCcb->config_mc(myConf,CALL,&confstatus,&confcrc); // config mc
  }

  delete myLog;
  delete myCommand;
  delete myCcb;
}
