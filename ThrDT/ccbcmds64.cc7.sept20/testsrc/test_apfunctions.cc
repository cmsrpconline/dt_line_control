#include <apfunctions.h>

#include <iostream>
#include <string.h>

#define NAME RobPresMsk
#define TAG  "RobPresMsk"
#define DIM  7


int main() {
  unsigned char str1[10]={0xFF,2,3,0,5,6,0,8,9,0xa}, str2[10];
  char str3[50];
  int l;
  std::string str4;

//  mystrcpy((char*)str2,(char*)str1,10);
//  strncpy((char*)str2,(char*)str1,10); // Don't copy chars after '\0'!!!
//  strprint((char*)str2,10);
//  strprint(str1,10);
//  strprint(str2,10);

//  array_to_string(str1,10,str3);
//  printf("str3: %s\n",str3);
//
//  array_to_string((char*)str1,10,&str4);
//  printf("str4: %s\n",str4.c_str());
//
//  array_to_string(str1,10,str3);
//  string_to_array(str3,16,(char*)str1,&l);
//  strprint(str1,l);
//  string_to_array(str3,16,str1,&l);
//  strprint(str1,l);

//  double vector[5]={1.0,0.5,1.2,1.3,1.4};
//  unsigned int vector[5]={1,2,3,4,5};
//  printf("Before: vector=%f %f %f %f %f\n",vector[0],vector[1] ,vector[2],
//         vector[3],vector[4]);
//  vector_reset(vector,4);
//  printf("After: vector=%f %f %f %f %f\n",vector[0],vector[1] ,vector[2],
//         vector[3],vector[4]);

  time_t start, stop;
  start = time(NULL);
//  start = clock();
//  apsleep(2130);
//  apsleep(200);
//  apsleep(6990);
  apsleep(9990);
  stop = time(NULL);
//  stop = clock();
  printf("Start:%d Stop:%d\n",start,stop);
  printf("Elapsed time: %d sec.\n",stop-start);
//  printf("Elapsed time: %d sec.\n",float(stop-start)/float(CLOCKS_PER_SEC));


 //  printf("Time: %s\n",get_time());

//  host_to_ccb((char)0x11,str1);
//  host_to_ccb((unsigned char)0x11,str1);
//  strprint(str1,1);

//  ccb_to_host(str1,str2);
//  strprint(str2,1);
//  ccb_to_host(str1,str3);
//  strprint(str3,1);

//  unsigned int tmp1=0xFCFF1111;
//  float tmp1=1.435;
//  wstring(tmp1,0,str1);
//  int idx = rstring(str1,0,&tmp1);
//  printf("idx=%d, var=%f\n",idx,tmp1);
//
//  std::string prova="<root>   <abcdefghixx>1.12</abcdefghixx><cc>-3</cc> <x12 idx=\"1\">123</x12></root>";
//  float iii;
//  char tag[20];
//  printf("Stringa: \"%s\"\n\n",prova.c_str());
//  read_from_xml(prova,strcpy(tag,"abcdefghixx"),&iii);
//  printf("%s: \"%f\"\n",tag,iii);
//  read_from_xml(prova,strcpy(tag,"cc"),&iii);
//  printf("%s: \"%f\"\n",tag,iii);
//  strcpy(tag,"x12");
//  printf("%s: \"%s\"\n",tag,xml_get(prova,tag,1).c_str());
//  printf("%s: \"%s\"\n",tag,xml_get(prova,tag).c_str());

//  unsigned short NAME[DIM];
//  int i;
//  std::string refstr, sss;
//  refstr=read_file("../ref_files/test-1.xml");
//  printf("File:\n%s",refstr.c_str());
//  read_from_xml(refstr,TAG,DIM,NAME);
//  for (i=0;i<DIM;++i)
//    printf("%s[%u]=%d\n",TAG,i,NAME[i]);
//  sss = xml_get(refstr,"AdcNoiseMax",25);
//  printf("Value=\"%s\"\n",sss.c_str());
//  sss = xml_get(refstr,"Flags");
//  printf("Value=\"%s\"\n",sss.c_str());
}
