#include <apfunctions.h>
#include <Clog.h>
#include <Ccommand.h>
#include <Cbti.h>

#include <iostream>

#define CCB_ID 0x1
#define SERVER "127.0.0.1"
//#define SERVER "icab149"
#define PORT 18889

int main() {
  const char brd=1, chip=2;
  int i;

// configurations for 0x14
  unsigned char conf[11]={0XA5,0X7F,0XDE,0,0X1D,0XCB,0,0X10,0X7E,0X39,0X7F},
    testin[13]={0X3,0XFF,0XFF,0XFF,0xc1,0xc,0xaa,0x5,0x3,0x1,0x2,0x1,0xFF};
// configurations for 0x54
  unsigned char
    conf_new[14]={0X29,0X17,0X3F,0X1E,0,0X1,0X37,0XB,0,0X1,0X1,0X3E,0XE,0XD},
    testin_new[17]={0x10,0,0X3F,0X3F,0X3F,0X3F,0X30,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0xa};
// traces for 0x52
    short trace[9]={10,4,5,2,6,22,15,4,7};

  Clog *myLog;
  Ccommand *myCmd;
  Cbti *myBti;
  Cconf *myConf;

// Initialise objects
  myLog = new Clog("log.txt"); // Log to file
//  myLog = new Clog(); // Log to DB
  myCmd = new Ccommand(CCB_ID,SERVER,PORT,myLog);
  myConf = new Cconf("../config_files/MB1-neg-default.txt"); // config
  myBti = new Cbti(myCmd); // New BTI object

  myCmd->set_write_log(true); // Switch on logger

//  myBti->read_TDC(0,0);
//  myBti->read_TDC_print();
//  mywait();
//
//  myBti->status_TDC(0,0);
//  myBti->status_TDC_print();
//  mywait();
//
  myBti->read_ROB_error();
  myBti->read_ROB_error_print();
  mywait();
//
//
//  myBti->MC_status_from_db();
//  myBti->MC_read_status_print();
//  mywait();
//
//  myBti->MC_read_status();
//  myBti->MC_read_status_print();
//  mywait();
//
//  myBti->config_BTI(myConf,-1,-1);
//  myBti->config_BTI_print();
//  mywait();
//
//  myBti->read_BTI_config("pippo.txt");
//  mywait();
//  myBti->write_BTI(brd,chip,conf,testin); // 0x14
//  myBti->write_BTI_print();
//  myBti->read_BTI(brd,chip); // 0x19
//  myBti->read_BTI_print();
//  mywait();
//  myBti->read_BTI(99,99); // 0x19
//  myBti->read_BTI_print();
//  mywait();
//
//  myBti->write_BTI_6b(brd,chip,conf_new,testin_new); // 0x54
//  myBti->write_BTI_6b_print();
//  myBti->read_BTI_6b(brd,chip); // 0x59
//  myBti->read_BTI_6b_print();
//  mywait();
//  myBti->read_BTI_6b(99,99); // 0x59
//  myBti->read_BTI_6b_print();
//  mywait();
//
//  myBti->enable_BTI(brd,0xFFFF,0xFF00); // 0x51
//  myBti->enable_BTI_print();
//  mywait();
//
//  myBti->load_BTI_emulation(brd,chip,trace); // 0x52
//  myBti->load_BTI_emulation_print();
//  mywait();
//
//  myBti->start_BTI_emulation(brd,chip); // 0x53
//  myBti->start_BTI_emulation_print();
//  mywait();
//
//  myBti->new_start_BTI_emulation(brd,0x1000F1FA); // 0x79
//  myBti->new_start_BTI_emulation_print();
//  mywait();
//
  myBti->compare_BTI_register(brd,chip); // 0x9c
  myBti->compare_BTI_register_print();
//  mywait();
}
