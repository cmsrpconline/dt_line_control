#include <apfunctions.h>
#include <Clog.h>
#include <Ccommand.h>
#include <Cttcrx.h>

#include <iostream>

#define CCB_ID 0x00
#define SERVER "127.0.0.1"
//#define SERVER "icab149"
#define PORT 18889

int main() {

  Clog *myLog;
  Ccommand *myCmd;
  Cttcrx *myTtcrx;

// Initialise objects
  myLog = new Clog("log.txt"); // Log to file
//  myLog = new Clog(); // Log to DB
  myCmd = new Ccommand(CCB_ID,SERVER,PORT,myLog);
  myTtcrx = new Cttcrx(myCmd); // New TTCrx object

//  myTtcrx->verbose_mode(true);
  myCmd->set_write_log(true); // Switch on logger

//  myTtcrx->read_TDC(0,0);
//  myTtcrx->read_TDC_print();
//  mywait();
//
//  myTtcrx->status_TDC(0,0);
//  myTtcrx->status_TDC_print();
//  mywait();
//
  myTtcrx->read_ROB_error();
  myTtcrx->read_ROB_error_print();
  mywait();
//
//  myTtcrx->MC_read_status(); // check mc status
//  myTtcrx->MC_read_status_print();
//  mywait();
//
//  myTtcrx->get_TTCRX_id();
//  myTtcrx->get_TTCRX_id_print();
//  mywait();
//
//  myTtcrx->set_TTC_fine_delay(1,50.0); // 0x8E
//  myTtcrx->set_TTC_fine_delay_print();
//  mywait();
//
//  myTtcrx->write_TTCRX(0,0xE); // set fine_delay_1 to 0ns (0x27)
//  myTtcrx->write_TTCRX_print();
//  mywait();
//
//  myTtcrx->read_TTCRX(0); // read fine_delay_1
//  myTtcrx->read_TTCRX_print();
//  mywait();
//
//  myTtcrx->write_TTCRX(1,0x37); // set fine_delay_2 to 19ns (0x27)
//  myTtcrx->write_TTCRX_print();
//  mywait();
//
//  myTtcrx->read_TTCRX(1); // read fine_delay_2
//  myTtcrx->read_TTCRX_print();
//  mywait();
//
//  myTtcrx->get_TTCRX_fine_delay(0);
//  myTtcrx->get_TTCRX_fine_delay_print();
//  mywait();
//
//  myTtcrx->get_TTCRX_fine_delay(1);
//  myTtcrx->get_TTCRX_fine_delay_print();
//  mywait();
//
//  myTtcrx->set_TTCRX_coarse_delay(50.0,100.0);
//  myTtcrx->set_TTCRX_coarse_delay(0.0,0.0);
//  myTtcrx->set_TTCRX_coarse_delay_print();
//  mywait();
//
//  myTtcrx->get_TTCRX_coarse_delay();
//  myTtcrx->get_TTCRX_coarse_delay_print();
//  mywait();
//
//  myTtcrx->reset_TTCRX_control_register();
//  myTtcrx->reset_TTCRX_control_register_print();
//  mywait();
//
//  myTtcrx->set_TTCRX_control_register(0x99);
//  myTtcrx->set_TTCRX_control_register_print();
//  mywait();
//
//  myTtcrx->get_TTCRX_control_register();
//  myTtcrx->get_TTCRX_control_register_print();
//  mywait();
//
//  myTtcrx->reset_TTCRX_error_counter();
//  myTtcrx->reset_TTCRX_error_counter_print();
//  mywait();
//
//  myTtcrx->get_TTCRX_error_counter();
//  myTtcrx->get_TTCRX_error_counter_print();
//  mywait();
//
//  myTtcrx->reset_TTCRX_status_register(false,true);
//  myTtcrx->reset_TTCRX_status_register_print();
//  mywait();
//
//  myTtcrx->get_TTCRX_status_register();
//  myTtcrx->get_TTCRX_status_register_print();
//  mywait();
//
//  myTtcrx->reset_TTCRX_bunch_counter();
//  myTtcrx->reset_TTCRX_bunch_counter_print();
//  mywait();
//
//  myTtcrx->get_TTCRX_bunch_counter();
//  myTtcrx->get_TTCRX_bunch_counter_print();
//  mywait();
//
//  myTtcrx->reset_TTCRX_event_counter();
//  myTtcrx->reset_TTCRX_event_counter_print();
//  mywait();
//
//  myTtcrx->get_TTCRX_event_counter();
//  myTtcrx->get_TTCRX_event_counter_print();
//  mywait();
//
//  unsigned char data[]={0xff,0xff,0xff};
//  myTtcrx->set_TTCRX_config(data);
//  myTtcrx->set_TTCRX_config_print();
//  mywait();
//
//  myTtcrx->reset_TTCRX_config();
//  myTtcrx->reset_TTCRX_config_print();
//  mywait();
//
//  myTtcrx->get_TTCRX_config();
//  myTtcrx->get_TTCRX_config_print();
//  mywait();
//
//  myTtcrx->read_TTCRX(0); // 0x28
//  myTtcrx->read_TTCRX(1); // 0x28
//  myTtcrx->read_TTCRX_decode((unsigned char*)"\x29\00\00");
//  myTtcrx->read_TTCRX_print();
//  mywait();
//
//  myTtcrx->TTC_skew((char)10); // 0x48
//  myTtcrx->TTC_skew_print();
//  mywait();
//
//  myTtcrx->emul_TTC((char)2); // 0x50
//  myTtcrx->emul_TTC_print();
//  mywait();
//
//  myTtcrx->sel_TTC_ck((char)0); // 0x2A
//  myTtcrx->sel_TTC_ck_print();
//  mywait();
//
//  myTtcrx->reset_lose_lock_counter(); // 0x8A
//  myTtcrx->reset_lose_lock_counter_print();
//  mywait();
//
//  myTtcrx->set_TTC_fine_delay((char)0,10.0); // 0x8E
//  myTtcrx->set_TTC_fine_delay_print();
//  mywait();
//
//  myTtcrx->write_TTC_boot(1,0xE); // 0xE3
//  myTtcrx->write_TTC_boot_print();
//  mywait();
//
//  myTtcrx->read_TTC_boot(1); // 0xE2
//  myTtcrx->read_TTC_boot_print();
//  mywait();
//
  myTtcrx->find_TTC_boot(); // 0xE0
  myTtcrx->find_TTC_boot_print();
//  mywait();
}
