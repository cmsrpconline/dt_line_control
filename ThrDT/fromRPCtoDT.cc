#include <fstream>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <string>
#include <map>

class DTindx{
public:
  DTindx() :_w(-9),_s(-9),_m(-9){}
  DTindx(int w, int s, int m) :_w(w),_s(s),_m(m){}


  int wheel() const {return _w;}
  int sector() const {return _s;}
  int station() const {return _m;}
  
  bool operator<(const DTindx& dtind) const{
    if(dtind.wheel()!=this->wheel())
      return dtind.wheel()<this->wheel();
    else if(dtind.sector()!=this->sector())
      return dtind.sector()<this->sector();
    else if(dtind.station()!=this->station())
      return dtind.station()<this->station();
    return false;
  }

private:
  const int _w;
  int _s;
  int _m;

};

class CCBserver{
public:
  CCBserver() : _t(""),_p(-1){}
  CCBserver(std::string t, int p) : _t(t),_p(p){}
  std::string tcp() {return _t;}
  int port(){return _p;}

private:
  std::string _t;
  int _p;  
};

int main(){
  int sz=0;
  char c[500];
  std::map<DTindx, CCBserver> dtcb;
  std::ifstream indt("CCBserversDT.prn");
  while (indt.good()||sz>2){
    indt.getline(c,500);
    std::stringstream bufs;
    bufs<<c;
    sz=bufs.str().size();
    if (sz>2 && bufs.str().find("CCBID") == bufs.str().npos){
      int id,w,s,m,c,cr,o,port,pid,sp;
      std::string node,node1,lnode;
      bufs>>id>>w>>s>>m>>c>>cr>>o>>port>>node>>pid>>sp>>node1>>lnode;
      DTindx ind(w,s,m);
      CCBserver cb(node,port);
      dtcb[ind]=cb;
    }
  }

  std::ifstream inrpc("BadAccessCamera.list");
  while(inrpc.good()||sz>2){
    inrpc.getline(c,500);
    std::stringstream  bufs;
    bufs << c; 
    sz=bufs.str().size();
    if (sz>2 && bufs.str().find("Chamber") == bufs.str().npos){
      std::string rpc,node;
      int r,w,s,m,p;
      bufs>>rpc>>r>>w>>s>>m>>node>>p;
      std::cout <<"RPC "<<std::left<<std::setw(18)<<rpc
		<<" W"<<std::right<<std::setw(2)<<std::setfill('+')<<w
		<<"/S"<<std::setw(2)<<std::setfill('0')<<s<<"/MB"<<m
		<<" node="<<node<<":"<<p
		<<" rbx="<<r<<std::endl;
      std::cout <<std::setfill(' ');
      DTindx ind(w,s,m);
      CCBserver cb = dtcb[ind];
      std::cout <<"CCBSERVER "<<cb.tcp()<<":"<<cb.port()<<std::endl;
    }
  } 
  
  

}
