#include <ccbcmds/apfunctions.h>
#include <ccbcmds/Cdef.h>
#include <ccbcmds/Ccommand.h>
#include <ccbcmds/Cccb.h>
#include <ccbcmds/Ci2c.h>
#include <ccbcmds/Clog.h>

#include <cstdio>
#include <iostream>
#include <fstream>
#include <ctime>


#define PCA9544 0x70 
#define AD7417  0x28
#define ciccio "10.176.60.236"

int main() {

  short ccbid;
  short ctrl_data[I2C_DATA_MAX];
  short err_data[I2C_DATA_MAX];
  

  // I need to select the Minicrate via Wheel Sector Station

  Clog *myLog=new Clog("ciccio.txt"); // No ccbcmds log

  //  ccbid = 28;
  int ccbport=10;
  std::cout << "Open Connection on "<<ciccio<<":"<<ccbport
	    <<" for ccb id "<<ccbid
	    <<std::endl;
  Ccommand *myCmd =new Ccommand(ccbport,ciccio,CCBPORT,myLog);
  if(!myCmd->connect()){
    std::cout <<" No connection "<<std::endl;
    return -1;
  }
  
  std::cout << " done it!" <<myCmd<<std::endl;

  return 0;
}
