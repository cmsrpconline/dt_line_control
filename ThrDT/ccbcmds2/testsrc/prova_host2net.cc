#include <apfunctions.h>

#include <iostream>
#include <cstdio>

// Test the conversion from "host" to "netework" format of numbers

int main() {
  short in1, out1;
  int in2, out2;
  float in3, out3;

  unsigned char str[4];

  std::cout << "Insert a short: ";
  std::cin >> in1;
  host_to_ccb(in1,str);
  std::cout << "string=";
  strprint(str,2);
  ccb_to_host(str,&out1);
  std::cout << "short: " << out1 << std::endl;


  std::cout << "Insert an int: ";
  std::cin >> in2;
  host_to_ccb(in2,str);
  std::cout << "string=";
  strprint(str,4);
  ccb_to_host(str,&out2);
  std::cout << "int: " << out2 << std::endl;


  std::cout << "Insert a float: ";
  std::cin >> in3;
  host_to_ccb(in3,str);
  std::cout << "string=";
  strprint(str,4);
  ccb_to_host(str,&out3);
  std::cout << "float: " << out3 << std::endl;

  return 0;
}
