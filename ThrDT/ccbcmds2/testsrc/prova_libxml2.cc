#include <ccbcmds/apfunctions.h>
#include <libxml/tree.h>

#define ROOTNAME "test_reference_values"
#define TAGNAME "AdcNoiseMax"
#define ATTNAME "idx"

int clean() {
/* Shutdown libxml */
  xmlCleanupParser();
  return 0;
}

int main() {
  char buffer[9000];
  xmlDocPtr doc=NULL; // Document pointer
  xmlNodePtr cur=NULL; // Node pointer
  xmlChar *value, *attr; // Read value, attribute

// Read file
  strcpy(buffer,read_file("../ref_files/test-1.xml"));

// Init libxml
  xmlInitParser();
  LIBXML_TEST_VERSION;

// Declare document pointer
  doc = xmlParseMemory(buffer,strlen(buffer));
  if (doc == NULL)
    return clean();

// Declare node pointer, point to root element
  cur = xmlDocGetRootElement(doc);
  if (cur == NULL)
    return clean();

  if (xmlStrcmp(cur->name,(const xmlChar *) ROOTNAME))
    return clean();

  cur = cur->xmlChildrenNode; // Point to first child of root
  while (cur != NULL) {
    if ((!xmlStrcmp(cur->name, (const xmlChar *)TAGNAME))){
      value = xmlNodeGetContent(cur);
      attr  = xmlGetProp(cur, (const xmlChar *)ATTNAME);
      printf("Value=%s Attr=%s\n",value,attr);
    }    
    cur = cur->next;
  }

  return clean();
}
