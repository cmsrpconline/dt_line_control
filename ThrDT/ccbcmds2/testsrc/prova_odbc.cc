#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>

#include <sqlext.h>

//#define USE_ORACLE

// Oracle DATASOURCE
#ifdef USE_ORACLE
  #define DATASOURCE "drifttube2"
////  #define DATASOURCE "ORACLE"
  #define NAME "drifttube"
  #define PSWD "daqcms"

#else
// MySQL DATASOURCE
  #define DATASOURCE "cmsdtdb"
  #define NAME "daqcms"
  #define PSWD "daqcms"
#endif

#define STR_LEN 40
#define FIELD_MAX_LENGTH 4096 // Maximum dimension (bytes) of a DB field

void geterror(SQLHANDLE);

int main() {

  SQLHANDLE henv;          // Environment handle
  SQLHANDLE hdbc;          // Connection handle
  SQLHANDLE hstmt;         // Statement handle
  SQLRETURN connct_rc;     // Connection return code
  SQLRETURN query_rc;      // Query return code

/*************/
/*  Connect  */
/*************/
  UCHAR info[STR_LEN]; // info string for SQLGetInfo
  SQLSMALLINT cbInfoValue;
  printf("*** Connecting to database\n");

  connct_rc = SQLAllocHandle(SQL_HANDLE_ENV,SQL_NULL_HANDLE,&henv);
  if (!SQL_SUCCEEDED(connct_rc)) {
    geterror(hdbc);
    return -1;
  }

  connct_rc = SQLSetEnvAttr(henv,SQL_ATTR_ODBC_VERSION,(void*)SQL_OV_ODBC3,0);
  if (!SQL_SUCCEEDED(connct_rc)) {
    geterror(hdbc);
    return -1;
  }

  connct_rc = SQLAllocHandle(SQL_HANDLE_DBC, henv, &hdbc);
  if (!SQL_SUCCEEDED(connct_rc)) {
    geterror(hdbc);
    return -1;
  }

  connct_rc = SQLConnect(hdbc, (SQLCHAR*)DATASOURCE, SQL_NTS,
              (SQLCHAR*)NAME, SQL_NTS, (SQLCHAR*)PSWD, SQL_NTS);
  if (!SQL_SUCCEEDED(connct_rc)) {
    geterror(hdbc);
    return -1;
  }

  connct_rc = SQLAllocHandle(SQL_HANDLE_STMT,hdbc,&hstmt);
  if (!SQL_SUCCEEDED(connct_rc)) {
    geterror(hdbc);
    return -1;
  }

// Get DB info
  SQLGetInfo(hdbc,SQL_SERVER_NAME, &info, STR_LEN, &cbInfoValue);
  if (info[0]!=0) printf("Server: %s\n", info);

  SQLGetInfo(hdbc,SQL_DBMS_NAME, &info, STR_LEN, &cbInfoValue);
  if (info[0]!=0) printf("DB type: %s\n", info);

  SQLGetInfo(hdbc,SQL_DBMS_VER, &info, STR_LEN, &cbInfoValue);
  if (info[0]!=0) printf("DB version: %s\n", info);

  printf("Connected!\n\n");

/******************/
/* Send SQL Query */
/******************/
  SQLCHAR sql_field[FIELD_MAX_LENGTH];
  SQLSMALLINT cols;
  SQLINTEGER cbValue, ColumnSize;

  char c, mquery[FIELD_MAX_LENGTH]="";

  printf("Query? ");
    while ((c=getchar()) != '\n')
      sprintf(mquery,"%s%c",mquery,c);

  query_rc = SQLExecDirect(hstmt,(SQLCHAR*)mquery,strlen(mquery)); // Send query
  SQLNumResultCols(hstmt,&cols); // Number of read columns

  printf("*** Query: '%s'.\nResults:\n",mquery);
  if (!SQL_SUCCEEDED(query_rc)) geterror(hdbc);
  while (SQLFetch(hstmt)==SQL_SUCCESS) { // Fetch rows
    for (int ii=1; ii<=cols; ++ii) {
      SQLGetData(hstmt, (SQLSMALLINT)(ii), SQL_C_CHAR, sql_field,
        FIELD_MAX_LENGTH, &cbValue); // Convert field into chars
      ColumnSize = strlen((char*)sql_field); // Field length
      printf("%s ",sql_field);
    }
    putchar('\n');
  }
  putchar('\n');

/****************/
/*  Disconnect  */
/****************/
  printf("Disconnecting from DB\n");
  printf("Freeing hstmt: %d\n",SQLFreeHandle(SQL_HANDLE_STMT,hstmt));
  printf("Disconnecting hdbc: %d\n",SQLDisconnect(hdbc));
  printf("Freeing hdbc: %d\n",SQLFreeHandle(SQL_HANDLE_DBC,hdbc));
  printf("Freeing henv: %d\n",SQLFreeHandle(SQL_HANDLE_ENV,henv));
}


void geterror(SQLHANDLE hdbc) {
  SQLCHAR SQLState[6], MessageText[257];
  SQLSMALLINT TextLength;
  SQLINTEGER SQLCode;

  SQLGetDiagRec(SQL_HANDLE_DBC,hdbc,1,SQLState,&SQLCode,
    MessageText,256,&TextLength);

  printf("Connection Error %d %s: %s\n",SQLCode,SQLState,MessageText);
}
