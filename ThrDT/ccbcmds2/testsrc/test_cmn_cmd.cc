#include <apfunctions.h>
#include <Clog.h>
#include <Ccommand.h>
#include <Ccmn_cmd.h>
#include <Cccb.h>

#include <ccbdb/Ccmsdtdb.h>

#include <iostream>

//#include <ptypes/pasync.h> // for plseep()
//#include <ptypes/ptypes.h>
//#include <ptypes/ptime.h>

#define CCBID 0

//#define SERVER "127.0.0.1"
#define SERVER "icab149"
#define PORT 18889
#define NNN 100

int main() {
  int i;
  Ccmn_cmd *myCmn, *myCmn2;

  cmsdtdb dtdbobj; // DB connector

  Clog *myLog = new Clog("log.txt"); // Log to file
//  Clog *myLog = new Clog(); // Log to DB

//  Ccommand *myCommand = new Ccommand();
//  Ccommand *myCommand = new Ccommand(CCBID,myLog);
  Ccommand *myCommand = new Ccommand(CCBID,SERVER,PORT,myLog);
  myCommand->set_db_connector(&dtdbobj); // Set DB connector
//  myCommand->set_server("icab149",18889); // set the server and port
//  myCommand->set_server("lxparenti",18888); // set the server and port
  myCommand->set_write_log(true); // Switch on logger


  Cconf *myConf = new Cconf("../config_files/MB1-neg-default.txt");

  myCmn = new Ccmn_cmd(myCommand); // New CMN_CMD object

  printf("Connected to %s:%d\n",myCommand->read_server_name(),myCommand->read_server_port());

/* Test execution time */
//  int etime;
//  std::ofstream outfile("tempi.txt");
//  pt::datetime before;
//
//  for (i=0;i<NNN;++i) {
//    before=pt::now();
//    myCmn->MC_read_status(); // check status
//    etime=pt::msecs(pt::now()-before);
//    outfile << etime << std::endl;
//  }
//
//  outfile.close();

/* Test 0xEA (looong loop) */
//  std::ofstream outfile2("test.txt");
//
//  i=0;
//  while (true) {
//    ++i;
//    myCmn->MC_read_status(); // check status
//    if (myCmn->mc_status.code==0) {
//      outfile2 << "MC status error: " << i << std::endl;
//      std::cout << "MC status error: " << i << std::endl;
//    } else {
//      std::cout << "MC status read: " << i << std::endl;
//    }
//
//    pt::psleep(1000);
//  }
//  outfile2.close();


/* Test new/delete */
//  for (i=0;i<100;++i) {
//    printf("Step #%d\n",i);
//
//    myCmn2 = new Ccmn_cmd(myCommand); // New CMN_CMD object
//    delete myCmn2;
//  }
//  mywait();

/* Test DB functionality */
  for (i=0;i<10;++i) {
    myCmn->retrieve_mc_info();
    myCmn->print_mc_info();
//
//    printf("Primary/Secondary port: %d/%d\n",myCmn->get_primary_port(),myCmn->get_secondary_port());
    mywait();
  }
//  mywait();

/* Test get_rob_mask */
//  char brdmsk[7];
//  myCmn->get_rob_mask(brdmsk);
//  printf("McType %d\n",myCmn->mc_status.McType);
//  strprint(brdmsk,7);

/* Test mc_firmware_version */
//  short Hver,Lver;
//  myCmn->mc_firmware_version();
//  myCmn->mc_firmware_version(&Hver,&Lver);
//  myCmn->mc_firmware_version_print();
//  mywait();
//
/* Test send_command() */
//  unsigned char ccc[]={0x55,0x55,0,CCBID+1,0,0,0,0xD,0,0,0x7,0xD0,0xea};
//  myCommand->send_command(ccc+12,1,2000,true); // create header
//  myCommand->send_command(ccc,13,0,true); // don't create header

/* Test configure */
  short errcode=0, confstatus=0, confcrc;
//  errcode = myCmn->config_mc(myConf,CBTI,0,-1,&confstatus,&confcrc);
//  printf("Errcode: %d  -- Confstatus: %d -- CRC: %#hX\n",errcode,confstatus,confcrc);
//  mywait();
//  errcode = myCmn->config_mc(myConf,CTRACO,-1,-1,&confstatus,&confcrc);
//  printf("Errcode: %d  -- Confstatus: %d -- CRC: %#hX\n",errcode,confstatus,confcrc);
//  mywait();
//  errcode = myCmn->config_mc(myConf,CLUT,-1,1,&confstatus,&confcrc);
//  printf("Errcode: %d  -- Confstatus: %d -- CRC: %#hX\n",errcode,confstatus,confcrc);
//  mywait();
//  errcode = myCmn->config_mc(myConf,CTSS,-1,-1,&confstatus,&confcrc);
//  printf("Errcode: %d  -- Confstatus: %d -- CRC: %#hX\n",errcode,confstatus,confcrc);
//  mywait();
//  errcode = myCmn->config_mc(myConf,CTSM,-1,-1,&confstatus,&confcrc);
//  printf("Errcode: %d  -- Confstatus: %d -- CRC: %#hX\n",errcode,confstatus,confcrc);
//  mywait();
//  errcode = myCmn->config_mc(myConf,CTDC,1,1,&confstatus,&confcrc);
//  printf("Errcode: %d  -- Confstatus: %d -- CRC: %#hX\n",errcode,confstatus,confcrc);
//  mywait();
//  errcode = myCmn->config_mc(myConf,CROB,-1,-1,&confstatus,&confcrc);
//  printf("Errcode: %d  -- Confstatus: %d -- CRC: %#hX\n",errcode,confstatus,confcrc);
//  mywait();
//  errcode = myCmn->config_mc(myConf,CTDC,-1,-1,&confstatus,&confcrc);
//  printf("Errcode: %d  -- Confstatus: %d -- CRC: %#hX\n",errcode,confstatus,confcrc);
//  mywait();
//  errcode = myCmn->config_mc(myConf,COTHER,-1,-1,&confstatus,&confcrc);
//  printf("Errcode: %d  -- Confstatus: %d -- CRC: %#hX\n",errcode,confstatus,confcrc);
//  mywait();
//  errcode = myCmn->config_mc(myConf,CALL,-1,-1,&confstatus,&confcrc);
//  printf("Errcode: %d  -- Confstatus: %d -- CRC: %#hX\n",errcode,confstatus,confcrc);
//  mywait();
//
//  myCmn->MC_read_test(); // read test 0x10
//  myCmn->MC_run_test(); // run test 0x12
//  printf("%s",myCmn->mc_test.xmlmsg.c_str());
//  mywait();
//  myCmn->MC_test_print();
//  mywait();
//
  myCmn->MC_temp(); // 0x3C
//  mywait();
  printf("%s",myCmn->mc_temp.xmlmsg.c_str());
  mywait();
//  myCmn->MC_temp_print();
//  mywait();

//  myCmn->MC_read_status(); // check status 0xEA
//  printf("%s",myCmn->mc_status.xmlmsg.c_str());
//  printf("%s",myCmn->boot_status.xmlmsg.c_str());
//  mywait();
//  myCmn->MC_read_status_decode((unsigned char*)"\x13\x00\x00\x00\x01\x00\x12\x00\x01\xFF\xEF\xCF\x3F\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x07\xAE\x50\x00\x00\x00\x00\x02\x0C\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x5A\xBC\x00\x03\x7B\xB8\x00\x02\x4F\x8E\x00\x03\x69\x9A\x00\x02\x78\x52\x00\x01\x63\xA8\x00\x02\x78\x52\x00\x01\x63\x84\x00\x02\x50\x95\x00\x03\x50\xCE\x00\x03\x51\x18\x00\x03\x4E\x00\x00\x02\x4D\xDC\x00\x02\x4D\xDC\x00\x02\x53\x60\x00\x03\x53\x84\x00\x02\x5F\xFA\x00\x01\x5F\xFA\x00\x01\x5F\xFA\x00\x01\x66\xE6\xFF\xFB\x66\xE6\xFF\xFB\x66\xE6\xFF\xFB\x46\x80\x00\x02\x01\x6F\x00\xE7\x01\x69\x00\xE1\x01\x6F\x00\xE7\x41\x00\x00\x06\x00\x04\x00\x00\x5A\xBC\x00\x03\x5B\x06\x00\x03\x7B\x70\x00\x02\x7B\xB8\x00\x02\x53\x3C\x00\x03\x53\x84\x00\x03\x53\x60\x00\x02\x53\xA8\x00\x02");
//  myCmn->MC_read_status_print();
//  mywait();
//  strprint(myCmn->mc_status.rawstatus,myCmn->mc_status.rawstatus_len);
//  mywait();
//  myCmn->MC_read_status_decode(myCmn->mc_status.rawstatus);
//  myCmn->MC_read_status_print();
//  mywait();
//
//  myCmn->PWR_on(2,1); // 0x2F
//  myCmn->PWR_on_print();
//  mywait();
//
//  myCmn->clear_V_monitor(); // 0x9E
//  myCmn->clear_V_monitor_print();
//  mywait();
//
//  myCmn->read_TDC(1,1);
//  myCmn->read_TDC_print();
//  mywait();
//
//  myCmn->status_TDC(1,1);
//  printf("%s",myCmn->status_tdc.xmlmsg.c_str());
//  mywait();
//  myCmn->status_TDC_print();
//  mywait();
//
//  myCmn->GetConfigCRC(); // 0xA3
//  myCmn->GetConfigCRC_print();
//  mywait();
//
//  myCmn->chamber_map(0x02A3); // 0x4B
//  myCmn->chamber_map_print();
//  mywait();
}
