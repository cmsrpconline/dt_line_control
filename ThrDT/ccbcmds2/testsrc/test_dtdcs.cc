#include <ccbcmds/Cdtdcs.h>
#include <ccbcmds/apfunctions.h>

#include <ccbcmds/Clog.h>
#include <ccbcmds/Ccommand.h>
#include <ccbcmds/Cccb.h>

#include <unistd.h>

//#define PARTNAME "W-2"
//#define PARTNAME "W-2S10"
//#define PARTNAME "W0MB3"
#define PARTNAME "W-2S10MB1"
#define CONFNAME "allcms"
//#define CONFNAME "empty"

int main() {
  int i, j, nccb, nexcluded;
//  int *ccblist = new int[NMAXCCB];
  dtdcs mydtdcs;

//  mydtdcs.verbose_mode(true);
//  mydtdcs.partSetMcMonitorInterval(31,25); // Set MC monitoring&logging time
//  mydtdcs.partSetPrMonitorInterval(32,26); // Set PADC monitoring/logging time

// Some tests on new/delete
//  Clog myLog("pippo");
//  Ccommand myCmd(0,"127.0.0.1",18889,&myLog);
//  Cccb *myCcb; // "ccb" object
//
//  for (i=0;i<5;++i) { // Ok
//    printf("Step #%d\n",i);
//    myCcb = new Cccb(&myCmd);
//    delete myCcb;
//  }
//  mywait();


//  for (i=0;i<3;++i) { // DB connections grow!!!
//    printf("Step #%d\n",i);
//    mydtdcs.partInitialize(PARTNAME);
//    mywait();
//    mydtdcs.partStopMonitor();
//    mydtdcs.partReset();
//    mywait();
//  }
//  mywait();

//  i=mydtdcs.dbTestConnetion();
//  printf("Result of connection test: %d.\n",i);
//  mywait();

// List available partitions
//  printf("Listing partitions...\n\n.");
//  mydtdcs.partShowAvailable();
//  mywait();


// Investigate mydtdcs.nccb problem
//  mydtdcs.partInitialize(PARTNAME);
//  i=0;
//  while (++i<100) {
//    nccb = mydtdcs.partGetNCcb();
//    printf("Step %d. nccb=%d\n",i,nccb);
//
//    mywait();
//  }
//
//  return 0;


// Initialize and shows a partition
  mydtdcs.disableAutoRecover();
  mydtdcs.partInitialize(PARTNAME);
//  mydtdcs.enableAutoRecover();

//  mydtdcs.partSetMcMonitorInterval(38,45); // Set MC monitoring&logging time
//  mydtdcs.partSetPrMonitorInterval(55,67); // Set PADC monitoring/logging time

  nccb = mydtdcs.partGetNCcb();
  nexcluded = mydtdcs.partGetNExcludedCcb();
//  ccblist = mydtdcs.partGetCcbList();

  printf("Initializing partition '%s' nccb=%d...\n\n",mydtdcs.partGetPartName(),nccb);
//  sleep(5);
  mywait();

//  printf("ccblist:");
//  for (i=0;i<nccb;++i)
//    printf("  %d",ccblist[i]);
//  putchar('\n');
//
//  printf("nccb: %d - excluded: %d.\n",nccb,nexcluded);

  mydtdcs.partShowActual();
  mywait();


//  mydtdcs.partRefreshStatus();
//  mywait();

  mydtdcs.partShowActual();
  mywait();

// Test connections
//  i=mydtdcs.ccbserverTestConnection(1);
//  i=mydtdcs.ccbserverTestConnection(265);
//  i=mydtdcs.ccbserverTestConnection(4);
//  i=mydtdcs.ccbTestConnection(1);
//  i=mydtdcs.ccbTestConnection(265);
//  i=mydtdcs.ccbTestConnection(4);
//  printf("Result of connection test: %d.\n",i);
//  mywait();

//  printf("ccbid (-999,1,2):%d\n",mydtdcs.ccbGetCcbId(-999,1,3));
//  printf("ccbid (0,1,2)   :%d\n",mydtdcs.ccbGetCcbId(0,1,2));
//  mywait();

//  int wheel,sector,station;
//  mydtdcs.ccbGetWhSecSt(1,&wheel,&sector,&station);
//  printf("Wh,Sec,St: %d %d %d\n",wheel,sector,station);
//  mydtdcs.ccbGetWhSecSt(99,&wheel,&sector,&station);
//  printf("Wh,Sec,St: %d %d %d\n",wheel,sector,station);
//  mywait();

  printf("Listing configurations...\n\n");
  mydtdcs.confShowAvailable();
  mywait();

// Configure a partition
  mydtdcs.confStart(CONFNAME);
  printf("Configuring partition with '%s'\n",mydtdcs.confGetConfName());
//  for (i=0;i<nccb;++i)
//    printf("CCBID: %d configs.confname: %s\n",ccblist[i],mydtdcs.ccbGetConfName(ccblist[i]));
  mywait();


// Wait until config is over
  bool cdone=false;
  Econfstatus cstatus;
  while (!cdone) {
    cstatus=mydtdcs.confGetStatus();

    if (cstatus==c_configured) {
      cdone=true;
      printf("Time: %s. Configuration done.\n",get_time());
    } else if (cstatus==c_undef) {
      cdone=true;
      printf("Time: %s. Configuration status undefined.\n",get_time());
    } else if (cstatus==c_configuring) {
      cdone=false;
      printf("Time: %s. Configuration ongoing.\n",get_time());
    } else if (cstatus==c_error) {
      cdone=true;
      printf("Time: %s. Configuration error.\n",get_time());
    }

    if (!cdone) sleep(10);
  }

//  printf("configs.confname: %s.\n",mydtdcs.ccbGetConfName(265));
//  printf("ConfStatus: %d.\n",mydtdcs.ccbGetConfStatus(265));

//  mydtdcs.ccbReconfigure(1);
//  mywait();
//  mydtdcs.confStop();
//  mywait();
//

// Get single CCB Status
//  float *ExtTemp=new float[7];
//  for (i=0;i<nccb;++i) {
//    int ccbid = ccblist[i];
//    time_t time;
//
//    printf("PhysSt: %d\n",mydtdcs.ccbGetPhysStatus(ccbid));
//    printf("LogicSt: %d\n",mydtdcs.ccbGetLogicStatus(ccbid));
//    printf("RobSt: %d\n",mydtdcs.ccbGetRobStatus(ccbid));
//    printf("Status Msg: %s\n",mydtdcs.ccbGetStatusMsg(ccbid));
//    printf("ROB Status Msg: %s\n",mydtdcs.ccbGetRobStatusMsg(ccbid));
//    time = mydtdcs.ccbGetStatusTime(ccbid);
//    printf("Last update: %s",ctime(&time));
//
//    printf("MaxTemp: %f\n",mydtdcs.ccbGetMaxTemp(ccbid));
//    printf("MaxRobTemp: %f\n",mydtdcs.ccbGetRobMaxTemp(ccbid));
//    printf("MaxTrbTemp: %f\n",mydtdcs.ccbGetTrbMaxTemp(ccbid));
//    printf("MaxExtTemp: %f\n",mydtdcs.ccbGetExtMaxTemp(ccbid));
//    printf("MaxFeTemp: %f\n",mydtdcs.ccbGetFeMaxTemp(ccbid));
//    ExtTemp=mydtdcs.ccbGetExtTemp(ccbid);
//    printf("ExtTemp:");
//    for (j=0;j<6;++j)
//      printf(" %4.1f",ExtTemp[j]);
//    printf("\n");
//
//
//    printf("Pressure HV: %f\n",mydtdcs.ccbGetHvPressure(ccbid));
//    printf("Pressure FE: %f\n",mydtdcs.ccbGetFePressure(ccbid));
//    time = mydtdcs.ccbGetPressureTime(ccbid);
//    printf("Last update: %s",ctime(&time));
//    printf("PadcSt: %d\n",mydtdcs.ccbGetPadcStatus(ccbid));
//    printf("PADC Status Msg: %s\n",mydtdcs.ccbGetPadcStatusMsg(ccbid));
//    printf("Configuration Status Msg: %s\n",mydtdcs.ccbGetConfStatusMsg(ccbid));
//  }
//  mydtdcs.ccbGetStatusTime(-1);
//  mywait();

// Send commands to a partition
//  printf("Send commands to partition.\n");
//  mydtdcs.partSendCommand(cmd_restoreconf);
//  mydtdcs.partSendCommand(cmd_saveconf);
//  mydtdcs.partSendCommand(cmd_autosetlink);
//  mydtdcs.partSendCommand(cmd_restartccb);
//  mydtdcs.partSendCommand(cmd_resetrob);
//  mywait();

// Send command to a ccb
//  printf("Send commands to partition.\n");
//  printf("%s\n",mydtdcs.ccbSendCommand(1,cmd_restoreconf).c_str());
//  printf("%s\n",mydtdcs.ccbSendCommand(10,cmd_saveconf).c_str());
//  printf("%s\n",mydtdcs.ccbSendCommand(2,cmd_autosetlink).c_str());
//  printf("%s\n",mydtdcs.ccbSendCommand(1,cmd_restartccb).c_str());
//  printf("%s\n",mydtdcs.ccbSendCommand(1,cmd_resetrob).c_str());
//  mywait();
//
//  printf("Config status? %d\n",mydtdcs.confGetStatus());
//  printf("Conf CRC: %s\n",mydtdcs.confCheckCrc()?"ok":"wrong");
//  mywait();
//
//// Monitor a partition
//  printf("Monitoring partition\n");
//
//  mydtdcs.partStopMonitor();
//  mywait();
//
//  mydtdcs.partStartMonitor();
//  mywait();
//

// Set/Unset partition for run
//  mydtdcs.partSetRun(1);
//  mydtdcs.partUnsetRun();
//  mywait();


// Status
//
  mydtdcs.partShowStatus();
  mywait();
//
//  std::string strout;
//  mydtdcs.partShowStatus(&strout);
//  printf("*** In Test_DTDCS ***\n%s\n",strout.c_str());
//  mywait();
//

//  printf("Partition Status\n");
//  Emcstatus *dtstatus;
//  Econfstatus *dtconfst;
//  dtstatus=mydtdcs.partGetPhysStatus();
//  dtstatus=mydtdcs.partGetLogicStatus();
//  dtstatus=mydtdcs.partGetRobStatus();
//  dtstatus=mydtdcs.partGetPadcStatus();
//  dtconfst=mydtdcs.confGetDetailedStatus();
//  for (i=0;i<nccb;++i) {
//    printf("%d ",dtconfst[i]);
//    printf("%d ",dtstatus[i]);
//  }
//  putchar('\n');
//
//  mywait();

//  printf("Temperatures:");
//  float *temperature = new float[NMAXCCB];
//  temperature=mydtdcs.partGetMaxTemp();
//  temperature=mydtdcs.partGetRobMaxTemp();
//  temperature=mydtdcs.partGetTrbMaxTemp();
//  temperature=mydtdcs.partGetExtMaxTemp();
//  temperature=mydtdcs.partGetFeMaxTemp();
//  for (i=0;i<nccb;++i)
//    printf("%f ",temperature[i]);
//  putchar('\n');
//
//  printf("Pressures:");
//  float *pressure = new float[NMAXCCB];
//  pressure=mydtdcs.partGetHvPressure();
//  pressure=mydtdcs.partGetFePressure();
//
//  for (i=0;i<nccb;++i)
//    printf("%f ",pressure[i]);
//  putchar('\n');
//
//  mywait();


// Reset the current partition
//  mydtdcs.partReset();
//  mydtdcs.partShowActual();
//  mywait();
//
//  printf("Config status? %d\n",mydtdcs.confGetStatus());
//  mywait();

  return 0;
}
