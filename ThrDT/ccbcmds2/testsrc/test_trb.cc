#include <apfunctions.h>
#include <Clog.h>
#include <Ccommand.h>
#include <Cccb.h>
#include <Ctrb.h>

#include <iostream>

#define SERVER_NAME "localhost"
#define SERVER_PORT 18889
#define CCB_ID 0x01

int main() {
  const char brd=0;

  Clog *myLog;
  Ccommand *myCmd;
  Cccb *myCcb;
  Ctrb *myTrb;


// Initialise objects
//  myLog = new Clog("log.txt"); // Log to file
  myLog = new Clog(); // Log to DB
  myCmd = new Ccommand(CCB_ID,SERVER_NAME,SERVER_PORT,myLog);
  myCcb = new Cccb(myCmd);
  myTrb = new Ctrb(myCmd); // New TRB object

//  myTrb->verbose_mode(true);
  myCmd->set_write_log(true); // Switch on logger

//  myTrb->read_TDC(0,0);
//  myTrb->read_TDC_print();
//  mywait();
//
//  myTrb->status_TDC(0,0);
//  myTrb->status_TDC_print();
//  mywait();
//
  myTrb->read_ROB_error();
  myTrb->read_ROB_error_print();
  mywait();
//
//  myTrb->MC_read_status(); // check status
//  myTrb->MC_read_status_print();
//  mywait();
//
//  myTrb->TRB_PI_test(brd); // 0x5F
//  myTrb->TRB_PI_test_print();
//  mywait();
//  myTrb->TRB_PI_test(99); // 0x5F
//  myTrb->TRB_PI_test_print();
//  mywait();
//
//  myTrb->TRB_PI_test(brd,1); // 0x5F
//  myTrb->TRB_PI_test_print();
//  mywait();
//  myTrb->TRB_PI_test(99,1); // 0x5F
//  myTrb->TRB_PI_test_print();
//  mywait();
//
//  myTrb->disable_temp_test(1); // 0x83
//  myTrb->disable_temp_test_print();
//  mywait();
//
//  myTrb->disable_TRB_CK((char)1); // 0x93
//  myTrb->disable_TRB_CK_print();
//  mywait();
//
//  myTrb->TRB_PI_test_0xA0(brd,0,1,1,0,1); // 0xA0
//  myTrb->TRB_PI_test_0xA0_print();
//  mywait();
//
//  myTrb->clear_TRB_SEU_counter(); // 0xA1
//  myTrb->clear_TRB_SEU_counter_print();
//  mywait();
//
  myTrb->test_IR(); // 0xA2
  myTrb->test_IR_print();
//  mywait();
}
