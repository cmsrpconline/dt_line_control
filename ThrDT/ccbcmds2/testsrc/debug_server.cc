// ptypes headers
#include <pinet.h>
#include <ptime.h>
USING_PTYPES

#include <iostream>

int main() {
  int ccb_id=0x00;
  const char command_line[13] = {0x55, 0x55, 0, 0x48, 0, 0, 0, 13, 0, 0, 0x13, 0x88, 0xEA};
  string server_name="127.0.0.1", cmd_line(command_line,15);
  ipstream client(server_name,18889);

  client.open();

  client.putline(cmd_line);
  client.flush();

  return 0;
}
