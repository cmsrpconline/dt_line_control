#include <apfunctions.h>
#include <Clog.h>
#include <Ccommand.h>
#include <Cfe.h>
#include <Cconf.h>

#include <ccbdb/Ccmsdtdb.h>

#include <iostream>

#define CCB_ID 1
#define SERVER "127.0.0.1"
#define PORT 18889


int main() {
  const char sl=1;
  int i;

  unsigned char mask[3]={0xA,0xB,0x1};
  char cDly=0, fDly=0, endSqDly=63, FEDisDly=5, fineofs[2]={0,0};
  char TpOffset[8]={145,145,145,145,145,55,55,55};
  float bias_gain[3]={1.001221,1.000610,1.000366},
    thr_gain[3]={1.006409,1.009705,1.006836}, width_gain=1.000793,
    bias_offset[3]={-0.000616,0.008669,0.012118},
    thr_offset[3]={-0.001883,-0.000465,-0.000172}, width_offset=0.017392;

  cmsdtdb dtdbobj; // DB connector

// Initialise objects
  Clog *myLog = new Clog("log.txt"); // Log to file
//  Clog *myLog = new Clog(); // Log to DB
  Ccommand *myCmd = new Ccommand(CCB_ID,SERVER,PORT,myLog);

  Cfe *myFe = new Cfe(myCmd); // New FE object
  Cconf *myConf = new Cconf("../config_files/MB1-neg-default.txt"); // config

  myCmd->set_write_log(true); // Switch on logger
  myCmd->set_db_connector(&dtdbobj); // Set DB connector

//  myFe->read_TDC(0,0);
//  myFe->read_TDC_print();
//
//  myFe->status_TDC(0,0);
//  myFe->status_TDC_print();
//
//  myFe->read_ROB_error();
//  myFe->read_ROB_error_print();
//  mywait();
//  myFe->MC_read_status();
//  myFe->MC_read_status_print();
//
//  myFe->config_FE(myConf);
//  mywait();
//
//  myFe->read_FE_config("pippo.txt");
//  mywait();
//
//  myFe->set_FE_thr((short)0,1.4,0.025); // 0x35
//  myFe->set_FE_thr_print();
//  mywait();
//  myFe->set_FE_thr((short)99,1.4,0.025); // 0x35
//  myFe->set_FE_thr_print();
//  mywait();
//
//  myFe->set_FE_width(3.1); // 0x47
//  myFe->set_FE_width_print();
//  mywait();
//
//  myFe->read_FE_temp(sl,(char)-1); // 0x36, all channels
//  myFe->read_FE_temp(sl,(char)0); // 0x36, only Tmax-Tmed
//  myFe->read_FE_temp_print();
//  mywait();
//
//  myFe->set_FE_mask((short)1,(short)3,mask); // 0x3A
//  myFe->set_FE_mask_print();
//  mywait();
//
//  myFe->set_FE_mask_ch((short)0,(short)3,(short)1); // 0x3B
//  myFe->set_FE_mask_print();
//  mywait();
//
//  myFe->read_FE_mask(); // 0x38
//  myFe->read_FE_mask_print();
//  mywait();
//
  for (i=0;i<10;++i) {
    myFe->FE_mask_to_db(); // Log FE mask in the DB.
    mywait();
  }
//
//  myFe->set_FE_Tmask((short)1,(short)0xF,(short)0x1); // 0x62
//  myFe->set_FE_Tmask((short)1,(short)0xF,(short)0x0); // 0x62
//  myFe->set_FE_Tmask_print();
//  mywait();
//
//  myFe->read_FE_Tmask(); // 0x71
//  myFe->read_FE_Tmask_print();
//  mywait();
//
//  myFe->FE_test(); // 0x4B
//  myFe->FE_test_print();
//  mywait();
//
//  myFe->FE_test(1); // 0x4B
//  myFe->FE_test_print();
//  mywait();
//
//  myFe->set_TP(cDly,fDly,cDly,fDly,endSqDly,0.5,0.5,FEDisDly,fineofs); // 0x4A
//  myFe->set_TP_print();
//  mywait();
//
//  myFe->set_rel_TP(0.0,0.53,0.53,endSqDly,FEDisDly); // 0x78
//  myFe->set_rel_TP_print();
//  mywait();
//
//  myFe->read_TP (); // 0x72
//  myFe->read_TP_print();
//  mywait();
//
//  myFe->test_TP_finedelay(); // 0x82
//  myFe->test_TP_finedelay_print();
//  mywait();
//
//  myFe->set_TP_offset(TpOffset,fineofs,20,100); // 0x8B
//  myFe->set_TP_offset_print();
//  mywait();
//
//  myFe->read_TP_offset(); // 0x8C
//  myFe->read_TP_offset_print();
//  mywait();
//
//  myFe->test_DAC(); // 0x68
//  myFe->test_DAC_print();
//  mywait();
//
//  myFe->set_calib_DAC(bias_gain,thr_gain,width_gain,bias_offset,thr_offset,width_offset); // 0x6B
//  myFe->set_calib_DAC_print();
//  mywait();
//
//  myFe->read_calib_DAC(); // 0x6E
//  myFe->read_calib_DAC_print();
//  mywait();
//
//  myFe->set_SL_mask(1,0); // 0x9A
//  myFe->set_SL_mask_print();
//  mywait();
//
//  myFe->set_SL_Tmask(1,0); // 0x9B
//  myFe->set_SL_Tmask_print();
//  mywait();
}
