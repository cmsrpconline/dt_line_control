#include <apfunctions.h>
#include <Clog.h>
#include <Ccommand.h>
#include <Ccmn_cmd.h>
#include <Cccb.h>

#include <iostream>

#include <ptypes/pasync.h> // for plseep()
#include <ptypes/ptypes.h>
#include <ptypes/ptime.h>

#include <pthread.h> // multithreading

//#define CCB_ID 0
#define CCB_ID 1
#define SERVER "127.0.0.1"
#define PORT 18889

#define NCCB 50
#define NCYC 20

int main() {
  int i;
  Clog *myLog;
  Ccommand *myCommand;
  Ccmn_cmd *myCmn;
  Cconf *myConf;

// Threads...
  void *thr_monitor1(void *), *thr_monitor2(void *), *thr_monitor3(void *);
  pthread_t thr_id[NCCB];

// Initialise objects
  myLog = new Clog("log.txt"); // Log to file
//  myLog = new Clog(); // Log to DB
  myCommand = new Ccommand(CCB_ID,SERVER,PORT,myLog);
//  myConf = new Cconf("../config_files/MB3-HHandHL.txt",UFILE); // config
//  myConf = new Cconf("andrea",UDATABASE); // config
  myCmn = new Ccmn_cmd(myCommand); // New CMN_CMD object

/* Test 0xEA (looong loop) */
//  std::ofstream outfile1("test.txt");
//
//  i=0;
//  while (true) {
//    ++i;
//    myCmn->MC_read_status(); // check status
//    if (myCmn->mc_status.code==0 || myCommand->data_read_len!=197) {
//      outfile1 << "MC status error: " << i << std::endl;
//      std::cout << "MC status error: " << i << std::endl;
//    } else {
//      std::cout << "MC status read: " << i << std::endl;
//    }
//
//    outfile1.flush();
//    pt::psleep(1000);
//  }
//  outfile1.close();


/* Concurrent access to the same ccb (same Ccmn_cmd) */
  myCommand->set_write_log(true); // Switch on logger

  for (i=0;i<NCCB;++i) {
    pthread_create(&thr_id[i],NULL,thr_monitor1,(void *)myCmn);
    pt::psleep(100);
  }

  for (i=0;i<NCCB;++i)
    pthread_join(thr_id[i],NULL);

/* Concurrent access to the same ccb (different Ccmn_cmd, same Ccommand) */
//  myCommand->set_write_log(true); // Switch on logger
//
//  for (i=0;i<NCCB;++i) {
//    pthread_create(&thr_id[i],NULL,thr_monitor2,(void *)myCommand);
//    pt::psleep(200);
//  }
//
//  for (i=0;i<NCCB;++i)
//    pthread_join(thr_id[i],NULL);


/* Concurrent access to the same ccb (different Ccommand) */
// OK. Sep20 2006
//
//  for (i=0;i<NCCB;++i) {
//    pthread_create(&thr_id[i],NULL,thr_monitor3,(void *)myLog);
//    pt::psleep(150);
//  }
//
//  for (i=0;i<NCCB;++i)
//    pthread_join(thr_id[i],NULL);


}


void *thr_monitor1(void *tmp) {
  int i;
  Ccmn_cmd *thr_cmn = (Ccmn_cmd*)tmp;

  i=0;
  while (i<NCYC) {
    ++i;
    thr_cmn->MC_read_status(); // check status
    if (thr_cmn->mc_status.code!=0x13) {
      std::cout << "MC status error. Code " << (int)(thr_cmn->mc_status.code) << std::endl;
    } else {
      std::cout << "MC status read: " << i << std::endl;
    }
    pt::psleep(200);
  }
}

void *thr_monitor2(void *tmp) {
  int i;
  Ccommand *thr_command = (Ccommand*)tmp;

  Ccmn_cmd thr_cmn(thr_command); // New CMN_CMD object

  i=0;
  while (i<NCYC) {
    ++i;
    thr_cmn.MC_read_status(); // check status
    if (thr_cmn.mc_status.code!=0x13 || thr_command->data_read_len!=197) {
      std::cout << "MC status error. Code " << (int)(thr_cmn.mc_status.code) << " " << thr_command->data_read_len << std::endl;
      strprint(thr_command->data_read,thr_command->data_read_len);
    } else {
      std::cout << "MC status read: " << i << std::endl;
    }
    pt::psleep(200);
  }
}

void *thr_monitor3(void *tmp) {
  int i;
  Clog *thr_log = (Clog*)tmp;

  Ccommand *thr_command = new Ccommand(CCB_ID,SERVER,PORT,thr_log);
  Ccmn_cmd thr_cmn(thr_command); // New CMN_CMD object

  thr_command->set_write_log(true); // Switch on logger

//  thr_cmn.MC_read_status(); // check status
//  thr_cmn.MC_read_status_print();

  i=0;
  while (i<NCYC) {
    ++i;
    thr_cmn.MC_read_status(); // check status
    if (thr_cmn.mc_status.code!=0x13 || thr_command->data_read_len!=197) {
      std::cout << "MC status error. Code " << (int)(thr_cmn.mc_status.code) << " " << thr_command->data_read_len << std::endl;
    } else {
      std::cout << "MC status read: " << i << std::endl;
    }

    pt::psleep(200);
  }
}
