#include <apfunctions.h>
#include <Clog.h>
#include <Ccommand.h>
#include <Cccb.h>
#include <Cref.h>

#include <ccbdb/Ccmsdtdb.h>

#include <iostream>

#define SERVER_NAME "127.0.0.1"
//#define SERVER_NAME "mc1"
//#define SERVER_NAME "icab149"
#define SERVER_PORT 18889
#define CCBID 384

int main(int argc, char **argv) {

  bool mc_program;
//  bool flag_ok, sb_ok, trb_ok, rob_ok;
  char message[2048];
  Emcstatus mc_phys_st, mc_logic_st;
  Emcstatus flag_tst, sb_tst, trb_tst, rob_tst;

  char nm1[]="../ref_files/boot.xml",
    nm2[]="../ref_files/status-3.xml",
    nm3[]="../ref_files/test-1.xml",
    nm4[]="../ref_files/robstatus.xml",
    nm5[]="../ref_files/padcstatus-commissioning.xml";

  cmsdtdb dtdbobj; // DB connector

  Clog *myLog = new Clog("log.txt");
//  myLog = new Clog();
  Ccommand *myCommand = new Ccommand(CCBID,SERVER_NAME,SERVER_PORT,myLog);
  Cccb *myCcb = new Cccb(myCommand);
  Cref *myRef = new Cref(nm1,nm2,nm3,nm4,nm5); // read reference from file
//  Cref *myRef = new Cref(CCBID,&dtdbobj); // read reference from DB

//  myCcb->MC_read_test(false);
//  myCcb->MC_test_print();

//  std::string refstr;
//  read_file("../ref_files/boot.xml",&refstr);

  Cref *testRef[300];
//  for (int i=0;i<300;++i) {
//    printf("Step %d\n",i);
//    testRef[i] = new Cref(CCBID,&dtdbobj); // read reference from DB
//    myCcb->MC_check_status(testRef[i],true,&mc_program, &mc_phys_st, &mc_logic_st,message); // Check MC status
//    myCcb->MC_check_test(testRef[i],false, &flag_tst, &sb_tst, &trb_tst, &rob_tst,message); // MC test
//  }

//  myRef->print_boot_ref();
//  mywait();

  myRef->print_status_ref();
  mywait();

//  myRef->print_test_ref();
//  mywait();

//  myRef->print_rob_ref();
//  mywait();

//  myRef->print_padc_ref();
//  mywait();

// Check MC status
  myCcb->MC_check_status(myRef,true,&mc_program, &mc_phys_st, &mc_logic_st,message); // disable QPLL check
//  myCcb->MC_check_status(myRef,false,&mc_program, &mc_phys_st, &mc_logic_st,message);
  myCcb->MC_check_status_print();
  printf("Switch off MC? %s\n",myCcb->mc_status_check.switch_off_mc?"yes":"no");
  printf("Switch off FE? %s\n",myCcb->mc_status_check.switch_off_fe?"yes":"no");
  mywait();

// MC test
//  myCcb->MC_check_test(myRef,false, &flag_tst, &sb_tst, &trb_tst, &rob_tst,message);
//  myCcb->MC_check_test_print();
//  mywait();

  return 0;
}
