#include <apfunctions.h>
#include <Clog.h>
#include <Ccommand.h>
#include <Cccb.h>

#include <ccbdb/Ccmsdtdb.h>

#include <iostream>

#define CCB_ID   1
#define PORT_ID   1
#define SERVER "127.0.0.1"
//#define SERVER "cmsdaquser0"
#define PORT 18889

int main() {

  char nm1[]="../ref_files/boot.xml",
    nm2[]="../ref_files/status-1.xml",
    nm3[]="../ref_files/test-1.xml",
    nm4[]="../ref_files/rosstatus.xml",
    nm5[]="../ref_files/padcstatus.xml";
  int i;

// Initialise objects
  cmsdtdb dtdbobj; // DB connector

  Clog *myLog = new Clog("log.txt"); // Log to file
//  Clog *myLog = new Clog(); // Log to DB
//  Ccommand *myCmd = new Ccommand();
//  Ccommand *myCmd = new Ccommand(ccb_id,myLog);
//  Ccommand *myCmd = new Ccommand(CCB_ID,SERVER,PORT,myLog);
//  Ccommand *myCmd = new Ccommand(CCB_ID,PORT_ID,SERVER,PORT,myLog);
  Ccommand *myCmd = new Ccommand(CCB_ID,PORT_ID,CCB_ID,SERVER,PORT,myLog);
  Cconf *myConf = new Cconf("../config_files/MB1-neg-default.txt"); // config
  Cccb *myCcb = new Cccb(myCmd); // New CCB object
  Cref *myRef = new Cref(nm1,nm2,nm3,nm4,nm5); // read reference from file


//  myCmd->set_server("icab149",18889); // set the server and port
//  myCmd->set_server("lxparenti",18888); // set the server and port

  myCmd->set_write_log(true); // Switch on logger
  myCmd->set_db_connector(&dtdbobj); // Set DB connector

  printf("Connected %s:%d\n",myCmd->read_server_name(),myCmd->read_server_port());

//  myCmd->connect();
//  pt::apsleep(5000);
//  myCmd->disconnect();

//  for (i=0;i<10;++i) {
//    printf("Step #%d\n",i);
//
//    myCcb->MC_status_to_db(myRef);
//    strprint(myCmd->data_read,myCmd->data_read_len);
//    mywait();
//
//    myCcb->MC_status_from_db();
//    myCcb->MC_read_status();
//    myCcb->MC_read_status_print();
//    mywait();
//  }
//
//    myCcb->MC_status_to_db(myRef);
//    myCcb->MC_read_status_reset();
//    mywait();
//    myCcb->MC_status_from_db();
//    myCcb->MC_read_status_print();
//    mywait();
//    myCcb->MC_temp_print();
//    mywait();
//
//  myCcb->read_TDC(0,0);
//  myCcb->read_TDC_print();
//  mywait();
//
//  myCcb->status_TDC(0,0);
//  myCcb->status_TDC_print();
//  mywait();
//
//  myCcb->read_ROB_error();
//  myCcb->read_ROB_error_print();
//  mywait();
//
//  myCcb->MC_read_test(); // read test 0x10
//  myCcb->MC_run_test(); // run test 0x12
//  myCcb->MC_test_print();
//  mywait();
//
//  myCcb->MC_read_status(); // check status 0xEA
//  myCcb->MC_read_status_print();
//  mywait();
//
//  myCcb->MC_temp(); // 0x3C
//  myCcb->MC_temp_print();
//  mywait();
//
//  myCcb->run_in_progress((char)0); // 0x46
//  myCcb->run_in_progress_print();
//  mywait();
//
//  myCcb->read_comm_err(); // 0xF0
//  myCcb->read_comm_err_print();
//  mywait();
//
//  myCcb->read_LINK(); // 0x76
//  myCcb->read_LINK_print();
//  mywait();
//
//  myCcb->disable_LINK_monitor((char)1); // 0x77
//  myCcb->disable_LINK_monitor_print();
//  mywait();
//
//  myCcb->disable_LINK_monitor((char)0); // 0x77
//  myCcb->disable_LINK_monitor_print();
//  mywait();
//
//  myCcb->script((char)8,(short)0x200,"sb"); // 0xEE
//  myCcb->script((char)8,(short)0x200,"mctest");
//  myCcb->script_print();
//  mywait();
//
//  myCcb->auto_trigger(1,10); // 0x70
//  myCcb->auto_trigger_print();
//  mywait();
//
//  myCcb->restart_CCB(); // 0xF8
//  myCcb->restart_CCB_print();
//  mywait();
//
//  myCcb->read_CPU_addr(0x11,0x2233,240); // 0xF5
//  myCcb->read_CPU_addr_print();
//  mywait();
//
//  myCcb->read_crate_sensor_id(); // 0x8F
//  myCcb->read_crate_sensor_id_print();
//  mywait();
//
//  myCcb->CCBrdy_pulse(); // 0x91
//  myCcb->CCBrdy_pulse_print();
//  mywait();
//
//  myCcb->pirw((char)1); // 0x92
//  myCcb->pirw_print();
//  mywait();
//
//  myCcb->piprog((char)1); // 0xAF
//  myCcb->piprog_print();
//  mywait();
//
//  myCcb->disable_SB_CK((char)1); // 0x94
//  myCcb->disable_SB_CK_print();
//  mywait();
//
//  myCcb->disable_OSC_CK((char)1); // 0x95
//  myCcb->disable_OSC_CK_print();
//  mywait();
//
//  myCcb->restore_MC_conf(); // 0x96
//  myCcb->restore_MC_conf_print();
//  mywait();
//
//  unsigned char ll=5, dd[]={0x1,0x44,0xac,0,0x4};
//  myCcb->write_custom_data(dd,ll); // 0x97
//  myCcb->write_custom_data_print();
//  mywait();
//
//  myCcb->read_custom_data(); // 0x98
//  myCcb->read_custom_data_print();
//  mywait();
//
//  myCcb->auto_set_LINK_boot(1,2); // 0xE7
//  myCcb->auto_set_LINK_boot_print();
//  mywait();
//
//  myCcb->auto_set_LINK(); // 0x74
//  myCcb->auto_set_LINK_print();
//  mywait();
//
//  char data[2]={1,2};
//  myCcb->LINK_test_boot(data,2); // 0xE4
//  myCcb->LINK_test_boot_print();
//  mywait();
//
//  myCcb->reset_SEU_boot(); // 0xE6
//  myCcb->reset_SEU_boot_print();
//  mywait();
//
//  myCcb->link_DAC_boot(0x110); // 0xE5
//  myCcb->link_DAC_boot_print();
//  mywait();
//
//  myCcb->restart_CCB(); // 0xF8
//  myCcb->restart_CCB_print();
//  mywait();
//
//  myCcb->hd_restart_CCB(); // 0xF7
//  myCcb->hd_restart_CCB_print();
//  mywait();
//
//  myCcb->exit_MC(); // 0xA5
//  myCcb->exit_MC_print();
//  mywait();
//
//
//  myCcb->save_MC_conf(); // 0x40
//  myCcb->save_MC_conf_print();
//  mywait();
//
//  myCcb->load_MC_conf(); // 0x61
//  myCcb->load_MC_conf_print();
//  mywait();
//
//  myCcb->find_sensors(); // 0xA6
//  myCcb->find_sensors_print();
//  mywait();
//
//  myCcb->protect_FLASH(1); // 0xED
//  myCcb->protect_FLASH_print();
//  mywait();
//
//  myCcb->erase_FLASH(); // 0xEB
//  myCcb->erase_FLASH_print();
//  mywait();
//
  unsigned char data[128];
  data[0]=0x11;
  data[127]=0xFF;
  myCcb->write_FLASH(0x11,0x2233,data); // 0xF3
  myCcb->write_FLASH_print();
  mywait();
}
