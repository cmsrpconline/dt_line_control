#include <apfunctions.h>
#include <Clog.h>
#include <Ccommand.h>

#include <iostream>


#define CCB_ID   555 
#define CCB_PORT 1
#define SERVER "127.0.0.1"
//#define SERVER "lx"
#define PORT 18889

int main() {
  int i, errcode;
  Clog *myLog;
  Ccommand *myCmd;

// Iinitialise objects
  myLog = new Clog("log.txt"); // Log to file
//  myLog = new Clog(); // Log to DB

  myCmd = new Ccommand(CCB_ID,CCB_PORT,1000+CCB_ID,SERVER,PORT,myLog);
  printf("Server= %s:%d\n",myCmd->read_server_name(),myCmd->read_server_port());

//  errcode=myCmd->send_command((unsigned char*)"\xEA",1,500,3);
//  printf("(Connected) Error: %s.\n",myCmd->read_error_message(errcode));
//
//  myCmd->disconnect();
//  errcode=myCmd->send_command((unsigned char*)"\xEA",1,500,3);
//  printf("(Disconnected) Error: %s.\n",myCmd->read_error_message(errcode));
//
//  myCmd->connect();
//  errcode=myCmd->send_command((unsigned char*)"\xEA",1,500,3);
//  printf("(Connected) Error: %s.\n",myCmd->read_error_message(errcode));
//
//  myCmd->disconnect();
//  errcode=myCmd->send_command((unsigned char*)"\xEA",1,500,3);
//  printf("(Disconnected) Error: %s.\n",myCmd->read_error_message(errcode));
//
//  myCmd->set_server("lxparenti",18888);
//  errcode=myCmd->send_command((unsigned char*)"\xEA",1,500,3);
//  printf("(Connected?) Error: %s.\n",myCmd->read_error_message(errcode));
//
//  myCmd->set_server("lxxx",18888);
//  errcode=myCmd->send_command((unsigned char*)"\xEA",1,500,3);
//  printf("(Connected?) Error: %s.\n",myCmd->read_error_message(errcode));

  errcode=myCmd->use_primary_port();
  printf("%s\n",errcode==0?"Switched to primary port":"Already using primary port");
  errcode=myCmd->send_command((unsigned char*)"\xEA",1,500,3);
//
  errcode=myCmd->use_secondary_port();
  printf("%s\n",errcode==0?"Switched to secondary port":"Already using secondary port");
  errcode=myCmd->send_command((unsigned char*)"\xEA",1,500,3);
//
  errcode=myCmd->use_primary_port();
  printf("%s\n",errcode==0?"Switched to primary port":"Already using primary port");
  errcode=myCmd->send_command((unsigned char*)"\xEA",1,500,3);
//
//  errcode=myCmd->send_command((unsigned char*)"\xEA",1,500);
//  errcode=myCmd->send_command((unsigned char*)"\xEA",1,500,10);
//  errcode=myCmd->send_command((unsigned char*)"\xEA",1,500,3);
//  printf("Error: %s.\n",myCmd->read_error_message(errcode));

// Test new/delete
//  for (i=0;i<100;++i) {
//    printf("Step #%d\n",i);
//
//
//    myCmd = new Ccommand(CCB_ID,SERVER,PORT,myLog);
//    myCmd->disconnect();
//    myCmd->connect();
//    myCmd->send_command((unsigned char*)"\xea",1,2);
//    myCmd->send_command((unsigned char*)"\x10",1,2);
//
//    mywait();
//
//    myCmd->disconnect();
//    delete myCmd;
//  }

  mywait();
}
