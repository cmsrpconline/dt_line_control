#include <apfunctions.h>
#include <Clog.h>
#include <Ccommand.h>
#include <Ctss.h>
#include <Cconf.h>

#include <iostream>


#define CCB_ID 0x01
#define SERVER "127.0.0.1"
//#define SERVER "icab149"
#define PORT 18889

int main() {
  const char brd=0;
  unsigned char conf[7]={0,0,0,0,0,0,0},
    testin[20]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

  Clog *myLog;
  Ccommand *myCmd;
  Ctss *myTss;
  Cconf *myConf;

// Iinitialise objects
  myLog = new Clog("log.txt"); // Log to file
//  myLog = new Clog(); // Log to DB
  myCmd = new Ccommand(CCB_ID,SERVER,PORT,myLog);
  myTss = new Ctss(myCmd); // New TSS object
  myConf = new Cconf("../config_files/MB1-neg-default.txt"); // config

//  myTss->verbose_mode(true);
  myCmd->set_write_log(true); // Switch on logger

//  myTss->read_TDC(0,0);
//  myTss->read_TDC_print();
//  mywait();
//
//  myTss->status_TDC(0,0);
//  myTss->status_TDC_print();
//  mywait();
//
  myTss->read_ROB_error();
  myTss->read_ROB_error_print();
  mywait();
//
//  myTss->config_TSS(myConf,-1);
//  mywait();
//  myTss->read_TSS_config("pippo.txt");
//  mywait();


  myTss->write_TSS(brd,conf,testin); // 0x16
  myTss->write_TSS_print();
//  mywait();
//
//  myTss->read_TSS(brd); // 0x1D
//  myTss->read_TSS_print();
//  mywait();
//
//  myTss->read_TSS(99); // 0x1D
//  myTss->read_TSS_print();
//  mywait();
}
