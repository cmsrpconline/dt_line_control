#include <ccbcmds/apfunctions.h>

#include <ccbdb/Ccmsdtdb.h>
#include <ccbdb/Cccbconfdb.h>
#include <ccbdb/Cccbmap.h>
#include <ccbdb/Cccbrefdb.h>
#include <ccbdb/Cccbstatus.h>
#include <ccbdb/Cccbdata.h>
#include <ccbdb/Cdtpartition.h>
#include <ccbdb/Cdtlogger.h>
#include <ccbdb/Cpadcdata.h>
#include <ccbdb/Cpadcstatus.h>
#include <ccbdb/Cdaccalib.h>

#define PRETESTS         true
#define TEST_CMSDTDB     false
#define TEST_CCBCONFDB   false
#define TEST_CCBMAP      false
#define TEST_CCBREFDB    false
#define TEST_CCBSTATUS   false
#define TEST_CCBDATA     false
#define TEST_DTPARTITION false
#define TEST_DTLOGGER    false
#define TEST_PADCDATA    false
#define TEST_PADCSTATUS  false
#define TEST_DACCALIB    false

#include <iostream>
#include <stdio.h>
#include <stdlib.h>

void query(char*); // SQL query
int fileread(char*,char*); // Read a complete file
void mywait(); // wait for a key to be pressed;

//#define CONFNAME "andrea"
#define CCBID 92
#define CONFNAME "LNL-2ch-default"
//#define CCBID 999

#define MAXREF 128

int main() {

  cmsdtdb dtdbobj;
//  cmsdtdb dtdbobj("cmsdtdb","daqcms","daqcms111");
//  cmsdtdb dtdbobj("cmsdtdb","daqcms","daqcms");
  char field[FIELD_MAX_LENGTH];
  int i,j,resu,rows,cols;

  if (PRETESTS) { // Begin Tests

    dtdbobj.sqlquery("SELECT * FROM ccbmap ORDER by ccbid",&rows,&cols);
    for (i=0; dtdbobj.fetchrow() == SQL_SUCCESS; ++i) {
      printf("*** Resu=");
      for (j=0; j<cols; ++j) {
        dtdbobj.returnfield(j,field);
        printf(" %s",field);
      }
      std::cout << std::endl;
    }
    mywait();

    dtdbobj.sqlquery("SELECT * FROM ccbstatus",&rows,&cols);
    if (dtdbobj.fetchrow()==SQL_SUCCESS) {
      for (j=0; j<cols; ++j) {
        dtdbobj.returnfield(j,field);
        printf(" %s",field);
      }
      std::cout << std::endl;
    }
    mywait();

    dtdbobj.sqlquery("SELECT * FROM ccbref",&rows,&cols);
    for (i=0; dtdbobj.fetchrow() == SQL_SUCCESS; ++i) {
      printf("*** Resu=");
      for (j=0; j<cols; ++j) {
        dtdbobj.returnfield(j,field);
        printf(" %s",field);
      }
      std::cout << std::endl;
    }

//    query("SELECT * FROM ccbmap ORDER by ccbid"); // ok (MySQL-ORACLE)
//    mywait();
//    query("SELECT * FROM ccbmap ORDER by ccbid"); // ok (MySQL-ORACLE)
//    mywait();

//    dtdbobj.verbose_mode(true);
//    dtdbobj.setcurrun(199);

//    dtdbobj.dblock();
//    printf("DTDB locked\n");
//
//    dtdbobj.dbunlock();
//    printf("DTDB unlocked\n");

//    for (i=0;i<10;++i) {
//      printf("In loop... step %d\n",i);
//      dtdbobj.disconnect();
//      dtdbobj.connect();
//      apsleep(5000);
//    }

//    for (i=0;i<10000;++i) {
////      resu=dtdbobj.sqlquery("SELECT COUNT(1) FROM ccbmap",&rows,&cols);
////      if (resu!=0) printf("%d. resu=%d\n",i,resu);
////      else printf("%d. Rows: %d - Cols: %d\n",i,rows,cols);
////
////      resu=dtdbobj.sqlquery("SELECT * FROM dtpartitions",&rows,&cols);
////      if (resu!=0) printf("%d. resu=%d\n",i,resu);
////      else printf("%d. Rows: %d - Cols: %d\n",i,rows,cols);
//
//      resu=dtdbobj.sqlquery("INSERT INTO logger (levl,body,runid) VALUES ('mesg','pippo',1)",&rows,&cols);
//      if (resu!=0) printf("%d. resu=%d\n",i,resu);
//      else printf("%d. Rows: %d - Cols: %d\n",i,rows,cols);
//
//      apsleep(20);
//    }
  } // End PRETESTS

  if (TEST_CMSDTDB) { // Begin Test cmsdtdb
    char mquer[1024];
    int i;

//    for (i=0;i<15;++i) {
//      int row,cols;
//      dtdbobj.sqlquery("SELECT pippo FROM paperino",&row,&cols); // ok (MySQL-ORACLE)
//    }
//    mywait();

    query("SELECT * FROM ccbmap ORDER by ccbid"); // ok (MySQL-ORACLE)
    mywait();
    query("SELECT * FROM ccbref"); //  ok (MySQL-ORACLE)
    mywait();
//    query("SELECT RUNID,CCBID,STATUSSTRUCT FROM ccbstatus"); // ok (MySQL-ORACLE)
//    mywait();
//    query("SELECT * FROM ccbstatus"); // ok (MySQL-Oracle)
//    mywait();
//    query("SELECT time,statusxml FROM ccbstatus ORDER by time"); // ok (MySQL-Oracle)
//    mywait();
//    query("SELECT * FROM ccbdata"); // ok (Oracle)
//    mywait();
//    query("SELECT * FROM configcmds"); // ok (MySQL-ORACLE)
//    mywait();
//    query("SELECT CONFID,CONFNAME FROM configs"); // ok (MySQL-ORACLE)
//    mywait();
//    query("SELECT * FROM configs"); // ok (MySQL-Oracle)
//    mywait();
//    query("SELECT * FROM daqpartition"); // ok (MySQL-ORACLE)
//    mywait();
//    query("SELECT * FROM dcsdaqstatus"); // ok (MySQL-Oracle)
//    mywait();
//    query("SELECT * FROM dtpartitions"); // ok (MySQL-ORACLE)
//    mywait();
//    query("SELECT MSGID,LEVL,BODY,RUNID FROM logger"); // ok (MySQL-Oracle)
//    mywait();
//    query("SELECT * FROM logger"); // ok (MySQL-Oracle)
//    mywait();
//    query("SELECT * FROM logger WHERE datetime>='23-JAN-07' ORDER BY msgid");
//    mywait();
//    query("SELECT * FROM runconfigs"); // ok (MySQL-Oracle)
//    mywait();
//    query("SELECT * FROM runfiles"); // ok (MySQL-ORACLE)
//    mywait();
//    query("SELECT RUN,TYPE,RUNCOMMENT FROM tbrun"); // ok (MySQL-Oracle)
//    mywait();
//    query("SELECT * FROM tbrun"); // ok (MySQL-Oracle)
//    mywait();
//    query("SELECT * FROM padcrelations"); // ok (Oracle)
//    mywait();
//    query("SELECT * FROM padcdata"); // ok (Oracle)
//    mywait();
//    query("SELECT * FROM padcstatus"); // ok (Oracle)
//    mywait();
  } // End Test cmsdtdb class

  if (TEST_CCBCONFDB) { // Begin Test Cccbconfdb
    int ii, kk, ccbid=CCBID;
    ccbconfdb myconf(&dtdbobj);

//    printf("Insert - code = %d\n",myconf.cfginsert(CONFNAME,0,"Prova"));
//    printf("Insert - code = %d\n",myconf.cfginsert("pippooo",0,""));
//    mywait();

//    myconf.confignewrun();
//    printf("Current run: %d\n",myconf.curr_run);
//
//    printf("Confkey: %d\n",myconf.confselect(CONFNAME));
//    myconf.updaterun(123);
//    printf("Current Run: %d\n",myconf.curr_run);
//
    char cmds[500][512], confs[100][64];
    int tot;
//
//    printf("Confkey: %d\n",myconf.confselect(CONFNAME));
//    myconf.cmdinsert("abcdef","\x55\x24\x54\x0\x4\x29\x17\x3F\x1E\x1F\xE\x2D\x1C\x3A\x29\x1\x3E\0\0\0\x3F\x3F\x3F\x3F\x30\0\0\0\0\0\0\0\0\0\0\1",brk_feb_mask);
//    myconf.retrievecmds(ccbid,&tot);
//    myconf.retrievecmds(ccbid,brk_feb_width,&tot);
//    printf("Found %d cmds for conf: %s ccbid: %d\n",tot,CONFNAME,ccbid);
//
//    myconf.confselect(CONFNAME);
//    myconf.retrievecmds(ccbid,&tot,cmds,3);
//    myconf.retrievecmds(ccbid,&tot,brk_feb_thr,cmds,3);
//    myconf.retrievecmds(ccbid,&tot,cmds);
//    myconf.retrievecmds(ccbid,brk_bti_phi,&tot,cmds);
//    printf("Found %d cmds for conf: %s ccbid: %d\n",tot,CONFNAME,ccbid);
//
//    for (int ii=0;ii<tot;ii++) {
//      printf("cmd %d: %s\n",ii,cmds[ii]);
//    }
//    mywait();
//
//    printf("configs.confname: %s.\n",myconf.retrieve_configs_confname(ccbid));
//
//    myconf.retrieveconfs(&tot,confs);
//    printf("%d conf(s) retrieved\n",tot);
//    for (ii=0;ii<tot;++ii)
//      printf("ConfName: %s\n",confs[ii]);
//
//    myconf.ccbinsert(27,1,1);
//    myconf.ccbinsert(28,1,1);
//    myconf.ccbinsert(29,1,1);
//    myconf.ccbinsert(30,1,1);
//
//    printf("First available confid=%d\n",myconf.get_available_confid());

    char maskxml[2048];
    resu=myconf.retrieve_confmask(ccbid,maskxml);

    printf("resu=%d\n*** Mask ***\n%s\n",resu,maskxml);

  } // End Test Cccbconfdb

  if (TEST_CCBMAP) { // Begin Test Cccbmap
    ccbmap myccbmap(&dtdbobj);
    char ccbserver[256];
    int ccbid = CCBID;

//    myccbmap.insert(ccbid,-3,9,4,10,20,1,99,"lxparenti","Nessun commento");
//    myccbmap.setrefs(ccbid,boot,1);
//    myccbmap.setrefs(ccbid,status,5);
//    myccbmap.setrefs(ccbid,test,12);
//    myccbmap.setrefs(ccbid,1,5,12);
//    mywait();
//

//    if (myccbmap.retrieve(0,1,1)>=0) {
    if (myccbmap.retrieve(ccbid)>=0) {
      printf("mId: %d\n",myccbmap.mId);
      printf("wheel: %d\n",myccbmap.wheel);
      printf("sector: %d\n",myccbmap.sector);
      printf("station: %d\n",myccbmap.station);
      printf("chamber: %d\n",myccbmap.chamber);
      printf("minicrate: %d\n",myccbmap.minicrate);
      printf("online: %d\n",myccbmap.online);
      printf("port: %d\n",myccbmap.port);
      printf("secport: %d\n",myccbmap.secport);
      printf("ccbserver: %s\n",myccbmap.ccbserver);
      printf("Comment: %s\n",myccbmap.comment);
    }

//    printf("Isonline: %d\n",myccbmap.isonline(ccbid));
//    myccbmap.setonline(ccbid);
//    printf("Isonline: %d\n",myccbmap.isonline(ccbid));
//    myccbmap.setoffline(ccbid);
//    printf("Isonline: %d\n",myccbmap.isonline(ccbid));
//
    myccbmap.setport(ccbid,80);
    printf("Primary port: %d\n",myccbmap.getport());

    myccbmap.setsecport(ccbid,999);
    printf("Secondary port: %d\n",myccbmap.getsecport());
//    myccbmap.setserver(ccbid,"127.0.0.1");
//    myccbmap.getserver(ccbid,ccbserver);
//    myccbmap.getserver(ccbserver);
//    printf("CCBserver: %s\n",ccbserver);

//    int sign;
//    float R,z;
//    myccbmap.insert_BTItheta(ccbid,-451.8,654.15,-1);
//    apsleep(1000);
//    myccbmap.insert_BTItheta(ccbid,-451.7,655.15,1);

//    myccbmap.retrieve_BTItheta(ccbid,&R,&z,&sign);
//    printf("ccbid=%d y=%f z=%f sign=%d\n",ccbid,R,z,sign);

  } // End Test Cccbmap


  if (TEST_CCBREFDB) { // Begin Test ccbref
    reftype rtype;
    char name[LEN1], cmnt[LEN1], refxml[8000];
    char cmntl[MAXREF][LEN2];
    char strreftype[][7]={"boot","status","test","rob"};
    short mctype;
    int ii, howmany, idl[MAXREF];

//    ccbrefdb myccbref;
    ccbrefdb myccbref(&dtdbobj);

// Insert PADC reference values in DB
//    fileread("../ref_files/padcstatus.xml",refxml);
//    myccbref.insert((short)1,padc,refxml,"PADC (dummy) reference values");

// Insert ROB reference values in DB
//    fileread("../ref_files/robstatus.xml",refxml);
//    myccbref.insert((short)1,rob,refxml,"ROB reference values");

// Insert boot values in DB
//    fileread("../ref_files/boot.xml",refxml);
//    myccbref.insert((short)1,boot,refxml,"Boot reference values");

//    for (ii=1; ii<=7;++ii) { // Insert status/test values in DB
//      sprintf(name,"../ref_files/status-%1d.xml",ii);
//      sprintf(cmnt,"Status reference values - mb%1d",ii);
////      sprintf(name,"../ref_files/test-%1d.xml",ii);
////      sprintf(cmnt,"Test reference values - mb%1d",ii);
//
//      fileread(name,refxml);
//
//      myccbref.insert((short)ii,status,refxml,cmnt);
////      myccbref.insert((short)ii,test,refxml,cmnt); 
//    }
//

    myccbref.retrieveref(CCBID,boot,refxml,cmnt);
    printf("*** Reference values:Comment:\n%s\n%s\n",cmnt,refxml);
    mywait();
    myccbref.retrieveref(CCBID,status,refxml,cmnt);
    printf("*** Reference values:Comment:\n%s\n%s\n",cmnt,refxml);
    mywait();
    myccbref.retrieveref(CCBID,test,refxml,cmnt);
    printf("*** Reference values:Comment:\n%s\n%s\n",cmnt,refxml);
    mywait();
    myccbref.retrieveref(CCBID,rob,refxml,cmnt);
    printf("*** Reference values:Comment:\n%s\n%s\n",cmnt,refxml);
    mywait();
    myccbref.retrieveref(CCBID,padc,refxml,cmnt);
    printf("*** Reference values:Comment:\n%s\n%s\n",cmnt,refxml);
    mywait();
//
//    for (ii=1; ii<=15; ++ii) {
//      myccbref.retrieveref(ii,&mctype,&rtype,refxml,cmnt);
//      printf("*** Reference values:\nMcType=%d\nRefType=%s\n%s\n*** Comment:\n%s\n",mctype,strreftype[rtype],refxml,cmnt);
//      mywait();
//    }
//
//
//    for (ii=1; ii<=16; ++ii) {
//      myccbref.retrieveref(ii,&mctype,&rtype,refxml,cmnt);
//      printf("*** Reference ID: %d MCType=%d RefType=%s Comment: %s\n",ii,mctype,strreftype[rtype],cmnt);
//      mywait();
//    }

//    myccbref.retrieveref(1,rob,&howmany,idl,cmntl);
//    for (ii=0; ii<howmany; ++ii)
//      printf("*** Reference: RefID= %d Comment: %s\n",idl[ii],cmntl[ii]);

//    myccbref.retrievelastref(1,rob,&ii,cmnt);
//    printf("*** Reference: RefID= %d Comment: %s\n",ii,cmnt);

  } // End Test ccbref

  if (TEST_CCBSTATUS) { // Begin Test Cccbstatus
//    ccbstatus myccbstatus;
    ccbstatus myccbstatus(&dtdbobj);
    int i;

    char time[21], rdst[FIELD_MAX_LENGTH], statusxml[1024], status[]="\x55\xC5\x13\x03\xE7\x00\x01\x00\x0E\x00\x01\xFF\xEF\xCF\x3F\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x07\xAE\x50\x00\x00\x00\x00\x02\x0C\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x5A\xBC\x00\x03\x7B\xB8\x00\x02\x4F\x8E\x00\x03\x69\x9A\x00\x02\x78\x52\x00\x01\x63\xA8\x00\x02\x78\x52\x00\x01\x63\x84\x00\x02\x50\x95\x00\x03\x50\xCE\x00\x03\x51\x18\x00\x03\x4E\x00\x00\x02\x4D\xDC\x00\x02\x4D\xDC\x00\x02\x53\x60\x00\x03\x53\x84\x00\x02\x5F\xFA\x00\x01\x5F\xFA\x00\x01\x5F\xFA\x00\x01\x66\xE6\xFF\xFB\x66\xE6\xFF\xFB\x66\xE6\xFF\xFB\x46\x80\x00\x02\x01\x6F\x00\xE7\x01\x69\x00\xE1\x01\x6F\x00\xE7\x41\x00\x00\x06\x00\x04\x00\x00\x5A\xBC\x00\x03\x5B\x06\x00\x03\x7B\x70\x00\x02\x7B\xB8\x00\x02\x53\x3C\x00\x03\x53\x84\x00\x03\x53\x60\x00\x02\x53\xA8\x00\x02";

//    myccbstatus.insert(status);
//    myccbstatus.insert(status,"<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");

//    myccbstatus.retrievelast(CCBID,rdst,time);
//    myccbstatus.retrievelast(CCBID,rdst,statusxml,time);
//    printf("Status: \"%s\"\n",rdst);
//    printf("Status xml: \"%s\"\n",statusxml);
//    printf("Time: \"%s\"\n",time);

    unsigned char rob_error_state[7];
    float mc_temp_rob[7], read_rob_pwr_Vcc[7], read_rob_pwr_Vdd[7], read_rob_pwr_current[7];

    for (i=0;i<7;++i) {
      rob_error_state[i]=255;
      mc_temp_rob[i]=25.0;
      read_rob_pwr_Vcc[i]=2.5;
      read_rob_pwr_Vdd[i]=3.3;
      read_rob_pwr_current[i]=1.0;
    }

    myccbstatus.rob_insert(CCBID,rob_error_state,mc_temp_rob,read_rob_pwr_Vcc,read_rob_pwr_Vdd,read_rob_pwr_current);

    if (myccbstatus.rob_retrievelast(CCBID,rob_error_state,mc_temp_rob,read_rob_pwr_Vcc,read_rob_pwr_Vdd,read_rob_pwr_current,time)==0) {

      printf("Last ROB status -- %s\n",time);
      for (i=0;i<7;++i)
        printf("%d) Err: %d Temp: %f Vcc: %f Vdd: %f I: %f\n",i,rob_error_state[i],mc_temp_rob[i],read_rob_pwr_Vcc[i],read_rob_pwr_Vdd[i],read_rob_pwr_current[i]);
    }

  } // End Test Cccbstatus

  if (TEST_CCBDATA) { // Begin Test Cccbdata
    ccbdata myccbdata(&dtdbobj);
//    ccbdata myccbdata;
    char data[1000],time[100];

    myccbdata.insert(1,11,"\x22\x33\x44",3);

//    myccbdata.retrievelast(2,data,time);
    myccbdata.retrievelast(1,data,time);

//    myccbdata.retrievelast(1,10,data,time);
//    myccbdata.retrievelast(1,56,data,time);

    printf("Data: %s\nTime: %s\n",data,time);

  } // End Test Cccbdata

  if (TEST_DTPARTITION) { // Begin Test dtpartition
//    dtpartition mydtpart;
    dtpartition mydtpart(&dtdbobj);

    char resu[1024];
    char *ccbconflist[250], partlist[10][32];
    char ccbserver[250][255];
    int nccb, npart, ccblist[250], portlist[250], secportlist[250];
    int i, rows, cols;

    for (i=0;i<250;++i) {
      ccbconflist[i] = new char[64];
    }

//    mydtpart.retrieveparts(&npart,partlist);
//    printf("*** Available partitions:\n");
//    for (i=0;i<npart;++i)
//      printf(" %s\n",partlist[i]);


//    mydtpart.sqlquery("SELECT ccbID FROM dtpartitions WHERE partname = 'prova'",&rows,&cols);
//    mydtpart.sqlquery("SELECT ccbID FROM dtpartitions WHERE partname = 'prova'",&rows,&cols);
//    mydtpart.fetchrow();
//    mydtpart.returnfield(0,resu);
//    printf("Resu: %s\n",resu);

//    for (i=0;i<4;++i) {
//      nccb++;
//      ccblist[i]=200+i;
//      strcpy(ccbconflist[i],"pippo");
//    }

//    mydtpart.select("tutte");
//    mydtpart.newdtpart("provaaaa",1);
//    mydtpart.insert(500);

//    mydtpart.insert("prova 2",4,ccblist);
//
//

    mydtpart.select("W-2");
//    mydtpart.retrieveccbs(&nccb,ccblist);
//    mydtpart.retrieveccbs("prova",&nccb,ccblist);
//    mydtpart.retrieveccbs(&nccb,ccblist,portlist);
//    mydtpart.retrieveccbs("prova",&nccb,ccblist,portlist);
//    mydtpart.retrieveccbs(&nccb,ccblist,portlist,ccbserver);
//    mydtpart.retrieveccbs("prova",&nccb,ccblist,portlist,ccbserver);
    mydtpart.retrieveccbs(&nccb,ccblist,portlist,secportlist,ccbserver);
//    mydtpart.retrieveccbs("prova",&nccb,ccblist,portlist,secportlist,ccbserver);
//    printf("CCB IDs  PORTs  Server:\n");
    for (i=0;i<nccb;++i)
      printf(" %3d     %3d  %3d  %s\n",ccblist[i],portlist[i],secportlist[i],ccbserver[i]);

  } // End Test dtpartition

  if (TEST_DTLOGGER) { // Begin Test dtlogger
    char msg[1024];
//    dtlogger mylog;
    dtlogger mylog(&dtdbobj);

    mylog.fetchlast(msg);
    printf("** Message: %s\n",msg);
    mywait();

    for (i=0;i<100;++i) {
      mylog.Tell("Prova di Tell");
//      if (mylog.fetchlast(msg)==0)
//        printf("*** Message: %s\n",msg);
//      mywait();
    }


//    mylog.Warn("Prova di Warn");
//    mylog.fetchlast(msg);
//    printf("*** Message: %s\n",msg);
//    mywait();

//    mylog.Erro("Prova di Erro");
//    mylog.fetchlast(msg);
//    printf("*** Message: %s\n",msg);
//    mywait();

  } // End Test dtlogger


  if (TEST_PADCDATA) { // Begin Test padcdata
//    padcdata mypadc;
    padcdata mypadc(&dtdbobj);

    short adc_count[10]={1023,616,616,573,614,614,612,573,574,639};
    int i;
    float data_read[10];

    mypadc.select(371);
    mypadc.padcinfo();
    mywait();

//    mypadc.padcinfo(2);
//    mywait();

    mypadc.retrievepadcdata(adc_count,data_read);
    for (i=0;i<10;++i)
      printf("data_read[%d]: %f\n",i,data_read[i]);
    mywait();

    mypadc.retrievepadcdata(3,adc_count,data_read);
    for (i=0;i<10;++i)
      printf("data_read[%d]: %f\n",i,data_read[i]);
    mywait();

  }  // End Test padcdata

  if (TEST_PADCSTATUS) { // Begin Test padcstatus
//        padcstatus mypadcs;
    padcstatus mypadcs(&dtdbobj);

    char adccount[256],padcxml[1024],time[256];

    mypadcs.insert(259,"pippo");
//    mypadcs.insert(270,"pippo","ahah");

    mypadcs.retrievelast(259,adccount,padcxml,time);
    printf(" adccount: %s\n xml: %s\n Time: %s\n",adccount,padcxml,time);
    mypadcs.retrievelast(270,adccount,padcxml,time);
    printf(" adccount: %s\n xml: %s\n Time:%s\n",adccount,padcxml,time);

  }  // End Test padcstatus


  if (TEST_DACCALIB) { // Begin Test daccalib
//    daccalib mydaccalib;
    daccalib mydaccalib(&dtdbobj);

    float calib[14];
    
    mydaccalib.select(16);
    mydaccalib.daccalib_print();

    mydaccalib.daccalib_print(24);

    mydaccalib.select(31);
    mydaccalib.retrievedaccalib(calib);
    std::cout << "DAC CALIB:";
    for (i=0;i<14;++i)
      std::cout << ' ' << calib[i];
    putchar('\n');


  }  // End Test daccalib

  return 0;
}


// SQL query
void query(char *mquer) {
  cmsdtdb mydb;
//  cmsdtdb mydb("cmsdtdb","daqcms","daqcms");
//  cmsdtdb mydb("cmsdtdb","","");
//  cmsdtdb mydb("cmsdtdb","daq","daq");

  char resu[FIELD_MAX_LENGTH];
  int i, j, rows, cols, len;

  mydb.sqlquery(mquer,&rows,&cols);
  std::cout << "Query: " << mquer << '\n';
  printf("Rows: %d - Cols: %d\n",rows,cols);
  for (i=0; mydb.fetchrow() == SQL_SUCCESS; ++i) {
    printf("*** Resu=");
    for (j=0; j<cols; ++j) {
      mydb.returnfield(j,resu);
      printf(" %s",resu);
    }
      std::cout << std::endl;
  }
}

// Read a complete file -- from www.cplusplus.com
int fileread(char *name, char *cont) {
  FILE *pFile;
  long lSize;

  pFile = fopen(name,"rt");
  if (pFile==NULL) return 0;

  // obtain file size.
  fseek (pFile, 0, SEEK_END);
  lSize = ftell(pFile);
  rewind (pFile);

  // copy the file into the buffer.
  fread (cont,1,lSize,pFile);
  cont[lSize]='\0'; // String terminator

  /*** the whole file is loaded in the buffer. ***/

  // terminate
  fclose (pFile);
  return lSize;
}
