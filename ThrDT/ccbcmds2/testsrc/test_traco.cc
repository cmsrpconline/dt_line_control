#include <apfunctions.h>
#include <Clog.h>
#include <Ccommand.h>
#include <Ctraco.h>
#include <Cconf.h>

#include <iostream>

#define CCB_ID 0x01
#define SERVER "127.0.0.1"
//#define SERVER "icab149"
#define PORT 18889

int main() {
  const char brd=0, chip=0;

  unsigned char conf[10]={0X1F,0X90,0X8A,0X8A,0,0XFF,0XFF,0X1,0XFF,0X4},
    testin[28]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
  short data[64] = {0x040E,0x040D,0x040C,0x040B,0x040A,0x0409,0x0408,0x0407,0x0406,0x0405,0x0404,0x0403,0x0402,0x0401,0x0400,0x03FF,0x03FE,0x03FD,0x03FC,0x03FB,0x03FA,0x03F9,0x03F8,0x03F7,0x03F6,0x03F5,0x03F4,0x03F3,0x03F2,0x03F1,0x03F0,0x03EF,0x03EE,0x03ED,0x03EC,0x03EB,0x03EA,0x03E9,0x03E8,0x03E7,0x03E6,0x03E5,0x03E4,0x03E3,0x03E2,0x03E1,0x03E0,0x03DF,0x03DE,0x03DD,0x03DC,0x03DB,0x03DA,0x03D9,0x03D8,0x03D7,0x03D6,0x03D5,0x03D4,0x03D3,0x03D2,0x03D1,0x03D0,0x03CF};

  Clog *myLog;
  Ccommand *myCmd;
  Ctraco *myTraco;
  Cconf *myConf;

// Initialise objects
  myLog = new Clog("log.txt"); // Log to file
//  myLog = new Clog(); // Log to DB
  myCmd = new Ccommand(CCB_ID,SERVER,PORT,myLog);
  myTraco = new Ctraco(myCmd); // New TRACO object
  myConf = new Cconf("../config_files/MB1-neg-default.txt"); // config

//  myTraco->verbose_mode(true);
  myCmd->set_write_log(true); // Switch on logger

//  myTraco->read_TDC(0,0);
//  myTraco->read_TDC_print();
// mywait();
//
//  myTraco->status_TDC(0,0);
//  myTraco->status_TDC_print();
// mywait();
//
  myTraco->read_ROB_error();
  myTraco->read_ROB_error_print();
  mywait();
//

//  myTraco->MC_read_status();  // check status
//  myTraco->MC_read_status_print();

//  myTraco->config_TRACO(myConf,-1,-1);
//  myTraco->config_TRACO_print();
//  myTraco->config_LUT(myConf,-1,-1);
//  myTraco->config_LUT_print();
//  mywait();
//
//  myTraco->read_TRACO_config("pippo.txt");
//  mywait();
//
//  myTraco->read_LUT_config("pippo.txt");
//  mywait();
//
//  myTraco->write_TRACO(brd,chip,conf,testin); // 0x15
//  myTraco->write_TRACO_print();
//  mywait();
//
//  myTraco->read_TRACO(brd,chip); // 0x1B
//  myTraco->read_TRACO_print();
//  mywait();
//  myTraco->read_TRACO(99,99); // 0x1B
//  myTraco->read_TRACO_print();
//  mywait();
//
//  myTraco->write_TRACO_LUTS(brd,chip,(short)0x12,(short)0x1F,(float)512.469,(float)-90.5469,(float)-10.5073); // 0x3E
//  myTraco->write_TRACO_LUTS_print();
//  mywait();
//  myTraco->write_MC_LUTS((short)0x1F,(float)512.469,(float)-90.5469,-1); // 0xA8
//  myTraco->write_MC_LUTS_print();
//  mywait();
//
//  myTraco->preload_LUTS(brd,chip,0,(short)0x00,data); // 0x4D
//  myTraco->preload_LUTS_print();
//  myTraco->preload_LUTS(brd,chip,0,(short)0x40,data); // 0x4D
//  myTraco->preload_LUTS_print();
//  myTraco->preload_LUTS(brd,chip,0,(short)0x80,data); // 0x4D
//  myTraco->preload_LUTS_print();
//  myTraco->preload_LUTS(brd,chip,0,(short)0xC0,data); // 0x4D
//  myTraco->preload_LUTS_print();
//  mywait();
//
//  myTraco->load_LUTS(brd,chip); // 0x4E
//  myTraco->load_LUTS_print();
//  mywait();
//
//  myTraco->read_LUTS(brd,chip,0,0,64); // 0x86
//  myTraco->read_LUTS_print();
//  mywait();
//  myTraco->read_LUTS(99,99,0,0,64); // 0x86
//  myTraco->read_LUTS_print();
//  mywait();
//
//  myTraco->read_LUTS_param(brd,chip); // 0x88
//  myTraco->read_LUTS_param_print();
//  mywait();
//  myTraco->read_LUTS_param(99,99); // 0x88
//  myTraco->read_LUTS_param_print();
//  mywait();
//
  myTraco->read_MC_LUTS_param(); // 0xA9
  myTraco->read_MC_LUTS_param_print();
//  mywait();
}
