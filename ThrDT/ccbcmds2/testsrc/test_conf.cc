#include <ccbcmds/apfunctions.h>
#include <ccbcmds/Ccommand.h>
#include <ccbcmds/Cconf.h>
#include <ccbcmds/Cconfupdate.h>

#ifdef __USE_CCBDB__ // DB access enabled
  #include <ccbdb/Ccmsdtdb.h>
  #include <ccbdb/Cccbconfdb.h>
#endif

#include <iostream>

#define CCBID 92

int main() {
  unsigned char cmd_line[CMD_MAXLINES][CMD_LINE_DIM];
  char file[]="../config_files/basic_feb_mask_mb1_default.txt.new";
//  char file[]="../config_files/basic_rob_mb1_default.txt";
//  char file[]="../config_files/basic_rob_mb3_chimney_default.txt";
//  char file[]="../config_files/basic_rob_mb4-s_default.txt"; // mbtype=4
//  char file[]="../config_files/basic_rob_mb4-8-12_default.txt"; // mbtype=4
//  char file[]="../config_files/basic_rob_mb4-9-11_default.txt"; // mbtype=5
//  char file[]="../config_files/basic_rob_mb4-4_default.txt"; // mbtype=6
//  char file[]="../config_files/basic_rob_mb4-10_default.txt"; // mbtype=7
//  char file[]="../config_files/basic_bti_phi_mb2_neg_default.txt";
//  char file[]="../config_files/basic_bti_phi_mb3_default.txt";
//  char file[]="../config_files/basic_bti_phi_mb4-s_neg_default.txt"; // mbtype=4
//  char file[]="../config_files/basic_bti_phi_mb4-8-12_neg_default.txt"; // mbtype=4
//  char file[]="../config_files/basic_bti_phi_mb4-9-11_default.txt"; // mbtype=5
//  char file[]="../config_files/basic_bti_phi_mb4-4_default.txt"; // mbtype=6
//  char file[]="../config_files/basic_bti_phi_mb4-10_neg_default.txt"; // mbtype=7
//  char file[]="../config_files/basic_bti_theta_mb1_mb2_mb3_default_cosmics.txt";
  char confmaskfile[]="../ref_files/conf_mask.xml";

const short MaxPhiCh[8]={0,49,60,72,96,48,72,60}; // Phi channels per Layer Vs. MBtype
const short MaxTheCh[8]={0,57,57,57,0,0,0,0}; // Theta channels per Layer Vs. MBtype


  int i,j,k, line_len[CMD_MAXLINES], lines;

// Open a configuration
  Cconf *myConf = new Cconf(file); // File
#ifdef __USE_CCBDB__ // DB access enabled
//  cmsdtdb dtdbobj;
//  Cconf *myConf = new Cconf("pippo",&dtdbobj); // DB
#endif

  myConf->verbose_mode(true); // Switch to verbose mode

//  myConf->set_run(100);
//  myConf->set_run(0);

//  lines = myConf->read_conf(CCBID,0,(char)-1,(char)-1,cmd_line,line_len); // read lines from configuration
//
//  printf("*** %d lines found\n",lines);
//
//  for (i=0;i<lines;++i)
//    strprint(cmd_line[i],line_len[i]);
//  mywait();

// TEST bti_theta_update

//  Sbti_theta_param parameters, ptmp;
//  unsigned char oneline[]={0x54,0x6,0,0x29,0x17,0x3E,0x1E,0,0x01,0x3F,0,0,0x01,0x1,0x3E,0x0E,0x10,0,0x3F,0x3F,0x3F,0x3F,0x30,0,0,0,0,0,0,0,0,0,0,0};
//  parameters.tolerance=1;
//  parameters.ccbid=0;
//  parameters.R=439.365;
//  parameters.z=653.05;
//  parameters.sign=-1;
//  parameters.ccbid=CCBID;
//  parameters.R=0;
//  parameters.z=0;
//  parameters.sign=0;

// One line
//  if (bti_theta_decode(oneline,&ptmp) == 0)
//    printf("Tolerance: %d -> %d\n",ptmp.tolerance,parameters.tolerance);
//  if (bti_theta_update(oneline,parameters) != 0)
//    printf("Not a BTI_theta command line\n");
//  if (bti_theta_decode(oneline,&ptmp) == 0)
//    strprint(oneline,34);
//
//  for (i=0; i<lines; ++i) { // Loop on config lines
////    if (bti_theta_decode(cmd_line[i],&ptmp) == 0)
////      printf("Tolerance: %d -> %d",ptmp.tolerance,parameters.tolerance);
//    bti_theta_update(cmd_line[i],parameters) == 0;
////    if (bti_theta_decode(cmd_line[i],&ptmp) == 0)
////      printf("%d\n",ptmp.tolerance);
////    if (bti_theta_decode(cmd_line[i],&ptmp) == 0)
////      strprint(cmd_line[i],line_len[i]);
//  }

// Test mbtype_from_db
//  short ccbidlist[]={84,155,122,277,52,278,330,367,227,369,131,365,381,115,379,87,356,123,362,91,223,207};
//  for (i=0;i<22;++i) {
//    short mbtype;
//    bool chimney, mb4_8;
//
//    mbtype_from_db(ccbidlist[i],&mbtype,&chimney,&mb4_8);
//    printf("ccbid: %d mbtype: %d chimney: %c mb4_8: %c\n",ccbidlist[i],mbtype,chimney?'y':'n',mb4_8?'y':'n');
//  }

// TEST fe_mask_update
  Sfe_mask_param parameters;
////  unsigned char maskcmdline[]="\x3B\x00\x01\x00\x50\x00\x01";
//  unsigned char maskcmdline[]="\x3A\x00\x01\x00\x05\x00\x00\x00";
//
//  mask_reset(parameters.femask); // Reset femask
//  parameters.ccbid=108;
//  parameters.mbtype=1; // Set mbtype
//  parameters.chimney=false;
//  parameters.mb4_8=false;
//
//  if (parameters.mbtype!=1 || parameters.chimney) {
//// Mask last channel, SL phi, L1 (except mb1)
//  parameters.femask[0][0][MaxPhiCh[parameters.mbtype]-1]=1;
//  parameters.femask[2][0][MaxPhiCh[parameters.mbtype]-1]=1;
//  }
//  if (parameters.chimney) { // Chimney: Mask last channel, SL theta, L1
//    parameters.femask[1][0][MaxTheCh[parameters.mbtype]-10]=1;
//  }
//// Mask first channel, SL theta, L4
//  parameters.femask[0][3][0]=1;
//  parameters.femask[1][3][0]=1;
//  parameters.femask[2][3][0]=1;
//
//  fe_mask_print(parameters);
//  mywait();
//
////
////  fe_mask_update(maskcmdline,parameters);
////  if (maskcmdline[0]==0x3B)
////    strprint(maskcmdline,7); // 0x3B case
////  else if (maskcmdline[0]==0x3A)
////    strprint(maskcmdline,8); // 0x3A case
//
//  mask_reset(parameters.femask);
//  for (i=0; i<lines; ++i) { // Loop on config lines
//    fe_mask_decode(cmd_line[i],&parameters);
////    fe_mask_update(cmd_line[i],parameters);
////    printf("%02X %02X %02X %02X %02X %02X %02X %02X\n",cmd_line[i][0],cmd_line[i][1],cmd_line[i][2],cmd_line[i][3],cmd_line[i][4],cmd_line[i][5],cmd_line[i][6],cmd_line[i][7]);
//  }
//  fe_mask_print(parameters);
//  mywait();

// TEST tdc_mask_update
  Stdc_mask_param tparameters;
//
  mask_reset(tparameters.tdcmask); // Reset tdcmask
  tparameters.mbtype=1; // Set mbtype
  tparameters.chimney=false;
  tparameters.mb4_8=false;
//
//// Mask some channels
//  for (i=0;i<8;++i) {
//    tparameters.tdcmask[0][0][12*i+0]=true; // SL0/L0/ch=12i+0
//    tparameters.tdcmask[0][1][12*i+1]=true; // SL0/L1/ch=12i+1
//    tparameters.tdcmask[0][2][12*i+2]=true; // SL0/L2/ch=12i+2
//    tparameters.tdcmask[0][3][12*i+3]=true; // SL0/L3/ch=12i+3
////
//    tparameters.tdcmask[1][0][12*i+4]=true; // SL1/L0/ch=12i+4
//    tparameters.tdcmask[1][1][12*i+5]=true; // SL1/L1/ch=12i+5
//    tparameters.tdcmask[1][2][12*i+6]=true; // SL1/L2/ch=12i+6
//    tparameters.tdcmask[1][3][12*i+7]=true; // SL1/L3/ch=12i+7
////
//    tparameters.tdcmask[2][0][12*i+8]=true; // SL0/L0/ch=12i+8
//    tparameters.tdcmask[2][1][12*i+9]=true; // SL0/L1/ch=12i+9
//    tparameters.tdcmask[2][2][12*i+10]=true; // SL0/L2/ch=12i+10
//    tparameters.tdcmask[2][3][12*i+11]=true; // SL0/L3/ch=12i+11
//  }

// Mask non-existing channels
//  tparameters.tdcmask[0][3][0]=true; // SL0/L4/ch0
//  tparameters.tdcmask[1][3][0]=true; // SL1/L4/ch0
//  tparameters.tdcmask[2][3][0]=true; // SL2/L4/ch0
//  bool prvCh,newCh=true;
//  for (k=1;k<97;++k) {
//    prvCh=newCh;
//    newCh=dt_channel_exist(k,0,tparameters.mbtype,tparameters.chimney,tparameters.mb4_8);
//    if (prvCh==true && newCh==false) {
//      tparameters.tdcmask[0][0][k-1]=true; // SL0/L4/ch=last
//      tparameters.tdcmask[2][0][k-1]=true; // SL2/L4/ch=last
//      break;
//    }
//  }
//  tdc_mask_print(tparameters); // print mask
//  mywait();

//
////  for (i=0; i<lines; ++i) { // Loop on config lines
////    tdc_mask_update(cmd_line[i],tparameters); // update cmdlines
////  }
//
//  mask_reset(tparameters.tdcmask); // Reset tdcmask
//
//  for (i=0; i<lines; ++i) { // Loop on config lines
//    tdc_mask_decode(cmd_line[i],&tparameters); // read mask from cmdlines
//  }
//
//  tdc_mask_print(tparameters); // print mask
//  mywait();


// TEST bti_mask_update
  Sbti_mask_param bparameters;
//
  mask_reset(bparameters.btimask); // Reset tdcmask
  bparameters.mbtype=1; // Set mbtype
  bparameters.chimney=false;
  bparameters.mb4_8=false;

//// Mask some channels
//  for (i=0;i<8;++i) {
//    bparameters.btimask[0][0][12*i+0]=true; // SL0/L0/ch=12i+0
//    bparameters.btimask[0][1][12*i+1]=true; // SL0/L1/ch=12i+1
//    bparameters.btimask[0][2][12*i+2]=true; // SL0/L2/ch=12i+2
//    bparameters.btimask[0][3][12*i+3]=true; // SL0/L3/ch=12i+3
////
//    bparameters.btimask[1][0][12*i+4]=true; // SL1/L0/ch=12i+4
//    bparameters.btimask[1][1][12*i+5]=true; // SL1/L1/ch=12i+5
//    bparameters.btimask[1][2][12*i+6]=true; // SL1/L2/ch=12i+6
//    bparameters.btimask[1][3][12*i+7]=true; // SL1/L3/ch=12i+7
////
//    bparameters.btimask[2][0][12*i+8]=true; // SL0/L0/ch=12i+8
//    bparameters.btimask[2][1][12*i+9]=true; // SL0/L1/ch=12i+9
//    bparameters.btimask[2][2][12*i+10]=true; // SL0/L2/ch=12i+10
//    bparameters.btimask[2][3][12*i+11]=true; // SL0/L3/ch=12i+11
//  }
////
//  for (i=0; i<lines; ++i) { // Loop on config lines
//    bti_mask_update(cmd_line[i],bparameters); // update cmdlines
//  }
//
//  mask_reset(bparameters.btimask); // Reset tdcmask
//
//  for (i=0; i<lines; ++i) { // Loop on config lines
////    strprint(cmd_line[i],6);
//    bti_mask_decode(cmd_line[i],&bparameters); // read mask from cmdlines
//  }
//
//  bti_mask_print(bparameters); // print mask



// Write the configuration to a file
//  lines = myConf->write_conf_to_file(CCBID,"prova.txt");
//  printf("*** %d lines written to file\n",lines);
//  mywait();

#ifdef __USE_CCBDB__ // DB access enabled
//  ccbconfdb sConf; // API to database
//  char nome[]="lnl";
//  int num;
//  sConf.confselect(nome);
//  sConf.retrievecmds(CCBID,&num);
//  printf("Configuration \"%s\" ccbid=%d: %d lines\n",nome,CCBID,num);
#endif

// Test the conf_mask part
//  myConf->read_confmask(confmaskfile);
//  myConf->print_confmask();
//  myConf->reset_confmask();
//  mywait();

//  std::string cmask="<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n<conf_mask>\n  <bti brd='5' chip='1'></bti>\n</conf_mask>";
//  myConf->read_confmask(cmask);
//  myConf->print_confmask();
//  myConf->reset_confmask();
//  mywait();

  for (i=0;i<10;++i) {
    myConf->read_confmask(92);
//    myConf->print_confmask();
    myConf->reset_confmask();
    mywait();
  }

  return 0;
}
