#include <apfunctions.h>
#include <Clog.h>
#include <Ccommand.h>
#include <Ctsm.h>
#include <Cconf.h>

#include <iostream>

#define CCB_ID 0x01
#define SERVER "127.0.0.1"
//#define SERVER "icab149"
#define PORT 18889

int main() {
  unsigned char conf[2]={0,0};

  Clog *myLog;
  Ccommand *myCmd;
  Ctsm *myTsm;
  Cconf *myConf;

// Initialise objects
  myLog = new Clog("log.txt"); // Log to file
//  myLog = new Clog(); // Log to DB
  myCmd = new Ccommand(CCB_ID,SERVER,PORT,myLog);
  myTsm = new Ctsm(myCmd); // New TSM object
  myConf = new Cconf("../config_files/MB1-neg-default.txt"); // config

//  myTsm->verbose_mode(true);
  myCmd->set_write_log(true); // Switch on logger

//  myTsm->read_TDC(0,0);
//  myTsm->read_TDC_print();
//  mywait();

//  myTsm->status_TDC(0,0);
//  myTsm->status_TDC_print();
//  mywait();
//
  myTsm->read_ROB_error();
  myTsm->read_ROB_error_print();
  mywait();
//
//  myTsm->config_TSM(myConf);
//  mywait();
//
//  myTsm->read_TSM_config("pippo.txt");
//  mywait();
//
//  myTsm->write_TSM(conf,conf,conf); // 0x17
//  myTsm->write_TSM_print();
//  mywait();
//
//  myTsm->read_TSM(); // 0x1F
//  myTsm->read_TSM_print();
//  mywait();
//
  myTsm->TrgOut_select(1,0,1); // 0x49
  myTsm->TrgOut_select_print();
//  mywait();
}
