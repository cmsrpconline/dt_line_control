#include <ccbcmds/Cccb.h>
#include <ccbcmds/apfunctions.h>

#ifdef __USE_CCBDB__ // DB access enabled
  #include <ccbdb/Cccbstatus.h> // Support for cmsdtdb DB
#endif

#include <iostream>
#include <cstdio>


/*****************************************************************************/
// CCB class
/*****************************************************************************/

// Class constructor; arguments: pointer_to_command_object
// Author: A.Parenti, Dec01 2006
// Modified: AP, Aug31 2007
Cccb::Cccb(Ccommand* ppp) : Ccmn_cmd(ppp) {
  MC_check_status_reset(); // Reset mc_status_check struct
  MC_check_test_reset(); // Reset mc_test_check struct
  save_MC_conf_reset(); // Reset save_mc_conf struct
  load_MC_conf_reset(); // Reset load_mc_conf struct
  restore_MC_conf_reset(); // Reset restore_mc_conf struct
  exit_MC_reset(); // Reset exit_mc struct
  restart_CCB_reset(); // Reset restart_ccb struct
  read_CPU_addr_reset(); // Reset stop_on_boot struct
  run_in_progress_reset(); // Reset run_in_prog struct
  read_comm_err_reset(); // Reset mc_read_comm_err struct
  auto_set_LINK_reset(); // Reset auto_set_link struct
  read_LINK_reset(); // Reset read_link struct
  disable_LINK_monitor_reset(); // Reset disble_link_mon struct
  script_reset(); // Reset mc_script struct
  auto_trigger_reset(); // Reset mc_auto_trigger struct
  read_crate_sensor_id_reset(); // Reset mc_crate_sensor struct
  CCBrdy_pulse_reset(); // Reset mc_ccbrdy_pulse struct
  pirw_reset(); // Reset mc_pirw struct
  piprog_reset(); // Reset mc_piprog struct
  disable_SB_CK_reset(); // Reset disable_sb_ck struct
  disable_OSC_CK_reset(); // Reset disable_osc_ck struct
  write_custom_data_reset(); // Reset wr_custom_data struct
  read_custom_data_reset(); // Reset rd_custom_data struct
  hd_restart_CCB_reset(); // Reset hd_restart_ccb struct
  find_sensors_reset(); // Reset mc_find_sensor struct
  auto_set_LINK_boot_reset(); // Reset auto_set_link_boot struct
  LINK_test_boot_reset(); // Reset link_test_boot struct
  reset_SEU_boot_reset(); // Reset reset_seu_boot struct
  link_DAC_boot_reset(); // Reset link_dac_boot struct
}


/*********************/
/* Database commands */
/*********************/

// Log minicrate status in the DB
// Author: A. Parenti, Jul25 2005
// Modified/Tested: AP, Oct30 2007
void Cccb::MC_status_to_db(Cref *Pcref) {
  bool disable_qpll_check=false, mc_program;
  Emcstatus phys_status, logic_status;
  char ErrMsg[1024];

  if (this==NULL) return; // Object deleted -> return

  MC_check_status(Pcref,disable_qpll_check,&mc_program,&phys_status,&logic_status,ErrMsg); // Read CCB status (0xEA) and compare it to reference
  MC_temp(); // Read MC temperature (0x3C)

#ifdef __USE_CCBDB__ // DB access enabled
  ccbstatus *myCcbstatus;
  if (cmd_obj->dtdbobj==NULL) {
    myCcbstatus=new ccbstatus; // Create a new connection to DB
//    printf("ccbstatus: new connection\n");
  } else {
    myCcbstatus=new ccbstatus(cmd_obj->dtdbobj); // Use an existing connection to DB
//    printf("ccbstatus: existing connection\n");
  }


  std::string sss="";
  char xmlout[2048]="";
  unsigned char hdr[CCB_HDR_LEN];
  int i;

  if (mc_program) { // CCB is in minicrate program

    cmd_obj->make_ccb_header(mc_status.rawstatus_len,hdr); // Create header

    for (i=0;i<CCB_HDR_LEN;++i)
      sss += hdr[i]; // Copy header
    for (i=0;i<mc_status.rawstatus_len;++i)
      sss += mc_status.rawstatus[i]; // Copy string

// Write some relevant info to xmlout
    strcat(xmlout,"<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n");
    strcat(xmlout,"<status_results>\n");
    sprintf(xmlout,"%s<PwrTrb>%#x</PwrTrb>\n",xmlout,mc_status.PwrTrb);
    sprintf(xmlout,"%s<PwrRob>%#x</PwrRob>\n",xmlout,mc_status.PwrRob);

    sprintf(xmlout,"%s<in_Fe_Vcc>%.2f</in_Fe_Vcc>\n",xmlout,mc_status.Fe_Vcc[0]);
    sprintf(xmlout,"%s<th_Fe_Vcc>%.2f</th_Fe_Vcc>\n",xmlout,mc_status.Fe_Vcc[1]);
    sprintf(xmlout,"%s<out_Fe_Vcc>%.2f</out_Fe_Vcc>\n",xmlout,mc_status.Fe_Vcc[2]);
    sprintf(xmlout,"%s<in_Fe_Vdd>%.2f</in_Fe_Vdd>\n",xmlout,mc_status.Fe_Vdd[0]);
    sprintf(xmlout,"%s<th_Fe_Vdd>%.2f</th_Fe_Vdd>\n",xmlout,mc_status.Fe_Vdd[1]);
    sprintf(xmlout,"%s<out_Fe_Vdd>%.2f</out_Fe_Vdd>\n",xmlout,mc_status.Fe_Vdd[2]);

    sprintf(xmlout,"%s<Sp_Vcc>%.2f</Sp_Vcc>\n",xmlout,mc_status.Sp_Vcc);
    sprintf(xmlout,"%s<Sp_Vdd>%.2f</Sp_Vdd>\n",xmlout,mc_status.Sp_Vdd);

    sprintf(xmlout,"%s<in_Fe_Thr>%.2f</in_Fe_Thr>\n",xmlout,mc_status.Fe_Thr[0]);
    sprintf(xmlout,"%s<th_Fe_Thr>%.2f</th_Fe_Thr>\n",xmlout,mc_status.Fe_Thr[1]);
    sprintf(xmlout,"%s<out_Fe_Thr>%.2f</out_Fe_Thr>\n",xmlout,mc_status.Fe_Thr[2]);

    sprintf(xmlout,"%s<in_Tmax>%d</in_Tmax>\n",xmlout,mc_status.in_Tmax);
    sprintf(xmlout,"%s<th_Tmax>%d</th_Tmax>\n",xmlout,mc_status.th_Tmax);
    sprintf(xmlout,"%s<out_Tmax>%d</out_Tmax>\n",xmlout,mc_status.out_Tmax);
    sprintf(xmlout,"%s<in_Tmed>%d</in_Tmed>\n",xmlout,mc_status.in_Tmed);
    sprintf(xmlout,"%s<th_Tmed>%d</th_Tmed>\n",xmlout,mc_status.th_Tmed);
    sprintf(xmlout,"%s<out_Tmed>%d</out_Tmed>\n",xmlout,mc_status.out_Tmed);
//    sprintf(xmlout,"%s<BrdMaxTemp>%.2f</BrdMaxTemp>\n",xmlout,mc_status.BrdMaxtemp);

// Temperatures from 0x3C
    for (i=0;i<7;++i)
      if (mc_temp.temp[i]!=0) sprintf(xmlout,"%s<RobTemp idx=\"%d\">%.2f</RobTemp>\n",xmlout,i,mc_temp.temp[i]);
    for (i=7;i<15;++i)
     if (mc_temp.temp[i]!=0) sprintf(xmlout,"%s<TrbTemp idx=\"%d\">%.2f</TrbTemp>\n",xmlout,(i-7),mc_temp.temp[i]);
    for (i=15;i<21;++i)
      if (mc_temp.temp[i]!=0) sprintf(xmlout,"%s<ExtTemp idx=\"%d\">%.2f</ExtTemp>\n",xmlout,(i-15),mc_temp.temp[i]);

// Add a couple of summary flags (mc_status, fe_status)
    if (phys_status==s_ok)
      sprintf(xmlout,"%s<phys_status>ok</phys_status>\n",xmlout);
    else if (phys_status==s_error)
      sprintf(xmlout,"%s<phys_status>error</phys_status>\n",xmlout);
    else
      sprintf(xmlout,"%s<phys_status>undef</phys_status>\n",xmlout);

    if (logic_status==s_ok)
      sprintf(xmlout,"%s<logic_status>ok</logic_status>\n",xmlout);
    else if (logic_status==s_error)
      sprintf(xmlout,"%s<logic_status>error</logic_status>\n",xmlout);
    else
      sprintf(xmlout,"%s<logic_status>undef</logic_status>\n",xmlout);

// ccbstatus.statusxml has dimension 2048... xmlout has to fit in!
    if (strlen(ErrMsg)+strlen(xmlout)<1948)
      sprintf(xmlout,"%s<ErrMsg>%s</ErrMsg>\n",xmlout,ErrMsg);
    else
      sprintf(xmlout,"%s<ErrMsg>Many status errors...</ErrMsg>\n",xmlout);

    strcat(xmlout,"</status_results>\n");

//    strprint((unsigned char*)sss.c_str(),sss.length());
//    std::cout << "statusxml: " << xmlout << std::endl;
//    std::cout << "strlength(xmlout): " << strlen(xmlout) << std::endl;

    myCcbstatus->insert((char*)sss.c_str(),xmlout);
  }

  delete myCcbstatus;
#else
  nodbmsg();
#endif
}


// Retrieve minicrate status from the DB
// Author: A. Parenti, Jul25 2005
// Modified/Tested: AP, Oct30 2007
void Cccb::MC_status_from_db() {
#ifdef __USE_CCBDB__ // DB access enabled
  bool retrieved;
  char sttime[50]="", statusxml[FIELD_MAX_LENGTH];
  unsigned int i;
  int l, idx;

  char ccbst1[2*READ_DIM+2]="";
  std::string ccbst2="";
  unsigned char ccbst3[READ_DIM]="";

  if (this==NULL) return; // Object deleted -> return

  ccbstatus *myCcbstatus;
  if (cmd_obj->dtdbobj==NULL) {
    myCcbstatus=new ccbstatus; // Create a new connection to DB
//    printf("ccbstatus: new connection\n");
  } else {
    myCcbstatus=new ccbstatus(cmd_obj->dtdbobj); // Use an existing connection to DB
//    printf("ccbstatus: existing connection\n");
  }

// lock "cmd_mutex"
  cmd_obj->cmdlock();

  if (myCcbstatus->retrievelast((int)cmd_obj->read_ccb_id(),ccbst1,statusxml,sttime)>=0)
    retrieved = true; // A status string has been found
  else
    retrieved = false;
//  printf("String1: %s\n",ccbst1);

// Dummy reply for testing
//  retrieved = true;
//  strcpy(ccbst1,"0x55151300110001000a0001ffefcf3f000000000000");
// End testing

  if (retrieved) { // A status string has been found
// NB: ccbst1 begins with "0x"!!! Remove ccbst1[0] and ccbst1[1].
    for (i=2;i<strlen(ccbst1);i+=2) { // Insert ' ' between two exadecimal
      ccbst2 += ccbst1[i];
      ccbst2 += ccbst1[i+1];
      ccbst2 += ' ';
    }
//    printf("String2: %s\n",ccbst2.c_str());
    string_to_array((char*)ccbst2.c_str(),16,ccbst3,&l); // Convert ccbst2 to an array ccbstr3

//    printf("String3: "); strprint(ccbst3,l);
    idx = MC_read_status_decode(ccbst3+CCB_HDR_LEN); // Decode string ccbstr3, strip away header

    if (_verbose_) {
      printf("\nDate/Time of \"Read Status\": %s\n",sttime);
      printf("Read status (0xEA): %d (%d) bytes read (decoded)\n",l,idx);
    }
  }

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();

  delete myCcbstatus;

#else
  nodbmsg();
#endif
}


/****************************/
/* Override ROB/TDC methods */
/****************************/

void Cccb::read_TDC(char tdc_brd, char tdc_chip) {}
int Cccb::read_TDC_decode(unsigned char *rdstr) {return 0;}
void Cccb::read_TDC_print() {}

void Cccb::status_TDC(char tdc_brd, char tdc_chip) {}
void Cccb::status_TDC_print() {}

void Cccb::read_ROB_error() {}
int Cccb::read_ROB_error_decode(unsigned char *rdstr) {return 0;}
void Cccb::read_ROB_error_print() {}


/*******************/
/* Check MC status */
/* Firmware v1.21  */
/*******************/
// Run "Status" command (0xEA) and compare values with a reference
// Author: A.Parenti, May02 2005
// Modified/Tested: AP, Oct29 2007

void Cccb::MC_check_status(Cref *Pcref, bool disable_qpll_check, bool *Pmc_program, Emcstatus *Pphys_st, Emcstatus *Plogic_st, char *ErrMsg) {
  bool exitnow=false;
  Emcstatus phys_st, logic_st; // local status
  unsigned char Data[4];
  unsigned short DataCrc, CrashFlags;
  unsigned int PwrCrashFlags;

  if (this==NULL) return; // Object deleted -> return

  mc_firmware_version(); // Get firmware version

  phys_st = s_ok; // "Physical" status: ok, till now
  logic_st = s_ok; // "Logical" status: ok, till now
  mc_status_check.switch_off_fe = false; // Reset switch_off_fe flag
  mc_status_check.switch_off_mc = false; // Reset switch_off_mc flag
  strcpy(ErrMsg,"");

  MC_check_status_reset(); // Reset mc_status_check struct

  MC_read_status(); // Read status

  if (mc_status.code==0x13) {
    *Pmc_program = true; // In Minicrate Program

// 1. Minicrate program

// 1a. Verify reference values
    if (Pcref->ref_status.code!=0x13) {
      phys_st = s_undef;
      logic_st = s_undef;
      strcat(ErrMsg,"Reference values not found\n");
      exitnow=true;
    }

// 1b. Verify McType
    if (!exitnow && mc_status.McType==0) {
      phys_st = s_undef;
      logic_st = s_undef;
      strcat(ErrMsg,"Unknown McType\n");
      exitnow=true;
    } else if (!exitnow && mc_status.McType!=Pcref->ref_status.McType) {
      phys_st = s_undef;
      logic_st = s_undef;
      strcat(ErrMsg,"Wrong McType\n");
      exitnow=true;
    }

// 1c. Verify minicrate status
    if (!exitnow) { // skip checks
      if (mc_status.PwrAn!=Pcref->ref_status.PwrAn)
        {phys_st = s_error; strcat(ErrMsg,"PwrAn error\n");}
      if (mc_status.PwrCK!=Pcref->ref_status.PwrCK)
        {phys_st = s_error; strcat(ErrMsg,"PwrCK error\n");}
      if (mc_status.PwrLed!=Pcref->ref_status.PwrLed)
        {phys_st = s_error; strcat(ErrMsg,"PwrLed error\n");}
      if (mc_status.PwrRpc!=Pcref->ref_status.PwrRpc)
        {phys_st = s_error; strcat(ErrMsg,"PwrRpc error\n");}
      if (mc_status.PwrTrbBuf!=Pcref->ref_status.PwrTrbBuf)
        {phys_st = s_error; strcat(ErrMsg,"PwrTrbBuf error\n");}
      if (mc_status.PwrTrbVcc!=Pcref->ref_status.PwrTrbVcc)
        {phys_st = s_error; strcat(ErrMsg,"PwrTrbVcc error\n");}
      if (mc_status.PwrSO!=Pcref->ref_status.PwrSO)
        {phys_st = s_error; strcat(ErrMsg,"PwrSO error\n");}
      if (mc_status.PwrDU!=Pcref->ref_status.PwrDU)
        {phys_st = s_error; strcat(ErrMsg,"PwrDU error\n");}
      if (mc_status.PwrDD!=Pcref->ref_status.PwrDD)
        {phys_st = s_error; strcat(ErrMsg,"PwrDD error\n");}
      if (mc_status.PwrSBCK!=Pcref->ref_status.PwrSBCK)
        {phys_st = s_error; strcat(ErrMsg,"PwrSBCK error\n");}
      if (mc_status.PwrTH!=Pcref->ref_status.PwrTH)
        {phys_st = s_error; strcat(ErrMsg,"PwrTH error\n");}
      if (mc_status.TTCrdy!=Pcref->ref_status.TTCrdy)
        {logic_st = s_error; strcat(ErrMsg,"TTCrdy error\n");}
      if (mc_status.PwrFlash!=Pcref->ref_status.PwrFlash)
        {phys_st = s_error; strcat(ErrMsg,"PwrFlash error\n");}
      if (mc_status.EnTtcCkMux!=Pcref->ref_status.EnTtcCkMux)
        {logic_st = s_error; strcat(ErrMsg,"EnTtcCkMux error\n");}
      if (!disable_qpll_check) if (mc_status.QpllARdy!=Pcref->ref_status.QpllARdy)
        {phys_st = s_error; strcat(ErrMsg,"QpllARdy error\n");}
      if (!disable_qpll_check) if (mc_status.QpllBRdy!=Pcref->ref_status.QpllBRdy)
        {phys_st = s_error; strcat(ErrMsg,"QpllBRdy error\n");}

      if (mc_status.PwrTrb!=Pcref->ref_status.PwrTrb)
        {phys_st = s_error; strcat(ErrMsg,"PwrTrb error\n");}
      if (mc_status.PwrRob!=Pcref->ref_status.PwrRob)
        {phys_st = s_error; strcat(ErrMsg,"PwrRob error\n");}

      if (mc_status.AlrmPwrAn!=Pcref->ref_status.AlrmPwrAn)
        {phys_st = s_error; strcat(ErrMsg,"AlrmPwrAn error\n");}
      if (mc_status.AlrmPwrCK!=Pcref->ref_status.AlrmPwrCK)
        {phys_st = s_error; strcat(ErrMsg,"AlrmPwrCK error\n");}
      if (mc_status.AlrmPwrLed!=Pcref->ref_status.AlrmPwrLed)
        {phys_st = s_error; strcat(ErrMsg,"AlrmPwrLed error\n");}
      if (mc_status.AlrmPwrRpc!=Pcref->ref_status.AlrmPwrRpc)
        {phys_st = s_error; strcat(ErrMsg,"AlrmPwrRpc error\n");}
      if (mc_status.AlrmPwrTrbBuf!=Pcref->ref_status.AlrmPwrTrbBuf)
        {phys_st = s_error; strcat(ErrMsg,"AlrmPwrTrbBuf error\n");}
      if (mc_status.AlrmPwrTrbVcc!=Pcref->ref_status.AlrmPwrTrbVcc)
        {phys_st = s_error; strcat(ErrMsg,"AlrmPwrTrbVcc error\n");}
      if (mc_status.AlrmPwrSO!=Pcref->ref_status.AlrmPwrSO)
        {phys_st = s_error; strcat(ErrMsg,"AlrmPwrSO error\n");}
      if (mc_status.AlrmPwrDU!=Pcref->ref_status.AlrmPwrDU)
        {phys_st = s_error; strcat(ErrMsg,"AlrmPwrDU error\n");}
      if (mc_status.AlrmPwrDD!=Pcref->ref_status.AlrmPwrDD)
        {phys_st = s_error; strcat(ErrMsg,"AlrmPwrDD error\n");}
      if (mc_status.AlrmPwrSBCK!=Pcref->ref_status.AlrmPwrSBCK)
        {phys_st = s_error; strcat(ErrMsg,"AlrmPwrSBCK error\n");}
      if (mc_status.AlrmPwrTH!=Pcref->ref_status.AlrmPwrTH)
        {phys_st = s_error; strcat(ErrMsg,"AlrmPwrTH error\n");}
      if (mc_status.AlrmTTCrdy!=Pcref->ref_status.AlrmTTCrdy)
        {logic_st = s_error; strcat(ErrMsg,"AlrmTTCrdy error\n");}
      if (mc_status.AlrmPwrFlash!=Pcref->ref_status.AlrmPwrFlash)
        {phys_st = s_error; strcat(ErrMsg,"AlrmPwrFlash error\n");}
      if (mc_status.AlrmEnTtcCkMux!=Pcref->ref_status.AlrmEnTtcCkMux)
        {logic_st = s_error; strcat(ErrMsg,"AlrmEnTtcCkMux error\n");}
      if (!disable_qpll_check) if (mc_status.AlrmQpllAChng!=Pcref->ref_status.AlrmQpllAChng)
        {phys_st = s_error; strcat(ErrMsg,"AlrmQpllAChng error\n");}
      if (!disable_qpll_check) if (mc_status.AlrmQpllBChng!=Pcref->ref_status.AlrmQpllBChng)
        {phys_st = s_error; strcat(ErrMsg,"AlrmQpllBChng error\n");}

      if (mc_status.AlrmPwrTrb!=Pcref->ref_status.AlrmPwrTrb)
        {phys_st = s_error; strcat(ErrMsg,"AlrmPwrTrb error\n");}
      if (mc_status.AlrmPwrRob!=Pcref->ref_status.AlrmPwrRob)
        {phys_st = s_error; strcat(ErrMsg,"AlrmPwrRob error\n");}
      if (mc_status.AlrmTempTrb!=Pcref->ref_status.AlrmTempTrb)
        {phys_st = s_error; strcat(ErrMsg,"AlrmPwrRob error\n");}
      if (mc_status.AlrmTempRob!=Pcref->ref_status.AlrmTempRob)
        {phys_st = s_error; strcat(ErrMsg,"AlrmPwrRob error\n");}
      if (HVer<1 || (HVer==1 && LVer<7)) { // Firmware < v1.7
        if (mc_status.LoseLockCount!=Pcref->ref_status.LoseLockCountTTC)
          {logic_st = s_error; strcat(ErrMsg,"LoseLockCountTTC error\n");}
      }
      else { // Firmware >= v1.7
        if (mc_status.LoseLockCountTTC!=Pcref->ref_status.LoseLockCountTTC)
          {logic_st = s_error; strcat(ErrMsg,"LoseLockCountTTC error\n");}
        if (!disable_qpll_check) if (mc_status.LoseLockCountQPLL1!=Pcref->ref_status.LoseLockCountQPLL1)
          {logic_st = s_error; strcat(ErrMsg,"LoseLockCountQPLL1 error\n");}
        if (!disable_qpll_check) if (mc_status.LoseLockCountQPLL2!=Pcref->ref_status.LoseLockCountQPLL2)
          {logic_st = s_error; strcat(ErrMsg,"LoseLockCountQPLL2 error\n");}
      }
      if (mc_status.SeuRam!=Pcref->ref_status.SeuRam)
        {logic_st = s_error; strcat(ErrMsg,"SeuRam error\n");}
      if (mc_status.SeuIntRam!=Pcref->ref_status.SeuIntRam)
        {logic_st = s_error; strcat(ErrMsg,"SeuIntRam error\n");}
      if (mc_status.SeuBTI!=Pcref->ref_status.SeuBTI)
        {logic_st = s_error; strcat(ErrMsg,"SeuBTI error\n");}
      if (mc_status.SeuTRACO!=Pcref->ref_status.SeuTRACO)
        {logic_st = s_error; strcat(ErrMsg,"SeuTRACO error\n");}
      if (mc_status.SeuLUT!=Pcref->ref_status.SeuLUT)
        {logic_st = s_error; strcat(ErrMsg,"SeuLUT error\n");}
      if (mc_status.SeuTSS!=Pcref->ref_status.SeuTSS)
        {logic_st = s_error; strcat(ErrMsg,"SeuTSS error\n");}

      if (mc_status.Vccin<Pcref->ref_status.VccinMin || mc_status.Vccin>Pcref->ref_status.VccinMax)
        {phys_st = s_error; strcat(ErrMsg,"Vccin error\n");}
      if (mc_status.Vddin<Pcref->ref_status.VddinMin || mc_status.Vddin>Pcref->ref_status.VddinMax)
        {phys_st = s_error; strcat(ErrMsg,"Vddin error\n");}
      if (mc_status.Vcc<Pcref->ref_status.VccMin || mc_status.Vcc>Pcref->ref_status.VccMax)
        {phys_st = s_error; strcat(ErrMsg,"Vcc error\n");}
      if (mc_status.Vdd<Pcref->ref_status.VddMin || mc_status.Vdd>Pcref->ref_status.VddMax)
        {phys_st = s_error; strcat(ErrMsg,"Vdd error\n");}
      if (mc_status.Tp1L<Pcref->ref_status.Tp1LMin || mc_status.Tp1L>Pcref->ref_status.Tp1LMax)
        {phys_st = s_error; strcat(ErrMsg,"Tp1L error\n");}
      if (mc_status.Tp1H<Pcref->ref_status.Tp1HMin || mc_status.Tp1H>Pcref->ref_status.Tp1HMax)
        {phys_st = s_error; strcat(ErrMsg,"Tp1H error\n");}
      if (mc_status.Tp2L<Pcref->ref_status.Tp2LMin || mc_status.Tp2L>Pcref->ref_status.Tp2LMax)
        {phys_st = s_error; strcat(ErrMsg,"Tp2L error\n");}
      if (mc_status.Tp2H<Pcref->ref_status.Tp2HMin || mc_status.Tp2H>Pcref->ref_status.Tp2HMax)
        {phys_st = s_error; strcat(ErrMsg,"Tp2H error\n");}

      if (mc_status.BrdMaxtemp<Pcref->ref_status.BrdMaxtempMin || mc_status.BrdMaxtemp>Pcref->ref_status.BrdMaxtempMax)
        {phys_st = s_error; strcat(ErrMsg,"BrdMaxtemp error\n");}

      if (mc_status.RunInProgress) { // RunInProgress bit was set
        if (HVer>1 || (HVer==1 && LVer>=7)) { // Firmware >= v1.7
          if (!mc_status.CCBReady) {logic_st = s_error; strcat(ErrMsg,"CCBReady error\n");}
        }
        if (HVer>1 || (HVer==1 && LVer>=16)) { // Firmware >= v1.16
          if (!mc_status.CfgNotChanged) {logic_st = s_error; strcat(ErrMsg,"CfgNotChanged error\n");}
        }
      }

      int FeMaxTemp;
      FeMaxTemp=0; // FE max temperature
      FeMaxTemp=(mc_status.in_Tmax>FeMaxTemp)?mc_status.in_Tmax:FeMaxTemp;
      FeMaxTemp=(mc_status.th_Tmax>FeMaxTemp)?mc_status.th_Tmax:FeMaxTemp;
      FeMaxTemp=(mc_status.out_Tmax>FeMaxTemp)?mc_status.out_Tmax:FeMaxTemp;

      if (FeMaxTemp!=0 && (FeMaxTemp<Pcref->ref_status.FeOffTMin || FeMaxTemp>Pcref->ref_status.FeOffTMax)) {
        mc_status_check.switch_off_fe=true; // FE needs to be switched off
        strcat(ErrMsg,"FE temp out-of safety range: SWITCH OFF\n");
      }

      if (mc_status.BrdMaxtemp!=0.0 && (mc_status.BrdMaxtemp<Pcref->ref_status.McOffTMin || mc_status.BrdMaxtemp>Pcref->ref_status.McOffTMax)) {
        mc_status_check.switch_off_mc=true; // MC needs to be switched off
        strcat(ErrMsg,"TRB/ROB temp out-of safety range: SWITCH OFF\n");
      }
    } // End of "if (!exitnow)"

// 1d. Verify Front-End status
    if (!exitnow) { // skip checks
      if (mc_status.Fe_Vcc[0]<Pcref->ref_status.Fe_VccMin[0] || mc_status.Fe_Vcc[0]>Pcref->ref_status.Fe_VccMax[0])
        {phys_st = s_error; strcat(ErrMsg,"Fe_Vcc[0] error\n");}
      if (mc_status.Fe_Vcc[1]<Pcref->ref_status.Fe_VccMin[1] || mc_status.Fe_Vcc[1]>Pcref->ref_status.Fe_VccMax[1])
        {phys_st = s_error; strcat(ErrMsg,"Fe_Vcc[1] error\n");}
      if (mc_status.Fe_Vcc[2]<Pcref->ref_status.Fe_VccMin[2] || mc_status.Fe_Vcc[2]>Pcref->ref_status.Fe_VccMax[2])
        {phys_st = s_error; strcat(ErrMsg,"Fe_Vcc[2] error\n");}
      if (mc_status.Fe_Vdd[0]<Pcref->ref_status.Fe_VddMin[0] || mc_status.Fe_Vdd[0]>Pcref->ref_status.Fe_VddMax[0])
        {phys_st = s_error; strcat(ErrMsg,"Fe_Vdd[0] error\n");}
      if (mc_status.Fe_Vdd[1]<Pcref->ref_status.Fe_VddMin[1] || mc_status.Fe_Vdd[1]>Pcref->ref_status.Fe_VddMax[1])
        {phys_st = s_error; strcat(ErrMsg,"Fe_Vdd[1] error\n");}
      if (mc_status.Fe_Vdd[2]<Pcref->ref_status.Fe_VddMin[2] || mc_status.Fe_Vdd[2]>Pcref->ref_status.Fe_VddMax[2])
        {phys_st = s_error; strcat(ErrMsg,"Fe_Vdd[2] error\n");}

      if (mc_status.Sp_Vcc<Pcref->ref_status.Sp_VccMin || mc_status.Sp_Vcc>Pcref->ref_status.Sp_VccMax)
        {phys_st = s_error; strcat(ErrMsg,"Sp_Vcc error\n");}
      if (mc_status.Sp_Vdd<Pcref->ref_status.Sp_VddMin || mc_status.Sp_Vdd>Pcref->ref_status.Sp_VddMax)
        {phys_st = s_error; strcat(ErrMsg,"Sp_Vdd error\n");}

      if (mc_status.in_Tmax<Pcref->ref_status.in_TmaxMin || mc_status.in_Tmax>Pcref->ref_status.in_TmaxMax)
        {phys_st = s_error; strcat(ErrMsg,"in_Tmax error\n");}
      if (mc_status.in_Tmed<Pcref->ref_status.in_TmedMin || mc_status.in_Tmed>Pcref->ref_status.in_TmedMax)
        {phys_st = s_error; strcat(ErrMsg,"in_Tmed error\n");}
      if (mc_status.th_Tmax<Pcref->ref_status.th_TmaxMin || mc_status.th_Tmax>Pcref->ref_status.th_TmaxMax)
        {phys_st = s_error; strcat(ErrMsg,"th_Tmax error\n");}
      if (mc_status.th_Tmed<Pcref->ref_status.th_TmedMin || mc_status.th_Tmed>Pcref->ref_status.th_TmedMax)
        {phys_st = s_error; strcat(ErrMsg,"th_Tmed error\n");}
      if (mc_status.out_Tmax<Pcref->ref_status.out_TmaxMin || mc_status.out_Tmax>Pcref->ref_status.out_TmaxMax)
        {phys_st = s_error; strcat(ErrMsg,"out_Tmax error\n");}
      if (mc_status.out_Tmed<Pcref->ref_status.out_TmedMin || mc_status.out_Tmed>Pcref->ref_status.out_TmedMax)
        {phys_st = s_error; strcat(ErrMsg,"out_Tmed error\n");}
    } // End of "if (!exitnow)"

  }
  else if (mc_status.code==0xE9) { // In boot program
    *Pmc_program = false;

// 2. In Boot Program
// 2a. Verify reference values
    if (Pcref->ref_boot.code!=0xE9) {
      phys_st = s_undef;
      logic_st = s_undef;
      strcat(ErrMsg,"Reference values not found\n");
      exitnow=true;
    }

// 2b. Verify minicrate status
    if (!exitnow) { // skip checks
      logic_st = s_undef; // Not defined in boot program

      if (boot_status.EUartIRQ!=Pcref->ref_boot.EUartIRQ)
        {phys_st = s_error; strcat(ErrMsg,"EUartIRQ error\n");}
      if (boot_status.TTCi2c!=Pcref->ref_boot.TTCi2c)
        {phys_st = s_error; strcat(ErrMsg,"TTCi2c error\n");}
      if (boot_status.CpldTtc!=Pcref->ref_boot.CpldTtc)
        {phys_st = s_error; strcat(ErrMsg,"CpldTtc error\n");}
      if (!disable_qpll_check) if (boot_status.QpllArdy!=Pcref->ref_boot.QpllArdy)
        {phys_st = s_error; strcat(ErrMsg,"QpllArdy error\n");}
      if (!disable_qpll_check) if (boot_status.QpllBrdy!=Pcref->ref_boot.QpllBrdy)
        {phys_st = s_error; strcat(ErrMsg,"QpllBrdy error\n");}
      if (!disable_qpll_check) if (boot_status.QpllAChng!=Pcref->ref_boot.QpllAChng)
        {phys_st = s_error; strcat(ErrMsg,"QpllAChng error\n");}
      if (!disable_qpll_check) if (boot_status.QpllBChng!=Pcref->ref_boot.QpllBChng)
        {phys_st = s_error; strcat(ErrMsg,"QpllBChng error\n");}
      if (boot_status.TtcCk!=Pcref->ref_boot.TtcCk)
        {phys_st = s_error; strcat(ErrMsg,"TtcCk error\n");}
      if (boot_status.IUart!=Pcref->ref_boot.IUart)
        {phys_st = s_error; strcat(ErrMsg,"IUart error\n");}
      if (boot_status.EUartA!=Pcref->ref_boot.EUartA)
        {phys_st = s_error; strcat(ErrMsg,"EUartA error\n");}
      if (boot_status.EUartB!=Pcref->ref_boot.EUartB)
        {phys_st = s_error; strcat(ErrMsg,"EUartB error\n");}
      if (boot_status.ExtRam!=Pcref->ref_boot.ExtRam)
        {phys_st = s_error; strcat(ErrMsg,"ExtRam error\n");}
      if (boot_status.Flash!=Pcref->ref_boot.Flash)
        {phys_st = s_error; strcat(ErrMsg,"Flash error\n");}
//    autorun is not used for error check
      if (boot_status.TTCrxRdy!=Pcref->ref_boot.TTCrxRdy)
        {phys_st = s_error; strcat(ErrMsg,"TTCrxRdy error\n");}
      if (boot_status.LinkAutoset!=Pcref->ref_boot.LinkAutoset)
        {phys_st = s_error; strcat(ErrMsg,"LinkAutoset error\n");}

      if (boot_status.PwrRam!=Pcref->ref_boot.PwrRam)
        {phys_st = s_error; strcat(ErrMsg,"PwrRam error\n");}
      if (boot_status.ErrRam!=Pcref->ref_boot.ErrRam)
        {phys_st = s_error; strcat(ErrMsg,"ErrRam error\n");}
      if (boot_status.vErrRam!=Pcref->ref_boot.vErrRam)
        {phys_st = s_error; strcat(ErrMsg,"vErrRam error\n");}
      if (boot_status.PwrFlash!=Pcref->ref_boot.PwrFlash)
        {phys_st = s_error; strcat(ErrMsg,"PwrFlash error\n");}
      if (boot_status.FlashID!=Pcref->ref_boot.FlashID)
        {phys_st = s_error; strcat(ErrMsg,"FlashID error\n");}
      if (boot_status.LinkOfs<Pcref->ref_boot.LinkOfsMin || boot_status.LinkOfs>Pcref->ref_boot.LinkOfsMax)
        {phys_st = s_error; strcat(ErrMsg,"LinkOfs error\n");}
      if (boot_status.LinkHyst<Pcref->ref_boot.LinkHystMin || boot_status.LinkHyst>Pcref->ref_boot.LinkHystMax)
        {phys_st = s_error; strcat(ErrMsg,"LinkHyst error\n");}
      if (boot_status.LinkAmp<Pcref->ref_boot.LinkAmpMin)
        {phys_st = s_error; strcat(ErrMsg,"LinkAmp error\n");}

// Check CRC: PwrCrashFlags
      wstring(~(boot_status.PwrCrashFlags),0,Data);
      DataCrc = crc(Data,sizeof(PwrCrashFlags));
      if (boot_status.PwrCrashCrc==DataCrc) {
        PwrCrashFlags = boot_status.PwrCrashFlags & 0xFFFFFFF; // take only 28 bits
        if (PwrCrashFlags<Pcref->ref_boot.PwrCrashFlags || PwrCrashFlags>Pcref->ref_boot.PwrCrashFlags)
          {phys_st = s_error; strcat(ErrMsg,"PwrCrashFlags error\n");}
      }

// Check CRC: CrashFlags
      wstring(~(boot_status.CrashFlags),0,Data);
      DataCrc = crc(Data,sizeof(CrashFlags));
      if (boot_status.CrashCrc==DataCrc) {
        CrashFlags = boot_status.CrashFlags & 0xFF; // take only 8 bits
        if (CrashFlags<Pcref->ref_boot.CrashFlags || CrashFlags>Pcref->ref_boot.CrashFlags)
          {phys_st = s_error; strcat(ErrMsg,"CrashFlags error\n");}
      }

      if (boot_status.PwrAnalog!=Pcref->ref_boot.PwrAnalog)
        {phys_st = s_error; strcat(ErrMsg,"PwrAnalog error\n");}

      if (boot_status.Vccin<Pcref->ref_boot.VccinMin || boot_status.Vccin>Pcref->ref_boot.VccinMax)
        {phys_st = s_error; strcat(ErrMsg,"Vccin error\n");}
      if (boot_status.Vddin<Pcref->ref_boot.VddinMin || boot_status.Vddin>Pcref->ref_boot.VddinMax)
        {phys_st = s_error; strcat(ErrMsg,"Vddin error\n");}
      if (boot_status.Vcc<Pcref->ref_boot.VccMin || boot_status.Vcc>Pcref->ref_boot.VccMax)
        {phys_st = s_error; strcat(ErrMsg,"Vcc error\n");}
      if (boot_status.Vdd<Pcref->ref_boot.VddMin || boot_status.Vdd>Pcref->ref_boot.VddMax)
        {phys_st = s_error; strcat(ErrMsg,"Vdd error\n");}
      if (boot_status.Tp1L<Pcref->ref_boot.Tp1LMin || boot_status.Tp1L>Pcref->ref_boot.Tp1LMax)
        {phys_st = s_error; strcat(ErrMsg,"Tp1L error\n");}
      if (boot_status.Tp1H<Pcref->ref_boot.Tp1HMin || boot_status.Tp1H>Pcref->ref_boot.Tp1HMax)
        {phys_st = s_error; strcat(ErrMsg,"Tp1H error\n");}
      if (boot_status.Tp2L<Pcref->ref_boot.Tp2LMin || boot_status.Tp2L>Pcref->ref_boot.Tp2LMax)
        {phys_st = s_error; strcat(ErrMsg,"Tp2L error\n");}
      if (boot_status.Tp2H<Pcref->ref_boot.Tp2HMin || boot_status.Tp2H>Pcref->ref_boot.Tp2HMax)
        {phys_st = s_error; strcat(ErrMsg,"Tp2H error\n");}
    } // End of "if (!exitnow)"
  }
  else { // Neither "minicrate" nor "boot" ccbreply
    *Pmc_program = false;
    phys_st = s_undef;
    logic_st = s_undef;
    sprintf(ErrMsg,"%s\n",cmd_obj->read_error_message(mc_status.ccbserver_code));
  }


// Fill-up mc_status_check struct (except xml)
  mc_status_check.mc_program=*Pmc_program;
  *Pphys_st=mc_status_check.phys_status=phys_st;
  *Plogic_st=mc_status_check.logic_status=logic_st;
  mc_status_check.msg=mc_status.msg;
  mc_status_check.msg+="\n**********************\n";
  mc_status_check.msg+="*** STATUS SUMMARY ***\n";
  mc_status_check.msg+="**********************\n";

// Create status xml (begin)
  char statres[2048];

  sprintf(statres,"<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n");
  sprintf(statres,"%s<mc_status>\n",statres);
  sprintf(statres,"%s  <ccbid>%d</ccbid>\n",statres,cmd_obj->read_ccb_id());
  if (mc_status.code==0x13)
    sprintf(statres,"%s  <program>minicrate</program>\n",statres);
  else if (mc_status.code==0xE9)
    sprintf(statres,"%s  <program>boot/program>\n",statres);
  else
    sprintf(statres,"%s  <program>unknown/program>\n",statres);

  if (phys_st==s_ok) {
    sprintf(statres,"%s  <phys_state>ok</phys_state>\n",statres);
    mc_status_check.msg+="*** Physical state: Ok\n";
  } else if (phys_st==s_error) {
    sprintf(statres,"%s  <phys_state>error</phys_state>\n",statres);
    mc_status_check.msg+="*** Physical state: Error\n";
  } else{
    sprintf(statres,"%s  <phys_state>unknown</phys_state>\n",statres);
    mc_status_check.msg+="*** Physical state: Unknown\n";
  }

  if (mc_status.code==0x13) {
    if (logic_st==s_ok) {
      sprintf(statres,"%s  <logic_state>ok</logic_state>\n",statres);
      mc_status_check.msg+="*** Logical state: Ok\n";
    } else if (logic_st==s_error) {
      sprintf(statres,"%s  <logic_state>error</logic_state>\n",statres);
      mc_status_check.msg+="*** Logical state: Error\n";
    } else {
      sprintf(statres,"%s  <logic_state>unknown</logic_state>\n",statres);
      mc_status_check.msg+="*** Logical state: Unknown\n";
    }
  }

  sprintf(statres,"%s  <ErrMsg>%s  </ErrMsg>\n",statres,ErrMsg);
  mc_status_check.msg+="*** Messages:\n";
  mc_status_check.msg+=ErrMsg;
  mc_status_check.msg+='\n';
  mc_status_check.msg+="*** End Messages:\n";

  sprintf(statres,"%s</mc_status>\n",statres);
// Create status xml (end)

  mc_status_check.xmlmsg=statres; // Copy status xml

// Log status results
  if (mc_status.code!=0) { // We got a ccbserver reply
    if (phys_st!=s_ok) // Status: Error
      cmd_obj->ccb_log->write_log(statres,erro);
    else if (logic_st!=s_ok) // Status: Warning
      cmd_obj->ccb_log->write_log(statres,warn);
  }
}


// Reset mc_status_check struct
// Author: A.Parenti, Aug30 2007
// Modified: AP, Oct22 2007
void Cccb::MC_check_status_reset() {
  if (this==NULL) return; // Object deleted -> return

  mc_status_check.mc_program=false;
  mc_status_check.phys_status=mc_status_check.logic_status=s_undef;
  mc_status_check.switch_off_mc=mc_status_check.switch_off_fe=false;
  mc_status_check.msg=mc_status_check.xmlmsg="";
}

// Print result of "MC check Status" to stdout
// Author: A. Parenti, Aug30 2007
void Cccb::MC_check_status_print() {
  if (this==NULL) return; // Object deleted -> return

// Set font color
  if (mc_status_check.phys_status==s_error || mc_status_check.logic_status==s_error)
    std::cout << REDBKG; // Error
  else if (mc_status_check.phys_status==s_undef || mc_status_check.logic_status==s_undef)
    std::cout << YLWBKG; // Warning
  else
    std::cout << GRNBKG; // All Ok

  printf("%s",mc_status_check.msg.c_str());

// Unset font color
  std::cout << STDBKG << std::endl; // Unset font color
}


/*************************/
/* Check MC test results */
/* Firmware v1.21        */
/*************************/
// Run "Read/Run Test" command (0x10/0x12) and compare values with a reference
// Author: A.Parenti, Jul08 2005
// Modified: AP, Jun09 2006 (logging)
// AP: Tested at Cessy, Jul05 2006
// Modified: AP, Oct09 2006
void Cccb::MC_check_test(Cref *Pcref,bool run_test,Emcstatus *Pflag_tst,Emcstatus *Psb_tst,Emcstatus *Ptrb_tst,Emcstatus *Prob_tst,char *ErrMsg) {
  bool exitnow=false;
  Emcstatus flag_tst,sb_tst,trb_tst,rob_tst; // Local test results
  int i;

  if (this==NULL) return; // Object deleted -> return

  mc_firmware_version(); // Get firmware version

// Reset all
  flag_tst=sb_tst=trb_tst=rob_tst=s_ok; // All ok, till now
  strcpy(ErrMsg,"");
  MC_check_test_reset();

  if (run_test)
    MC_run_test(); // Run Test
  else
    MC_read_test(); // Read Test results

  if (mc_test.code!=0x11) {
    flag_tst=sb_tst=trb_tst=rob_tst=s_undef;
    strcpy(ErrMsg,"Wrong ccb reply\n");
    exitnow=true;
  }

  if (Pcref->ref_test.code!=0x11) {
    flag_tst=sb_tst=trb_tst=rob_tst=s_undef;
    strcat(ErrMsg,"Reference values not found\n");
    exitnow=true;
  }

  if (!exitnow && mc_test.McType!=Pcref->ref_test.McType) {
    flag_tst=sb_tst=trb_tst=rob_tst=s_undef;
    strcat(ErrMsg,"Wrong McType\n");
    exitnow=true;
  }

  if (!exitnow) {
// Check Flags
    if (mc_test.TTCFpga!=Pcref->ref_test.TTCFpga)
      {flag_tst=s_error; strcat(ErrMsg,"TTCFpga error\n");}
    if (mc_test.AnPwr!=Pcref->ref_test.AnPwr)
      {flag_tst=s_error; strcat(ErrMsg,"AnPwr error\n");}
    if (mc_test.CKPwr!=Pcref->ref_test.CKPwr)
      {flag_tst=s_error; strcat(ErrMsg,"CKPwr error\n");}
    if (mc_test.LedPwr!=Pcref->ref_test.LedPwr)
      {flag_tst=s_error; strcat(ErrMsg,"LedPwr error\n");}
    if (mc_test.RpcPwr!=Pcref->ref_test.RpcPwr)
      {flag_tst=s_error; strcat(ErrMsg,"RpcPwr error\n");}
    if (mc_test.BufTrbPwr!=Pcref->ref_test.BufTrbPwr)
      {flag_tst=s_error; strcat(ErrMsg,"BufTrbPwr error\n");}
    if (mc_test.VccTrbPwr!=Pcref->ref_test.VccTrbPwr)
      {flag_tst=s_error; strcat(ErrMsg,"VccTrbPwr error\n");}
    if (mc_test.B1w!=Pcref->ref_test.B1w)
      {flag_tst=s_error; strcat(ErrMsg,"B1w error\n");}
    if (mc_test.SOPwr!=Pcref->ref_test.SOPwr)
      {flag_tst=s_error; strcat(ErrMsg,"SOPwr error\n");}
    if (mc_test.DUPwr!=Pcref->ref_test.DUPwr)
      {flag_tst=s_error; strcat(ErrMsg,"DUPwr error\n");}
    if (mc_test.DDPwr!=Pcref->ref_test.DDPwr)
      {flag_tst=s_error; strcat(ErrMsg,"DDPwr error\n");}
    if (mc_test.SBCKPwr!=Pcref->ref_test.SBCKPwr)
      {flag_tst=s_error; strcat(ErrMsg,"SBCKPwr error\n");}
    if (mc_test.THPwr!=Pcref->ref_test.THPwr)
      {flag_tst=s_error; strcat(ErrMsg,"THPwr error\n");}
    if (mc_test.CPUdelay!=Pcref->ref_test.CPUdelay)
      {flag_tst=s_error; strcat(ErrMsg,"CPUdelay error\n");}
    if (mc_test.TPFineDelay1!=Pcref->ref_test.TPFineDelay1)
      {flag_tst=s_error; strcat(ErrMsg,"TPFineDelay1 error\n");}
    if (mc_test.TPFineDelay2!=Pcref->ref_test.TPFineDelay2)
      {flag_tst=s_error; strcat(ErrMsg,"TPFineDelay2 error\n");}

// Check Server Board
    for (i=0;i<11;i++)
      if (mc_test.OnTime[i]<Pcref->ref_test.OnTimeMin[i] || mc_test.OnTime[i]>Pcref->ref_test.OnTimeMax[i])
        {sb_tst=s_error; sprintf(ErrMsg,"%sOnTime[%d] error\n",ErrMsg,i);}
    for (i=0;i<32;i++)
      if (mc_test.AdcNoise[i]<Pcref->ref_test.AdcNoiseMin[i] || mc_test.AdcNoise[i]>Pcref->ref_test.AdcNoiseMax[i])
        {sb_tst=s_error; sprintf(ErrMsg,"%sAdcNoise[%d] error\n",ErrMsg,i);}
    if (mc_test.Dac!=Pcref->ref_test.Dac)
      {sb_tst=s_error; strcat(ErrMsg,"Dac error\n");}
    if (mc_test.SbTestJtag!=Pcref->ref_test.SbTestJtag)
      {sb_tst=s_error; strcat(ErrMsg,"SbTestJtag error\n");}
    if (mc_test.SbTestPi!=Pcref->ref_test.SbTestPi)
      {sb_tst=s_error; strcat(ErrMsg,"SbTestPi error\n");}

// Check TRBs
    if (mc_test.TrbPwr!=Pcref->ref_test.TrbPwr)
      {trb_tst=s_error; strcat(ErrMsg,"TrbPwr error\n");}
    if (mc_test.TrbBadJtagAddr!=Pcref->ref_test.TrbBadJtagAddr)
      {trb_tst=s_error; strcat(ErrMsg,"TrbBadJtagAddr error\n");}
    for (i=0;i<8;++i)
      if (mc_test.TrbPresMsk[i]!=Pcref->ref_test.TrbPresMsk[i])
        {trb_tst=s_error; sprintf(ErrMsg,"%sTrbPresMsk[%d] error\n",ErrMsg,i);}
    for (i=0;i<8;++i)
      if (mc_test.TrbTestJtag[i]!=Pcref->ref_test.TrbTestJtag[i])
        {trb_tst=s_error; sprintf(ErrMsg,"%sTrbTestJtag[%d] error\n",ErrMsg,i);}
    for (i=0;i<8;++i)
      if (mc_test.TrbFindSensor[i]!=Pcref->ref_test.TrbFindSensor[i])
        {trb_tst=s_error; sprintf(ErrMsg,"%sTrbFindSensor[%d] error\n",ErrMsg,i);}
    for (i=0;i<8;++i)
      if (mc_test.TrbOnTime[i]<Pcref->ref_test.TrbOnTimeMin[i] || mc_test.TrbOnTime[i]>Pcref->ref_test.TrbOnTimeMax[i])
        {trb_tst=s_error; sprintf(ErrMsg,"%sTrbOnTime[%d] error\n",ErrMsg,i);}
    for (i=0;i<6;++i)
      if (mc_test.TrbPiTest[i]!=Pcref->ref_test.TrbPiTest[i])
        {trb_tst=s_error; sprintf(ErrMsg,"%sTrbPiTest[%d] error\n",ErrMsg,i);}

// Check ROBs
    if (mc_test.RobPwr!=Pcref->ref_test.RobPwr)
      {rob_tst=s_error; strcat(ErrMsg,"RobPwr error\n");}
    if (mc_test.RobBadJtagAddr!=Pcref->ref_test.RobBadJtagAddr)
      {rob_tst=s_error; strcat(ErrMsg,"RobBadJtagAddr error\n");}
    if (mc_test.RobOverlapAddr!=Pcref->ref_test.RobOverlapAddr)
      {rob_tst=s_error; strcat(ErrMsg,"RobOverlapAddr error\n");}
    for (i=0;i<7;++i)
      if (mc_test.RobPresMsk[i]!=Pcref->ref_test.RobPresMsk[i])
        {rob_tst=s_error; sprintf(ErrMsg,"%sRobPresMsk[%d] error\n",ErrMsg,i);}
    for (i=0;i<7;++i)
      if (mc_test.RobTestJtag[i]!=Pcref->ref_test.RobTestJtag[i])
        {rob_tst=s_error; sprintf(ErrMsg,"%sRobTestJtag[%d] error\n",ErrMsg,i);}
    for (i=0;i<7;++i)
      if (mc_test.RobFindSensor[i]!=Pcref->ref_test.RobFindSensor[i])
        {rob_tst=s_error; sprintf(ErrMsg,"%sRobFindSensor[%d] error\n",ErrMsg,i);}

    if (HVer<1 || (HVer==1 && LVer<15)) { // Firmware < v1.15
      for (i=0;i<7;++i)
        if (mc_test.RobOnTime[i]<Pcref->ref_test.RobOnTimeMin[i] || mc_test.RobOnTime[i]>Pcref->ref_test.RobOnTimeMax[i])
          {rob_tst=s_error; sprintf(ErrMsg,"%sRobOnTime[%d] error\n",ErrMsg,i);}
    }
    else { // Firmware >= v1.15: RobOnTime returned in 0.1 ms 
      for (i=0;i<7;++i)
        if (mc_test.RobOnTime[i]<(10*Pcref->ref_test.RobOnTimeMin[i]) || mc_test.RobOnTime[i]>(10*Pcref->ref_test.RobOnTimeMax[i]))
          {rob_tst=s_error; sprintf(ErrMsg,"%sRobOnTime[%d] error\n",ErrMsg,i);}
    }
  }


// Fill-up mc_test_check struct (except xml)
  *Pflag_tst=mc_test_check.Flags=flag_tst;
  *Psb_tst=mc_test_check.SrvBrd=sb_tst;
  *Ptrb_tst=mc_test_check.TrgBrd=trb_tst;
  *Prob_tst=mc_test_check.RoBrd=rob_tst;
  mc_test_check.msg=mc_test.msg;
  mc_test_check.msg+="\n********************\n";
  mc_test_check.msg+="*** TEST SUMMARY ***\n";
  mc_test_check.msg+="********************\n";

// Create test xml (begin)
  char testres[4096];

  sprintf(testres,"<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n");
  sprintf(testres,"%s<mc_test>\n",testres);
  sprintf(testres,"%s  <ccbid>%d</ccbid>\n",testres,cmd_obj->read_ccb_id());

  if (flag_tst==s_ok) {
    sprintf(testres,"%s  <flags_test>ok</flags_test>\n",testres);
    mc_test_check.msg+="Flags test: Ok\n";
  } else if (flag_tst==s_error) {
    sprintf(testres,"%s  <flags_test>error</flags_test>\n",testres);
    mc_test_check.msg+="Flags test: Error\n";
  } else {
    sprintf(testres,"%s  <flags_test>unknown</flags_test>\n",testres);
    mc_test_check.msg+="Flags test: Unknown\n";
  }

  if (sb_tst==s_ok) {
    sprintf(testres,"%s  <sb_test>ok</sb_test>\n",testres);
    mc_test_check.msg+="Server Board test: Ok\n";
  } else if (sb_tst==s_error) {
    sprintf(testres,"%s  <sb_test>error</sb_test>\n",testres);
    mc_test_check.msg+="Server Board test: Error\n";
  } else {
    sprintf(testres,"%s  <sb_test>unknown</sb_test>\n",testres);
    mc_test_check.msg+="Server Board test: Unknown\n";
  }

  if (trb_tst==s_ok) {
    sprintf(testres,"%s  <trb_test>ok</trb_test>\n",testres);
    mc_test_check.msg+="TRB test: Ok\n";
  } else if (trb_tst==s_error) {
    sprintf(testres,"%s  <trb_test>error</trb_test>\n",testres);
    mc_test_check.msg+="TRB test: Error\n";
  } else {
    sprintf(testres,"%s  <trb_test>unknown</trb_test>\n",testres);
    mc_test_check.msg+="TRB test: Unknown\n";
  }

  if (rob_tst==s_ok) {
    sprintf(testres,"%s  <rob_test>ok</rob_test>\n",testres);
    mc_test_check.msg+="ROB test: Ok\n";
  } else if (rob_tst==s_error) {
    sprintf(testres,"%s  <rob_test>error</rob_test>\n",testres);
    mc_test_check.msg+="ROB test: Error\n";
  } else {
    sprintf(testres,"%s  <rob_test>unknown</rob_test>\n",testres);
    mc_test_check.msg+="ROB test: Unknown\n";
  }

  sprintf(testres,"%s  <ErrMsg>%s  </ErrMsg>\n",testres,ErrMsg);
  mc_test_check.msg+="*** Messages:\n";
  mc_test_check.msg+=ErrMsg;
  mc_test_check.msg+='\n';
  mc_test_check.msg+="*** End Messages:\n";

  sprintf(testres,"%s</mc_test>\n",testres);
// Create test xml (end)

  mc_test_check.xmlmsg=testres; // Copy test xml

// Log test results
  if (mc_test.code!=0) { // We got a ccbserver reply

    if (flag_tst==s_error || sb_tst==s_error || trb_tst==s_error || rob_tst==s_error)
      cmd_obj->ccb_log->write_log(testres,erro); // Test: error
    else if (flag_tst==s_undef || sb_tst==s_undef || trb_tst==s_undef || rob_tst==s_undef)
      cmd_obj->ccb_log->write_log(testres,warn); // Test: warning

  }
}


// Reset mc_test_check struct
// Author: A.Parenti, Aug31 2007
void Cccb::MC_check_test_reset() {
  if (this==NULL) return; // Object deleted -> return

  mc_test_check.Flags=mc_test_check.SrvBrd=s_undef;
  mc_test_check.TrgBrd=mc_test_check.RoBrd=s_undef;
  mc_test_check.msg=mc_test_check.xmlmsg="";
}


// Print result of "MC check Test" to stdout
// Author: A. Parenti, Aug31 2007
void Cccb::MC_check_test_print() {
  if (this==NULL) return; // Object deleted -> return

// Set font color
  if (mc_test_check.Flags==s_error || mc_test_check.SrvBrd==s_error || mc_test_check.TrgBrd==s_error || mc_test_check.RoBrd==s_error)
    std::cout << REDBKG; // Error
  else if (mc_test_check.Flags==s_undef || mc_test_check.SrvBrd==s_undef || mc_test_check.TrgBrd==s_undef || mc_test_check.RoBrd==s_undef)
    std::cout << YLWBKG; // Warning
  else
    std::cout << GRNBKG; // All Ok

  printf("%s",mc_test_check.msg.c_str());

// Unset font color
  std::cout << STDBKG << std::endl; // Unset font color
}



/******************************************/
/* "Save MC configuration" (0x40) command */
/******************************************/

// Send "Save MC configuration" (0x40) command and decode reply
// Author:  A.Parenti Jan06 2005
// Modified: AP, Mar24 2005
// Tested at PD, Mar25 2005
void Cccb::save_MC_conf() {
  const unsigned char command_code=0x40;
  unsigned char command_line[1];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

  mc_firmware_version(); // Get firmware version

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;

  save_MC_conf_reset(); // Reset save_mc_conf struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1,SAVE_MC_CONF_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = save_MC_conf_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    save_mc_conf.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nSave MC configuration (0x40): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}

// Reset save_mc_conf struct
// Author: A.Parenti Aug03 2006
// Modified: AP, Dec07 2006
void Cccb::save_MC_conf_reset() {
  if (this==NULL) return; // Object deleted -> return

  save_mc_conf.code1 = save_mc_conf.code2 = 0;
  save_mc_conf.error_old = save_mc_conf.error = 0;

  save_mc_conf.ccbserver_code=-5;
  save_mc_conf.msg="";
}


// Decode "Save MC configuration" (0x40) reply
// Author:  A.Parenti Feb11 2005
// Modified: AP, Dec12 2006
int Cccb::save_MC_conf_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x40, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&save_mc_conf.code1);
  idx = rstring(rdstr,idx,&save_mc_conf.code2);

// Fill-up the "result" structure
  if (HVer<1 || (HVer==1 && LVer<17)) { // Firmware < v1.17
    idx = rstring(rdstr,idx,&save_mc_conf.error_old);
  } else { // Firmware >= v1.17
    idx = rstring(rdstr,idx,&save_mc_conf.error);
  }

// Fill-up the error code
  if (save_mc_conf.code1==CCB_BUSY_CODE) // ccb is busy
    save_mc_conf.ccbserver_code = -1;
  else if (save_mc_conf.code1==CCB_UNKNOWN1 && save_mc_conf.code2==CCB_UNKNOWN2) // ccb: unknown command
    save_mc_conf.ccbserver_code = 1;
  else if (save_mc_conf.code1==right_reply && save_mc_conf.code2==command_code) // ccb: right reply
    save_mc_conf.ccbserver_code = 0;
  else // wrong ccb reply
    save_mc_conf.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  save_mc_conf.msg="\nSave MC configuration (0x40)\n";

  if (save_mc_conf.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(save_mc_conf.ccbserver_code)); save_mc_conf.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",save_mc_conf.code1); save_mc_conf.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",save_mc_conf.code2); save_mc_conf.msg+=tmpstr;
    if (HVer<1 || (HVer==1 && LVer<17)) { // Firmware < v1.17
      sprintf(tmpstr,"Result: %s\n", (save_mc_conf.error_old==0)?"Success":"Error"); save_mc_conf.msg+=tmpstr;
    } else { // Firmware >= v1.17
      if (save_mc_conf.error==0) {
        save_mc_conf.msg+="Result: Success.\n";
      } else if (save_mc_conf.error==-1) {
        save_mc_conf.msg+="Result: Error writing data to FLASH.\n";
      } else if (save_mc_conf.error==-2) {
        save_mc_conf.msg+="Result: Power FLASH fail.\n";
      } else if (save_mc_conf.error==-5) {
        save_mc_conf.msg+="Result: Invalid mode. Software error.\n";
      } else if (save_mc_conf.error==-7) {
        save_mc_conf.msg+="Result: Invalid update mode. Software error.\n";
      } else {
        sprintf(tmpstr,"Error: code %d.\n",save_mc_conf.error); save_mc_conf.msg+=tmpstr;
      }
    }
  }

  return idx;
}


// Print results to standard output
// Author:  A.Parenti Jan06 2005
// Modified: AP, Dec07 2006
void Cccb::save_MC_conf_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",save_mc_conf.msg.c_str());
}



/******************************************/
/* "Load MC configuration" (0x61) command */
/******************************************/

// Send "Load MC configuration" (0x61) command and decode reply
// Author:  A.Parenti Jan12 2005
// Modified: AP, Dec05 2006
void Cccb::load_MC_conf() {
  const unsigned char command_code=0x61;
  unsigned char command_line[1];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

  mc_firmware_version(); // Get firmware version

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;

  load_MC_conf_reset(); // Reset load_mc_conf struct
// Send the command line
  printf("\nLoading MC configuration (0x61): it may take ~1 minute\n");
  send_command_result = cmd_obj->send_command(command_line,1,LOAD_MC_CONF_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = load_MC_conf_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    load_mc_conf.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nLoad MC configuration (0x61): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}

// Reset load_mc_conf struct
// Author: A.Parenti Aug03 2006
// Modified: AP, Dec07 2006
void Cccb::load_MC_conf_reset() {
  if (this==NULL) return; // Object deleted -> return

  load_mc_conf.code1 = load_mc_conf.code2 = 0;
  load_mc_conf.error_old = load_mc_conf.error = 0;;

  load_mc_conf.ccbserver_code=-5;
  load_mc_conf.msg="";
}


// Decode "Load MC configuration" (0x61) reply
// Author:  A.Parenti Feb11 2005
// Modified: AP, Dec12 2006
int Cccb::load_MC_conf_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x61, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&load_mc_conf.code1);
  idx = rstring(rdstr,idx,&load_mc_conf.code2);

// Fill-up the "result" structure
  if (HVer<1 || (HVer==1 && LVer<17)) { // Firmware < v1.17
    idx = rstring(rdstr,idx,&load_mc_conf.error_old);
  } else { // Firmware >= v1.17
    idx = rstring(rdstr,idx,&load_mc_conf.error);
  }

// Fill-up the error code
  if (load_mc_conf.code1==CCB_BUSY_CODE) // ccb is busy
    load_mc_conf.ccbserver_code = -1;
  else if (load_mc_conf.code1==CCB_UNKNOWN1 && load_mc_conf.code2==CCB_UNKNOWN2) // ccb: unknown command
    load_mc_conf.ccbserver_code = 1;
  else if (load_mc_conf.code1==right_reply && load_mc_conf.code2==command_code) // ccb: right reply
    load_mc_conf.ccbserver_code = 0;
  else // wrong ccb reply
    load_mc_conf.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  load_mc_conf.msg="\nLoad MC configuration (0x61)\n";

  if (load_mc_conf.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(load_mc_conf.ccbserver_code)); load_mc_conf.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n", load_mc_conf.code1); load_mc_conf.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n", load_mc_conf.code2); load_mc_conf.msg+=tmpstr;
    if (HVer<1 || (HVer==1 && LVer<17)) { // Firmware < v1.17
      sprintf(tmpstr,"Result: %s\n", (load_mc_conf.error_old==0)?"Success":"Error"); load_mc_conf.msg+=tmpstr;
    } else { // Firmware >= v1.17
      if (load_mc_conf.error==0) {
        load_mc_conf.msg+="Result: Success.\n";
      } else if (load_mc_conf.error==-2) {
        load_mc_conf.msg+="Result: Power FLASH fail.\n";
      } else if (load_mc_conf.error==-3) {
        load_mc_conf.msg+="Result: Invalid file size. File not found.\n";
      } else if (load_mc_conf.error==-4) {
        load_mc_conf.msg+="Result: CRC error, data corrupted.\n";
      } else if (load_mc_conf.error==-5) {
        load_mc_conf.msg+="Result: Invalid mode. Software error.\n";
      } else if (load_mc_conf.error==-6) {
        load_mc_conf.msg+="Result: Read error. Software error.\n";
      } else if (load_mc_conf.error==-7) {
        load_mc_conf.msg+="Result: Invalid update mode. Software error.\n";
      } else if (load_mc_conf.error==-8) {
        load_mc_conf.msg+="Result: Invalid file version. Data saved with old version format.\n";
      } else if (load_mc_conf.error==-9) {
        load_mc_conf.msg+="Result: Board changed. Invalid sensor code.\n";
      } else if (load_mc_conf.error==-32767) { // 0x8001
        load_mc_conf.msg+="Result: Error writing data on chip register.\n";
      } else if (load_mc_conf.error==1) {
        load_mc_conf.msg+="Result: Error writing data on front-end by I2C bus or TTCrx I2C bus.\n";
      } else if (load_mc_conf.error==0xFF) {
        load_mc_conf.msg+="Result: SDA short circuit on front-ends I2C bus or TTCrx I2C bus.\n";
      } else if (load_mc_conf.error==0xFE) {
        load_mc_conf.msg+="Result: SCL short circuit on front-ends I2C bus or TTCrx I2C bus.\n";
      } else {
        sprintf(tmpstr,"Error: code %d.\n",load_mc_conf.error); load_mc_conf.msg+=tmpstr;
      }
    }
  }

  return idx;
}



// Print results to standard output
// Author:  A.Parenti Jan12 2005
// Modified: AP, Dec07 2006
void Cccb::load_MC_conf_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",load_mc_conf.msg.c_str());
}


/*********************************************/
/* "Restore MC configuration" (0x96) command */
/*********************************************/

// Send "Restore MC configuration" (0x96) command and decode reply
// Author:  A.Parenti May09 2005
// Modified: AP, Dec13 2006
void Cccb::restore_MC_conf() {
  const unsigned char command_code=0x96;
  unsigned char command_line[1];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

  mc_firmware_version(); // Get firmware version
  restore_MC_conf_reset(); // Reset restore_mc_conf struct
  if (HVer!=1 || (HVer==1 && LVer<8) || (HVer==1 && LVer>16)) { // Firmware < v1.8 || > v.16
    char tmpstr[200];
    sprintf(tmpstr,"Attention: command %#X supported between v1.8 and v1.16 (actual: v%d.%d)\nFrom v1.17 this commande is included in \"Load MC configuration\" (0x61) command\n",command_code,HVer,LVer);
    restore_mc_conf.msg=tmpstr;
    std::cout << tmpstr;
    return;
  }

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;

// Send the command line
  printf("\nRestoring MC configuration (0x96): it may take ~1 minute\n");
  send_command_result = cmd_obj->send_command(command_line,1,RESTORE_MC_CONF_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = restore_MC_conf_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    restore_mc_conf.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nRestore MC configuration (0x96): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}

// Reset restore_mc_conf struct
// Author: A.Parenti Aug03 2006
// Modified: AP, Dec07 2006
void Cccb::restore_MC_conf_reset() {
  if (this==NULL) return; // Object deleted -> return

  restore_mc_conf.code1 = restore_mc_conf.code2 = restore_mc_conf.result = 0;

  restore_mc_conf.ccbserver_code=-5;
  restore_mc_conf.msg="";
}


// Decode "Restore MC configuration" (0x96) reply
// Author:  A.Parenti May09 2005
// Modified: AP, Dec07 2006
int Cccb::restore_MC_conf_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x96, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&restore_mc_conf.code1);
  idx = rstring(rdstr,idx,&restore_mc_conf.code2);

// Fill-up the "result" structure
  idx = rstring(rdstr,idx,&restore_mc_conf.result);

// Fill-up the error code
  if (restore_mc_conf.code1==CCB_BUSY_CODE) // ccb is busy
    restore_mc_conf.ccbserver_code = -1;
  else if (restore_mc_conf.code1==CCB_UNKNOWN1 && restore_mc_conf.code2==CCB_UNKNOWN2) // ccb: unknown command
    restore_mc_conf.ccbserver_code = 1;
  else if (restore_mc_conf.code1==right_reply && restore_mc_conf.code2==command_code) // ccb: right reply
    restore_mc_conf.ccbserver_code = 0;
  else // wrong ccb reply
    restore_mc_conf.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  restore_mc_conf.msg="\nRestore MC configuration (0x96)\n";

  if (restore_mc_conf.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(restore_mc_conf.ccbserver_code)); restore_mc_conf.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",restore_mc_conf.code1); restore_mc_conf.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",restore_mc_conf.code2); restore_mc_conf.msg+=tmpstr;
    sprintf(tmpstr,"Result: %s\n",(restore_mc_conf.result==1)?"Success":"Error"); restore_mc_conf.msg+=tmpstr;
  }

  return idx;
}


// Print results to standard output
// Author:  A.Parenti May09 2005
// Modified: AP, Dec07 2006
void Cccb::restore_MC_conf_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",restore_mc_conf.msg.c_str());
}



/*************************/
/* "Exit" (0xA5) command */
/*************************/

// Send "Exit" (0xA5) command and decode reply
// Author:  A.Parenti Jan10 2005
// Modified: AP, Mar24 2005
// Tested at PD, Mar25 2005
void Cccb::exit_MC() {
  const unsigned char command_code=0xA5;
  unsigned char command_line[1];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;

  exit_MC_reset(); // Reset exit_mc struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1,EXIT_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = exit_MC_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    exit_mc.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nExit (0xA5): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}

// Reset exit_mc struct
// Author: A.Parenti Aug03 2006
// Modified: AP, Dec07 2006
void Cccb::exit_MC_reset() {
  if (this==NULL) return; // Object deleted -> return

  exit_mc.code1 = exit_mc.code2 = 0;

  exit_mc.ccbserver_code=-5;
  exit_mc.msg="";
}


// Decode "Exit" (0xA5) reply
// Author:  A.Parenti Feb11 2005
// Modified: AP, Dec07 2006
int Cccb::exit_MC_decode(unsigned char *rdstr) {
  const unsigned char command_code=0xA5, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&exit_mc.code1);
  idx = rstring(rdstr,idx,&exit_mc.code2);

// Fill-up the error code
  if (exit_mc.code1==CCB_BUSY_CODE) // ccb is busy
    exit_mc.ccbserver_code = -1;
  else if (exit_mc.code1==CCB_UNKNOWN1 && exit_mc.code2==CCB_UNKNOWN2) // ccb: unknown command
    exit_mc.ccbserver_code = 1;
  else if (exit_mc.code1==right_reply && exit_mc.code2==command_code) // ccb: right reply
    exit_mc.ccbserver_code = 0;
  else // wrong ccb reply
    exit_mc.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  exit_mc.msg="\nExit (0xA5)\n";

  if (exit_mc.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(exit_mc.ccbserver_code)); exit_mc.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",exit_mc.code1); exit_mc.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",exit_mc.code2); exit_mc.msg+=tmpstr;
  }

  return idx;
}

// Print results to standard output
// Author:  A.Parenti Jan10 2005
// Modified: AP, Dec07 2006
void Cccb::exit_MC_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",exit_mc.msg.c_str());
}


/***********************************/
/* "Watchdog Reset" (0xF8) command */
/***********************************/

// Send "Watchdog Reset" (0xF8) command and decode reply
// Author:  A.Parenti Jan10 2005
// Modified: AP, Mar24 2005
// Tested at PD, Mar25 2005
void Cccb::restart_CCB() {
  const unsigned char command_code=0xF8;
  unsigned char command_line[1];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;

  restart_CCB_reset(); // Reset restart_ccb struct

// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1,RESTART_CCB_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = restart_CCB_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    restart_ccb.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nWatchdog Reset (0xF8): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}

// Reset restart_ccb struct
// Author: A.Parenti Aug03 2006
// Modified: AP, Dec07 2006
void Cccb::restart_CCB_reset() {
  if (this==NULL) return; // Object deleted -> return

  restart_ccb.code1 = restart_ccb.code2 = 0;

  restart_ccb.ccbserver_code=-5;
  restart_ccb.msg="";
}


// Decode "Watchdog Reset" (0xF8) reply
// Author:  A.Parenti Feb11 2005
// Modified: AP, Dec07 2006
int Cccb::restart_CCB_decode(unsigned char *rdstr) {
  const unsigned char command_code=0xF8, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&restart_ccb.code1);
  idx = rstring(rdstr,idx,&restart_ccb.code2);

// Fill-up the error code
  if (restart_ccb.code1==CCB_BUSY_CODE) // ccb is busy
    restart_ccb.ccbserver_code = -1;
  else if (restart_ccb.code1==CCB_UNKNOWN1 && restart_ccb.code2==CCB_UNKNOWN2) // ccb: unknown command
    restart_ccb.ccbserver_code = 1;
  else if (restart_ccb.code1==right_reply && restart_ccb.code2==command_code) // ccb: right reply
    restart_ccb.ccbserver_code = 0;
  else // wrong ccb reply
    restart_ccb.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  restart_ccb.msg="\nWatchdog Reset (0xF8)\n";

  if (restart_ccb.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(restart_ccb.ccbserver_code)); restart_ccb.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",restart_ccb.code1); restart_ccb.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",restart_ccb.code2); restart_ccb.msg+=tmpstr;
  }

  return idx;
}


// Print results to standard output
// Author:  A.Parenti Jan10 2005
// Modified: AP, Dec07 2006
void Cccb::restart_CCB_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",restart_ccb.msg.c_str());
}


/*******************************************/
/* "Read CPU address space" (0xF5) command */
/*******************************************/

// Send "Read CPU address space" (0xF5) command and decode reply
// Author:  A.Parenti, Nov02 2007
void Cccb::read_CPU_addr(char page,short addr,short size) {
  const unsigned char command_code=0xF5;
  unsigned char command_line[7];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// Check "size"
  if (size>READ_CPU_ADDR_MAX_SIZE)
    size=READ_CPU_ADDR_MAX_SIZE;
  else if (size<1)
    size=1;

// create the command line
  command_line[0]=command_code;
  command_line[1]=page;
  wstring(addr,2,command_line);
  command_line[4]=0; // sizeh is always 0
  wstring(size,5,command_line);

  read_CPU_addr_reset(); // Reset rd_cpu_addr struct
  rd_cpu_addr.size=size; // Save data size

// Send the command line
  send_command_result = cmd_obj->send_command(command_line,7,RD_CPU_ADDR_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = read_CPU_addr_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    rd_cpu_addr.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nRead CPU address space (0xF5): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}

// Reset rd_cpu_addr struct
// Author: A.Parenti Nov02 2007
void Cccb::read_CPU_addr_reset() {
  if (this==NULL) return; // Object deleted -> return

  int i;

  rd_cpu_addr.code = rd_cpu_addr.page = rd_cpu_addr.size = 0;
  for (i=0;i<READ_CPU_ADDR_MAX_SIZE;++i)
    rd_cpu_addr.data[i]=0;
  rd_cpu_addr.addr=0;

  rd_cpu_addr.ccbserver_code=-5;
  rd_cpu_addr.msg="";
}


// Decode "Read CPU address space" (0xF5) reply
// Author:  A.Parenti, Nov02 2007
int Cccb::read_CPU_addr_decode(unsigned char *rdstr) {
  const unsigned char right_reply=0xF4;
  unsigned char code2;
  int idx=0, i;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&rd_cpu_addr.code);
  code2 = rdstr[idx];

// Fill-up the "result" structure
  idx = rstring(rdstr,idx,&rd_cpu_addr.page);
  idx = rstring(rdstr,idx,&rd_cpu_addr.addr);
  for (i=0;i<rd_cpu_addr.size;++i)
    idx = rstring(rdstr,idx,&(rd_cpu_addr.data[i]));

// Fill-up the error code
  if (rd_cpu_addr.code==CCB_BUSY_CODE) // ccb is busy
    rd_cpu_addr.ccbserver_code = -1;
  else if (rd_cpu_addr.code==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    rd_cpu_addr.ccbserver_code = 1;
  else if (rd_cpu_addr.code==right_reply) // ccb: right reply
    rd_cpu_addr.ccbserver_code = 0;
  else // wrong ccb reply
    rd_cpu_addr.ccbserver_code = 2;

// Fill-up message
  char tmpstr[1024];

  rd_cpu_addr.msg="\nRead CPU address space (0xF5)\n";

  if (rd_cpu_addr.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(rd_cpu_addr.ccbserver_code)); rd_cpu_addr.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code: %#2X\n",rd_cpu_addr.code); rd_cpu_addr.msg+=tmpstr;
    sprintf(tmpstr,"page: %#2X\n",rd_cpu_addr.page); rd_cpu_addr.msg+=tmpstr;
    sprintf(tmpstr,"addr: %#4X\n",rd_cpu_addr.addr); rd_cpu_addr.msg+=tmpstr;
    sprintf(tmpstr,"size: %d\n",rd_cpu_addr.size); rd_cpu_addr.msg+=tmpstr;
    array_to_string(rd_cpu_addr.data,rd_cpu_addr.size,tmpstr); rd_cpu_addr.msg+=tmpstr; rd_cpu_addr.msg+='\n';
  }

  return idx;
}


// Print results to standard output
// Author:  A.Parenti, Nov02 2007
void Cccb::read_CPU_addr_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",rd_cpu_addr.msg.c_str());
}



/************************************/
/* "Run in Progress" (0x46) command */
/************************************/

// Send "Run in Progress" (0x46) command and decode reply
// Author:  A.Parenti Jan16 2005
// Modified: AP, Mar24 2005
// Tested at PD, Mar29 2005
void Cccb::run_in_progress(char on) {
  const unsigned char command_code=0x46;
  unsigned char command_line[2];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=on;

  run_in_progress_reset(); // Reset run_in_prog struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,2,RUN_IN_PROG_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = run_in_progress_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    run_in_prog.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nRun in Progress (0x46): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}

// Reset run_in_prog struct
// Author: A.Parenti Aug03 2006
// Modified: AP, Dec07 2006
void Cccb::run_in_progress_reset() {
  if (this==NULL) return; // Object deleted -> return

  run_in_prog.code1 = run_in_prog.code2 = 0;

  run_in_prog.ccbserver_code=-5;
  run_in_prog.msg="";
}


// Decode "Run in Progress" (0x46) reply
// Author:  A.Parenti Feb11 2005
// Modified: AP, Dec07 2006
int Cccb::run_in_progress_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x46, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&run_in_prog.code1);
  idx = rstring(rdstr,idx,&run_in_prog.code2);

// Fill-up the error code
  if (run_in_prog.code1==CCB_BUSY_CODE) // ccb is busy
    run_in_prog.ccbserver_code = -1;
  else if (run_in_prog.code1==CCB_UNKNOWN1 && run_in_prog.code2==CCB_UNKNOWN2) // ccb: unknown command
    run_in_prog.ccbserver_code = 1;
  else if (run_in_prog.code1==right_reply && run_in_prog.code2==command_code) // ccb: right reply
    run_in_prog.ccbserver_code = 0;
  else // wrong ccb reply
    run_in_prog.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  run_in_prog.msg="\nRun in Progress (0x46)\n";

  if (run_in_prog.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(run_in_prog.ccbserver_code)); run_in_prog.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",run_in_prog.code1); run_in_prog.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",run_in_prog.code2); run_in_prog.msg+=tmpstr;
  }

  return idx;
}



// Print results to standard output
// Author:  A.Parenti Jan16 2005
// Modified: AP, Dec07 2006
void Cccb::run_in_progress_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",run_in_prog.msg.c_str());
}


/*********************************************/
/* "Read Communication Error" (0xF0) command */
/*********************************************/

// Send "Read Communication Error" (0xF0) command and decode reply
// Author:  A.Parenti Jan17 2005
// Modified: AP, Mar24 2005
// Tested at PD, Mar25 2005
void Cccb::read_comm_err() {
  const unsigned char command_code=0xF0;
  unsigned char command_line[1];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;

  read_comm_err_reset(); // Reset mc_read_comm_err struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1,READ_COMM_ERR_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = read_comm_err_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    mc_read_comm_err.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nRead Communication Error (0xF0): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}

// Reset mc_read_comm_err struct
// Author: A.Parenti Aug03 2006
// Modified: AP, Dec07 2006
void Cccb::read_comm_err_reset() {
  if (this==NULL) return; // Object deleted -> return

  mc_read_comm_err.code = mc_read_comm_err.port = 0;
  mc_read_comm_err.Parity = mc_read_comm_err.Framing = 0;
  mc_read_comm_err.Break = mc_read_comm_err.Noise = 0;
  mc_read_comm_err.Overrun = mc_read_comm_err.Sync = 0;
  mc_read_comm_err.Crc = mc_read_comm_err.LoseData = 0;
  mc_read_comm_err.Size = mc_read_comm_err.TimeOut = 0;
  mc_read_comm_err.Unexpected = mc_read_comm_err.BufferOverflow = 0;
  mc_read_comm_err.BufferEmpty = mc_read_comm_err.BufferTooSmall =0;

  mc_read_comm_err.ccbserver_code=-5;
  mc_read_comm_err.msg="";
}


// Decode "Read Communication Error" (0xF0) reply
// Author:  A.Parenti Feb11 2005
// Modified: AP, Dec07 2006
int Cccb::read_comm_err_decode(unsigned char *rdstr) {
  const unsigned char right_reply=0xF0;
  unsigned char code2, flags;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&mc_read_comm_err.code);
  code2 = rdstr[idx];

// Fill-up the "result" structure
  idx = rstring(rdstr,idx,&mc_read_comm_err.port);

  idx = rstring(rdstr,idx,&flags);
  mc_read_comm_err.Parity         = takebit(flags,0);
  mc_read_comm_err.Framing        = takebit(flags,1);
  mc_read_comm_err.Break          = takebit(flags,2);
  mc_read_comm_err.Noise          = takebit(flags,3);
  mc_read_comm_err.Overrun        = takebit(flags,4);
  mc_read_comm_err.Sync           = takebit(flags,5);
  mc_read_comm_err.Crc            = takebit(flags,6);
  mc_read_comm_err.LoseData       = takebit(flags,7);
  idx = rstring(rdstr,idx,&flags);
  mc_read_comm_err.Size           = takebit(flags,0);
  mc_read_comm_err.TimeOut        = takebit(flags,1);
  mc_read_comm_err.Unexpected     = takebit(flags,2);
  mc_read_comm_err.BufferOverflow = takebit(flags,3);
  mc_read_comm_err.BufferEmpty    = takebit(flags,4);
  mc_read_comm_err.BufferTooSmall = takebit(flags,5);

// Fill-up the error code
  if (mc_read_comm_err.code==CCB_BUSY_CODE) // ccb is busy
    mc_read_comm_err.ccbserver_code = -1;
  else if (mc_read_comm_err.code==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    mc_read_comm_err.ccbserver_code = 1;
  else if (mc_read_comm_err.code==right_reply) // ccb: right reply
    mc_read_comm_err.ccbserver_code = 0;
  else // wrong ccb reply
    mc_read_comm_err.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  mc_read_comm_err.msg="\nRead Communication Error (0xF0)\n";

  if (mc_read_comm_err.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(mc_read_comm_err.ccbserver_code)); mc_read_comm_err.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code: %#2X\n", mc_read_comm_err.code); mc_read_comm_err.msg+=tmpstr;
    sprintf(tmpstr,"port: %d (1=primary, 2=secondary)\n", mc_read_comm_err.port); mc_read_comm_err.msg+=tmpstr;

    sprintf(tmpstr,"Parity: %d\n",mc_read_comm_err.Parity); mc_read_comm_err.msg+=tmpstr;
    sprintf(tmpstr,"Framing: %d\n",mc_read_comm_err.Framing); mc_read_comm_err.msg+=tmpstr;
    sprintf(tmpstr,"Break: %d\n",mc_read_comm_err.Break); mc_read_comm_err.msg+=tmpstr;
    sprintf(tmpstr,"Noise: %d\n",mc_read_comm_err.Noise); mc_read_comm_err.msg+=tmpstr;
    sprintf(tmpstr,"Overrun: %d\n",mc_read_comm_err.Overrun); mc_read_comm_err.msg+=tmpstr;
    sprintf(tmpstr,"Sync: %d\n",mc_read_comm_err.Sync); mc_read_comm_err.msg+=tmpstr;
    sprintf(tmpstr,"Crc: %d\n",mc_read_comm_err.Crc); mc_read_comm_err.msg+=tmpstr;
    sprintf(tmpstr,"LoseData: %d\n",mc_read_comm_err.LoseData); mc_read_comm_err.msg+=tmpstr;
    sprintf(tmpstr,"Size: %d\n",mc_read_comm_err.Size); mc_read_comm_err.msg+=tmpstr;
    sprintf(tmpstr,"TimeOut: %d\n",mc_read_comm_err.TimeOut); mc_read_comm_err.msg+=tmpstr;
    sprintf(tmpstr,"Unexpected: %d\n",mc_read_comm_err.Unexpected); mc_read_comm_err.msg+=tmpstr;
    sprintf(tmpstr,"BufferOverflow: %d\n",mc_read_comm_err.BufferOverflow); mc_read_comm_err.msg+=tmpstr;
    sprintf(tmpstr,"BufferEmpty: %d\n",mc_read_comm_err.BufferEmpty); mc_read_comm_err.msg+=tmpstr;
    sprintf(tmpstr,"BufferTooSmall: %d\n",mc_read_comm_err.BufferTooSmall); mc_read_comm_err.msg+=tmpstr;
  }

  return idx;
}


// Print results to standard output
// Author:  A.Parenti Jan17 2005
// Modified: AP, Dec07 2006
void Cccb::read_comm_err_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",mc_read_comm_err.msg.c_str());
}


/**********************************/
/* "Auto Set Link" (0x74) command */
/**********************************/

// Send "Auto Set Link" (0x74) command and decode reply
// Author:  A.Parenti Jan17 2005
// Modified: AP, Mar24 2005
// Tested at PD, Mar25 2005
void Cccb::auto_set_LINK() {
  const unsigned char command_code=0x74;
  unsigned char command_line[1];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;

  auto_set_LINK_reset(); // Reset auto_set_link struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1,AUTO_SET_LINK_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = auto_set_LINK_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
  {
    cmd_obj->use_secondary_port();
    send_command_result = cmd_obj->send_command(command_line,1,AUTO_SET_LINK_TIMEOUT);
    if (send_command_result>=0)  
    {
      cmd_obj->use_primary_port();
      idx = auto_set_LINK_decode(cmd_obj->data_read); // Decode reply
     }
     else  auto_set_link.ccbserver_code = send_command_result;
   }

  if (_verbose_)
    printf("\nAuto Set Link (0x74): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}

// Reset auto_set_link struct
// Author: A.Parenti Aug03 2006
// Modified: AP, Dec07 2006
void Cccb::auto_set_LINK_reset() {
  if (this==NULL) return; // Object deleted -> return

  auto_set_link.code = 0;
  auto_set_link.Offset = auto_set_link.Hyst = 0;
  auto_set_link.Apl = auto_set_link.Thr = 0;

  auto_set_link.ccbserver_code=-5;
  auto_set_link.msg="";
}


// Decode "Auto Set Link" (0x74) reply
// Author:  A.Parenti Feb11 2005
// Modified: AP, Dec07 2006
int Cccb::auto_set_LINK_decode(unsigned char *rdstr) {
  const unsigned char right_reply=0x75;
  unsigned char code2;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&auto_set_link.code);
  code2 = rdstr[idx];

// Fill-up the "result" structure
  idx = rstring(rdstr,idx,&auto_set_link.Offset);
  idx = rstring(rdstr,idx,&auto_set_link.Hyst);
  idx = rstring(rdstr,idx,&auto_set_link.Apl);
  idx = rstring(rdstr,idx,&auto_set_link.Thr);

// Fill-up the error code
  if (auto_set_link.code==CCB_BUSY_CODE) // ccb is busy
    auto_set_link.ccbserver_code = -1;
  else if (auto_set_link.code==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    auto_set_link.ccbserver_code = 1;
  else if (auto_set_link.code==right_reply) // ccb: right reply
    auto_set_link.ccbserver_code = 0;
  else // wrong ccb reply
    auto_set_link.ccbserver_code = 2;  

// Fill-up message
  char tmpstr[100];

  auto_set_link.msg="\nAuto Set Link (0x74)\n";

  if (auto_set_link.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(auto_set_link.ccbserver_code)); auto_set_link.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code: %#2X\n", auto_set_link.code); auto_set_link.msg+=tmpstr;
    sprintf(tmpstr,"Offset: %d\n", auto_set_link.Offset); auto_set_link.msg+=tmpstr;
    sprintf(tmpstr,"Hyst: %d\n", auto_set_link.Hyst); auto_set_link.msg+=tmpstr;
    sprintf(tmpstr,"Apl: %d\n", auto_set_link.Apl); auto_set_link.msg+=tmpstr;
    sprintf(tmpstr,"Thr: %d\n", auto_set_link.Thr); auto_set_link.msg+=tmpstr;
  }

  return idx;
}


// Print results to standard output
// Author:  A.Parenti Jan17 2005
// Modified: AP, Dec07 2006
void Cccb::auto_set_LINK_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",auto_set_link.msg.c_str());
}


/***********************************/
/* "Read Link Data" (0x76) command */
/***********************************/

// Send "Read Link Data" (0x76) command and decode reply
// Author:  A.Parenti Jan17 2005
// Modified: AP, Mar24 2005
// Tested at PD, Mar25 2005
void Cccb::read_LINK() {
  const unsigned char command_code=0x76;
  unsigned char command_line[1];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;

  read_LINK_reset(); // Reset read_link struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1,READ_LINK_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = read_LINK_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    read_link.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nRead Link Data (0x76): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset read_link struct
// Author: A.Parenti Aug03 2006
// Modified: AP, Dec09 2006
void Cccb::read_LINK_reset() {
  if (this==NULL) return; // Object deleted -> return

  read_link.code = 0;
  read_link.Offset = read_link.Hyst = read_link.Apl = read_link.Thr = 0;

  read_link.ccbserver_code=-5;
  read_link.msg="";
}


// Decode "Read Link Data" (0x76) reply
// Author:  A.Parenti Feb11 2005
// Modified: AP, Dec09 2006
int Cccb::read_LINK_decode(unsigned char*rdstr) {
  const unsigned char right_reply=0x75;
  unsigned char code2;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&read_link.code);
  code2 = rdstr[idx];

// Fill-up the "result" structure
  idx = rstring(rdstr,idx,&read_link.Offset);
  idx = rstring(rdstr,idx,&read_link.Hyst);
  idx = rstring(rdstr,idx,&read_link.Apl);
  idx = rstring(rdstr,idx,&read_link.Thr);

// Fill-up the error code
  if (read_link.code==CCB_BUSY_CODE) // ccb is busy
    read_link.ccbserver_code = -1;
  else if (read_link.code==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    read_link.ccbserver_code = 1;
  else if (read_link.code==right_reply) // ccb: right reply
    read_link.ccbserver_code = 0;
  else // wrong ccb reply
    read_link.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  read_link.msg="\nRead Link Data (0x76)\n";

  if (read_link.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(read_link.ccbserver_code)); read_link.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code: %#2X\n",read_link.code); read_link.msg+=tmpstr;
    sprintf(tmpstr,"Offset: %d\n",read_link.Offset); read_link.msg+=tmpstr;
    sprintf(tmpstr,"Hyst: %d\n",read_link.Hyst); read_link.msg+=tmpstr;
    sprintf(tmpstr,"Apl: %d\n",read_link.Apl); read_link.msg+=tmpstr;
    sprintf(tmpstr,"Thr: %d\n",read_link.Thr); read_link.msg+=tmpstr;
  }

  return idx;
}


// Print results to standard output
// Author:  A.Parenti Jan17 2005
// Modified: AP, Dec09 2006
void Cccb::read_LINK_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",read_link.msg.c_str());
}


/*****************************************/
/* "Disable Link Monitor" (0x77) command */
/*****************************************/

// Send "Disable Link Monitor" (0x77) command and decode reply
// Author:  A.Parenti Jan17 2005
// Modified: AP, Mar24 2005
// Tested at PD, Mar29 2005
void Cccb::disable_LINK_monitor(char disable) {
  const unsigned char command_code=0x77;
  unsigned char command_line[2];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=disable;

  disable_LINK_monitor_reset(); // Reset disble_link_mon struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,2,DISABLE_LINK_MONITOR_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = disable_LINK_monitor_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    disble_link_mon.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nDisable Link Monitor (0x77): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset disble_link_mon struct
// Author: A.Parenti Aug03 2006
// Modified: AP, Dec09 2006
void Cccb::disable_LINK_monitor_reset() {
  if (this==NULL) return; // Object deleted -> return

  disble_link_mon.code1 = disble_link_mon.code2 = 0;

  disble_link_mon.ccbserver_code=-5;
  disble_link_mon.msg="";
}


// Decode "Disable Link Monitor" (0x77) reply
// Author:  A.Parenti Feb11 2005
// Modified: AP, Dec09 2006
int Cccb::disable_LINK_monitor_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x77, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&disble_link_mon.code1);
  idx = rstring(rdstr,idx,&disble_link_mon.code2);

// Fill-up the error code
  if (disble_link_mon.code1==CCB_BUSY_CODE) // ccb is busy
    disble_link_mon.ccbserver_code = -1;
  else if (disble_link_mon.code1==CCB_UNKNOWN1 && disble_link_mon.code2==CCB_UNKNOWN2) // ccb: unknown command
    disble_link_mon.ccbserver_code = 1;
  else if (disble_link_mon.code1==right_reply && disble_link_mon.code2==command_code) // ccb: right reply
    disble_link_mon.ccbserver_code = 0;
  else // wrong ccb reply
    disble_link_mon.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  disble_link_mon.msg="\nDisable Link Monitor (0x77)\n";

  if (disble_link_mon.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(disble_link_mon.ccbserver_code)); disble_link_mon.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",disble_link_mon.code1); disble_link_mon.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",disble_link_mon.code2); disble_link_mon.msg+=tmpstr;
}

  return idx;
}


// Print results to standard output
// Author:  A.Parenti Jan17 2005
// Modified: AP, Dec09 2006
void Cccb::disable_LINK_monitor_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",disble_link_mon.msg.c_str());
}


/***************************/
/* "Script" (0xEE) command */
/***************************/

// Send "Script" (0xEE) command and decode reply
// Author:  A.Parenti 18Jan 2005
// Modified: AP, Mar24 2005
// Tested at PD, Mar29 2005
void Cccb::script(char page, short addr, char arg[SCRIPT_MAX_SIZE]) {
  const unsigned char command_code=0xEE;
  unsigned char *command_line;
  char arg_size;
  int i, send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// Check "size"
  arg_size = strlen(arg);
  if (arg_size>SCRIPT_MAX_SIZE)
    arg_size=SCRIPT_MAX_SIZE;

// create the command line
  command_line = new unsigned char [5+arg_size];
  command_line[0]=command_code;
  command_line[1]=page;
  wstring(addr,2,command_line);
  for (i=0;i<arg_size;++i)
    command_line[4+i]=arg[i];
  command_line[4+arg_size]=0; // line terminator

  script_reset(); // Reset mc_script struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,5+arg_size,SCRIPT_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = script_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    mc_script.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nScript (0xEE): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset mc_script struct
// Author: A.Parenti Aug03 2006
// Modified: AP, Dec09 2006
void Cccb::script_reset() {
  if (this==NULL) return; // Object deleted -> return

  mc_script.code = mc_script.size = 0;

  mc_script.ccbserver_code=-5;
  mc_script.msg="";
}

// Decode "Script" (0xEE) reply
// Author:  A.Parenti Feb11 2005
// Modified: AP, Dec09 2006
int Cccb::script_decode(unsigned char *rdstr) {
  const unsigned char right_reply=0xEE;
  unsigned char code2;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&mc_script.code);
  code2 = rdstr[idx];

// Fill-up the "result" structure
  idx = rstring(rdstr,idx,&mc_script.size);

// Fill-up the error code
  if (mc_script.code==CCB_BUSY_CODE) // ccb is busy
    mc_script.ccbserver_code = -1;
  else if (mc_script.code==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    mc_script.ccbserver_code = 1;
  else if (mc_script.code==right_reply) // ccb: right reply
    mc_script.ccbserver_code = 0;
  else // wrong ccb reply
    mc_script.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  mc_script.msg="\nScript (0xEE)\n";

  if (mc_script.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(mc_script.ccbserver_code)); mc_script.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code: %#2X\n",mc_script.code); mc_script.msg+=tmpstr;
    sprintf(tmpstr,"size: %#2X\n",mc_script.size); mc_script.msg+=tmpstr;
    if (mc_script.size>0)
      mc_script.msg="Script is valid and will be executed.\n";
  }

  return idx;
}


// Print results to standard output
// Author:  A.Parenti Jan18 2005
// Modified: AP, Dec09 2006
void Cccb::script_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",mc_script.msg.c_str());
}


/*********************************/
/* "Auto Trigger" (0x70) command */
/*********************************/

// Send "Auto Trigger" (0x70) command and decode reply
// Author:  A.Parenti Jan18 2005
// Modified: AP, Mar24 2005
// Tested at PD, Mar29 2005
void Cccb::auto_trigger(char en, char L1A_delay) {
  const unsigned char command_code=0x70;
  unsigned char command_line[3];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=en;
  command_line[2]=(0x7F &  L1A_delay);

  auto_trigger_reset(); // Reset mc_auto_trigger struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,3,AUTO_TRIGGER_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = auto_trigger_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    mc_auto_trigger.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nAuto Trigger (0x70): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset mc_auto_trigger struct
// Author: A.Parenti Aug03 2006
// Modified: AP, Dec11 2006
void Cccb::auto_trigger_reset() {
  if (this==NULL) return; // Object deleted -> return

  mc_auto_trigger.code1 = mc_auto_trigger.code2 = 0;

  mc_auto_trigger.ccbserver_code=-5;
  mc_auto_trigger.msg="";
}


// Decode "Auto Trigger" (0x70) reply
// Author:  A.Parenti Feb11 2005
// Modified: AP, Dec11 2006
int Cccb::auto_trigger_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x70, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&mc_auto_trigger.code1);
  idx = rstring(rdstr,idx,&mc_auto_trigger.code2);

// Fill-up the error code
  if (mc_auto_trigger.code1==CCB_BUSY_CODE) // ccb is busy
    mc_auto_trigger.ccbserver_code = -1;
  else if (mc_auto_trigger.code1==CCB_UNKNOWN1 && mc_auto_trigger.code2==CCB_UNKNOWN2) // ccb: unknown command
    mc_auto_trigger.ccbserver_code = 1;
  else if (mc_auto_trigger.code1==right_reply && mc_auto_trigger.code2==command_code) // ccb: right reply
    mc_auto_trigger.ccbserver_code = 0;
  else // wrong ccb reply
    mc_auto_trigger.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  mc_auto_trigger.msg="\nAuto Trigger (0x70)\n";

  if (mc_auto_trigger.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(mc_auto_trigger.ccbserver_code)); mc_auto_trigger.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",mc_auto_trigger.code1); mc_auto_trigger.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",mc_auto_trigger.code2); mc_auto_trigger.msg+=tmpstr;
  }

  return idx;
}


// Print results to standard output
// Author:  A.Parenti Jan18 2005
// Modified: AP, Dec11 2006
void Cccb::auto_trigger_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",mc_auto_trigger.msg.c_str());
}


/*********************************************/
/* "Read MC crate sensors ID" (0x8F) command */
/*********************************************/

// Send "Read MC crate sensors ID" (0x8F) command and decode reply
// Author:  A.Parenti Feb28 2005
// Tested at PD, Mar25 2005
// Modified: AP, Dec07 2006
void Cccb::read_crate_sensor_id() {
  const unsigned char command_code=0x8F;
  unsigned char command_line[1];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

  mc_firmware_version(); // Get firmware version
  read_crate_sensor_id_reset(); // Reset mc_crate_sensor struct
  if (HVer<1 || (HVer==1 && LVer<6)) { // Firmware < v1.6
    char tmpstr[200];
    sprintf(tmpstr,"Attention: command %#X supported after v1.6 (actual: v%d.%d)\n",command_code,HVer,LVer);
    mc_crate_sensor.msg=tmpstr;
    std::cout << tmpstr;
    return;
  }

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;

// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1,RD_CRATE_SENSOR_ID_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = read_crate_sensor_id_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    mc_crate_sensor.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nRead MC crate sensors ID (0x8F): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset mc_crate_sensor struct
// Author: A.Parenti Aug03 2006
// Modified: AP, Dec11 2006
void Cccb::read_crate_sensor_id_reset() {
  int i, j;

  if (this==NULL) return; // Object deleted -> return

  mc_crate_sensor.code=0;
  for (i=0;i<21;++i)
    for (j=0;j<8;++j)
      mc_crate_sensor.snsr_code[i][j]=0;

  mc_crate_sensor.ccbserver_code=-5;
  mc_crate_sensor.msg="";
}


// Decode "Read MC crate sensors ID" (0x8F) reply
// Author:  A.Parenti Feb28 2005
// Modified: AP, Dec11 2006
int Cccb::read_crate_sensor_id_decode(unsigned char *rdstr) {
  const unsigned char right_reply=0x90;
  unsigned char code2;
  int idx=0, i, j;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&mc_crate_sensor.code);
  code2 = rdstr[idx];

// Fill-up the "result" structure
  for (i=0;i<21;++i)
    for (j=0;j<8;++j)
      idx = rstring(rdstr,idx,&mc_crate_sensor.snsr_code[i][j]);

// Fill-up the error code
  if (mc_crate_sensor.code==CCB_BUSY_CODE) // ccb is busy
    mc_crate_sensor.ccbserver_code = -1;
  else if (mc_crate_sensor.code==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    mc_crate_sensor.ccbserver_code = 1;
  else if (mc_crate_sensor.code==right_reply) // ccb: right reply
    mc_crate_sensor.ccbserver_code = 0;
  else // wrong ccb reply
    mc_crate_sensor.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  mc_crate_sensor.msg="\nRead MC crate sensors ID (0x8F)\n";

  if (mc_crate_sensor.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(mc_crate_sensor.ccbserver_code)); mc_crate_sensor.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code: %#2X\n",mc_crate_sensor.code); mc_crate_sensor.msg+=tmpstr;
    for (i=0;i<21;++i)
      for (j=0;j<8;++j) {
        sprintf(tmpstr,"sensor code[%d][%d]: %#2X\n",i,j,mc_crate_sensor.snsr_code[i][j]);  mc_crate_sensor.msg+=tmpstr;
      }
  }

  return idx;
}


// Print results to standard output
// Author:  A.Parenti Feb28 2005
// Modified: AP, Dec11 2006
void Cccb::read_crate_sensor_id_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",mc_crate_sensor.msg.c_str());
}


/*********************************/
/* "CCBRdy Pulse" (0x91) command */
/*********************************/

// Send "CCBRdy Pulse" (0x91) command and decode reply
// Author:  A.Parenti Apr14 2005
// Modified: AP, Dec07 2006
void Cccb::CCBrdy_pulse() {
  const unsigned char command_code=0x91;
  unsigned char command_line[1];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

  mc_firmware_version(); // Get firmware version
  CCBrdy_pulse_reset(); // Reset mc_ccbrdy_pulse struct
  if (HVer<1 || (HVer==1 && LVer<7)) { // Firmware < v1.7
    char tmpstr[200];
    sprintf(tmpstr,"Attention: command %#X supported after v1.7 (actual: v%d.%d)\n",command_code,HVer,LVer);
    mc_ccbrdy_pulse.msg=tmpstr;
    std::cout << tmpstr;
    return;
  }

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;

// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1,CCBRDY_PULSE_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = CCBrdy_pulse_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    mc_ccbrdy_pulse.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nCCBRdy Pulse (0x91): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}

// Reset mc_ccbrdy_pulse struct
// Author: A.Parenti Aug03 2006
// Modified: AP, Dec11 2006
void Cccb::CCBrdy_pulse_reset() {
  if (this==NULL) return; // Object deleted -> return

  mc_ccbrdy_pulse.code1 = mc_ccbrdy_pulse.code2 = 0;

  mc_ccbrdy_pulse.ccbserver_code=-5;
  mc_ccbrdy_pulse.msg="";
}


// Decode "CCBRdy Pulse" (0x91) reply
// Author:  A.Parenti Apr14 2005
// Modified: AP, Dec11 2006
int Cccb::CCBrdy_pulse_decode(unsigned char *rdstr) {
  const unsigned char right_reply=CCB_OXFC, command_code=0x91;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&mc_ccbrdy_pulse.code1);
  idx = rstring(rdstr,idx,&mc_ccbrdy_pulse.code2);

// Fill-up the error code
  if (mc_ccbrdy_pulse.code1==CCB_BUSY_CODE) // ccb is busy
    mc_ccbrdy_pulse.ccbserver_code = -1;
  else if (mc_ccbrdy_pulse.code1==CCB_UNKNOWN1 && mc_ccbrdy_pulse.code2==CCB_UNKNOWN2) // ccb: unknown command
    mc_ccbrdy_pulse.ccbserver_code = 1;
  else if (mc_ccbrdy_pulse.code1==right_reply && mc_ccbrdy_pulse.code2==command_code) // ccb: right reply
    mc_ccbrdy_pulse.ccbserver_code = 0;
  else // wrong ccb reply
    mc_ccbrdy_pulse.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  mc_ccbrdy_pulse.msg="\nCCBRdy Pulse (0x91)\n";

  if (mc_ccbrdy_pulse.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(mc_ccbrdy_pulse.ccbserver_code)); mc_ccbrdy_pulse.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",mc_ccbrdy_pulse.code1); mc_ccbrdy_pulse.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",mc_ccbrdy_pulse.code2); mc_ccbrdy_pulse.msg+=tmpstr;
  }

  return idx;
}


// Print results to standard output
// Author:  A.Parenti Apr14 2005
// Modified: AP, Dec11 2006
void Cccb::CCBrdy_pulse_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",mc_ccbrdy_pulse.msg.c_str());
}



/*************************/
/* "PIRW" (0x92) command */
/*************************/

// Send "PIRW" (0x92) command and decode reply
// Author:  A.Parenti May09 2005
// Modified: AP, Dec07 2006
void Cccb::pirw(char state) {
  const unsigned char command_code=0x92;
  unsigned char command_line[2];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

  mc_firmware_version(); // Get firmware version
  pirw_reset(); // Reset mc_pirw struct
  if (HVer<1 || (HVer==1 && LVer<8)) { // Firmware < v1.8
    char tmpstr[200];
    sprintf(tmpstr,"Attention: command %#X supported after v1.8 (actual: v%d.%d)\n",command_code,HVer,LVer);
    mc_pirw.msg=tmpstr;
    std::cout << tmpstr;
    return;
  }


// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=state;

// Send the command line
  send_command_result = cmd_obj->send_command(command_line,2,PIRW_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = pirw_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    mc_pirw.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nPIRW (0x92): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}

// Reset mc_pirw struct
// Author: A.Parenti Aug03 2006
// Modified: AP, Dec11 2006
void Cccb::pirw_reset() {
  if (this==NULL) return; // Object deleted -> return

  mc_pirw.code1 = mc_pirw.code2 = 0;

  mc_pirw.ccbserver_code=-5;
  mc_pirw.msg="";
}


// Decode "PIRW" (0x92) reply
// Author:  A.Parenti May09 2005
// Modified: AP, Dec11 2006
int Cccb::pirw_decode(unsigned char *rdstr) {
  const unsigned char right_reply=CCB_OXFC, command_code=0x92;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&mc_pirw.code1);
  idx = rstring(rdstr,idx,&mc_pirw.code2);

// Fill-up the error code
  if (mc_pirw.code1==CCB_BUSY_CODE) // ccb is busy
    mc_pirw.ccbserver_code = -1;
  else if (mc_pirw.code1==CCB_UNKNOWN1 && mc_pirw.code2==CCB_UNKNOWN2) // ccb: unknown command
    mc_pirw.ccbserver_code = 1;
  else if (mc_pirw.code1==right_reply && mc_pirw.code2==command_code) // ccb: right reply
    mc_pirw.ccbserver_code = 0;
  else // wrong ccb reply
    mc_pirw.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  mc_pirw.msg="\nPIRW (0x92)\n";

  if (mc_pirw.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(mc_pirw.ccbserver_code)); mc_pirw.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n", mc_pirw.code1); mc_pirw.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n", mc_pirw.code2); mc_pirw.msg+=tmpstr;
  }

  return idx;
}


// Print results to standard output
// Author:  A.Parenti May09 2005
// Modified: AP, Dec11 2006
void Cccb::pirw_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",mc_pirw.msg.c_str());
}



/***************************/
/* "PIPROG" (0xAF) command */
/***************************/

// Send "PIPROG" (0xAF) command and decode reply
// Author:  A.Parenti Oct09 2007
void Cccb::piprog(char state) {
  const unsigned char command_code=0xAF;
  unsigned char command_line[2];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

  mc_firmware_version(); // Get firmware version
  piprog_reset(); // Reset mc_piprog struct
  if (HVer<1 || (HVer==1 && LVer<20)) { // Firmware < v1.20
    char tmpstr[200];
    sprintf(tmpstr,"Attention: command %#X supported after v1.20 (actual: v%d.%d)\n",command_code,HVer,LVer);
    mc_piprog.msg=tmpstr;
    std::cout << tmpstr;
    return;
  }


// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=state;

// Send the command line
  send_command_result = cmd_obj->send_command(command_line,2,PIPROG_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = piprog_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    mc_piprog.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nPIPROG (0xAF): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}

// Reset mc_piprog struct
// Author: A.Parenti Oct09 2007
void Cccb::piprog_reset() {
  if (this==NULL) return; // Object deleted -> return

  mc_piprog.code1 = mc_piprog.code2 = 0;

  mc_piprog.ccbserver_code=-5;
  mc_piprog.msg="";
}


// Decode "PIPROG" (0xAF) reply
// Author:  A.Parenti Oct09 2007
int Cccb::piprog_decode(unsigned char *rdstr) {
  const unsigned char right_reply=CCB_OXFC, command_code=0xAF;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&mc_piprog.code1);
  idx = rstring(rdstr,idx,&mc_piprog.code2);

// Fill-up the error code
  if (mc_piprog.code1==CCB_BUSY_CODE) // ccb is busy
    mc_piprog.ccbserver_code = -1;
  else if (mc_piprog.code1==CCB_UNKNOWN1 && mc_piprog.code2==CCB_UNKNOWN2) // ccb: unknown command
    mc_piprog.ccbserver_code = 1;
  else if (mc_piprog.code1==right_reply && mc_piprog.code2==command_code) // ccb: right reply
    mc_piprog.ccbserver_code = 0;
  else // wrong ccb reply
    mc_piprog.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  mc_piprog.msg="\nPIPROG (0xAF)\n";

  if (mc_piprog.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(mc_piprog.ccbserver_code)); mc_piprog.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n", mc_piprog.code1); mc_piprog.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n", mc_piprog.code2); mc_piprog.msg+=tmpstr;
  }

  return idx;
}


// Print results to standard output
// Author:  A.Parenti May09 2005
// Modified: AP, Dec11 2006
void Cccb::piprog_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",mc_piprog.msg.c_str());
}



/**********************************/
/* "Disable SB CK" (0x94) command */
/**********************************/

// Send "Disable SB CK" (0x94) command and decode reply
// Author:  A.Parenti May09 2005
// Modified: AP, Dec07 2006
void Cccb::disable_SB_CK(char state) {
  const unsigned char command_code=0x94;
  unsigned char command_line[2];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

  mc_firmware_version(); // Get firmware version
  disable_SB_CK_reset(); // Reset disable_sb_ck struct
  if (HVer<1 || (HVer==1 && LVer<8)) { // Firmware < v1.8
    char tmpstr[200];
    sprintf(tmpstr,"Attention: command %#X supported after v1.8 (actual: v%d.%d)\n",command_code,HVer,LVer);
    disable_sb_ck.msg=tmpstr;
    std::cout << tmpstr;
    return;
  }

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=state;

// Send the command line
  send_command_result = cmd_obj->send_command(command_line,2,DISABLE_SB_CK_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = disable_SB_CK_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    disable_sb_ck.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nDisable SB CK (0x94): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset disable_sb_ck struct
// Author: A.Parenti Aug03 2006
// Modified: AP, Dec11 2006
void Cccb::disable_SB_CK_reset() {
  if (this==NULL) return; // Object deleted -> return

  disable_sb_ck.code1 = disable_sb_ck.code2 = 0;

  disable_sb_ck.ccbserver_code=-5;
}


// Decode "Disable SB CK" (0x94) reply
// Author:  A.Parenti May09 2005
// Modified: AP, Dec11 2006
int Cccb::disable_SB_CK_decode(unsigned char *rdstr) {
  const unsigned char right_reply=CCB_OXFC, command_code=0x94;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&disable_sb_ck.code1);
  idx = rstring(rdstr,idx,&disable_sb_ck.code2);

// Fill-up the error code
  if (disable_sb_ck.code1==CCB_BUSY_CODE) // ccb is busy
    disable_sb_ck.ccbserver_code = -1;
  else if (disable_sb_ck.code1==CCB_UNKNOWN1 && disable_sb_ck.code2==CCB_UNKNOWN2) // ccb: unknown command
    disable_sb_ck.ccbserver_code = 1;
  else if (disable_sb_ck.code1==right_reply && disable_sb_ck.code2==command_code) // ccb: right reply
    disable_sb_ck.ccbserver_code = 0;
  else // wrong ccb reply
    disable_sb_ck.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  disable_sb_ck.msg="\nDisable SB CK (0x94)\n";

  if (disable_sb_ck.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(disable_sb_ck.ccbserver_code)); disable_sb_ck.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",disable_sb_ck.code1); disable_sb_ck.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",disable_sb_ck.code2); disable_sb_ck.msg+=tmpstr;
  }

  return idx;
}


// Print results to standard output
// Author:  A.Parenti May09 2005
// Modified: AP, Dec11 2006
void Cccb::disable_SB_CK_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",disable_sb_ck.msg.c_str());
}


/******************************************/
/* "Disable oscillator CK" (0x95) command */
/******************************************/

// Send "Disable oscillator CK" (0x95) command and decode reply
// Author:  A.Parenti May09 2005
// Modified: AP, Dec11 2006
void Cccb::disable_OSC_CK(char state) {
  const unsigned char command_code=0x95;
  unsigned char command_line[2];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

  mc_firmware_version(); // Get firmware version
  disable_OSC_CK_reset(); // Reset disable_osc_ck struct 
  if (HVer<1 || (HVer==1 && LVer<8)) { // Firmware < v1.8
    char tmpstr[200];
    sprintf(tmpstr,"Attention: command %#X supported after v1.8 (actual: v%d.%d)\n",command_code,HVer,LVer);
    disable_osc_ck.msg=tmpstr;
    std::cout << tmpstr;
    return;
  }

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=state;

// Send the command line
  send_command_result = cmd_obj->send_command(command_line,2,DISABLE_OSC_CK_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = disable_OSC_CK_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    disable_osc_ck.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nDisable oscillator CK (0x95): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset disable_osc_ck struct 
// Author: A.Parenti Aug03 2006
// Modified: AP, Dec11 2006
void Cccb::disable_OSC_CK_reset() {
  if (this==NULL) return; // Object deleted -> return

  disable_osc_ck.code1 = disable_osc_ck.code2 = 0;

  disable_osc_ck.ccbserver_code=-5;
  disable_osc_ck.msg="";
}


// Decode "Disable OSC CK" (0x95) reply
// Author:  A.Parenti May09 2005
// Modified: AP, Dec11 2006
int Cccb::disable_OSC_CK_decode(unsigned char *rdstr) {
  const unsigned char right_reply=CCB_OXFC, command_code=0x95;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&disable_osc_ck.code1);
  idx = rstring(rdstr,idx,&disable_osc_ck.code2);

// Fill-up the error code
  if (disable_osc_ck.code1==CCB_BUSY_CODE) // ccb is busy
    disable_osc_ck.ccbserver_code = -1;
  else if (disable_osc_ck.code1==CCB_UNKNOWN1 && disable_osc_ck.code2==CCB_UNKNOWN2) // ccb: unknown command
    disable_osc_ck.ccbserver_code = 1;
  else if (disable_osc_ck.code1==right_reply && disable_osc_ck.code2==command_code) // ccb: right reply
    disable_osc_ck.ccbserver_code = 0;
  else // wrong ccb reply
    disable_osc_ck.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  std::cout << "\nDisable oscillator CK (0x95)\n";

  if (disable_osc_ck.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(disable_osc_ck.ccbserver_code)); disable_osc_ck.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",disable_osc_ck.code1); disable_osc_ck.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",disable_osc_ck.code2); disable_osc_ck.msg+=tmpstr;
  }

  return idx;
}


// Print results to standard output
// Author:  A.Parenti May09 2005
// Modified: AP, Dec11 2006
void Cccb::disable_OSC_CK_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",disable_osc_ck.msg.c_str());
}



/**************************************/
/* "Write custom data" (0x97) command */
/**************************************/

// Send "Write custom data" (0x97) command and decode reply
// Author:  A.Parenti May10 2005
// Modified: AP, Dec07 2006
void Cccb::write_custom_data(unsigned char data[CUSTOM_DATA_MAX_SIZE], unsigned char size) {
  const unsigned char command_code=0x97;
  unsigned char *command_line;
  int i, send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

  mc_firmware_version(); // Get firmware version
  write_custom_data_reset(); // Reset wr_custom_data struct
  if (HVer<1 || (HVer==1 && LVer<8)) { // Firmware < v1.8
    char tmpstr[200];
    sprintf(tmpstr,"Attention: command %#X supported after v1.8 (actual: v%d.%d)\n",command_code,HVer,LVer);
    wr_custom_data.msg=tmpstr;
    std::cout << tmpstr;
    return;
  }

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// Check "size"
  if (size>CUSTOM_DATA_MAX_SIZE)
    size=CUSTOM_DATA_MAX_SIZE;

// create the command line
  command_line = new unsigned char [1+size];
  command_line[0]=command_code;
  for (i=0;i<size;++i)
    wstring(data[i],1+i,command_line);

// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1+size,WR_CUSTOM_DATA_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = write_custom_data_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    wr_custom_data.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nWrite custom data (0x97): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset wr_custom_data struct
// Author: A.Parenti Aug03 2006
// Modified: AP, Dec11 2006
void Cccb::write_custom_data_reset() {
  if (this==NULL) return; // Object deleted -> return

  wr_custom_data.code1 = wr_custom_data.code2 = 0;

  wr_custom_data.ccbserver_code=-5;
  wr_custom_data.msg="";
}


// Decode "Write custom data" (0x97) reply
// Author:  A.Parenti May10 2005
// Modified: AP, Dec11 2006
int Cccb::write_custom_data_decode(unsigned char *rdstr) {
  const unsigned char right_reply=CCB_OXFC, command_code=0x97;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&wr_custom_data.code1);
  idx = rstring(rdstr,idx,&wr_custom_data.code2);

// Fill-up the error code
  if (wr_custom_data.code1==CCB_BUSY_CODE) // ccb is busy
    wr_custom_data.ccbserver_code = -1;
  else if (wr_custom_data.code1==CCB_UNKNOWN1 && wr_custom_data.code2==CCB_UNKNOWN2) // ccb: unknown command
    wr_custom_data.ccbserver_code = 1;
  else if (wr_custom_data.code1==right_reply && wr_custom_data.code2==command_code) // ccb: right reply
    wr_custom_data.ccbserver_code = 0;
  else // wrong ccb reply
    wr_custom_data.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  wr_custom_data.msg="\nWrite custom data (0x97)\n";

  if (wr_custom_data.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(wr_custom_data.ccbserver_code)); wr_custom_data.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",wr_custom_data.code1); wr_custom_data.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",wr_custom_data.code2); wr_custom_data.msg+=tmpstr;
  }

  return idx;
}


// Print results to standard output
// Author:  A.Parenti May10 2005
// Modified: AP, Dec11 2006
void Cccb::write_custom_data_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",wr_custom_data.msg.c_str());
}



/*************************************/
/* "Read custom data" (0x98) command */
/*************************************/

// Send "Read custom data" (0x98) command and decode reply
// Author:  A.Parenti May10 2005
// Modified: AP, Dec07 2006
void Cccb::read_custom_data() {
  const unsigned char command_code=0x98;
  unsigned char command_line[1];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

  mc_firmware_version(); // Get firmware version
  read_custom_data_reset(); // Reset rd_custom_data struct
  if (HVer<1 || (HVer==1 && LVer<8)) { // Firmware < v1.8
    char tmpstr[200];
    sprintf(tmpstr,"Attention: command %#X supported after v1.8 (actual: v%d.%d)\n",command_code,HVer,LVer);
    rd_custom_data.msg=tmpstr;
    std::cout << tmpstr;
    return;
  }

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;

// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1,RD_CUSTOM_DATA_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = read_custom_data_decode(cmd_obj->data_read,cmd_obj->data_read_len); // Decode reply
  }
  else // copy error code
    rd_custom_data.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nRead custom data (0x98): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset rd_custom_data struct
// Author: A.Parenti Aug03 2006
// Modified: AP, Dec11 2006
void Cccb::read_custom_data_reset(){
  int i;

  if (this==NULL) return; // Object deleted -> return

  rd_custom_data.code = rd_custom_data.size = 0;
  for (i=0;i<CUSTOM_DATA_MAX_SIZE;++i)
    rd_custom_data.data[CUSTOM_DATA_MAX_SIZE] = 0;

  rd_custom_data.ccbserver_code=-5;
  rd_custom_data.msg="";
}


// Decode "Read custom data" (0x98) reply
// Author:  A.Parenti May10 2005
// Modified: AP, Dec11 2006
int Cccb::read_custom_data_decode(unsigned char *rdstr, int rdstr_len) {
  const unsigned char right_reply=0x99;
  unsigned char code2;
  int i, idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&rd_custom_data.code);
  code2 = rdstr[idx];

// Fill-up the "result" structure
  rd_custom_data.size = rdstr_len-1;
  for (i=0;i<rd_custom_data.size;++i)
    idx = rstring(rdstr,idx,rd_custom_data.data+i);

// Fill-up the error code
  if (rd_custom_data.code==CCB_BUSY_CODE) // ccb is busy
    rd_custom_data.ccbserver_code = -1;
  else if (rd_custom_data.code==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    rd_custom_data.ccbserver_code = 1;
  else if (rd_custom_data.code==right_reply) // ccb: right reply
    rd_custom_data.ccbserver_code = 0;
  else // wrong ccb reply
    rd_custom_data.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  rd_custom_data.msg="\nRead custom data (0x98)\n";

  if (rd_custom_data.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(rd_custom_data.ccbserver_code)); rd_custom_data.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n", rd_custom_data.code); rd_custom_data.msg+=tmpstr;
    for (i=0;i<rd_custom_data.size;++i) {
      sprintf(tmpstr,"data[%d]: %#2X\n", i, rd_custom_data.data[i]); rd_custom_data.msg+=tmpstr;
    }
  }

  return idx;
}


// Print results to standard output
// Author:  A.Parenti May10 2005
// Modified: AP, Dec11 2006
void Cccb::read_custom_data_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",rd_custom_data.msg.c_str());
}




/**************************************/
/* "HD Watchdog Reset" (0xF7) command */
/**************************************/

// Send "HD Watchdog Reset" (0xF7) command and decode reply
// Author:  A.Parenti Sep07 2006
void Cccb::hd_restart_CCB() {
  const unsigned char command_code=0xF7;
  unsigned char command_line[1];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;

  hd_restart_CCB_reset(); // Reset hd_restart_ccb struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1,RESTART_CCB_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = hd_restart_CCB_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    hd_restart_ccb.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nHD Watchdog Reset (0xF7): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}

// Reset hd_restart_ccb struct
// Author: A.Parenti Aug03 2006
// Modified: AP, Dec11 2006
void Cccb::hd_restart_CCB_reset() {
  if (this==NULL) return; // Object deleted -> return

  hd_restart_ccb.code1 = hd_restart_ccb.code2 = 0;

  hd_restart_ccb.ccbserver_code=-5;
  hd_restart_ccb.msg="";
}


// Decode "HD Watchdog Reset" (0xF7) reply
// Author:  A.Parenti Feb11 2005
// Modified: AP, Dec11 2006
int Cccb::hd_restart_CCB_decode(unsigned char *rdstr) {
  const unsigned char command_code=0xF7, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&hd_restart_ccb.code1);
  idx = rstring(rdstr,idx,&hd_restart_ccb.code2);

// Fill-up the error code
  if (hd_restart_ccb.code1==CCB_BUSY_CODE) // ccb is busy
    hd_restart_ccb.ccbserver_code = -1;
  else if (hd_restart_ccb.code1==CCB_UNKNOWN1 && hd_restart_ccb.code2==CCB_UNKNOWN2) // ccb: unknown command
    hd_restart_ccb.ccbserver_code = 1;
  else if (hd_restart_ccb.code1==right_reply && hd_restart_ccb.code2==command_code) // ccb: right reply
    hd_restart_ccb.ccbserver_code = 0;
  else // wrong ccb reply
    hd_restart_ccb.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  hd_restart_ccb.msg="\nHD Watchdog Reset (0xF7)\n";

  if (hd_restart_ccb.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(hd_restart_ccb.ccbserver_code)); hd_restart_ccb.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",hd_restart_ccb.code1); hd_restart_ccb.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",hd_restart_ccb.code2); hd_restart_ccb.msg+=tmpstr;
  }

  return idx;
}



// Print results to standard output
// Author:  A.Parenti Jan10 2005
// Modified: AP, Dec11 2006
void Cccb::hd_restart_CCB_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",hd_restart_ccb.msg.c_str());
}


/*********************************/
/* "Find Sensors" (0xA6) command */
/*********************************/

// Send "Find Sensors" (0xA6) command and decode reply
// Author:  A.Parenti Nov30 2006
// Modified: AP, Dec07 2006
void Cccb::find_sensors() {
  const unsigned char command_code=0xA6;
  unsigned char command_line[1];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

  mc_firmware_version(); // Get firmware version
  find_sensors_reset(); // Reset mc_find_sensor struct
  if (HVer<1 || (HVer==1 && LVer<17)) { // Firmware < v1.17
    char tmpstr[200];
    sprintf(tmpstr,"Attention: command %#X supported after v1.17 (actual: v%d.%d)\n",command_code,HVer,LVer);
    mc_find_sensor.msg=tmpstr;
    std::cout << tmpstr;
    return;
  }

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;

// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1,FIND_SENSORS_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = find_sensors_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    mc_find_sensor.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nFind Sensors (0xA6): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset mc_find_sensor struct
// Author: A.Parenti Nov30 2006
// Modified: AP, Dec11 2006
void Cccb::find_sensors_reset() {
  int i, j;

  if (this==NULL) return; // Object deleted -> return

  mc_find_sensor.code=0;
  for (i=0;i<21;++i)
    for (j=0;j<8;++j)
      mc_find_sensor.snsr_code[i][j]=0;

  mc_find_sensor.ccbserver_code=-5;
  mc_find_sensor.msg="";
}


// Decode "Find Sensors" (0xA6) reply
// Author:  A.Parenti Nov30 2006
// Modified: AP, Dec11 2006
int Cccb::find_sensors_decode(unsigned char *rdstr) {
  const unsigned char right_reply=0xA7;
  unsigned char code2;
  int idx=0, i, j;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&mc_find_sensor.code);
  code2 = rdstr[idx];

// Fill-up the "result" structure
  for (i=0;i<21;++i)
    for (j=0;j<8;++j)
      idx = rstring(rdstr,idx,&mc_find_sensor.snsr_code[i][j]);

// Fill-up the error code
  if (mc_find_sensor.code==CCB_BUSY_CODE) // ccb is busy
    mc_find_sensor.ccbserver_code = -1;
  else if (mc_find_sensor.code==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    mc_find_sensor.ccbserver_code = 1;
  else if (mc_find_sensor.code==right_reply) // ccb: right reply
    mc_find_sensor.ccbserver_code = 0;
  else // wrong ccb reply
    mc_find_sensor.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  mc_find_sensor.msg="\nFind Sensors (0xA6)\n";

  if (mc_find_sensor.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(mc_find_sensor.ccbserver_code)); mc_find_sensor.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code: %#2X\n",mc_find_sensor.code); mc_find_sensor.msg+=tmpstr;
    for (i=0;i<21;++i)
      for (j=0;j<8;++j) {
        sprintf(tmpstr,"sensor code[%d][%d]: %#2X\n",i,j,mc_find_sensor.snsr_code[i][j]); mc_find_sensor.msg+=tmpstr;
      }
  }
  return idx;
}


// Print results to standard output
// Author:  A.Parenti Nov30 2006
// Modified: AP, Dec11 2006
void Cccb::find_sensors_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",mc_find_sensor.msg.c_str());
}


/*****************************************/
/* "Auto LINK set (boot)" (0xE7) command */
/*****************************************/

// Send "Auto LINK set (boot)" (0xE7) command and decode reply
// Author:  A.Parenti Sep07 2006
void Cccb::auto_set_LINK_boot(short levL, short levH) {
  const unsigned char command_code=0xE7;
  unsigned char command_line[5];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  wstring(levL,1,command_line);
  wstring(levH,3,command_line);

  auto_set_LINK_boot_reset(); // Reset auto_set_link_boot struct

// Send the command line
  send_command_result = cmd_obj->send_command(command_line,5,AUTO_SET_LINK_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = auto_set_LINK_boot_decode(cmd_obj->data_read,cmd_obj->data_read_len); // Decode reply
  }
  else // copy error code
    auto_set_link_boot.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nAuto LINK set (boot) (0xE7): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset auto_set_link_boot struct
// Author: A.Parenti Sep07 2006
// Modified: AP, Dec11 2006
void Cccb::auto_set_LINK_boot_reset(){
  if (this==NULL) return; // Object deleted -> return

  auto_set_link_boot.code1 = auto_set_link_boot.code2 = 0;
  auto_set_link_boot.result = 0;

  auto_set_link_boot.ccbserver_code=-5;
  auto_set_link_boot.msg="";
}


// Decode "Auto LINK set (boot)" (0xE7) reply
// Author:  A.Parenti Sep07 2006
// Modified: AP, Dec11 2006
int Cccb::auto_set_LINK_boot_decode(unsigned char *rdstr, int rdstr_len) {
  const unsigned char command_code=0xE7, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&auto_set_link_boot.code1);
  idx = rstring(rdstr,idx,&auto_set_link_boot.code2);

// Fill-up the "result" structure
  idx = rstring(rdstr,idx,&auto_set_link_boot.result);

// Fill-up the error code
  if (auto_set_link_boot.code1==CCB_BUSY_CODE) // ccb is busy
    auto_set_link_boot.ccbserver_code = -1;
  else if (auto_set_link_boot.code1==CCB_UNKNOWN1 && auto_set_link_boot.code2==CCB_UNKNOWN2) // ccb: unknown command
    auto_set_link_boot.ccbserver_code = 1;
  else if (auto_set_link_boot.code1==right_reply && auto_set_link_boot.code2==command_code) // ccb: right reply
    auto_set_link_boot.ccbserver_code = 0;
  else // wrong ccb reply
    auto_set_link_boot.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  auto_set_link_boot.msg="\nAuto LINK set (boot) (0xE7)\n";

  if (auto_set_link_boot.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(auto_set_link_boot.ccbserver_code)); auto_set_link_boot.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",auto_set_link_boot.code1); auto_set_link_boot.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",auto_set_link_boot.code2); auto_set_link_boot.msg+=tmpstr;
    sprintf(tmpstr,"Result: %s\n",(auto_set_link_boot.result!=0)?"Success":"Error"); auto_set_link_boot.msg+=tmpstr;
  }

  return idx;
}


// Print results to standard output
// Author:  A.Parenti, Sep07 2006
// Modified: AP, Dec11 2006
void Cccb::auto_set_LINK_boot_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",auto_set_link_boot.msg.c_str());
}



/*************************************/
/* "LINK test (boot)" (0xE4) command */
/*************************************/

// Send "LINK test (boot)" (0xE4) command and decode reply
// Author:  A.Parenti Sep07 2006
void Cccb::LINK_test_boot(char data[LINK_TEST_MAX_SIZE], short size) {
  const unsigned char command_code=0xE4;
  unsigned char *command_line;
  int send_command_result, idx=0, i;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

  if (size<LINK_TEST_MIN_SIZE || size>LINK_TEST_MAX_SIZE) {
    LINK_test_boot_reset(); // Reset link_test_boot struct
    return;
  }

// create the command line
  command_line = new unsigned char[1+size];
  command_line[0]=command_code;
  for (i=0;i<size;++i)
    command_line[i+1]=data[i];

  LINK_test_boot_reset(); // Reset link_test_boot struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,size+1,LINK_TEST_BOOT_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = LINK_test_boot_decode(cmd_obj->data_read,cmd_obj->data_read_len); // Decode reply
  }
  else // copy error code
    link_test_boot.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nLINK test (boot) (0xE4): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset link_test_boot struct
// Author: A.Parenti Sep07 2006
// Modified: AP, Dec11 2006
void Cccb::LINK_test_boot_reset(){
  if (this==NULL) return; // Object deleted -> return

  link_test_boot.code1 = link_test_boot.code2 = 0;

  link_test_boot.ccbserver_code=-5;
  link_test_boot.msg="";
}


// Decode "LINK test (boot)" (0xE4) reply
// Author:  A.Parenti Sep07 2006
// Modified: AP, Dec12 2006
int Cccb::LINK_test_boot_decode(unsigned char *rdstr, int rdstr_len) {
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return
// identification code of the structure
  idx = rstring(rdstr,idx,&link_test_boot.code1);
  idx = rstring(rdstr,idx,&link_test_boot.code2);

// Fill-up the error code
  if (link_test_boot.code1==CCB_BUSY_CODE) // ccb is busy
    link_test_boot.ccbserver_code = -1;
  else if (link_test_boot.code1==CCB_UNKNOWN1 && link_test_boot.code2==CCB_UNKNOWN2) // ccb: unknown command
    link_test_boot.ccbserver_code = 1;
  else if (link_test_boot.code1==0 && link_test_boot.code2==0) // ccb: right reply (== no reply)
    link_test_boot.ccbserver_code = 0;
  else // wrong ccb reply
    link_test_boot.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  link_test_boot.msg="\nLINK test (boot) (0xE4)\n";

  if (link_test_boot.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(link_test_boot.ccbserver_code)); link_test_boot.msg+=tmpstr;
  } else {
    link_test_boot.msg+="Result: ok\n";
  }

  return idx;
}


// Print results to standard output
// Author:  A.Parenti, Sep07 2006
// Modified: AP, Dec11 2006
void Cccb::LINK_test_boot_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",link_test_boot.msg.c_str());
}



/*************************************/
/* "Reset SEU (boot)" (0xE6) command */
/*************************************/

// Send "Reset SEU (boot)" (0xE6) command and decode reply
// Author:  A.Parenti Sep07 2006
void Cccb::reset_SEU_boot() {
  const unsigned char command_code=0xE6;
  unsigned char command_line[1];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;

  reset_SEU_boot_reset(); // Reset reset_seu_boot struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1,RESET_SEU_BOOT_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = reset_SEU_boot_decode(cmd_obj->data_read,cmd_obj->data_read_len); // Decode reply
  }
  else // copy error code
    reset_seu_boot.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nReset SEU (boot) (0xE6): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset reset_seu_boot struct
// Author: A.Parenti Sep07 2006
// Modified: AP, Dec11 2006
void Cccb::reset_SEU_boot_reset(){
  if (this==NULL) return; // Object deleted -> return

  reset_seu_boot.code1 = reset_seu_boot.code2 = 0;

  reset_seu_boot.ccbserver_code=-5;
  reset_seu_boot.msg="";
}


// Decode "Reset SEU (boot)" (0xE6) reply
// Author:  A.Parenti Sep07 2006
// Modified: AP, Dec11 2006
int Cccb::reset_SEU_boot_decode(unsigned char *rdstr, int rdstr_len) {
  const unsigned char command_code=0xE6, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&reset_seu_boot.code1);
  idx = rstring(rdstr,idx,&reset_seu_boot.code2);

// Fill-up the error code
  if (reset_seu_boot.code1==CCB_BUSY_CODE) // ccb is busy
    reset_seu_boot.ccbserver_code = -1;
  else if (reset_seu_boot.code1==CCB_UNKNOWN1 && reset_seu_boot.code2==CCB_UNKNOWN2) // ccb: unknown command
    reset_seu_boot.ccbserver_code = 1;
  else if (reset_seu_boot.code1==right_reply && reset_seu_boot.code2==command_code) // ccb: right reply
    reset_seu_boot.ccbserver_code = 0;
  else // wrong ccb reply
    reset_seu_boot.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  reset_seu_boot.msg="\nReset SEU (boot) (0xE6)\n";

  if (reset_seu_boot.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(reset_seu_boot.ccbserver_code)); reset_seu_boot.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n", reset_seu_boot.code1); reset_seu_boot.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n", reset_seu_boot.code2); reset_seu_boot.msg+=tmpstr;
  }

  return idx;
}


// Print results to standard output
// Author:  A.Parenti, Sep07 2006
// Modified: AP, Dec11 2006
void Cccb::reset_SEU_boot_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",reset_seu_boot.msg.c_str());
}



/*************************************/
/* "Link DAC (boot)" (0xE5) command */
/*************************************/

// Send "Link DAC (boot)" (0xE5) command and decode reply
// Author:  A.Parenti Sep07 2006
void Cccb::link_DAC_boot(short dacvalue) {
  const unsigned char command_code=0xE5;
  unsigned char command_line[3];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  wstring(dacvalue,1,command_line);

  link_DAC_boot_reset(); // Reset link_dac_boot struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,3,LINK_DAC_BOOT_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = link_DAC_boot_decode(cmd_obj->data_read,cmd_obj->data_read_len); // Decode reply
  }
  else // copy error code
    link_dac_boot.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nLink DAC (boot) (0xE5): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset link_dac struct
// Author: A.Parenti Sep07 2006
// Modified: AP, Dec11 2006
void Cccb::link_DAC_boot_reset(){
  if (this==NULL) return; // Object deleted -> return

  link_dac_boot.code1 = link_dac_boot.code2 = 0;

  link_dac_boot.ccbserver_code=-5;
  link_dac_boot.msg="";
}


// Decode "Link DAC (boot)" (0xE5) reply
// Author:  A.Parenti Sep07 2006
// Modified: AP, Dec11 2006
int Cccb::link_DAC_boot_decode(unsigned char *rdstr, int rdstr_len) {
  const unsigned char command_code=0xE5, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&link_dac_boot.code1);
  idx = rstring(rdstr,idx,&link_dac_boot.code2);

// Fill-up the error code
  if (link_dac_boot.code1==CCB_BUSY_CODE) // ccb is busy
    link_dac_boot.ccbserver_code = -1;
  else if (link_dac_boot.code1==CCB_UNKNOWN1 && link_dac_boot.code2==CCB_UNKNOWN2) // ccb: unknown command
    link_dac_boot.ccbserver_code = 1;
  else if (link_dac_boot.code1==right_reply && link_dac_boot.code2==command_code) // ccb: right reply
    link_dac_boot.ccbserver_code = 0;
  else // wrong ccb reply
    link_dac_boot.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  link_dac_boot.msg="\nLink DAC (boot) (0xE5)\n";

  if (link_dac_boot.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(link_dac_boot.ccbserver_code)); link_dac_boot.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",link_dac_boot.code1); link_dac_boot.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",link_dac_boot.code2); link_dac_boot.msg+=tmpstr;
  }

  return idx;
}


// Print results to standard output
// Author:  A.Parenti, Sep07 2006
// Modified: AP, Dec11 2006
void Cccb::link_DAC_boot_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",link_dac_boot.msg.c_str());
}



/********************************************/
/* "Protect/Unprotect Flash" (0xED) command */
/********************************************/

// Send "Protect/Unprotect Flash" (0xED) command and decode reply
// Author:  A.Parenti, Nov02 2007
void Cccb::protect_FLASH(char protect) {
  const unsigned char command_code=0xED;
  unsigned char command_line[2];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=protect;

  protect_FLASH_reset(); // Reset protect_flash struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,2,PROTECT_FLASH_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = protect_FLASH_decode(cmd_obj->data_read,cmd_obj->data_read_len); // Decode reply
  }
  else // copy error code
    protect_flash.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nProtect/Unprotect Flash (0xED): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset protect_flash struct
// Author: A.Parenti Nov02 2007
void Cccb::protect_FLASH_reset(){
  if (this==NULL) return; // Object deleted -> return

  protect_flash.code1 = protect_flash.code2 = 0;

  protect_flash.ccbserver_code=-5;
  protect_flash.msg="";
}


// Decode "Protect/Unprotect Flash" (0xED) reply
// Author:  A.Parenti Nov02 2007
int Cccb::protect_FLASH_decode(unsigned char *rdstr, int rdstr_len) {
  const unsigned char command_code=0xED, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&protect_flash.code1);
  idx = rstring(rdstr,idx,&protect_flash.code2);

// Fill-up the error code
  if (protect_flash.code1==CCB_BUSY_CODE) // ccb is busy
    protect_flash.ccbserver_code = -1;
  else if (protect_flash.code1==CCB_UNKNOWN1 && protect_flash.code2==CCB_UNKNOWN2) // ccb: unknown command
    protect_flash.ccbserver_code = 1;
  else if (protect_flash.code1==right_reply && protect_flash.code2==command_code) // ccb: right reply
    protect_flash.ccbserver_code = 0;
  else // wrong ccb reply
    protect_flash.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  protect_flash.msg="\nProtect/Unprotect Flash (0xED)\n";

  if (protect_flash.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(protect_flash.ccbserver_code)); protect_flash.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",protect_flash.code1); protect_flash.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",protect_flash.code2); protect_flash.msg+=tmpstr;
  }

  return idx;
}


// Print results to standard output
// Author:  A.Parenti, Nov02 2007
void Cccb::protect_FLASH_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",protect_flash.msg.c_str());
}




/********************************/
/* "Erase Flash" (0xEB) command */
/********************************/

// Send "Erase Flash" (0xEB) command and decode reply
// Author:  A.Parenti, Nov02 2007
void Cccb::erase_FLASH() {
  const unsigned char command_code=0xEB;
  unsigned char command_line[1];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;

  erase_FLASH_reset(); // Reset erase_flash struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,2,ERASE_FLASH_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = erase_FLASH_decode(cmd_obj->data_read,cmd_obj->data_read_len); // Decode reply
  }
  else // copy error code
    erase_flash.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nErase Flash (0xEB): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset erase_flash struct
// Author: A.Parenti Nov02 2007
void Cccb::erase_FLASH_reset(){
  if (this==NULL) return; // Object deleted -> return

  erase_flash.code1 = erase_flash.code2 = 0;

  erase_flash.ccbserver_code=-5;
  erase_flash.msg="";
}


// Decode "Erase Flash" (0xEB) reply
// Author:  A.Parenti Nov02 2007
int Cccb::erase_FLASH_decode(unsigned char *rdstr, int rdstr_len) {
  const unsigned char command_code=0xEB, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&erase_flash.code1);
  idx = rstring(rdstr,idx,&erase_flash.code2);

// Fill-up the error code
  if (erase_flash.code1==CCB_BUSY_CODE) // ccb is busy
    erase_flash.ccbserver_code = -1;
  else if (erase_flash.code1==CCB_UNKNOWN1 && erase_flash.code2==CCB_UNKNOWN2) // ccb: unknown command
    erase_flash.ccbserver_code = 1;
  else if (erase_flash.code1==right_reply && erase_flash.code2==command_code) // ccb: right reply
    erase_flash.ccbserver_code = 0;
  else // wrong ccb reply
    erase_flash.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  erase_flash.msg="\nErase Flash (0xEB)\n";

  if (erase_flash.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(erase_flash.ccbserver_code)); erase_flash.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",erase_flash.code1); erase_flash.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",erase_flash.code2); erase_flash.msg+=tmpstr;
  }

  return idx;
}


// Print results to standard output
// Author:  A.Parenti, Nov02 2007
void Cccb::erase_FLASH_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",erase_flash.msg.c_str());
}



/********************************/
/* "Write Flash" (0xF3) command */
/********************************/

// Send "Write Flash" (0xF3) command and decode reply
// Author:  A.Parenti, Nov02 2007
void Cccb::write_FLASH(char page, short addr,unsigned char data[128]) {
  const unsigned char command_code=0xF3;
  unsigned char command_line[132];
  int send_command_result, idx=0, i;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=page;
  wstring(addr,2,command_line);
  for (i=0;i<128;++i)
    command_line[4+i]=data[i];

  write_FLASH_reset(); // Reset wr_flash struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,132,WRITE_FLASH_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = write_FLASH_decode(cmd_obj->data_read,cmd_obj->data_read_len); // Decode reply
  }
  else // copy error code
    wr_flash.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nWrite Flash (0xF3): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset wr_flash struct
// Author: A.Parenti Nov02 2007
void Cccb::write_FLASH_reset(){
  if (this==NULL) return; // Object deleted -> return

  wr_flash.code = 0;
  wr_flash.error = wr_flash.sector = 0;

  wr_flash.ccbserver_code=-5;
  wr_flash.msg="";
}


// Decode "Write Flash" (0xF3) reply
// Author:  A.Parenti Nov02 2007
int Cccb::write_FLASH_decode(unsigned char *rdstr, int rdstr_len) {
  const unsigned char right_reply=0xF2;
  unsigned char code2;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&wr_flash.code);
  code2 = rdstr[idx];

// Fill-up the "result" structure
  idx = rstring(rdstr,idx,&wr_flash.error);
  idx = rstring(rdstr,idx,&wr_flash.sector);


// Fill-up the error code
  if (wr_flash.code==CCB_BUSY_CODE) // ccb is busy
    wr_flash.ccbserver_code = -1;
  else if (wr_flash.code==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    wr_flash.ccbserver_code = 1;
  else if (wr_flash.code==right_reply) // ccb: right reply
    wr_flash.ccbserver_code = 0;
  else // wrong ccb reply
    wr_flash.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  wr_flash.msg="\nWrite Flash (0xF3)\n";

  if (wr_flash.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(wr_flash.ccbserver_code)); wr_flash.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code: %#2X\n",wr_flash.code); wr_flash.msg+=tmpstr;
    sprintf(tmpstr,"error: %d\n",wr_flash.error); wr_flash.msg+=tmpstr;
    sprintf(tmpstr,"sector: %#2X\n",wr_flash.sector); wr_flash.msg+=tmpstr;
    if (wr_flash.error==0)
      wr_flash.msg+="Result: write ok\n";
    else {
      if (wr_flash.sector==0)
        wr_flash.msg+="Result: write error (128 byte flash sector)\n";
      if (wr_flash.sector==0xFF)
        wr_flash.msg+="Result: write error (256 byte flash sector)\n";
      else
        wr_flash.msg+="Result: write error\n";
    }
  }

  return idx;
}


// Print results to standard output
// Author:  A.Parenti, Nov02 2007
void Cccb::write_FLASH_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",wr_flash.msg.c_str());
}
