#include <Cconfupdate.h>
#include <Cdef.h>

#include <cmath>
#include <cstdio>

#ifdef __USE_CCBDB__ // DB access enabled
  #include <ccbdb/Cccbmap.h> // support for cmsdtdb DB
#endif

/*****************************************************************************/
// Decode/update command lines for theta-BTI configuration (0x54 code)
/*****************************************************************************/

// Decode command lines for theta-BTI configuration (0x54 code)
// Author: A.Parenti, Mar06 2007
int bti_theta_decode(unsigned char *cmdline, Sbti_theta_param *parameters) {

// Parameters
  int KMIN, KMAX; // intermediate values

  if (cmdline[0]!=0x54) // It's not a BTI config.
    return -1;

  if (cmdline[1]!=6 && cmdline[1]!=7) // It's not a theta BTI.
    return -1;

  if (cmdline[2]>31) // Wrong BTI Nr.
    return -1;

// Evaluate intermediate values
  KMIN = cmdline[10];
  KMAX = cmdline[9];

// Extract parameters
  parameters->tolerance = (KMAX-KMIN-1)/2;

  return 0;
}


// Update command lines for theta-BTI configuration (0x54 code)
// Author: A.Parenti, Feb28 2007
// Modified: AP, Mar16 2007
int bti_theta_update(unsigned char *cmdline, Sbti_theta_param parameters) {
  const int channel_map[32]={0,1,2,3,16,17,18,19,4,5,6,7,20,21,22,23,8,9,10,11,24,25,26,27,12,13,14,15,28,29,30,31}; // BTI to channel mapping

// Parameters
  int channel, BTIC, sign; // parameters
  const float pitch=4.2, h=1.3; // constant parameters
  float R, z, DISTP2; // parameters
  int st43, re43, psimax, psimin; // intermediate values
  float XMAX; // intermediate values
  int KMIN, KMAX; // final values

  if (cmdline[0]!=0x54) // It's not a BTI config.
    return -1;

  if (cmdline[1]!=6 && cmdline[1]!=7) // It's not a theta BTI.
    return -1;

  if (cmdline[2]>31) // Wrong BTI Nr.
    return -1;

  channel = channel_map[cmdline[2]]+32*(cmdline[1]-6);
  st43    = cmdline[3] & 0x3f;
  re43    = (cmdline[4] >> 4) & 0x03;
  BTIC    = (3*st43+re43)/4;
  DISTP2  = 2*BTIC*h/pitch;

  if (parameters.ccbid<=0) {
    R = parameters.R;
    z = parameters.z;
    sign = parameters.sign;
  } else {
#ifdef __USE_CCBDB__ // DB access enabled
    ccbmap *myccbmap = new ccbmap;
    int resu;

    resu=myccbmap->retrieve_BTItheta(parameters.ccbid,&R,&z,&sign);
    delete myccbmap;

    if (resu!=0) return -1; // DB query error -> return

#else
    printf("DB access needed!\n");
    return -1;
#endif
  }

// Evaluate intermediate values
  XMAX   = z + sign*channel*pitch;
  psimax = (int)floor(-sign*1000.*atan(XMAX/R)); // (mrad)
  psimin = psimax;

// Evaluate final values
  KMIN = (int)floor(DISTP2*tan(psimin/1000.))+BTIC-parameters.tolerance;
  KMAX = KMIN+1+2*parameters.tolerance;

// Back in 0x00-0x3F range
  KMIN = KMIN >? 0x00;
  KMIN = KMIN <? 0x3F;

  KMAX = KMAX >? 0x00;
  KMAX = KMAX <? 0x3F;

// Debug
//  printf("\n");
//  printf("*** BRD=%d BTI=%d ch=%d BTIC=%d ***\n",cmdline[1],cmdline[2],channel,BTIC);
//  printf("XMAX: %f, psimax: %d\n",XMAX,psimax);
//  printf("KMAX: %d -> %d\n",cmdline[9],KMAX);
//  printf("KMIN: %d -> %d\n",cmdline[10],KMIN);

// Replace bytes in command line
  cmdline[9]  = KMAX;
  cmdline[10] = KMIN;

  return 0;
}


/*****************************************************************************/
// Generic mask commands
/*****************************************************************************/
const short MaxPhiCh[8]={0,49,60,72,96,48,72,60}; // Phi channels per Layer Vs. MBtype
const short MaxTheCh[8]={0,57,57,57,0,0,0,0}; // Theta channels per Layer Vs. MBtype
const short Ch2Layer[4]={3,1,2,0}; // LayerNr as a function of ChannelNr


// Return mbtype, chimney, mb4_8 from DB. Input: ccbid.
// Author: A.Parenti, Jul09 2007
int mbtype_from_db(short ccbid,short *mbtype,bool *chimney, bool *mb4_8) {

// Reset all
  *mbtype=0;
  *chimney=false;
  *mb4_8=false;

#ifdef __USE_CCBDB__ // DB access enabled
    ccbmap *myccbmap = new ccbmap;
    int resu;

    resu=myccbmap->retrieve(ccbid);
    if (resu!=0) return -1; // DB query error -> return

    if (myccbmap->station==1 || myccbmap->station==2 || myccbmap->station==3) {
      *mbtype=myccbmap->station;
      if ((myccbmap->wheel==1 && myccbmap->sector==4) || (myccbmap->wheel==-1 && myccbmap->sector==3)) *chimney=true;
    } else if (myccbmap->station==4 && (myccbmap->sector==1 || myccbmap->sector==2 || myccbmap->sector==3 || myccbmap->sector==5 || myccbmap->sector==6 || myccbmap->sector==7)) {
      *mbtype=4;
      if (myccbmap->wheel==-1 && myccbmap->sector==3) *chimney=true;
    } else if (myccbmap->station==4 && (myccbmap->sector==8 || myccbmap->sector==12)) {
      *mbtype=4;
      *mb4_8=true;
    } else if (myccbmap->station==4 && (myccbmap->sector==9 || myccbmap->sector==11)) {
      *mbtype=5;
    } else if (myccbmap->station==4 && (myccbmap->sector==4 || myccbmap->sector==13)) {
      *mbtype=6;
      if (myccbmap->wheel==1) *chimney=true;
    } else if (myccbmap->station==4 && (myccbmap->sector==10 || myccbmap->sector==14)) {
      *mbtype=7;
    }

    delete myccbmap;
    return 0;

#else
    printf("DB access needed!\n");
    return -1;
#endif
}


// Returns true if <ch> exists in the given chamber type
// Author: A.Parenti, Jul04 2007
bool dt_channel_exist(int ch, short SLayer, short mbtype, bool chimney, bool mb4_8) {
  bool ChExist=true;

  if (mbtype<1 || mbtype>7) return false; // non-existing mbtype
  if (SLayer<0 || SLayer>2) return false; // non-existing SLayer

  if (SLayer==0 || SLayer==2) { // Phi SL
    if (ch<0 || ch>=MaxPhiCh[mbtype]) ChExist=false;
    if (mbtype==4 && mb4_8 && ch>=(MaxPhiCh[mbtype]-4)) ChExist=false; // mb4_8: like mb4 but last phi FEB (ie 4 channels) is missing
  } else if (SLayer==1) { // Theta SL
    if (ch<0 || ch>=MaxTheCh[mbtype]) ChExist=false;
    if ((mbtype==1 || mbtype==2 || mbtype==3) && chimney && ch>=(MaxTheCh[mbtype]-9)) ChExist=false; // Chimney: last two theta FEBs (ie 9 channels) are missing
  }

  return ChExist;

}

// Reset a mask
// Author: A.Parenti, Jun26 2007
void mask_reset(bool mask[3][4][96]) {
  int i, j, k;
  for (i=0;i<3;++i) 
    for (j=0;j<4;++j)
      for (k=0;k<96;++k)
        mask[i][j][k]=0;
}


// Print a mask to stout
// Author: A.Parenti, Jun01 2007
// Modified/Tested: AP, Jul04 2007
void mask_print(bool mask[3][4][96], short mbtype, bool chimney, bool mb4_8) {
  int j, k;

  mbtype=(mbtype<=7)?mbtype:0;
  mbtype=(mbtype>=0)?mbtype:0;

// SLayer PHI2
  printf("** Legenda: %so%s=ch_enabled, %sx%s=ch_masked **\n",GRNBKG,STDBKG,REDBKG,STDBKG); 
  printf("   SL PHI2\n");
  for (j=3;j>=0;--j) { // Loop over Layer
    printf("L%d ",j+1);
    for (k=0;k<MaxPhiCh[mbtype];++k) // Loop over channels
      if (!dt_channel_exist(k,2,mbtype,chimney,mb4_8)) printf("%s ",STDBKG);
      else if (mask[2][j][k]) printf("%sx",REDBKG);
      else printf("%so",GRNBKG);
    printf("%s\n",STDBKG);
  }

// SLayer THETA
  printf("   SL THETA\n");
  for (j=3;j>=0;--j) { // Loop over Layer
    printf("L%d ",j+1);
    for (k=0;k<MaxTheCh[mbtype];++k) // Loop over channels
      if (!dt_channel_exist(k,1,mbtype,chimney,mb4_8)) printf("%s ",STDBKG);
      else if (mask[1][j][k]) printf("%sx",REDBKG);
      else printf("%so",GRNBKG);
    printf("%s\n",STDBKG);
  }

// SLayer PHI1
  printf("   SL PHI1\n");
  for (j=3;j>=0;--j) { // Loop over Layer
    printf("L%d ",j+1);
    for (k=0;k<MaxPhiCh[mbtype];++k) // Loop over channels
      if (!dt_channel_exist(k,0,mbtype,chimney,mb4_8)) printf("%s ",STDBKG);
      else if (mask[0][j][k]) printf("%sx",REDBKG);
      else printf("%so",GRNBKG);
    printf("%s\n",STDBKG);
  }
}

/*****************************************************************************/
// Decode/update command lines for FE mask configuration (0x3A/0x3B codes)
/*****************************************************************************/

// Decode command lines for FE mask configuration (0x3A/0x3B codes)
// Author: A.Parenti, Jun01 2007
// Modified/Tested: AP, Jun25 2007
int fe_mask_decode(unsigned char *cmdline, Sfe_mask_param *parameters) {
  int i, j, k;

  bool bmask;
  short mbtype, SLayer, Layer, SLchannel, Lchannel, ChStatus;

  if (cmdline[0]!=0x3A && cmdline[0]!=0x3B) // It's not a FE mask
    return -1;

  mbtype=(parameters->mbtype<=7)?parameters->mbtype:0;
  mbtype=(parameters->mbtype>=0)?parameters->mbtype:0;

  if (cmdline[0]==0x3A) { // FEB mask command
    bool Feb20;
    char Byte0, Byte1, Byte2; // FEB mask
    short FebNr;
    int ChOffset;

    SLayer=(cmdline[1]<<8) + cmdline[2]; // Read SuperLayer number (SL)
    FebNr=(cmdline[3]<<8) + cmdline[4]; // Read FEB number
    Byte0=cmdline[5]; // Read mask, Byte0
    Byte1=cmdline[6]; // Read mask, Byte1
    Byte2=cmdline[7]; // Read mask, Byte2

    ChOffset=FebNr*4;

// Is this a FEB20 board?
    Feb20=false;
    if (mbtype==1 && (SLayer==0 || SLayer==2) && FebNr==11) Feb20=true;
    if (SLayer==1 && FebNr==13) Feb20=true;

// Byte0: FEB channels 0:7
    for (i=0;i<8;++i) { // Loop over bits
      Layer = Ch2Layer[i%4]; // Layer Number
      Lchannel=int(i/4)+ChOffset; // Channel Number (on Layer)
      bmask=(Byte0>>i)&1; // Channel on/off

      parameters->femask[SLayer][Layer][Lchannel]=bmask; // Copy mask
    }

// Byte1: FEB channels 8:15
    for (i=0;i<8;++i) { // Loop over bits
      Layer = Ch2Layer[i%4]; // Layer Number
      Lchannel=int(i/4)+ChOffset+2; // Channel Number (on Layer)
      bmask=(Byte1>>i)&1; // Channel on/off

      parameters->femask[SLayer][Layer][Lchannel]=bmask; // Copy mask
    }

// Byte2: FEB channels 16:19 (only FEB20)
    if (Feb20)
      for (i=0;i<4;++i) { // Loop over bits
        Layer = Ch2Layer[i%4]; // Layer Number
        Lchannel=int(i/4)+ChOffset+4; // Channel Number (on Layer)
        bmask=(Byte2>>i)&1; // Channel on/off

        parameters->femask[SLayer][Layer][Lchannel]=bmask; // Copy mask
      }
  }

  if (cmdline[0]==0x3B) { // FE Channel mask command

    SLayer=(cmdline[1]<<8) + cmdline[2]; // SuperLayer number (SL)
    SLchannel=(cmdline[3]<<8) + cmdline[4]; // Channel number in SL

    ChStatus=(cmdline[5]<<8) + cmdline[6]; // Channel enabled if 0
    bmask=(ChStatus!=0);

    Layer = Ch2Layer[SLchannel%4]; // Layer number
    Lchannel=int(SLchannel/4); // Channel number in Layer

    parameters->femask[SLayer][Layer][Lchannel]=bmask; // Copy mask
  }


// Mask non-existing channels.
  for (j=0;j<4;++j) { // Loop on layers
    for (k=MaxPhiCh[mbtype];k<96;++k) {
      parameters->femask[0][j][k]=1; // Phi1 SL
      parameters->femask[2][j][k]=1; // Phi2 SL
    }
    for (k=MaxTheCh[mbtype];k<96;++k) {
      parameters->femask[1][j][k]=1; // Theta SL
    }
  }

  return 0;
}


// Update command lines for FE mask configuration (0x3A/0x3B codes)
// Author: A.Parenti, Jun06 2007
// Modified/Tested: AP, Jun25 2007
int fe_mask_update(unsigned char *cmdline, Sfe_mask_param parameters) {
  int i;

  bool bmask;
  short mbtype, SLayer, Layer, SLchannel, Lchannel;

  if (cmdline[0]!=0x3A && cmdline[0]!=0x3B) // It's not a FE mask
    return -1;

  mbtype=(parameters.mbtype<=7)?parameters.mbtype:0;
  mbtype=(parameters.mbtype>=0)?parameters.mbtype:0;

  if (cmdline[0]==0x3A) { // FEB mask command
    bool Feb20;
    char Byte0=0, Byte1=0, Byte2=0; // FEB mask
    short FebNr;
    int ChOffset;

    SLayer=(cmdline[1]<<8) + cmdline[2]; // SuperLayer number (SL)
    FebNr=(cmdline[3]<<8) + cmdline[4]; // FEB number

    ChOffset=FebNr*4;

// Is this a FEB20 board?
    Feb20=false;
    if (mbtype==1 && (SLayer==0 || SLayer==2) && FebNr==11) Feb20=true;
    if (SLayer==1 && FebNr==13) Feb20=true;

// Byte0: FEB channels 0:7
    for (i=0;i<8;++i) { // Loop over bits
      Layer = Ch2Layer[i%4]; // Layer Number
      Lchannel=int(i/4)+ChOffset; // Channel Number (on Layer)

      bmask=parameters.femask[SLayer][Layer][Lchannel]; // Read new mask

      Byte0 |= (bmask<<i); // Copy channel mask to FEB mask
    }

// Byte1: FEB channels 8:15
    for (i=0;i<8;++i) { // Loop over bits
      Layer = Ch2Layer[i%4]; // Layer Number
      Lchannel=int(i/4)+ChOffset+2; // Channel Number (on Layer)

      bmask=parameters.femask[SLayer][Layer][Lchannel]; // Read new mask

      Byte1 |= (bmask<<i); // Copy channel mask to FEB mask
    }

// Byte2: FEB channels 16:19 (only FEB20)
    if (Feb20)
      for (i=0;i<4;++i) { // Loop over bits
        Layer = Ch2Layer[i%4]; // Layer Number
        Lchannel=int(i/4)+ChOffset+4; // Channel Number (on Layer)

        bmask=parameters.femask[SLayer][Layer][Lchannel]; // Read new mask

        Byte2 |= (bmask<<i); // Copy channel mask to FEB mask
      }

    cmdline[5]=Byte0; // Update mask, Byte0
    cmdline[6]=Byte1; // Update mask, Byte1
    cmdline[7]=Byte2; // Update mask, Byte2

  }


  if (cmdline[0]==0x3B) { // FE Channel mask command

    SLayer=(cmdline[1]<<8) + cmdline[2]; // SuperLayer number (SL)
    SLchannel=(cmdline[3]<<8) + cmdline[4]; // Channel number in SL

    Layer = Ch2Layer[SLchannel%4]; // Layer number
    Lchannel=int(SLchannel/4); // Channel number in Layer

    bmask=parameters.femask[SLayer][Layer][Lchannel]; // Read mask

    cmdline[5]=0; // Update mask
    cmdline[6]=bmask?1:0; // Update mask (Channel enabled if 0)

  }

  return 0;
}


// Print FE mask to stdout
// Author: A.Parenti, Jul03 2007
void fe_mask_print(Sfe_mask_param parameters) {
  mask_print(parameters.femask,parameters.mbtype,parameters.chimney,parameters.mb4_8);
}



/*****************************************************************************/
// Decode/update command lines for TDC mask configuration (0x18/0x44 codes)
/*****************************************************************************/

// Decode command lines for TDC mask configuration (0x18/0x44 codes)
// Author: A.Parenti, Jul04 2007
// Mofigied/Tested: A.Parenti, Jul12 2007
int tdc_mask_decode(unsigned char *cmdline, Stdc_mask_param *parameters) {
  int i, j, k;

  bool PhiSL, ThetaSL;
  unsigned char brd, chip, control[5];
  short mbtype, SLayer;
  int ChOffset;

  if (cmdline[0]!=0x18 && cmdline[0]!=0x44) // It's not a TDC mask
    return -1;

  mbtype=(parameters->mbtype<=7)?parameters->mbtype:0;
  mbtype=(parameters->mbtype>=0)?parameters->mbtype:0;

  brd=cmdline[1]; // Board Nr.
  chip=cmdline[2]; // Chip Nr.

  if (brd>6) return -1; // Non-existing board
  if (chip>3) return -1; // Non existing chip

// Copy "control" parameter from commmand line
  if (cmdline[0]==0x18)
    for (i=0;i<5;++i)
      control[i] = cmdline[3+i];
  else if (cmdline[0]==0x44)
    for (i=0;i<5;++i)
      control[i] = cmdline[84+i];

// Is it Phi or Theta board?
  ThetaSL=PhiSL=false;
  switch (mbtype) {
  case 1: // mb1
  case 2: // mb2
    if (brd<=3) PhiSL=true;
    else if (brd<=5) {ThetaSL=true; brd-=4;}
    break;
  case 3: // mb3
    if (brd<=4) PhiSL=true;
    else if (brd<=6) {ThetaSL=true; brd-=5;}
    break;
  case 4: // mb4-s, mb4_8
    if (brd<=5) PhiSL=true;
    break;
  case 5: // mb4-9
    if (brd<=2) PhiSL=true;
    break;
  case 6: // mb4-4
    if (brd<=4) PhiSL=true;
    break;
  case 7: // mb4-10
    if (brd<=3) PhiSL=true;
    break;
  }

  if (!PhiSL && !ThetaSL) return -1; // Not PhiSL nor ThetaSL

// Extract TDC mask
  bool TDCmask[32];
  for (i=0;i<3;++i) // Bits 0:2
    TDCmask[i]=!((control[0]>>(5+i))&1);
  for (i=0;i<8;++i) // Bits 3:10
    TDCmask[3+i]=!((control[1]>>i)&1);
  for (i=0;i<8;++i) // Bits 11:18
    TDCmask[11+i]=!((control[2]>>i)&1);
  for (i=0;i<8;++i) // Bits 19:26
    TDCmask[19+i]=!((control[3]>>i)&1);
  for (i=0;i<5;++i) // Bits 27:31
    TDCmask[27+i]=!((control[4]>>i)&1);

// Now updates parameters->tdcmask
  if (PhiSL) {
    ChOffset=brd*16+chip*4;

// Phi2 SL
    SLayer=2;

    parameters->tdcmask[SLayer][3][ChOffset]|=TDCmask[0];
    parameters->tdcmask[SLayer][1][ChOffset]|=TDCmask[1];
    parameters->tdcmask[SLayer][2][ChOffset]|=TDCmask[2];
    parameters->tdcmask[SLayer][0][ChOffset]|=TDCmask[3];

    parameters->tdcmask[SLayer][3][ChOffset+1]|=TDCmask[4];
    parameters->tdcmask[SLayer][1][ChOffset+1]|=TDCmask[5];
    parameters->tdcmask[SLayer][2][ChOffset+1]|=TDCmask[6];
    parameters->tdcmask[SLayer][0][ChOffset+1]|=TDCmask[7];

    parameters->tdcmask[SLayer][3][ChOffset+2]|=TDCmask[8];
    parameters->tdcmask[SLayer][1][ChOffset+2]|=TDCmask[9];
    parameters->tdcmask[SLayer][2][ChOffset+2]|=TDCmask[10];
    parameters->tdcmask[SLayer][0][ChOffset+2]|=TDCmask[11];

    parameters->tdcmask[SLayer][3][ChOffset+3]|=TDCmask[12];
    parameters->tdcmask[SLayer][1][ChOffset+3]|=TDCmask[13];
    parameters->tdcmask[SLayer][2][ChOffset+3]|=TDCmask[14];
    parameters->tdcmask[SLayer][0][ChOffset+3]|=TDCmask[15];

// Phi1 SL
    SLayer=0;

    parameters->tdcmask[SLayer][3][ChOffset]|=TDCmask[31];
    parameters->tdcmask[SLayer][1][ChOffset]|=TDCmask[30];
    parameters->tdcmask[SLayer][2][ChOffset]|=TDCmask[29];
    parameters->tdcmask[SLayer][0][ChOffset]|=TDCmask[28];

    parameters->tdcmask[SLayer][3][ChOffset+1]|=TDCmask[27];
    parameters->tdcmask[SLayer][1][ChOffset+1]|=TDCmask[26];
    parameters->tdcmask[SLayer][2][ChOffset+1]|=TDCmask[25];
    parameters->tdcmask[SLayer][0][ChOffset+1]|=TDCmask[24];

    parameters->tdcmask[SLayer][3][ChOffset+2]|=TDCmask[23];
    parameters->tdcmask[SLayer][1][ChOffset+2]|=TDCmask[22];
    parameters->tdcmask[SLayer][2][ChOffset+2]|=TDCmask[21];
    parameters->tdcmask[SLayer][0][ChOffset+2]|=TDCmask[20];

    parameters->tdcmask[SLayer][3][ChOffset+3]|=TDCmask[19];
    parameters->tdcmask[SLayer][1][ChOffset+3]|=TDCmask[18];
    parameters->tdcmask[SLayer][2][ChOffset+3]|=TDCmask[17];
    parameters->tdcmask[SLayer][0][ChOffset+3]|=TDCmask[16];

  } else if (ThetaSL) {
    SLayer=1; // Theta SL

    ChOffset=brd*32+chip*4;

    parameters->tdcmask[SLayer][3][ChOffset+16]|=TDCmask[0];
    parameters->tdcmask[SLayer][1][ChOffset+16]|=TDCmask[1];
    parameters->tdcmask[SLayer][2][ChOffset+16]|=TDCmask[2];
    parameters->tdcmask[SLayer][0][ChOffset+16]|=TDCmask[3];

    parameters->tdcmask[SLayer][3][ChOffset+17]|=TDCmask[4];
    parameters->tdcmask[SLayer][1][ChOffset+17]|=TDCmask[5];
    parameters->tdcmask[SLayer][2][ChOffset+17]|=TDCmask[6];
    parameters->tdcmask[SLayer][0][ChOffset+17]|=TDCmask[7];

    parameters->tdcmask[SLayer][3][ChOffset+18]|=TDCmask[8];
    parameters->tdcmask[SLayer][1][ChOffset+18]|=TDCmask[9];
    parameters->tdcmask[SLayer][2][ChOffset+18]|=TDCmask[10];
    parameters->tdcmask[SLayer][0][ChOffset+18]|=TDCmask[11];

    parameters->tdcmask[SLayer][3][ChOffset+19]|=TDCmask[12];
    parameters->tdcmask[SLayer][1][ChOffset+19]|=TDCmask[13];
    parameters->tdcmask[SLayer][2][ChOffset+19]|=TDCmask[14];
    parameters->tdcmask[SLayer][0][ChOffset+19]|=TDCmask[15];

    parameters->tdcmask[SLayer][3][ChOffset]|=TDCmask[31];
    parameters->tdcmask[SLayer][1][ChOffset]|=TDCmask[30];
    parameters->tdcmask[SLayer][2][ChOffset]|=TDCmask[29];
    parameters->tdcmask[SLayer][0][ChOffset]|=TDCmask[28];

    parameters->tdcmask[SLayer][3][ChOffset+1]|=TDCmask[27];
    parameters->tdcmask[SLayer][1][ChOffset+1]|=TDCmask[26];
    parameters->tdcmask[SLayer][2][ChOffset+1]|=TDCmask[25];
    parameters->tdcmask[SLayer][0][ChOffset+1]|=TDCmask[24];

    parameters->tdcmask[SLayer][3][ChOffset+2]|=TDCmask[23];
    parameters->tdcmask[SLayer][1][ChOffset+2]|=TDCmask[22];
    parameters->tdcmask[SLayer][2][ChOffset+2]|=TDCmask[21];
    parameters->tdcmask[SLayer][0][ChOffset+2]|=TDCmask[20];

    parameters->tdcmask[SLayer][3][ChOffset+3]|=TDCmask[19];
    parameters->tdcmask[SLayer][1][ChOffset+3]|=TDCmask[18];
    parameters->tdcmask[SLayer][2][ChOffset+3]|=TDCmask[17];
    parameters->tdcmask[SLayer][0][ChOffset+3]|=TDCmask[16];
  }

// Mask non-existing channels.
  for (j=0;j<4;++j) { // Loop on layers
    for (k=MaxPhiCh[mbtype];k<96;++k) {
      parameters->tdcmask[0][j][k]=true; // Phi1 SL
      parameters->tdcmask[2][j][k]=true; // Phi2 SL
    }
    for (k=MaxTheCh[mbtype];k<96;++k) {
      parameters->tdcmask[1][j][k]=true; // Theta SL
    }
  }

  return 0;
}


// Update command lines for TDC mask configuration (0x18/0x44 codes)
// Author: A.Parenti, Jul06 2007
// Tested: AP, Jul06 2007
int tdc_mask_update(unsigned char *cmdline, Stdc_mask_param parameters) {
  int i, j;

  bool PhiSL, ThetaSL;
  unsigned char brd, chip, control[5];
  short mbtype, SLayer;
  int ChOffset;

  if (cmdline[0]!=0x18 && cmdline[0]!=0x44) // It's not a TDC mask
    return -1;

  mbtype=(parameters.mbtype<=7)?parameters.mbtype:0;
  mbtype=(parameters.mbtype>=0)?parameters.mbtype:0;

  brd=cmdline[1]; // Board Nr.
  chip=cmdline[2]; // Chip Nr.

  if (brd>6) return -1; // Non-existing board
  if (chip>3) return -1; // Non existing chip

// Copy "control" parameter from commmand line
  if (cmdline[0]==0x18)
    for (i=0;i<5;++i)
      control[i] = cmdline[3+i];
  else if (cmdline[0]==0x44)
    for (i=0;i<5;++i)
      control[i] = cmdline[84+i];

// Is it Phi or Theta board?
  ThetaSL=PhiSL=false;
  switch (mbtype) {
  case 1: // mb1
  case 2: // mb2
    if (brd<=3) PhiSL=true;
    else if (brd<=5) {ThetaSL=true; brd-=4;}
    break;
  case 3: // mb3
    if (brd<=4) PhiSL=true;
    else if (brd<=6) {ThetaSL=true; brd-=5;}
    break;
  case 4: // mb4-s, mb4_8
    if (brd<=5) PhiSL=true;
    break;
  case 5: // mb4-9
    if (brd<=2) PhiSL=true;
    break;
  case 6: // mb4-4
    if (brd<=4) PhiSL=true;
    break;
  case 7: // mb4-10
    if (brd<=3) PhiSL=true;
    break;
  }

  if (!PhiSL && !ThetaSL) return -1; // Not PhiSL nor ThetaSL

// Extract TDC mask
  bool TDCmask[32];

  if (PhiSL) {
    ChOffset=brd*16+chip*4;

// Phi2 SL
    SLayer=2;

    TDCmask[0]=parameters.tdcmask[SLayer][3][ChOffset];
    TDCmask[1]=parameters.tdcmask[SLayer][1][ChOffset];
    TDCmask[2]=parameters.tdcmask[SLayer][2][ChOffset];
    TDCmask[3]=parameters.tdcmask[SLayer][0][ChOffset];

    TDCmask[4]=parameters.tdcmask[SLayer][3][ChOffset+1];
    TDCmask[5]=parameters.tdcmask[SLayer][1][ChOffset+1];
    TDCmask[6]=parameters.tdcmask[SLayer][2][ChOffset+1];
    TDCmask[7]=parameters.tdcmask[SLayer][0][ChOffset+1];

    TDCmask[8]=parameters.tdcmask[SLayer][3][ChOffset+2];
    TDCmask[9]=parameters.tdcmask[SLayer][1][ChOffset+2];
    TDCmask[10]=parameters.tdcmask[SLayer][2][ChOffset+2];
    TDCmask[11]=parameters.tdcmask[SLayer][0][ChOffset+2];

    TDCmask[12]=parameters.tdcmask[SLayer][3][ChOffset+3];
    TDCmask[13]=parameters.tdcmask[SLayer][1][ChOffset+3];
    TDCmask[14]=parameters.tdcmask[SLayer][2][ChOffset+3];
    TDCmask[15]=parameters.tdcmask[SLayer][0][ChOffset+3];

// Phi1 SL
    SLayer=0;

    TDCmask[31]=parameters.tdcmask[SLayer][3][ChOffset];
    TDCmask[30]=parameters.tdcmask[SLayer][1][ChOffset];
    TDCmask[29]=parameters.tdcmask[SLayer][2][ChOffset];
    TDCmask[28]=parameters.tdcmask[SLayer][0][ChOffset];

    TDCmask[27]=parameters.tdcmask[SLayer][3][ChOffset+1];
    TDCmask[26]=parameters.tdcmask[SLayer][1][ChOffset+1];
    TDCmask[25]=parameters.tdcmask[SLayer][2][ChOffset+1];
    TDCmask[24]=parameters.tdcmask[SLayer][0][ChOffset+1];

    TDCmask[23]=parameters.tdcmask[SLayer][3][ChOffset+2];
    TDCmask[22]=parameters.tdcmask[SLayer][1][ChOffset+2];
    TDCmask[21]=parameters.tdcmask[SLayer][2][ChOffset+2];
    TDCmask[20]=parameters.tdcmask[SLayer][0][ChOffset+2];

    TDCmask[19]=parameters.tdcmask[SLayer][3][ChOffset+3];
    TDCmask[18]=parameters.tdcmask[SLayer][1][ChOffset+3];
    TDCmask[17]=parameters.tdcmask[SLayer][2][ChOffset+3];
    TDCmask[16]=parameters.tdcmask[SLayer][0][ChOffset+3];

  } else if (ThetaSL) {
    SLayer=1; // Theta SL

    ChOffset=brd*32+chip*4;

    TDCmask[0]=parameters.tdcmask[SLayer][3][ChOffset+16];
    TDCmask[1]=parameters.tdcmask[SLayer][1][ChOffset+16];
    TDCmask[2]=parameters.tdcmask[SLayer][2][ChOffset+16];
    TDCmask[3]=parameters.tdcmask[SLayer][0][ChOffset+16];

    TDCmask[4]=parameters.tdcmask[SLayer][3][ChOffset+17];
    TDCmask[5]=parameters.tdcmask[SLayer][1][ChOffset+17];
    TDCmask[6]=parameters.tdcmask[SLayer][2][ChOffset+17];
    TDCmask[7]=parameters.tdcmask[SLayer][0][ChOffset+17];

    TDCmask[8]=parameters.tdcmask[SLayer][3][ChOffset+18];
    TDCmask[9]=parameters.tdcmask[SLayer][1][ChOffset+18];
    TDCmask[10]=parameters.tdcmask[SLayer][2][ChOffset+18];
    TDCmask[11]=parameters.tdcmask[SLayer][0][ChOffset+18];

    TDCmask[12]=parameters.tdcmask[SLayer][3][ChOffset+19];
    TDCmask[13]=parameters.tdcmask[SLayer][1][ChOffset+19];
    TDCmask[14]=parameters.tdcmask[SLayer][2][ChOffset+19];
    TDCmask[15]=parameters.tdcmask[SLayer][0][ChOffset+19];

    TDCmask[31]=parameters.tdcmask[SLayer][3][ChOffset];
    TDCmask[30]=parameters.tdcmask[SLayer][1][ChOffset];
    TDCmask[29]=parameters.tdcmask[SLayer][2][ChOffset];
    TDCmask[28]=parameters.tdcmask[SLayer][0][ChOffset];

    TDCmask[27]=parameters.tdcmask[SLayer][3][ChOffset+1];
    TDCmask[26]=parameters.tdcmask[SLayer][1][ChOffset+1];
    TDCmask[25]=parameters.tdcmask[SLayer][2][ChOffset+1];
    TDCmask[24]=parameters.tdcmask[SLayer][0][ChOffset+1];

    TDCmask[23]=parameters.tdcmask[SLayer][3][ChOffset+2];
    TDCmask[22]=parameters.tdcmask[SLayer][1][ChOffset+2];
    TDCmask[21]=parameters.tdcmask[SLayer][2][ChOffset+2];
    TDCmask[20]=parameters.tdcmask[SLayer][0][ChOffset+2];

    TDCmask[19]=parameters.tdcmask[SLayer][3][ChOffset+3];
    TDCmask[18]=parameters.tdcmask[SLayer][1][ChOffset+3];
    TDCmask[17]=parameters.tdcmask[SLayer][2][ChOffset+3];
    TDCmask[16]=parameters.tdcmask[SLayer][0][ChOffset+3];
  }


// Reset mask bits in "control"
  control[0]=control[0]&0x1F; // bits 0:4 are not masks
  control[1]=control[2]=control[3]=0;
  control[4]=control[4]&0xE0; // bits 5:7 are not masks

// Now copy TDCmask to "control" parameter
  for (i=0;i<3;++i) // Bits 0:2
    control[0] |= (!TDCmask[i])<<(5+i);
  for (i=0;i<8;++i) // Bits 3:10
    control[1] |= (!TDCmask[3+i])<<i;
  for (i=0;i<8;++i) // Bits 11:18
    control[2] |= (!TDCmask[11+i])<<i;
  for (i=0;i<8;++i) // Bits 19:26
    control[3] |= (!TDCmask[19+i])<<i;
  for (i=0;i<5;++i) // Bits 27:31
    control[4] |= (!TDCmask[27+i])<<i;

// Finally copy "control" parameter to commmand line
  if (cmdline[0]==0x18)
    for (i=0;i<5;++i)
      cmdline[3+i] = control[i];
  else if (cmdline[0]==0x44)
    for (i=0;i<5;++i)
      cmdline[84+i] = control[i];

// Parity of control[0:4]
  char parity;
  for (i=parity=0;i<5;++i)
    for (j=0;j<8;++j)
      parity+=(control[i]>>j)&1;
  parity-=(control[4]>>7)&1;
  parity%=2; // This is the parity

  control[4]=(control[4])&0x7F + (parity<<7)&80; // Update parity

  return 0;
}


// Print FE mask to stdout
// Author: A.Parenti, Jul03 2007
void tdc_mask_print(Stdc_mask_param parameters) {
  mask_print(parameters.tdcmask,parameters.mbtype,parameters.chimney,parameters.mb4_8);
}



/*****************************************************************************/
// Decode/update command lines for BTI mask configuration (0x54 code)
/*****************************************************************************/

// Decode command lines for BTI mask configuration (0x54 code)
// Author: A.Parenti, Jul19 2007
int bti_mask_decode(unsigned char *cmdline, Sbti_mask_param *parameters) {
  int i, j, k;

  bool PhiSL, ThetaSL, BTImask[9];
  unsigned char brd, chip;
  short mbtype, SLayer;
  int ChOffset;

  if (cmdline[0]!=0x54) // It's not a BTI mask
    return -1;

  mbtype=(parameters->mbtype<=7)?parameters->mbtype:0;
  mbtype=(parameters->mbtype>=0)?parameters->mbtype:0;

  brd=cmdline[1]; // Board Nr.
  chip=cmdline[2]; // Chip Nr.

  if (brd>7) return -1; // Non-existing board
  if (chip>31) return -1; // Non existing chip

// Is it Phi or Theta board?
  ThetaSL=PhiSL=false;
  switch (mbtype) {
  case 1: // mb1
    if (brd==6 || brd==7) {ThetaSL=true; brd-=6;}
    else if ((brd<3 && chip<32) || (brd==3 && chip<8)) PhiSL=true;
    break;
  case 2: // mb2
    if (brd==6 || brd==7) {ThetaSL=true; brd-=6;}
    else if (brd<4 && chip<32) PhiSL=true;
    break;
  case 3: // mb3
    if (brd==6 || brd==7) {ThetaSL=true; brd-=6;}
    else if (brd<5 && chip<32) PhiSL=true;
    break;
  case 4: // mb4-s, mb4_8
    if (brd<6 && chip<32) PhiSL=true;
    break;
  case 5: // mb4-9
    if (brd<3 && chip<32) PhiSL=true;
    break;
  case 6: // mb4-4
    if (brd<5 && chip<32) PhiSL=true;
    break;
  case 7: // mb4-10
    if (brd<4 && chip<32) PhiSL=true;
    break;
  }

  if (!PhiSL && !ThetaSL) return -1; // Not PhiSL nor ThetaSL

// Copy BTImask parameter from commmand line
  for (i=0;i<6;++i)
    BTImask[i]=!((cmdline[5]>>i)&1);
  for (i=0;i<3;++i)
    BTImask[i+6]=!((cmdline[4]>>i)&1);

// Now copy mask to parameters->btimask
  if (PhiSL) {
    if ((chip%8)<4) SLayer=0; // Phi1 SL
    else SLayer=2; // Phi2 SL

    ChOffset=brd*16+(int)(chip/8)*4+(chip%4);

    parameters->btimask[SLayer][3][ChOffset]|=BTImask[0];
    parameters->btimask[SLayer][1][ChOffset]|=BTImask[1];
    parameters->btimask[SLayer][2][ChOffset]|=BTImask[2];
    parameters->btimask[SLayer][0][ChOffset]|=BTImask[3];

    if (ChOffset<95) {
      parameters->btimask[SLayer][3][ChOffset+1]|=BTImask[4];
      parameters->btimask[SLayer][1][ChOffset+1]|=BTImask[5];
      parameters->btimask[SLayer][2][ChOffset+1]|=BTImask[6];
      parameters->btimask[SLayer][0][ChOffset+1]|=BTImask[7];
    }

    if (ChOffset<94)
      parameters->btimask[SLayer][3][ChOffset+2]|=BTImask[8];

  } else if (ThetaSL) {
    SLayer=1; // Theta SL

    if ((chip%8)<4)
      ChOffset=brd*32+ chip-4*(int)(chip/8);
    else
      ChOffset=brd*32+ chip+12-4*(int)(chip/8);

    parameters->btimask[SLayer][3][ChOffset]|=BTImask[0];
    parameters->btimask[SLayer][1][ChOffset]|=BTImask[1];
    parameters->btimask[SLayer][2][ChOffset]|=BTImask[2];
    parameters->btimask[SLayer][0][ChOffset]|=BTImask[3];

    if (ChOffset<95) {
      parameters->btimask[SLayer][3][ChOffset+1]|=BTImask[4];
      parameters->btimask[SLayer][1][ChOffset+1]|=BTImask[5];
      parameters->btimask[SLayer][2][ChOffset+1]|=BTImask[6];
      parameters->btimask[SLayer][0][ChOffset+1]|=BTImask[7];
    }

    if (ChOffset<94)
      parameters->btimask[SLayer][3][ChOffset+2]|=BTImask[8];

  }

// Mask non-existing channels.
  for (j=0;j<4;++j) { // Loop on layers
    for (k=MaxPhiCh[mbtype];k<96;++k) {
      parameters->btimask[0][j][k]=1; // Phi1 SL
      parameters->btimask[2][j][k]=1; // Phi2 SL
    }
    for (k=MaxTheCh[mbtype];k<96;++k) {
      parameters->btimask[1][j][k]=1; // Theta SL
    }
  }

  return 0;
}


// Update command lines for BTI mask configuration (0x54 code)
// Author: A.Parenti, Jul19 2007
int bti_mask_update(unsigned char *cmdline, Sbti_mask_param parameters) {
  int i;

  bool PhiSL, ThetaSL, BTImask[9];
  unsigned char brd, chip;
  short mbtype, SLayer;
  int ChOffset;

  if (cmdline[0]!=0x54) // It's not a BTI mask
    return -1;

  mbtype=(parameters.mbtype<=7)?parameters.mbtype:0;
  mbtype=(parameters.mbtype>=0)?parameters.mbtype:0;

  brd=cmdline[1]; // Board Nr.
  chip=cmdline[2]; // Chip Nr.

  if (brd>7) return -1; // Non-existing board
  if (chip>31) return -1; // Non existing chip

// Is it Phi or Theta board?
  ThetaSL=PhiSL=false;
  switch (mbtype) {
  case 1: // mb1
    if (brd==6 || brd==7) {ThetaSL=true; brd-=6;}
    else if ((brd<3 && chip<32) || (brd==3 && chip<8)) PhiSL=true;
    break;
  case 2: // mb2
    if (brd==6 || brd==7) {ThetaSL=true; brd-=6;}
    else if (brd<4 && chip<32) PhiSL=true;
    break;
  case 3: // mb3
    if (brd==6 || brd==7) {ThetaSL=true; brd-=6;}
    else if (brd<5 && chip<32) PhiSL=true;
    break;
  case 4: // mb4-s, mb4_8
    if (brd<6 && chip<32) PhiSL=true;
    break;
  case 5: // mb4-9
    if (brd<3 && chip<32) PhiSL=true;
    break;
  case 6: // mb4-4
    if (brd<5 && chip<32) PhiSL=true;
    break;
  case 7: // mb4-10
    if (brd<4 && chip<32) PhiSL=true;
    break;
  }

  if (!PhiSL && !ThetaSL) return -1; // Not PhiSL nor ThetaSL



  if (PhiSL) {
    if ((chip%8)<4) SLayer=0; // Phi1 SL
    else SLayer=2; // Phi2 SL

    ChOffset=brd*16+(int)(chip/8)*4+(chip%4);

    BTImask[0]=parameters.btimask[SLayer][3][ChOffset];
    BTImask[1]=parameters.btimask[SLayer][1][ChOffset];
    BTImask[2]=parameters.btimask[SLayer][2][ChOffset];
    BTImask[3]=parameters.btimask[SLayer][0][ChOffset];

    if (ChOffset<95) {
      BTImask[4]=parameters.btimask[SLayer][3][ChOffset+1];
      BTImask[5]=parameters.btimask[SLayer][1][ChOffset+1];
      BTImask[6]=parameters.btimask[SLayer][2][ChOffset+1];
      BTImask[7]=parameters.btimask[SLayer][0][ChOffset+1];
    }

    if (ChOffset<94)
      BTImask[8]=parameters.btimask[SLayer][3][ChOffset+2];

  } else if (ThetaSL) {
    SLayer=1; // Theta SL

    if ((chip%8)<4)
      ChOffset=brd*32+ chip-4*(int)(chip/8);
    else
      ChOffset=brd*32+ chip+12-4*(int)(chip/8);

    BTImask[0]=parameters.btimask[SLayer][3][ChOffset];
    BTImask[1]=parameters.btimask[SLayer][1][ChOffset];
    BTImask[2]=parameters.btimask[SLayer][2][ChOffset];
    BTImask[3]=parameters.btimask[SLayer][0][ChOffset];

    if (ChOffset<95) {
      BTImask[4]=parameters.btimask[SLayer][3][ChOffset+1];
      BTImask[5]=parameters.btimask[SLayer][1][ChOffset+1];
      BTImask[6]=parameters.btimask[SLayer][2][ChOffset+1];
      BTImask[7]=parameters.btimask[SLayer][0][ChOffset+1];
    }

    if (ChOffset<94)
      BTImask[8]=parameters.btimask[SLayer][3][ChOffset+2];

  }

// Reset mask bits in command line
  cmdline[5]=cmdline[5]&0xC0;
  cmdline[4]=cmdline[4]&0xF8;

// Now copy BTImask to command line
  for (i=0;i<6;++i)
    cmdline[5] |= (!BTImask[i])<<i;
  for (i=0;i<3;++i)
    cmdline[4] |= (!BTImask[i+6])<<i;

  return 0;
}


// Print BTI mask to stdout
// Author: A.Parenti, Jul10 2007
void bti_mask_print(Sbti_mask_param parameters) {
  mask_print(parameters.btimask,parameters.mbtype,parameters.chimney,parameters.mb4_8);
}
