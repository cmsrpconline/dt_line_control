#include <ccbcmds/Cfe.h>
#include <ccbcmds/apfunctions.h>

#ifdef __USE_CCBDB__ // DB access enabled
  #include <ccbdb/Cccbdata.h>
#endif

#include <iostream>
#include <cstdio>


/*****************************************************************************/
// FE class
/*****************************************************************************/

// Class constructor; arguments: pointer_to_command_object
// Author: A.Parenti, Dec01 2006
Cfe::Cfe(Ccommand* ppp) : Ccmn_cmd(ppp) {
  set_FE_thr_reset(); // Reset set_fe_thr struct
  set_FE_width_reset(); // Reset set_fe_width struct
  read_FE_temp_reset(); // Reset read_fe_temp struct
  read_FE_mask_reset(); // Reset read_fe_mask struct
  set_FE_mask_reset(); // Reset set_fe_mask struct
  set_SL_mask_reset(); // Reset set_sl_mask struct
  read_FE_Tmask_reset(); // Reset read_fe_Tmask struct
  set_FE_Tmask_reset(); // Reset set_fe_Tmask struct
  set_SL_Tmask_reset(); // Reset set_sl_Tmask struct
  FE_test_reset(); // Reset fe_test struct
  set_TP_reset(); // Reset set_tp struct
  set_rel_TP_reset(); // Reset set_rel_tp struct
  read_TP_reset(); // Reset read_tp struct
  test_TP_finedelay_reset(); // Reset test_tp_finedelay struct
  set_TP_offset_reset(); // Reset set_tp_offset struct
  read_TP_offset_reset(); // Reset read_tp_offset struct
  test_DAC_reset(); // Reset test_dac struct
  set_calib_DAC_reset(); // Reset set_calib_dac struct
  read_calib_DAC_reset(); // Reset read_calib_dac struct
}

/****************************/
/* Override ROB/TDC methods */
/****************************/

void Cfe::read_TDC(char tdc_brd, char tdc_chip) {}
int Cfe::read_TDC_decode(unsigned char *rdstr) {return 0;}
void Cfe::read_TDC_print() {}

void Cfe::status_TDC(char tdc_brd, char tdc_chip) {}
void Cfe::status_TDC_print() {}

void Cfe::read_ROB_error() {}
int Cfe::read_ROB_error_decode(unsigned char *rdstr) {return 0;}
void Cfe::read_ROB_error_print() {}

/*********************/
/* Database commands */
/*********************/

// Log FE mask in the DB
// Author: A.Parenti, Nov03 2006
// Modified/Tested: AP, Oct30 2007
void Cfe::FE_mask_to_db() {
  if (this==NULL) return; // Object deleted -> return

#ifdef __USE_CCBDB__ // DB access enabled

  ccbdata *myCcbdata;
  if (cmd_obj->dtdbobj==NULL) {
    myCcbdata=new ccbdata; // Create a new connection to DB
//    printf("ccdata: new connection\n");
  } else {
    myCcbdata=new ccbdata(cmd_obj->dtdbobj); // Use an existing connection to DB
//    printf("ccbdata: existing connection\n");
  }

  read_FE_mask(); // Read mask
  if (read_fe_mask.code==0x39) { // Right reply -> Write to DB
    myCcbdata->insert(cmd_obj->read_ccb_id(),0x38,(char *)read_fe_mask.MadMsk,216);
  }

  delete myCcbdata;
#else
  nodbmsg();
#endif
}


/****************/
/* Configure FE */
/****************/

// Configure the FE, reading from a "myconf" object
// Author: A.Parenti, Sep02 2005
// Modified: AP, Apr10 2006
// Modified: AP, Nov17 2006 (Added CRC) 
short Cfe::config_FE(Cconf *myconf) {
  if (this==NULL) return 0; // Object deleted -> return

  short conferr, confstatus, confcrc;

// FE error is BIT7 of conferr
  conferr = config_mc(myconf,CFE,-1,-1,&confstatus,&confcrc);

  if (((conferr>>7)&0x1)!=0) {
    config_FE_result.error = true;
    config_FE_result.crc = 0;
    return 1;
  }
  else {
    config_FE_result.error = false;
    config_FE_result.crc = confcrc;
    return 0;
  }
}

// Print to stdout the result of FE configuration
// Author: A.Parenti, Sep02 2005
// Modified: AP, Nov17 2006
void Cfe::config_FE_print() {
  if (this==NULL) return; // Object deleted -> return

  if (config_FE_result.sent>0) {
    printf("Configure FE: %d lines sent (%d successful)\n",config_FE_result.sent,config_FE_result.sent-config_FE_result.bad);
    printf("              CRC=%#hX\n",config_FE_result.crc);
    printf("              Error=%d\n",config_FE_result.error);
  }
}

// Retrive FE config from CCB, dump it to file
// Author: A.Parenti, Oct30 2006
void Cfe::read_FE_config(char *filename) {
  unsigned char cmdline[11];
  char strcmd[34];
  short sl, brd;
  int i;
  std::ofstream outfile(filename,std::ofstream::app);

  if (this==NULL) return; // Object deleted -> return

  printf("Read FE config (0x35/0x47/0x3A).\n");

  if (outfile.is_open()) {
// FE, 0x35

    MC_read_status();

    if (mc_status.code==0x13)
      for (sl=0;sl<3;++sl) {

        cmdline[0]=0x35;
        wstring(sl,1,cmdline);
        wstring(mc_status.Fe_Bias[sl],3,cmdline);
        wstring(mc_status.Fe_Thr[sl],7,cmdline);
        
        strcpy(strcmd,"");
        sprintf(strcmd,"%02X",cmdline[0]);
        for (i=1;i<11;++i)
          sprintf(strcmd,"%s %02X",strcmd,cmdline[i]);
        outfile << strcmd << '\n';
        outfile.flush();
      }

// FE, 0x47

    if (mc_status.code==0x13) {
      cmdline[0]=0x47;
      wstring(mc_status.Fe_Width,1,cmdline);

      strcpy(strcmd,"");
      sprintf(strcmd,"%02X",cmdline[0]);
      for (i=1;i<5;++i)
      sprintf(strcmd,"%s %02X",strcmd,cmdline[i]);
      outfile << strcmd << '\n';
      outfile.flush();
    }

// FE, 0x3A
    read_FE_mask();

    if (read_fe_mask.code==0x39) {
      for (sl=0;sl<3;++sl)
        for (brd=0;brd<=24;++brd)
          if (read_fe_mask.MadMsk[sl][brd][0]!=0 || read_fe_mask.MadMsk[sl][brd][1]!=0 || read_fe_mask.MadMsk[sl][brd][2]!=0) {

            cmdline[0]=0x3A;
            wstring(sl,1,cmdline);
            wstring(brd,3,cmdline);
            mystrcpy(cmdline+5,read_fe_mask.MadMsk[sl][brd],3);

            strcpy(strcmd,"");
            sprintf(strcmd,"%02X",cmdline[0]);
            for (i=1;i<8;++i)
              sprintf(strcmd,"%s %02X",strcmd,cmdline[i]);
            outfile << strcmd << '\n';
            outfile.flush();
          }
    }

    outfile.close();
  } else
    printf("Impossible to open file '%s'.\n",filename);

}


/*************************************/
/* "set FE threshold" (0x35) command */
/*************************************/

// Send "set FE threshold" (0x35) command and decode reply
// Inputs: superlayer, bias, threshold
// Author: A. Parenti, Dec22 2004
// Modified: AP, Mar24 2005
// Note: Tested at Cessy, Oct05 2006
void Cfe::set_FE_thr(short sl, float bias, float threshold) {
  const unsigned char command_code=0x35;
  unsigned char command_line[11];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  wstring(sl,1,command_line);
  wstring(bias,3,command_line);
  wstring(threshold,7,command_line);


  set_FE_thr_reset(); // Reset set_fe_thr struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,11,SET_FE_THR_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = set_FE_thr_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    set_fe_thr.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nSet FE threshold (0x35): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset set_fe_thr struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec11 2006
void Cfe::set_FE_thr_reset() {
  if (this==NULL) return; // Object deleted -> return

  set_fe_thr.code1=0;
  set_fe_thr.bias=set_fe_thr.threshold=set_fe_thr.adc_bias=0;
  set_fe_thr.adc_threshold=0;

  set_fe_thr.code2=set_fe_thr.narg=0;
  set_fe_thr.ccbserver_code=-5;
  set_fe_thr.msg="";
}


// Decode "set FE threshold" (0x35) reply
// Author: A. Parenti, Jan31 2005
// Modified: AP, Dec11 2006
int Cfe::set_FE_thr_decode(unsigned char *rdstr) {
  const unsigned char right_reply=0x25, command_code=0x35;
  unsigned char code2;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&set_fe_thr.code1);
  code2 = rdstr[idx];

// Fill-up the "result" structure
  if (set_fe_thr.code1==right_reply) {
    idx = rstring(rdstr,idx,&set_fe_thr.bias);
    idx = rstring(rdstr,idx,&set_fe_thr.threshold);
    idx = rstring(rdstr,idx,&set_fe_thr.adc_bias);
    idx = rstring(rdstr,idx,&set_fe_thr.adc_threshold);
  } else if (set_fe_thr.code1==CCB_OXFC && code2==command_code) {
    idx = rstring(rdstr,idx,&set_fe_thr.code2);
    idx = rstring(rdstr,idx,&set_fe_thr.narg);
  }

// Fill-up the error code
  if (set_fe_thr.code1==CCB_BUSY_CODE) // ccb is busy
    set_fe_thr.ccbserver_code = -1;
  else if (set_fe_thr.code1==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    set_fe_thr.ccbserver_code = 1;
  else if (set_fe_thr.code1==right_reply) // ccb: right reply
    set_fe_thr.ccbserver_code = 0;
  else if (set_fe_thr.code1==CCB_OXFC && code2==command_code) // ccb: right reply
    set_fe_thr.ccbserver_code = 0;
  else // wrong ccb reply
    set_fe_thr.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  set_fe_thr.msg="\nSet FE threshold (0x35)\n";

  if (set_fe_thr.ccbserver_code!=0) {// Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(set_fe_thr.ccbserver_code)); set_fe_thr.msg+=tmpstr;
  } else {
    if (set_fe_thr.code1==right_reply) {
      sprintf(tmpstr,"code: %#2X\n", set_fe_thr.code1); set_fe_thr.msg+=tmpstr;
      sprintf(tmpstr,"bias: %.2f (V)\n",set_fe_thr.bias); set_fe_thr.msg+=tmpstr;
      sprintf(tmpstr,"threshold: %.3f (V)\n",set_fe_thr.threshold); set_fe_thr.msg+=tmpstr;
      sprintf(tmpstr,"adc_bias: %.2f (V)\n",set_fe_thr.adc_bias); set_fe_thr.msg+=tmpstr;
      sprintf(tmpstr,"adc_threshold: %.3f (V)\n",set_fe_thr.adc_threshold); set_fe_thr.msg+=tmpstr;
    } else {
      sprintf(tmpstr,"code1: %#2X\n",set_fe_thr.code1); set_fe_thr.msg+=tmpstr;
      sprintf(tmpstr,"code2: %#2X\n",set_fe_thr.code2); set_fe_thr.msg+=tmpstr;
      sprintf(tmpstr,"Wrong argument. narg: %d\n",set_fe_thr.narg); set_fe_thr.msg+=tmpstr;
    }
  }

  return idx;
}



// Print result of "set FE threshold" (0x35) to stdout
// Author: A. Parenti, Dec22 2004
// Modified: AP, Dec11 2006
void Cfe::set_FE_thr_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",set_fe_thr.msg.c_str());
}


/*********************************/
/* "set FE width" (0x47) command */
/*********************************/

// Send "set FE width" (0x47) command and decode reply
// Author: A. Parenti, Jan03 2005
// Note: Tested at LNL, Mar09 2005
// Modified: AP, Mar24 2005
void Cfe::set_FE_width(float width) {
  const unsigned char command_code=0x47;
  unsigned char command_line[5];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  wstring(width,1,command_line);

  set_FE_width_reset(); // Reset set_fe_width struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,5,SET_FE_WIDTH_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = set_FE_width_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    set_fe_width.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nSet FE width (0x47): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset set_fe_width struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec11 2006 
void Cfe::set_FE_width_reset() {
  if (this==NULL) return; // Object deleted -> return

  set_fe_width.code=0;
  set_fe_width.width=set_fe_width.adc_width=0;

  set_fe_width.ccbserver_code=-5;
  set_fe_width.msg="";
}


// Decode "set FE width" (0x47) reply
// Author: A. Parenti, Jan31 2005
// Modified: AP, Dec11 2006 
int Cfe::set_FE_width_decode(unsigned char *rdstr) {
  const unsigned char right_reply=0x24;
  unsigned char code2;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure

  idx = rstring(rdstr,idx,&set_fe_width.code);
  code2 = rdstr[idx];

// Fill-up the "result" structure

  idx = rstring(rdstr,idx,&set_fe_width.width);
  idx = rstring(rdstr,idx,&set_fe_width.adc_width);

// Fill-up the error code
  if (set_fe_width.code==CCB_BUSY_CODE) // ccb is busy
    set_fe_width.ccbserver_code = -1;
  else if (set_fe_width.code==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    set_fe_width.ccbserver_code = 1;
  else if (set_fe_width.code==right_reply) // ccb: right reply
    set_fe_width.ccbserver_code = 0;
  else // wrong ccb reply
    set_fe_width.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];


  set_fe_width.msg="\nSet FE width (0x47)\n";
  if (set_fe_width.ccbserver_code!=0) {// Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(set_fe_width.ccbserver_code)); set_fe_width.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code: %#2X\n",set_fe_width.code); set_fe_width.msg+=tmpstr;
    sprintf(tmpstr,"width: %.2f (V)\n",set_fe_width.width); set_fe_width.msg+=tmpstr;
    sprintf(tmpstr,"adc_width: %.2f (V)\n",set_fe_width.adc_width); set_fe_width.msg+=tmpstr;
  }

  return idx;
}


// Print result of "set FE width" (0x47) to stdout
// Author: A. Parenti, Jan03 2005
// Modified: AP, Dec11 2006 
void Cfe::set_FE_width_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",set_fe_width.msg.c_str());
}



/*****************************************/
/* "read FE temperatures" (0x36) command */
/*****************************************/

// Send "read FE temperatures" (0x36) command and decode reply
// Inputs: superlayer, option (-1=all, 0=only Tmax-Tmed)
// Author: A. Parenti, Dec23 2004
// Note: Tested at LNL, Mar09 2005
// Modified: AP, Mar24 2005
void Cfe::read_FE_temp(char sl, char opt) {
  const unsigned char command_code=0x36;
  unsigned char command_line[3];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=sl;
  command_line[2]=opt;

  read_FE_temp_reset(); // Reset read_fe_temp struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,3,READ_FE_TEMP_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = read_FE_temp_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    read_fe_temp.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nRead FE temperature (0x36): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset read_fe_temp struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec11 2006
void Cfe::read_FE_temp_reset() {
  int i;

  if (this==NULL) return; // Object deleted -> return

  read_fe_temp.code=0;
  for (i=0;i<98;++i)
    read_fe_temp.temp[i]=0;

  read_fe_temp.ccbserver_code=-5;
  read_fe_temp.msg="";
}


// Decode "read FE temperatures" (0x36) reply
// Author: A. Parenti, Jan31 2005
// Modified: AP, Dec11 2006
int Cfe::read_FE_temp_decode(unsigned char *rdstr) {
  const unsigned char right_reply=0x37;
  unsigned char code2;
  int i, idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure

  idx = rstring(rdstr,idx,&read_fe_temp.code);
  code2 = rdstr[idx];

// Fill-up the "result" structure

  for (i=0;i<98;++i)
    idx = rstring(rdstr,idx,read_fe_temp.temp+i);

// Fill-up the error code
  if (read_fe_temp.code==CCB_BUSY_CODE) // ccb is busy
    read_fe_temp.ccbserver_code = -1;
  else if (read_fe_temp.code==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    read_fe_temp.ccbserver_code = 1;
  else if (read_fe_temp.code==right_reply) // ccb: right reply
    read_fe_temp.ccbserver_code = 0;
  else // wrong ccb reply
    read_fe_temp.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  read_fe_temp.msg="\nRead FE temperature (0x36)\n";

  if (read_fe_temp.ccbserver_code!=0) {// Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(read_fe_temp.ccbserver_code)); read_fe_temp.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code: %#2X\n", read_fe_temp.code); read_fe_temp.msg+=tmpstr;
    for (i=0;i<98;++i) {
      sprintf(tmpstr,"Temp[%d]: %d (0.1 C)\n", i,read_fe_temp.temp[i]); read_fe_temp.msg+=tmpstr;
    }
  }

  return idx;
}


// Print result of "read FE temperatures" (0x36) to stdout
// Author: A. Parenti, Dec23 2004
// Modified: AP, Dec11 2006
void Cfe::read_FE_temp_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",read_fe_temp.msg.c_str());
}



/*********************************/
/* "read FE mask" (0x38) command */
/*********************************/

// Send "read FE mask" (0x38) command and decode reply
// Author: A. Parenti, Dec23 2004
// Note: Tested at LNL, Mar09 2005
// Modified: AP, Mar24 2005
void Cfe::read_FE_mask() {
  const unsigned char command_code=0x38;
  unsigned char command_line[1];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;

  read_FE_mask_reset(); // Reset read_fe_mask struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1,READ_FE_MASK_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = read_FE_mask_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    read_fe_mask.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nRead FE mask (0x38): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset read_fe_mask struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec11 2006
void Cfe::read_FE_mask_reset() {
  int i,j,k;

  if (this==NULL) return; // Object deleted -> return

  read_fe_mask.code=0;

  for (i=0;i<3;++i)
    for (j=0;j<24;++j)
      for (k=0;k<3;++k)
        read_fe_mask.MadMsk[i][j][k]=0;

  read_fe_mask.ccbserver_code=-5;
  read_fe_mask.msg="";
}


// Decode "read FE mask" (0x38) reply
// Author: A. Parenti, Jan31 2005
// Modified: AP, Dec11 2006
int Cfe::read_FE_mask_decode(unsigned char *rdstr) {
  const unsigned char right_reply=0x39;
  unsigned char code2, tmpbyte;
  int idx=0, i, j, k;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure

  idx = rstring(rdstr,idx,&read_fe_mask.code);
  code2 = rdstr[idx];

// Fill-up the "result" structure

  for (k=0;k<3;++k)
    for (j=0;j<24;++j)
      for (i=0;i<3;++i) {
        idx = rstring(rdstr,idx,&tmpbyte);
        read_fe_mask.MadMsk[k][j][i]=tmpbyte;
      }
// Fill-up the error code
  if (read_fe_mask.code==CCB_BUSY_CODE) // ccb is busy
    read_fe_mask.ccbserver_code = -1;
  else if (read_fe_mask.code==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    read_fe_mask.ccbserver_code = 1;
  else if (read_fe_mask.code==right_reply) // ccb: right reply
    read_fe_mask.ccbserver_code = 0;
  else // wrong ccb reply
    read_fe_mask.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  read_fe_mask.msg="\nRead FE mask (0x38)\n";

  if (read_fe_mask.ccbserver_code!=0) {// Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(read_fe_mask.ccbserver_code)); read_fe_mask.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code: %#2X\n", read_fe_mask.code); read_fe_mask.msg+=tmpstr;
    for (k=0;k<3;++k)
      for (j=0;j<24;++j)
        for (i=0;i<3;++i) {
          sprintf(tmpstr,"MadMsk[%d][%d][%d]: %#2X\n",k,j,i,read_fe_mask.MadMsk[k][j][i]); read_fe_mask.msg+=tmpstr;
        }
  }

  return idx;
}


// Print result of "read FE mask" (0x38) to stdout
// Author: A. Parenti, Dec23 2004
// Modified: AP, Dec11 2006
void Cfe::read_FE_mask_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",read_fe_mask.msg.c_str());
}



/****************************/
/* "Mask FE" (0x3A) command */
/****************************/

// Send "Mask FE" (0x3A) command and decode reply
// Author: A. Parenti, Dec23 2004
// Note: Tested at LNL, Mar09 2005
// Modified: Mar24 2005
void Cfe::set_FE_mask(short sl, short brd, unsigned char mask[3]) {
  const unsigned char command_code=0x3A;
  unsigned char command_line[8];
  int i, send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  wstring(sl,1,command_line);
  wstring(brd,3,command_line);
  for (i=0;i<3;++i)
    command_line[i+5]=mask[i];

  set_FE_mask_reset(); // Reset set_fe_mask struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,8,SET_FE_MASK_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = set_FE_mask_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    set_fe_mask.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nMask FE (0x3A): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset set_fe_mask struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec11 2006
void Cfe::set_FE_mask_reset() {
  int i,j,k;

  if (this==NULL) return; // Object deleted -> return

  set_fe_mask.code=0;

  for (i=0;i<3;++i)
    for (j=0;j<24;++j)
      for (k=0;k<3;++k)
        set_fe_mask.MadMsk[i][j][k]=0;

  set_fe_mask.ccbserver_code=-5;
  set_fe_mask.msg="";
}


// Decode "Mask FE" (0x3A - 0x3B) reply
// Author: A. Parenti, Jan31 2005
// Modified: AP, Dec11 2006
int Cfe::set_FE_mask_decode(unsigned char *rdstr) {
  const unsigned char right_reply=0x39;
  unsigned char code2, tmpbyte;
  int idx=0, i, j, k;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure

  idx = rstring(cmd_obj->data_read,idx,&set_fe_mask.code);
  code2 = cmd_obj->data_read[idx];

// Fill-up the "result" structure

  for (k=0;k<3;++k)
    for (j=0;j<24;++j)
      for (i=0;i<3;++i) {
        idx = rstring(cmd_obj->data_read,idx,&tmpbyte);
        set_fe_mask.MadMsk[k][j][i]=tmpbyte;
      }
// Fill-up the error code
  if (set_fe_mask.code==CCB_BUSY_CODE) // ccb is busy
    set_fe_mask.ccbserver_code = -1;
  else if (set_fe_mask.code==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    set_fe_mask.ccbserver_code = 1;
  else if (set_fe_mask.code==right_reply) // ccb: right reply
    set_fe_mask.ccbserver_code = 0;
  else // wrong ccb reply
    set_fe_mask.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  set_fe_mask.msg="\nMask FE (0x3A - 0x3B)\n";

  if (set_fe_mask.ccbserver_code!=0) {// Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(set_fe_mask.ccbserver_code)); set_fe_mask.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code: %#2X\n", set_fe_mask.code); set_fe_mask.msg+=tmpstr;
    for (k=0;k<3;++k)
      for (j=0;j<24;++j)
        for (i=0;i<3;++i) {
          sprintf(tmpstr,"MadMsk[%d][%d][%d]: %#2X\n",k,j,i,set_fe_mask.MadMsk[k][j][i]); set_fe_mask.msg+=tmpstr;
        }
  }

  return idx;
}


// Print result of "Mask FE" (0x3A - 0x3B) to stdout
// Author: A. Parenti, Dec23 2004
// Modified: AP, Dec11 2006
void Cfe::set_FE_mask_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",set_fe_mask.msg.c_str());
}


/************************************/
/* "Mask FE channel" (0x3B) command */
/************************************/

// Send "Mask FE channel" (0x3B) command and decode reply
// Author: A. Parenti, Dec23 2004
// Note: Tested at LNL, Mar09 2005
// Modified: AP, Mar24 2005
void Cfe::set_FE_mask_ch(short sl, short channel, short status) {
  const unsigned char command_code=0x3B;
  unsigned char command_line[7];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  wstring(sl,1,command_line);
  wstring(channel,3,command_line);
  wstring(status,5,command_line);

  set_FE_mask_reset(); // Reset set_fe_mask struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,7,SET_FE_MASK_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = set_FE_mask_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    set_fe_mask.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nMask FE channel (0x3B): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


/******************************************/
/* "FE super layer enable" (0x9A) command */
/******************************************/

// Send "FE super layer enable" (0x9A) command and decode reply
// Author: A. Parenti, Jul13 2005
// Modified: AP, Dec11 2006
void Cfe::set_SL_mask(char sl, char enable) {
  const unsigned char command_code=0x9A;
  unsigned char command_line[3];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

  mc_firmware_version(); // Get firmware version
  set_SL_mask_reset(); // Reset set_sl_mask struct
  if (HVer<1 || (HVer==1 && LVer<10)) { // Firmware < v1.10
    char tmpstr[200];
    sprintf(tmpstr,"Attention: command %#X supported after v1.10 (actual: v%d.%d)\n",command_code,HVer,LVer);
    set_sl_mask.msg=tmpstr;
    std::cout << tmpstr;
    return;
  }

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=sl;
  command_line[2]=enable;

// Send the command line
  send_command_result = cmd_obj->send_command(command_line,3,SET_SL_MASK_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = set_SL_mask_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    set_sl_mask.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nFE Super Layer enable (0x9A): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset set_sl_mask struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec11 2006
void Cfe::set_SL_mask_reset() {
  if (this==NULL) return; // Object deleted -> return

  set_sl_mask.code1=set_sl_mask.code2=set_sl_mask.result=0;

  set_sl_mask.ccbserver_code=-5;
  set_sl_mask.msg="";
}


// Decode "FE super layer enable" (0x9A) reply
// Author: A. Parenti, Jul13 2005
// Modified: AP, Dec12 2006
int Cfe::set_SL_mask_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x9A, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure

  idx = rstring(cmd_obj->data_read,idx,&set_sl_mask.code1);
  idx = rstring(cmd_obj->data_read,idx,&set_sl_mask.code2);

// Fill-up the "result" structure
  idx = rstring(cmd_obj->data_read,idx,&set_sl_mask.result);

// Fill-up the error code
  if (set_sl_mask.code1==CCB_BUSY_CODE) // ccb is busy
    set_sl_mask.ccbserver_code = -1;
  else if (set_sl_mask.code1==CCB_UNKNOWN1 && set_sl_mask.code2==CCB_UNKNOWN2) // ccb: unknown command
    set_sl_mask.ccbserver_code = 1;
  else if (set_sl_mask.code1==right_reply && set_sl_mask.code2==command_code) // ccb: right reply
    set_sl_mask.ccbserver_code = 0;
  else // wrong ccb reply
    set_sl_mask.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  set_sl_mask.msg="\nFE super layer enable (0x9A)\n";

  if (set_sl_mask.ccbserver_code!=0) {// Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(set_sl_mask.ccbserver_code)); set_sl_mask.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n", set_sl_mask.code1); set_sl_mask.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n", set_sl_mask.code2); set_sl_mask.msg+=tmpstr;

    switch (set_sl_mask.result) {
    case 0:
      set_sl_mask.msg+="Result: Success\n";
      break;
    case 1:
      set_sl_mask.msg+="Result: Not connected (no_ack)\n";
      break;
    case -1:
      set_sl_mask.msg+="Result: SDA error\n";
      break;
    case -2:
      set_sl_mask.msg+="Result: SCL error\n";
      break;
    default:
      set_sl_mask.msg+="Result: \n";
    }
  }

  return idx;
}


// Print result of "FE super layer enable" (0x9A) to stdout
// Author: A. Parenti, Jul13 2005
// Modified: AP, Dec11 2006
void Cfe::set_SL_mask_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",set_sl_mask.msg.c_str());
}



/*********************************************/
/* "read FE temperature mask" (0x71) command */
/*********************************************/

// Send "read FE temperature mask" (0x71) command and decode reply
// Author: A. Parenti, Jan03 2005
// Note: Tested at LNL, Mar09 2005
// Modified: AP, Mar24 2005
void Cfe::read_FE_Tmask() {
  const unsigned char command_code=0x71;
  unsigned char command_line[1];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;

  read_FE_Tmask_reset(); // Reset read_fe_Tmask struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1,READ_FE_TMASK_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = read_FE_Tmask_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    read_fe_Tmask.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nRead FE temperature mask (0x71): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset read_fe_Tmask struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec11 2006
void Cfe::read_FE_Tmask_reset() {
  int i,j;

  if (this==NULL) return; // Object deleted -> return

  read_fe_Tmask.code=0;

  for (i=0;i<3;++i)
    for (j=0;j<24;++j)
      read_fe_Tmask.TempMsk[i][j]=0;

  read_fe_Tmask.ccbserver_code=-5;
  read_fe_Tmask.msg="";
}


// Decode "read FE temperature mask" (0x71) reply
// Author: A. Parenti, Feb01 2005
// Modified: AP, Dec11 2006
int Cfe::read_FE_Tmask_decode(unsigned char *rdstr) {
  const unsigned char right_reply=0x63;
  unsigned char code2, tmpbyte;
  int i, j, idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure

  idx = rstring(rdstr,idx,&read_fe_Tmask.code);
  code2 = rdstr[idx];

// Fill-up the "result" structure

  for (j=0;j<3;++j)
    for (i=0;i<24;++i) {
      idx = rstring(rdstr,idx,&tmpbyte);
      read_fe_Tmask.TempMsk[j][i]=tmpbyte;
    }

// Fill-up the error code
  if (read_fe_Tmask.code==CCB_BUSY_CODE) // ccb is busy
    read_fe_Tmask.ccbserver_code = -1;
  else if (read_fe_Tmask.code==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    read_fe_Tmask.ccbserver_code = 1;
  else if (read_fe_Tmask.code==right_reply) // ccb: right reply
    read_fe_Tmask.ccbserver_code = 0;
  else // wrong ccb reply
    read_fe_Tmask.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  read_fe_Tmask.msg="\nRead FE temperature mask (0x71)\n";

  if (read_fe_Tmask.ccbserver_code!=0) {// Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(read_fe_Tmask.ccbserver_code)); read_fe_Tmask.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code: %#2X\n", read_fe_Tmask.code); read_fe_Tmask.msg+=tmpstr;
    for (j=0;j<3;++j)
      for (i=0;i<24;++i) {
        sprintf(tmpstr,"TempMsk[%d][%d]: %#2X\n",j,i,read_fe_Tmask.TempMsk[j][i]); read_fe_Tmask.msg+=tmpstr;
      }
  }

  return idx;
}


// Print result of "read FE temperature mask" (0x71) to stdout
// Author: A. Parenti, Jan03 2005
// Modified: AP, Dec11 2006
void Cfe::read_FE_Tmask_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",read_fe_Tmask.msg.c_str());
}



/********************************************/
/* "set FE temperature mask" (0x62) command */
/********************************************/

// Send "set FE temperature mask" (0x62) command and decode reply
// Author: A. Parenti, Jan03 2005
// Note: Tested at LNL, Mar09 2005
// Modified: AP, Mar24 2005
void Cfe::set_FE_Tmask(short sl, short chip, short status) {
  const unsigned char command_code=0x62;
  unsigned char command_line[7];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  wstring(sl,1,command_line);
  wstring(chip,3,command_line);
  wstring(status,5,command_line);

  set_FE_Tmask_reset(); // Reset set_fe_Tmask struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,7,SET_FE_TMASK_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = set_FE_Tmask_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    set_fe_Tmask.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nset FE temperature mask(0x62): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset read_fe_Tmask struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec11 2006
void Cfe::set_FE_Tmask_reset() {
  int i,j;

  if (this==NULL) return; // Object deleted -> return

  set_fe_Tmask.code=0;

  for (i=0;i<3;++i)
    for (j=0;j<24;++j)
      set_fe_Tmask.TempMsk[i][j]=0;

  set_fe_Tmask.ccbserver_code=-5;
  set_fe_Tmask.msg="";
}


// Decode "Mask FE" (0x62) reply
// Author: A. Parenti, Feb01 2005
// Modified: AP, Dec11 2006
int Cfe::set_FE_Tmask_decode(unsigned char *rdstr) {
  const unsigned char right_reply=0x63;
  unsigned char code2, tmpbyte;
  int i, j, idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure

  idx = rstring(rdstr,idx,&set_fe_Tmask.code);
  code2 = rdstr[idx];

// Fill-up the "result" structure

  for (j=0;j<3;++j)
    for (i=0;i<24;++i) {
      idx = rstring(rdstr,idx,&tmpbyte);
      set_fe_Tmask.TempMsk[j][i]=tmpbyte;
    }

// Fill-up the error code
  if (set_fe_Tmask.code==CCB_BUSY_CODE) // ccb is busy
    set_fe_Tmask.ccbserver_code = -1;
  else if (set_fe_Tmask.code==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    set_fe_Tmask.ccbserver_code = 1;
  else if (set_fe_Tmask.code==right_reply) // ccb: right reply
    set_fe_Tmask.ccbserver_code = 0;
  else // wrong ccb reply
    set_fe_Tmask.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  set_fe_Tmask.msg="\nset FE temperature mask (0x62)\n";

  if (set_fe_Tmask.ccbserver_code!=0) {// Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(set_fe_Tmask.ccbserver_code)); set_fe_Tmask.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code: %#2X\n",set_fe_Tmask.code); set_fe_Tmask.msg+=tmpstr;
    for (j=0;j<3;++j)
      for (i=0;i<24;++i) {
        sprintf(tmpstr,"TempMsk[%d][%d]: %#2X\n",j,i,set_fe_Tmask.TempMsk[j][i]); set_fe_Tmask.msg+=tmpstr;
      }
  }

  return idx;
}


// Print result of "Mask FE" (0x62) to stdout
// Author: A. Parenti, Jan03 2005
// Modified: AP, Dec11 2006
void Cfe::set_FE_Tmask_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",set_fe_Tmask.msg.c_str());
}



/***********************************************/
/* "FE super layer Tmax enable" (0x9B) command */
/***********************************************/

// Send "FE super layer Tmax enable" (0x9B) command and decode reply
// Author: A. Parenti, Jul13 2005
// Modified: AP, Dec11 2006
void Cfe::set_SL_Tmask(char sl, char enable) {
  const unsigned char command_code=0x9B;
  unsigned char command_line[3];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

  mc_firmware_version(); // Get firmware version
  set_SL_Tmask_reset(); // Reset set_sl_Tmask struct
  if (HVer<1 || (HVer==1 && LVer<10)) { // Firmware < v1.10
    char tmpstr[200];
    sprintf(tmpstr,"Attention: command %#X supported after v1.10 (actual: v%d.%d)\n",command_code,HVer,LVer);
    set_sl_Tmask.msg=tmpstr;
    std::cout << tmpstr;
    return;
  }

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=sl;
  command_line[2]=enable;

// Send the command line
  send_command_result = cmd_obj->send_command(command_line,3,SET_SL_TMASK_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = set_SL_Tmask_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    set_sl_Tmask.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nFE Super Layer Tmax enable (0x9B): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset set_sl_Tmask struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec11 2006
void Cfe::set_SL_Tmask_reset() {
  if (this==NULL) return; // Object deleted -> return

  set_sl_Tmask.code1=set_sl_Tmask.code2=set_sl_Tmask.result=0;

  set_sl_Tmask.ccbserver_code=-5;
  set_sl_Tmask.msg="";
}


// Decode "FE super layer Tmax enable" (0x9B) reply
// Author: A. Parenti, Jul13 2005
// Modified: AP, Dec12 2006
int Cfe::set_SL_Tmask_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x9B, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure

  idx = rstring(cmd_obj->data_read,idx,&set_sl_Tmask.code1);
  idx = rstring(cmd_obj->data_read,idx,&set_sl_Tmask.code2);

// Fill-up the "result" structure
  idx = rstring(cmd_obj->data_read,idx,&set_sl_Tmask.result);

// Fill-up the error code
  if (set_sl_Tmask.code1==CCB_BUSY_CODE) // ccb is busy
    set_sl_Tmask.ccbserver_code = -1;
  else if (set_sl_Tmask.code1==CCB_UNKNOWN1 && set_sl_Tmask.code2==CCB_UNKNOWN2) // ccb: unknown command
    set_sl_Tmask.ccbserver_code = 1;
  else if (set_sl_Tmask.code1==right_reply && set_sl_Tmask.code2==command_code) // ccb: right reply
    set_sl_Tmask.ccbserver_code = 0;
  else // wrong ccb reply
    set_sl_Tmask.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  set_sl_Tmask.msg="\nFE super layer Tmask enable (0x9B)\n";

  if (set_sl_Tmask.ccbserver_code!=0) {// Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(set_sl_Tmask.ccbserver_code)); set_sl_Tmask.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n", set_sl_Tmask.code1); set_sl_Tmask.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n", set_sl_Tmask.code2); set_sl_Tmask.msg+=tmpstr;

    switch (set_sl_Tmask.result) {
    case 0:
      set_sl_Tmask.msg+="Result: Success\n";
      break;
    case 1:
      set_sl_Tmask.msg+="Result: Not connected (no_ack)\n";
      break;
    case -1:
      set_sl_Tmask.msg+="Result: SDA error\n";
      break;
    case -2:
      set_sl_Tmask.msg+="Result: SCL error\n";
      break;
    default:
      set_sl_Tmask.msg+="Result: \n";
    }
  }

  return idx;
}


// Print result of "FE super layer Tmask enable" (0x9B) to stdout
// Author: A. Parenti, Jul13 2004
// Modified: AP, Dec11 2006
void Cfe::set_SL_Tmask_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",set_sl_Tmask.msg.c_str());
}



/****************************/
/* "FE test" (0x4B) command */
/****************************/

// Send "FE test" (0x4B) command and decode reply
// Author: A. Parenti, Jan03 2005
// Note: Tested at LNL, Mar09 2005
// Modified: AP, Mar24 2005
void Cfe::FE_test() {
  const unsigned char command_code=0x4B;
  unsigned char command_line[1];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;

  FE_test_reset(); // Reset fe_test struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1,FE_TEST_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = FE_test_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    fe_test.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nFE test (0x4B): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}

// Send "FE test" (0x4B) command and decode reply
// Author: A. Parenti, Oct09 2007
void Cfe::FE_test(char force) {
  const unsigned char command_code=0x4B;
  unsigned char command_line[2];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

  mc_firmware_version(); // Get firmware version
  FE_test_reset(); // Reset fe_test struct
  if (HVer<1 || (HVer==1 && LVer<20)) { // Firmware < v1.20
    char tmpstr[200];
    sprintf(tmpstr,"Attention: command %#X w/ argument <force> is supported after v1.20 (actual: v%d.%d)\n",command_code,HVer,LVer);
    fe_test.msg=tmpstr;
    std::cout << tmpstr;
    return;
  }

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=force;

// Send the command line
  send_command_result = cmd_obj->send_command(command_line,2,FE_TEST_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = FE_test_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    fe_test.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nFE test (0x4B): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset fe_test struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec11 2006
void Cfe::FE_test_reset() {
  int i;

  if (this==NULL) return; // Object deleted -> return

  fe_test.code=0;
  for(i=0;i<3;++i) {
    fe_test.Fe_TestI2C[i]=fe_test.SlPresMsk[i]=0;
    fe_test.SlBoardPresMsk[i]=fe_test.SlExBoardMsk[i]=0;
  }

  fe_test.ccbserver_code=-5;
  fe_test.msg="";
}


// Decode "FE test" (0x4B) reply
// Author: A. Parenti, Feb01 2005
// Modified: AP, Dec11 2006
int Cfe::FE_test_decode(unsigned char *rdstr) {
  const unsigned char right_reply=0x4C;
  unsigned char code2;
  int i, idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure

  idx = rstring(rdstr,idx,&fe_test.code);
  code2 = rdstr[idx];

// Fill-up the "result" structure
  for (i=0;i<3;++i)
    idx = rstring(rdstr,idx,fe_test.Fe_TestI2C+i);
  for (i=0;i<3;++i)
    idx = rstring(rdstr,idx,fe_test.SlPresMsk+i);
  for (i=0;i<3;++i)
    idx = rstring(rdstr,idx,fe_test.SlBoardPresMsk+i);
  for (i=0;i<3;++i)
    idx = rstring(rdstr,idx,fe_test.SlExBoardMsk+i);

// Fill-up the error code
  if (fe_test.code==CCB_BUSY_CODE) // ccb is busy
    fe_test.ccbserver_code = -1;
  else if (fe_test.code==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    fe_test.ccbserver_code = 1;
  else if (fe_test.code==right_reply) // ccb: right reply
    fe_test.ccbserver_code = 0;
  else // wrong ccb reply
    fe_test.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  fe_test.msg="\nFE test (0x4B)\n";

  if (fe_test.ccbserver_code!=0) {// Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(fe_test.ccbserver_code)); fe_test.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code: %#2X\n", fe_test.code); fe_test.msg+=tmpstr;
    sprintf(tmpstr,"Fe_TestI2C: %#2X %#2X %#2X\n",fe_test.Fe_TestI2C[0],fe_test.Fe_TestI2C[1],fe_test.Fe_TestI2C[2]); fe_test.msg+=tmpstr;
    sprintf(tmpstr,"SlPresMsk: %#2X %#2X %#2X\n",fe_test.SlPresMsk[0],fe_test.SlPresMsk[1],fe_test.SlPresMsk[2]); fe_test.msg+=tmpstr;
    sprintf(tmpstr,"SlBoardPresMsk: %#2X %#2X %#2X\n",fe_test.SlBoardPresMsk[0],fe_test.SlBoardPresMsk[1],fe_test.SlBoardPresMsk[2]); fe_test.msg+=tmpstr;
    sprintf(tmpstr,"SlExBoardMsk: %#2X %#2X %#2X\n",fe_test.SlExBoardMsk[0],fe_test.SlExBoardMsk[1],fe_test.SlExBoardMsk[2]); fe_test.msg+=tmpstr;
  }

  return idx;
}


// Print result of "FE test" (0x4B) to stdout
// Author: A. Parenti, Jan03 2005
// Modified: AP, Dec11 2006
void Cfe::FE_test_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",fe_test.msg.c_str());
}


/* TEST PULSE */

/****************************************/
/* "Test Pulse settings" (0x4A) command */
/****************************************/

// Send "Test Pulse settings" (0x4A) command and decode reply
// Author: A. Parenti, Jan12 2005
// Note: Tested at LNL, Mar09 2005
// Modified: Mar24 2005
void Cfe::set_TP(char coarsedelay1, char finedelay1, char coarsedelay2, char finedelay2, char endSqDly, float Amplitude1, float Amplitude2, char FEDisDly, char fineofs[2]) {
  const unsigned char command_code=0x4A;
  unsigned char command_line[17];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=coarsedelay1;
  command_line[2]=finedelay1;
  command_line[3]=coarsedelay2;
  command_line[4]=finedelay2;
  command_line[5]=endSqDly;
  wstring(Amplitude1,6,command_line);
  wstring(Amplitude2,10,command_line);
  command_line[14]=FEDisDly;
  command_line[15]=fineofs[0];
  command_line[16]=fineofs[1];

  set_TP_reset(); // Reset set_tp struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,17,SET_TP_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = set_TP_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    set_tp.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nTest Pulse settings (0x4A): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset set_tp struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec11 2006
void Cfe::set_TP_reset() {
  if (this==NULL) return; // Object deleted -> return

  set_tp.code1=set_tp.code2=0;

  set_tp.ccbserver_code=-5;
  set_tp.msg="";
}


// Decode "Test Pulse settings" (0x4A) reply
// Author: A. Parenti, Feb01 2005
// Modified: AP, Dec11 2006
int Cfe::set_TP_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x4A, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure

  idx = rstring(rdstr,idx,&set_tp.code1);
  idx = rstring(rdstr,idx,&set_tp.code2);

// Fill-up the error code
  if (set_tp.code1==CCB_BUSY_CODE) // ccb is busy
    set_tp.ccbserver_code = -1;
  else if (set_tp.code1==CCB_UNKNOWN1 && set_tp.code2==CCB_UNKNOWN2) // ccb: unknown command
    set_tp.ccbserver_code = 1;
  else if (set_tp.code1==right_reply && set_tp.code2==command_code) // ccb: right reply
    set_tp.ccbserver_code = 0;
  else // wrong ccb reply
    set_tp.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  set_tp.msg="\nTest Pulse settings (0x4A)\n";

  if (set_tp.ccbserver_code!=0) {// Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(set_tp.ccbserver_code)); set_tp.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",set_tp.code1); set_tp.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",set_tp.code2); set_tp.msg+=tmpstr;
  }

  return idx;
}


// Print result of "Test Pulse settings" (0x4A) to stdout
// Author: A. Parenti, Jan12 2005
// Modified: AP, Dec11 2006
void Cfe::set_TP_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",set_tp.msg.c_str());
}


/*************************************************/
/* "Relative Test Pulse settings" (0x78) command */
/*************************************************/

// Send "Relative Test Pulse settings" (0x78) command and decode reply
// Author: A. Parenti, Jan12 2005
// Note: Tested at LNL, Mar09 2005
// Modified: AP, Mar24 2005
void Cfe::set_rel_TP(float delay, float Amplitude1, float Amplitude2, char endSqDly, char FEDisDly) {
  const unsigned char command_code=0x78;
  unsigned char command_line[15];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  wstring(delay,1,command_line);
  wstring(Amplitude1,5,command_line);
  wstring(Amplitude2,9,command_line);
  command_line[13]=endSqDly;
  command_line[14]=FEDisDly;

  set_rel_TP_reset(); // Reset set_rel_tp struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,15,SET_TP_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = set_rel_TP_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    set_rel_tp.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nRelative Test Pulse settings (0x78): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset set_rel_tp struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec11 2006
void Cfe::set_rel_TP_reset() {
  if (this==NULL) return; // Object deleted -> return

  set_rel_tp.code1=set_rel_tp.code2=0;

  set_rel_tp.ccbserver_code=-5;
}


// Decode "Relative Test Pulse settings" (0x78) reply
// Author: A. Parenti, Feb01 2005
// Modified: AP, Dec11 2006
int Cfe::set_rel_TP_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x78, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure

  idx = rstring(rdstr,idx,&set_rel_tp.code1);
  idx = rstring(rdstr,idx,&set_rel_tp.code2);

// Fill-up the error code
  if (set_rel_tp.code1==CCB_BUSY_CODE) // ccb is busy
    set_rel_tp.ccbserver_code = -1;
  else if (set_rel_tp.code1==CCB_UNKNOWN1 && set_rel_tp.code2==CCB_UNKNOWN2) // ccb: unknown command
    set_rel_tp.ccbserver_code = 1;
  else if (set_rel_tp.code1==right_reply && set_rel_tp.code2==command_code) // ccb: right reply
    set_rel_tp.ccbserver_code = 0;
  else // wrong ccb reply
    set_rel_tp.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  set_rel_tp.msg="\nRelative Test Pulse settings (0x78)\n";

  if (set_rel_tp.ccbserver_code!=0) {// Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(set_rel_tp.ccbserver_code)); set_rel_tp.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",set_rel_tp.code1); set_rel_tp.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",set_rel_tp.code2); set_rel_tp.msg+=tmpstr;
  }

  return idx;
}



// Print result of "Relative Test Pulse settings" (0x78) to stdout
// Author: A. Parenti, Jan12 2005
// Modified: AP, Dec11 2006
void Cfe::set_rel_TP_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",set_rel_tp.msg.c_str());
}


/*********************************************/
/* "Read Test Pulse settings" (0x72) command */
/*********************************************/

// Send "Read Test Pulse settings" (0x72) command and decode reply
// Author: A. Parenti, Jan12 2005
// Note: Tested at LNL, Mar09 2005
// Modified: AP, Mar24 2005
void Cfe::read_TP() {
  const unsigned char command_code=0x72;
  unsigned char command_line[1];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;

  read_TP_reset(); // Reset read_tp struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1,READ_TP_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = read_TP_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    read_tp.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nRead Test Pulse settings (0x72): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset read_tp struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec11 2006
void Cfe::read_TP_reset() {
  if (this==NULL) return; // Object deleted -> return

  read_tp.code=0;
  read_tp.coarsedelay1=read_tp.finedelay1=read_tp.coarsedelay2=0;
  read_tp.finedelay2=read_tp.endSqDly=0;
  read_tp.Amplitude1=read_tp.Amplitude2=0;
  read_tp.FEDisDly=read_tp.fineofs1=read_tp.fineofs2=0;

  read_tp.ccbserver_code=-5;
  read_tp.msg="";
}


// Decode "Read Test Pulse settings" (0x72 reply
// Author: A. Parenti, Feb01 2005
// Modified: AP, Dec11 2006
int Cfe::read_TP_decode(unsigned char *rdstr) {
  const unsigned char right_reply=0x73;
  unsigned char code2;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure

  idx = rstring(rdstr,idx,&read_tp.code);
  code2 = rdstr[idx];

// Fill-up the "result" structure

  idx = rstring(rdstr,idx,&read_tp.coarsedelay1);
  idx = rstring(rdstr,idx,&read_tp.finedelay1);
  idx = rstring(rdstr,idx,&read_tp.coarsedelay2);
  idx = rstring(rdstr,idx,&read_tp.finedelay2);
  idx = rstring(rdstr,idx,&read_tp.endSqDly);
  idx = rstring(rdstr,idx,&read_tp.Amplitude1);
  idx = rstring(rdstr,idx,&read_tp.Amplitude2);
  idx = rstring(rdstr,idx,&read_tp.FEDisDly);
  idx = rstring(rdstr,idx,&read_tp.fineofs1);
  idx = rstring(rdstr,idx,&read_tp.fineofs2);

// Fill-up the error code
  if (read_tp.code==CCB_BUSY_CODE) // ccb is busy
    read_tp.ccbserver_code = -1;
  else if (read_tp.code==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    read_tp.ccbserver_code = 1;
  else if (read_tp.code==right_reply) // ccb: right reply
    read_tp.ccbserver_code = 0;
  else // wrong ccb reply
    read_tp.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

   read_tp.msg="\nRead Test Pulse settings (0x72)\n";

  if (read_tp.ccbserver_code!=0) {// Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(read_tp.ccbserver_code)); read_tp.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code: %#2X\n", read_tp.code); read_tp.msg+=tmpstr;
    sprintf(tmpstr,"coarsedelay1: %d (*25 ns)\n",read_tp.coarsedelay1); read_tp.msg+=tmpstr;
    sprintf(tmpstr,"finedelay1: %d (*0.150 ns)\n",read_tp.finedelay1); read_tp.msg+=tmpstr;
    sprintf(tmpstr,"coarsedelay2: %d (*25 ns)\n",read_tp.coarsedelay2); read_tp.msg+=tmpstr;
    sprintf(tmpstr,"finedelay2: %d (*0.150 ns)\n",read_tp.finedelay2); read_tp.msg+=tmpstr;
    sprintf(tmpstr,"endSqDly: %d (*25 ns)\n",read_tp.endSqDly); read_tp.msg+=tmpstr;
    sprintf(tmpstr,"Amplitude1: %.3f (V)\n",read_tp.Amplitude1); read_tp.msg+=tmpstr;
    sprintf(tmpstr,"Amplitude2: %.3f (V)\n",read_tp.Amplitude2); read_tp.msg+=tmpstr;
    sprintf(tmpstr,"FEDisDly: %d (*25 ns)\n",read_tp.FEDisDly); read_tp.msg+=tmpstr;
    sprintf(tmpstr,"fineofs1: %d (*0.150 ns)\n",read_tp.fineofs1); read_tp.msg+=tmpstr;
    sprintf(tmpstr,"fineofs2: %d (*0.150 ns)\n",read_tp.fineofs2); read_tp.msg+=tmpstr;
  }

  return idx;
}


// Print result of "Read Test Pulse settings" (0x72) to stdout
// Author: A. Parenti, Jan12 2005
// Modified: AP, Dec11 2006
void Cfe::read_TP_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",read_tp.msg.c_str());
}


/************************************/
/* "Test Fine Delay" (0x82) command */
/************************************/

// Send "Test Fine Delay" (0x82) command and decode reply
// Author: A. Parenti, Jan17 2005
// Note: Tested at LNL, Mar09 2005
// Modified: AP, Mar24 2005
void Cfe::test_TP_finedelay() {
  const unsigned char command_code=0x82;
  unsigned char command_line[1];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;

  test_TP_finedelay_reset(); // Reset test_tp_finedelay struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1,TEST_TP_FINEDELAY_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = test_TP_finedelay_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    test_tp_finedelay.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nTest Fine Delay (0x82): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset test_tp_finedelay struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec11 2006
void Cfe::test_TP_finedelay_reset() {
  if (this==NULL) return; // Object deleted -> return

  test_tp_finedelay.code1=test_tp_finedelay.code2=0;
  test_tp_finedelay.CpuCkDelay=test_tp_finedelay.TpFineDelay1=0;
  test_tp_finedelay.TpFineDelay2=0;

  test_tp_finedelay.ccbserver_code=-5;
  test_tp_finedelay.msg="";
}


// Decode "Test Fine Delay" (0x82) reply
// Author: A. Parenti, Feb01 2005
// Modified: AP, Dec11 2006
int Cfe::test_TP_finedelay_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x82, right_reply=CCB_OXFC;
  unsigned char flags;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure

  idx = rstring(rdstr,idx,&test_tp_finedelay.code1);
  idx = rstring(rdstr,idx,&test_tp_finedelay.code2);

// Fill-up the "result" structure

  idx = rstring(rdstr,idx,&flags);
  test_tp_finedelay.CpuCkDelay   = takebit(flags,0);
  test_tp_finedelay.TpFineDelay1 = takebit(flags,1);
  test_tp_finedelay.TpFineDelay2 = takebit(flags,2);

// Fill-up the error code
  if (test_tp_finedelay.code1==CCB_BUSY_CODE) // ccb is busy
    test_tp_finedelay.ccbserver_code = -1;
  else if (test_tp_finedelay.code1==CCB_UNKNOWN1 && test_tp_finedelay.code2==CCB_UNKNOWN2) // ccb: unknown command
    test_tp_finedelay.ccbserver_code = 1;
  else if (test_tp_finedelay.code1==right_reply && test_tp_finedelay.code2==command_code) // ccb: right reply
    test_tp_finedelay.ccbserver_code = 0;
  else // wrong ccb reply
    test_tp_finedelay.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  test_tp_finedelay.msg="\nTest Fine Delay (0x82)\n";

  if (test_tp_finedelay.ccbserver_code!=0) {// Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(test_tp_finedelay.ccbserver_code)); test_tp_finedelay.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",test_tp_finedelay.code1); test_tp_finedelay.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",test_tp_finedelay.code2); test_tp_finedelay.msg+=tmpstr;
    sprintf(tmpstr,"CpuCkDelay: %d\n",test_tp_finedelay.CpuCkDelay); test_tp_finedelay.msg+=tmpstr;
    sprintf(tmpstr,"TpFineDelay1: %d\n",test_tp_finedelay.TpFineDelay1); test_tp_finedelay.msg+=tmpstr;
    sprintf(tmpstr,"TpFineDelay2: %d\n",test_tp_finedelay.TpFineDelay2); test_tp_finedelay.msg+=tmpstr;
  }

  return idx;
}


// Print result of "Test Fine Delay" (0x82) to stdout
// Author: A. Parenti, Jan17 2005
// Modified: AP, Dec11 2006
void Cfe::test_TP_finedelay_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",test_tp_finedelay.msg.c_str());
}


/**********************************/
/* "Set TP offset" (0x8B) command */
/**********************************/

// Send "Set TP offset" (0x8B) command and decode reply
// Author: A. Parenti, Jan18 2005
// Note: Tested at LNL, Mar09 2005
// Modfied: AP, Jul14 2005 (updated to firmware 1.9)
// Modified: AP, Sep08 2006
void Cfe::set_TP_offset(char TpOffset[8], char fineofs[2], char min, char max) {
  const unsigned char command_code=0x8B;
  unsigned char command_line[11];
  int i, send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

  mc_firmware_version(); // Get firmware version (needed for send_command)

// lock "cmd_mutex"
  cmd_obj->cmdlock();

  if ((max-min)<SET_TP_OFFSET_MAX_MIN)
    max = min + SET_TP_OFFSET_MAX_MIN;

// create the command line
  command_line[0]=command_code;
  for (i=0;i<8;++i)
    command_line[1+i] = TpOffset[i];
  for (i=0;i<2;++i)
    command_line[9+i] = fineofs[i];
  command_line[11] = min;
  command_line[12] = max;

// Send the command line
  set_TP_offset_reset(); // Reset set_tp_offset struct
  if (HVer<1 || (HVer==1 && LVer<9)) // firmware < v1.9
    send_command_result = cmd_obj->send_command(command_line,11,SET_TP_OFFSET_TIMEOUT);
  else  // firmware >= v1.9
    send_command_result = cmd_obj->send_command(command_line,13,SET_TP_OFFSET_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = set_TP_offset_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    set_tp_offset.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nSet TP offset (0x8B): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset set_tp_offset struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec11 2006
void Cfe::set_TP_offset_reset() {
  if (this==NULL) return; // Object deleted -> return

  set_tp_offset.code1=set_tp_offset.code2=0;

  set_tp_offset.ccbserver_code=-5;
}


// Decode "Set TP offset" (0x8B) reply
// Author: A. Parenti, Feb01 2005
// Modified: AP, Dec11 2006
int Cfe::set_TP_offset_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x8B, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure

  idx = rstring(rdstr,idx,&set_tp_offset.code1);
  idx = rstring(rdstr,idx,&set_tp_offset.code2);

// Fill-up the error code
  if (set_tp_offset.code1==CCB_BUSY_CODE) // ccb is busy
    set_tp_offset.ccbserver_code = -1;
  else if (set_tp_offset.code1==CCB_UNKNOWN1 && set_tp_offset.code2==CCB_UNKNOWN2) // ccb: unknown command
    set_tp_offset.ccbserver_code = 1;
  else if (set_tp_offset.code1==right_reply && set_tp_offset.code2==command_code) // ccb: right reply
    set_tp_offset.ccbserver_code = 0;
  else // wrong ccb reply
    set_tp_offset.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  set_tp_offset.msg="\nSet TP offset (0x8B)\n";

  if (set_tp_offset.ccbserver_code!=0) {// Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(set_tp_offset.ccbserver_code)); set_tp_offset.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",set_tp_offset.code1); set_tp_offset.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",set_tp_offset.code2); set_tp_offset.msg+=tmpstr;
  }

  return idx;
}


// Print result of "Set TP offset" (0x8B) to stdout
// Author: A. Parenti, Jan18 2005
// Modified: AP, Dec11 2006
void Cfe::set_TP_offset_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",set_tp_offset.msg.c_str());
}


/***********************************/
/* "Read TP offset" (0x8C) command */
/***********************************/

// Send "Read TP offset" (0x8C) command and decode reply
// Author: A. Parenti, Jan18 2005
// Note: Tested at LNL, Mar09 2005
// Modified: AP, Jul14 2005 (updated to firmware 1.9)
void Cfe::read_TP_offset() {
  const unsigned char command_code=0x8C;
  unsigned char command_line[1];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

  mc_firmware_version(); // Get firmware version (needed for decoding)

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;

  read_TP_offset_reset(); // Reset read_tp_offset struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1,READ_TP_OFFSET_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = read_TP_offset_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    read_tp_offset.ccbserver_code= send_command_result;

  if (_verbose_)
    printf("\nRead TP offset (0x8C): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset read_tp_offset struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec11 2006
void Cfe::read_TP_offset_reset() {
  int i;

  if (this==NULL) return; // Object deleted -> return

  read_tp_offset.code=0;
  for (i=0;i<8;++i)
    read_tp_offset.TpOffset[i]=0;
  for (i=0;i<2;++i)
    read_tp_offset.fineofs[i]=0;
  read_tp_offset.min=read_tp_offset.max=0;

  read_tp_offset.ccbserver_code=-5;
  read_tp_offset.msg="";
}


// Decode "Read TP offset" (0x8C) reply
// Author: A. Parenti, Feb01 2005
// Modified: AP, Dec11 2006
int Cfe::read_TP_offset_decode(unsigned char *rdstr) {
  const unsigned char right_reply=0x8D;
  unsigned char code2;
  int i, idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure

  idx = rstring(rdstr,idx,&read_tp_offset.code);
  code2 = rdstr[idx];

// Fill-up the "result" structure

  for (i=0;i<8;++i)
    idx = rstring(rdstr,idx,read_tp_offset.TpOffset+i);
  for (i=0;i<2;++i)
    idx = rstring(rdstr,idx,read_tp_offset.fineofs+i);

  if (HVer>1 || (HVer==1 && LVer>=9)) {// Firmware >= v1.9
    idx = rstring(rdstr,idx,&read_tp_offset.min);
    idx = rstring(rdstr,idx,&read_tp_offset.max);
  }

// Fill-up the error code
  if (read_tp_offset.code==CCB_BUSY_CODE) // ccb is busy
    read_tp_offset.ccbserver_code= -1;
  else if (read_tp_offset.code==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    read_tp_offset.ccbserver_code= 1;
  else if (read_tp_offset.code==right_reply) // ccb: right reply
    read_tp_offset.ccbserver_code= 0;
  else // wrong ccb reply
     read_tp_offset.ccbserver_code= 2;

// Fill-up message
  char tmpstr[100];

  read_tp_offset.msg="\nRead TP offset (0x8C)\n";

  if (read_tp_offset.ccbserver_code!=0) {// Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(read_tp_offset.ccbserver_code)); read_tp_offset.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code: %#2X\n", read_tp_offset.code); read_tp_offset.msg+=tmpstr;
    for (i=0;i<8;++i) {
      sprintf(tmpstr,"TpOffset[%d]: %d\n", i, read_tp_offset.TpOffset[i]); read_tp_offset.msg+=tmpstr;
    }
    for (i=0;i<2;++i) {
      sprintf(tmpstr,"fineofs[%d]: %d\n", i, read_tp_offset.fineofs[i]); read_tp_offset.msg+=tmpstr;
    }

    if (HVer>1 || (HVer==1 && LVer>=9)) {// Firmware >= v1.9
      sprintf(tmpstr,"min: %d\n", read_tp_offset.min); read_tp_offset.msg+=tmpstr;
      sprintf(tmpstr,"max: %d\n", read_tp_offset.max); read_tp_offset.msg+=tmpstr;
    }
  }


  return idx;
}


// Print result of "Read TP offset" (0x8C) to stdout
// Author: A. Parenti, Jan18 2005
// Modified: AP, Dec11 2006
void Cfe::read_TP_offset_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",read_tp_offset.msg.c_str());
}

/* DAC */


/*****************************/
/* "Test DAC" (0x68) command */
/*****************************/

// Send "Test DAC" (0x68) command and decode reply
// Author: A. Parenti, Jan17 2005
// Note: Tested at LNL, Mar09 2005
// Modified: AP, Mar24 2005 
void Cfe::test_DAC() {
  const unsigned char command_code=0x68;
  unsigned char command_line[1];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;

  test_DAC_reset(); // Reset test_dac struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1,TEST_DAC_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = test_DAC_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    test_dac.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nTest DAC (0x68): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset test_dac struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec11 2006
void Cfe::test_DAC_reset() {
  if (this==NULL) return; // Object deleted -> return

  test_dac.code1=test_dac.code2=test_dac.result=0;

  test_dac.ccbserver_code=-5;
  test_dac.msg="";
}


// Decode "Test DAC" (0x68) reply
// Author: A. Parenti, Feb01 2005
// Modified: AP, Dec12 2006
int Cfe::test_DAC_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x68, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure

  idx = rstring(rdstr,idx,&test_dac.code1);
  idx = rstring(rdstr,idx,&test_dac.code2);

// Fill-up the "result" structure
  idx = rstring(rdstr,idx,&test_dac.result);

// Fill-up the error code
  if (test_dac.code1==CCB_BUSY_CODE) // ccb is busy
    test_dac.ccbserver_code = -1;
  else if (test_dac.code1==CCB_UNKNOWN1 && test_dac.code2==CCB_UNKNOWN2) // ccb: unknown command
    test_dac.ccbserver_code = 1;
  else if (test_dac.code1==right_reply && test_dac.code2==command_code) // ccb: right reply
    test_dac.ccbserver_code = 0;
  else // wrong ccb reply
    test_dac.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  test_dac.msg="\nTest DAC (0x68)\n";

  if (test_dac.ccbserver_code!=0) {// Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(test_dac.ccbserver_code)); test_dac.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",test_dac.code1); test_dac.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",test_dac.code2); test_dac.msg+=tmpstr;
//    sprintf(tmpstr,"Result: %#2X\n",test_dac.result); test_dac.msg+=tmpstr;

    switch (test_dac.result) {
    case 0x37f:
      test_dac.msg+="Result: All DACs are OK\n";
      break;
    default:
      test_dac.msg+="Result: DAC error\n";
    }
  }

  return idx;
}


// Print result of "Test DAC" (0x68) to stdout
// Author: A. Parenti, Jan17 2005
// Modified: AP, Dec11 2006
void Cfe::test_DAC_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",test_dac.msg.c_str());
}


/************************************/
/* "Calibration DAC" (0x6B) command */
/************************************/

// Send "Calib DAC" (0x6B) command and decode reply
// Author: A. Parenti, Jan17 2005
// Note: Tested at LNL, Mar09 2005
// Modified: AP, Mar24 2005
void Cfe::set_calib_DAC(float bias_gain[3], float thr_gain[3], float width_gain,
  float bias_offset[3], float thr_offset[3], float width_offset) {

  const unsigned char command_code=0x6B;
  unsigned char command_line[57];
  int i, send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  idx = 1;

  for (i=0;i<3;++i)
    idx = wstring(bias_gain[i],idx,command_line);
  for (i=0;i<3;++i)
    idx=wstring(thr_gain[i],idx,command_line);

  idx=wstring(width_gain,idx,command_line);

  for (i=0;i<3;++i)
    idx=wstring(bias_offset[i],idx,command_line);
  for (i=0;i<3;++i)
    idx=wstring(thr_offset[i],idx,command_line);

  idx=wstring(width_offset,idx,command_line);

  idx = 0;

  set_calib_DAC_reset(); // Reset set_calib_dac struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,57,SET_CALIB_DAC_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = set_calib_DAC_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    set_calib_dac.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nCalibration DAC (0x6B): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset set_calib_dac struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec11 2006
void Cfe::set_calib_DAC_reset() {
  if (this==NULL) return; // Object deleted -> return

  set_calib_dac.code1=set_calib_dac.code2=0;

  set_calib_dac.ccbserver_code=-5;
  set_calib_dac.msg="";
}


// Decode "Calib DAC" (0x6B) reply
// Author: A. Parenti, Feb01 2005
// Modified: AP, Dec11 2006
int Cfe::set_calib_DAC_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x6B, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure

  idx = rstring(rdstr,idx,&set_calib_dac.code1);
  idx = rstring(rdstr,idx,&set_calib_dac.code2);

// Fill-up the error code
  if (set_calib_dac.code1==CCB_BUSY_CODE) // ccb is busy
    set_calib_dac.ccbserver_code = -1;
  else if (set_calib_dac.code1==CCB_UNKNOWN1 && set_calib_dac.code2==CCB_UNKNOWN2) // ccb: unknown command
    set_calib_dac.ccbserver_code = 1;
  else if (set_calib_dac.code1==right_reply && set_calib_dac.code2==command_code) // ccb: right reply
    set_calib_dac.ccbserver_code = 0;
  else // wrong ccb reply
    set_calib_dac.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  set_calib_dac.msg="\nCalibration DAC (0x6B)\n";

  if (set_calib_dac.ccbserver_code!=0) {// Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(set_calib_dac.ccbserver_code)); set_calib_dac.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n", set_calib_dac.code1); set_calib_dac.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n", set_calib_dac.code2); set_calib_dac.msg+=tmpstr;
  }

  return idx;
}


// Print result of "Calibration DAC" (0x6B) to stdout
// Author: A. Parenti, Jan17 2005
// Modified: AP, Dec11 2006
void Cfe::set_calib_DAC_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",set_calib_dac.msg.c_str());
}


/*****************************************/
/* "Read Calibration DAC" (0x6E) command */
/*****************************************/

// Send "Calib DAC" (0x6E) command and decode reply
// Author: A. Parenti, Jan17 2005
// Note: Tested at LNL, Mar09 2005
// Modified: AP, Mar24 2005
void Cfe::read_calib_DAC() {
  const unsigned char command_code=0x6E;
  unsigned char command_line[1];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;

  read_calib_DAC_reset(); // Reset read_calib_dac struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1,READ_CALIB_DAC_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = read_calib_DAC_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    read_calib_dac.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nRead Calibration DAC (0x6E): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset read_calib_dac struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec11 2006
void Cfe::read_calib_DAC_reset() {
  int i;

  if (this==NULL) return; // Object deleted -> return

  read_calib_dac.code=0;
  for (i=0;i<3;++i) {
    read_calib_dac.bias_gain[i]=read_calib_dac.thr_gain[i]=0;
    read_calib_dac.bias_offset[i]=read_calib_dac.thr_offset[i]=0;
  }
  read_calib_dac.width_gain=read_calib_dac.width_offset=0;

  read_calib_dac.ccbserver_code=-5;
  read_calib_dac.msg="";
}


// Decode "Calib DAC" (0x6E) reply
// Author: A. Parenti, Feb01 2005
// Modified: AP, Dec11 2006
int Cfe::read_calib_DAC_decode(unsigned char *rdstr) {
  const unsigned char right_reply=0x6F;
  unsigned char code2;
  int i, idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure

  idx = rstring(rdstr,idx,&read_calib_dac.code);
  code2 = rdstr[idx];

// Fill-up the "result" structure
  for (i=0;i<3;++i)
    idx = rstring(rdstr,idx,read_calib_dac.bias_gain+i);
  for (i=0;i<3;++i)
    idx = rstring(rdstr,idx,read_calib_dac.thr_gain+i);
  idx = rstring(rdstr,idx,&read_calib_dac.width_gain);

  for (i=0;i<3;++i)
    idx = rstring(rdstr,idx,read_calib_dac.bias_offset+i);
  for (i=0;i<3;++i)
    idx = rstring(rdstr,idx,read_calib_dac.thr_offset+i);
  idx = rstring(rdstr,idx,&read_calib_dac.width_offset);

// Fill-up the error code
  if (read_calib_dac.code==CCB_BUSY_CODE) // ccb is busy
    read_calib_dac.ccbserver_code = -1;
  else if (read_calib_dac.code==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    read_calib_dac.ccbserver_code = 1;
  else if (read_calib_dac.code==right_reply) // ccb: right reply
    read_calib_dac.ccbserver_code = 0;
  else // wrong ccb reply
    read_calib_dac.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  read_calib_dac.msg="\nRead Calibration DAC (0x6E)\n";

  if (read_calib_dac.ccbserver_code!=0) {// Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(read_calib_dac.ccbserver_code)); read_calib_dac.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code: %#2X\n",read_calib_dac.code); read_calib_dac.msg+=tmpstr;

    for (i=0;i<3;++i) {
      sprintf(tmpstr,"bias_gain[%d]: %.2f\n",i,read_calib_dac.bias_gain[i]); read_calib_dac.msg+=tmpstr;
    }
    for (i=0;i<3;++i){
      sprintf(tmpstr,"thr_gain[%d]: %.2f\n",i,read_calib_dac.thr_gain[i]); read_calib_dac.msg+=tmpstr;
    }
    sprintf(tmpstr,"width_gain: %.2f\n",read_calib_dac.width_gain); read_calib_dac.msg+=tmpstr;

    for (i=0;i<3;++i) {
      sprintf(tmpstr,"bias_offset[%d]: %.2f\n",i,read_calib_dac.bias_offset[i]); read_calib_dac.msg+=tmpstr;
    }
    for (i=0;i<3;++i) {
      sprintf(tmpstr,"thr_offset[%d]: %.2f\n",i,read_calib_dac.thr_offset[i]); read_calib_dac.msg+=tmpstr;
    }
    sprintf(tmpstr,"width_offset: %.2f\n",read_calib_dac.width_offset); read_calib_dac.msg+=tmpstr;
  }

  return idx;
}



// Print result of "Read Calibration DAC" (0x6E) to stdout
// Author: A. Parenti, Jan17 2005
// Modified: AP, Dec11 2006
void Cfe::read_calib_DAC_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",read_calib_dac.msg.c_str());
}

