#include <ccbcmds/Ctsm.h>
#include <ccbcmds/apfunctions.h>

#include <iostream>
#include <cstdio>

/*****************************************************************************/
// TSM class
/*****************************************************************************/

// Class constructor; arguments: pointer_to_command_object
// Author: A.Parenti, Dec01 2006
Ctsm::Ctsm(Ccommand* ppp) : Ccmn_cmd(ppp) {
  read_TSM_reset(); // Reset read_tsm struct
  write_TSM_reset(); // Reset write_tsm struct
  TrgOut_select_reset(); // Reset trgout_sel struct
}


/****************************/
/* Override ROB/TDC methods */
/****************************/

void Ctsm::read_TDC(char tdc_brd, char tdc_chip) {}
int Ctsm::read_TDC_decode(unsigned char *rdstr) {return 0;}
void Ctsm::read_TDC_print() {}

void Ctsm::status_TDC(char tdc_brd, char tdc_chip) {}
void Ctsm::status_TDC_print() {}

void Ctsm::read_ROB_error() {}
int Ctsm::read_ROB_error_decode(unsigned char *rdstr) {return 0;}
void Ctsm::read_ROB_error_print() {}

/*****************/
/* Configure TSM */
/*****************/

// Configure a TSM, reading from a "myconf" object
// Author: A.Parenti, Sep02 2005
// Modified: AP, Apr10 2006
// Modified: AP, Nov17 2006 (Added CRC)
short Ctsm::config_TSM(Cconf *myconf) {
  short conferr, confstatus, confcrc;

  if (this==NULL) return 0; // Object deleted -> return

// TSM error is BIT4 of conferr
  conferr = config_mc(myconf,CTSM,-1,-1,&confstatus,&confcrc);

  if (((conferr>>4)&0x1)!=0) {
    config_TSM_result.error = true;
    config_TSM_result.crc = 0;
    return 1;
  }
  else {
    config_TSM_result.error = false;
    config_TSM_result.crc = confcrc;
    return 0;
  }
}

// Print to stdout the result of TSM configuration
// Author: A.Parenti, Sep02 2005
// Modified: AP, Nov17 2006
void Ctsm::config_TSM_print() {
  if (this==NULL) return; // Object deleted -> return

  if (config_TSM_result.sent>0) {
    printf("Configure TSM: %d lines sent (%d successful)\n",config_TSM_result.sent,config_TSM_result.sent-config_TSM_result.bad);
    printf("               CRC=%#hX\n",config_TSM_result.crc);
    printf("               Error=%d\n",config_TSM_result.error);
  }
}


// Retrive TSM config from CCB, dump it to file
// Author: A.Parenti, Oct30 2006
// Modified: AP, Nov02 2006
void Ctsm::read_TSM_config(char *filename) {
  unsigned char cmdline[7];
  char strcmd[22];
  int i;
  std::ofstream outfile(filename,std::ofstream::app);

  if (this==NULL) return; // Object deleted -> return

  printf("Read TSM config (0x17/0x49).\n");

  if (outfile.is_open()) {

// TSM, 0x17
    read_TSM();

    if (read_tsm.code==0x20) {
      cmdline[0]=0x17;
      mystrcpy(cmdline+1,read_tsm.conf_sorter,2);
      mystrcpy(cmdline+3,read_tsm.conf_dataup,2);
      mystrcpy(cmdline+5,read_tsm.conf_datadown,2);

      strcpy(strcmd,"");
      sprintf(strcmd,"%02X",cmdline[0]);
      for (i=1;i<7;++i)
        sprintf(strcmd,"%s %02X",strcmd,cmdline[i]);
      outfile << strcmd << '\n';
      outfile.flush();
    }


// TSM, 0x49
    MC_read_status();

    if (mc_status.code==0x13) {
      cmdline[0]=0x49;
      cmdline[1]=mc_status.EnTrgPhi;
      cmdline[2]=mc_status.EnTrgThe;
      cmdline[3]=mc_status.EnTrgH;


      strcpy(strcmd,"");
      sprintf(strcmd,"%02X",cmdline[0]);
      for (i=1;i<4;++i)
        sprintf(strcmd,"%s %02X",strcmd,cmdline[i]);
      outfile << strcmd << '\n';
      outfile.flush();
    }

      outfile.close();
    } else
      printf("Impossible to open file '%s'.\n",filename);

}



/**********************************/
/* "read TSM" (0x1F) command */
/**********************************/

// Send "read TSM" (0x1F) command and decode reply
// Author: A. Parenti, Dec18 2004
// Note: Tested at LNL, Mar09 2005
// Modified: AP, Mar24 2005
void Ctsm::read_TSM() {
  unsigned char command_line[1]={0x1F};
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

  read_TSM_reset(); // Reset read_tsm struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1,READ_TSM_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = read_TSM_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    read_tsm.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nRead TSM (0x1F): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset read_tsm struct
// Author: A.Parenti. Aug09 2006
// Modified: AP, Dec07 2006
void Ctsm::read_TSM_reset() {
  int i;

  if (this==NULL) return; // Object deleted -> return

  read_tsm.code=0;
  for (i=0;i<2;++i)
    read_tsm.conf_sorter[i]=read_tsm.conf_dataup[i]=
      read_tsm.conf_datadown[i]=0;

  read_tsm.ccbserver_code=-5;
  read_tsm.msg="";
}


// Decode "read TSM" (0x1F) reply
// Author: A. Parenti, Feb08 2005
// Modified: AP, Dec07 2006
int Ctsm::read_TSM_decode(unsigned char *rdstr) {
  const unsigned char right_reply=0x20;
  unsigned char code2;
  int i, idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&read_tsm.code);
  code2 = rdstr[idx];

// Fill-up the "result" structure

  for (i=0;i<2;++i)
    idx = rstring(rdstr,idx,read_tsm.conf_sorter+i);
  for (i=0;i<2;++i)
    idx = rstring(rdstr,idx,read_tsm.conf_dataup+i);
  for (i=0;i<2;++i)
    idx = rstring(rdstr,idx,read_tsm.conf_datadown+i);


// Fill-up the error code
  if (read_tsm.code==CCB_BUSY_CODE) // ccb is busy
    read_tsm.ccbserver_code = -1;
  else if (read_tsm.code==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    read_tsm.ccbserver_code = 1;
  else if (read_tsm.code==right_reply) // ccb: right reply
    read_tsm.ccbserver_code = 0;
  else // wrong ccb reply
    read_tsm.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  read_tsm.msg="\nRead TSM (0x1F)\n";

  if (read_tsm.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(read_tsm.ccbserver_code)); read_tsm.msg+=tmpstr;
  } else {

    sprintf(tmpstr,"code: %#2X\n", read_tsm.code); read_tsm.msg+=tmpstr;
    for (i=0;i<2;++i) {
      sprintf(tmpstr,"conf_sorter[%i]: %#2X\n",i,read_tsm.conf_sorter[i]); read_tsm.msg+=tmpstr;
    }
    for (i=0;i<2;++i) {
      sprintf(tmpstr,"conf_dataup[%i]: %#2X\n",i,read_tsm.conf_dataup[i]); read_tsm.msg+=tmpstr;
    }
    for (i=0;i<2;++i) {
      sprintf(tmpstr,"conf_datadown[%i]: %#2X\n",i,read_tsm.conf_datadown[i]); read_tsm.msg+=tmpstr;
    }
  }

  return idx;
}



// Print result of "read TSM" (0x1F) to stdout
// Author: A. Parenti, Dec18 2004
// Modified: AP, Dec07 2006
void Ctsm::read_TSM_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",read_tsm.msg.c_str());
}


/******************************/
/* "write TSM" (0x17) command */
/******************************/

// Send "write TSM" (0x17) command and decode reply
// Author: A. Parenti, Dec18 2004
// Note: Tested at LNL, Mar09 2005
// Modified: AP, Mar24 2005
void Ctsm::write_TSM(unsigned char conf_sorter[2], unsigned char conf_dataup[2], unsigned char conf_datadown[2]) {
  const unsigned char command_code=0x17;
  unsigned char command_line[7];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  mystrcpy(command_line+1,conf_sorter,2);
  mystrcpy(command_line+3,conf_dataup,2);
  mystrcpy(command_line+5,conf_datadown,2);

  write_TSM_reset(); // Reset write_tsm struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,7,WRITE_TSM_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = write_TSM_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    write_tsm.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nWrite TSM (0x17): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset write_tsm struct
// Author: A.Parenti, Aug09 2006
// Modified: AP, Dec07 2006
void Ctsm::write_TSM_reset() {
  if (this==NULL) return; // Object deleted -> return

  write_tsm.code1=write_tsm.code2= write_tsm.result=0;

  write_tsm.ccbserver_code=-5;
  write_tsm.msg="";
}


// Decode "write TSM" (0x17) reply
// Author: A. Parenti, Feb08 2005
// Modified: AP, Dec12 2006
int Ctsm::write_TSM_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x17, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  write_tsm.code1  = rdstr[0];
  write_tsm.code2  = rdstr[1];

// Fill-up the "result" structure
  write_tsm.result = rdstr[2];

  idx=3;

// Fill-up the error code
  if (write_tsm.code1==CCB_BUSY_CODE) // ccb is busy
    write_tsm.ccbserver_code = -1;
  else if (write_tsm.code1==CCB_UNKNOWN1 && write_tsm.code2==CCB_UNKNOWN2) // ccb: unknown command
    write_tsm.ccbserver_code = 1;
  else if (write_tsm.code1==right_reply && write_tsm.code2==command_code) // ccb: right reply
    write_tsm.ccbserver_code = 0;
  else // wrong ccb reply
    write_tsm.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  write_tsm.msg="\nWrite TSM (0x17)\n";

  if (write_tsm.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(write_tsm.ccbserver_code)); write_tsm.msg+=tmpstr;
  } else {

    sprintf(tmpstr,"code1: %#2X\n",write_tsm.code1); write_tsm.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",write_tsm.code2); write_tsm.msg+=tmpstr;

    switch (write_tsm.result) {
    case 0:
      write_tsm.msg+="Result: write error\n";
      break;
    default:
      write_tsm.msg+="Result: write successful\n";
    }
  }

  return idx;
}


// Print results of "write TSM" (0x17) command to standard output
// Author:  A.Parenti Dec18 2004
// Modified: AP, Dec07 2006
void Ctsm::write_TSM_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",write_tsm.msg.c_str());
}


/**********************************/
/* "TrgOut Select" (0x49) command */
/**********************************/

// Send "TrgOut Select" (0x49) command and decode reply
// Author: A. Parenti, Jan12 2005
// Note: Tested at LNL, Mar09 2005
// Modified: AP, Mar24 2005
void Ctsm::TrgOut_select(char EnTrg_phi, char EnTrg_theta, char HTrg) {
  const unsigned char command_code=0x49;
  unsigned char command_line[4];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=EnTrg_phi;
  command_line[2]=EnTrg_theta;
  command_line[3]=HTrg;

  TrgOut_select_reset(); // Reset trgout_sel struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,4,TRGOUT_SELECT_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = TrgOut_select_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    trgout_sel.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nTrgOut Select (0x49): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset trgout_sel struct
// Author: A.Parenti, Aug09 2006
// Modified: AP, Dec07 2006
void Ctsm::TrgOut_select_reset() {
  if (this==NULL) return; // Object deleted -> return

  trgout_sel.code1=trgout_sel.code2=0;

  trgout_sel.ccbserver_code=-5;
  trgout_sel.msg="";
}


// Decode "TrgOut Select" (0x49) reply
// Author: A. Parenti, Feb08 2005
// Modified: AP, Dec07 2006
int Ctsm::TrgOut_select_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x49, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  trgout_sel.code1  = rdstr[0];
  trgout_sel.code2  = rdstr[1];

  idx=2;

// Fill-up the error code
  if (trgout_sel.code1==CCB_BUSY_CODE) // ccb is busy
    trgout_sel.ccbserver_code = -1;
  else if (trgout_sel.code1==CCB_UNKNOWN1 && trgout_sel.code2==CCB_UNKNOWN2) // ccb: unknown command
    trgout_sel.ccbserver_code = 1;
  else if (trgout_sel.code1==right_reply && trgout_sel.code2==command_code) // ccb: right reply
    trgout_sel.ccbserver_code = 0;
  else // wrong ccb reply
    trgout_sel.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  trgout_sel.msg="\nTrgOut Select (0x49)\n";

  if (trgout_sel.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(trgout_sel.ccbserver_code)); trgout_sel.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",trgout_sel.code1); trgout_sel.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",trgout_sel.code2); trgout_sel.msg+=tmpstr;
  }

  return idx;
}


// Print results of "TrgOut Select" (0x49) command to standard output
// Author:  A.Parenti Jan12 2005
// Modified: AP, Dec07 2006
void Ctsm::TrgOut_select_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",trgout_sel.msg.c_str());
}
