#include <ccbcmds/Cdtdcs.h>

#include <ccbcmds/Cccb.h>
#include <ccbcmds/Ccmn_cmd.h>
#include <ccbcmds/Cconf.h>
#include <ccbcmds/Cref.h>
#include <ccbcmds/apfunctions.h>
#include <ccbcmds/Ci2c.h>
#include <ccbcmds/Cfe.h>
#include <ccbcmds/Crob.h>
#include <ccbcmds/Cttcrx.h>

#ifdef __USE_CCBDB__ // DB access enabled
  #include <ccbdb/Cdtpartition.h>
  #include <ccbdb/Cccbconfdb.h>
  #include <ccbdb/Cccbmap.h>
  #include <ccbdb/Cdaccalib.h>
#endif

#include <pthread.h> // POSIX threads

#include <cstdio>
#include <ctime>

#include <unistd.h>


void ap_testcancel(int tid); // test thread for pending cancellations

// Declare single ccb functions
void *monitor_a_mc(void *ccb);
void *monitor_a_pr(void *ccb);
void *recovertask_a_mc(void *ccb);
void *configure_a_mc(void *ccb);
void *setrun_a_mc(void *ccb);
void *unsetrun_a_mc(void *ccb);
void *sendcommand_a_mc(void *ccb);

/*****************************************************************************/
// Single CCB functions
// Only used in this source file
/*****************************************************************************/

// Test the calling thread for pending cancellations
// Author: A.Parenti, Oct03 2007
void ap_testcancel(int tid) {
  if (tid==0) pthread_exit(NULL); // Thread canceled -> exit

  pthread_setcancelstate(PTHREAD_CANCEL_ENABLE,NULL);
  pthread_testcancel();
  pthread_setcancelstate(PTHREAD_CANCEL_DISABLE,NULL);
}

// Monitor the status of a single minicrate
// Author: A. Parenti, Feb05 2006
// Modified/Tested: AP, Oct22 2007
// Modified/Tested: S.Ventura, Feb20 2007 (Qpll status added)
void *monitor_a_mc(void *ccb) {
  int j;
  time_t writetime=0;
  bool mc_program;
  char message[1024];
  Emcstatus phys_status, logic_status;

  Sdtccb *lccb = (Sdtccb*)ccb;

  ap_testcancel(lccb->mc_monitor_tid); // Test for pending cancellations

// Disable thread cancellation
  pthread_setcancelstate(PTHREAD_CANCEL_DISABLE,NULL);

  Cccb myCcb(lccb->ccbcmd); // "CCB" object
  Crob myRob(lccb->ccbcmd); // "ROB" object
#ifdef __USE_CCBDB__ // DB access enabled
  Cref myRef(lccb->ccbid,lccb->dtdbobj); // Read reference values
#endif

// Set verbosity level
  myCcb.verbose_mode(lccb->_verbose_);

  if ((lccb->mc_monitor_int)<=MC_MONITOR_MIN_INT)
    lccb->mc_monitor_int = MC_MONITOR_INTERVAL; // Set to default

  if ((lccb->mc_write_int)<=(lccb->mc_monitor_int))
    lccb->mc_write_int=lccb->mc_monitor_int; // Must be >= monitoring interval

  printf("Start monitoring ccb status: id=%d, port=%d, interval=%d s, logging every %d s\n",lccb->ccbid,lccb->ccbport,lccb->mc_monitor_int,lccb->mc_write_int);

  apsleep(lccb->mc_monitor_delay); // Delay of monitor thread

// Tell to the minicrate its MC type
  short ChamberMap = (((lccb->wheel)<<8)&0x0F00) + (((lccb->sector)<<8)&0x00F0) + ((lccb->station)&0x000F);
  myCcb.chamber_map(ChamberMap);

  while(1) { // Infinite loop
    if (lccb->_verbose_)
      printf("*** %s. Monitoring status CCB=%d\n",get_time(),lccb->ccbid);

    ap_testcancel(lccb->mc_monitor_tid); // Test for pending cancellations

#ifdef __USE_CCBDB__ // DB access enabled
  #ifdef LOGTODB
    if ((lccb->mc_status_time-writetime)>=(lccb->mc_write_int)) {
      myCcb.MC_status_to_db(&myRef); // Write status and temp's to DB
      writetime=lccb->mc_status_time;
    } else
  #endif
    {
      myCcb.MC_check_status(&myRef,true,&mc_program,&phys_status,&logic_status,message); // Check status      
      myCcb.MC_temp(); // Read temperatures
    }
#else
    myCcb.MC_read_status();
    myCcb.MC_temp(); // Read temperatures
#endif

    if (myCcb.mc_status.ccbserver_code != 0) { // Error -> Re-Check status
      for (j=0;j<RETRY_INTERVAL;++j) { // Sleep for RETRY_INTERVAL sec's
        ap_testcancel(lccb->mc_monitor_tid); // Test for pending cancellations
        sleep(1);
      }
#ifdef __USE_CCBDB__ // DB access enabled
      myCcb.MC_check_status(&myRef,true,&mc_program,&phys_status,&logic_status,message);
#else
      myCcb.MC_read_status();
#endif
    }

    if (myCcb.mc_status.ccbserver_code == 0) { // Copy to lccb structure
      lccb->mc_status_time=time(NULL); // Update mc_status_time
    }

// Copy board temperatures from 0x3C
    lccb->MaxTemp=0.0;
    lccb->MaxTempRob=lccb->MaxTempTrb=lccb->MaxTempExt=lccb->MaxTempFe=0.0;
    for (j=0;j<6;++j)
      lccb->TempExt[j]=0.0;

    if (myCcb.mc_temp.code==0x3D) { // Right reply
      for (j=0;j<=6;++j) // ROB max temp
        lccb->MaxTempRob = ((lccb->MaxTempRob)>?myCcb.mc_temp.temp[j]);
      for (j=7;j<=14;++j) // TRB max temp
        lccb->MaxTempTrb = ((lccb->MaxTempTrb)>?myCcb.mc_temp.temp[j]);
      for (j=15;j<=20;++j) // Ext sensor max temp
        lccb->MaxTempExt = ((lccb->MaxTempExt)>?myCcb.mc_temp.temp[j]);
      for (j=0;j<6;++j) // All Ext sensor temp's
        lccb->TempExt[j]=myCcb.mc_temp.temp[15+j];

// Overall max temperature;
      lccb->MaxTemp = (lccb->MaxTemp) >? (lccb->MaxTempRob);
      lccb->MaxTemp = (lccb->MaxTemp) >? (lccb->MaxTempTrb);
      lccb->MaxTemp = (lccb->MaxTemp) >? (lccb->MaxTempExt);
    }
    if (myCcb.mc_status.code==0x13) {
      lccb->MaxTempFe = ((lccb->MaxTempFe)>?(myCcb.mc_status.in_Tmax)/10.0);
      lccb->MaxTempFe = ((lccb->MaxTempFe)>?(myCcb.mc_status.th_Tmax/10.0));
      lccb->MaxTempFe = ((lccb->MaxTempFe)>?(myCcb.mc_status.out_Tmax/10.0));
// Overall max temperature;
      lccb->MaxTemp = (lccb->MaxTemp) >? myCcb.mc_status.BrdMaxtemp;
      lccb->MaxTemp = (lccb->MaxTemp) >? (lccb->MaxTempFe);

//QPLL status
      lccb->QpllARdy = myCcb.mc_status.QpllARdy;
      lccb->QpllBRdy = myCcb.mc_status.QpllBRdy;
      lccb->AlrmQpllAChng=myCcb.mc_status.AlrmQpllAChng;
      lccb->AlrmQpllBChng=myCcb.mc_status.AlrmQpllBChng;
    }

  ap_testcancel(lccb->mc_monitor_tid); // Test for pending cancellations
// Check ROB status
#ifdef __USE_CCBDB__ // DB access enabled
  #ifdef LOGTODB
    if ((lccb->mc_status_time-writetime)>=(lccb->mc_write_int))
      myRob.status_ROB_to_db(&myRef); // Read status and write to DB
    else
  #endif
      myRob.status_ROB(&myRef); // Just read status
#endif

// Copy status variables
    if (phys_status==s_undef && lccb->physst==s_undef)
      lccb->physst = s_error; // 2nd undef -> goes to error
    else if (phys_status==s_undef && lccb->physst==s_error)
      lccb->physst = s_error; // Remain in error
    else
      lccb->physst = phys_status; // Copy lccb->physst

// Copy switch_off flags
    lccb->switch_off_fe=myCcb.mc_status_check.switch_off_fe;
    lccb->switch_off_mc=myCcb.mc_status_check.switch_off_mc;

    if (logic_status==s_undef && lccb->logicst==s_undef)
      lccb->logicst = s_error; // 2nd undef -> goes to error
    else if (logic_status==s_undef && lccb->logicst==s_error)
      lccb->logicst = s_error; // Remain in error
    else
      lccb->logicst = logic_status; // Copy lccb->physst

    if (myRob.status_rob.ccbserver_error) { // Communication error
      if (lccb->robst==s_undef) lccb->robst=s_error; // 2nd undef -> goto err
      else if (lccb->robst==s_error) lccb->robst=s_error; // Remain in error
      else lccb->robst=s_undef;
    } else {
      if (myRob.status_rob.error) lccb->robst=s_error;
      else lccb->robst=s_ok;
    }

    strcpy(lccb->ccbStatusMsg,myCcb.mc_status.msg.c_str()); // Copy CCB status message
    strcpy(lccb->ccbErrorMsg,message); // Copy CCB error message
    strcpy(lccb->robStatusMsg,myRob.status_rob.stmsg.c_str()); // Copy ROB status message
    strcpy(lccb->robErrorMsg,myRob.status_rob.errmsg.c_str()); // Copy ROB error message

// Sleep for some time (depending also on the cfg process)
    j=0;
    while (1) {
      ap_testcancel(lccb->mc_monitor_tid); // Test for pending cancellations

      if (lccb->ForceRefreshStatusMc) break; // Forced refresh of the status
      if (lccb->cfgall!=c_configuring && j>lccb->mc_monitor_int) break;
      if (lccb->cfgall==c_configuring && j>lccb->mc_monitor_int && j>MC_MONITOR_INT_CFG) break;

      sleep(1);
      ++j;
    }

    lccb->ForceRefreshStatusMc=false; // Disable forcing of refresh
  }

  return NULL;
}


// Monitor the pressure of a single chamber
// Author: A. Parenti, Aug08 2006
// Modified/Tested: AP, Dec07 2007
void *monitor_a_pr(void *ccb) {
  int j;
  time_t writetime=0;
  Sdtccb *lccb = (Sdtccb*)ccb;

  ap_testcancel(lccb->pr_monitor_tid); // Test for pending cancellations

// Disable thread cancellation
  pthread_setcancelstate(PTHREAD_CANCEL_DISABLE,NULL);

  Ci2c myI2c(lccb->ccbcmd); // "I2C" object
#ifdef __USE_CCBDB__ // DB access enabled
  Cref myRef(lccb->ccbid,lccb->dtdbobj); // Read reference values
#endif

// Set verbosity level
  myI2c.verbose_mode(lccb->_verbose_);

  if ((lccb->pr_monitor_int)<=PR_MONITOR_MIN_INT)
    lccb->pr_monitor_int = PR_MONITOR_INTERVAL; // Set to default

  if ((lccb->pr_write_int)<=(lccb->pr_monitor_int))
    lccb->pr_write_int=lccb->pr_monitor_int; // Must be >= monitoring interval

  printf("Start monitoring pressure ccb: id=%d, port=%d, interval=%d s, logging every %d s\n",lccb->ccbid,lccb->ccbport,lccb->pr_monitor_int,lccb->pr_write_int);

  apsleep(lccb->pr_monitor_delay); // Delay of monitor thread

  while(1) { // Infinite loop
    if (lccb->_verbose_)
      printf("*** %s. Monitoring pressure CCB=%d\n",get_time(),lccb->ccbid);

// Read PADC values
    bool padc_error;
    short adc_count[10];
    float volt_read[10], data_read[10];

    lccb->press100_hv=0.0; // Reset pressure
    lccb->press100_fe=0.0; // Reset pressure

    ap_testcancel(lccb->pr_monitor_tid); // Test for pending cancellations
    myI2c.read_PADC(&padc_error,adc_count,volt_read); // Read PADC

    if (padc_error) { // Error -> Re-Read PADC

      for (j=0;j<RETRY_INTERVAL;++j) { // Sleep for RETRY_INTERVAL sec's
        ap_testcancel(lccb->pr_monitor_tid); // Test for pending cancellations
        sleep(1);
      }
      myI2c.read_PADC(&padc_error,adc_count,volt_read);
    }

    if (!padc_error) {
      lccb->pr_status_time=time(NULL); // Update pr_status_time
      if (myI2c.use_PADC_LUT(adc_count,data_read)==0) {
        lccb->press100_hv=(data_read[0]+data_read[1])/2;
        lccb->press100_fe=(data_read[4]+data_read[5])/2;
      }
    }

#ifdef __USE_CCBDB__ // DB access enabled
  #ifdef LOGTODB
    if ((lccb->pr_status_time-writetime)>=PR_TO_DB_INTERVAL) {
      ap_testcancel(lccb->pr_monitor_tid); // Test for pending cancellations
      myI2c.PADC_status_to_db(&myRef,adc_count); // Write pressures to DB

      writetime=lccb->pr_status_time;
    } else
  #endif
    myI2c.status_PADC(&myRef,data_read);
#endif

// Copy status variables
    if (padc_error) { // Communication error
      if (lccb->padcst==s_undef) lccb->padcst=s_error; // 2nd undef -> goto err
      else if (lccb->padcst==s_error) lccb->padcst=s_error; // Remain in error
      else lccb->padcst=s_undef;
    } else {
      if (myI2c.status_padc.error) lccb->padcst=s_error;
      else lccb->padcst=s_ok;
    }

    strcpy(lccb->padcStatusMsg,myI2c.status_padc.stmsg.c_str()); // Copy PADC status message
    strcpy(lccb->padcErrorMsg,myI2c.status_padc.errmsg.c_str()); // Copy PADC error message

// Sleep for some time (depending also on the cfg process)
    j=0;
    while (1) {
      ap_testcancel(lccb->pr_monitor_tid); // Test for pending cancellations

      if (lccb->ForceRefreshStatusPr) break; // Forced refresh of the status
      if (lccb->cfgall!=c_configuring && j>lccb->pr_monitor_int) break;
      if (lccb->cfgall==c_configuring && j>lccb->pr_monitor_int && j>PR_MONITOR_INT_CFG) break;

      sleep(1);
      ++j;
    }

    lccb->ForceRefreshStatusPr=false; // Disable forcing of refresh
  }

  return NULL;
}


// Recover task:
// * swap from optical to copper link, and viceversa, if needed
// * any unreachable minicrate is flagged as "excluded"
// * any "excluded" minicrate is recovered (eg reconfigured) when appears again
// Author: A. Parenti, Mar02 2007
// Modified: AP, Apr03 2008
void *recovertask_a_mc(void *ccb) {
  int j, error, timewait;
  Sdtccb *lccb = (Sdtccb*)ccb;

  ap_testcancel(lccb->mc_recover_tid); // Test for pending cancellations

// Disable thread cancellation
  pthread_setcancelstate(PTHREAD_CANCEL_DISABLE,NULL);

  Cccb myCcb(lccb->ccbcmd); // "ccb" object

// Set thread attributes (to create detached configuration threads)
  pthread_attr_t thr_attr;
  pthread_attr_init(&thr_attr);
  pthread_attr_setdetachstate(&thr_attr,PTHREAD_CREATE_DETACHED);

#ifdef __USE_CCBDB__ // DB access enabled
  ccbmap myccbmap(lccb->dtdbobj);
#endif

// Set verbosity level
  myCcb.verbose_mode(lccb->_verbose_);

  printf("Start recover task: ccbid=%d, port=%d, interval=%d s\n",
        lccb->ccbid,lccb->ccbport,MC_RECOVER_INT_DFLT);

  apsleep(lccb->mc_recover_delay); // Delay of recover thread

  while(1) { // Infinite loop
    if (lccb->_verbose_)
      printf("*** %s. Recover task CCB=%d\n",get_time(),lccb->ccbid);

// Set "timewait" to default
    timewait=MC_RECOVER_INT_DFLT;

// First attempt to communicate

#if FALSE
// Force a recover of the MC threshold
    if (lccb->ccbcmd->use_primary_port()==0) 
       {
         printf("trying to retrain lost primary (ccbid %d, port %d )\n",lccb->ccbid,lccb->ccbport);
            lccb->ccbcmd->use_secondary_port();
            myCcb.auto_set_LINK();
            lccb->ccbcmd->use_primary_port();
       }
#endif

    ap_testcancel(lccb->mc_recover_tid); // Test for pending cancellations
      myCcb.MC_read_status(); // Read status

//A.P. original test 
//#if FALSE
    if (myCcb.mc_status.ccbserver_code != 0) { // Error -> Change port and retry

// Toggle optical<->copper link
      if(lccb->ccbcmd->use_secondary_port()!=0)
        lccb->ccbcmd->use_primary_port();

      ap_testcancel(lccb->mc_recover_tid); // Test for pending cancellations
      myCcb.MC_read_status(); // Read status
    }
//#endif
    if (myCcb.mc_status.ccbserver_code == 0)  // if MC is reachable on secondary try retrain
        if (lccb->ccbcmd->which_port())
        {
           myCcb.auto_set_LINK();
           lccb->ccbcmd->use_primary_port();
           myCcb.MC_read_status(); // Read status on primary
           if (myCcb.mc_status.ccbserver_code != 0) 
            {
               lccb->ccbcmd->use_secondary_port(); //else switch back on secondary
               myCcb.mc_status.ccbserver_code = 0;
            }
         }

    if (myCcb.mc_status.ccbserver_code == 0) { // Minicrate is reachable
      if (lccb->excluded && lccb->auto_recover) { // Recover CCB
        printf("****** RECOVERING CCB:%d\n",lccb->ccbid);

        ap_testcancel(lccb->mc_recover_tid); // Test for pending cancellations
        myCcb.GetConfigCRC(); // Get configuration CRC

        if ((myCcb.mc_status.HVersion<1 || (myCcb.mc_status.HVersion==1 && myCcb.mc_status.LVersion<16)) || // Old firmware, cannot check CfgNotChanged, CRC
            (myCcb.mc_status.RunInProgress && !myCcb.mc_status.CfgNotChanged) || // Configuration has changed during run
            (myCcb.getconfigcrc.ccbserver_code!=0 || myCcb.getconfigcrc.error!=0 || myCcb.getconfigcrc.crc!=lccb->confcrc)) { // configuration CRC is changed
          if (lccb->conf_tid==0 && (lccb->cfgall)!=c_configuring // Not yet configuring
              && strlen(lccb->DTconfname)>0) { // some configuration has been selected
            lccb->cfgall=c_configuring; // Set flag to "configuring"
            error=pthread_create(&(lccb->conf_tid),&thr_attr,configure_a_mc,(void*)(lccb)); // open thread
            if (error!=0) { // Thread not created (too much user processes?)
              printf("Error. Configuring thread not created. Ccb:%d -> Return\n",lccb->ccbid);
              lccb->conf_tid=0;
              lccb->cfgall=c_error; // Otherwise dtdcs stuck in "configuring"!
              error=0;
            }
          }
        }
        printf("****** RECOVERED  CCB:%d\n",lccb->ccbid);
      }
      lccb->excluded = false;
#ifdef __USE_CCBDB__ // DB access enabled
      ap_testcancel(lccb->mc_recover_tid); // Test for pending cancellations
      myccbmap.setonline(lccb->ccbid); // Set ccb online in DB
#endif
    } else { // Minicrate is unreacheable

      if (!lccb->excluded) { // Last time was reacheable...
        printf("****** EXCLUDED   CCB:%d\n",lccb->ccbid);
        timewait=MC_RECOVER_INT_SHRT; // Set "timewait" to a shorter interval
      }

      lccb->excluded = true; // exclude ccb from partition
//      lccb->ccbcmd->use_primary_port(); // MC not responding: stick to primary port

#ifdef __USE_CCBDB__ // DB access enabled
      ap_testcancel(lccb->mc_recover_tid); // Test for pending cancellations
      myccbmap.setoffline(lccb->ccbid); // Set ccb offline in DB
#endif
    }

    for (j=0;j<timewait;++j) { // Sleep for "timewait" sec's
      ap_testcancel(lccb->mc_recover_tid); // Test for pending cancellations
      sleep(1);
    }
  }

  return NULL;
}


// Configure a single minicrate
// Author: A. Parenti, Feb01 2006
// Modified: AP, Feb14 2007 (added pthread_setcancelstate)
// Modified: AP, Jul21 2007 (added DAC calibration)
// Modified: AP, Sep14 2007 (Revised DAC calibration)
// Modified: AP, Oct23 2007 (More significant error messages)
// Modified: AP, Oct29 2007 (Read from DB list of defective components)
// Modified: AP, Oct31 2007 (Update status only for configured components)
void *configure_a_mc(void *ccb) {
#ifdef __USE_CCBDB__ // DB access enabled
  Sdtccb *lccb = (Sdtccb*)ccb;
  char tmpstr[128];
  short conf_err, conf_status, conf_crc;
  float DacCalibs[14];

  ap_testcancel(lccb->conf_tid); // Test for pending cancellations

// Disable premature thread cancellation (would bring to hangup)
  pthread_setcancelstate(PTHREAD_CANCEL_DISABLE,NULL);

  Cconf myConf(lccb->DTconfname,lccb->dtdbobj); // Open configuration
  Cccb myCcb(lccb->ccbcmd); // "ccb" object
  Cttcrx myTtcrx(lccb->ccbcmd);
  Cfe myFe(lccb->ccbcmd);

  myConf.read_confmask(lccb->ccbid); // Read "defective" component list

// Enable thread cancellation
  pthread_setcancelstate(PTHREAD_CANCEL_ENABLE,NULL);
  lccb->conf_tready=true; // Thread is ready

  lccb->confccb=&myCcb;

// Set verbosity level
  myConf.verbose_mode(lccb->_verbose_);
  myCcb.verbose_mode(lccb->_verbose_);

  lccb->confmsg = ""; // Reset messages

// DAC calibration
  myTtcrx.get_TTCRX_id(); // Get TTCid
  if (myTtcrx.ttcrx_id.read_err!=0) {
    DacCalibs[0]=DacCalibs[1]=DacCalibs[2]=DacCalibs[3]=DacCalibs[4]=DacCalibs[5]=DacCalibs[6]=1.0;
    DacCalibs[7]=DacCalibs[8]=DacCalibs[9]=DacCalibs[10]=DacCalibs[11]=DacCalibs[12]=DacCalibs[13]=0.0;
    lccb->confmsg += "TTCRX_id not read. Using standard DAC calib.\n";
  } else {

    daccalib myDacCalib(lccb->dtdbobj);
    if (myDacCalib.retrievedaccalib(myTtcrx.ttcrx_id.id,DacCalibs)!=0) {
      DacCalibs[0]=DacCalibs[1]=DacCalibs[2]=DacCalibs[3]=DacCalibs[4]=DacCalibs[5]=DacCalibs[6]=1.0;
      DacCalibs[7]=DacCalibs[8]=DacCalibs[9]=DacCalibs[10]=DacCalibs[11]=DacCalibs[12]=DacCalibs[13]=0.0;
    lccb->confmsg += "No DAC calib found in DB. Using standard DAC calib.\n";
    }
  }

// Thr offsets are in mV in the database, but 0x6B command wants them in Volts
  DacCalibs[11]/=1000;
  DacCalibs[12]/=1000;
  DacCalibs[13]/=1000;

  myFe.set_calib_DAC(DacCalibs+1,DacCalibs+4,*DacCalibs,DacCalibs+8,DacCalibs+11,*(DacCalibs+7));
  if (myFe.set_calib_dac.ccbserver_code==0)
    lccb->confmsg += "DAC calibrated!\n";
  else
    lccb->confmsg += "DAC calibration error!\n";

// Save old config status and errors
  lccb->LastConfSt=lccb->ConfSt;
  lccb->LastConfEr=lccb->ConfEr;

// config ALL
  if (lccb->conf_tid==0) { // Thread has ben cancelled somewhere
    printf("Quitting configuration CCB=%d\n",lccb->ccbid);
    lccb->confmsg += "Configuration aborted.\n";
    pthread_exit(NULL);
  }
  lccb->cfgall = c_configuring; // Set flag to "configuring"
  lccb->confmsg += "Configuring minicrate...";
  conf_err = myCcb.config_mc(&myConf,CALL,&conf_status,&conf_crc);
  lccb->confmsg += " done!\n";

// Save configuration time, status, errors
  lccb->cfg_time = time(NULL);
  lccb->ConfSt   = conf_status;
  lccb->ConfEr   = conf_err;

// Save CRC
  lccb->confcrc = conf_crc; // CRC of configuration
  lccb->lastcrc = conf_crc; // Last read CRC
  sprintf(tmpstr,"CRC: %#hX\n",lccb->confcrc);
  lccb->confmsg += tmpstr;

// BTI config result: BIT0
  if ((conf_err&0x1)!=0) {
    lccb->cfgbti=c_error;
    sprintf(tmpstr,"BTI: %d errors\n",myCcb.BTI_bad);
    lccb->confmsg += tmpstr;
  }
  else if ((conf_status&0x1)==0) { // configured!
    lccb->cfgbti=c_configured;
    lccb->confmsg += "BTI configured\n";
  } else { // Nothing done: keep old status and error
    lccb->ConfSt = (lccb->ConfSt&0XFFFE)+(lccb->LastConfSt&0X0001);
    lccb->ConfEr = (lccb->ConfEr&0XFFFE)+(lccb->LastConfEr&0X0001);
    lccb->confmsg += "BTI: nothing done\n";
  }


// TRACO config result: BIT1
  if (((conf_err>>1)&0x1)!=0) {
    lccb->cfgtraco=c_error;
    sprintf(tmpstr,"TRACO: %d errors\n",myCcb.TRACO_bad);
    lccb->confmsg += tmpstr;
  }
  else if (((conf_status>>1)&0x1)==0) { // configured!
    lccb->cfgtraco=c_configured;
    lccb->confmsg += "TRACO configured\n";
  } else { // Nothing done: keep old status and error
    lccb->ConfSt = (lccb->ConfSt&0XFFFD)+(lccb->LastConfSt&0X0002);
    lccb->ConfEr = (lccb->ConfEr&0XFFFD)+(lccb->LastConfEr&0X0002);
    lccb->confmsg += "TRACO: nothing done\n";
  }


// LUT config result: BIT2
  if (((conf_err>>2)&0x1)!=0) {
    lccb->cfglut=c_error;
    sprintf(tmpstr,"LUT: %d errors\n",myCcb.LUT_bad);
    lccb->confmsg += tmpstr;
  }
  else if (((conf_status>>2)&0x1)==0) { // configured!
    lccb->cfglut=c_configured;
    lccb->confmsg += "LUT configured\n";
  } else { // Nothing done: keep old status and error
    lccb->ConfSt = (lccb->ConfSt&0XFFFB)+(lccb->LastConfSt&0X0004);
    lccb->ConfEr = (lccb->ConfEr&0XFFFB)+(lccb->LastConfEr&0X0004);
    lccb->confmsg += "LUT: nothing done\n";
  }


// TSS config result: BIT3
  if (((conf_err>>3)&0x1)!=0) {
    lccb->cfgtss=c_error;
    sprintf(tmpstr,"TSS: %d errors\n",myCcb.TSS_bad);
    lccb->confmsg += tmpstr;
  }
  else if (((conf_status>>3)&0x1)==0) { // configured!
    lccb->cfgtss=c_configured;
    lccb->confmsg += "TSS configured\n";
  } else { // Nothing done: keep old status and error
    lccb->ConfSt = (lccb->ConfSt&0XFFF7)+(lccb->LastConfSt&0X0008);
    lccb->ConfEr = (lccb->ConfEr&0XFFF7)+(lccb->LastConfEr&0X0008);
    lccb->confmsg += "TSS: nothing done\n";
  }


// TSM config result: BIT4
  if (((conf_err>>4)&0x1)!=0) {
    lccb->cfgtsm=c_error;
    sprintf(tmpstr,"TSM: %d errors\n",myCcb.TSM_bad);
    lccb->confmsg += tmpstr;
  }
  else if (((conf_status>>4)&0x1)==0) { // configured!
    lccb->cfgtsm=c_configured;
    lccb->confmsg += "TSM configured\n";
  } else { // Nothing done: keep old status and error
    lccb->ConfSt = (lccb->ConfSt&0XFFEF)+(lccb->LastConfSt&0X0010);
    lccb->ConfEr = (lccb->ConfEr&0XFFEF)+(lccb->LastConfEr&0X0010);
    lccb->confmsg += "TSM: nothing done\n";
  }


// TDC config result: BIT5
  if (((conf_err>>5)&0x1)!=0) {
    lccb->cfgtdc=c_error;
    sprintf(tmpstr,"TDC: %d errors\n",myCcb.TDC_bad);
    lccb->confmsg += tmpstr;
  }
  else if (((conf_status>>5)&0x1)==0) { // configured!
    lccb->cfgtdc=c_configured;
    lccb->confmsg += "TDC configured\n";
  } else { // Nothing done: keep old status and error
    lccb->ConfSt = (lccb->ConfSt&0XFFDF)+(lccb->LastConfSt&0X0020);
    lccb->ConfEr = (lccb->ConfEr&0XFFDF)+(lccb->LastConfEr&0X0020);
    lccb->confmsg += "TDC: nothing done\n";
  }


// ROB config result: BIT6
  if (((conf_err>>6)&0x1)!=0) {
    lccb->cfgrob=c_error;
    sprintf(tmpstr,"ROB: %d errors\n",myCcb.ROB_bad);
    lccb->confmsg += tmpstr;
  }
  else if (((conf_status>>6)&0x1)==0) { // configured!
    lccb->cfgrob=c_configured;
    lccb->confmsg += "ROB configured\n";
  } else { // Nothing done: keep old status and error
    lccb->ConfSt = (lccb->ConfSt&0XFFBF)+(lccb->LastConfSt&0X0040);
    lccb->ConfEr = (lccb->ConfEr&0XFFBF)+(lccb->LastConfEr&0X0040);
//    lccb->confmsg += "ROB: nothing done\n";
  }


// FE config result:BIT7
  if (((conf_err>>7)&0x1)!=0) {
    lccb->cfgfe=c_error;
    sprintf(tmpstr,"FE: %d errors\n",myCcb.FE_bad);
    lccb->confmsg += tmpstr;
  }
  else if (((conf_status>>7)&0x1)==0) { // configured!
    lccb->cfgfe=c_configured;
    lccb->confmsg += "FE configured\n";
  } else { // Nothing done: keep old status and error
    lccb->ConfSt = (lccb->ConfSt&0XFF7F)+(lccb->LastConfSt&0X0080);
    lccb->ConfEr = (lccb->ConfEr&0XFF7F)+(lccb->LastConfEr&0X0080);
    lccb->confmsg += "FE: nothing done\n";
  }


// Configuration aborted? Check BIT15
  if ((conf_err&0x8000)!=0) {
    lccb->confmsg += "Configuration aborted because of communication errors\n";
  }

  if (lccb->ConfEr!=0)
    lccb->cfgall=c_error;  // CCB configuration error
  else if ((lccb->ConfSt&0xBF)==0) // takes only 8 LS bits, but ROB (6th) bit
    lccb->cfgall=c_configured; // CCB configured
  else
    lccb->cfgall=c_undef; // undefined state

// Print config result to stdout
  printf("Configured ccb: %d\n", lccb->ccbid);
  switch (lccb->cfgall) {
  case c_configured:
    printf("Result: configuration successful\n");
    break;
  case c_configuring:
    printf("Result: configuring\n");
    break;
  case c_error:
    printf("Result: configuration error\n");
    break;
  default:
    printf("Result: undefined\n");
  }

  printf("Messages:\n%s\n", lccb->confmsg.c_str());

  lccb->conf_tid = 0; // On return reset pthread_id
  lccb->conf_tready=false;
  lccb->confccb=0;
#endif

  return NULL;
}


// Start-of-run operations for a single minicrate
// Author: A. Parenti, Nov08 2006
// Modified: AP, Feb07 2007 (added ROB_reset)
// Modified: AP, Feb14 2007 (added pthread_setcancelstate)
void *setrun_a_mc(void *ccb) {
// Disable premature thread cancellation (would bring to hangup)
  pthread_setcancelstate(PTHREAD_CANCEL_DISABLE,NULL);

  Sdtccb *lccb = (Sdtccb*)ccb;

  Cccb myCcb(lccb->ccbcmd); // "ccb" object
  Cfe  myFe(lccb->ccbcmd); // "FE" object
  Crob myRob(lccb->ccbcmd); // "ROB" object

  pthread_setcancelstate(PTHREAD_CANCEL_ENABLE,NULL); // Enable thread cancellation

// Set verbosity level
  myCcb.verbose_mode(lccb->_verbose_);

  myCcb.run_in_progress(1); // Set "run_in_progress" flag

  //Taken away to avoid ROS errors @ start of run
  //myRob.reset_ROB(); // Reset ROB (0x32)

#ifdef LOGTODB
  myFe.FE_mask_to_db(); // Log FE mask in the DB
#endif

  printf("CCBid=%d, port=%d, set-up for run\n",lccb->ccbid,lccb->ccbport);

  lccb->runset = true; // MC is set for run 

  lccb->setrun_tid = 0; // On return reset pthread_id
  lccb->setrun_tready = false; 
  return NULL;
}

// Stop-run operations for a single minicrate
// Author: A. Parenti, Nov08 2006
// Modified: AP, Feb13 2007 (added pthread_setcancelstate)
void *unsetrun_a_mc(void *ccb) {
// Disable premature thread cancellation (would bring to hangup)
  pthread_setcancelstate(PTHREAD_CANCEL_DISABLE,NULL);

  Sdtccb *lccb = (Sdtccb*)ccb;

  Cccb myCcb(lccb->ccbcmd); // "ccb" object

  pthread_setcancelstate(PTHREAD_CANCEL_ENABLE,NULL); // Enable thread cancellation

// Set verbosity level
  myCcb.verbose_mode(lccb->_verbose_);

  myCcb.run_in_progress(0); // Unset "run_in_progress" flag

  printf("CCBid=%d, port=%d, unset-up for run\n",lccb->ccbid,lccb->ccbport);

  lccb->runset = false; // MC is not set for run 

  lccb->setrun_tid = 0; // On return reset pthread_id
  lccb->setrun_tready = false;
  return NULL;
}



// Send command to a single ccb
// Author: A. Parenti, Nov17 2006
// Modified: AP, Dec13 2006
// Modified: AP, Feb13 2007 (added pthread_setcancelstate)
// Modified: AP, Aug31 2007 (new syntax for MC_check_test)
void *sendcommand_a_mc(void *ccb) {
// Disable premature thread cancellation (would bring to hangup)
  pthread_setcancelstate(PTHREAD_CANCEL_DISABLE,NULL);

  Sdtccb *lccb = (Sdtccb*)ccb;
  Cccb myCcb(lccb->ccbcmd); // "ccb" object
  Crob myRob(lccb->ccbcmd); // "ROB" object

  pthread_setcancelstate(PTHREAD_CANCEL_ENABLE,NULL); // Enable thread cancellation

// Set verbosity level
  myCcb.verbose_mode(lccb->_verbose_);
  myRob.verbose_mode(lccb->_verbose_);

//  myCcb.mc_firmware_version(); // Get firmware version

 lccb->CmdMsg="";

  if (lccb->command_type == cmd_restoreconf) {
    myCcb.load_MC_conf(); // 0x61
    lccb->CmdMsg=myCcb.load_mc_conf.msg;

//    if (myCcb.HVer!=1 || (myCcb.HVer==1 && myCcb.LVer>16)) {
      myCcb.restore_MC_conf(); // 0x96
      lccb->CmdMsg+=myCcb.restore_mc_conf.msg;
//    }

  } else if (lccb->command_type == cmd_saveconf) {
    myCcb.save_MC_conf(); // 0x40
    lccb->CmdMsg=myCcb.save_mc_conf.msg;

  } else if (lccb->command_type == cmd_autosetlink) {
    myCcb.auto_set_LINK(); // 0x74
    lccb->CmdMsg=myCcb.auto_set_link.msg;

  } else if (lccb->command_type == cmd_restartccb) {
    myCcb.restart_CCB(); // 0xF8
    lccb->CmdMsg=myCcb.restart_ccb.msg;

  } else if (lccb->command_type == cmd_resetrob) {
    myRob.reset_ROB(); // 0x32
    lccb->CmdMsg=myRob.reset_rob.msg;

  } else if (lccb->command_type == cmd_mctemperature) {
    myCcb.MC_temp(); // 0x3C
    lccb->CmdMsg=myCcb.mc_temp.msg;

  } else if (lccb->command_type == cmd_mctest) {
    Emcstatus flag_tst, sb_tst, trb_tst, rob_tst;
    char message[2048];
#ifdef __USE_CCBDB__ // DB access enabled
    Cref myRef(lccb->ccbid,lccb->dtdbobj); // Read reference values
#endif

#ifdef __USE_CCBDB__ // DB access enabled
    myCcb.MC_check_test(&myRef,false,&flag_tst,&sb_tst,&trb_tst,&rob_tst,message);
#else
    myCcb.MC_test();
#endif
    lccb->CmdMsg=myCcb.mc_test.msg;
    lccb->CmdMsg+="Messages:\n";
    lccb->CmdMsg+=message;

  } else if (lccb->command_type == cmd_checktdc) {
    char brd_id, chip;
    char tmpstr[100];
    for (brd_id=0;brd_id<7;++brd_id) {
      for (chip=0;chip<4;++chip){

        sprintf(tmpstr,"\nBoard:%d Chip:%d",brd_id,chip);
        myCcb.status_TDC(brd_id,chip); // 0x43
        lccb->CmdMsg+=tmpstr;
        lccb->CmdMsg+=myCcb.status_tdc.msg;
      }
    }

  } else if (lccb->command_type == cmd_readroberror) {
    myRob.read_ROB_error(); // 0x33
    lccb->CmdMsg=myRob.read_rob_error.msg;

  } else if (lccb->command_type == cmd_resetloselock) {
    Cttcrx myttc(lccb->ccbcmd);
    myttc.reset_lose_lock_counter(); // 0x8A
    myttc.reset_lose_lock_counter_print();
    lccb->CmdMsg=myttc.reset_llc.msg;

  } else if (lccb->command_type == cmd_configcrc) {
    myCcb.GetConfigCRC(); // 0xA3
    lccb->lastcrc=myCcb.getconfigcrc.crc;
    lccb->CmdMsg=myCcb.getconfigcrc.msg;
  } else if (lccb->command_type == cmd_mcstatus) {
    Emcstatus mc_phys_st, mc_logic_st;
    char message[1024];
    bool mc_program;
    printf("Trying mc_status\n");
    //lccb->ccbcmd->use_primary_port();
#ifdef __USE_CCBDB__ // DB access enabled
    Cref myRef(lccb->ccbid,lccb->dtdbobj); // Read reference values
    myCcb.MC_check_status(&myRef,false,&mc_program, &mc_phys_st, &mc_logic_st, message);
    myCcb.MC_read_status_print();
    lccb->CmdMsg=myCcb.mc_status_check.msg;
    //lccb->CmdMsg=myCcb.mc_status.msg;
    //lccb->CmdMsg=message;
#else
    lccb->CmdMsg="No DB available. Couldn't check ref values...";
#endif
  }

  printf( "Ccb %d message %s /n", lccb->ccbid ,lccb->CmdMsg.c_str() );
  return NULL;
}


/*****************************************************************************/
// dtdcs CLASS
/*****************************************************************************/

// Class constructor
// Author: A. Parenti, Feb01 2006
// Modified: AP, Jul27 2007
dtdcs::dtdcs() {

  strcpy(partname,""); // Reset partition name
  strcpy(DTconfname,""); // Reset configuration name

  mc_monitor_int=MC_MONITOR_INTERVAL; // Set to default
  pr_monitor_int=PR_MONITOR_INTERVAL; // Set to default
  mc_write_int=MC_TO_DB_INTERVAL; // Set to default
  pr_write_int=PR_TO_DB_INTERVAL; // Set to default

  nccb=0; // Reset ccb number
  _verbose_ = VERBOSE_DEFAULT; // Set verbosity level to default
  auto_recover = AUTOREC_DEFAULT; // Set ccb autorecover to default

#ifdef LOGTODB
  dcslog = new Clog(); // Log to DB
#else
  dcslog = new Clog("dtdcs.log"); // Create logger object (to file)
#endif
}

// Class destructor
// Author: A. Parenti, Feb02 2006
// Modified: AP, May24 2005
dtdcs::~dtdcs() {

  partReset(); // Reset partition
//  delete dcslog; // Delete logger (AP. Commented on May24 2006)
}

// Go to verbose mode (in=true) or not
void dtdcs::verbose_mode(bool in) {
  int i;

  if (this==NULL) return; // Object deleted -> return

  _verbose_ = in;
  for (i=0; i<nccb && i<NMAXCCB; ++i)
    dtccb[i]._verbose_ = in;
}


// Set ccb autorecover on
void dtdcs::enableAutoRecover() {
  int i;

  if (this==NULL) return; // Object deleted -> return

  auto_recover = true;
  for (i=0; i<nccb && i<NMAXCCB; ++i)
    dtccb[i].auto_recover = true;
}

// Set ccb autorecover off
void dtdcs::disableAutoRecover() {
  int i;

  if (this==NULL) return; // Object deleted -> return

  auto_recover = false;
  for (i=0; i<nccb && i<NMAXCCB; ++i)
    dtccb[i].auto_recover = false;
}

// Check connection with DB. Return 0 when ok.
// Author: A.Parenti, Jan19 2007
// Modified/Tested: AP, Apr10 2007
int dtdcs::dbTestConnetion() {
  if (this==NULL) return -1; // Object deleted -> return

#ifdef __USE_CCBDB__ // DB access enabled
  if (SQL_SUCCEEDED(dcsdbobj.connct_rc)) {
    printf("Connection with DB is fine.\n");
    return 0;
  } else {
    printf("Impossible to connect to DB.\n");
    return -1;
  }
#else
  nodbmsg();
  return -1;
#endif
}


//*******************//
// partition methods //
//*******************//

// Initialize a partition (use last selected partition), start monitoring
// Author: A. Parenti, Feb03 2006
// Modified/Tested: AP, Oct31 2007
int dtdcs::partInitialize(bool startmon) {
  if (this==NULL) return -1; // Object deleted -> return

#ifdef __USE_CCBDB__ // DB access enabled
  int i, j, ccblist[NMAXCCB], portlist[NMAXCCB], secportlist[NMAXCCB];
  char serverlist[NMAXCCB][255];
  dtpartition dcspart(&dcsdbobj);
  ccbmap myccbmap(&dcsdbobj);

  if (strlen(partname)==0) { // partname is empty!
    nccb=0;
    return -1;
  }

// Read partition from DB
  dcspart.retrieveccbs(partname,&nccb,ccblist,portlist,secportlist,serverlist);
  if (nccb<=0) {
    nccb=0;
    return -1;
  } else if (nccb>NMAXCCB) {
    printf("Too much ccb's!!!\n");
    printf("Found %d, maximum is %d. Check partition or recompile library with higher NMAXCCB\n",nccb,NMAXCCB);
    nccb=NMAXCCB;
  }

// Copy info in CCB descriptors
  for (i=0;i<nccb && i<NMAXCCB;++i) {

    if (myccbmap.retrieve(ccblist[i])==0) { // retrieve ccb info
      dtccb[i].wheel=myccbmap.wheel;
      dtccb[i].sector=myccbmap.sector;
      dtccb[i].station=myccbmap.station;
    } else {
      dtccb[i].wheel=-999;
      dtccb[i].sector=-999;
      dtccb[i].station=-999;
    }

    dtccb[i].ccbid   = ccblist[i];
    dtccb[i].ccbport = portlist[i];

//// Sectors 1-6: secondary is 1000+ccbid. Sectors 7-12: 2000+ccbid
//// (Not used, secondary port is read from DB)
//    if ((dtccb[i].sector>=1&&dtccb[i].sector<=6)||dtccb[i].sector==13)
//      secportlist[i]=1000+ccblist[i]; // "first" secondary
//    else if ((dtccb[i].sector>=7&&dtccb[i].sector<=12)||dtccb[i].sector==14)
//      secportlist[i]=2000+ccblist[i]; // "second" secondary
//    else
//      secportlist[i]=ccblist[i];

    strcpy(dtccb[i].ccbserver,serverlist[i]);
    strcpy(dtccb[i].DTconfname,""); // reset DT confname
    strcpy(dtccb[i].MCconfname,""); // reset MC confname
    dtccb[i].ccblog = dcslog;
    dtccb[i].dtdbobj= &dcsdbobj; // Set DB connector
    dtccb[i].ccbcmd = new Ccommand(ccblist[i],portlist[i],secportlist[i],dtccb[i].ccbserver,CCBPORT,dcslog);
    dtccb[i].ccbcmd->set_db_connector(&dcsdbobj); // Set DB connector
//    dtccb[i].ccbcmd->set_write_log(true); // Switch on command logger
    dtccb[i]._verbose_ = _verbose_; // Set verbosity level

    dtccb[i].cfgbti = dtccb[i].cfgtraco = dtccb[i].cfglut = dtccb[i].cfgtss = dtccb[i].cfgtsm = dtccb[i].cfgtdc = dtccb[i].cfgrob = dtccb[i].cfgfe = dtccb[i].cfgall = c_undef; // Reset configuration status
    dtccb[i].cfg_time = 0; // Reset configuration time
    dtccb[i].confcrc = dtccb[i].lastcrc = 0; // Reset configuration CRC
    dtccb[i].ConfSt=dtccb[i].LastConfSt=0xFFFF; // Reset conf status
    dtccb[i].ConfEr=dtccb[i].LastConfEr=0; // Reset conf error
    dtccb[i].confmsg=""; // Reset config messages

    dtccb[i].physst = dtccb[i].logicst = s_undef; // Reset minicrate status
    dtccb[i].robst = dtccb[i].padcst = s_undef; // Reset ROB & PADC status
    dtccb[i].switch_off_fe = dtccb[i].switch_off_mc = false; // Reset flags
    strcpy(dtccb[i].ccbStatusMsg,""); // Reset minicrate status messages
    strcpy(dtccb[i].ccbErrorMsg,""); // Reset minicrate error messages
    strcpy(dtccb[i].robStatusMsg,""); // Reset ROB status messages
    strcpy(dtccb[i].robErrorMsg,""); // Reset ROB error messages
    strcpy(dtccb[i].padcStatusMsg,""); // Reset PADC status messages
    strcpy(dtccb[i].padcErrorMsg,""); // Reset PADC error messages
    dtccb[i].MaxTemp = 0.0; // Reset brd temperature
    dtccb[i].MaxTempRob=dtccb[i].MaxTempTrb=dtccb[i].MaxTempExt=dtccb[i].MaxTempFe=0.0;
    for (j=0;j<6;++j)
      dtccb[i].TempExt[j]=0.0;

    dtccb[i].press100_hv = dtccb[i].press100_fe = 0.0; // Reset pressures
    dtccb[i].mc_status_time = dtccb[i].pr_status_time = 0;

    dtccb[i].mc_monitor_int = mc_monitor_int; // Set time interval for minicrate monitoring
    dtccb[i].pr_monitor_int = pr_monitor_int; // Set time interval for pressure monitoring
    dtccb[i].mc_write_int = mc_write_int; // Set time interval for minicrate status logging
    dtccb[i].pr_write_int = pr_write_int; // Set time interval for pressure status logging
    dtccb[i].ForceRefreshStatusMc = false; // Reset to default
    dtccb[i].ForceRefreshStatusPr = false; // Reset to default

    dtccb[i].runset = false; // Reset to default
    dtccb[i].excluded = false; // Reset "excluded" flag
    dtccb[i].auto_recover = auto_recover; // Set to global value

// Reset pthread IDs
    dtccb[i].mc_monitor_tid = dtccb[i].pr_monitor_tid = 0;
    dtccb[i].mc_recover_tid = dtccb[i].conf_tid = 0;
    dtccb[i].setrun_tid = 0;
// Reset tready flag
    dtccb[i].conf_tready = dtccb[i].setrun_tready = false;

// Reset pthread delays
    dtccb[i].mc_monitor_delay = dtccb[i].pr_monitor_delay = 0;
    dtccb[i].mc_recover_delay = 0;
  }

//  printf("%sPartition initialized. Starting monitor.%s\n",BLUBKG,STDBKG);

  if (startmon) partStartMonitor(); // Start monitoring

//  printf("%sMonitor started.%s\n",BLUBKG,STDBKG);

// Log partition name and status
  if (strlen(partname)<PARTNAMESIZE) {
    char partstr[1024];
    sprintf(partstr,"<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n");
    sprintf(partstr,"%s<partition>\n",partstr);
    sprintf(partstr,"%s  <partname>%s</partname>\n",partstr,partname);
    sprintf(partstr,"%s  <status>initialized</status>\n",partstr);
    sprintf(partstr,"%s<partition>\n",partstr);

    dcslog->write_log(partstr,mesg);
  }

  return 0;
#else
  nodbmsg();
  return -1;
#endif
}

// Initialize a partition
// Author: A. Parenti, Jan31 2006
int dtdcs::partInitialize(char *partition) {
  if (this==NULL) return -1; // Object deleted -> return

  strncpy(this->partname,partition,PARTNAMESIZE);

  return partInitialize(true);
}


// Reset a partition
// Author: A. Parenti, Feb03 2006
// Modified/tested: AP, Jun20 2007
void dtdcs::partReset() {
  int i;

  if (this==NULL) return; // Object deleted -> return

  confStop(); // Abort configuration, if ongoing
  partStopMonitor(); // Abort monitoring, if ongoing
  partStopSetRun(); // Abort pre/post-run operations, if ongoing

  if (nccb>0) {
// Log partition name and status
    if (strlen(partname)<PARTNAMESIZE) {
      char partstr[1024];
      sprintf(partstr,"<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n");
      sprintf(partstr,"%s<partition>\n",partstr);
      sprintf(partstr,"%s  <partname>%s</partname>\n",partstr,partname);
      sprintf(partstr,"%s  <status>reset</status>\n",partstr);
      sprintf(partstr,"%s<partition>\n",partstr);

      dcslog->write_log(partstr,mesg);
    }
  }

// Reset what's needed
  for (i=0;i<nccb && i<NMAXCCB;++i) {
    if (dtccb[i].ccbid!=0) // Check that this object exsists
      delete dtccb[i].ccbcmd; // Delete Ccommand object from dtccb[i]
    dtccb[i].ccbcmd=NULL;
    dtccb[i].ccblog=NULL;
    dtccb[i].dtdbobj=NULL;
  }

  nccb=0; // Reset nccb
}

// Return No. of ccb in current partition
// Author: A.Parenti, Jan16 2007
// Modified: AP, Jun18 2007
int dtdcs::partGetNCcb() {
  if (this==NULL) return 0; // Object deleted -> return

  if (nccb>NMAXCCB) { // Something strange happened... re-read nccb
    int i, newnccb;
    for (i=newnccb=0;i<NMAXCCB;++i)
      if (dtccb[i].ccbid!=0) ++newnccb;

    nccb=newnccb;

    printf("nccb>NMAXCCB. Re-read nccb.\n");
  }

  return (nccb<NMAXCCB)?nccb:NMAXCCB;
}

// Return No. of "excluded" ccb in current partition
// Author: A.Parenti, Feb15 2007
int dtdcs::partGetNExcludedCcb() {
  int i, nexcluded;

  if (this==NULL) return 0; // Object deleted -> return

  for (i=nexcluded=0; i<nccb && i<NMAXCCB; ++i) {
    if (dtccb[i].excluded) ++nexcluded;
  }

  return nexcluded;
}

// Return list of ccb in partition
// Author: A.Parenti, Jan19 2007
int* dtdcs::partGetCcbList(){
  int i;
  static int ccblist[NMAXCCB];

  if (this==NULL) return ccblist; // Object deleted -> return

  for (i=0;i<nccb && i<NMAXCCB;++i)
    ccblist[i]=dtccb[i].ccbid;
  for (i=nccb;i<NMAXCCB;++i)
    ccblist[i]=0;

  return ccblist;
}

// Return the name of partition
// Author: A.Parenti, Jan16 2007
char* dtdcs::partGetPartName() {
  if (this==NULL) return ""; // Object deleted -> return

  return partname;
}


// Print the actual partition
// Author: A. Parenti, Feb01 2006
// Modified: AP, Feb14 2007
void dtdcs::partShowActual() {
  int i;

  if (this==NULL) return; // Object deleted -> return

  printf("\n*** Partition Name: %s ***\n",partname);
  printf("  [excluded]  ccbId  ccbPort  wheel  sector  station  ccbServer\n");

  for (i=0;i<nccb && i<NMAXCCB;++i)
    printf("      [%c]     %5d    %5d  %+5d   %5d    %5d  %s\n",dtccb[i].excluded?'x':' ',dtccb[i].ccbid,dtccb[i].ccbport,dtccb[i].wheel,dtccb[i].sector,dtccb[i].station,dtccb[i].ccbserver);
  printf("*** End Partition ***\n\n");

}

// Shows the available partitions
// Author: A. Parenti, Feb03 2006
// Modified/Tested: AP, Apr10 2007
void dtdcs::partShowAvailable() {
  if (this==NULL) return; // Object deleted -> return

#ifdef __USE_CCBDB__ // DB access enabled
  dtpartition dcspart(&dcsdbobj);
  char partlist[NMAXPART][PARTNAMESIZE];
  int i, npart=0;

  dcspart.retrieveparts(&npart,partlist);

  printf("*** Available partitions:\n");
  for (i=0;i<npart;++i)
    printf(" '%s'\n",partlist[i]);
#else
  nodbmsg();
#endif
}


// Monitor the status of the selected partition
// Author: A. Parenti, Feb02 2006
// Modified: AP, Apr02 2007
void dtdcs::partStartMonitor() {
  int i, timewait;;
  int error;

  if (this==NULL) return; // Object deleted -> return

  if (nccb<=0) return;

//  timewait = (int)ceil(1000*dtccb[0].mc_monitor_int/nccb/2);
//  if (timewait>5000)
//    timewait=5000;
//  else if (timewait<1000)
//      timewait=1000;

//  timewait=100;
  timewait=1;

// Set thread attributes (to create detached threads)
  pthread_attr_t thr_attr;
  pthread_attr_init(&thr_attr);
  pthread_attr_setdetachstate(&thr_attr,PTHREAD_CREATE_DETACHED);

  for (i=0;i<nccb && i<NMAXCCB;++i) { // Start monitoring minicrates
    if (dtccb[i].mc_monitor_tid != 0) {
      if (_verbose_)
        printf("Already monitoring status. Ccb:%d -> Return\n",dtccb[i].ccbid);
    } else {
      dtccb[i].mc_monitor_delay = i*timewait; // set delay of thread
      error=pthread_create(&(dtccb[i].mc_monitor_tid),&thr_attr,monitor_a_mc,(void*)(dtccb+i)); // open thread
      if (error!=0) { // Thread not created (too much user processes?)
        printf("Error. Monitoring thread not created. Ccb:%d -> Return\n",dtccb[i].ccbid);
        dtccb[i].mc_monitor_tid=0;
        error=0;
      }
    }
  }

  for (i=0;i<nccb && i<NMAXCCB;++i) { // Start monitoring pressures
    if (dtccb[i].pr_monitor_tid != 0) {
      if (_verbose_)
        printf("Already monitoring pressure. Ccb:%d -> Return\n",dtccb[i].ccbid);
    } else {
      dtccb[i].pr_monitor_delay = (nccb+i)*timewait; // set delay of thread
      error=pthread_create(&(dtccb[i].pr_monitor_tid),&thr_attr,monitor_a_pr,(void*)(dtccb+i)); // open thread
     if (error!=0) { // Thread not created (too much user processes?)
        printf("Error. Pressure monitoring thread not created. Ccb:%d -> Return\n",dtccb[i].ccbid);
        dtccb[i].pr_monitor_tid=0;
        error=0;
      }
    }
  }

  for (i=0;i<nccb && i<NMAXCCB;++i) { // Start recover task
    if (dtccb[i].mc_recover_tid != 0) {
      if (_verbose_)
        printf("Already running recovering task. Ccb:%d -> Return\n",dtccb[i].ccbid);
    } else {
      dtccb[i].mc_recover_delay = (100*nccb+i)*timewait; // set delay of thread
      error=pthread_create(&(dtccb[i].mc_recover_tid),&thr_attr,recovertask_a_mc,(void*)(dtccb+i)); // open thread
      if (error!=0) { // Thread not created (too much user processes?)
        printf("Error (%d). Recovering task thread not created. Ccb:%d -> Return\n",error,dtccb[i].ccbid);
        dtccb[i].mc_recover_tid=0;
        error=0;
      }
    }
  }

  return;
}


// Abort ongoing monitoring
// Author: A. Parenti, May10 2006
// Modified/Tesetd: AP, Oct22 2007
void dtdcs::partStopMonitor() {
  int i, j, errcode;

  if (this==NULL) return; // Object deleted -> return

#ifdef __USE_CCBDB__ // DB access enabled
  ccbmap myccbmap(&dcsdbobj);
#endif

  for (i=0;i<nccb && i<NMAXCCB;++i) {
#ifdef __USE_CCBDB__ // DB access enabled
    myccbmap.setoffline(dtccb[i].ccbid); // Set ccb offline in DB
#endif

// Stop and reset minicrate monitor
    dtccb[i].logicst = dtccb[i].physst = s_undef;
    dtccb[i].robst = s_undef;
    dtccb[i].switch_off_fe = dtccb[i].switch_off_mc = false;
    strcpy(dtccb[i].ccbStatusMsg,"");
    strcpy(dtccb[i].ccbErrorMsg,"");
    strcpy(dtccb[i].robStatusMsg,"");
    strcpy(dtccb[i].robErrorMsg,"");
    dtccb[i].MaxTemp = 0;
    dtccb[i].MaxTempRob=dtccb[i].MaxTempTrb=dtccb[i].MaxTempExt=dtccb[i].MaxTempFe=0.0;
    for (j=0;j<6;++j)
      dtccb[i].TempExt[j]=0.0;
    dtccb[i].mc_status_time = 0;
    if (_verbose_ && dtccb[i].mc_monitor_tid!=0)
      printf("Aborting minicrate monitor: ccbid=%d errcode=%d\n",dtccb[i].ccbid,errcode);
//    pthread_cancel(dtccb[i].mc_monitor_tid); // Try to kill thread
    dtccb[i].mc_monitor_tid = 0; // Reset pthread_id => thread will exit

// Stop and reset pressure monitor
    dtccb[i].padcst = s_undef;
    strcpy(dtccb[i].padcStatusMsg,"");
    strcpy(dtccb[i].padcErrorMsg,"");
    dtccb[i].press100_hv = dtccb[i].press100_fe = 0.0;
    dtccb[i].pr_status_time = 0;
    if (_verbose_ && dtccb[i].pr_monitor_tid!=0)
      printf("Aborted pressure monitor: ccbid=%d errcode=%d\n",dtccb[i].ccbid,errcode);
//    pthread_cancel(dtccb[i].pr_monitor_tid); // Try to kill thread
    dtccb[i].pr_monitor_tid = 0; // Reset pthread_id => thread will exit

// Stop and reset recover task
    dtccb[i].excluded=false;
    dtccb[i].auto_recover=AUTOREC_DEFAULT;
    if (_verbose_ && dtccb[i].mc_recover_tid!=0)
      printf("Aborted recover task: ccbid=%d errcode=%d\n",dtccb[i].ccbid,errcode);
//    pthread_cancel(dtccb[i].mc_recover_tid); // Try to kill thread
    dtccb[i].mc_recover_tid = 0; // Reset pthread_id => thread will exit
  }

  if (nccb>0)
    sleep(5); // Wait for awhile, to be sure threads are closed

  return;
}


// Set minicrate monitoring/logging time interval (seconds)
// Author: A.Parenti, Jul27 2007
void dtdcs::partSetMcMonitorInterval(int monitor_time,int write_time) {
  int i;

  if (this==NULL) return; // Object deleted -> return

  if (monitor_time>MC_MONITOR_MIN_INT) {
    mc_monitor_int = monitor_time;
    for (i=0;i<nccb && i<NMAXCCB;++i)
      dtccb[i].mc_monitor_int = monitor_time;

    if (write_time<=monitor_time)
      write_time=monitor_time;

      mc_write_int = write_time;
      for (i=0;i<nccb && i<NMAXCCB;++i)
        dtccb[i].mc_write_int = write_time;
  }
}


// Set pressure monitoring/logging interval (seconds)
// Author: A.Parenti, Jul27 2007
void dtdcs::partSetPrMonitorInterval(int monitor_time,int write_time) {
  int i;

  if (this==NULL) return; // Object deleted -> return

  if (monitor_time>PR_MONITOR_MIN_INT) {
    pr_monitor_int = monitor_time;
    for (i=0;i<nccb && i<NMAXCCB;++i)
      dtccb[i].pr_monitor_int = monitor_time;

    if (write_time<=monitor_time)
      write_time=monitor_time;

      pr_write_int = write_time;
      for (i=0;i<nccb && i<NMAXCCB;++i)
        dtccb[i].pr_write_int = write_time;
  }
}


// Force the application to update partition status
// Author: A.Parenti, Jul06 2007
void dtdcs::partRefreshStatus() {
  int i;

  if (this==NULL) return; // Object deleted -> return

  for (i=0;i<nccb && i<NMAXCCB;++i) {
    dtccb[i].ForceRefreshStatusMc=true; // Force status update
    dtccb[i].ForceRefreshStatusPr=true; // Force status update
  }

  sleep(5);
}

// Print partition status to stdout
// Author: A.Parenti, Feb14 2007
void dtdcs::partShowStatus() {
  std::string strout;

  if (this==NULL) return; // Object deleted -> return

  partShowStatus(&strout);
  printf("%s\n",strout.c_str());
}

// Print partition status to string
// Author: A.Parenti, May11 2006
// Modified: AP, May11 2007
void dtdcs::partShowStatus(std::string *strout) {
  char tmpstr[256];
  int i;

  if (this==NULL) return; // Object deleted -> return

  *strout  = "\n***  DT partition status ***\n";
  *strout += "  CCB  PHYSST LOGICST ROBST CONFST MAXTEMP sens100_HV sens100_FE [excluded]\n";
  *strout += "                                     (°C)     (bar)      (bar)\n";
  for (i=0;i<nccb && i<NMAXCCB;++i) {
    sprintf(tmpstr,"  %3d     %1d      %1d      %1d      %1d     %5.2f  %+8.6f  %+8.6f     [%c]\n",dtccb[i].ccbid,dtccb[i].physst,dtccb[i].logicst,dtccb[i].robst,dtccb[i].cfgall,dtccb[i].MaxTemp,dtccb[i].press100_hv,dtccb[i].press100_fe,dtccb[i].excluded?'x':' ');
    *strout += tmpstr;
  }
  *strout += "***  End status          ***\n\n";
}


// Return an array w/ MC "physical" status
// Author: A.Parenti, Jan19 2007
Emcstatus* dtdcs::partGetPhysStatus() {
  int i;
  static Emcstatus dtphysst[NMAXCCB];

  if (this==NULL) return dtphysst; // Object deleted -> return

  for (i=0;i<nccb && i<NMAXCCB;++i)
    dtphysst[i]=dtccb[i].physst;
  for (i=nccb;i<NMAXCCB;++i)
    dtphysst[i]=s_undef;

  return dtphysst;
}


// Return an array w/ MC "logical" status
// Author: A.Parenti, Jan19 2007
Emcstatus* dtdcs::partGetLogicStatus() {
  int i;
  static Emcstatus dtlogicst[NMAXCCB];

  if (this==NULL) return dtlogicst; // Object deleted -> return

  for (i=0;i<nccb && i<NMAXCCB;++i)
    dtlogicst[i]=dtccb[i].logicst;
  for (i=nccb;i<NMAXCCB;++i)
    dtlogicst[i]=s_undef;

  return dtlogicst;
}


// Return an array w/ FE "switch off" flags
// Author: A.Parenti, Oct22 2007
bool* dtdcs::partGetSwitchOffFe() {
  int i;
  static bool dtswitchoff[NMAXCCB];

  if (this==NULL) return dtswitchoff; // Object deleted -> return

  for (i=0;i<nccb && i<NMAXCCB;++i)
    dtswitchoff[i]=dtccb[i].switch_off_fe;
  for (i=nccb;i<NMAXCCB;++i)
    dtswitchoff[i]=false;

  return dtswitchoff;
}


// Return an array w/ MC "switch off" flags
// Author: A.Parenti, Oct22 2007
bool* dtdcs::partGetSwitchOffMc() {
  int i;
  static bool dtswitchoff[NMAXCCB];

  if (this==NULL) return dtswitchoff; // Object deleted -> return

  for (i=0;i<nccb && i<NMAXCCB;++i)
    dtswitchoff[i]=dtccb[i].switch_off_mc;
  for (i=nccb;i<NMAXCCB;++i)
    dtswitchoff[i]=false;

  return dtswitchoff;
}


// Return an array w/ ROB status
// Author: A.Parenti, May11 2007
Emcstatus* dtdcs::partGetRobStatus() {
  int i;
  static Emcstatus dtrobst[NMAXCCB];

  if (this==NULL) return dtrobst; // Object deleted -> return

  for (i=0;i<nccb && i<NMAXCCB;++i)
    dtrobst[i]=dtccb[i].robst;
  for (i=nccb;i<NMAXCCB;++i)
    dtrobst[i]=s_undef;

  return dtrobst;
}


// Return an array w/ PADC status
// Author: A.Parenti, Jul25 2007
Emcstatus* dtdcs::partGetPadcStatus() {
  int i;
  static Emcstatus dtpadcst[NMAXCCB];

  if (this==NULL) return dtpadcst; // Object deleted -> return

  for (i=0;i<nccb && i<NMAXCCB;++i)
    dtpadcst[i]=dtccb[i].padcst;
  for (i=nccb;i<NMAXCCB;++i)
    dtpadcst[i]=s_undef;

  return dtpadcst;
}


// Return an array w/ MC "excluded" flag
// Author: A.Parenti, Feb15 2007
bool* dtdcs::partGetExcluded() {
  int i;
  static bool dtexcluded[NMAXCCB];

  if (this==NULL) return dtexcluded; // Object deleted -> return

  for (i=0;i<nccb && i<NMAXCCB;++i)
    dtexcluded[i]=dtccb[i].excluded;
  for (i=nccb;i<NMAXCCB;++i)
    dtexcluded[i]=false;

  return dtexcluded;
}


// Return an array w/ max MC temperatures
// Author: A.Parenti, Jan19 2007
float* dtdcs::partGetMaxTemp() {
  int i;
  static float MaxTemp[NMAXCCB];

  if (this==NULL) return MaxTemp; // Object deleted -> return

  for (i=0;i<nccb && i<NMAXCCB;++i)
    MaxTemp[i]=dtccb[i].MaxTemp;
  for (i=nccb;i<NMAXCCB;++i)
    MaxTemp[i]=0.0;

  return MaxTemp;
}


// Return an array w/ max ROB temperatures
// Author: A.Parenti, Jan19 2007
float* dtdcs::partGetRobMaxTemp() {
  int i;
  static float RobMaxTemp[NMAXCCB];

  if (this==NULL) return RobMaxTemp; // Object deleted -> return

  for (i=0;i<nccb && i<NMAXCCB;++i)
    RobMaxTemp[i]=dtccb[i].MaxTempRob;
  for (i=nccb;i<NMAXCCB;++i)
    RobMaxTemp[i]=0.0;

  return RobMaxTemp;
}


// Return an array w/ max TRB temperatures
// Author: A.Parenti, Jan19 2007
float* dtdcs::partGetTrbMaxTemp() {
  int i;
  static float TrbMaxTemp[NMAXCCB];

  if (this==NULL) return TrbMaxTemp; // Object deleted -> return

  for (i=0;i<nccb && i<NMAXCCB;++i)
    TrbMaxTemp[i]=dtccb[i].MaxTempTrb;
  for (i=nccb;i<NMAXCCB;++i)
    TrbMaxTemp[i]=0.0;

  return TrbMaxTemp;
}


// Return an array w/ max EXT temperatures
// Author: A.Parenti, Jan19 2007
float* dtdcs::partGetExtMaxTemp() {
  int i;
  static float ExtMaxTemp[NMAXCCB];

  if (this==NULL) return ExtMaxTemp; // Object deleted -> return

  for (i=0;i<nccb && i<NMAXCCB;++i)
    ExtMaxTemp[i]=dtccb[i].MaxTempExt;
  for (i=nccb;i<NMAXCCB;++i)
    ExtMaxTemp[i]=0.0;

  return ExtMaxTemp;
}


// Return an array w/ max FE temperatures
// Author: A.Parenti, Jan19 2007
float* dtdcs::partGetFeMaxTemp() {
  int i;
  static float FeMaxTemp[NMAXCCB];

  if (this==NULL) return FeMaxTemp; // Object deleted -> return

  for (i=0;i<nccb && i<NMAXCCB;++i)
    FeMaxTemp[i]=dtccb[i].MaxTempFe;
  for (i=nccb;i<NMAXCCB;++i)
    FeMaxTemp[i]=0.0;

  return FeMaxTemp;
}


// Return an array w/ gas pressures HV side
// Author: A.Parenti, Jan19 2007
float* dtdcs::partGetHvPressure() {
  int i;
  static float HvPressure[NMAXCCB];

  if (this==NULL) return HvPressure; // Object deleted -> return

  for (i=0;i<nccb && i<NMAXCCB;++i)
    HvPressure[i]=dtccb[i].press100_hv;
  for (i=nccb;i<NMAXCCB;++i)
    HvPressure[i]=0.0;

  return HvPressure;
}


// Return an array w/ gas pressures FE side
// Author: A.Parenti, Jan19 2007
float* dtdcs::partGetFePressure() {
  int i;
  static float FePressure[NMAXCCB];

  if (this==NULL) return FePressure; // Object deleted -> return

  for (i=0;i<nccb && i<NMAXCCB;++i)
    FePressure[i]=dtccb[i].press100_fe;
  for (i=nccb;i<NMAXCCB;++i)
    FePressure[i]=0.0;

  return FePressure;
}


// Perform pre-run operations
// Author: A. Parenti, Nov08 2006
// Tested at PD, Nov10 2006
// Modified: AP, Apr02 2007
void dtdcs::partSetRun(int RunNr) {
  int i, error;

  if (this==NULL) return; // Object deleted -> return

// Set thread attributes (to create detached threads)
  pthread_attr_t thr_attr;
  pthread_attr_init(&thr_attr);
  pthread_attr_setdetachstate(&thr_attr,PTHREAD_CREATE_DETACHED);

  if (nccb<=0) return;

  for (i=0;i<nccb && i<NMAXCCB;++i) {
    if (dtccb[i].setrun_tid!=0) {
      if (_verbose_)
        printf("Already setting/unsetting run, ccb: %d -> Return\n",dtccb[i].ccbid);
    } else if (dtccb[i].excluded) {
      if (_verbose_)
        printf("Attempt of setting run on excluded minicrate. Ccb:%d -> Return\n",dtccb[i].ccbid);
    } else {
      error=pthread_create(&(dtccb[i].setrun_tid),&thr_attr,setrun_a_mc,(void*)(dtccb+i)); // open thread
      if (error!=0) { // Thread not created (too much user processes?)
        printf("Error. Set-up run thread not created. Ccb:%d -> Return\n",dtccb[i].ccbid);
        dtccb[i].setrun_tid=0;
        error=0;
      }
    }
  }

#ifdef __USE_CCBDB__ // DB access enabled
// Associate RunNr to configuration, in the DB
  Cconf myConf(DTconfname,&dcsdbobj);
  myConf.set_run(RunNr);
#endif
}

// Perform post-run operations
// Author: A. Parenti, Nov08 2006
// Tested at PD, Nov10 2006
// Modified: AP, Apr02 2007
void dtdcs::partUnsetRun() {
  int i, error;

  if (this==NULL) return; // Object deleted -> return

// Set thread attributes (to create detached threads)
  pthread_attr_t thr_attr;
  pthread_attr_init(&thr_attr);
  pthread_attr_setdetachstate(&thr_attr,PTHREAD_CREATE_DETACHED);

  for (i=0;i<nccb && i<NMAXCCB;++i) {
    if (dtccb[i].setrun_tid!=0) {
      if (_verbose_)
        printf("Already setting/unsetting run, ccb: %d -> Return\n",dtccb[i].ccbid);
    } else if (dtccb[i].excluded) {
      if (_verbose_)
        printf("Attempt of unsetting run on excluded minicrate. Ccb:%d -> Return\n",dtccb[i].ccbid);
    } else {
      error=pthread_create(&(dtccb[i].setrun_tid),&thr_attr,unsetrun_a_mc,(void*)(dtccb+i)); // open thread
      if (error!=0) { // Thread not created (too much user processes?)
        printf("Error. Unset-up run thread not created. Ccb:%d -> Return\n",dtccb[i].ccbid);
        dtccb[i].setrun_tid=0;
        error=0;
      }
    }
  }
}


// Abort pre/post-run operations
// Author: A. Parenti, Feb14 2007
void dtdcs::partStopSetRun() {
  int i, errcode;

  if (this==NULL) return; // Object deleted -> return

  for (i=0;i<nccb && i<NMAXCCB;++i) {
    dtccb[i].runset=false; // MC is not set for run
    if (dtccb[i].setrun_tid != 0) { // Means that thread is running
      if (dtccb[i].setrun_tready)
        errcode = pthread_cancel(dtccb[i].setrun_tid); // Cancel thread
      dtccb[i].ccbcmd->cmdunlock(); // To avoid accidental locks...
      dtccb[i].setrun_tid = 0; // Reset pthread_id
      dtccb[i].setrun_tready = false;

      if (_verbose_)
        printf("Aborted pre/post-run: ccbid=%d errcode=%d\n",dtccb[i].ccbid,errcode);
    }
  }

}


// Return 'true' if partition is ready to run
// Author: A. Parenti, Nov08 2006
// Modified: AP, Mar02 2007
bool dtdcs::partReadyForRun() {
  int i;

  if (this==NULL) return false; // Object deleted -> return

  for (i=0;i<nccb && i<NMAXCCB;++i) {
    if (!dtccb[i].excluded) { // minicrate not excluded
      if (dtccb[i].cfgall!=c_configured) return false; // Not configured!
      if (!dtccb[i].runset) return false; // Not ready for run!
    }
  }

  return true;
}


// Send commands to the partition
// Author: A. Parenti, Nov17 2006
// Modified: AP, Apr02 2007
void dtdcs::partSendCommand(Ecommand cmdtype) {
  int i, error;
  pthread_t tid[NMAXCCB];

  if (this==NULL) return; // Object deleted -> return

  for (i=0;i<nccb && i<NMAXCCB;++i) {
    if (dtccb[i].excluded) {
      tid[i]=0;
      if (_verbose_)
        printf("Attempt of sending command on excluded minicrate. Ccb:%d -> Return\n",dtccb[i].ccbid);
    } else {
      dtccb[i].command_type = cmdtype;
      error=pthread_create(&(tid[i]),NULL,sendcommand_a_mc,(void*)(dtccb+i)); // open thread
      if (error!=0) { // Thread not created (too much user processes?)
        printf("Error. Send-command thread not created. Ccb:%d -> Return\n",dtccb[i].ccbid);
        tid[i]=0;
        error=0;
      }
    }
  }

  for (i=0;i<nccb && i<NMAXCCB;++i)
    if (tid[i]!=0) {
      pthread_join(tid[i],NULL);
    }
}

//***********************//
// Configuration methods //
//***********************//

// Shows the available configurations
// Author: A. Parenti, May02 2006
// Modified/Tested: AP, Apr10 2007
void dtdcs::confShowAvailable() {
  if (this==NULL) return; // Object deleted -> return

#ifdef __USE_CCBDB__ // DB access enabled
  ccbconfdb dcsconf(&dcsdbobj);
  char conflist[NMAXCONF][CONFNAMESIZE];
  int i, nconf=0;

  dcsconf.retrieveconfs(&nconf,conflist);

  printf("*** Available configurations:\n");
  for (i=0;i<nconf;++i)
    printf(" '%s'\n",conflist[i]);

#else
  nodbmsg();
#endif
}

// Return current DT confname (configsets.name)
// Author: A.Parenti, Jan16 2007
char* dtdcs::confGetConfName() {
  if (this==NULL) return ""; // Object deleted -> return

  return DTconfname;
}

// Configure a partition
// Author: A. Parenti, Feb01 2006
// Modified: AP, Apr03 2008
void dtdcs::confSelect(char *cname) {
  int i, error;

  if (this==NULL) return; // Object deleted -> return

#ifdef __USE_CCBDB__ // DB access enabled
  ccbconfdb dcsconf(&dcsdbobj); // Datasource from /etc/ccbdb.conf
  dcsconf.confselect(cname);
#endif

// Set thread attributes (to create detached threads)
  pthread_attr_t thr_attr;
  pthread_attr_init(&thr_attr);
  pthread_attr_setdetachstate(&thr_attr,PTHREAD_CREATE_DETACHED);

  strcpy(DTconfname,cname);

// Log partition+configuration name
  if (strlen(partname)<PARTNAMESIZE && strlen(DTconfname)<CONFNAMESIZE) {
    char partstr[1024];
    sprintf(partstr,"<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n");
    sprintf(partstr,"%s<partition>\n",partstr);
    sprintf(partstr,"%s  <partname>%s</partname>\n",partstr,partname);
    sprintf(partstr,"%s  <confname>%s</confname>\n",partstr,DTconfname);
    sprintf(partstr,"%s  <status>assuming configured partition</status>\n",partstr);
    sprintf(partstr,"%s<partition>\n",partstr);

    dcslog->write_log(partstr,mesg);
  }

// Start configuration
  for (i=0;i<nccb && i<NMAXCCB;++i) {
    if (dtccb[i].cfgall==c_configuring) {
      if (_verbose_)
        printf("Already configuring ccb: %d -> Return\n",dtccb[i].ccbid);
    } else if (dtccb[i].excluded) {
      strcpy(dtccb[i].DTconfname,cname); // Set DT confname
      strcpy(dtccb[i].MCconfname,""); // Set MC confname
      dtccb[i].cfgall=c_undef; // Set flag to "undef"
      if (_verbose_)
        printf("Attempt of configuring an excluded minicrate. Ccb:%d -> Return\n",dtccb[i].ccbid);
    } else {
      strcpy(dtccb[i].DTconfname,cname); // Set DT confname
#ifdef __USE_CCBDB__ // DB access enabled
      strcpy(dtccb[i].MCconfname,dcsconf.retrieve_configs_confname(dtccb[i].ccbid)); // Set MC confname
#else
      strcpy(dtccb[i].MCconfname,""); // Set MC confname
#endif
      dtccb[i].cfgall=c_configured; // Set flag to "configuring"
    }
  }
}

// Configure a partition
// Author: A. Parenti, Feb01 2006
// Modified: AP, Apr03 2008
void dtdcs::confStart(char *cname) {
  int i, error;

  if (this==NULL) return; // Object deleted -> return

#ifdef __USE_CCBDB__ // DB access enabled
  ccbconfdb dcsconf(&dcsdbobj); // Datasource from /etc/ccbdb.conf
  dcsconf.confselect(cname);
#endif

// Set thread attributes (to create detached threads)
  pthread_attr_t thr_attr;
  pthread_attr_init(&thr_attr);
  pthread_attr_setdetachstate(&thr_attr,PTHREAD_CREATE_DETACHED);

  strcpy(DTconfname,cname);

// Log partition+configuration name
  if (strlen(partname)<PARTNAMESIZE && strlen(DTconfname)<CONFNAMESIZE) {
    char partstr[1024];
    sprintf(partstr,"<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n");
    sprintf(partstr,"%s<partition>\n",partstr);
    sprintf(partstr,"%s  <partname>%s</partname>\n",partstr,partname);
    sprintf(partstr,"%s  <confname>%s</confname>\n",partstr,DTconfname);
    sprintf(partstr,"%s  <status>configuring</status>\n",partstr);
    sprintf(partstr,"%s<partition>\n",partstr);

    dcslog->write_log(partstr,mesg);
  }

// Start configuration
  for (i=0;i<nccb && i<NMAXCCB;++i) {
    if (dtccb[i].cfgall==c_configuring) {
      if (_verbose_)
        printf("Already configuring ccb: %d -> Return\n",dtccb[i].ccbid);
    } else if (dtccb[i].excluded) {
      strcpy(dtccb[i].DTconfname,cname); // Set DT confname
      strcpy(dtccb[i].MCconfname,""); // Set MC confname
      dtccb[i].cfgall=c_undef; // Set flag to "undef"
      if (_verbose_)
        printf("Attempt of configuring an excluded minicrate. Ccb:%d -> Return\n",dtccb[i].ccbid);
    } else {
      strcpy(dtccb[i].DTconfname,cname); // Set DT confname
#ifdef __USE_CCBDB__ // DB access enabled
      strcpy(dtccb[i].MCconfname,dcsconf.retrieve_configs_confname(dtccb[i].ccbid)); // Set MC confname
#else
      strcpy(dtccb[i].MCconfname,""); // Set MC confname
#endif
      dtccb[i].cfgall=c_configuring; // Set flag to "configuring"
      error=pthread_create(&(dtccb[i].conf_tid),&thr_attr,configure_a_mc,(void*)(dtccb+i)); // open thread
      if (error!=0) { // Thread not created (too much user processes?)
        printf("Error (%d). Configuring thread not created. Ccb:%d -> Return\n",error,dtccb[i].ccbid);
        dtccb[i].conf_tid=0;
        dtccb[i].cfgall=c_error; // Otherwise dtdcs stuck in "configuring"!
        error=0;
      }
    }
  }
}


// Abort ongoing configuration
// Author: A. Parenti, May10 2006
// Modified: AP, Jun20 2007
void dtdcs::confStop() {
  int i, errcode;

  if (this==NULL) return; // Object deleted -> return

  if (strlen(partname)<PARTNAMESIZE && strlen(DTconfname)<CONFNAMESIZE && strlen(DTconfname)>0) {
// Log partition+configuration name
    char partstr[1024];
    sprintf(partstr,"<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n");
    sprintf(partstr,"%s<partition>\n",partstr);
    sprintf(partstr,"%s  <partname>%s</partname>\n",partstr,partname);
    sprintf(partstr,"%s  <confname>%s</confname>\n",partstr,DTconfname);
    sprintf(partstr,"%s  <status>configuration aborted</status>\n",partstr);
    sprintf(partstr,"%s<partition>\n",partstr);

    dcslog->write_log(partstr,mesg);
  }

// Reset DT confname (configsets.name)
  strcpy(DTconfname,"");

// Stop threads

  for (i=0;i<nccb && i<NMAXCCB;++i) {
    dtccb[i].cfgbti = dtccb[i].cfgtraco = dtccb[i].cfglut = dtccb[i].cfgtss = dtccb[i].cfgtsm = dtccb[i].cfgtdc = dtccb[i].cfgrob = dtccb[i].cfgfe = dtccb[i].cfgall = c_undef; // Reset configuration status
    dtccb[i].confmsg=""; // Reset configuration messages
    if (dtccb[i].conf_tid != 0) { // Means that thread is running
      printf("%d. Killing thread %d, ccbid %d\n",i,(int)dtccb[i].conf_tid,dtccb[i].ccbid);
      if (dtccb[i].conf_tready)
        errcode = pthread_cancel(dtccb[i].conf_tid); // Cancel thread
      dtccb[i].ccbcmd->cmdunlock(); // To avoid accidental locks...
      dtccb[i].conf_tid = 0; // Reset pthread_id
      dtccb[i].conf_tready = false;

      if (_verbose_)
        printf("Aborted config: ccbid=%d errcode=%d\n",dtccb[i].ccbid,errcode);
    }
  }
}


// Return configuration status: undef,...
// Author: A. Parenti, May05 2006
// Modified: AP, Feb15 2007
Econfstatus dtdcs::confGetStatus() {
  int i;

  if (this==NULL) return c_undef; // Object deleted -> return

  if (nccb==0) return c_undef;

  for (i=0;i<nccb && i<NMAXCCB;++i) {
    if (!dtccb[i].excluded && dtccb[i].cfgall==c_configuring)
      return c_configuring; // Still configuring
  }

  for (i=0;i<nccb && i<NMAXCCB;++i) {
    if (!dtccb[i].excluded && dtccb[i].cfgall == c_error)
      return c_error; // Configuration error
  }

  for (i=0;i<nccb && i<NMAXCCB;++i) {
    if (!dtccb[i].excluded && dtccb[i].cfgall == c_undef)
      return c_undef; // Status undefined
  }

  return c_configured; // All configured and ok
}


// Return an array w/ MC configuration status
// Author: A.Parenti, Sep10 2007
Econfstatus* dtdcs::confGetDetailedStatus() {
  int i;
  static Econfstatus dtconfst[NMAXCCB];

  if (this==NULL) return dtconfst; // Object deleted -> return

  for (i=0;i<nccb && i<NMAXCCB;++i)
    dtconfst[i]=dtccb[i].cfgall;

  return dtconfst;
}



// Return 'true' if configuration CRC is correct
// Author: A.Parenti, Dec13 2006
// Modified: AP, Feb15 2007
bool dtdcs::confCheckCrc() {
  int i;

  if (this==NULL) return false; // Object deleted -> return

  partSendCommand(cmd_configcrc); // Read actual CRC

  for (i=0;i<nccb && i<NMAXCCB;++i) {
    if (!dtccb[i].excluded && dtccb[i].lastcrc!=dtccb[i].confcrc)
      return false; // Wrong CRC!
  }

  return true;
}


//********************//
// Single CCB methods //
//********************//

// Return the index of the given ccbid
// Author: A.Parenti, Jan15 2007 (Tested)
int dtdcs::idxFromCcbId(int ccbid) {
  int i;

  if (this==NULL) return -1; // Object deleted -> return

  for (i=0;i<nccb && i<NMAXCCB;++i) {
    if (dtccb[i].ccbid==ccbid) {
      return i;
    }
  }

  return -1; // ccb not in partition
}


// Return ccbid
// Author: A.Parenti, Jan16 2007 (Tested)
int dtdcs::ccbGetCcbId(int wheel, int sector, int station) {
  int i;

  if (this==NULL) return 0; // Object deleted -> return

  for (i=0;i<nccb && i<NMAXCCB;++i)
    if (dtccb[i].wheel==wheel && dtccb[i].sector==sector &&
        dtccb[i].station==station)
      return dtccb[i].ccbid;

  return 0; // ccb not in partition
}

// Return wheel, sector, station
// Author: A.Parenti, Jan16 2007 (Tested)
void dtdcs::ccbGetWhSecSt(int ccbid, int *wheel, int *sector, int *station) {
  if (this==NULL) return; // Object deleted -> return

  int i = idxFromCcbId(ccbid);

  if (i>=0) {
    *wheel = dtccb[i].wheel;
    *sector = dtccb[i].sector;
    *station = dtccb[i].station;
  } else {  // ccb not in partition
    *wheel = -999;
    *sector = -999;
    *station = -999;
  }
}

// Check connection w/ ccbserver. Return 0 when ok.
// Author: A.Parenti, Jan23 2007 (Tested)
int dtdcs::ccbserverTestConnection(int ccbid) {
  bool connected;

  if (this==NULL) return -1; // Object deleted -> return

  int i = idxFromCcbId(ccbid);

  if (i>=0) {
    connected = dtccb[i].ccbcmd->connect();
    if (connected)
      return 0;
    else
      return -1;
  } else // ccb not in partition
    return 1;
}


// Check connection w/ ccb. Return 0 when ok.
// Author: A.Parenti, Jan23 2007 (Tested)
int dtdcs::ccbTestConnection(int ccbid) {
  if (this==NULL) return -1; // Object deleted -> return

  int i = idxFromCcbId(ccbid);

  if (i>=0) {
    Cccb myCcb(dtccb[i].ccbcmd); // "ccb" object

    myCcb.MC_read_status();
    if (myCcb.mc_status.ccbserver_code==-103 || // ccb timeout
        myCcb.mc_status.ccbserver_code==-4) // server unreacheable 
      return -1;
    else // Got some reply from ccb
      return 0;
  } else // ccb not in partition
    return 1;
}


// Return minicrate "physical" status
// Author: A.Parenti, Jan16 2007
Emcstatus dtdcs::ccbGetPhysStatus(int ccbid) {
  if (this==NULL) return s_undef; // Object deleted -> return

  int i = idxFromCcbId(ccbid);

  if (i>=0)
    return dtccb[i].physst;
  else
    return s_undef;
}


// Return minicrate "logical" status
// Author: A.Parenti, Jan16 2007
Emcstatus dtdcs::ccbGetLogicStatus(int ccbid) {
  if (this==NULL) return s_undef; // Object deleted -> return

  int i = idxFromCcbId(ccbid);

  if (i>=0)
    return dtccb[i].logicst;
  else
    return s_undef;
}


// Return FE "switch off" flag
// Author: A.Parenti, Oct22 2007
bool dtdcs::ccbGetSwitchOffFe(int ccbid) {
  if (this==NULL) return false; // Object deleted -> return

  int i = idxFromCcbId(ccbid);

  if (i>=0)
    return dtccb[i].switch_off_fe;
  else
    return false;
}


// Return MC "switch off" flag
// Author: A.Parenti, Oct22 2007
bool dtdcs::ccbGetSwitchOffMc(int ccbid) {
  if (this==NULL) return false; // Object deleted -> return

  int i = idxFromCcbId(ccbid);

  if (i>=0)
    return dtccb[i].switch_off_mc;
  else
    return false;
}


// Return ROB status
// Author: A.Parenti, May11 2007
Emcstatus dtdcs::ccbGetRobStatus(int ccbid) {
  if (this==NULL) return s_undef; // Object deleted -> return

  int i = idxFromCcbId(ccbid);

  if (i>=0)
    return dtccb[i].robst;
  else
    return s_undef;
}


// Return PADC status
// Author: A.Parenti, Jul25 2007
Emcstatus dtdcs::ccbGetPadcStatus(int ccbid) {
  if (this==NULL) return s_undef; // Object deleted -> return

  int i = idxFromCcbId(ccbid);

  if (i>=0)
    return dtccb[i].padcst;
  else
    return s_undef;
}


// Force the update of ccb status
// Author: A.Parenti, Oct09 2007
void dtdcs::ccbRefreshStatus(int ccbid) {
  if (this==NULL) return; // Object deleted -> return

  int i = idxFromCcbId(ccbid);

  if (i>0) {
    dtccb[i].ForceRefreshStatusMc=true; // Force status update
    dtccb[i].ForceRefreshStatusPr=true; // Force status update

    sleep(5); // Wait for update
  }
}


// Return minicrate "excluded" flag
// Author: A.Parenti, Feb15 2007
bool dtdcs::ccbGetExcluded(int ccbid) {
  if (this==NULL) return false; // Object deleted -> return

  int i = idxFromCcbId(ccbid);

  if (i>=0)
    return dtccb[i].excluded;
  else
    return false;
}


// Return CCB status string
// Author: A.Parenti, Jan16 2007
char* dtdcs::ccbGetStatusMsg(int ccbid) {
  if (this==NULL) return ""; // Object deleted -> return

  int i = idxFromCcbId(ccbid);

  if (i>=0)
    return dtccb[i].ccbStatusMsg;
  else
    return "";
}


// Return CCB errors string
// Author: A.Parenti, Sep27 2007
char* dtdcs::ccbGetErrorMsg(int ccbid) {
  if (this==NULL) return ""; // Object deleted -> return

  int i = idxFromCcbId(ccbid);

  if (i>=0)
    return dtccb[i].ccbErrorMsg;
  else
    return "";
}


// Return ROB status string
// Author: A.Parenti, May11 2007
char* dtdcs::ccbGetRobStatusMsg(int ccbid) {
  if (this==NULL) return ""; // Object deleted -> return

  int i = idxFromCcbId(ccbid);

  if (i>=0)
    return dtccb[i].robStatusMsg;
  else
    return ""; // ccb not in partition
}


// Return ROB error string
// Author: A.Parenti, Oct08 2007
char* dtdcs::ccbGetRobErrorMsg(int ccbid) {
  if (this==NULL) return ""; // Object deleted -> return

  int i = idxFromCcbId(ccbid);

  if (i>=0)
    return dtccb[i].robErrorMsg;
  else
    return ""; // ccb not in partition
}


// Return PADC status string
// Author: A.Parenti, Jul25 2007
char* dtdcs::ccbGetPadcStatusMsg(int ccbid) {
  if (this==NULL) return ""; // Object deleted -> return

  int i = idxFromCcbId(ccbid);

  if (i>=0)
    return dtccb[i].padcStatusMsg;
  else
    return ""; // ccb not in partition
}


// Return PADC error string
// Author: A.Parenti, Oct08 2007
char* dtdcs::ccbGetPadcErrorMsg(int ccbid) {
  if (this==NULL) return ""; // Object deleted -> return

  int i = idxFromCcbId(ccbid);

  if (i>=0)
    return dtccb[i].padcErrorMsg;
  else
    return ""; // ccb not in partition
}


// Return configuration messages
// Author: A.Parenti, Sep10 2007
char* dtdcs::ccbGetConfStatusMsg(int ccbid) {
  if (this==NULL) return ""; // Object deleted -> return

  int i = idxFromCcbId(ccbid);

  if (i>=0)
    return (char*)(dtccb[i].confmsg.c_str());
  else
    return ""; // ccb not in partition
}


// Return time of last update
// Author: A.Parenti, Jan19 2007
time_t dtdcs::ccbGetStatusTime(int ccbid) {
  if (this==NULL) return (time_t)0; // Object deleted -> return

  int i = idxFromCcbId(ccbid);

  if (i>=0) {
    printf ("CCBID: %d Last status update: %s\n",ccbid,ctime(&dtccb[i].mc_status_time));
    return dtccb[i].mc_status_time;
  }
  else {
    printf ("CCBID: %d not found in partition.\n",ccbid);
      return (time_t)0;// ccb not in partition
  }
}

// Return max minicrate temperature
// Author: A.Parenti, Jan16 2007
float dtdcs::ccbGetMaxTemp(int ccbid) {
  if (this==NULL) return 0.0; // Object deleted -> return

  int i = idxFromCcbId(ccbid);

  if (i>=0)
    return dtccb[i].MaxTemp;
  else
    return 0.0; // ccb not in partition
}


// Return max ROB temperature
// Author: A.Parenti, Jan16 2007
float dtdcs::ccbGetRobMaxTemp(int ccbid) {
  if (this==NULL) return 0.0; // Object deleted -> return

  int i = idxFromCcbId(ccbid);

  if (i>=0)
    return dtccb[i].MaxTempRob;
  else
    return 0.0; // ccb not in partition
}

// Return max TRB temperature
// Author: A.Parenti, Jan16 2007
float dtdcs::ccbGetTrbMaxTemp(int ccbid) {
  if (this==NULL) return 0.0; // Object deleted -> return

  int i = idxFromCcbId(ccbid);

  if (i>=0)
    return dtccb[i].MaxTempTrb;
  else
    return 0.0; // ccb not in partition
}

// Return max EXT temperature
// Author: A.Parenti, Jan16 2007
float dtdcs::ccbGetExtMaxTemp(int ccbid) {
  if (this==NULL) return 0.0; // Object deleted -> return

  int i = idxFromCcbId(ccbid);

  if (i>=0)
    return dtccb[i].MaxTempExt;
  else
    return 0.0; // ccb not in partition
}

// Return max FE temperature
// Author: A.Parenti, Jan16 2007
float dtdcs::ccbGetFeMaxTemp(int ccbid) {
  if (this==NULL) return 0.0; // Object deleted -> return

  int i = idxFromCcbId(ccbid);

  if (i>=0)
    return dtccb[i].MaxTempFe;
  else
    return 0.0; // ccb not in partition
}


// Return all EXT temperatures
// Author: A.Parenti, Jul03 2007
float* dtdcs::ccbGetExtTemp(int ccbid) {
  static float ExtTemp[6];

  if (this==NULL) return ExtTemp; // Object deleted -> return

  int i=idxFromCcbId(ccbid), j;

  if (i>=0)
    for (j=0;j<6;++j)
      ExtTemp[j]=dtccb[i].TempExt[j];
  else
    vector_reset(ExtTemp,7); // ccb not in partition

  return ExtTemp;
}


// Return gas pressure, HV side
// Author: A.Parenti, Jan16 2007
float dtdcs::ccbGetHvPressure(int ccbid) {
  if (this==NULL) return 0.0; // Object deleted -> return

  int i = idxFromCcbId(ccbid);

  if (i>=0)
    return dtccb[i].press100_hv;
  else
    return 0.0; // ccb not in partition
}

// Return gas pressure, FE side
// Author: A.Parenti, Jan16 2007
float dtdcs::ccbGetFePressure(int ccbid) {
  if (this==NULL) return 0.0; // Object deleted -> return

  int i = idxFromCcbId(ccbid);

  if (i>=0)
    return dtccb[i].press100_fe;
  else
    return 0.0; // ccb not in partition
}

// Return time of last update
// Author: A.Parenti, Jan19 2007
time_t dtdcs::ccbGetPressureTime(int ccbid) {
  if (this==NULL) return (time_t)0; // Object deleted -> return

  int i = idxFromCcbId(ccbid);

  if (i>=0) {
    printf ("CCBID: %d Last pressure update: %s\n",ccbid,ctime(&dtccb[i].pr_status_time));
    return dtccb[i].pr_status_time;
  }
  else {
    printf ("CCBID: %d not found in partition.\n",ccbid);
      return (time_t)0;// ccb not in partition
  }
}

// Return configuration status: undef,...
// Author: A.Parenti, Jan26 2007
Econfstatus dtdcs::ccbGetConfStatus(int ccbid) {
  if (this==NULL) return c_undef; // Object deleted -> return

  int i = idxFromCcbId(ccbid);

  if (i>=0)
    return dtccb[i].cfgall;
  else
    return c_undef; // ccb not in partition
}


// Return the name of current configuration (configs.ConfName)
// Author: A.Parenti, Mar14 2007
char* dtdcs::ccbGetConfName(int ccbid) {
  if (this==NULL) return ""; // Object deleted -> return

  int i = idxFromCcbId(ccbid);

  if (i>=0)
    return dtccb[i].MCconfname;
  else
    return ""; // ccb not in partition
}


// Configure again a minicrate
// Author: A. Parenti, Jan23 2007
// Modified: AP, Apr03 2008
void dtdcs::ccbReconfigure(int ccbid) {
  if (this==NULL) return; // Object deleted -> return

  int i = idxFromCcbId(ccbid);
  int error;

// Set thread attributes (to create detached threads)
  pthread_attr_t thr_attr;
  pthread_attr_init(&thr_attr);
  pthread_attr_setdetachstate(&thr_attr,PTHREAD_CREATE_DETACHED);

  if (i>=0) {
    if (dtccb[i].cfgall==c_configuring) {
      if (_verbose_)
        printf("Already configuring ccb: %d -> Return\n",dtccb[i].ccbid);
    } else {
      dtccb[i].cfgall=c_configuring; // Set flag to "configuring"
      error=pthread_create(&(dtccb[i].conf_tid),&thr_attr,configure_a_mc,(void*)(dtccb+i)); // open thread
      if (error!=0) { // Thread not created (too much user processes?)
        printf("Error. Configuring thread not created. Ccb:%d -> Return\n",dtccb[i].ccbid);
        dtccb[i].conf_tid=0;
        dtccb[i].cfgall=c_error; // Otherwise dtdcs stuck in "configuring"!
        error=0;
      }
    }
  }
}


// Send a command to a ccb
// Author: A. Parenti, Dec13 2006
// Modified: AP, Jan16 2007 (Tested)
std::string dtdcs::ccbSendCommand(int ccbid, Ecommand cmdtype) {
  if (this==NULL) return ""; // Object deleted -> return

  int i = idxFromCcbId(ccbid);

  if (i>=0) {
    dtccb[i].command_type = cmdtype;
    sendcommand_a_mc((void*)(dtccb+i));
    return dtccb[i].CmdMsg;
  } else
    return "CCB not in partition"; // ccb not in partition
}

// Return results of the last command
// Author: A.Parenti, Dec13 2006
// Modified: AP, Jan16 2007
std::string dtdcs::ccbGetCommandResult(int ccbid) {
  if (this==NULL) return ""; // Object deleted -> return

  int i = idxFromCcbId(ccbid);

  if (i>=0)
    return dtccb[i].CmdMsg;
  else
    return "CCB not in partition"; // ccb not in partition
}
