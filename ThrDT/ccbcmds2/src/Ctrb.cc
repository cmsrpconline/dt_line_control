#include <ccbcmds/Ctrb.h>
#include <ccbcmds/apfunctions.h>

#include <iostream>
#include <cstdio>

/*****************************************************************************/
// TRB class
/*****************************************************************************/


// Class constructor; arguments: pointer_to_command_object
// Author: A.Parenti, Dec01 2006
Ctrb::Ctrb(Ccommand* ppp) : Ccmn_cmd(ppp) {
  TRB_PI_test_reset(); // Reset trb_pi_test struct
  disable_temp_test_reset(); // Reset trb_temp_test struct
  disable_TRB_CK_reset(); // Reset disable_trb_ck struct
  TRB_PI_test_0xA0_reset(); // Reset trb_pi_test_0xA0 struct
  clear_TRB_SEU_counter_reset(); // Reset clear_trb_seu_counter struct
  test_IR_reset(); // Reset test_ir struct
}

/****************************/
/* Override ROB/TDC methods */
/****************************/

void Ctrb::read_TDC(char tdc_brd, char tdc_chip) {}
int Ctrb::read_TDC_decode(unsigned char *rdstr) {return 0;}
void Ctrb::read_TDC_print() {}

void Ctrb::status_TDC(char tdc_brd, char tdc_chip) {}
void Ctrb::status_TDC_print() {}

void Ctrb::read_ROB_error() {}
int Ctrb::read_ROB_error_decode(unsigned char *rdstr) {return 0;}
void Ctrb::read_ROB_error_print() {}

/********************************/
/* "TRB PI test" (0x5F) command */
/********************************/

// Send "TRB PI test" (0x5F) command and decode reply
// Author: A. Parenti, Jan15 2005
// Note: Tested at Cessy, Oct05 2006
// Modified: AP, Dec07 2006
void Ctrb::TRB_PI_test(char trb_brd) {
  const unsigned char command_code=0x5F;
  unsigned char command_line[2];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

  mc_firmware_version(); // Get firmware version
  TRB_PI_test_reset(); // Reset trb_pi_test struct
  if (HVer>1 || (HVer==1 && LVer>=14)) { // Firmware >= v1.14
    char tmpstr[100];
    sprintf(tmpstr,"Attention: TRB_PI_test(char) supported until v1.13 (actual: v%d.%d). Use TRB_PI_test(char, char).\n",HVer,LVer);
    trb_pi_test.msg=tmpstr;
    std::cout << tmpstr;
    return;
  }

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=trb_brd;

// Send the command line
  send_command_result = cmd_obj->send_command(command_line,2,TRB_PI_TEST_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = TRB_PI_test_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    trb_pi_test.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nTRB PI test (0x5F): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Send "TRB PI test" (0x5F) command and decode reply
// Author: A.Parenti, Jul20 2006
// Modified: AP, Dec07 2006
void Ctrb::TRB_PI_test(char trb_brd, char ignorectrl) {
  const unsigned char command_code=0x5F;
  unsigned char command_line[3];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

  mc_firmware_version(); // Get firmware version
  TRB_PI_test_reset(); // Reset trb_pi_test struct
  if (HVer<1 || (HVer==1 && LVer<14)) { // Firmware < v1.14
    char tmpstr[100];
    sprintf(tmpstr,"Attention: TRB_PI_test(char,char) supported from v1.14 (actual: v%d.%d). Use TRB_PI_test(char).\n",HVer,LVer);
    trb_pi_test.msg=tmpstr;
    std::cout << tmpstr;
    return;
  }

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=trb_brd;
  command_line[2]=ignorectrl;

// Send the command line
  send_command_result = cmd_obj->send_command(command_line,3,TRB_PI_TEST_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = TRB_PI_test_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    trb_pi_test.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nTRB PI test (0x5F): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset trb_pi_test struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec07 2006
void Ctrb::TRB_PI_test_reset() {
  int i;

  if (this==NULL) return; // Object deleted -> return

  trb_pi_test.code1=trb_pi_test.tss_err=0;
  for (i=0;i<4;++i)
    trb_pi_test.traco_err[i]=0;
  for (i=0;i<32;++i)
    trb_pi_test.bti_err[i]=0;

  trb_pi_test.code2=trb_pi_test.error=0;
  trb_pi_test.ccbserver_code=-5;
  trb_pi_test.msg="";
}


// Decode "TRB PI test" (0x5F) reply
// Author: A. Parenti, Feb03 2005
// Modified: AP, Dec07 2006
int Ctrb::TRB_PI_test_decode(unsigned char *rdstr) {
  const unsigned char right_reply=0x60, command_code=0x5F;
  unsigned char code2;
  int i, idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure

  idx = rstring(rdstr,idx,&trb_pi_test.code1);
  code2 = rdstr[idx];

// Fill-up the "result" structure
  if (trb_pi_test.code1==right_reply) {
    idx = rstring(rdstr,idx,&trb_pi_test.tss_err);
    for (i=0;i<4;++i)
      idx = rstring(rdstr,idx,trb_pi_test.traco_err+i);
    for (i=0;i<32;++i)
      idx = rstring(rdstr,idx,trb_pi_test.bti_err+i);
  } else if (trb_pi_test.code1==CCB_OXFC && code2==command_code) {
    idx = rstring(rdstr,idx,&trb_pi_test.code2);
    idx = rstring(rdstr,idx,&trb_pi_test.error);
  }

// Fill-up the error code
  if (trb_pi_test.code1==CCB_BUSY_CODE) // ccb is busy
    trb_pi_test.ccbserver_code = -1;
  else if (trb_pi_test.code1==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    trb_pi_test.ccbserver_code = 1;
  else if (trb_pi_test.code1==right_reply) // ccb: right reply
    trb_pi_test.ccbserver_code = 0;
  else if (trb_pi_test.code1==CCB_OXFC && code2==command_code) // ccb: right reply
    trb_pi_test.ccbserver_code = 0;
  else // wrong ccb reply
    trb_pi_test.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  trb_pi_test.msg="\nTRB PI test (0x5F)\n";

  if (trb_pi_test.ccbserver_code!=0) {// Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(trb_pi_test.ccbserver_code)); trb_pi_test.msg+=tmpstr;
  } else {
    if (trb_pi_test.code1==right_reply) {
      sprintf(tmpstr,"code: %#2X\n",trb_pi_test.code1); trb_pi_test.msg+=tmpstr;

      sprintf(tmpstr,"tss: %#2X\n",trb_pi_test.tss_err); trb_pi_test.msg+=tmpstr;
      for (i=0;i<4;++i) {
        sprintf(tmpstr,"traco[%i]: %#2X\n",i,trb_pi_test.traco_err[i]); trb_pi_test.msg+=tmpstr;
      }
      for (i=0;i<32;++i) {
        sprintf(tmpstr,"bti[%i]: %#2X\n",i,trb_pi_test.bti_err[i]); trb_pi_test.msg+=tmpstr;
      }
    } else {
      sprintf(tmpstr,"code1: %#2X\n",trb_pi_test.code1); trb_pi_test.msg+=tmpstr;
      sprintf(tmpstr,"code2: %#2X\n",trb_pi_test.code2); trb_pi_test.msg+=tmpstr;
      sprintf(tmpstr,"Wrong argument. error: %d\n",trb_pi_test.error); trb_pi_test.msg+=tmpstr;
    }
  }

  return idx;
}

// Print result of "TRB PI test" (0x5F) to stdout
// Author: A. Parenti, Jan15 2005
// Modified: AP, Dec07 2006
void Ctrb::TRB_PI_test_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",trb_pi_test.msg.c_str());
}


/*********************************************/
/* "Disable temperature test" (0x83) command */
/*********************************************/

// Send "Disable temperature test" (0x83) command and decode reply
// Author: A. Parenti, Jan15 2005
// Note: Tested at LNL, Mar09 2005
// Modified: AP, Mar24 2005
void Ctrb::disable_temp_test(char disable) {
  const unsigned char command_code=0x83;
  unsigned char command_line[2];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=disable;

  disable_temp_test_reset(); // Reset trb_temp_test struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,2,TRB_DISABLE_TEMP_TEST_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = disable_temp_test_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    trb_temp_test.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nDisable temperature test (0x83): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset trb_temp_test struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec07 2006
void Ctrb::disable_temp_test_reset (){
  if (this==NULL) return; // Object deleted -> return

  trb_temp_test.code1=trb_temp_test.code2=0;

  trb_temp_test.ccbserver_code=-5;
  trb_temp_test.msg="";
}


// Decode "Disable temperature test" (0x83) reply
// Author: A. Parenti, Feb03 2005
// Modified: AP, Dec07 2006
int Ctrb::disable_temp_test_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x83, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure

  idx = rstring(rdstr,idx,&trb_temp_test.code1);
  idx = rstring(rdstr,idx,&trb_temp_test.code2);

// Fill-up the error code
  if (trb_temp_test.code1==CCB_BUSY_CODE) // ccb is busy
    trb_temp_test.ccbserver_code = -1;
  else if (trb_temp_test.code1==CCB_UNKNOWN1 && trb_temp_test.code2==CCB_UNKNOWN2) // ccb: unknown command
    trb_temp_test.ccbserver_code = 1;
  else if (trb_temp_test.code1==right_reply && trb_temp_test.code2==command_code) // ccb: right reply
    trb_temp_test.ccbserver_code = 0;
  else // wrong ccb reply
    trb_temp_test.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  trb_temp_test.msg="\nDisable temperature test (0x83)\n";

  if (trb_temp_test.ccbserver_code!=0) {// Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(trb_temp_test.ccbserver_code)); trb_temp_test.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",trb_temp_test.code1); trb_temp_test.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",trb_temp_test.code2); trb_temp_test.msg+=tmpstr;
  }

  return idx;
}


// Print result of "Disable temperature test" (0x83) to stdout
// Author: A. Parenti, Jan15 2005
// Modified: AP, Dec07 2006
void Ctrb::disable_temp_test_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",trb_temp_test.msg.c_str());
}


/***********************************/
/* "Disable TRB CK" (0x93) command */
/***********************************/

// Send "Disable TRB CK" (0x93) command and decode reply
// Author: A. Parenti, May09 2005
// Modified: AP, Dec07 2006
void Ctrb::disable_TRB_CK(char state) {
  const unsigned char command_code=0x93;
  unsigned char command_line[2];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

  mc_firmware_version(); // Get firmware version
  disable_TRB_CK_reset(); // Reset disable_trb_ck struct
  if (HVer<1 || (HVer==1 && LVer<8)) { // Firmware < v1.8
    char tmpstr[100];
    sprintf(tmpstr,"Attention: command %#X supported after v1.8 (actual: v%d.%d)\n",command_code,HVer,LVer);
    disable_trb_ck.msg=tmpstr;
    std::cout << tmpstr;
    return;
  }

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=state;

// Send the command line
  send_command_result = cmd_obj->send_command(command_line,2,TRB_DISABLE_TRB_CK_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = disable_TRB_CK_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    disable_trb_ck.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nDisable TRB CK (0x93): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset disable_trb_ck struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec07 2006
void Ctrb::disable_TRB_CK_reset() {
  if (this==NULL) return; // Object deleted -> return

  disable_trb_ck.code1=disable_trb_ck.code2=0;

  disable_trb_ck.ccbserver_code=-5;
  disable_trb_ck.msg="";
}


// Decode "Disable TRB CK" (0x93) reply
// Author: A. Parenti, May09 2005
// Modified: AP, Dec07 2006
int Ctrb::disable_TRB_CK_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x93, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure

  idx = rstring(rdstr,idx,&disable_trb_ck.code1);
  idx = rstring(rdstr,idx,&disable_trb_ck.code2);

// Fill-up the error code
  if (disable_trb_ck.code1==CCB_BUSY_CODE) // ccb is busy
    disable_trb_ck.ccbserver_code = -1;
  else if (disable_trb_ck.code1==CCB_UNKNOWN1 && disable_trb_ck.code2==CCB_UNKNOWN2) // ccb: unknown command
    disable_trb_ck.ccbserver_code = 1;
  else if (disable_trb_ck.code1==right_reply && disable_trb_ck.code2==command_code) // ccb: right reply
    disable_trb_ck.ccbserver_code = 0;
  else // wrong ccb reply
    disable_trb_ck.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  disable_trb_ck.msg="\nDisable TRB CK (0x93)\n";

  if (disable_trb_ck.ccbserver_code!=0) {// Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(disable_trb_ck.ccbserver_code)); disable_trb_ck.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",disable_trb_ck.code1); disable_trb_ck.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",disable_trb_ck.code2); disable_trb_ck.msg+=tmpstr;
  }

  return idx;
}


// Print result of "Disable TRB CK" (0x93) to stdout
// Author: A. Parenti, May09 2005
// Modified: AP, Dec07 2006
void Ctrb::disable_TRB_CK_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",disable_trb_ck.msg.c_str());
}


/********************************/
/* "TRB PI test" (0xA0) command */
/********************************/

// Send "TRB PI test" (0xA0) command and decode reply
// Author: A. Parenti, Jul20 2006
// Modified: AP, Dec07 2006
void Ctrb::TRB_PI_test_0xA0(char nbrd, char idchip, char nchip, char reg, char write, char data) {
  const unsigned char command_code=0xA0;
  unsigned char command_line[7];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

  mc_firmware_version(); // Get firmware version
  TRB_PI_test_0xA0_reset(); // Reset trb_pi_test_0xA0 struct
  if (HVer<1 || (HVer==1 && LVer<14)) { // Firmware < v1.14
    char tmpstr[100];
    sprintf(tmpstr,"Attention: command %#X supported after v1.14 (actual: v%d.%d)\n",command_code,HVer,LVer);
    trb_pi_test_0xA0.msg=tmpstr;
    std::cout << tmpstr;
    return;
  }

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=nbrd;
  command_line[2]=idchip;
  command_line[3]=nchip;
  command_line[4]=reg;
  command_line[5]=write;
  command_line[6]=data;

// Send the command line
  send_command_result = cmd_obj->send_command(command_line,7,TRB_PI_TEST_0xA0_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = TRB_PI_test_0xA0_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    trb_pi_test_0xA0.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nTRB PI test (0xA0): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset trb_pi_test_0xA0 struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec07 2006
void Ctrb::TRB_PI_test_0xA0_reset() {
  if (this==NULL) return; // Object deleted -> return

  trb_pi_test_0xA0.code1=trb_pi_test_0xA0.code2=trb_pi_test_0xA0.data=0;

  trb_pi_test_0xA0.ccbserver_code=-5;
  trb_pi_test_0xA0.msg="";
}


// Decode "TRB PI test" (0xA0) reply
// Author: A. Parenti, Jul20 2006
// Modified: AP, Dec07 2006
int Ctrb::TRB_PI_test_0xA0_decode(unsigned char *rdstr) {
  const unsigned char command_code=0xA0, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure

  idx = rstring(rdstr,idx,&trb_pi_test_0xA0.code1);
  idx = rstring(rdstr,idx,&trb_pi_test_0xA0.code2);

// Fill-up the "result" structure

  idx = rstring(rdstr,idx,&trb_pi_test_0xA0.data);

// Fill-up the error code
  if (trb_pi_test_0xA0.code1==CCB_BUSY_CODE) // ccb is busy
    trb_pi_test_0xA0.ccbserver_code = -1;
  else if (trb_pi_test_0xA0.code1==CCB_UNKNOWN1 && trb_pi_test_0xA0.code2==CCB_UNKNOWN2) // ccb: unknown command
    trb_pi_test_0xA0.ccbserver_code = 1;
  else if (trb_pi_test_0xA0.code1==right_reply && trb_pi_test_0xA0.code2==command_code) // ccb: right reply
    trb_pi_test_0xA0.ccbserver_code = 0;
  else // wrong ccb reply
    trb_pi_test_0xA0.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  trb_pi_test_0xA0.msg="\nTRB PI test (0xA0)\n";

  if (trb_pi_test_0xA0.ccbserver_code!=0) {// Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(trb_pi_test_0xA0.ccbserver_code)); trb_pi_test_0xA0.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",trb_pi_test_0xA0.code1); trb_pi_test_0xA0.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",trb_pi_test_0xA0.code2); trb_pi_test_0xA0.msg+=tmpstr;

    sprintf(tmpstr,"data: %#2X\n",trb_pi_test_0xA0.data); trb_pi_test_0xA0.msg+=tmpstr;
  }

  return idx;
}

// Print result of "TRB PI test_0xA0" (0xA0) to stdout
// Author: A. Parenti, Jul20 2006
// Modified: AP, Dec07 2006
void Ctrb::TRB_PI_test_0xA0_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",trb_pi_test_0xA0.msg.c_str());
}


/*******************************************/
/* "Clear TRB SEU Counters" (0xA1) command */
/*******************************************/

// Send "Clear TRB SEU Counters" (0xA1) command and decode reply
// Author: A. Parenti, Jul20 2006
// Modified: AP, Dec07 2006
void Ctrb::clear_TRB_SEU_counter() {
  const unsigned char command_code=0xA1;
  unsigned char command_line[1];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

  mc_firmware_version(); // Get firmware version
  clear_TRB_SEU_counter_reset(); // Reset clear_trb_seu_counter struct
  if (HVer<1 || (HVer==1 && LVer<14)) { // Firmware < v1.14
    char tmpstr[100];
    sprintf(tmpstr,"Attention: command %#X supported after v1.14 (actual: v%d.%d)\n",command_code,HVer,LVer);
    clear_trb_seu_counter.msg=tmpstr;
    std::cout << tmpstr;
    return;
  }

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;

// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1,CLEAR_TRB_SEU_COUNTER_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = clear_TRB_SEU_counter_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    clear_trb_seu_counter.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nClear TRB SEU Counters (0xA1): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset clear_trb_seu_counter struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec07 2006
void Ctrb::clear_TRB_SEU_counter_reset() {
  if (this==NULL) return; // Object deleted -> return

  clear_trb_seu_counter.code1=clear_trb_seu_counter.code2=0;

  clear_trb_seu_counter.ccbserver_code=-5;
  clear_trb_seu_counter.msg="";
}


// Decode "Clear TRB SEU Counters" (0xA1) reply
// Author: A. Parenti, Jul20 2006
// Modified: AP, Dec07 2006
int Ctrb::clear_TRB_SEU_counter_decode(unsigned char *rdstr) {
  const unsigned char command_code=0xA1, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure

  idx = rstring(rdstr,idx,&clear_trb_seu_counter.code1);

// Fill-up the "result" structure

  idx = rstring(rdstr,idx,&clear_trb_seu_counter.code2);

// Fill-up the error code
  if (clear_trb_seu_counter.code1==CCB_BUSY_CODE) // ccb is busy
    clear_trb_seu_counter.ccbserver_code = -1;
  else if (clear_trb_seu_counter.code1==CCB_UNKNOWN1 && clear_trb_seu_counter.code2==CCB_UNKNOWN2) // ccb: unknown command
    clear_trb_seu_counter.ccbserver_code = 1;
  else if (clear_trb_seu_counter.code1==right_reply && clear_trb_seu_counter.code2==command_code) // ccb: right reply
    clear_trb_seu_counter.ccbserver_code = 0;
  else // wrong ccb reply
    clear_trb_seu_counter.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  clear_trb_seu_counter.msg="\nClear TRB SEU Counters (0xA1)\n";

  if (clear_trb_seu_counter.ccbserver_code!=0) {// Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(clear_trb_seu_counter.ccbserver_code)); clear_trb_seu_counter.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",clear_trb_seu_counter.code1); clear_trb_seu_counter.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",clear_trb_seu_counter.code2); clear_trb_seu_counter.msg+=tmpstr;
  }

  return idx;
}

// Print result of "Clear TRB SEU Counters_0xA1" (0xA1) to stdout
// Author: A. Parenti, Jul20 2006
// Modified: AP, Dec07 2006
void Ctrb::clear_TRB_SEU_counter_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",clear_trb_seu_counter.msg.c_str());
}



/****************************/
/* "Test IR" (0xA2) command */
/****************************/

// Send "Test IR" (0xA2) command and decode reply
// Author: A. Parenti, Jul20 2006
// Modified: AP, Dec07 2006
void Ctrb::test_IR() {
  const unsigned char command_code=0xA2;
  unsigned char command_line[1];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

  mc_firmware_version(); // Get firmware version
  test_IR_reset(); // Reset test_ir struct
  if (HVer<1 || (HVer==1 && LVer<14)) { // Firmware < v1.14
    char tmpstr[100];
    printf("Attention: command %#X supported after v1.14 (actual: v%d.%d)\n",command_code,HVer,LVer);
    test_ir.msg=tmpstr;
    std::cout << tmpstr;
    return;
  }

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;

// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1,TEST_IR_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = test_IR_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    test_ir.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nTest IR (0xA2): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset test_ir struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec07 2006
void Ctrb::test_IR_reset() {
  if (this==NULL) return; // Object deleted -> return

  test_ir.code1=test_ir.code2=0;

  test_ir.ccbserver_code=-5;
  test_ir.msg="";
}


// Decode "Test IR" (0xA2) reply
// Author: A. Parenti, Jul20 2006
// Modified: AP, Dec07 2006
int Ctrb::test_IR_decode(unsigned char *rdstr) {
  const unsigned char command_code=0xA2, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure

  idx = rstring(rdstr,idx,&test_ir.code1);

// Fill-up the "result" structure

  idx = rstring(rdstr,idx,&test_ir.code2);

// Fill-up the error code
  if (test_ir.code1==CCB_BUSY_CODE) // ccb is busy
    test_ir.ccbserver_code = -1;
  else if (test_ir.code1==CCB_UNKNOWN1 && test_ir.code2==CCB_UNKNOWN2) // ccb: unknown command
    test_ir.ccbserver_code = 1;
  else if (test_ir.code1==right_reply && test_ir.code2==command_code) // ccb: right reply
    test_ir.ccbserver_code = 0;
  else // wrong ccb reply
    test_ir.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  test_ir.msg="\nTest IR (0xA2)\n";

  if (test_ir.ccbserver_code!=0) {// Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(test_ir.ccbserver_code)); test_ir.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",test_ir.code1); test_ir.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",test_ir.code2); test_ir.msg+=tmpstr;
}

  return idx;
}

// Print result of "Test IR_0xA2" (0xA2) to stdout
// Author: A. Parenti, Jul20 2006
// Modified: AP, Dec07 2006
void Ctrb::test_IR_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",test_ir.msg.c_str());
}
