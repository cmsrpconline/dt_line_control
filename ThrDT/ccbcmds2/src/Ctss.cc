#include <ccbcmds/Ctss.h>
#include <ccbcmds/apfunctions.h>

#include <iostream>
#include <cstdio>

/*****************************************************************************/
// TSS class
/*****************************************************************************/


// Class constructor; arguments: pointer_to_command_object
// Author: A.Parenti, Dec01 2006
Ctss::Ctss(Ccommand* ppp) : Ccmn_cmd(ppp) {
  read_TSS_reset(); // Reset read_tss struct
  write_TSS_reset(); // Reset write_tss struct
}


/****************************/
/* Override ROB/TDC methods */
/****************************/

void Ctss::read_TDC(char tdc_brd, char tdc_chip) {}
int Ctss::read_TDC_decode(unsigned char *rdstr) {return 0;}
void Ctss::read_TDC_print() {}

void Ctss::status_TDC(char tdc_brd, char tdc_chip) {}
void Ctss::status_TDC_print() {}

void Ctss::read_ROB_error() {}
int Ctss::read_ROB_error_decode(unsigned char *rdstr) {return 0;}
void Ctss::read_ROB_error_print() {}

/*****************/
/* Configure TSS */
/*****************/

// Configure a TSS, reading from a "myconf" object
// Author: A.Parenti, Sep02 2005
// Modified: AP, Apr10 2006
// Modified: AP, Nov17 2006 (Added CRC)
short Ctss::config_TSS(Cconf *myconf, char tss_brd) {
  short conferr, confstatus, confcrc;

  if (this==NULL) return 0; // Object deleted -> return

// TSS error is BIT3 of conferr
  conferr = config_mc(myconf,CTSS,tss_brd,-1,&confstatus,&confcrc);

  if (((conferr>>3)&0x1)!=0) {
    config_TSS_result.error = true;
    config_TSS_result.crc = 0;
    return 1;
  }
  else {
    config_TSS_result.error = false;
    config_TSS_result.crc = confcrc;
    return 0;
  }
}

// Print to stdout the result of TSS configuration
// Author: A.Parenti, Sep02 2005
// Modified: AP, Nov17 2006
void Ctss::config_TSS_print() {
  if (this==NULL) return; // Object deleted -> return

  if (config_TSS_result.sent>0) {
    printf("Configure TSS: %d lines sent (%d successful)\n",config_TSS_result.sent,config_TSS_result.sent-config_TSS_result.bad);
    printf("               CRC=%#hX\n",config_TSS_result.crc);
    printf("               Error=%d\n",config_TSS_result.error);
  }
}


// Retrive TSS config from CCB, dump it to file
// Author: A.Parenti, Oct30 2006
// Modified: AP, Nov02 2006
void Ctss::read_TSS_config(char *filename) {
  char brd;
  unsigned char cmdline[29];
  char strcmd[88];
  int i;
  std::ofstream outfile(filename,std::ofstream::app);

  if (this==NULL) return; // Object deleted -> return

  printf("Read TSS config (0x16).\n");

  if (outfile.is_open()) {
// TSS, 0x16
    for (brd=0;brd<=5;++brd) {
      printf("Brd:%d ...",brd);

      read_TSS(brd);

      if (read_tss.code1==0x1E) {
        cmdline[0]=0x1D;
        cmdline[1]=brd;
        mystrcpy(cmdline+2,read_tss.conf,7);
        mystrcpy(cmdline+9,read_tss.testin,20);

        strcpy(strcmd,"");
        sprintf(strcmd,"%02X",cmdline[0]);
        for (i=1;i<29;++i)
          sprintf(strcmd,"%s %02X",strcmd,cmdline[i]);
        outfile << strcmd << '\n';
        outfile.flush();
      }

      printf("done!\n");
    }

    outfile.close();
  } else
    printf("Impossible to open file '%s'.\n",filename);

}

/*****************************/
/* "read TSS" (0x1D) command */
/*****************************/

// Send "read TSS" (0x1D) command and decode reply
// Author: A. Parenti, Dec18 2004
// Modified: AP, Mar24 2005
// Note: Tested at Cessy, Oct05 2006
void Ctss::read_TSS(char tss_brd) {
  const unsigned char command_code=0x1D;
  unsigned char command_line[2];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=tss_brd;

  read_TSS_reset(); // Reset read_tss struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,2,READ_TSS_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = read_TSS_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    read_tss.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nRead TSS (0x1D): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset read_tss struct
// Author: A.Parenti, Aug09 2006
// Modified: AP, Dec07 2006
void Ctss::read_TSS_reset() {
  int i;

  if (this==NULL) return; // Object deleted -> return

  read_tss.code1=0;
  for (i=0;i<7;++i)
    read_tss.conf[i]=0;
  for (i=0;i<20;++i)
    read_tss.testin[i]=0;
  for (i=0;i<32;++i)
    read_tss.testout[i]=0;

  read_tss.code2=read_tss.narg=0;
  read_tss.ccbserver_code=-5;
}


// Decode "read TSS" (0x1D) reply
// Author: A. Parenti, Feb08 2005
// Modified: AP, Dec07 2006
int Ctss::read_TSS_decode(unsigned char *rdstr) {
  const unsigned char right_reply=0x1E, command_code=0x1D;
  unsigned char code2;
  int i, idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&read_tss.code1);
  code2 = rdstr[idx];

// Fill-up the "result" structure
  if (read_tss.code1==right_reply) {
    for (i=0;i<7;++i)
      idx = rstring(rdstr,idx,read_tss.conf+i);
    for (i=0;i<20;++i)
      idx = rstring(rdstr,idx,read_tss.testin+i);
    for (i=0;i<32;++i)
      idx = rstring(rdstr,idx,read_tss.testout+i);
  } else if (read_tss.code1==CCB_OXFC && code2==command_code) {
    idx = rstring(rdstr,idx,&read_tss.code2);
    idx = rstring(rdstr,idx,&read_tss.narg);
  }

// Fill-up the error code
  if (read_tss.code1==CCB_BUSY_CODE) // ccb is busy
    read_tss.ccbserver_code = -1;
  else if (read_tss.code1==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    read_tss.ccbserver_code = 1;
  else if (read_tss.code1==right_reply) // ccb: right reply
    read_tss.ccbserver_code = 0;
  else if (read_tss.code1==CCB_OXFC && code2==command_code) // ccb: right reply
    read_tss.ccbserver_code = 0;
  else // wrong ccb reply
    read_tss.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  read_tss.msg="\nRead TSS (0x1D)\n";

  if (read_tss.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(read_tss.ccbserver_code)); read_tss.msg+=tmpstr;
  } else {

    if (read_tss.code1==right_reply) {
      sprintf(tmpstr,"code: %#2X\n",read_tss.code1); read_tss.msg+=tmpstr;
      for (i=0;i<7;++i) {
        sprintf(tmpstr,"conf[%i]: %#2X\n",i,read_tss.conf[i]); read_tss.msg+=tmpstr;
      }
      for (i=0;i<20;++i) {
        sprintf(tmpstr,"testin[%i]: %#2X\n",i,read_tss.testin[i]); read_tss.msg+=tmpstr;
      }
      for (i=0;i<32;++i) {
        sprintf(tmpstr,"testout[%i]: %#2X\n",i,read_tss.testout[i]); read_tss.msg+=tmpstr;
      }
    } else {
      sprintf(tmpstr,"code1: %#2X\n",read_tss.code1); read_tss.msg+=tmpstr;
      sprintf(tmpstr,"code2: %#2X\n",read_tss.code2); read_tss.msg+=tmpstr;
      sprintf(tmpstr,"Wrong argument. narg: %d\n",read_tss.narg); read_tss.msg+=tmpstr;
    }
  }

  return idx;
}


// Print result of "read TSS" (0x1D) to stdout
// Author: A. Parenti, Dec18 2004
// Modified: AP, Dec07 2006
void Ctss::read_TSS_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",read_tss.msg.c_str());
}


/**********************************/
/* "write TSS" (0x16) command */
/**********************************/

// Send "write TSS" (0x16) command and decode reply
// Author: A. Parenti, Dec18 2004
// Note: Tested at LNL, Mar09 2005
// Modified: AP, Mar24 2005
void Ctss::write_TSS(char tss_brd, unsigned char conf[7], unsigned char testin[20]) {
  const unsigned char command_code=0x16;
  unsigned char command_line[29];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=tss_brd;
  mystrcpy(command_line+2,conf,7);
  mystrcpy(command_line+9,testin,20);

  write_TSS_reset(); // Reset write_tss struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,29,WRITE_TSS_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = write_TSS_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    write_tss.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nWrite TSS (0x16): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset write_tss struct
// Author: A.Parenti, Aug09 2006
// Modified: AP, Dec07 2006
void Ctss::write_TSS_reset() {
  if (this==NULL) return; // Object deleted -> return

  write_tss.code1=write_tss.code2=0;
  write_tss.result=0;

  write_tss.ccbserver_code=-5;
  write_tss.msg="";
}


// Decode "write TSS" (0x16) reply
// Author: A. Parenti, Feb08 2005
// Modified: AP, Dec12 2006
int Ctss::write_TSS_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x16, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  write_tss.code1  = rdstr[0];
  write_tss.code2  = rdstr[1];

// Fill-up the "result" structure
  write_tss.result = rdstr[2];

  idx=3;

// Fill-up the error code
  if (write_tss.code1==CCB_BUSY_CODE) // ccb is busy
    write_tss.ccbserver_code = -1;
  else if (write_tss.code1==CCB_UNKNOWN1 && write_tss.code2==CCB_UNKNOWN2) // ccb: unknown command
    write_tss.ccbserver_code = 1;
  else if (write_tss.code1==right_reply && write_tss.code2==command_code) // ccb: right reply
    write_tss.ccbserver_code = 0;
  else // wrong ccb reply
    write_tss.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

 write_tss.msg="\nWrite TSS (0x16)\n";

  if (write_tss.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(write_tss.ccbserver_code)); write_tss.msg+=tmpstr;
  } else {

    sprintf(tmpstr,"code1: %#2X\n",write_tss.code1); write_tss.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",write_tss.code2); write_tss.msg+=tmpstr;

    switch (write_tss.result) {
    case 0:
      write_tss.msg+="Result: Verify error\n";
      break;
    case 1:
      write_tss.msg+="Result: Write successful\n";
      break;
    case -1:
      write_tss.msg+="Result: Parameter error\n";
      break;
    }
  }

  return idx;
}


// Print results of "write TSS" (0x16) command to standard output
// Author:  A.Parenti Dec18 2004
// Modified: AP, Dec07 2006
void Ctss::write_TSS_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",write_tss.msg.c_str());
}
