The ccbcmds library defines a class that is a starting point for a drift tube
DCS. The class is "dtdcs", defined in ccbcmd/Cdtdcs.h.

The port of the ccbserver is set in the inc/Cdef.h file; set CCBPORT before
compiling and installing the ccbcmds library.

The DB that will be used is defined in the /etc/ccbdb.conf; check the
settings and change them if necessary. You can log to DB or to file by defining
LOGTODB in inc/Cdef.h or not.

Since much threads are started by the class, some user limits must be 
checked and possibly set. In particular:
- max user processes >= 1024
- stack size <= 2048 kbytes
You can check the current values whit "ulimit -a" (bash) or "limit" (tcsh) 
and set them with:
- "ulimit -u 1024" or "limit maxproc 1024"
- "ulimit -s 2048" or "limit stacksize 2048"
If these values are not set properly, library may crash.

The provided functionalities are:

Initialization of a partition
*****************************
*  int partInitialize(char *partition); // Input: partition_name
*  int partInitialize(); // Re-use the last selected partition 

Both methods return 0 if a valid partition was selected.
When a partition is initialized, the monitoring also starts.
For each CCB in the partition a struct dtccb[i] is created; the number of
CCBs is nccb.

*  void verbose_mode(bool in); -> switch to verbose mode (in=true)
*  void enableAutoRecover(); -> enable ccb autorecover (eg reconfiguration)
*  void disableAutoRecover(); -> disable ccb autorecover


Resetting the partition
***********************
*  void partReset(); // Reset a dt partition


Show partition info
*******************
*  int partGetNCcb(); // Return No. of ccb in current partition
*  int partGetNExcludedCcb(); // Return No. of "excluded" ccb in current
   partition
*  int* partGetCcbList(); // Return a list of ccb in partition 
*  char* partGetPartName(); // Return the name of partition
*  void partShowActual(); // Print to stdout the actual partition
*  void partShowAvailable(); // Print to stdout the available partitions

Monitoring
**********

*  void partStartMonitor(); // Start monitor with default interval
*  void partStopMonitor(); // Abort monitoring, if ongoing
*  void partSetMcMonitorInterval(int monitor_time,int write_time); // Set
   minicrate monitoring/logging time interval (seconds)
*  void partSetPrMonitorInterval(int monitor_time,int write_time); // Set
   pressure monitoring/logging interval (seconds)
*  void partRefreshStatus(); // Force the application to update partition
   status
*  void partShowStatus(); // Print partition status to stdout
*  void partShowStatus(std::string *strout); // Print partition status to a
   string

*  Emcstatus* partGetPhysStatus(); // Return an array w/ MC "physical" status
*  Emcstatus* partGetLogicStatus(); // Return an array w/ MC "logical" status
*  bool *partGetSwitchOffFe(); // Return an array w/ FE "switch off" flags
*  bool *partGetSwitchOffMc(); // Return an array w/ MC "switch off" flags
*  Emcstatus* partGetRobStatus(); // Return an array w/ ROB status
*  Emcstatus* partGetPadcStatus(); // Return an array w/ PADC status

*  bool* partGetExcluded(); // Return an array w/ MC "excluded" flag
*  float* partGetMaxTemp(); // Return an array w/ max MC temperatures
*  float* partGetRobMaxTemp(); // Return an array w/ max ROB temperatures
*  float* partGetTrbMaxTemp(); // Return an array w/ max TRB temperatures
*  float* partGetExtMaxTemp(); // Return an array w/ max EXT temperatures
*  float* partGetFeMaxTemp(); // Return an array w/ max FE temperatures
*  float* partGetHvPressure(); // Return an array w/ gas pressures HV side
*  float* partGetFePressure(); // Return an array w/ gas pressures FE side


The monitoring status is written in the DB and stored locally in 
dtccb[i].

Other commands
**************
*  void partSetRun(int RunNr); // Perform pre-run operations
*  void partUnsetRun(); // Perform post-run operations
*  void partStopSetRun(); // Abort pre/post-run operations
*  bool partReadyForRun(); // Return 'true' if partition is ready to run
*  void partSendCommand(Ecommand cmdtype); // Input: command_type


Configurations
**************
*  void confShowAvailable(); // Show the available configurations
*  char* confGetConfName(); // Return the name of current configuration
*  void confStart(char *cname); // Start configuration
*  void confStop(); // Abort configuration, if ongoing
*  Econfstatus confGetStatus(); // Return configuration status: undef,...
*  Econfstatus* confGetDetailedStatus(); // Return an array w/ ccb
   configuration status: undef,...
*  bool confCheckCrc(); // Return 'true' if configuration CRC is correct


**********************
* SINGLE CCB methods *
**********************

Connection status
*****************
*  int ccbserverTestConnection(int ccbid); // Check connection w/ ccbserver.
   Return 0 when ok, >0 if ccb is not in partition.
*  int ccbTestConnection(int ccbid); // Check connection w/ ccb. Return 0 when
   ok, >0 if ccb is not in partition.

CCB information
***************
*  int idxFromCcbId(int ccbid); // Return the index of the given ccbid
*  int ccbGetCcbId(int wheel, int sector, int station); // Return ccbid
*  void ccbGetWhSecSt(int ccbid, int *wheel, int *sector, int *station);
   // Return wheel, sector, station

CCB status
**********
*  time_t ccbGetStatusTime(int ccbid); // Return time of last update
*  time_t ccbGetPressureTime(int ccbid); // Return time of last update

*  Emcstatus ccbGetPhysStatus(int ccbid); // Return minicrate "physical" status
*  Emcstatus ccbGetLogicStatus(int ccbid); // Return minicrate "logical" status
*  bool ccbGetSwitchOffFe(int ccbid); // Return FE "switch off" flag
*  bool ccbGetSwitchOffMc(int ccbid); // Return MC "switch off" flag
*  Emcstatus ccbGetRobStatus(int ccbid); // Return ROB status
*  Emcstatus ccbGetPadcStatus(int ccbid); // Return PADC status
*  void ccbRefreshStatus(int ccbid); // Force the update of ccb status
*  bool ccbGetExcluded(int ccbid); // Return minicrate "excluded" flag
*  char* ccbGetStatusMsg(int ccbid); // Return CCB status string
*  char* ccbGetErrorMsg(int ccbid); // Return CCB errors string
*  char* ccbGetRobStatusMsg(int ccbid); // Return ROB status string
*  char* ccbGetRobErrorMsg(int ccbid); // Return ROB error string
*  char* ccbGetPadcStatusMsg(int ccbid); // Return PADC status string
*  char* ccbGetPadcErrorMsg(int ccbid); // Return PADC error string

*  float ccbGetMaxTemp(int ccbid); // Return max minicrate temperature
*  float ccbGetRobMaxTemp(int ccbid); // Return max ROB temperature
*  float ccbGetTrbMaxTemp(int ccbid); // Return max TRB temperature
*  float ccbGetExtMaxTemp(int ccbid); // Return max EXT temperature
*  float ccbGetFeMaxTemp(int ccbid); // Return max FE temperature
*  float* ccbGetExtTemp(int ccbid); // Return all (7) EXT temperatures

*  float ccbGetHvPressure(int ccbid); // Return gas pressure, HV side
*  float ccbGetFePressure(int ccbid); // Return gas pressure, FE side

CCB Configuration
*****************
*  Econfstatus ccbGetConfStatus(int ccbid); // Return configuration status:
   undef,...
*  char* ccbGetConfName(int ccbid); // Return the name of current
   configuration (configs.ConfName)
*  void ccbReconfigure(int ccbid); // Configure again a minicrate

Other commands
**************
*  std::string ccbSendCommand(int ccbid, Ecommand cmdtype); // Send a command
   to a ccb. Return result
*  std::string ccbGetCommandResult(int ccbid); // Return results of the last
   command

Author: A. Parenti
Revision: Oct22 2007
