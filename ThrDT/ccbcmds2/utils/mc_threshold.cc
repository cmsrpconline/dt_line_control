#include <ccbcmds/apfunctions.h>
#include <ccbcmds/Clog.h>
#include <ccbcmds/Ccommand.h>
#include <ccbcmds/Ccmn_cmd.h>
#include <ccbcmds/Cfe.h>
#include <ccbcmds/Cdef.h>

#include <iostream>
#include <stdio.h>
#include <string>

// Author: A.Parenti, Jul21 2006
// Modified: AP, Jan26 2007
int main(int argc, char *argv[]) {
  char str0[24], str1[24], str2[24];
  short ccbport, i;
  float width;

  Clog *myLog;
  Ccommand *myCmd;
  Ccmn_cmd *myCmn;
  Cfe *myFe;

  if (argc==2) {
    sscanf(argv[1],"%d",&ccbport);
  } else {
    printf("Usage: mc_threshold <ccbport>\n");

    printf("ccbport? ");
    scanf("%d",&ccbport);
  }

#ifdef LOGTODB
  myLog = new Clog(); // Log to DB
#else
  myLog = new Clog("log.txt"); // Log to file
#endif
  myCmd = new Ccommand(ccbport,CCBSERVER,CCBPORT,myLog);
  myCmn = new Ccmn_cmd(myCmd); // New CCB object
  myFe  = new Cfe(myCmd); // New FE object 

  printf("Using server %s:%d\n",myCmd->read_server_name(),myCmd->read_server_port());

  myCmn->MC_read_status();

  printf("*** Actual status ***\n");
  printf("Biases: %f %f %f\n",myCmn->mc_status.Fe_Bias[0],
         myCmn->mc_status.Fe_Bias[1],myCmn->mc_status.Fe_Bias[2]);
  printf("Thresholds: %f %f %f\n",myCmn->mc_status.Fe_Thr[0],
         myCmn->mc_status.Fe_Thr[1],myCmn->mc_status.Fe_Thr[2]);
  printf("Width: %f\n",myCmn->mc_status.Fe_Width);


  i=0;
  while (i<1 || i>4) {
    printf("\n");
    printf("1. Set biases\n");
    printf("2. Set thresholds\n");
    printf("3. Set width\n");
    printf("4. quit\n");

    scanf("%d",&i);
  }

  myCmd->set_write_log(true); // Switch on logger

  switch (i) {
  case 1:
    printf("Bias0 Bias1 Bias2?\n");
    scanf("%s %s %s",str0,str1,str2);

    myFe->set_FE_thr(0,strtod(str0,NULL),myCmn->mc_status.Fe_Thr[0]);
    myFe->set_FE_thr_print();
    myFe->set_FE_thr(1,strtod(str1,NULL),myCmn->mc_status.Fe_Thr[1]);
    myFe->set_FE_thr_print();
    myFe->set_FE_thr(2,strtod(str2,NULL),myCmn->mc_status.Fe_Thr[1]);
    myFe->set_FE_thr_print();
    break;
  case 2:
    printf("Thr0 Thr1 Thr2?\n");
    scanf("%s %s %s",str0,str1,str2);

    myFe->set_FE_thr(0,myCmn->mc_status.Fe_Bias[0],strtod(str0,NULL));
    myFe->set_FE_thr_print();
    myFe->set_FE_thr(1,myCmn->mc_status.Fe_Bias[1],strtod(str1,NULL));
    myFe->set_FE_thr_print();
    myFe->set_FE_thr(2,myCmn->mc_status.Fe_Bias[2],strtod(str2,NULL));
    myFe->set_FE_thr_print();
    break;
  case 3:
    printf("Width?\n");
    scanf("%f",&width);

    myFe->set_FE_width(width);
    myFe->set_FE_width_print();
    break;
  }

}
