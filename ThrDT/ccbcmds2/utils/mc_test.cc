#include <ccbcmds/apfunctions.h>
#include <ccbcmds/Clog.h>
#include <ccbcmds/Ccommand.h>
#include <ccbcmds/Cccb.h>
#include <ccbcmds/Cdef.h>

#ifdef __USE_CCBDB__ // DB access enabled
  #include <ccbdb/Ccmsdtdb.h>
#endif

#include <iostream>
#include <stdio.h>

// Author: A.Parenti, Oct10 2006
// Modified: AP, Aug31 2007
#ifdef __USE_CCBDB__ // DB access enabled
int main(int argc, char *argv[]) {
  Emcstatus flag_tst, sb_tst, trb_tst, rob_tst;
  char message[4096];
  short pippo, ccbport, ccbid;

  Clog *myLog;
  Ccommand *myCmd;
  Cccb *myCcb;
  Cref *myRef;

  if (argc==3) {
    sscanf(argv[1],"%d",&pippo);
    ccbport=pippo;

    sscanf(argv[2],"%d",&pippo);
    ccbid=pippo;
  } else {
    printf("Usage: mc_test [port] [ccbid]\n");

    printf("port? ");
    scanf("%d",&pippo);
    ccbport=pippo;

    printf("ccbid? ");
    scanf("%d",&pippo);
    ccbid=pippo;
  }

  cmsdtdb mydtdtb;
  myLog = new Clog("log.txt"); // Log to file
  myCmd = new Ccommand(ccbid,ccbport,CCBSERVER,CCBPORT,myLog);
  myCcb = new Cccb(myCmd); // New CCB object
  myRef = new Cref(ccbid,&mydtdtb); // read reference from DB

  printf("Using server %s:%d\n",myCmd->read_server_name(),myCmd->read_server_port());

  printf("*** Actual time: %s\n",get_time());
  myCcb->MC_check_test(myRef,false,&flag_tst,&sb_tst,&trb_tst,&rob_tst,message); // 0x10 (only read test results)
  myCcb->MC_check_test_print();
}

#else
int main() {
  std::cout << "Nothing done. Please compile with DB support.\n";
}
#endif
