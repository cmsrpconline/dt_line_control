#include <iostream>
#include <stdio.h>

#ifndef __USE_CCBDB__ // No DB access
int main() {
  std::cout << "Nothing done. Please compile with DB support.\n";
}
#else

#include <ccbdb/Cdtpartition.h>

#include <ccbcmds/apfunctions.h>
#include <ccbcmds/Clog.h>
#include <ccbcmds/Cccb.h>
#include <ccbcmds/Cdef.h>

// Author: A. Parenti, Aug03 2006
// Modified: AP, Nov09 2006
int main(int argc, char *argv[]) {
  char on;
  char partname[32]; // Partition name
  int i, nccb;
  int ccblist[256], portlist[256];
  Ccommand *myCmd;
  Clog *myLog;
  Cccb *myCcb;

  dtpartition mypart; // datasource from /etc/ccbdb.conf

  if (argc < 3) {
    printf("*** Usage: dt_run_in_progress <partion name> <on>\n");
    printf("*** on: 1=run_in_progress, 0\n");

// List available partitions
    char partitionlist[256][32];
    int npart;

    printf("Available partitions:\n");
    mypart.retrieveparts(&npart,partitionlist);
    for (i=0;i<npart;++i)
      printf("  %s\n",partitionlist[i]);

    return -1;
  }  else {
    sscanf(argv[1],"%s",&partname);
    sscanf(argv[2],"%d",&on);

    mypart.retrieveccbs(partname,&nccb,ccblist,portlist);
  }

#ifdef LOGTODB
  myLog = new Clog(); // Log to DB
#else
  myLog = new Clog("log.txt"); // Log to file
#endif

  for (i=0;i<nccb;++i) {

    myCmd = new Ccommand(portlist[i],CCBSERVER,CCBPORT,myLog);
    myCcb = new Cccb(myCmd); // New CCB object

    myCmd->set_write_log(true); // Switch on logger

    printf("\nUsing server %s:%d\n",myCmd->read_server_name(),myCmd->read_server_port());
    printf("*** Run in Progress, ccbid: %d\n",ccblist[i]);
    printf("*** Actual time: %s\n",get_time());
    myCcb->run_in_progress(on); // 0x46
    myCcb->run_in_progress_print();

    delete myCcb;
    delete myCmd;
  }
}

#endif
