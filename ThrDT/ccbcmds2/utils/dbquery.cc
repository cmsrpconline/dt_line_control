#include <iostream>
#include <stdio.h>

#ifndef __USE_CCBDB__ // No DB access
int main() {
  std::cout << "Nothing done. Please compile with DB support.\n";
}
#else

#include <ccbdb/Ccmsdtdb.h>

// Author: A. Parenti, Jul18 2007
// Send a query to the default DB, print result on stdout
// Tested on MySQL/Oracle
int main(int argc, char *argv[]) {
  char c;
  char resu[FIELD_MAX_LENGTH];
  int i, j, rows, cols, len;
  std::string thequery="";
  cmsdtdb thedb;

  printf("Query? ");
  while ((c=getchar()) != '\n')
    thequery+=c;

  thedb.sqlquery((char*)thequery.c_str(),&rows,&cols); // Submit query


  std::cout << "Query result:\n";
  for (i=0; thedb.fetchrow() == SQL_SUCCESS; ++i) {
    for (j=0; j<cols; ++j) {
      thedb.returnfield(j,resu);
      printf(" %s\n",resu);
    }
    std::cout << std::endl;
  }
}

#endif
