#include <iostream>
#include <stdio.h>

#ifndef __USE_CCBDB__ // No DB access
int main() {
  std::cout << "Nothing done. Please compile with DB support.\n";
}
#else

#include <ccbdb/Cdtpartition.h>

#include <ccbcmds/apfunctions.h>
#include <ccbcmds/Clog.h>
#include <ccbcmds/Crob.h>
#include <ccbcmds/Cdef.h>


#define NMAX 300

// Author: A. Parenti, Jul29 2006
// Modified: AP, Oct23 2007
int main(int argc, char *argv[]) {
  char partname[32]; // Partition name
  char ccbserver[NMAX][255];
  int i, nccb;
  int ccblist[NMAX], portlist[NMAX], secportlist[NMAX];
  Ccommand *myCmd;
  Clog *myLog;
  Crob *myRob;

  dtpartition mypart; // datasource from /etc/ccbdb.conf

  if (argc < 2) {
    printf("*** Usage: dt_robreset <partion name>\n");

// List available partitions
    char partitionlist[NMAX][32];
    int npart;

    printf("Available partitions:\n");
    mypart.retrieveparts(&npart,partitionlist);
    for (i=0;i<npart;++i)
      printf("  %s\n",partitionlist[i]);

    return -1;
  }  else {
    sscanf(argv[1],"%s",&partname);

    mypart.retrieveccbs(partname,&nccb,ccblist,portlist,secportlist,ccbserver);
  }

#ifdef LOGTODB
  myLog = new Clog(); // Log to DB
#else
  myLog = new Clog("log.txt"); // Log to file
#endif

  for (i=0;i<nccb;++i) {

    myCmd = new Ccommand(portlist[i],ccbserver[i],CCBPORT,myLog);
    myRob = new Crob(myCmd); // New ROB object

    myCmd->set_write_log(true); // Switch on logger

    printf("\nUsing server %s:%d\n",myCmd->read_server_name(),myCmd->read_server_port());
    printf("*** Reset ROB, ccbid: %d\n",ccblist[i]);
    printf("*** Actual time: %s\n",get_time());
    myRob->reset_ROB(); // 0x32
    myRob->reset_ROB_print();

    delete myRob;
    delete myCmd;
  }
}

#endif
