#include <ccbcmds/apfunctions.h>
#include <ccbcmds/Clog.h>
#include <ccbcmds/Ccommand.h>
#include <ccbcmds/Cccb.h>
#include <ccbcmds/Cdef.h>

#include <iostream>
#include <stdio.h>

// Author: A.Parenti, Jun29 2006
// Modified: AP, Jan26 2007
int main(int argc, char *argv[]) {
  short ccbport;
  Clog *myLog;
  Ccommand *myCmd;
  Cccb *myCcb;

  if (argc < 2) {
    printf("*** Usage: mc_temp [ccbport]\n");

    printf("ccbport? ");
    scanf("%d",&ccbport);
  }  else
    sscanf(argv[1],"%d",&ccbport);

  myLog = new Clog("log.txt"); // Log to file
  myCmd = new Ccommand(ccbport,CCBSERVER,CCBPORT,myLog);
  myCcb = new Cccb(myCmd); // New CCB object

  printf("Using server %s:%d\n",myCmd->read_server_name(),myCmd->read_server_port());

//  while (true) {
    printf("\n\n\n");
    printf("*** Temperature Monitor ***\n");
    printf("*** Actual time: %s\n",get_time());
    myCcb->MC_temp(); // 0x3C
    myCcb->MC_temp_print();
//    apsleep(30000);
//  }
}
