#include <ccbcmds/apfunctions.h>
#include <ccbcmds/Clog.h>
#include <ccbcmds/Ccommand.h>
#include <ccbcmds/Cconf.h>
#include <ccbcmds/Ccmn_cmd.h>
#include <ccbcmds/Cdef.h>

// For DAC calibration
#include <ccbcmds/Cttcrx.h>
#include <ccbcmds/Cfe.h>
#ifdef __USE_CCBDB__ // DB access enabled
  #include <ccbdb/Cdaccalib.h>
#endif

#include <iostream>
#include <stdio.h>

void ctype_list(void);

// Author: A.Parenti, Jun29 2006
// Modified: AP, Apr19 2007 (crashed for some users: lowered CMD_MAXLINES->999)
// Modified: AP, Sep14 2007 (Revised DAC calibration)
int main(int argc, char *argv[]) {
  unsigned char cmd_line[999][CMD_LINE_DIM];
  char filename[128];
  short ccbport;
  short conf_err, conf_status, conf_crc;
  int i, line_len[999], lines;

  Clog *myLog;
  Ccommand *myCmd;
  Cconf *myConf;
  Ccmn_cmd *myCmn;
  ConfType ctype;

  if (argc < 4) {
    printf("*** Usage: mc_config <ccbport> <filename> <config_type>\n");
    printf("*** <config_type> assume values:\n");
    ctype_list();

    printf("ccbport? ");
    scanf("%d",&ccbport);
    printf("config file? ");
    scanf("%s",filename);

// Select configuration type
    i=0;
    while (i<1 || i>10) {
      printf("config type (1-10)? ");
      scanf("%d",&i);
    }
  }  else {
    sscanf(argv[1],"%d",&ccbport);
    sscanf(argv[2],"%s",filename);
    sscanf(argv[3],"%d",&i);
  }

  switch(i) {
  case 1:
    ctype=CALL;
    break;
  case 2:
    ctype=CBTI;
    break;
  case 3:
    ctype=CTRACO;
    break;
  case 4:
    ctype=CLUT;
    break;
  case 5:
    ctype=CTSS;
    break;
  case 6:
    ctype=CTSM;
    break;
  case 7:
    ctype=CTDC;
    break;
  case 8:
    ctype=CROB;
    break;
  case 9:
    ctype=CFE;
    break;
  case 10:
    ctype=COTHER;
    break;
  default:
    ctype=CALL;
  }

#ifdef LOGTODB
  myLog = new Clog(); // Log to DB
#else
  myLog = new Clog("log.txt"); // Log to file
#endif
  myCmd = new Ccommand(ccbport,CCBSERVER,CCBPORT,myLog);
  myConf = new Cconf(filename);
  myCmn = new Ccmn_cmd(myCmd); // New CMN_CMD object

  lines = myConf->read_conf(ccbport,(unsigned char)0,(char)-1,(char)-1,cmd_line,line_len); // read lines from configuration

  if (lines<=0) {
    printf("File %s not found or empty.\n",filename);
    return -1;
  }

  printf("Using server %s:%d\n",myCmd->read_server_name(),myCmd->read_server_port());

// DAC calibration
  if (ctype==CALL || ctype==CFE) {
    float DacCalibs[14];
    Cttcrx myTtcrx(myCmd);
    Cfe myFe(myCmd);

    printf("%sCalibrating DAC...%s",BLUBKG,STDBKG);

#ifdef __USE_CCBDB__ // DB access enabled   
    myTtcrx.get_TTCRX_id(); // Get TTCid
    if (myTtcrx.ttcrx_id.read_err!=0) {
      DacCalibs[0]=DacCalibs[1]=DacCalibs[2]=DacCalibs[3]=DacCalibs[4]=DacCalibs[5]=DacCalibs[6]=1.0;
      DacCalibs[7]=DacCalibs[8]=DacCalibs[9]=DacCalibs[10]=DacCalibs[11]=DacCalibs[12]=DacCalibs[13]=0.0;
      printf(" TTCRX_id not read, using standard calib...");
    } else {

      daccalib myDacCalib;
      if (myDacCalib.retrievedaccalib(myTtcrx.ttcrx_id.id,DacCalibs)!=0) {
	DacCalibs[0]=DacCalibs[1]=DacCalibs[2]=DacCalibs[3]=DacCalibs[4]=DacCalibs[5]=DacCalibs[6]=1.0;
	DacCalibs[7]=DacCalibs[8]=DacCalibs[9]=DacCalibs[10]=DacCalibs[11]=DacCalibs[12]=DacCalibs[13]=0.0;
      printf(" No calib found from DB, using standard calib...");
      }
    }
#else // DB access disabled
    DacCalibs[0]=DacCalibs[1]=DacCalibs[2]=DacCalibs[3]=DacCalibs[4]=DacCalibs[5]=DacCalibs[6]=1.0;
    DacCalibs[7]=DacCalibs[8]=DacCalibs[9]=DacCalibs[10]=DacCalibs[11]=DacCalibs[12]=DacCalibs[13]=0.0;
#endif

// Thr offsets are in mV in the database, but 0x6B command wants them in Volts
    DacCalibs[11]/=1000;
    DacCalibs[12]/=1000;
    DacCalibs[13]/=1000;

    myFe.set_calib_DAC(DacCalibs+1,DacCalibs+4,*DacCalibs,DacCalibs+8,DacCalibs+11,*(DacCalibs+7));
    if (myFe.set_calib_dac.ccbserver_code==0)
      printf("%s calibration done!%s\n",GRNBKG,STDBKG);
    else
      printf("%s calibration error!%s\n",REDBKG,STDBKG);

  }

// config ALL
  conf_err = myCmn->config_mc(myConf,ctype,-1,-1,&conf_status,&conf_crc);

// Write Conf results to stdout
  printf("*** ccbport: %d\n",ccbport);
  printf("*** File %s: %d lines found\n",filename,lines);
  printf("*** Configuration done at: %s\n",get_time());

  if ((conf_status&0x1FF)==0x1FF) // nothing configured!
    printf("%sCCBport %d - Nothing configured%s\n",REDBKG,ccbport,STDBKG);

// BTI config result: BIT0
  if ((conf_err&0x1)!=0)
    printf("%sCCBport %d - BTI config: error%s\n",REDBKG,ccbport,STDBKG);
  else if ((conf_status&0x1)==0) // configured!
    printf("%sCCBport %d - BTI config: ok%s\n",GRNBKG,ccbport,STDBKG);

// TRACO config result: BIT1
  if (((conf_err>>1)&0x1)!=0)
    printf("%sCCBport %d - TRACO config: error%s\n",REDBKG,ccbport,STDBKG);
  else if (((conf_status>>1)&0x1)==0) // configured!
    printf("%sCCBport %d - TRACO config: ok%s\n",GRNBKG,ccbport,STDBKG);

// LUT config result: BIT2
  if (((conf_err>>2)&0x1)!=0)
    printf("%sCCBport %d - LUT config: error%s\n",REDBKG,ccbport,STDBKG);
  else if (((conf_status>>2)&0x1)==0) // configured!
    printf("%sCCBport %d - LUT config: ok%s\n",GRNBKG,ccbport,STDBKG);

// TSS config result: BIT3
  if (((conf_err>>3)&0x1)!=0)
    printf("%sCCBport %d - TSS config: error%s\n",REDBKG,ccbport,STDBKG);
  else if (((conf_status>>3)&0x1)==0) // configured!
    printf("%sCCBport %d - TSS config: ok%s\n",GRNBKG,ccbport,STDBKG);

// TSM config result: BIT4
  if (((conf_err>>4)&0x1)!=0)
    printf("%sCCBport %d - TSM config: error%s\n",REDBKG,ccbport,STDBKG);
  else if (((conf_status>>4)&0x1)==0) // configured!
    printf("%sCCBport %d - TSM config: ok%s\n",GRNBKG,ccbport,STDBKG);

// TDC config result: BIT5
  if (((conf_err>>5)&0x1)!=0)
    printf("%sCCBport %d - TDC config: error%s\n",REDBKG,ccbport,STDBKG);
  else if (((conf_status>>5)&0x1)==0) // configured!
    printf("%sCCBport %d - TDC config: ok%s\n",GRNBKG,ccbport,STDBKG);

// ROB config result: BIT6
  if (((conf_err>>6)&0x1)!=0)
    printf("%sCCBport %d - ROB config: error%s\n",REDBKG,ccbport,STDBKG);
  else if (((conf_status>>6)&0x1)==0) // configured!
    printf("%sCCBport %d - ROB config: ok%s\n",GRNBKG,ccbport,STDBKG);

// FE config result:BIT7
  if (((conf_err>>7)&0x1)!=0)
    printf("%sCCBport %d - FE config: error%s\n",REDBKG,ccbport,STDBKG);
  else if (((conf_status>>7)&0x1)==0) // configured!
    printf("%sCCBport %d - FE config: ok%s\n",GRNBKG,ccbport,STDBKG);

// Other configs: BIT8
  if (((conf_status>>8)&0x1)==0) // configured!
    printf("%sCCBport %d - Other config: done%s\n",GRNBKG,ccbport,STDBKG);

// CRC
  printf("%sCCBport %d - configuration CRC: %#hX%s\n",BLUBKG,ccbport,conf_crc,STDBKG);

}


void ctype_list() {
  printf("1.  ALL\n");
  printf("2.  BTI\n");
  printf("3.  TRACO\n");
  printf("4.  LUT\n");
  printf("5.  TSS\n");
  printf("6.  TSM\n");
  printf("7.  TDC\n");
  printf("8.  ROB\n");
  printf("9.  FE\n");
  printf("10. OTHER\n");
}
