#include <ccbcmds/apfunctions.h>
#include <ccbcmds/Clog.h>
#include <ccbcmds/Ccommand.h>
#include <ccbcmds/Cccb.h>
#include <ccbcmds/Cdef.h>


#include <iostream>
#include <stdio.h>

// Author: A.Parenti, Jul01 2006
// Modified: AP, Aug30 2007
int main(int argc, char *argv[]) {
  bool mc_program;
  char message[1024];
  short pippo, ccbport, ccbid;
  Emcstatus mc_phys_st, mc_logic_st;

  Clog *myLog;
  Ccommand *myCmd;
  Cccb *myCcb;
  Cref *myRef;
  char mccbsrv[64];
  int errcod;
  char *errstr;
  unsigned char mycmd;
 unsigned short optoid;

  if (argc>=3) {
    sscanf(argv[1],"%x",&pippo);
    mycmd=pippo;
    sscanf(argv[2],"%d",&pippo);
    ccbport=pippo;

    if (argc==4) sscanf(argv[3],"%s",&mccbsrv);
    else strcpy(mccbsrv,"127.0.0.1");
  } else {
    printf("Usage: ccb_debug cmd port [host]\n");

   exit (-1);
  }

#ifdef LOGTODB
  myLog = new Clog(); // Log to DB
#else
  myLog = new Clog("log.txt"); // Log to file
#endif
  //myCmd = new Ccommand(1,ccbport,mccbsrv,CCBPORT,myLog);
  //optoid = 0xff00+ccbport;
  myCmd = new Ccommand(ccbport,mccbsrv,18885,myLog);

  printf("Using server %s:%d\n",myCmd->read_server_name(),myCmd->read_server_port());
  printf("cmd  %x\n",mycmd);
  printf("port  %x\n",ccbport);

  printf("*** Actual time: %s\n",get_time());

  unsigned char cmdline[256];
  cmdline[0]=mycmd;
  //myCmd->use_secondary_port();
  errcod=myCmd->send_command(cmdline,1,2000);
  errstr=myCmd->read_error_message(errcod);

   
   printf("CCB reply:");
  for (int i=0; i<(myCmd->data_read_len); ++i)
    printf(" %02X",myCmd->data_read[i]);
  putchar('\n');
  printf("Error code: %s\n",errstr);



}

