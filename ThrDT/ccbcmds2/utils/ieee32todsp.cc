#include <ccbcmds/apfunctions.h>
#include <iostream>
#include <cstdio>

// Author: A.Parenti, Jul31 2006
// Modified: AP, Aug01 2006
int main(int argc, char *argv[]) {
  unsigned char array[4];
  float ieee32_float;

  if (argc < 2) {
    printf("*** Usage: ieee32todsp [ieee32_float]\n");

    printf("IEEE32 float? ");
    scanf("%f",&ieee32_float);
  }
  else {
    sscanf(argv[1],"%f",&ieee32_float);
  }

  host_to_ccb(ieee32_float,array);

  printf("Conversion to DSP: %f ---> %02hhx %02hhx %02hhx %02hhx (HEX)\n",ieee32_float,array[0],array[1],array[2],array[3]);

}
