#include <iostream>
#include <stdio.h>

#ifndef __USE_CCBDB__ // No DB access
int main() {
  std::cout << "Nothing done. Please compile with DB support.\n";
}
#else

#include <ccbdb/Ccmsdtdb.h>

#include <ccbcmds/apfunctions.h>

// Author: A. Parenti, Apr16 2007
// Modified: AP, May15 2007 (Tested on MySQL/Oracle)
int main(int argc, char *argv[]) {
  int oldID, newID;
  int i, rows=0, fields=0, resu=0;
  char mquer[LEN2];
  char tname[8][32]={"ccbmap","ccbgeom","ccbstatus","robstatus","ccbdata","refrelations","ccbrelations","dtrelations"};
  cmsdtdb mydtdtb;


  printf("*** This program update the ccbID in all tables.\n");

  if (argc != 3) {
    printf("*** Usage: %s [oldID] [newID]\n",argv[0]);

    printf("Old ccbID: ");
    scanf("%d",&oldID);

    printf("New ccbID: ");
    scanf("%d",&newID);

  } else {
    sscanf(argv[1],"%d",&oldID);
    sscanf(argv[2],"%d",&newID);
  }

  for (i=0;i<8;++i) {
    sprintf(mquer,"UPDATE %s SET ccbid=%d WHERE ccbid=%d",tname[i],newID,oldID);
    resu = mydtdtb.sqlquery(mquer,&rows,&fields);
//    printf("%s\n",mquer);

    if (resu!=0) {
      printf("Error in updating table %s. Quitting...\n",tname[i]);
      return resu;
    }
  }

  printf("All Done!\n");
  return 0;
}

#endif
