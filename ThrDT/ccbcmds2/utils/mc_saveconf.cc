#include <ccbcmds/apfunctions.h>
#include <ccbcmds/Clog.h>
#include <ccbcmds/Ccommand.h>
#include <ccbcmds/Cccb.h>
#include <ccbcmds/Cdef.h>

#include <iostream>
#include <stdio.h>

// Author: S.Ventura, Jul13 2006
// Modified: A.Parenti, Jan26 2007
int main(int argc, char *argv[]) {
  short ccbport;
  Clog *myLog;
  Ccommand *myCmd;
  Cccb *myCcb;

  if (argc < 2) {
    printf("*** Usage: mc_saveconf [ccbport]\n");

    printf("ccbport? ");
    scanf("%d",&ccbport);
  }  else
    sscanf(argv[1],"%d",&ccbport);

#ifdef LOGTODB
  myLog = new Clog(); // Log to DB
#else
  myLog = new Clog("log.txt"); // Log to file
#endif
  myCmd = new Ccommand(ccbport,CCBSERVER,CCBPORT,myLog);
  myCcb = new Cccb(myCmd); // New CCB object

  myCmd->set_write_log(true); // Switch on logger

  printf("Using server %s:%d\n",myCmd->read_server_name(),myCmd->read_server_port());

  printf("\n\n\n");
  printf("*** save conf ccbport: %d\n",ccbport);
  printf("*** Actual time: %s\n",get_time());
  myCcb->save_MC_conf(); // 0x40
  myCcb->save_MC_conf_print();
}
