#include <iostream>
#include <stdio.h>

#ifndef __USE_CCBDB__ // No DB access
int main() {
  std::cout << "Nothing done. Please compile with DB support.\n";
}
#else

#include <ccbdb/Cccbmap.h>

// Retrieve chamber info from DB
// Author: A.Parenti, Jul06 2006
// Modified: AP, Jul16 2007

int main(int argc, char *argv[]) {
  ccbmap myccbmap;

  if (argc==2) {
    short ccbid;
    sscanf(argv[1],"%d",&ccbid);
    
    if (myccbmap.retrieve(ccbid)!=0) {
      printf("MC not found in DB.\n");
      return -1;
    }
  }

  else if (argc==4) {
    int wheel, sector, station;

    sscanf(argv[1],"%d",&wheel);
    sscanf(argv[2],"%d",&sector);
    sscanf(argv[3],"%d",&station);
    if (myccbmap.retrieve(wheel,sector,station)!=0) {
      printf("MC not found in DB.\n");
      return -1;
    }
  }

  else {
    printf("Usage: mc_map <ccbid>\n");
    printf("Usage: mc_map <wheel> <sector> <station>\n");
    return 0;
  }

  printf("ccbid: %d\n",myccbmap.mId);
  printf("wheel: %d\n",myccbmap.wheel);
  printf("sector: %d\n",myccbmap.sector);
  printf("station: %d\n",myccbmap.station);
  printf("port: %d\n",myccbmap.port);
  printf("online: %s\n",(myccbmap.online==1)?"yes":"no");
  printf("comment: %s\n",myccbmap.comment);

}
  
#endif
