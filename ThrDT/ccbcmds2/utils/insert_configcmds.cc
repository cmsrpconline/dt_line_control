#include <iostream>
#include <stdio.h>

#ifndef __USE_CCBDB__ // No DB access
int main() {
  std::cout << "Nothing done. Please compile with DB support.\n";
}
#else

#include <ccbdb/Ccmsdtdb.h>
#include <ccbdb/Cccbconfdb.h>

#include <ccbcmds/Cconf.h>
#include <ccbcmds/apfunctions.h>


// Author: A. Parenti, Mar23 2006
// Modified: AP, Mar08 2007
int main(int argc, char *argv[]) {
  unsigned char cmd_line[CMD_MAXLINES][CMD_LINE_DIM], *tmpline;
  char filename[LEN1]="", BrickName[LEN1]="", c;
  int i, lines, linesok, line_len[CMD_MAXLINES];
  int BrkType;

  if (argc != 4) {
    printf("*** This program insert commands in the configcmds table.\n");
    printf("*** Usage: %s [filename] [BrickName] [BrickType]\n",argv[0]);

    printf("Filename? ");
    while ((c=getchar()) != '\n')
      sprintf(filename,"%s%c",filename,c);
    printf("BrickName? ");
    while ((c=getchar()) != '\n')
      sprintf(BrickName,"%s%c",BrickName,c);
    printf("Brick Types:\n  0. brk_other\n  1. brk_bti_phi\n  2. brk_bti_theta\n  3. brk_traco\n  4. brk_traco_luts\n  5. brk_tss\n  6. brk_tsm\n  7. brk_rob\n  8. brk_feb_thr\n  9. brk_feb_width\n  10. brk_feb_mask\n");
    printf("BrickType? ");
    scanf("%d",&BrkType);
  } else {
    strcpy(filename,argv[1]);
    strcpy(BrickName,argv[2]);
    sscanf(argv[3],"%d",&BrkType);
  }

  tmpline = new unsigned char[CMD_LINE_DIM];

  Cconf myconf(filename);
  lines = myconf.read_conf((short)0,(unsigned char)0,(char)-1,(char)-1,cmd_line,line_len); // Read all command lines from file
  printf("%d lines read from file %s\n",lines,filename);

  ccbconfdb newConf; // API to database. Datasource from /etc/ccbdb.conf
  printf("Writing to DB...\n");
  for (i=linesok=0;i<lines;++i) { // Loop over lines

    delete [] tmpline; // free tmpline[]
    tmpline = new unsigned char[line_len[i]+CCB_HDR_LEN]; // Reallocate space for tmpline

    myconf.make_ccb_header(line_len[i],tmpline); // Create ccb header
    mystrcpy(tmpline+CCB_HDR_LEN,cmd_line[i],line_len[i]); // Append command line to header

    if (newConf.cmdinsert(BrickName,(char *)tmpline,(Ebrktype)BrkType)==0) // Write the line to the DB
      linesok++; // The line has been succesfully written
  }

  printf("%d lines written to <configcmds>\n",linesok);

  return 0;
}

#endif
