#include <ccbcmds/apfunctions.h>
#include <ccbcmds/Ccommand.h>
#include <ccbcmds/Cdef.h>

#include <ccbcmds/Cbti.h>
#include <ccbcmds/Ctraco.h>
#include <ccbcmds/Ctss.h>
#include <ccbcmds/Ctsm.h>
#include <ccbcmds/Ctdc.h>
#include <ccbcmds/Cfe.h>

#include <cstdio>

// Author: A. Parenti, Oct27 2006
// Modified: AP, Jan26 2007
int main(int argc, char *argv[]) {
  char fname[100];
  short ccbport;
  Clog *myLog;
  Ccommand *myCmd;

  if (argc < 2) {
    printf("*** Usage: mc_dump_conf [ccbport]\n");

    printf("ccbport? ");
    scanf("%d",&ccbport);
  }  else {
    sscanf(argv[1],"%d",&ccbport);
  }

  sprintf(fname,"mc_conf_%d.txt",ccbport);

  myLog = new Clog("log.txt"); // Log to file
  myCmd = new Ccommand(ccbport,CCBSERVER,CCBPORT,myLog);
  Cbti* myBti = new Cbti(myCmd); // New BTI object
  Ctraco* myTraco = new Ctraco(myCmd); // New TRACO object
  Ctss* myTss = new Ctss(myCmd); // New TSS object
  Ctsm* myTsm = new Ctsm(myCmd); // New TSM object
  Ctdc* myTdc = new Ctdc(myCmd); // New TDC object
  Cfe* myFe = new Cfe(myCmd); // New FE object

  printf("Using server %s:%d\n",myCmd->read_server_name(),myCmd->read_server_port());

  printf("\n\n\n");
  printf("*** Dump MC configuration to file***\n");
  printf("*** Actual time: %s\n",get_time());

  myBti->read_BTI_config(fname);
  myTraco->read_TRACO_config(fname);
  myTss->read_TSS_config(fname);
  myTsm->read_TSM_config(fname);
  myTdc->read_TDC_config(fname);
  myFe->read_FE_config(fname);

}
