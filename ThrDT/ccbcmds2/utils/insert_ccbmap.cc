#include <iostream>
#include <stdio.h>

#ifndef __USE_CCBDB__ // No DB access
int main() {
  std::cout << "Nothing done. Please compile with DB support.\n";
}
#else

#include <ccbdb/Cccbmap.h>
#include <ccbcmds/apfunctions.h>

// Author: A. Parenti, Jan30 2006
// Tested on MySQL/Oracle
// Modified: AP, Oct10 2007
int main(int argc, char *argv[]) {
  ccbmap myccbmap;

  char filename[LEN1]="", content[LEN2]="", c;
  char ccbserver[255];
  int ccbid,wheel,sector,station,chamber,minicrate,on_line,port,secport;
  int bootrefid,statusrefid,testrefid;
  std::string cmnt="";

  printf("*** This program insert an entry in the ccbmap table.\n");
  printf("*** Usage: %s [filename]\n",argv[0]);

  if (argc != 2) {
    printf("Structure of the file must be: \"(int)ccbid  (int)wheel (int)sector (int)station (int)chamber (int)minicrate (int)on_line (int)port (int) sec_port (char*)ccbserver (int)bootrefid (int)statusrefid (int)testrefid\"\n");

    printf("Filename? ");
    while ((c=getchar()) != '\n')
      sprintf(filename,"%s%c",filename,c);

  } else
    strcpy(filename,argv[1]);

  strcpy(content,read_file(filename));
  if (strlen(content)==0) {
    printf("File not existing or empty.\n");
    return -1;
  }

  printf("Do you want to add a comment? ");
  do {
    cmnt += (c=getchar());
  } while (c != '\n');

//  cmnt.replace(cmnt.length()-1,1,"\0");

//  printf("Filename: '%s'\n",filename);
//  printf("File content: '%s'\n",content);
//  printf("Comment: '%s'\n",cmnt.c_str());

  sscanf(content,"%d %d %d %d %d %d %d %d %d %s %d %d %d",&ccbid,&wheel,&sector,&station,&chamber,&minicrate,&on_line,&port,&secport,ccbserver,&bootrefid,&statusrefid,&testrefid);

  if (myccbmap.insert(ccbid,wheel,sector,station,chamber,minicrate,on_line,port,secport,ccbserver,(char*)cmnt.c_str())!=0) return -1;
  if (myccbmap.setrefs(ccbid,bootrefid,statusrefid,testrefid)!=0) return -1;

  printf("Done!\n");
  return 0;
}

#endif
