#! /bin/tcsh -f

set quotes = '"'
set str = "refstr"
set oggetto = "ref_test"
set lista = (Dac SbTestJtag SbTestPi TrbPwr TrbBadJtagAddr RobPwr RobBadJtagAddr RobOverlapAddr McType)
# Array dimensions
set len = ()

# This is for variables

foreach nome (${lista})
#  set stringa = "pch=read_from_str(pch,&"${oggetto}"->"${nome}");"
  set stringa = "read_from_xml(${str},${quotes}${nome}${quotes},&${oggetto}.${nome});"
  echo ${stringa} >> prova.txt
end

# This is for arrays

#@ iii=0
#while ($iii<$#lista)
#  @ iii++
#  set stringa = "read_from_xml(${str},${quotes}${lista[$iii]}${quotes},${len[$iii]},${oggetto}.${lista[$iii]});"
#  echo ${stringa} >> prova.txt
#end
