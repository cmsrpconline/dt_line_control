#! /bin/tcsh -f

#set obj1 = "bst"
set obj1 = "tst"
#set obj2 = "boot_status"
set obj2 = "mc_test"
set lista = (RobPwr RobBadJtagAddr RobOverlapAddr RobPresMsk RobTestJtag RobFindSensor RobOnTime)

foreach nome (${lista})
  set stringa = "if (${obj2}.${nome}<Pcref->m${obj1}->${nome} || ${obj2}.${nome}>Pcref->M${obj1}->${nome})"
  echo ${stringa} >> prova.txt
  echo "*Psb_ok = false;"  >> prova.txt
end
