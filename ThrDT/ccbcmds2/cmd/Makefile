# Makefile for ccbcmds package.
# Author: A.Parenti, Feb21 2005

# Modifications
# Jun21 2005: Added support for Sandro's DB
# Aug23 2005: Added "install" and "uninstall" flags
# Jul14 2006: Compile various utilities
# Mar02 2007: ptypes and ccbdb/odbc can be disabled
# Jun01 2007: added "-lptypes" in making shared library, to avoid "undefined symbols"

# Uncomment next line to disable ptypes (thus TCP/IP)
#disable_ptypes := 'yes'
export disable_ptypes # export this variable to child processes

# Uncomment next line to disable ccbdb (thus DB access via ODBC)
#disable_ccbdb  := 'yes'
export disable_ccbdb # export this variable to child processes

SHELL = /bin/sh

# base path
base_dir  := $(shell cd ..; pwd)

# list of src, inc, obj files
src_files := $(wildcard ${base_dir}/src/*.cc)
inc_files := $(wildcard ${base_dir}/inc/*.h)
obj_files := $(subst src,obj,$(patsubst %.cc,%.o,${src_files}))

# library name and directory
lib_name  := ccbcmds
lib_dir   := ${base_dir}/lib
# library file
sh_lib_file  := ${lib_dir}/lib${lib_name}.so

# install/uninstall variables
prefix  = /usr
libdir  = ${prefix}/lib
incdir  = ${prefix}/include/${lib_name}
version = 2.9.1

# compiler/linker options
LIBS := -L${base_dir}/lib -L${base_dir}/ccbdb -L/usr/lib
LIBS += -l${lib_name} #		ccbcmds library
LIBS += -lpthread #		pthreads
LIBS += -lxml2 #		libxml2
INCS := -I${base_dir}/inc # my includes
INCS += -I/usr/include/libxml2/ # libxml2 includes
SHLIBS = -lpthread -lxml2 # Needed when linking shared lib, otherwise we got undefined symbols

ifeq (${disable_ptypes},) # ptypes enabled (thus TCP/IP)
  LIBS += -L${base_dir}/lib -lptypes
  INCS += -I/usr/include/ptypes/
  CPPFLAGS  += -D'__USE_PTYPES__'
  SHLIBS +=  -L${base_dir}/lib -lptypes
endif

ifeq (${disable_ccbdb},) # ccbdb enabled (thus DB access via ODBC)
  LIBS += -L${base_dir}/ccbdb -lccbdb -lodbc
  CPPFLAGS  += -D'__USE_CCBDB__'
  SHLIBS += -L${base_dir}/ccbdb -lccbdb -lodbc
endif

default:	lib

		@echo ""
		@echo "Makefile version: Aug07 2007"
		@echo ""
		@echo "Syntax:"
		@echo "make lib -------> make libraries (default)"
		@echo "make install ---> install libraries and headers"
		@echo "make uninstall -> uninstall libraries and headers"
		@echo "make testexe----> build exe from testsrc/main.cc"
		@echo "make utils -----> build some utilities"
		@echo "make clean -----> delete object and *~ files"
		@echo "make cleanall --> delete also libs and exe"

clean:
		@echo ""
		@echo "-> Making clean"
		$(RM) ../obj/*.o
		$(RM) ../src/*~
		$(RM) ../testsrc/*~
		$(RM) ../utils/*~
		$(RM) ../inc/*~
		$(RM) ../exe/core*

cleanall:	clean
		$(RM) ../lib/*
		$(RM) ../exe/main
		$(RM) ../exe/dbquery
		$(RM) ../exe/insert_*
		$(RM) ../exe/update_*
		$(RM) ../exe/mc_*
		$(RM) ../exe/dt_*
		$(RM) ../exe/ttc_fine_delay
		$(RM) ../exe/dsptoieee32
		$(RM) ../exe/ieee32todsp

obj:		${src_files}
		@echo ""
		@echo "-> Making object files"
		@ cd ${base_dir}/obj; make

${obj_files}:	obj

${sh_lib_file}:	${obj_files}
		@echo ""
		@echo "-> Making library"
		cd ${base_dir}/lib
		${CXX} -fPIC -shared -o ${sh_lib_file} ${obj_files} ${SHLIBS}

lib:		${sh_lib_file}


install:	lib
		@echo ""
		@echo "-> Installing library and header files"
		mkdir -p ${libdir}

		cp -p ${sh_lib_file} ${libdir}/lib${lib_name}.so.${version}
		chown root.root ${libdir}/lib${lib_name}.so.${version}
		chmod 755 ${libdir}/lib${lib_name}.so.${version}
		ln -fs ${libdir}/lib${lib_name}.so.${version} ${libdir}/lib${lib_name}.so

		mkdir -p ${incdir}
		cp -p ${base_dir}/inc/*.h ${incdir}
		chown root.root ${incdir}/*

uninstall:
		@echo ""
		@echo "-> Uninstalling library and header files"
		$(RM) ${libdir}/lib${lib_name}.so
		$(RM) ${libdir}/lib${lib_name}.so.${version}
		-rmdir --ignore-fail-on-non-empty ${libdir}
		$(RM) ${incdir}/*
		-rmdir -p --ignore-fail-on-non-empty ${incdir}

testexe:	lib ${base_dir}/testsrc/main.cc
		@echo ""
		@echo "-> Making exe"
		${CXX} ${INCS} -g ${CPPFLAGS} -o ${base_dir}/exe/main ${base_dir}/testsrc/main.cc ${LIBS}

utils:		lib

		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/dbquery ${base_dir}/utils/dbquery.cc ${LIBS}
		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/insert_ccbmap ${base_dir}/utils/insert_ccbmap.cc ${LIBS}
		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/insert_ccbref ${base_dir}/utils/insert_ccbref.cc ${LIBS}
		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/insert_configcmds ${base_dir}/utils/insert_configcmds.cc ${LIBS}
		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/insert_dtpartition ${base_dir}/utils/insert_dtpartition.cc ${LIBS}
		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/update_ccbid ${base_dir}/utils/update_ccbid.cc ${LIBS}

		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/dsptoieee32 ${base_dir}/utils/dsptoieee32.cc ${LIBS}
		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/ieee32todsp ${base_dir}/utils/ieee32todsp.cc ${LIBS}

		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/dt_checkconf ${base_dir}/utils/dt_checkconf.cc ${LIBS}
		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/dt_config ${base_dir}/utils/dt_config.cc ${LIBS}
		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/dt_monitor ${base_dir}/utils/dt_monitor.cc ${LIBS}
		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/dt_monitor_noDB ${base_dir}/utils/dt_monitor_noDB.cc ${LIBS}
		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/dt_robreset ${base_dir}/utils/dt_robreset.cc ${LIBS}
		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/dt_run_in_progress ${base_dir}/utils/dt_run_in_progress.cc ${LIBS}
		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/dt_temp ${base_dir}/utils/dt_temp.cc ${LIBS}

		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/mc_autosetlink ${base_dir}/utils/mc_autosetlink.cc ${LIBS}
		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/mc_checktdc ${base_dir}/utils/mc_checktdc.cc ${LIBS}
		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/mc_config ${base_dir}/utils/mc_config.cc ${LIBS}
		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/mc_dump_config ${base_dir}/utils/mc_dump_config.cc ${LIBS}
		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/mc_generic_command ${base_dir}/utils/mc_generic_command.cc ${LIBS}
		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/mc_loadconf ${base_dir}/utils/mc_loadconf.cc ${LIBS}
		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/mc_map ${base_dir}/utils/mc_map.cc ${LIBS}
		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/mc_mask_fe_channel ${base_dir}/utils/mc_mask_fe_channel.cc ${LIBS}
		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/mc_padc_monitor ${base_dir}/utils/mc_padc_monitor.cc ${LIBS}
		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/mc_pwr ${base_dir}/utils/mc_pwr.cc ${LIBS}
		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/mc_readroberror ${base_dir}/utils/mc_readroberror.cc ${LIBS}
		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/mc_restart ${base_dir}/utils/mc_restart.cc ${LIBS}
		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/mc_restoreconf ${base_dir}/utils/mc_restoreconf.cc ${LIBS}
		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/mc_robreset ${base_dir}/utils/mc_robreset.cc ${LIBS}
		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/mc_saveconf ${base_dir}/utils/mc_saveconf.cc ${LIBS}
		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/mc_status ${base_dir}/utils/mc_status.cc ${LIBS}
		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/mc_stop_on_boot ${base_dir}/utils/mc_stop_on_boot.cc ${LIBS}
		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/mc_temp ${base_dir}/utils/mc_temp.cc ${LIBS}
		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/mc_test ${base_dir}/utils/mc_test.cc ${LIBS}
		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/mc_threshold ${base_dir}/utils/mc_threshold.cc ${LIBS}
		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/opto_status ${base_dir}/utils/opto_status.cc ${LIBS}
		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/ccb_debug  ${base_dir}/utils/ccb_debug.cc ${LIBS}
		${CXX} ${INCS} ${CPPFLAGS} -o ${base_dir}/exe/ttc_fine_delay ${base_dir}/utils/ttc_fine_delay.cc ${LIBS}
