#! /bin/tcsh -f
# Create some lines of "sscanf" and append them to prova.txt

set lista= (Vccin Vddin Vcc Vdd Tp1L Tp1H Tp2L Tp2H)
set oggetto = "bst->"
set tipo = f
set strutt = boot_status
set virgolette='"'

foreach nome (${lista})
  set stringa1="pch=strchr(pch,' ');\nsscanf(pch,${virgolette}%${tipo}${virgolette},&${oggetto}${nome});\nprintf(${virgolette}%${tipo} ${virgolette},${oggetto}${nome});"
  echo ${stringa1} >> prova.txt
end
