#!/bin/tcsh -f

set virg='"'
set cr='\\n'
set tipo='.2f'
#set tipo='u'
#set tipo='#2X'
set struct=mc_test
set nomi1=""
set nomi2=""

# Lista delle variabili
#set nomi1=(code TTCFpga AnPwr CKPwr LedPwr RpcPwr BufTrbPwr VccTrbPwr B1w SOPwr DUPwr DDPwr SBCKPwr THPwr CPUdelay TPFineDelay1 TPFineDelay2)
#set nomi2=(OnTime)
#set nomi2=(AdcNoise)
#set nomi1=(Dac SbTestJtag SbTestPi TrbPwr TrbBadJtagAddr)
#set nomi2=(TrbPresMsk TrbTestJtag TrbFindSensor TrbOnTime)
#set nomi2=TrbPiTest
#set nomi1=(RobPwr RobBadJtagAddr RobOverlapAddr)
#set nomi2=(RobPresMsk RobTestJtag RobFindSensor RobOnTime)
#set nomi1=(McType Abort_Vccin Abort_Vddin Vfofftest nTrbBrd)
set nomi1=(Vfoff Vtrbmin Vtrbmax)

@ i=1
while ($i <= $#nomi1)
  echo sprintf\(tmpstr,\"\ \ \<${nomi1[$i]}\>%${tipo}\</${nomi1[$i]}\>${cr}\",${struct}.${nomi1[$i]}\)\; ${struct}.xmlmsg+=tmpstr\;
  @ i++
end


@ i=1

echo "for (i=0;i<3;++i) {"
while ($i <= $#nomi2)
  echo sprintf\(tmpstr,\"  \<${nomi2[$i]} idx=\\${virg}%d\\${virg}\>%${tipo}\</${nomi2[$i]}\>${cr}\",i,${struct}.${nomi2[$i]}\[i\]\)\; ${struct}.xmlmsg+=tmpstr\;
  @ i++
end
echo "}"
