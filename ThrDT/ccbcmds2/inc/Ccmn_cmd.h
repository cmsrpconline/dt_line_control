// Here are defined ccb commands common to all objects

// include Ccmn_cmd.h only once
#ifndef __CCMN_CMD_H__
#define __CCMN_CMD_H__

#include <ccbcmds/Ccommand.h> // defines Ccommand class
#include <ccbcmds/Cconf.h>    // configurations class
#include <ccbcmds/Cdef.h>     // Some definitions

#define COMM_ERRS_MAX  4      // Max number of communication errors
                              // before aborting configuration
// Used in configuration
#define WRITE_BTI_TIMEOUT         15000
#define ENABLE_BTI_TIMEOUT        15000
#define WRITE_TRACO_TIMEOUT       15000
#define WRITE_TRACO_LUTS_TIMEOUT  15000
#define WRITE_MC_LUTS_TIMEOUT     80000
#define PRELOAD_LUTS_TIMEOUT      15000
#define LOAD_LUTS_TIMEOUT         15000
#define WRITE_TSS_TIMEOUT         15000
#define WRITE_TSM_TIMEOUT         15000
#define TRGOUT_SELECT_TIMEOUT     15000
#define WRITE_TDC_TIMEOUT         15000
#define WRITE_TDC_CONTROL_TIMEOUT 15000
#define RESET_ROB_TIMEOUT         15000
#define SET_CPU_CK_DELAY_TIMEOUT  15000
#define SET_FE_THR_TIMEOUT        15000
#define SET_FE_WIDTH_TIMEOUT      15000
#define SET_FE_MASK_TIMEOUT       15000
#define SET_FE_TMASK_TIMEOUT      15000

#define READ_TDC_TIMEOUT         5000
#define READ_ROB_ERROR_TIMEOUT   5000

// default timeouts
#define READ_TEST_TIMEOUT       15000
#define RUN_TEST_TIMEOUT       300000
#define MC_STATUS_TIMEOUT        2000
#define MC_TEMP_TIMEOUT          5000
#define PWR_ON_TIMEOUT          10000
#define CLEAR_V_MONITOR_TIMEOUT 15000
#define GETCONFIGCRC_TIMEOUT    15000
#define CHAMBER_MAP_TIMEOUT      5000


// Configuration Types
enum ConfType { CBTI, CTRACO, CLUT, CTSS, CTSM, CTDC, CROB, CFE, COTHER, CALL};

// Class used to store CCB configuration results
class confcmd {
 public:
  unsigned short len, sent, bad; // expected_reply_length, send and bad commands

// class constructor and destructor
// Input: code, expected_reply, len
  confcmd(unsigned char, unsigned char*, unsigned short);
  ~confcmd();

// Add last command to sent (and in case to bad) counter.
  void add(unsigned char*); // Input: data_read
  void add(bool); // Input: error

// Reset counters
  void reset();

// Print results
  void print();

  unsigned char code, *expected_reply; // command code and expected reply
};

/*****************************************************************************/
// CMN_CMD data structures
/*****************************************************************************/

// Format of return data from "read/run test results" (0x10/0x12) command
// (MINICRATE program v1.21)
struct Smc_test {
  unsigned char code;
  bool TTCFpga, AnPwr, CKPwr, LedPwr, RpcPwr, BufTrbPwr, VccTrbPwr, B1w, SOPwr,
    DUPwr, DDPwr, SBCKPwr, THPwr, CPUdelay, TPFineDelay1, TPFineDelay2;
  unsigned short OnTime[11], AdcNoise[32], Dac, SbTestJtag, SbTestPi;
  unsigned char TrbPwr, TrbBadJtagAddr;
  unsigned short TrbPresMsk[8], TrbTestJtag[8];
  unsigned char TrbFindSensor[8];
  unsigned short TrbOnTime[8], TrbPiTest[6];
  unsigned char RobPwr, RobBadJtagAddr, RobOverlapAddr;
  unsigned short RobPresMsk[7], RobTestJtag[7];
  unsigned char RobFindSensor[7];
  unsigned short RobOnTime[7];
  unsigned short McType;
// From here, only if firmware >= 1.11
  unsigned char Abort_Vccin, Abort_Vddin;
// From here, only if firmware >= 1.17
  bool Vfofftest;
  unsigned char nTrbBrd;
  float Vfoff, Vtrbmin, Vtrbmax;

// ccbserver code
  int ccbserver_code;
// print-out of results
  std::string msg;
  std::string xmlmsg;
};

// Format of return data from "minicrate status" (0xEA) command
// (MINICRATE program v1.21)
struct Smc_status {
  unsigned char code;
  unsigned short Ccb_ID, HVersion, LVersion, McType;
  bool PwrAn, PwrCK, PwrLed, PwrRpc, PwrTrbBuf, PwrTrbVcc, PwrSO, PwrDU,
    PwrDD, PwrSBCK, PwrTH, TTCrdy, PwrFlash, EnTtcCkMux, QpllARdy, QpllBRdy;
  unsigned char PwrTrb, PwrRob;
  bool AlrmPwrAn, AlrmPwrCK, AlrmPwrLed, AlrmPwrRpc, AlrmPwrTrbBuf,
    AlrmPwrTrbVcc, AlrmPwrSO, AlrmPwrDU, AlrmPwrDD, AlrmPwrSBCK, AlrmPwrTH,
    AlrmTTCrdy, AlrmPwrFlash, AlrmEnTtcCkMux, AlrmQpllAChng, AlrmQpllBChng;
  unsigned char AlrmPwrTrb, AlrmPwrRob, AlrmTempTrb, AlrmTempRob;
  unsigned char LoseLockCount; // Firmware < 1.7
  unsigned char LoseLockCountTTC, LoseLockCountQPLL1, LoseLockCountQPLL2,
    LoseLockCountUnused; // Firmware >= 1.7
  unsigned int RamAddr, SeuRam;
  unsigned short IntRamAddr;
  unsigned int SeuIntRam, SeuBTI, SeuTRACO, SeuLUT, SeuTSS;
  float Vccin, Vddin, Vcc, Vdd, Tp1L, Tp1H, Tp2L, Tp2H, Fe_Vcc[3], Fe_Vdd[3],
    Sp_Vcc, Sp_Vdd, Fe_Bias[3], Fe_Thr[3], Fe_Width;
  short in_Tmax, in_Tmed, th_Tmax, th_Tmed, out_Tmax, out_Tmed;
  float BrdMaxtemp;
  unsigned char CpuCkDelay;
  bool SelQPLL1, SelQPLL2;
// From here, only if firmware >= 1.6
  bool EnTrgPhi, EnTrgThe, EnTrgH, DisTrbCk, DisSbCk,
    DisOsc;
  char  L1A_Delay:7;
  bool EnAutoTrg, Sel1AVeto, ForceTp, CCBReady;
// From here, only if firmware >= 1.7
  bool RunInProgress;
// From here, only if firmware >= 1.11
  float Vccin_min, Vccin_max, Vddin_min, Vddin_max, Sb_Vcc_min, Sb_Vcc_max,
    Sb_Vdd_min, Sb_Vdd_max;
// From here, only if firmware >= 1.16
  bool CfgNotChanged;
// From here, only if firmware >= 1.17
  bool CfgLoaded, InvalidArg;
// From here, only if firmware >= 1.20
  short ChamberMap;
  short CfgLoadResult;

// ccbserver code
  int ccbserver_code;
// Raw data
  unsigned char rawstatus[255], rawstatus_len;
// print-out of results
  std::string msg;
  std::string xmlmsg;
};


// Format of return data from "minicrate status" (0xEA) command
// (BOOT program v3.9)
struct Sboot_status {
  unsigned char code, rsr;
  unsigned short Ccb_ID, HVersion, LVersion;
  bool EUartIRQ, TTCi2c, CpldTtc, QpllArdy, QpllBrdy, QpllAChng, QpllBChng,
    TtcCk, IUart, EUartA, EUartB, ExtRam, Flash, AutoRun, TTCrxRdy,
    LinkAutoset;
  unsigned short PwrRam, ErrRam, vErrRam, PwrFlash, FlashID, LinkOfs, LinkHyst, LinkAmp,
    LinkThr;
  unsigned int SEUCounter, RefreshAddr, PwrCrashFlags;
  unsigned short PwrCrashCrc, CrashFlags, CrashCrc, PwrAnalog;
  float Vccin, Vddin, Vcc, Vdd, Tp1H, Tp1L, Tp2H, Tp2L;

// ccbserver code
  int ccbserver_code;
// print-out of results
  std::string msg;
  std::string xmlmsg;
};

// Format of return data from "minicrate temperature" (0x3C) command
struct Smc_temp {
  unsigned char code;
  float temp[21];
  int ccbserver_code;
  std::string msg;
  std::string xmlmsg;
};

// Format of return data from "Power on/off" (0x2F) command,
// also "Chamber Map" (0x4B)
struct Scmn_1 {
  unsigned char code1, code2, result;
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "Clear min max voltage monitor" (0x9E) command
struct Sclear_v_monitor {
  unsigned char code1, code2;
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "read TDC" (0x43) command
struct Sread_TDC {
  unsigned char code1, setup[81], control[5], status[8], idcode[4];
  unsigned char code2;
  char narg;
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "Read ROB error" (0x33) command
struct Sread_ROB_error {
  unsigned char code, state[7];
  unsigned char status; // Obsolete, only for backward compatibility (before v1.10)
  bool error;
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "GetConfigCRC" (0xA3) command
struct Sgetconfigcrc {
  unsigned char code;
  short crc;
  char error;
  int ccbserver_code;
  std::string msg;
};

// Status TDC results
struct Sstatus_TDC {
  unsigned int id_code_long; // ID_code, long format
  bool error, warning; // error and warning flags
  std::string msg; // error/warnin messages
  std::string xmlmsg;
};

/*****************************************************************************/
// CMN_CMD class
/*****************************************************************************/

class Ccmn_cmd {
 public:
// Class constructor; arguments: pointer_to_command_object
  Ccmn_cmd(Ccommand*);
  virtual ~Ccmn_cmd();

/* Switch on/off debug printouts */
  void verbose_mode(bool in); // Go to verbose mode (in=true) or not

/* Database commands */
  void retrieve_mc_info(); // Retrieve mc info from DB
  void print_mc_info(); // Print mc info

  int get_primary_port(); // Get the primary port number fron DB. 
  int get_secondary_port(); // Get the secondary port number fron DB. 

/* HIGH LEVEL COMMANDS */

// Configure all. Input: Cconf_object, ConfType, (brd, chip).
// Return errcode (0 if ok): BIT0 (BTI error), BIT1 (TRACO), BIT2 (LUT),
//   BIT3 (TSS), BIT4 (TSM), BIT5 (TDC), BIT6 (ROB), BIT 7 (FE)
// Output: confstatus (0 if ok): BIT0 (BTI not configured), ...; confcrc: CRC of configuration
  int BTI_sent, TRACO_sent, LUT_sent, TSS_sent, TSM_sent, TDC_sent,
    ROB_sent, FE_sent, OTHER_sent, TOT_sent; // Sent command lines
  int BTI_bad, TRACO_bad, LUT_bad, TSS_bad, TSM_bad, TDC_bad,
    ROB_bad, FE_bad, TOT_bad; // Unsuccessful command lines
  short config_mc(Cconf *myconf, ConfType type, short *confstatus, short *confcrc);
  short config_mc(Cconf *myconf, ConfType type, char brd, char chip,
    short *confstatus, short *confcrc);

// Get minicrate firmware version (uses 0xea command)
  void mc_firmware_version();
  void mc_firmware_version(short*,short*); // Output: PHversion, PLVersion
  void mc_firmware_version_print();

// Return the mask of existing ROBs
  void get_rob_mask(char brdmsk[7]);

/* ROB/TDC commands (needed for configuration) */

// "read TDC" (0x43) command
  Sread_TDC read_tdc; // Contains command results
  virtual void read_TDC(char,char); // Send command and decode reply. Input: brd, chip.
  virtual void read_TDC_reset(); // Reset read_tdc struct
  virtual int read_TDC_decode(unsigned char*); // Decode reply. Input: data_read
  virtual void read_TDC_print(); // Print reply to stdout

// Check TDC status. Input: brd, chip
  Sstatus_TDC status_tdc;
  virtual void status_TDC(char,char);
  virtual void status_TDC_reset(); // Reset status_tdc struct
  virtual void status_TDC_print();

// "Read ROB error" (0x33) command
  Sread_ROB_error read_rob_error; // Contains command results
  virtual void read_ROB_error(); // Send command and decode reply.
  virtual void read_ROB_error_reset(); // Reset read_rob_error struct
  virtual int read_ROB_error_decode(unsigned char*); // Decode reply. Input: data_read
  virtual void read_ROB_error_print(); // Print reply to stdout

/* LOW LEVEL COMMANDS */

// "Read/Run Test" (0x10/0x12) command
  Smc_test mc_test; // Contains command results
  void MC_read_test(); // Send command 0x10 and decode reply.
  void MC_run_test(); // Send command 0x12 and decode reply.
  void MC_test_reset(); // Reset mc_test struct
  int MC_test_decode(unsigned char*); // Decode mc_test reply. Input: data_read
  void MC_test_print(); // Print reply to stdout

// "Read Status" (0xEA) command
  Smc_status mc_status; // Contains command results
  Sboot_status boot_status; // Contains command results (in "BOOT" progam)
  void MC_read_status(); // Send command and decode reply.
  void MC_read_status_reset(); // Reset boot_status and mc_status
  int MC_read_status_decode(unsigned char*); // Decode reply. Input: data_read
  void MC_read_status_print(); // Print reply to stdout

// "Minicrate Temperature" (0x3C) command
  Smc_temp mc_temp; // Contains command results
  void MC_temp(); // Send command and decode reply.
  int MC_temp_decode(unsigned char*); // Decode reply. Input: data_read
  void MC_temp_reset(); // Reset mc_temp struct
  void MC_temp_print(); // Print reply to stdout

// "Power on/off" (0x2F) command
  Scmn_1 pwr_on; // Contains command results
  void PWR_on(char, char); // Send command and decode reply. Input: pwr_id (power identifier), on (0=off, 1=on)
  void PWR_on_reset(); // Reset pwr_on struct
  int PWR_on_decode(unsigned char*); // Send command and decode reply. Input: data_read
  void PWR_on_print(); // Print reply to stdout

// "Clear min max voltage monitor" (0x9E) command
  Sclear_v_monitor clear_v_monitor; // Contains command results
  void clear_V_monitor(); // Send command and decode reply.
  void clear_V_monitor_reset(); // Reset clear_v_monitor struct
  int clear_V_monitor_decode(unsigned char*); // Send command and decode reply. Input: data_read
  void clear_V_monitor_print(); // Print reply to stdout

// "GetConfigCRC" (0xA3) command
  Sgetconfigcrc getconfigcrc; // Contains command results
  void GetConfigCRC(); // Send command and decode reply.
  void GetConfigCRC_reset(); // Reset getconfigcrc struct
  int GetConfigCRC_decode(unsigned char*); // Send command and decode reply. Input: data_read
  void GetConfigCRC_print(); // Print reply to stdout

// "Chamber Map" (0xB4) command
  Scmn_1 chamb_map; // Contains command results
  void chamber_map(short ID); // Send command and decode reply. Input: ID (bits 15-12: unused; bits 11-8: wheel; bits 7-4: sector; bits 3-0: station)
  void chamber_map_reset(); // Reset chamb_map struct
  int chamber_map_decode(unsigned char*); // Send command and decode reply. Input: data_read
  void chamber_map_print(); // Print reply to stdout

// config objects
  confcmd *cbti0x54, *cbti0x14, *cbti0x51, *ctraco0x15, *clut0x3E, *clut0xA8,
    *clut0x4D, *clut0x4E, *ctss0x16, *ctsm0x17, *ctsm0x49, *ctdc0x44,
    *ctdc0x18, *crob0x26, *crob0x32, *cfe0x35, *cfe0x47, *cfe0x3A, *cfe0x3B,
    *cfe0x62, *cother;

  int conflinestodo;
  int conflinesdone;

 protected:
  Ccommand *cmd_obj; // Ccommand object

  bool _verbose_;
  short HVer, LVer; // Firmware version

// Variables in Cccbmap.h
  bool InfoRet;
  int wheel, sector, station, chamber_id, minicrate_id, online, port, secport;
  char ccbserver[255], comment[255];

 private:

  void new_cpu_ck_delay(unsigned char); // Set CPU-CK delay. Input: delay
  void new_cpu_ck_delay(); // Read actual CPU-Ck delay and set a new delay

};

#endif // __CCMN_CMD_H__
