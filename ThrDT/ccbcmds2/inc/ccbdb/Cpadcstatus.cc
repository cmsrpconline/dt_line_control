#include "Cpadcstatus.h"

#include <cstring>

padcstatus::padcstatus() {
  dtdbobj = new cmsdtdb;
  newconn = true;
}

padcstatus::padcstatus(cmsdtdb* thisobj) {
  dtdbobj = thisobj;
  newconn = false;
}

padcstatus::~padcstatus() {
  if (newconn)
    delete dtdbobj;
}


// Insert a new entry in padcstatus
// Author: A.Parenti, Oct13 2006
// Modified: AP, Apr04 2007 (Tested on MySQL-Oracle)
int padcstatus::insert(int ccbID,char *mst) {
  return insert(ccbID,mst,"");
}


int padcstatus::insert(int ccbID, char *mst, char *statusxml) {
  char mquer[FIELD_MAX_LENGTH];
  int rows=0, fields=0, reso;
  int padcId=0;

  dtdbobj->dblock(); // lock the mutex

  sprintf(mquer,"SELECT PadcId FROM ccbmap WHERE CcbID='%d'",ccbID);
  reso=dtdbobj->sqlquery(mquer,&rows,&fields);
  if (reso==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {
    char myfie[LEN1];
    dtdbobj->returnfield(0,myfie);
    sscanf(myfie,"%d",&padcId);
  } else {
    dtdbobj->dbunlock(); // unlock the mutex
    return reso;
  }

  if (padcId!=0) {
    sprintf(mquer,"INSERT INTO padcstatus (padcID,adccount,statusxml,time) VALUES ('%d','%s','%s',CURRENT_TIMESTAMP)",padcId,mst,statusxml);
    reso=dtdbobj->sqlquery(mquer,&rows,&fields);

    dtdbobj->dbunlock(); // unlock the mutex
    return reso;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return -1;
}


// Retrieve last entry in padcstatus
// Author: A.Parenti, Oct13 2006
// Modified: AP, Apr04 2007 (Tested on MySQL-Oracle)
int padcstatus::retrievelast(int ccbID, char *mst, char *sttime) {
  char statusxml[FIELD_MAX_LENGTH];

  return retrievelast(ccbID,mst,statusxml,sttime);
}

int padcstatus::retrievelast(int ccbID, char *mst, char *statusxml, char *sttime) {
  char mquer[LEN2];
  int rows=0, fields=0, reso;

  dtdbobj->dblock(); // lock the mutex

// Reset results
  strcpy(mst,"");
  strcpy(statusxml,"");
  strcpy(sttime,"");

  sprintf(mquer,"SELECT padcstatus.adccount,padcstatus.statusxml,padcstatus.time FROM padcstatus,ccbmap WHERE padcstatus.padcId=ccbmap.PadcId AND ccbmap.ccbID='%d'ORDER BY time DESC",ccbID);
  reso = dtdbobj->sqlquery(mquer,&rows,&fields);

  if (reso==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {
    dtdbobj->returnfield(0,mst);
    dtdbobj->returnfield(1,statusxml);
    dtdbobj->returnfield(2,sttime);

    dtdbobj->dbunlock(); // unlock the mutex
    return 1;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return 0;
}
