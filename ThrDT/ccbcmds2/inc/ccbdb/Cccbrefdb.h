#ifndef _ccbrefdb_
#define _ccbrefdb_
#include "Ccmsdtdb.h"

class ccbrefdb {

 public:
  ccbrefdb(); // Create a new connection to DB
  ccbrefdb(cmsdtdb* dtdbobj); // Use an existing connection to DB
  ~ccbrefdb();

  int insert(short mctype, reftype rtype,char *refxml,char cmnt[LEN1]); // Insert a new reference
  int retrieveref(int ccbid,reftype rtype,char *refxml,char *cmnt); // Retrieve a reference. Input: ccbid, rtype.
  int retrieveref(int refid,short *mctype,reftype *rtype,char *refxml,char *cmnt); // Retrieve a reference. Input: refid
  int retrieveref(short mctype, reftype rtype, int *howmany, int *refid, char cmnt[][LEN2]); // Return a list of available references. Input: mctype, reftype
  int retrievelastref(short mctype, reftype rtype, int *refid, char cmnt[LEN2]); // Return the last available reference. Input: mctype, reftype

 private:
  bool newconn; // True if a new connection to DB is open
  cmsdtdb* dtdbobj;
};

#endif
