#ifndef _daccalib_
#define _daccalib_
#include "Ccmsdtdb.h"

class daccalib {
 public:
   
  daccalib(); // Create a new connection to DB
  daccalib(cmsdtdb* dtdbobj); // Use an existing connection to DB
  ~daccalib();

  int select(int thisttc); // Select a TTC and recover info

// Print DAC calibration
  void daccalib_print();
  void daccalib_print(int thisttc);

// Retrieve DAC calibrations
  int retrievedaccalib(float calibrations[14]);
  int retrievedaccalib(int thisttc,float calibrations[14]);

 private:
  bool newconn; // True if a new connection to DB is open
  bool error;
  char Datetime[32];
  int TTCid;
  float width_gain,bias_gain[3],thr_gain[3],width_ofst,bias_ofst[3],thr_ofst[3];

  void reset(); // Reset the class

  cmsdtdb* dtdbobj;
};

#endif
