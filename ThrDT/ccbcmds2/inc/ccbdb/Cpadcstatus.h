#ifndef _padcstatusdb_
#define _padcstatusdb_
#include "Ccmsdtdb.h"

class padcstatus {

 public:
  padcstatus(); // Create a new connection to DB
  padcstatus(cmsdtdb* dtdbobj); // Use an existing connection to DB
  ~padcstatus();

// Insert a new entry in padcstatus
  int insert(int ccbID,char *mst); // Input: ccbID,addcount
  int insert(int ccbcID,char *mst, char *statusxml); // Input: ...,statusxml

// Retrieve last entry in padcstatus
  int retrievelast(int ccbID, char *mst,char *sttime); // Input: ccbID
  int retrievelast(int ccbID, char *mst, char *statusxml,char *sttime); // Input: ccbID

 private:
  bool newconn; // True if a new connection to DB is open
  char *status;
  int mId;
  cmsdtdb* dtdbobj;
};

#endif
