#ifndef _dtpart_
#define _dtpart
#include "Ccmsdtdb.h"

class dtpartition {

 public:
  dtpartition(); // Create a new connection to DB
  dtpartition(cmsdtdb* dtdbobj); // Use an existing connection to DB
  ~dtpartition();

// Create new partition
  int newdtpart(char *thepart, int defa); // defa=1 for default part's

// Insert CCBs in a partition
  int insert(int ccb);
  int insert(char *thepart, int nccb, int *ccblist);

  int select(char *thepart); // Select a partition
  int retrieveparts(int *npart, char partlist[][32]); // Retrieve a list of partitions

// Retrieve CCBs forming a partition
  int retrieveccbs(char *thepart, int *nccb, int *ccblist, int *portlist, int *secportlist, char ccbserver[][255]);
  int retrieveccbs(int *nccb, int *ccblist, int *portlist, int *secportlist, char ccbserver[][255]);

// For backward compatibility
  int retrieveccbs(char *thepart, int *nccb, int *ccblist, int *portlist, char ccbserver[][255]);
  int retrieveccbs(int *nccb, int *ccblist, int *portlist, char ccbserver[][255]);
  int retrieveccbs(char *thepart, int *nccb, int *ccblist, int *portlist);
  int retrieveccbs(int *nccb, int *ccblist, int *portlist);
  int retrieveccbs(char *thepart, int *nccb, int *ccblist);
  int retrieveccbs(int *nccb, int *ccblist);

 private:
  bool newconn; // True if a new connection to DB is open
  char partname[32];
  int mId;
  cmsdtdb* dtdbobj;

};

#endif
