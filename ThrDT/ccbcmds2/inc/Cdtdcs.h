// Include Cdtdcs.h only once
#ifndef __CDTDCS_H__
#define __CDTDCS_H__

#include <ccbcmds/Clog.h>
#include <ccbcmds/Ccommand.h>
#include <ccbcmds/Cccb.h>
#include <ccbcmds/Cdef.h>     // Some definitions

#ifdef __USE_CCBDB__ // DB access enabled
  #include <ccbdb/Ccmsdtdb.h>
#endif

#include <ctime>
#include <string>

#define NMAXCCB 300 // Max number of CCBs in a partition
#define NMAXPART 500 // Max number of partitions
#define NMAXCONF 100 // Max number of configurations
#define PARTNAMESIZE 32 // Partition Name Size
#define CONFNAMESIZE 64 // Configuration Name Size

#define MC_MONITOR_INTERVAL  60 // Default minicrate monitoring interval (sec)
#define MC_MONITOR_MIN_INT   15 // Minimum interval for minicrate monitoring
#define MC_MONITOR_INT_CFG  120 // Minicrate monitoring interval during cfgs
#define PR_MONITOR_INTERVAL 300 // Default pressure monitoring interval (sec)
#define PR_MONITOR_MIN_INT   30 // Minimum interval for pressure monitoring
#define PR_MONITOR_INT_CFG  600 // Minicrate monitoring interval during cfgs
#define MONITOR_INT_CONF    300 // Monitoring interval during configs    
#define MC_RECOVER_INT_DFLT 300 // Recover task interval (sec), default
#define MC_RECOVER_INT_SHRT  60 // Recover task interval (sec)
#define MC_TO_DB_INTERVAL   600 // Minicrate_status -> DB interval (sec)
#define PR_TO_DB_INTERVAL   600 // Pressure_status  -> DB interval (sec)
#define RETRY_INTERVAL       10 // In case of timeout, retry to read a ccb after this interval (sec)

#define AUTOREC_DEFAULT    true

enum Econfstatus {c_configured=0,c_undef,c_configuring,c_error}; // Configuration status
enum Ecommand {cmd_restoreconf=0,cmd_saveconf,cmd_autosetlink,cmd_restartccb,cmd_resetrob,cmd_mctemperature,cmd_mctest,
           cmd_checktdc,cmd_readroberror,cmd_resetloselock,cmd_configcrc,cmd_mcstatus}; // available ccb commands

struct Sdtccb { // CCB descriptor

  int ccbid, ccbport; // CCB ID, primary port
  char ccbserver[255]; // CCBserver
  int wheel, sector, station; // Wheel, sector, station

/* CCB CONFIGURATION */
  char DTconfname[CONFNAMESIZE]; // DT confname (configsets.name)
  char MCconfname[CONFNAMESIZE]; // MC confname (configs.confname)

  Econfstatus cfgbti, cfgtraco, cfglut, cfgtss, cfgtsm, cfgtdc, cfgrob, cfgfe,
   cfgall; // Configuration status
  time_t cfg_time;
  short confcrc, lastcrc; // CRC of configuration, CRC read from 0xA3
  short ConfSt, LastConfSt; // Status of current and previous configurations
  short ConfEr, LastConfEr; // Errors of current and previous configurations
  std::string confmsg; // Config messages
  int conftodo,confdone;

/* CCB STATUS */
  Emcstatus physst, logicst; // Minicrate: Physical and logical status
  Emcstatus robst, padcst; // ROB & PADC status
  bool switch_off_fe, switch_off_mc; // When true, FE or MC need to be switched off
  char ccbStatusMsg[2048]; // Minicrate status messages
  char ccbErrorMsg[1024]; // Minicrate error messages
  char robStatusMsg[1024]; // ROB status messages
  char robErrorMsg[1024]; // ROB error messages
  char padcStatusMsg[1024]; // PADC status messages
  char padcErrorMsg[1024]; // PADC error messages
  float MaxTemp; // Max Board temperature
  float MaxTempRob, MaxTempTrb, MaxTempExt, MaxTempFe;
  float TempExt[7]; // All Ext sensor temp's
  float press100_hv, press100_fe; // PADC readings, sens100
//  float press500_hv, press500_fe; // PADC readings, sens500
  time_t mc_status_time, pr_status_time; // Last update of mc_status, pressure
  int mc_monitor_int, pr_monitor_int; // Time interval of minicrate/pressure monitoring (sec);
  int mc_write_int, pr_write_int; // Time interval of minicrate/pressure status logging (sec);
  bool ForceRefreshStatusMc,ForceRefreshStatusPr;// Force status update if true
  bool QpllARdy, QpllBRdy,AlrmQpllAChng, AlrmQpllBChng;

/* CCB recovery */
  bool excluded; // 'true' when ccb has been excluded from partition
  bool auto_recover; // when 'true' the ccb is auto-recovered whenever possible

/* OTHER CCB COMMANDS */
  Ecommand command_type; // Command to be executed in dt_command
  std::string CmdMsg; // Command results

/* THREAD RELATED */

  pthread_t mc_monitor_tid, pr_monitor_tid; // Monitoring thread ID
  pthread_t mc_recover_tid, conf_tid; // RecoverTask and configuration thread ID
  pthread_t setrun_tid; // Set-run/unset-run thread ID
  bool conf_tready, setrun_tready; // Go true when thread is completely started
  int mc_monitor_delay, pr_monitor_delay; // Delay of monitor threads (msec)
  int mc_recover_delay; // Delay of recover thread (msec)

/* A.O.B. */

  Ccommand *ccbcmd; // "command" object
  Clog *ccblog; // "Logger" object
  Cccb *confccb; // "ccb" object (conf)

#ifdef __USE_CCBDB__ // DB access enabled
  cmsdtdb *dtdbobj; // DB connector
#endif

  bool _verbose_;
  bool runset; // If true, pre-run operations were done
};


class dtdcs {
 public:
  dtdcs();
  ~dtdcs();

// Go to verbose mode (in=true) or not
  void verbose_mode(bool in);

// Set ccb autorecover on/off
  void enableAutoRecover();
  void disableAutoRecover();

// Check connections
  int dbTestConnetion(); // Check connection with DB. Return 0 when ok.

//*******************//
// partition methods //
//*******************//

// Initialize a dt partition, start monitoring. Return 0 if ok.
  int partInitialize(char *partition); // Input: partition_name.
  int partInitialize(bool startmonitor=true); // Re-use the last selected partition
  void partReset(); // Reset a dt partition

// Partition info
  int partGetNCcb(); // Return No. of ccb in current partition
  int partGetNExcludedCcb(); // Return No. of "excluded" ccb in current partition
  int* partGetCcbList(); // Return a list of ccb in partition 
  char* partGetPartName(); // Return the name of partition
  void partShowActual(); // Print to stdout  the actual partition
  void partShowAvailable(); // Print to stdout  the available partitions

// Monitoring
  void partStartMonitor(); // Start monitor with default interval
  void partStopMonitor(); // Abort monitoring, if ongoing
  void partSetMcMonitorInterval(int monitor_time,int write_time); // Set minicrate monitoring/logging time interval (seconds)
  void partSetPrMonitorInterval(int monitor_time,int write_time); // Set pressure monitoring/logging interval (seconds)
  void partRefreshStatus(); // Force the application to update partition status
  void partShowStatus(); // Print partition status to stdout
  void partShowStatus(std::string *strout); // Print partition status to a string

// Get partition status, temperatures, gas pressures
  Emcstatus* partGetPhysStatus(); // Return an array w/ MC "physical" status
  Emcstatus* partGetLogicStatus(); // Return an array w/ MC "logical" status
  bool *partGetSwitchOffFe(); // Return an array w/ FE "switch off" flags
  bool *partGetSwitchOffMc(); // Return an array w/ MC "switch off" flags
  Emcstatus* partGetRobStatus(); // Return an array w/ ROB status
  Emcstatus* partGetPadcStatus(); // Return an array w/ PADC status

  bool* partGetExcluded(); // Return an array w/ MC "excluded" flag
  float* partGetMaxTemp(); // Return an array w/ max MC temperatures
  float* partGetRobMaxTemp(); // Return an array w/ max ROB temperatures
  float* partGetTrbMaxTemp(); // Return an array w/ max TRB temperatures
  float* partGetExtMaxTemp(); // Return an array w/ max EXT temperatures
  float* partGetFeMaxTemp(); // Return an array w/ max FE temperatures
  float* partGetHvPressure(); // Return an array w/ gas pressures HV side
  float* partGetFePressure(); // Return an array w/ gas pressures FE side

// Send commands to partition
  void partSetRun(int RunNr); // Perform pre-run operations
  void partUnsetRun(); // Perform post-run operations
  void partStopSetRun(); // Abort pre/post-run operations
  bool partReadyForRun(); // Return 'true' if partition is ready to run
  void partSendCommand(Ecommand cmdtype); // Input: command_type

//***********************//
// Configuration methods //
//***********************//

  void confShowAvailable(); // Show the available configurations
  char* confGetConfName(); // Return the name of current configuration (configsets.name)
  void confSelect(char *cname); // Select a configured set
  void confStart(char *cname); // Start configuration
  void confStop(); // Abort configuration, if ongoing
  Econfstatus confGetStatus(); // Return configuration status: undef,...
  Econfstatus* confGetDetailedStatus(); // Return an array w/ ccb configuration status: undef,...
  bool confCheckCrc(); // Return 'true' if configuration CRC is correct

//********************//
// Single CCB methods //
//********************//

// ccb info
  int idxFromCcbId(int ccbid); // Return the index of the given ccbid
  int ccbGetCcbId(int wheel, int sector, int station); // Return ccbid
  void ccbGetWhSecSt(int ccbid, int *wheel, int *sector, int *station); // Return wheel, sector, station

  int ccbserverTestConnection(int ccbid); // Check connection w/ ccbserver. Return 0 when ok, >0 if ccb is not in partition.
  int ccbTestConnection(int ccbid); // Check connection w/ ccb. Return 0 when ok, >0 if ccb is not in partition.

// Time of last update
  time_t ccbGetStatusTime(int ccbid); // Return time of last update
  time_t ccbGetPressureTime(int ccbid); // Return time of last update

// Get minicrate status
  Emcstatus ccbGetPhysStatus(int ccbid); // Return minicrate "physical" status
  Emcstatus ccbGetLogicStatus(int ccbid); // Return minicrate "logical" status
  bool ccbGetSwitchOffFe(int ccbid); // Return FE "switch off" flag
  bool ccbGetSwitchOffMc(int ccbid); // Return MC "switch off" flag
  Emcstatus ccbGetRobStatus(int ccbid); // Return ROB status
  Emcstatus ccbGetPadcStatus(int ccbid); // Return PADC status
  void ccbRefreshStatus(int ccbid); // Force the update of ccb status
  bool ccbGetExcluded(int ccbid); // Return minicrate "excluded" flag
  char* ccbGetStatusMsg(int ccbid); // Return CCB status string
  char* ccbGetErrorMsg(int ccbid); // Return CCB errors string
  char* ccbGetRobStatusMsg(int ccbid); // Return ROB status string
  char* ccbGetRobErrorMsg(int ccbid); // Return ROB error string
  char* ccbGetPadcStatusMsg(int ccbid); // Return PADC status string
  char* ccbGetPadcErrorMsg(int ccbid); // Return PADC error string
  char* ccbGetConfStatusMsg(int ccbid); // Return configuration messages

// Get Max Temperatures
  float ccbGetMaxTemp(int ccbid); // Return max minicrate temperature
  float ccbGetRobMaxTemp(int ccbid); // Return max ROB temperature
  float ccbGetTrbMaxTemp(int ccbid); // Return max TRB temperature
  float ccbGetExtMaxTemp(int ccbid); // Return max EXT temperature
  float ccbGetFeMaxTemp(int ccbid); // Return max FE temperature
  float* ccbGetExtTemp(int ccbid); // Return all (7) EXT temperatures

// Get pressures
  float ccbGetHvPressure(int ccbid); // Return gas pressure, HV side
  float ccbGetFePressure(int ccbid); // Return gas pressure, FE side

// Configuration
  Econfstatus ccbGetConfStatus(int ccbid); // Return configuration status: undef,...
  char* ccbGetConfName(int ccbid); // Return the name of current configuration (configs.ConfName)
  void ccbReconfigure(int ccbid); // Configure again a minicrate

// Send commands
  std::string ccbSendCommand(int ccbid, Ecommand cmdtype); // Send a command to a ccb. Return result
  std::string ccbGetCommandResult(int ccbid); // Return results of the last command

  Sdtccb dtccb[NMAXCCB]; // CCB descriptor
 protected:
  Clog *dcslog; // Logger object

 private:

  bool _verbose_;
  bool auto_recover;
  char partname[PARTNAMESIZE]; // Partition name
  char DTconfname[CONFNAMESIZE]; // DT confname (configsets.name)
  int mc_monitor_int, pr_monitor_int; // Time interval of minicrate/pressure monitoring (sec);
  int mc_write_int, pr_write_int; // Time interval of minicrate/pressure status logging (sec);

  int nccb; // Number of CCBs in the partition

#ifdef __USE_CCBDB__ // DB access enabled
  cmsdtdb dcsdbobj; // DB connector
#endif
};

#endif // __CDTDCS_H__
