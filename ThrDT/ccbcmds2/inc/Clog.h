// Include Clog.h only once
#ifndef __CLOG_H__
#define __CLOG_H__

#include <ccbcmds/Cdef.h> // Some definitions

#ifdef __USE_CCBDB__ // Disable DB access
  #include <ccbdb/Cdtlogger.h> // Support for cmsdtdb DB
#else // "msglevel" is defined in Cdtlogger.h and needed here
  enum msglevel {mesg='m',warn='w',erro='e'};
#endif

#include <fstream>
#include <string>

// for mutex
#include <pthread.h>

// enable/disable mutex
#define ENABLE_LOG_MUTEX true

/***************************************************************************/
// Clog class definition
/***************************************************************************/

class Clog {
 public:
// Constructor & Destructor
  Clog(char*); // Input: filename
  Clog(); // Log to DB
  ~Clog();

// Write string to log. Input: string, msg_type (mesg,warn,erro)
  int write_log(char*, msglevel);

 private:

  file_or_db logto; // Logger type: File or DB
#ifdef __USE_CCBDB__ // Disable DB access
  dtlogger *myLog; // Log to DB
#endif

  std::string fname; // filename
  std::ofstream logfile;
  pthread_mutex_t log_mutex2; // mutex object: avoid conflicts in logging
};

#endif // __CLOG_H__
