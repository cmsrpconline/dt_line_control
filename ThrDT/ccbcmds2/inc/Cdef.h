// Include Cdef.h only once
#ifndef __CDEF_H__
#define __CDEF_H__

enum file_or_db { UFILE, UDATABASE}; // Use File or Data Base
enum Emcstatus {s_ok=0,s_undef,s_error}; // Flags for mc status

// Default: Non-verbose mode
//#define VERBOSE_DEFAULT false
#define VERBOSE_DEFAULT false

#define LOGTODB // Comment this line if you wanna log to file

#define nodbmsg() printf("DB access disabled. Recompile with DB support.\n");
#define notcpmsg() printf("TCP/IP disabled. Recompile with TCP/IP support.\n");

/* Defaults for dtdcs class and utilities */

// ccbserver and port
#define CCBSERVER "127.0.0.1"
#define CCBPORT   18889

// xterm escape sequences
#define CLRSCR "\033[2J\033[0;0H" // clear screen
#define REDBKG "\033[41;37;1m"    // Red bkg
#define GRNBKG "\033[42;37;1m"    // Green bkg
#define YLWBKG "\033[43;37;1m"    // Yellow bkg
#define BLUBKG "\033[44;37;1m"    // Blue bkg
#define STDBKG "\033[0m"          // Standard bkg

#endif // __CDEF_H__
