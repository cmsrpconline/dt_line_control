// include Cttcrx.h only once
#ifndef __CTTCRX_H__
#define __CTTCRX_H__

// defines classes Ccmn_cmd
#include <ccbcmds/Ccmn_cmd.h>

// default timeouts
#define READ_TTCRX_TIMEOUT   5000
#define WRITE_TTCRX_TIMEOUT 15000
#define TTC_SKEW_TIMEOUT    15000
#define EMUL_TTC_TIMEOUT    15000
#define SEL_TTC_CK_TIMEOUT  15000
#define RESET_LOSE_LOCK_COUNTER_TIMEOUT 15000
#define TTC_FINE_DELAY_TIMEOUT          15000
#define FIND_TTC_TIMEOUT     5000

/*****************************************************************************/
// TTCrx data structures
/*****************************************************************************/

// Format of return data from get_TTCRX_id command
struct Sttcrx_id {
  unsigned short id:14;
  unsigned char id_i2c:6, mma:2, mmb:2, read_err;
  std::string msg;
};

// Format of return data from get_TTCRX_error_counter_register command
struct Sttcrx_err_count_reg {
  unsigned short single_err_count;
  unsigned char double_err_count, seu_err_count, read_err;
  std::string msg;
};

// Format of return data from reset_TTCRX_status_register
struct Sttcrx_reset_status_reg {
  unsigned char write_err;
  unsigned char data1, data2, result1, result2;
  std::string msg;
};

// Format of return data from get_bunch_counter command
// and get_event_counter
struct Sget_counter {
  unsigned short counter;
  unsigned char read_err;
  std::string msg;
};

// Format of return data from reset_bunch_counter command,
// reset_event_counter
struct Sset_register {
  unsigned char write_err;
  std::string msg;
};

// Format of return data from "Get TTCrx config" command
struct Sget_ttcrx_config {
  unsigned char dll_isel:3, pll_isel:3, dll_sel_aux_1:1, dll_sel_aux_2:1; // config 1
  unsigned char mux_select:3, cf_sel_test_PD:1, cf_sel_inputA:1, cf_PLL_aux_reset:1, cf_DLL_aux_reset:1, cf_en_check_machineA:1; // config2
  unsigned char frequ_check_period:3, cf_dis_INITfaster:1, cf_dis_watchdog:1, cf_en_Hamming:1, cf_en_testIO:1, cf_en_check_machineB:1; // config3
  unsigned char read_err;
  std::string msg;
};

// Format of return data from "read TTCrx" (0x28) command,
// "TTC Read (boot)" (0xE2)
struct Sread_TTCRX {
  unsigned char code, data;
  char result;
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "write TTCrx" (0x27) command
// also "TTC skew" (0x48) command and "TTCrx fine delay" (0x8E)
// "set TTCrx coarse delay"
struct Sttc_1 {
  unsigned char code1, code2;
  char result;
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "Emul TTC" (0x50) command
// also "Sel TTC Ck" (0x2A), "reset Lose Lock Counter" (0x8A)
struct Sttc_2 {
  unsigned char code1, code2;
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "TTC Write (boot)" (0xE3),
// "TTC find" (0xE0)
struct Sttc_3 {
  unsigned char code1, code2;
  char error;
  int ccbserver_code;
  std::string msg;
};

/*****************************************************************************/
// TTCrx class
/*****************************************************************************/

class Cttcrx : public Ccmn_cmd {
 public:
// Class constructor; arguments: pointer_to_ccb_object
  Cttcrx(Ccommand*);

/* Override ROB/TDC methods */
  void read_TDC(char,char);
  int read_TDC_decode(unsigned char*);
  void read_TDC_print();

  void status_TDC(char,char);
  void status_TDC_print();

  void read_ROB_error();
  int read_ROB_error_decode(unsigned char*);
  void read_ROB_error_print();

/* HIGH LEVEL COMMANDS */

// "Read the ID of the TTCrx"
  Sttcrx_id ttcrx_id;
  void get_TTCRX_id(); // Send command and decode reply.
  void get_TTCRX_id_reset(); // Reset ttcrx_id struct
  void get_TTCRX_id_print(); // Print reply to stdout

// "Get the TTCrx fine delay" command
  Sread_TTCRX get_ttcrx_fine_delay;
  void get_TTCRX_fine_delay(char); // Send command and decode reply. Input: ch (0/1 for delay 1/2).
  void get_TTCRX_fine_delay_reset(); // Reset get_ttcrx_fine_delay struct
  void get_TTCRX_fine_delay_print(); // Print reply to stdout

// "Get the TTCrx coarse delay"
  Sread_TTCRX get_ttcrx_coarse_delay;
  void get_TTCRX_coarse_delay(); // Send command and decode reply.
  void get_TTCRX_coarse_delay_reset(); // Reset get_ttcrx_coarse_delay struct
  void get_TTCRX_coarse_delay_print(); // Print reply to stdout

// "Set the TTCrx coarse delay"
  Sttc_1 set_ttcrx_coarse_delay;
  void set_TTCRX_coarse_delay(float,float); // Send command and decode reply. Input: coarse_delay_1, coarse_delay_2
  void set_TTCRX_coarse_delay_reset(); // Reset set_ttcrx_coarse_delay struct
  void set_TTCRX_coarse_delay_print(); // Print reply to stdout

// "Get the TTCrx Control Register"
  Sread_TTCRX get_ttcrx_control_reg;
  void get_TTCRX_control_register(); // Send command and decode reply.
  void get_TTCRX_control_register_reset();// Reset get_ttcrx_control_reg struct
  void get_TTCRX_control_register_print(); // Print reply to stdout

// "Set the TTCrx Control Register"
  Sttc_1 set_ttcrx_control_reg;
  void set_TTCRX_control_register(unsigned char); // Send command and decode reply. Input: data
  void set_TTCRX_control_register_reset();// Reset set_ttcrx_control_reg struct
  void set_TTCRX_control_register_print(); // Print reply to stdout

// "Reset the TTCrx Control Register"
  Sttc_1 reset_ttcrx_control_reg;
  void reset_TTCRX_control_register(); // Send command and decode reply.
  void reset_TTCRX_control_register_reset(); // Reset reset_ttcrx_control_reg struct
  void reset_TTCRX_control_register_print(); // Print reply to stdout

// "Get the Error Counter Register"
  Sttcrx_err_count_reg get_err_count;
  void get_TTCRX_error_counter(); // Send command and decode reply.
  void get_TTCRX_error_counter_reset(); // Reset get_err_count struct
  void get_TTCRX_error_counter_print(); // Print reply to stdout

// "Reset the Error Counter Register" (?)
  Sset_register reset_err_count;
  void reset_TTCRX_error_counter(); // Send command and decode reply. Input: write_log
  void reset_TTCRX_error_counter_reset(); // Reset reset_err_count struct
  void reset_TTCRX_error_counter_print(); // Print reply to stdout

// "Get the Status Register"
  Sread_TTCRX get_ttcrx_status_reg;
  void get_TTCRX_status_register(); // Send command and decode reply.
  void get_TTCRX_status_register_reset(); // Reset get_ttcrx_status_reg struct
  void get_TTCRX_status_register_print(); // Print reply to stdout

// "Reset the Status Register"
  Sttcrx_reset_status_reg reset_ttcrx_status_reg;
  void reset_TTCRX_status_register(bool,bool); // Send command and decode reply. Input: reset_status, reset_watchdog
  void reset_TTCRX_status_register_reset(); // Reset reset_ttcrx_status_reg struct
  void reset_TTCRX_status_register_print(); // Print reply to stdout

// "Get Bunch Counter"
  Sget_counter get_b_counter;
  void get_TTCRX_bunch_counter(); // Send command and decode reply.
  void get_TTCRX_bunch_counter_reset(); // Reset get_b_counter struct
  void get_TTCRX_bunch_counter_print(); // Print reply to stdout

// "Reset Bunch Counter"
  Sset_register reset_b_counter;
  void reset_TTCRX_bunch_counter(); // Send command and decode reply.
  void reset_TTCRX_bunch_counter_reset(); // Reset reset_b_counter struct
  void reset_TTCRX_bunch_counter_print(); // Print reply to stdout

// "Get Event Counter"
  Sget_counter get_e_counter;
  void get_TTCRX_event_counter(); // Send command and decode reply.
  void get_TTCRX_event_counter_reset(); // Reset get_e_counter struct
  void get_TTCRX_event_counter_print(); // Print reply to stdout

// "Reset Event Counter"
  Sset_register reset_e_counter;
  void reset_TTCRX_event_counter(); // Send command and decode reply.
  void reset_TTCRX_event_counter_reset(); // Reset reset_e_counter struct
  void reset_TTCRX_event_counter_print(); // Print reply to stdout

// "Get TTCrx Config"
  Sget_ttcrx_config get_ttcrx_config;
  void get_TTCRX_config(); // Send command and decode reply.
  void get_TTCRX_config_reset(); // Reset get_ttcrx_config struct
  void get_TTCRX_config_print(); // Print reply to stdout

// "Set TTCrx Config"
  Sset_register set_ttcrx_config;
  void set_TTCRX_config(unsigned char*); // Send command and decode reply. Input: config[3]
  void set_TTCRX_config_reset(); // Reset set_ttcrx_config struct
  void set_TTCRX_config_print(); // Print reply to stdout

// "Reset TTCrx Config"
  Sset_register reset_ttcrx_config;
  void reset_TTCRX_config(); // Send command and decode reply.
  void reset_TTCRX_config_reset(); // Reset reset_ttcrx_config struct
  void reset_TTCRX_config_print(); // Print reply to stdout


/* LOW LEVEL COMMANDS */

// "read TTCrx" (0x28) command
  Sread_TTCRX read_ttcrx; // Contains command results
  void read_TTCRX(unsigned char); // Send command and decode reply. Input: addr
  void read_TTCRX_reset(); // Reset read_ttcrx struct
  int read_TTCRX_decode(unsigned char*); // Decode reply. Input: data_read
  void read_TTCRX_print(); // Print reply to stdout

// "write TTCrx" (0x27) command
  Sttc_1 write_ttcrx; // Contains command results
  void write_TTCRX(unsigned char,unsigned char); // Send command and decode reply. Input: addr, data
  void write_TTCRX_reset(); // Reset write_ttcrx struct
  int write_TTCRX_decode(unsigned char*); // Decode reply. Input: data_read
  void write_TTCRX_print(); // Print reply to stdout

// "TTC skew" (0x48) command
  Sttc_1 ttc_skew; // Contains command results
  void TTC_skew(char); // Send command and decode reply. Input: delay (unit 1ns)
  void TTC_skew_reset(); // Reset ttc_skew struct
  int TTC_skew_decode(unsigned char*); // Decode reply. Input: data_read
  void TTC_skew_print(); // Print reply to stdout

// "Emul TTC" (0x50) command
  Sttc_2 emul_ttc; // Contains command results
  void emul_TTC(char); // Send command and decode reply. Input: cmd (0=SQADV, 1=SQTEST - test pulse, 2=RESET CCB, 3=SQRST)
  void emul_TTC_reset(); // Reset emul_ttc struct
  int emul_TTC_decode(unsigned char*); // Decode reply. Input: data_read
  void emul_TTC_print(); // Print reply to stdout

// "sel TTC Ck" (0x2A) command
  Sttc_2 sel_ttc_ck; // Contains command results
  void sel_TTC_ck(char); // Send command and decode reply. Input: Ck_type (bit0=QPLL1, bit2=QPLL2, bit2=EnTtcCkMux)
  void sel_TTC_ck_reset(); // Reset sel_ttc_ck struct
  int sel_TTC_ck_decode(unsigned char*); // Decode reply. Input: data_read
  void sel_TTC_ck_print(); // Print reply to stdout

// "reset lose lock counter" (0x8A) command
  Sttc_2 reset_llc; // Contains command results
  void reset_lose_lock_counter(); // Send command and decode reply.
  void reset_lose_lock_counter_reset(); // Reset reset_llc struct
  int reset_lose_lock_counter_decode(unsigned char*); // Decode reply. Input: data_read
  void reset_lose_lock_counter_print(); // Print reply to stdout


// "set TTCrx fine delay" (0x8E) command
  Sttc_1 ttc_fine_delay;
  void set_TTC_fine_delay(char,float); // Send command and decode reply. Input: ch (0 or 1 for delay 1 or 2), delay (ns)
  void set_TTC_fine_delay_reset(); // Reset ttc_fine_delay struct
  int set_TTC_fine_delay_decode(unsigned char*); // Decode reply. Input: data_read
  void set_TTC_fine_delay_print(); // Print reply to stdout

/* BOOT COMMANDS */

// "TTC Write (boot)" (0xE3) command
  Sttc_3 write_ttc_boot; // Contains command results
  void write_TTC_boot(unsigned char,unsigned char); // Send command and decode reply. Input: addr, data
  void write_TTC_boot_reset(); // Reset write_ttc_boot struct
  int write_TTC_boot_decode(unsigned char*); // Decode reply. Input: data_read
  void write_TTC_boot_print(); // Print reply to stdout

// "TTC Read (boot)" (0xE2) command
  Sread_TTCRX read_ttc_boot; // Contains command results
  void read_TTC_boot(unsigned char); // Send command and decode reply. Input: addr
  void read_TTC_boot_reset(); // Reset read_ttc_boot struct
  int read_TTC_boot_decode(unsigned char*); // Decode reply. Input: data_read
  void read_TTC_boot_print(); // Print reply to stdout

// "TTC Find (boot)" (0xE0) command
  Sttc_3 find_ttc_boot; // Contains command results
  void find_TTC_boot(); // Send command and decode reply.
  void find_TTC_boot_reset(); // Reset find_ttc_boot struct
  int find_TTC_boot_decode(unsigned char*); // Decode reply. Input: data_read
  void find_TTC_boot_print(); // Print reply to stdout

 private:
  char* TTCRX_result_to_str(char result); // Decode RdTTCrx/WrTTCrx result.

};

#endif // __CTTCRX_H__
