// include Ctrb.h only once
#ifndef __CTRB_H__
#define __CTRB_H__

// defines classes Ccmn_cmd
#include <ccbcmds/Ccmn_cmd.h>

// default timeouts
#define TRB_PI_TEST_TIMEOUT           15000
#define TRB_DISABLE_TEMP_TEST_TIMEOUT 15000
#define TRB_DISABLE_TRB_CK_TIMEOUT    15000
#define TRB_PI_TEST_0xA0_TIMEOUT      15000
#define CLEAR_TRB_SEU_COUNTER_TIMEOUT 15000
#define TEST_IR_TIMEOUT               15000

/*****************************************************************************/
// TRB data structures
/*****************************************************************************/

// Format of return data from "TRB PI test" (0x5F) command
struct Strb_pi_test {
  unsigned char code1, tss_err, traco_err[4], bti_err[32];
  unsigned char code2, error;
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "Disable temperature test" (0x83) command,
// also "Disable TRB CK" (0x93), "Clear TRB SEU Counters" (0xA1),
// "Test IR" (0xA2)
struct Strb_1 {
  unsigned char code1, code2;
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "TRB PI test" (0xA0) command
struct Strb_pi_test_0xA0 {
  unsigned char code1, code2, data;
  int ccbserver_code;
  std::string msg;
};


/*****************************************************************************/
// TRB class
/*****************************************************************************/

class Ctrb : public Ccmn_cmd {
 public:
// Class constructor; arguments: pointer_to_ccb_object
  Ctrb(Ccommand*);

/* Override ROB/TDC methods */
  void read_TDC(char,char);
  int read_TDC_decode(unsigned char*);
  void read_TDC_print();

  void status_TDC(char,char);
  void status_TDC_print();

  void read_ROB_error();
  int read_ROB_error_decode(unsigned char*);
  void read_ROB_error_print();

/* HIGH LEVEL COMMANDS */

/* LOW LEVEL COMMANDS */

// "TRB PI test" (0x5F) command
  Strb_pi_test trb_pi_test; // Contains command results
  void TRB_PI_test(char); // Send command and decode reply. Input: brd.
  void TRB_PI_test(char,char); // Send command and decode reply. Input: brd, ignorectrl
  void TRB_PI_test_reset(); // Reset trb_pi_test struct
  int TRB_PI_test_decode(unsigned char*); // Secode reply. Input: data_read
  void TRB_PI_test_print(); // Print reply to stdout

  // "Disable temperature test" (0x83) command
  Strb_1 trb_temp_test; // Contains command results
  void disable_temp_test(char); // Send command and decode reply. Input: disable (if 1 disable TRB/ROB temperature test for 1 hour)
  void disable_temp_test_reset(); // Reset trb_temp_test struct
  int disable_temp_test_decode(unsigned char*); // Decode reply. Input: data_read
  void disable_temp_test_print(); // Print reply to stdout

  // "Disable TRB CK" (0x93) command
  Strb_1 disable_trb_ck; // Contains command results
  void disable_TRB_CK(char); // Send command and decode reply. Input: state (1=disable)
  void disable_TRB_CK_reset(); // Reset disable_trb_ck struct
  int disable_TRB_CK_decode(unsigned char*); // Decode reply. Input: data_read
  void disable_TRB_CK_print(); // Print reply to stdout

  // "TRB PI test" (0xA0) command
  Strb_pi_test_0xA0 trb_pi_test_0xA0; // Contains command results
  void TRB_PI_test_0xA0(char,char,char,char,char,char); // Send command and decode reply. Input: nbrd, idchip (0=TSS, 1=TRACO, 2=BTI), nchip, reg, write (1=WRITE, 0=READ), write_data
  void TRB_PI_test_0xA0_reset(); // Reset trb_pi_test_0xA0 struct
  int TRB_PI_test_0xA0_decode(unsigned char*); // Decode reply. Input: data_read
  void TRB_PI_test_0xA0_print(); // Print reply to stdout


  // "Clear TRB SEU Counters" (0xA1) command
  Strb_1 clear_trb_seu_counter; // Contains command results
  void clear_TRB_SEU_counter(); // Send command and decode reply.
  void clear_TRB_SEU_counter_reset(); // Reset clear_trb_seu_counter struct
  int clear_TRB_SEU_counter_decode(unsigned char*); // Decode reply. Input: data_read
  void clear_TRB_SEU_counter_print(); // Print reply to stdout

  // "Test IR" (0xA2) command
  Strb_1 test_ir; // Contains command results
  void test_IR(); // Send command and decode reply.
  void test_IR_reset(); // Reset test_ir struct
  int test_IR_decode(unsigned char*); // Decode reply. Input: data_read
  void test_IR_print(); // Print reply to stdout
};

#endif // __CTRB_H__
