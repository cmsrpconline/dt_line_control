// Include Cref.h only once
#ifndef __CREF_H__
#define __CREF_H__

#include <fstream>
#include <string>

#include <ccbcmds/Cdef.h> // Some definitions

#ifdef __USE_CCBDB__ // DB access enabled
  #include <ccbdb/Ccmsdtdb.h>
#endif

#define BOOT_XML_ROOT   "boot_reference_values"
#define STATUS_XML_ROOT "status_reference_values"
#define TEST_XML_ROOT   "test_reference_values"
#define ROB_XML_ROOT    "rob_reference_values"
#define PADC_XML_ROOT   "padc_reference_values"

#define XML_MAX_DIM 9000

/*****************************************************************************/
// CCB/Minicrate reference data structures
/*****************************************************************************/

// Reference values for "minicrate status" (0xEA) command
// (BOOT program v3.9)
struct Sref_boot {
  unsigned char code;
//  unsigned short HVersion, LVersion;
  bool EUartIRQ, TTCi2c, CpldTtc, QpllArdy, QpllBrdy, QpllAChng, QpllBChng,
    TtcCk, IUart, EUartA, EUartB, ExtRam, Flash, TTCrxRdy,
    LinkAutoset;
  unsigned short PwrRam, ErrRam, vErrRam, PwrFlash, FlashID, LinkOfsMin,
    LinkHystMin, LinkAmpMin, LinkOfsMax, LinkHystMax;
  unsigned int PwrCrashFlags;
  unsigned short CrashFlags, PwrAnalog;
  float VccinMin, VddinMin, VccMin, VddMin, Tp1HMin, Tp1LMin, Tp2HMin, Tp2LMin,
    VccinMax, VddinMax, VccMax, VddMax, Tp1HMax, Tp1LMax, Tp2HMax, Tp2LMax;
};


// Reference values for "minicrate status" (0xEA) command
// (MINICRATE program v1.21)
struct Sref_status {
  unsigned char code;
//  unsigned short HVersion, LVersion, McType;
  unsigned short McType;
  bool PwrAn, PwrCK, PwrLed, PwrRpc, PwrTrbBuf, PwrTrbVcc, PwrSO, PwrDU,
    PwrDD, PwrSBCK, PwrTH, TTCrdy, PwrFlash, EnTtcCkMux, QpllARdy, QpllBRdy;
  unsigned char PwrTrb, PwrRob;
  bool AlrmPwrAn, AlrmPwrCK, AlrmPwrLed, AlrmPwrRpc, AlrmPwrTrbBuf,
    AlrmPwrTrbVcc, AlrmPwrSO, AlrmPwrDU, AlrmPwrDD, AlrmPwrSBCK, AlrmPwrTH,
    AlrmTTCrdy, AlrmPwrFlash, AlrmEnTtcCkMux, AlrmQpllAChng, AlrmQpllBChng;
  unsigned char AlrmPwrTrb, AlrmPwrRob, AlrmTempTrb, AlrmTempRob,
    LoseLockCountTTC, LoseLockCountQPLL1, LoseLockCountQPLL2;
  unsigned int SeuRam;
  unsigned int SeuIntRam, SeuBTI, SeuTRACO, SeuLUT, SeuTSS;
  float VccinMin, VddinMin, VccMin, VddMin, Tp1LMin, Tp1HMin, Tp2LMin, Tp2HMin,
    Fe_VccMin[3], Fe_VddMin[3], Sp_VccMin, Sp_VddMin;
  float VccinMax, VddinMax, VccMax, VddMax, Tp1LMax, Tp1HMax, Tp2LMax, Tp2HMax,
    Fe_VccMax[3], Fe_VddMax[3], Sp_VccMax, Sp_VddMax;
  short in_TmaxMin, in_TmedMin, th_TmaxMin, th_TmedMin, out_TmaxMin,
    out_TmedMin;
  short in_TmaxMax, in_TmedMax, th_TmaxMax, th_TmedMax, out_TmaxMax,
    out_TmedMax;
  float BrdMaxtempMin, BrdMaxtempMax;
  short FeOffTMin, FeOffTMax;
  float McOffTMin, McOffTMax;
};



// Reference values for "read/run test results" (0x10/0x12) command
struct Sref_test {
  unsigned char code;
  bool TTCFpga, AnPwr, CKPwr, LedPwr, RpcPwr, BufTrbPwr, VccTrbPwr, B1w, SOPwr,
    DUPwr, DDPwr, SBCKPwr, THPwr, CPUdelay, TPFineDelay1, TPFineDelay2;
  unsigned short OnTimeMin[11], OnTimeMax[11], AdcNoiseMin[32],
    AdcNoiseMax[32], Dac, SbTestJtag, SbTestPi;
  unsigned char TrbPwr, TrbBadJtagAddr;
  unsigned short TrbPresMsk[8], TrbTestJtag[8];
  unsigned char TrbFindSensor[8];
  unsigned short TrbOnTimeMin[8], TrbOnTimeMax[8], TrbPiTest[6];
  unsigned char RobPwr, RobBadJtagAddr, RobOverlapAddr;
  unsigned short RobPresMsk[7], RobTestJtag[7];
  unsigned char RobFindSensor[7];
  unsigned short RobOnTimeMin[7], RobOnTimeMax[7];
  unsigned short McType;
};


// Reference values for ROB status (0x3C+0x5B) commands
struct Sref_rob {
  float RobTempMin[7], RobTempMax[7];
  float RobVccMin[7], RobVccMax[7];
  float RobVddMin[7], RobVddMax[7];
  float RobIMin[7], RobIMax[7];
};

// Reference values for PADC status
struct Sref_padc {
  float Sens100HvMin, Sens500HvMin, SensHvVccMin; // HV side: press. (bar), V
  float Sens100HvMax, Sens500HvMax, SensHvVccMax;
  float Sens100FeMin, Sens500FeMin, SensFeVccMin; // FE side: press. (bar), V
  float Sens100FeMax, Sens500FeMax, SensFeVccMax;
  float PadcVccMin, PadcVddMin;
  float PadcVccMax, PadcVddMax;
  float pRelVarMax; // Maximum relative variation allowed for pressure
  float pMin; // Lower limit for press. variation check
};

/*****************************************************************************/
// Cref class definition
/*****************************************************************************/

class Cref {
 public:
// Constructor.
  Cref(char *file1, char *file2, char *file3, char *file4, char *file5); // Input: filenames (boot,status,test,rob)
#ifdef __USE_CCBDB__ // DB access enabled
  Cref(short the_id,cmsdtdb *thisdtdb); // Input: ccbid, DB_connector
#endif

// print reference values to stout
  void print_boot_ref();
  void print_status_ref();
  void print_test_ref();
  void print_rob_ref();
  void print_padc_ref();

// reference values
  Sref_boot ref_boot; // boot status reference
  Sref_status ref_status; // mc status reference
  Sref_test ref_test; // test results reference
  Sref_rob ref_rob; // ROB status reference
  Sref_padc ref_padc; // PADC status reference

 private:

  file_or_db reffrom; // Reference from file or db

// ccb_id
  short ccb_id;

// filenames
  std::string BootName;
  std::string StatusName;
  std::string TestName;
  std::string RobName;
  std::string PadcName;

// Load reference values from file or DB.
  int load_boot_ref();
  int load_status_ref();
  int load_test_ref();
  int load_rob_ref();
  int load_padc_ref();

#ifdef __USE_CCBDB__ // DB access enabled
  cmsdtdb *dtdbobj;
#endif
};

#endif // __CREF_H__
