// include Cbti.h only once
#ifndef __CBTI_H__
#define __CBTI_H__

// defines classes Ccmn_cmd
#include <ccbcmds/Ccmn_cmd.h>

// configurations class
#include <ccbcmds/Cconf.h>

// default timeouts
#define READ_BTI_TIMEOUT           5000
#define LOAD_BTI_EMUL_TIMEOUT     15000
#define START_BTI_EMUL_TIMEOUT    15000
#define COMP_BTI_REGISTER_TIMEOUT  5000

/*****************************************************************************/
// BTI data structures
/*****************************************************************************/

// Configuration results
struct Sconfig_BTI {
  short sent, bad;  // Sent and unsuccessful cmds
  short crc;
  bool error;
};

// Format of return data from "Read BTI 8bit" (0x19) command
struct Sread_BTI {
  unsigned char code1, conf[11], testin[13], testout[13];
  unsigned char code2;
  char narg;
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "Read BTI 6bit" (0x59) command
struct Sread_BTI_6b {
  unsigned char code1, conf[14], testin[17], testout[17];
  unsigned char code2;
  char narg;
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "Write BTI 8bit" (0x14),
// "Write BTI 6bit" (0x54) command, "Enable BTI" (0x51) command,
// "Load BTI emulation trace" (0x52), "Start emulation" (0x53),
// "New start emulation" (0x79)
struct Sbti_1 {
  unsigned char code1, code2;
  char result;
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "Compare BTI register" (0x9C) command
struct Scomp_bti_register {
  unsigned char code, conf[11], testin[13];
  int ccbserver_code;
  std::string msg;
};


/*****************************************************************************/
// BTI class
/*****************************************************************************/

class Cbti : public Ccmn_cmd {
 public:
// Class constructor; arguments: pointer_to_command_object
  Cbti(Ccommand*);

/* Override ROB/TDC methods */
  void read_TDC(char,char);
  int read_TDC_decode(unsigned char*);
  void read_TDC_print();

  void status_TDC(char,char);
  void status_TDC_print();

  void read_ROB_error();
  int read_ROB_error_decode(unsigned char*);
  void read_ROB_error_print();

/* HIGH LEVEL COMMANDS */

// Configure the BTI. Input: Cconf_object, brd, chip
// Use brd=-1 (chip=-1) to configure all brd's (chip's)
// Return 0 if everything went fine
  Sconfig_BTI config_BTI_result;
  short config_BTI(Cconf*,char,char);
  void config_BTI_print();

//  Retrive BTI config from CCB, dump it to file
  void read_BTI_config(char *filename);

/* LOW LEVEL COMMANDS */

// "Read BTI 8bit" (0x19) command
  Sread_BTI read_bti; // Contains command results
  void read_BTI(char,char); // Send command and decode reply. Input: brd, chip
  void read_BTI_reset(); // Reset read_bti struct
  int read_BTI_decode(unsigned char*); // Decode reply. Input: data_read
  void read_BTI_print(); // Print reply to stdout

// "Read BTI 6bit" (0x59) command
  Sread_BTI_6b read_bti_6b; // Contains command results
  void read_BTI_6b(char, char); // Send command and decode reply. Input: brd, chip.
  void read_BTI_6b_reset(); // Reset read_bti_6b struct
  int read_BTI_6b_decode(unsigned char*); // Decode reply. Input: data_read
  void read_BTI_6b_print(); // Print reply to stdout

// "Write BTI 8bit" (0x14) command
  Sbti_1 write_bti; // Contains command results
  void write_BTI(char, char, unsigned char*, unsigned char*); // Send command and decode reply. Inputs: brd, chip, conf[11], testin[13]
  void write_BTI_reset(); // Reset write_bti struct
  int write_BTI_decode(unsigned char*); // Decode reply. Input: data_read
  void write_BTI_print(); // Print reply to standard output

// "Write BTI 6bit" (0x54) command
  Sbti_1 write_bti_6b; // Contains command results
  void write_BTI_6b(char, char, unsigned char*, unsigned char*); // Send command and decode reply. Inputs: brd, chip, conf[14], testin[17]
  void write_BTI_6b_reset(); // Reset write_bti_new struct
  int write_BTI_6b_decode(unsigned char*); // Decode reply. Input: data_read
  void write_BTI_6b_print(); // Print reply to standard output

// "Enable BTI" (0x51) command
  Sbti_1 enable_bti; // Contains command results
  void enable_BTI(char, short,short); // Send command and decode reply. Inputs: brd, inner_msk, outer_msk
  void enable_BTI_reset(); // Reset enable_bti struct
  int enable_BTI_decode(unsigned char*); // Decode reply. Input: data_read
  void enable_BTI_print(); // Print reply to standard output

// "Load BTI emulation trace" (0x52) command
  Sbti_1 load_bti_emul; // Contains command results
  void load_BTI_emulation(char,char,short*); // Send command and decode reply. Inputs: brd, chip, trace[9]
  void load_BTI_emulation_reset(); // Reset load_bti_emul struct
  int load_BTI_emulation_decode(unsigned char*); // Decode reply. Input: data_read
  void load_BTI_emulation_print(); // Print reply to standard output

// "Start BTI emulation" (0x53) command
  Sbti_1 start_bti_emul; // Contains command results
  void start_BTI_emulation(char, char); // Send command and decode reply. Input: bti, chip
  void start_BTI_emulation_reset(); // Reset start_bti_emul struct
  int start_BTI_emulation_decode(unsigned char*); // Decode reply. Input: data_read
  void start_BTI_emulation_print(); // Print reply to standard output

// "New Start BTI emulation" (0x79) command
  Sbti_1 new_start_bti_emul; // Contains command results
  void new_start_BTI_emulation(char,int); // Send command and decode reply. Input: brd, btimsk
  void new_start_BTI_emulation_reset(); // Reset new_start_bti_emul struct
  int new_start_BTI_emulation_decode(unsigned char*); // Decode reply. Input: data_read
  void new_start_BTI_emulation_print(); // Print reply to standard output

// "Compare BTI register" (0x9C) command
  Scomp_bti_register comp_bti_register; // Contains command results
  void compare_BTI_register(char, char); // Send command and decode reply. Inputs: brd, chip.
  void compare_BTI_register_reset(); // Reset comp_bti_register struct
  int compare_BTI_register_decode(unsigned char*); // Decode reply. Input: data_read
  void compare_BTI_register_print(); // Print reply to standard output

};

#endif // __CBTI_H__
