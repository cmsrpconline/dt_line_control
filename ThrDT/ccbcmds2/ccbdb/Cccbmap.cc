#include "Cccbmap.h"
#include <cstring>

char strreftypem[][7]={"boot","status","test"};

ccbmap::ccbmap() {
  dtdbobj = new cmsdtdb;
  newconn = true;
}

ccbmap::ccbmap(cmsdtdb* thisobj) {
  dtdbobj = thisobj;
  newconn = false;
}

ccbmap::~ccbmap() {
  if (newconn)
    delete dtdbobj;
}


// Author: S.Ventura
// Modified: AP, Oct10 2007
int ccbmap::insert(int ccbid,int wh,int sec,int sta,int cha,int min,int onl,int prt,int sprt,char *serv,char *comm) {
  mId=ccbid;
  wheel=wh;
  sector=sec;
  station=sta;
  chamber=cha;
  minicrate=min;
  online=onl;
  port=prt;
  secport=sprt;
  strcpy(ccbserver,serv);
  strcpy(comment,comm);

  return this->insert();
}

// Author: S.Ventura
// Modified: AP, Oct10 2007 (Tested)
int ccbmap::insert() {
  char mquer[LEN2];
  int rows=0, fields=0, resu=0;

  dtdbobj->dblock(); // lock the mutex

  sprintf(mquer,"INSERT INTO ccbmap (ccbID,wheel,sector,station,chamber,minicrate,on_line,port,secport,ccbserver,cmnt) VALUES ('%d','%d','%d','%d','%d','%d','%d','%d','%d','%s','%s')",mId,wheel,sector,station,chamber,minicrate,online,port,secport,ccbserver,comment);
  resu = dtdbobj->sqlquery(mquer,&rows,&fields);

  if (resu!=0) {
    dtdbobj->dbunlock(); // unlock the mutex
    return resu;
  }

  sprintf(mquer,"INSERT INTO refrelations (ccbID) VALUES ('%d')",mId);
  resu = dtdbobj->sqlquery(mquer,&rows,&fields);

  dtdbobj->dbunlock(); // unlock the mutex
  return resu;
}



// Author: AP, Jan20 2006
// Modified: AP, Apr18 2007 (Tested on Oracle/MySQL)
int ccbmap::setrefs(int ccbid,reftype rtype,int refid) {
  char mquer[LEN2];
  int rows=0, fields=0, resu=0;

  mId = ccbid;

  sprintf(mquer,"UPDATE refrelations SET %srefid='%d' WHERE CcbID='%d'",strreftypem[rtype],refid,ccbid);

  dtdbobj->dblock(); // lock the mutex
  resu=dtdbobj->sqlquery(mquer,&rows,&fields);
  dtdbobj->dbunlock(); // unlock the mutex

  return resu;
}

// Author: AP, Jan20 2006
// Modified: AP, Apr18 2007 (Tested on Oracle/MySQL)
int ccbmap::setrefs(int ccbid,int bootrefid,int statusrefid,int testrefid) {
  char mquer[LEN2];
  int rows=0, fields=0, resu=0;

  mId = ccbid;

  sprintf(mquer,"UPDATE refrelations SET bootrefid='%d', statusrefid='%d', testrefid='%d' WHERE CcbID='%d'",bootrefid,statusrefid,testrefid,ccbid);

  dtdbobj->dblock(); // lock the mutex
  resu=dtdbobj->sqlquery(mquer,&rows,&fields);
  dtdbobj->dbunlock(); // unlock the mutex

  return resu;
}


// Author: S.Ventura
// Modified: AP, Oct10 2007 (Tested)
int ccbmap::retrieve(int ccbid) {
  char mquer[LEN2];
  int rows=0, fields=0, resu=0;

  dtdbobj->dblock(); // lock the mutex
  mId = ccbid;

  sprintf(mquer,"SELECT wheel,sector,station,chamber,minicrate,on_line,port,secport,ccbserver,cmnt FROM ccbmap WHERE ccbID='%d'",ccbid);
  resu = dtdbobj->sqlquery(mquer,&rows,&fields);

  if (resu==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {
    char myfie[LEN1];

    dtdbobj->returnfield(0,myfie);
    sscanf(myfie,"%d",&wheel);
    dtdbobj->returnfield(1,myfie);
    sscanf(myfie,"%d",&sector);
    dtdbobj->returnfield(2,myfie);
    sscanf(myfie,"%d",&station);
    dtdbobj->returnfield(3,myfie);
    sscanf(myfie,"%d",&chamber);
    dtdbobj->returnfield(4,myfie);
    sscanf(myfie,"%d",&minicrate);
    dtdbobj->returnfield(5,myfie);
    sscanf(myfie,"%d",&online);
    dtdbobj->returnfield(6,myfie);
    sscanf(myfie,"%d",&port);
    dtdbobj->returnfield(7,myfie);
    sscanf(myfie,"%d",&secport);
    dtdbobj->returnfield(8,myfie);
    strcpy(ccbserver,myfie);
    dtdbobj->returnfield(9,myfie);
    strcpy(comment,myfie);

    dtdbobj->dbunlock(); // unlock the mutex
    return 0;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return -1;
}


// Author: S.Ventura
// Modified: AP, Oct10 2006 (Tested)
int ccbmap::retrieve(int this_wheel, int this_sector, int this_station) {
  char mquer[LEN2];
  int rows=0, fields=0, resu=0;

  dtdbobj->dblock(); // lock the mutex

  sprintf(mquer,"SELECT ccbID,chamber,minicrate,on_line,port,secport,ccbserver,cmnt FROM ccbmap WHERE wheel='%d' AND sector='%d' AND station='%d'", this_wheel, this_sector, this_station);
  resu = dtdbobj->sqlquery(mquer,&rows,&fields);

  if (resu==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {
    char myfie[LEN1];

    wheel = this_wheel;
    sector = this_sector;
    station = this_station; 

    dtdbobj->returnfield(0,myfie);
    sscanf(myfie,"%d",&mId);
    dtdbobj->returnfield(1,myfie);
    sscanf(myfie,"%d",&chamber);
    dtdbobj->returnfield(2,myfie);
    sscanf(myfie,"%d",&minicrate);
    dtdbobj->returnfield(3,myfie);
    sscanf(myfie,"%d",&online);
    dtdbobj->returnfield(4,myfie);
    sscanf(myfie,"%d",&port);
    dtdbobj->returnfield(5,myfie);
    sscanf(myfie,"%d",&secport);
    dtdbobj->returnfield(6,myfie);
    strcpy(ccbserver,myfie);
    dtdbobj->returnfield(7,myfie);
    strcpy(comment,myfie);

    dtdbobj->dbunlock(); // unlock the mutex
    return 0;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return -1;
}

// Author: S.Ventura
// Modified: AP, Apr03 2007 (Tested on MySQL/ORACLE)
int ccbmap::isonline(int ccbid) {
  char mquer[LEN2];  
  int rows=0, fields=0, resu=0;

  dtdbobj->dblock(); // lock the mutex
  mId = ccbid;

  sprintf(mquer,"SELECT on_line FROM ccbmap WHERE ccbID='%d'",ccbid);
  resu = dtdbobj->sqlquery(mquer,&rows,&fields);

  if (resu==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {
    char myfie[LEN1];
    dtdbobj->returnfield(0,myfie);
    sscanf(myfie,"%d",&online);

    dtdbobj->dbunlock(); // unlock the mutex
    return online;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return -1;
}

// Author: S.Ventura
int ccbmap::setonline(int ccbid) {
  mId=ccbid;
  return setonline();
}

// Author: S.Ventura
// Modified: AP, Apr18 2007 (Tested on MySQL-ORACLE)
int ccbmap::setonline() {
  char mquer[LEN2];
  int rows=0, fields=0, resu=0;
  sprintf(mquer,"UPDATE ccbmap SET on_line=1 WHERE ccbID='%d'", mId);

  dtdbobj->dblock(); // lock the mutex
  resu=dtdbobj->sqlquery(mquer,&rows,&fields);
  dtdbobj->dbunlock(); // unlock the mutex

  return resu;
}

// Author: S.Ventura
int ccbmap::setoffline(int ccbid) {
  mId=ccbid;
  return setoffline();
}

// Author: S.Ventura
// Modified: AP, Apr18 2007 (Tested on MySQL-ORACLE)
int ccbmap::setoffline() {
  char mquer[LEN2];
  int rows=0, fields=0, resu=0;

  sprintf(mquer,"UPDATE ccbmap SET on_line=0 WHERE ccbID='%d'", mId);

  dtdbobj->dblock(); // lock the mutex
  resu=dtdbobj->sqlquery(mquer,&rows,&fields);
  dtdbobj->dbunlock(); // unlock the mutex

  return resu;
}


// Author: S.Ventura
int ccbmap::getport(int ccbid) {
  mId=ccbid;
  return getport();
}


// Author: S.Ventura
// Modified: AP, Apr03 2007 (Tested on MySQL-ORACLE)
int ccbmap::getport() {
  char mquer[LEN2];
  int rows=0, fields=0, resu=0;

  dtdbobj->dblock(); // lock the mutex

  sprintf(mquer,"SELECT port FROM ccbmap WHERE ccbID='%d'",mId);
  resu = dtdbobj->sqlquery(mquer,&rows,&fields);

  if (resu==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {
    char myfie[LEN1];

    dtdbobj->returnfield(0,myfie);
    sscanf(myfie,"%d",&port);

    dtdbobj->dbunlock(); // unlock the mutex
    return port;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return -1;
}


// Author: S.Ventura
int ccbmap::setport(int ccbid, int mport) {
  mId=ccbid;
  return setport(mport);
}

// Author: S.Ventura
// Modified: AP, Apr18 2007 (Tested on MySQL/Oracle)
int ccbmap::setport(int mport) {
  char mquer[LEN2];
  int rows=0, fields=0, resu=0;

  sprintf(mquer,"UPDATE ccbmap SET port=%d WHERE ccbID='%d'",mport,mId);

  dtdbobj->dblock(); // lock the mutex
  resu=dtdbobj->sqlquery(mquer,&rows,&fields);
  dtdbobj->dbunlock(); // unlock the mutex

  return resu;
}


// Author: A.Parenti, Oct10 2007 (Tested)
int ccbmap::getsecport(int ccbid) {
  mId=ccbid;
  return getport();
}


// Author: A.Parenti, Oct10 2007 (Tested)
int ccbmap::getsecport() {
  char mquer[LEN2];
  int rows=0, fields=0, resu=0;

  dtdbobj->dblock(); // lock the mutex

  sprintf(mquer,"SELECT secport FROM ccbmap WHERE ccbID='%d'",mId);
  resu = dtdbobj->sqlquery(mquer,&rows,&fields);

  if (resu==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {
    char myfie[LEN1];

    dtdbobj->returnfield(0,myfie);
    sscanf(myfie,"%d",&port);

    dtdbobj->dbunlock(); // unlock the mutex
    return port;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return -1;
}


// Author: A.Parenti, Oct10 2007 (Tested)
int ccbmap::setsecport(int ccbid, int mport) {
  mId=ccbid;
  return setsecport(mport);
}

// Author: A.Parenti, Oct10 2007 (Tested)
int ccbmap::setsecport(int mport) {
  char mquer[LEN2];
  int rows=0, fields=0, resu=0;

  sprintf(mquer,"UPDATE ccbmap SET secport=%d WHERE ccbID='%d'",mport,mId);

  dtdbobj->dblock(); // lock the mutex
  resu=dtdbobj->sqlquery(mquer,&rows,&fields);
  dtdbobj->dbunlock(); // unlock the mutex

  return resu;
}


// Author: A.Parenti, Nov07 2006
int ccbmap::setserver(int ccbid,char *ccbserver) {
  mId=ccbid;
  return setserver(ccbserver);
}


// Author: A.Parenti, Nov07 2006
// Modified: AP, Apr18 2007 (Tested on MySQL/Oracle)
int ccbmap::setserver(char *ccbserver) {
  char mquer[LEN2];
  int rows=0, fields=0, resu=0;

  sprintf(mquer,"UPDATE ccbmap SET ccbserver='%s' WHERE ccbID='%d'",ccbserver,mId);

  dtdbobj->dblock(); // lock the mutex
  resu=dtdbobj->sqlquery(mquer,&rows,&fields);
  dtdbobj->dbunlock(); // unlock the mutex

  return resu;
}


// Author: A.Parenti, Nov07 2006
int ccbmap::getserver(int ccbid,char *ccbserver) {
  mId=ccbid;
  return getserver(ccbserver);
}


// Author: A.Parenti, Nov07 2006
// Modified: AP, Apr03 2007 (Tested on MySQL-ORACLE)
int ccbmap::getserver(char *ccbserver) {
  char mquer[LEN2];
  int rows=0, fields=0, resu=0;

  dtdbobj->dblock(); // lock the mutex

  sprintf(mquer,"SELECT ccbserver FROM ccbmap WHERE ccbID='%d'",mId);
  resu = dtdbobj->sqlquery(mquer,&rows,&fields);

  if (resu==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {
    char myfie[LEN1];

    dtdbobj->returnfield(0,ccbserver);

    dtdbobj->dbunlock(); // unlock the mutex
    return 0;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return -1;
}


// Retrieve <BTItheta_R>, <BTItheta_z> and <BTItheta_sign> from table ccbgeom
// Author: A.Parenti, Mar01 2007
// Modified: AP, Apr03 2007 (Tested on Oracle/MySQL)
int ccbmap::retrieve_BTItheta(int ccbID, float *R, float *z, int *sign) {
  char mquer[LEN2];
  int rows=0, fields=0, resu=0;

  dtdbobj->dblock(); // lock the mutex

  *R = *z = *sign = 0;
  mId=ccbID;

  sprintf(mquer,"SELECT BTItheta_R,BTItheta_z,BTItheta_sign FROM ccbgeom WHERE ccbID='%d' ORDER by Datetime DESC",mId);
  resu = dtdbobj->sqlquery(mquer,&rows,&fields);

  if (resu==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {
    char myfie[LEN1];

    dtdbobj->returnfield(0,myfie);
    sscanf(myfie,"%f",R);

    dtdbobj->returnfield(1,myfie);
    sscanf(myfie,"%f",z);

    dtdbobj->returnfield(2,myfie);
    sscanf(myfie,"%d",sign);

    dtdbobj->dbunlock(); // unlock the mutex
    return 0;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return -1;
}


// Insert <BTItheta_R>, <BTItheta_z> and <BTItheta_sign> in table ccbgeom
// Author: A.Parenti, Mar01 2007
// Modified: AP, Apr18 2007 (Tested on Oracle/MySQL)
int ccbmap::insert_BTItheta(int ccbID, float R, float z, int sign) {
  char mquer[LEN2];
  int rows=0, fields=0, resu=0;

  mId=ccbID;
  sprintf(mquer,"INSERT INTO ccbgeom (ccbID,BTItheta_R,BTItheta_z,BTItheta_sign) VALUES ('%d','%f','%f','%d')",mId,R,z,sign);

  dtdbobj->dblock(); // lock the mutex
  resu=dtdbobj->sqlquery(mquer,&rows,&fields);
  dtdbobj->dbunlock(); // unlock the mutex

  return resu;
}
