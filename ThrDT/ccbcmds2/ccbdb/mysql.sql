-- This scripts create cmsdtdb tables in MySQL

/* LIST TABLES */
SHOW TABLES;

/* DROP TABLES */
--DROP TABLE ccbmap;
--DROP TABLE ccbstatus;
--DROP TABLE ccbdata;
--DROP TABLE refrelations;
--DROP TABLE ccbref;
--DROP TABLE configsets;
--DROP TABLE ccbrelations;
--DROP TABLE configs;
--DROP TABLE cfgautorel;
--DROP TABLE cfgrelations;
--DROP TABLE configcmds;
--DROP TABLE daqconfigs;
--DROP TABLE daqpartition;
--DROP TABLE daqrelations;
--DROP TABLE dcsdaqstatus;
--DROP TABLE dtpartitions;
--DROP TABLE dtrelations;
--DROP TABLE logger;
--DROP TABLE runfiles;
--DROP TABLE tbrun;
--DROP TABLE padcrelations;
--DROP TABLE padcdata;
--DROP TABLE padcstatus;


-- A.P. Aug09 2006
CREATE TABLE ccbmap (
  CcbID MEDIUMINT(5),
  ch_id_ofl MEDIUMINT(5), -- Chamber ID in offline DB
  wheel MEDIUMINT(5) NOT NULL,
  sector MEDIUMINT(5) NOT NULL,
  station MEDIUMINT(9) NOT NULL,
  chamber MEDIUMINT(5) DEFAULT '0',
  minicrate MEDIUMINT(9) DEFAULT '0',
  on_line TINYINT(4) NOT NULL DEFAULT '0',
  port MEDIUMINT(9) NOT NULL,
  ccbserver VARCHAR(255) DEFAULT '127.0.0.1' NOT NULL,
  PadcId MEDIUMINT(5) DEFAULT '0',
  cmnt VARCHAR(255) DEFAULT NULL,
  CONSTRAINT ccbmap_pk PRIMARY KEY (ccbID),
  CONSTRAINT ccbmap_uq UNIQUE (wheel,sector,station)
--  CONSTRAINT ccbmap_uq2 UNIQUE (port,ccbserver) -- useful or not?
);

-- A.P. Mar21 2006
CREATE TABLE ccbstatus (
  RunID INT(11) NOT NULL,
  ccbID MEDIUMINT(5) NOT NULL,
  statusstruct TEXT NOT NULL,
  statusxml VARCHAR(1024),
  time TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

-- A.P. Nov03 2006
CREATE TABLE ccbdata (
  RunID INT(11) NOT NULL,
  ccbID MEDIUMINT(5) NOT NULL,
  CmdCode VARCHAR(12) NOT NULL,
  Data TEXT NOT NULL,
  time TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

-- AP Apr28 2006
CREATE TABLE refrelations (
  CcbID MEDIUMINT(11),
  bootrefid INT(11) DEFAULT '0',
  statusrefid INT(11) DEFAULT '0',
  testrefid INT(11) DEFAULT '0',
  CONSTRAINT refrelations_pk PRIMARY KEY (CcbID)
);

-- A.P. Jan18 2006
CREATE TABLE ccbref (
  refid INT(11),
  mctype TINYINT(1) DEFAULT NULL CHECK (mctype BETWEEN 1 AND 7),
  reftype ENUM('boot','status','test'),
  refxml1 TEXT,
  refxml2 TEXT,
  refxml3 TEXT,
  cmnt VARCHAR(255) DEFAULT '',
  CONSTRAINT ccbref_pk PRIMARY KEY (refid)
);

-- AP after Luca Ciano May01 2006
CREATE TABLE configsets (
  ConfKey INT(11) AUTO_INCREMENT,
  Defa TINYINT(4) NOT NULL,
  Name VARCHAR(64) NOT NULL,
  Cmnt TEXT NOT NULL,
  Run INT(11) DEFAULT '0',
  ConfDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  CONSTRAINT configsets_pk PRIMARY KEY (ConfKey)
);

-- AP after Luca Ciano Apr28 2006
CREATE TABLE ccbrelations (
  ID INT(11) AUTO_INCREMENT,
  CcbID MEDIUMINT(11) NOT NULL,
  ConfKey INT(11) NOT NULL,
  ConfCcbKey INT(11) NOT NULL,
  CONSTRAINT ccbrelations_pk PRIMARY KEY (ID),
  CONSTRAINT ccbrelations_uq UNIQUE (CcbID,ConfKey)
);

-- A.P. Jan18 2006
CREATE TABLE configs (
  ConfID INT(11) AUTO_INCREMENT,
  ConfName VARCHAR(64) NOT NULL UNIQUE,
  ConfDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  CONSTRAINT configs_pk PRIMARY KEY (ConfID)
);

-- AP after Luca Ciano Oct23 2006
CREATE TABLE cfgautorel (
  ID INT(10) AUTO_INCREMENT,
  ConfKeyF INT(11) NOT NULL,
  ConfKeyS INT(11) NOT NULL,
  CONSTRAINT cfgautorel_pk PRIMARY KEY (ID),
  CONSTRAINT cfgautorel_uq UNIQUE (ConfKeyF,ConfKeyS)
);

-- AP after Luca Ciano Oct23 2006
CREATE TABLE cfgrelations (
  ID INT(11) AUTO_INCREMENT,
  ConfID INT(11) NOT NULL,
  CmdID BIGINT(20) NOT NULL,
  CONSTRAINT cfgrelations_pk PRIMARY KEY (ID),
  CONSTRAINT cfgrelations_uq UNIQUE (ConfID,CmdID)
);

-- A.P. Nov02 2006
CREATE TABLE configcmds (
  cmdid BIGINT(20) AUTO_INCREMENT,
  CmdName VARCHAR(32) DEFAULT '',    
  ConfToken VARCHAR(64) NOT NULL,
  ConfData TEXT NOT NULL,
  CONSTRAINT configcmds_pk PRIMARY KEY (cmdid),
  KEY ConfToken (ConfToken)
);

-- AP Apr12 2006
CREATE TABLE daqconfigs (
  ID INT(11) AUTO_INCREMENT,
  CmsID INT(11) NOT NULL,
  ConfName VARCHAR(32) NOT NULL,
  ConfData MEDIUMTEXT NOT NULL,
  CONSTRAINT daqconfigs_pk PRIMARY KEY (ID)
);

-- AP after Luca Ciano Apr12 2006
CREATE TABLE daqpartition (
  DaqPartitionID INT(11) AUTO_INCREMENT,
  PartName VARCHAR(32) NOT NULL,
  CmsID INT(11) NOT NULL,
  CONSTRAINT daqpartition_pk PRIMARY KEY (DaqPartitionID)
);

-- AP after Luca Ciano Apr12 2006
CREATE TABLE daqrelations (
  ID INT(11) AUTO_INCREMENT,
  ConfKey INT(11) NOT NULL,
  ConfDaqKey INT(11) NOT NULL,
  CONSTRAINT daqrelations_pk PRIMARY KEY (ID)
);

-- A.P. Jan18 2006
CREATE TABLE dcsdaqstatus (
  lastupdate TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  dtpart VARCHAR(32) NOT NULL,
  daqpart VARCHAR(32) NOT NULL,
  dtconfig VARCHAR(32) NOT NULL,
  status TINYINT(4) NOT NULL DEFAULT '0'
);

-- AP after Luca Ciano Nov03 2006
CREATE TABLE dtpartitions (
  PartKey INT(11) AUTO_INCREMENT,
  Run INT(11) DEFAULT '0',
  PartName VARCHAR(32) NOT NULL,
  PartDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  Defa TINYINT(4) NOT NULL,
  CONSTRAINT dtpartitions_pk PRIMARY KEY (PartKey),
  CONSTRAINT dtpartitions_uq UNIQUE (PartName)
);

-- AP after Luca Ciano Apr12 2006
CREATE TABLE dtrelations (
  ID INT(11) NOT NULL AUTO_INCREMENT,
  PartKey INT(11) NOT NULL,
  CcbID INT(11) NOT NULL,
  CONSTRAINT dtrelations_pk PRIMARY KEY (ID),
  CONSTRAINT dtrelations_uq UNIQUE KEY (PartKey,CcbID)
);

-- A.P. Jan18 2006
CREATE TABLE logger (
  msgid BIGINT(14) AUTO_INCREMENT,
  datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  levl ENUM('mesg','warn','erro') DEFAULT 'mesg',
  body LONGTEXT NOT NULL,
  runid INT(11) DEFAULT '0',
  CONSTRAINT logger_pk PRIMARY KEY (msgid),
  KEY datetime (datetime),
  KEY runid (runid)
);

-- A.P. Jan18 2006
CREATE TABLE runfiles (
  run INT(11) DEFAULT '0',
  filename VARCHAR(255) NOT NULL,
  evts INT(11) NOT NULL,
  writerid MEDIUMINT(8) NOT NULL,
  KEY run (run)
);

-- AP after Luca Ciano Apr12 2006
CREATE TABLE tbrun (
  run INT(11) DEFAULT '0',
  ConfKey INT(11) NOT NULL,
  PartKey INT(11) NOT NULL,
  runtimestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  type VARCHAR(40),
  runcomment VARCHAR(40),
  CONSTRAINT tbrun_pk PRIMARY KEY (run)
);

-- A.P. Aug01 2006
CREATE TABLE padcrelations (
  PadcId MEDIUMINT(5),
  ManifoldFeId MEDIUMINT(5) NOT NULL,
  ManifoldHvId MEDIUMINT(5) NOT NULL,
  Datetime TIMESTAMP,
  PadcDataId MEDIUMINT(5) AUTO_INCREMENT,
  CONSTRAINT padcrelations_pk PRIMARY KEY (PadcId,Datetime),
  CONSTRAINT padcrelations_uq UNIQUE (PadcDataId)
);

-- A.P. Aug01 2006
CREATE TABLE padcdata (
  PadcDataId MEDIUMINT(5),
  AdcCount MEDIUMINT(5),
  sens100A_HV FLOAT,
  sens100B_HV FLOAT,
  sens500_HV FLOAT,
  sensHV_Vcc FLOAT,
  sens100A_FE FLOAT,
  sens100B_FE FLOAT,
  sens500_FE FLOAT,
  sensFE_Vcc FLOAT,
  PADC_Vcc FLOAT,
  PADC_Vdd FLOAT,
  CONSTRAINT padcdata_pk PRIMARY KEY (PadcDataId,AdcCount)
);

-- A.P. Oct09 2006
CREATE TABLE padcstatus (
  padcID MEDIUMINT(5) NOT NULL,
  adccount VARCHAR(256) NOT NULL,
  statusxml VARCHAR(1024),
  time TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

/* DESCRIBE TABLES */
--DESCRIBE ccbmap;
--DESCRIBE ccbstatus;
--DESCRIBE ccbdata;
--DESCRIBE refrelations;
--DESCRIBE ccbref;
--DESCRIBE ccbrelations;
--DESCRIBE configs;
--DESCRIBE cfgautorel;
--DESCRIBE cfgrelations;
--DESCRIBE configcmds;
--DESCRIBE daqconfigs;
--DESCRIBE daqpartition;
--DESCRIBE daqrelations;
--DESCRIBE dcsdaqstatus;
--DESCRIBE dtpartitions;
--DESCRIBE dtrelations;
--DESCRIBE logger;
--DESCRIBE runfiles;
--DESCRIBE tbrun;
--DESCRIBE padcrelations;
--DESCRIBE padcdata;
--DESCRIBE padcstatus;


/* LIST TABLES */
SHOW TABLES;
