-- This script creates cmsdtdb tables in ORACLE

/* LIST TABLES */
SELECT TABLE_NAME FROM USER_TABLES;

/* DROP TABLES */
--DROP TABLE ccbmap;
--DROP TABLE confmask;
--DROP TABLE ccbgeom;
--DROP TABLE ccbstatus;
--DROP TABLE robstatus;
--DROP TABLE ccbdata;
--DROP TABLE refrelations;
--DROP TABLE ccbref;
--DROP TABLE configsets;
--DROP TABLE ccbrelations;
--DROP TABLE configs;
--DROP TABLE cfg2brkrel;
--DROP TABLE cfgbricks;
--DROP TABLE cfgrelations;
--DROP TABLE configcmds;
--DROP TABLE daqconfigs;
--DROP TABLE daqpartition;
--DROP TABLE daqrelations;
--DROP TABLE dcsdaqstatus;
--DROP TABLE dtpartitions;
--DROP TABLE dtrelations;
--DROP TABLE logger;
--DROP TABLE runfiles;
--DROP TABLE tbrun;
--DROP TABLE padcrelations;
--DROP TABLE padcdata;
--DROP TABLE padcstatus;
--DROP TABLE daccalibs;


-- A.P. Aug09 2006
-- Modified: AP, Oct10 2007
CREATE TABLE ccbmap (
  CcbID NUMBER(7),
  ch_id_ofl NUMBER(7), -- Chamber ID in offline DB
  wheel NUMBER(7) NOT NULL,
  sector NUMBER(7) NOT NULL,
  station NUMBER(7) NOT NULL,
  chamber NUMBER(7) DEFAULT '0',
  minicrate NUMBER(7) DEFAULT '0',
  on_line NUMBER(3) DEFAULT '0' NOT NULL,
  port NUMBER(7) NOT NULL,
  secport NUMBER(7) NOT NULL,
  ccbserver VARCHAR2(255) DEFAULT '127.0.0.1' NOT NULL,
  PadcId NUMBER(7) DEFAULT '0',
  cmnt VARCHAR2(255) DEFAULT '',
  CONSTRAINT ccbmap_pk PRIMARY KEY (ccbID),
  CONSTRAINT ccbmap_uq UNIQUE (wheel,sector,station)
--  CONSTRAINT ccbmap_uq2 UNIQUE (port,ccbserver) -- useful or not?
);


-- A.P. Oct28 2007
CREATE TABLE confmask (
  ccbID NUMBER(7),
  maskxml VARCHAR2(2048),
  Datetime TIMESTAMP(0) DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT confmask_pk PRIMARY KEY (ccbID,Datetime)
);

-- A.P. Mar13 2007
CREATE TABLE ccbgeom (
  CcbID NUMBER(7),
  BTItheta_R NUMBER,
  BTItheta_z NUMBER,
  BTItheta_sign NUMBER(3),
  Datetime TIMESTAMP(0) DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT ccbgeom_pk PRIMARY KEY (ccbID,Datetime)
);

-- A.P. Mar21 2006
-- Modified: AP Oct04 2007
CREATE TABLE ccbstatus (
  RunID NUMBER(10) NOT NULL,
  ccbID NUMBER(7) NOT NULL,
--  statusstruct CLOB NOT NULL,
  statusstruct VARCHAR2(1024) NOT NULL,
  statusxml VARCHAR2(2048),
  time TIMESTAMP(0) DEFAULT CURRENT_TIMESTAMP
);

-- Create and index for ccbstatus
CREATE INDEX ccbstatus_idx ON ccbstatus (ccbID);

-- A.P. May16 2007
CREATE TABLE robstatus (
  RunID NUMBER(10) NOT NULL,
  ccbID NUMBER(7) NOT NULL,
  RobErrorState VARCHAR(64),
  McTempRob VARCHAR2(64),
  ReadRobPwrVcc VARCHAR2(64),
  ReadRobPwrVdd VARCHAR2(64),
  ReadRobPwrCurrent VARCHAR2(64),
  time TIMESTAMP(0) DEFAULT CURRENT_TIMESTAMP
);

-- Create and index for robstatus
CREATE INDEX robstatus_idx ON robstatus (ccbID);


-- A.P. Nov03 2006
-- Modified: AP Mar15 2007
CREATE TABLE ccbdata (
  RunID NUMBER(10) NOT NULL,
  ccbID NUMBER(7) NOT NULL,
  CmdCode VARCHAR2(12) NOT NULL,
--  Data CLOB NOT NULL,
  Data VARCHAR2(1024) NOT NULL,
  time TIMESTAMP(0) DEFAULT CURRENT_TIMESTAMP
);

-- AP Apr28 2006
-- Modified: AP, Jul25 2007
CREATE TABLE refrelations (
  CcbID NUMBER(7),
  bootrefid NUMBER(10) DEFAULT '0',
  statusrefid NUMBER(10) DEFAULT '0',
  testrefid NUMBER(10) DEFAULT '0',
  robrefid NUMBER(10) DEFAULT '0',
  padcrefid NUMBER(10) DEFAULT '0',
  CONSTRAINT refrelations_pk PRIMARY KEY (CcbID)
);

-- A.P. Jan18 2006
-- Modified: AP Jul25 2007
CREATE TABLE ccbref (
  refid NUMBER(10),
  mctype NUMBER(1) DEFAULT NULL CHECK (mctype BETWEEN 1 AND 7),
  reftype CHAR(6) CHECK (reftype IN ('boot','status','test','rob','padc')),
  refxml1 VARCHAR2(4000),
  refxml2 VARCHAR2(4000),
  refxml3 VARCHAR2(4000),
  cmnt VARCHAR2(255) DEFAULT '',
  CONSTRAINT ccbref2_pk PRIMARY KEY (refid)
);

-- BEGIN CONFIGSETS --
-- AP after Luca Ciano Apr12 2006
-- Modified: AP, Dec28 2006
-- Modified: LC & AP, Aug09 2007
CREATE TABLE configsets (
  ConfKey NUMBER(10),
  Defa NUMBER(3) NOT NULL,
  Name VARCHAR2(64) NOT NULL,
  Cmnt VARCHAR2(255) DEFAULT '',
  Run NUMBER(10) DEFAULT '0',
  ConfDate TIMESTAMP(0) DEFAULT CURRENT_TIMESTAMP,
  obso TIMESTAMP(0) DEFAULT NULL,
  CONSTRAINT configsets_pk PRIMARY KEY (ConfKey)
);

-- Emulates the MySQL AUTO_TRIGGER
create sequence configsets_seq
start with 1
increment by 1
nomaxvalue;

create trigger configsets_trigger
before insert on configsets
for each row
begin
select configsets_seq.nextval into :new.ConfKey from dual;
end;
/
-- END CONFIGSETS --

-- BEGIN CCBRELATIONS --
-- AP after Luca Ciano Apr28 2006
CREATE TABLE ccbrelations (
  ID NUMBER(10),
  CcbID NUMBER(7) NOT NULL,
  ConfKey NUMBER(10) NOT NULL,
  ConfCcbKey NUMBER(10) NOT NULL,
  CONSTRAINT ccbrelations_pk PRIMARY KEY (ID),
  CONSTRAINT ccbrelations_uq UNIQUE (CcbID,ConfKey)
);

-- Emulates the MySQL AUTO_TRIGGER
create sequence ccbrelations_seq
start with 1
increment by 1
nomaxvalue;

create trigger ccbrelations_trigger
before insert on ccbrelations
for each row
begin
select ccbrelations_seq.nextval into :new.ID from dual;
end;
/
-- END CCBRELATIONS --

-- BEGIN CONFIGS --
-- A.P. Jan20 2006
-- Modified: LC & AP, Aug09 2007
CREATE TABLE configs (
  ConfID NUMBER(10),
  ConfName VARCHAR2(64) NOT NULL UNIQUE,
  ConfDate TIMESTAMP(0) DEFAULT CURRENT_TIMESTAMP,
  obso TIMESTAMP(0) DEFAULT NULL,
  CONSTRAINT configs_pk PRIMARY KEY (ConfID)
);
--   confdate ON UPDATE CURRENT_TIMESTAMP -- How to do in Oracle ??

-- Emulates the MySQL AUTO_TRIGGER
create sequence configs_seq
start with 1
increment by 1
nomaxvalue;

create trigger configs_trigger
before insert on configs
for each row
begin
select configs_seq.nextval into :new.ConfID from dual;
end;
/
-- END CONFIGS --

-- BEGIN CFG2BRKREL --
-- AP & Luca Ciano Mar08 2007
CREATE TABLE cfg2brkrel (
  ID NUMBER(10),
  ConfID NUMBER(10) NOT NULL,
  BrkID NUMBER(10) NOT NULL,
  CONSTRAINT cfg2brkrel_pk PRIMARY KEY  (ID),
  CONSTRAINT cfg2brkrel_uq UNIQUE (ConfID,BrkID)
);

-- Emulates the MySQL AUTO_TRIGGER
create sequence cfg2brkrel_seq
start with 1
increment by 1
nomaxvalue;

create trigger cfg2brkrel_trigger
before insert on cfg2brkrel
for each row
begin
select cfg2brkrel_seq.nextval into :new.ID from dual;
end;
/
-- END CFG2BRKREL --


-- BEGIN CFGBRICKS --
-- AP & Luca Ciano Mar14 2007
-- Modified: LC & AP, Aug09 2007
CREATE TABLE cfgbricks (
  BrkID NUMBER(10),
  BrkName VARCHAR2(64) NOT NULL,
  Cmnt VARCHAR2(32) DEFAULT NULL,
  BrkType NUMBER(3) NOT NULL,
  obso TIMESTAMP(0) DEFAULT NULL,
  CONSTRAINT cfgbricks_pk PRIMARY KEY (BrkID),
  CONSTRAINT cfgbricks_uq UNIQUE (BrkName,BrkType)
);

-- Emulates the MySQL AUTO_TRIGGER
create sequence cfgbricks_seq
start with 1
increment by 1
nomaxvalue;

create trigger cfgbricks_trigger
before insert on cfgbricks
for each row
begin
select cfgbricks_seq.nextval into :new.BrkID from dual;
end;
/
-- END CFGBRICKS --


-- BEGIN CFGRELATIONS --
-- AP after Luca Ciano Mar08 2007
CREATE TABLE cfgrelations (
  ID NUMBER(20),
  BrkID NUMBER(10) NOT NULL,
  CmdID NUMBER(20) NOT NULL,
  CONSTRAINT cfgrelations_pk PRIMARY KEY (ID),
  CONSTRAINT cfgrelations_uq UNIQUE (BrkID,CmdID)
);

-- Emulates the MySQL AUTO_TRIGGER
create sequence cfgrelations_seq
start with 1
increment by 1
nomaxvalue;

create trigger cfgrelations_trigger
before insert on cfgrelations
for each row
begin
select cfgrelations_seq.nextval into :new.ID from dual;
end;
/
-- END CFGRELATIONS --

-- BEGIN CONFIGCMDS --
-- A.P. Nov02 2006
-- Modified: AP, Mar15 2007
CREATE TABLE configcmds (
  cmdid NUMBER(20),
  CmdName VARCHAR2(32) DEFAULT '',    
  ConfToken VARCHAR2(64) NOT NULL,
--  ConfData CLOB NOT NULL,
  ConfData VARCHAR2(1024) NOT NULL,
  CONSTRAINT configcmds_pk PRIMARY KEY (cmdid)
);
--  KEY `ConfToken` (`ConfToken`) -- How to do in Oracle ?

-- Emulates the MySQL AUTO_TRIGGER
create sequence configcmds_seq
start with 1
increment by 1
nomaxvalue;

create trigger configcmds_trigger
before insert on configcmds
for each row
begin
select configcmds_seq.nextval into :new.cmdid from dual;
end;
/
-- END CONFIGCMDS --

-- BEGIN daqconfigs --
-- AP Apr12 2006
CREATE TABLE daqconfigs (
  ID NUMBER(10),
  CmsID NUMBER(11) NOT NULL,
  ConfName VARCHAR2(32) NOT NULL,
  ConfData CLOB NOT NULL,
  CONSTRAINT daqconfigs_pk PRIMARY KEY (ID)
);

-- Emulates the MySQL AUTO_TRIGGER
create sequence daqconfigs_seq
start with 1
increment by 1
nomaxvalue;

create trigger daqconfigs_trigger
before insert on daqconfigs
for each row
begin
select daqconfigs_seq.nextval into :new.ID from dual;
end;
/
-- END DAQCONFIGS --

-- BEGIN DAQPARTITION --
-- AP after Luca Ciano Apr12 2006
CREATE TABLE daqpartition (
  DaqPartitionID NUMBER(10),
  PartName VARCHAR2(32) NOT NULL,
  CmsID NUMBER(10) NOT NULL,
  CONSTRAINT daqpartition_pk PRIMARY KEY (DaqPartitionID)
);

-- Emulates the MySQL AUTO_TRIGGER
create sequence daqpartition_seq
start with 1
increment by 1
nomaxvalue;

create trigger daqpartition_trigger
before insert on daqpartition
for each row
begin
select daqpartition_seq.nextval into :new.DaqPartitionID from dual;
end;
/
-- END DAQPARTITION --

-- BEGIN DAQRELATIONS --
-- AP after Luca Ciano Apr12 2006
CREATE TABLE daqrelations (
  ID NUMBER(10),
  ConfKey NUMBER(10) NOT NULL,
  ConfDaqKey NUMBER(10) NOT NULL,
  CONSTRAINT daqrelations_pk PRIMARY KEY (ID)
);

-- Emulates the MySQL AUTO_TRIGGER
create sequence daqrelations_seq
start with 1
increment by 1
nomaxvalue;

create trigger daqrelations_trigger
before insert on daqrelations
for each row
begin
select daqrelations_seq.nextval into :new.ID from dual;
end;
/
-- END DAQRELATIONS --

-- A.P. Jan20 2006
CREATE TABLE dcsdaqstatus (
  lastupdate TIMESTAMP(0) DEFAULT CURRENT_TIMESTAMP,
  dtpart VARCHAR2(32) NOT NULL,
  daqpart VARCHAR2(32) NOT NULL,
  dtconfig VARCHAR2(32) NOT NULL,
  status NUMBER(3) DEFAULT '0'
);
--  lastupdate on update CURRENT_TIMESTAMP -- How to do in Oracle ??

-- BEGIN DTPARTITIONS --
-- AP after Luca Ciano Apr17 2006
-- Modified: AP, Oct29 2007
CREATE TABLE dtpartitions (
  PartKey NUMBER(10),
  Run NUMBER(10) DEFAULT '0',
  PartName VARCHAR2(32) NOT NULL,
  PartDate TIMESTAMP(0) DEFAULT CURRENT_TIMESTAMP,
  Defa NUMBER(3) NOT NULL,
  obso TIMESTAMP(0) DEFAULT NULL,
  CONSTRAINT dtpartitions_pk PRIMARY KEY (PartKey),
  CONSTRAINT dtpartitions_uq UNIQUE (PartName)
);

-- Emulates the MySQL AUTO_TRIGGER
create sequence dtpartitions_seq
start with 1
increment by 1
nomaxvalue;

create trigger dtpartitions_trigger
before insert on dtpartitions
for each row
begin
select dtpartitions_seq.nextval into :new.PartKey from dual;
end;
/
-- END DTPARTITIONS --

-- BEGIN DTRELATIONS --
-- AP after Luca Ciano Apr12 2006
CREATE TABLE dtrelations (
  ID NUMBER(10) NOT NULL,
  PartKey NUMBER(10) NOT NULL,
  CcbID NUMBER(10) NOT NULL,
  CONSTRAINT dtrelations_pk PRIMARY KEY (ID),
  CONSTRAINT dtrelations_uq UNIQUE (PartKey,CcbID)
);

-- Emulates the MySQL AUTO_TRIGGER
create sequence dtrelations_seq
start with 1
increment by 1
nomaxvalue;

create trigger dtrelations_trigger
before insert on dtrelations
for each row
begin
select dtrelations_seq.nextval into :new.ID from dual;
end;
/
-- END DTRELATIONS --

-- BEGIN LOGGER --
-- A.P. Jan20 2006
-- Modified: AP, Dec28 2006
CREATE TABLE logger (
  msgid NUMBER(20),
  datetime TIMESTAMP(0) DEFAULT CURRENT_TIMESTAMP,
  levl CHAR(4) DEFAULT 'mesg' CHECK (levl IN ('mesg','warn','erro')),
  body VARCHAR2(1024) NOT NULL,
  runid NUMBER(10) DEFAULT '0',
  CONSTRAINT logger_pk PRIMARY KEY (msgid)
);
--  datetime on update CURRENT_TIMESTAMP -- How to do in Oracle?
--  KEY datetime (datetime)
--  KEY runid (runid)

-- Emulates the MySQL AUTO_TRIGGER
create sequence logger_seq
start with 1
increment by 1
nomaxvalue;

create trigger logger_trigger
before insert on logger
for each row
begin
select logger_seq.nextval into :new.msgid from dual;
end;
/
-- END LOGGER --

-- A.P. Dec23 2005
CREATE TABLE runfiles (
  run NUMBER(10) DEFAULT '0',
  filename VARCHAR2(255) NOT NULL,
  evts NUMBER(10) NOT NULL,
  writerid NUMBER(7) NOT NULL
);
-- KEY run (run)

-- AP after Luca Ciano Apr12 2006
CREATE TABLE tbrun (
  run NUMBER(10) DEFAULT '0',
  ConfKey NUMBER(10) NOT NULL,
  PartKey NUMBER(10) NOT NULL,
  runtimestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  type VARCHAR2(40),
  runcomment VARCHAR2(40),
  CONSTRAINT tbrun_pk PRIMARY KEY (run)
);

-- BEGIN PADCRELATIONS --
-- A.P. Aug01 2006
CREATE TABLE padcrelations (
  PadcId NUMBER(7),
  ManifoldFeId NUMBER(7) NOT NULL,
  ManifoldHvId NUMBER(7) NOT NULL,
  Datetime TIMESTAMP(0),
  PadcDataId NUMBER(7),
  CONSTRAINT padcrelations_pk PRIMARY KEY (PadcId,Datetime),
  CONSTRAINT padcrelations_uq UNIQUE (PadcDataId)
);

-- Emulates the MySQL AUTO_TRIGGER
create sequence padcrelations_seq
start with 1
increment by 1
nomaxvalue;

create trigger padcrelations_trigger
before insert on padcrelations
for each row
begin
select padcrelations_seq.nextval into :new.PadcDataId from dual;
end;
/
-- END PADCRELATIONS --

-- A.P. Aug01 2006
CREATE TABLE padcdata (
  PadcDataId NUMBER(7),
  AdcCount NUMBER(7),
  sens100A_HV NUMBER,
  sens100B_HV NUMBER,
  sens500_HV NUMBER,
  sensHV_Vcc NUMBER,
  sens100A_FE NUMBER,
  sens100B_FE NUMBER,
  sens500_FE NUMBER,
  sensFE_Vcc NUMBER,
  PADC_Vcc NUMBER,
  PADC_Vdd NUMBER,
  CONSTRAINT padcdata_pk PRIMARY KEY (PadcDataId,AdcCount)
);

-- A.P. Oct09 2006
CREATE TABLE padcstatus (
  padcID NUMBER(7) NOT NULL,
  adccount VARCHAR2(256) NOT NULL,
  statusxml VARCHAR2(1024),
  time TIMESTAMP(0) DEFAULT CURRENT_TIMESTAMP
);

-- Create and index for padcstatus
CREATE INDEX padcstatus_idx ON padcstatus (padcID);


-- A.P. Jul20 2007
CREATE TABLE daccalibs (
  TTCid NUMBER(7) NOT NULL,
  width_gain NUMBER,
  bias_gain1 NUMBER,
  bias_gain2 NUMBER,
  bias_gain3 NUMBER,
  thr_gain1 NUMBER,
  thr_gain2 NUMBER,
  thr_gain3 NUMBER,
  width_ofst NUMBER,
  bias_ofst1 NUMBER,
  bias_ofst2 NUMBER,
  bias_ofst3 NUMBER,
  thr_ofst1 NUMBER,
  thr_ofst2 NUMBER,
  thr_ofst3 NUMBER,
  Datetime TIMESTAMP(0) NOT NULL,
  CONSTRAINT daccalibs_pk PRIMARY KEY (TTCid,Datetime)
);



/* DESCRIBE TABLES */
--DESCRIBE ccbmap;
--DESCRIBE confmask;
--DESCRIBE ccbgeom;
--DESCRIBE ccbstatus;
--DESCRIBE robstatus;
--DESCRIBE ccbdata;
--DESCRIBE ccbref;
--DESCRIBE refrelations;
--DESCRIBE configsets;
--DESCRIBE ccbrelations;
--DESCRIBE configs;
--DESCRIBE cfg2brkrel;
--DESCRIBE cfgbricks;
--DESCRIBE cfgrelations;
--DESCRIBE configcmds;
--DESCRIBE daqconfigs;
--DESCRIBE daqpartition;
--DESCRIBE daqrelations;
--DESCRIBE dcsdaqstatus;
--DESCRIBE dtpartitions;
--DESCRIBE dtrelations;
--DESCRIBE logger;
--DESCRIBE runfiles;
--DESCRIBE tbrun;
--DESCRIBE padcrelations;
--DESCRIBE padcdata;
--DESCRIBE padcstatus;
--DESCRIBE daccalibs;

/* LIST TABLES */
SELECT TABLE_NAME FROM USER_TABLES;
