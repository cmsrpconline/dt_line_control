// Dummy ccb server
// Author: A.Parenti, Nov29 2006
// Modified: AP, Dec07 2007

#include <iostream>
#include <ptime.h>
#include <pasync.h>
#include <pinet.h>

#include <unistd.h>

USING_PTYPES

const int srvport = 18889;
const int hlen=12; // header length
const int maxclients = 500;

// This serves a single connection
void *clnthread(void *cln) {
  ipstream *client = (ipstream*)cln;

  try {
    while (!client->get_eof()) { // NB get_eof()=true when connection is closed by client
      unsigned char header[hlen]; // header
      unsigned char received[243], reply[300]; // payload, reply
      short i, ccbid; // index, ccbid
      int qlen, rlen; // query length, reply lenght

      client->read(header,hlen); // Read header

      ccbid = (header[2]<<8)+header[3];
      qlen = (header[4]<<24)+(header[5]<<16)+(header[6]<<8)+header[7];
      client->read(received,qlen-hlen); // Read payload

      printf("Received %d chars: 0x",qlen);
      for (i=0;i<hlen;++i)
        printf("%02x",header[i]);
      for (i=0;i<qlen-hlen;++i)
        printf("%02x",received[i]);
      printf("\n");

// Create header for the reply
      for (i=0;i<hlen;++i)
        reply[i]=header[i];

// Simulates corrupted header
//        reply[0]=0xBC;

      if (false) {
// Simulates CCB error/timeout
        for (i=1;i<=4;++i)
          reply[hlen-i]=0xFF;
        rlen=1;
//        reply[hlen]=1; // RunOn error
        reply[hlen]=2; // CCB timeout
//        reply[hlen]=3; // CRC error
//        reply[hlen]=4; // Other error

        int timeout=(header[8]<<24)+(header[9]<<16)+(header[10]<<8)+header[11];
        pt::psleep(timeout);
      }
      else if (received[0]==0xea) { // MC status
        unsigned char stdstatus[]="\x13\x00\x00\x00\x01\x00\x14\x00\x03\xFF\xEF\xDF\x7F\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x07\xAE\x50\x00\x00\x00\x00\x02\x0C\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x5A\xBC\x00\x03\x7B\xB8\x00\x02\x4F\x8E\x00\x03\x69\x9A\x00\x02\x78\x52\x00\x01\x63\xA8\x00\x02\x78\x52\x00\x01\x63\x84\x00\x02\x50\x95\x00\x03\x50\xCE\x00\x03\x51\x18\x00\x03\x4E\x00\x00\x02\x4D\xDC\x00\x02\x4D\xDC\x00\x02\x53\x60\x00\x03\x53\x84\x00\x02\x5F\xFA\x00\x01\x5F\xFA\x00\x01\x5F\xFA\x00\x01\x66\xE6\xFF\xFB\x66\xE6\xFF\xFB\x66\xE6\xFF\xFB\x46\x80\x00\x02\x01\x6F\x00\xE7\x01\x69\x00\xE1\x01\x6F\x00\xE7\x41\x00\x00\x06\x5A\x04\x00\x00\x5A\xBC\x00\x03\x5B\x06\x00\x03\x7B\x70\x00\x02\x7B\xB8\x00\x02\x53\x3C\x00\x03\x53\x84\x00\x03\x53\x60\x00\x02\x53\xA8\x00\x02\x02\xA3";

        for (i=0;i<sizeof(stdstatus);++i)
          reply[hlen+i]=stdstatus[i];

        rlen=sizeof(stdstatus)-1;
//        reply[hlen+1]=header[2]; // ccbid
//        reply[hlen+2]=header[3]; // ccbid
//
        reply[hlen+1]=0x00; // ccbid
        reply[hlen+2]=0x45; // ccbid


        reply[hlen+3]=0;  // firmware: HVer
        reply[hlen+4]=1;  // firmware: HVer
        reply[hlen+5]=0;  // firmware: LVer
        reply[hlen+6]=20; // firmware: LVer
      }
      else if (received[0]==0x10 || received[0]==0x12) { // MC test
        unsigned char stdtest[]="\x11\xFF\xFF\x00\x05\x00\x05\x00\x05\x00\x05\x00\x05\x00\x05\x00\x05\x00\x05\x00\x05\x00\x05\x00\x05\x00\x02\x00\x02\x00\x02\x00\x02\x00\x02\x00\x02\x00\x02\x00\x02\x00\x02\x00\x02\x00\x02\x00\x02\x00\x02\x00\x02\x00\x02\x00\x02\x00\x02\x00\x02\x00\x02\x00\x02\x00\x02\x00\x02\x00\x02\x00\x02\x00\x02\x00\x02\x00\x02\x00\x02\x00\x02\x00\x02\x00\x02\x00\x02\x03\x7f\x04\x03\x00\x00\xdf\x00\x00\x02\x00\x04\x00\x08\x00\x10\x00\x20\x00\x00\x00\x80\x80\x00\x04\x25\x04\x25\x04\x25\x04\x25\x04\x25\x00\x00\x04\x20\x04\x20\x01\x01\x01\x01\x01\x00\x01\x01\x01\xf4\x01\xf4\x01\xf4\x01\xf4\x01\xf4\x04\xf4\x01\xf4\x01\xf4\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xff\xff\x7f\x00\x00\x01\x00\x02\x00\x04\x00\x08\x00\x10\x00\x20\x00\x40\x00\x05\x04\x05\x04\x05\x04\x05\x04\x05\x04\x05\x04\x05\x04\x01\x01\x01\x01\x01\x01\x01\x00\x0a\x00\x0a\x00\x0a\x00\x0a\x00\x0a\x00\x0a\x00\x0a\x00\x03\x00\x00\x00\x06\x60\x00\x00\x02\x60\x00\x00\x02\x60\x00\x00\x02";

        for (i=0;i<sizeof(stdtest);++i)
          reply[hlen+i]=stdtest[i];

        rlen=sizeof(stdtest);
      }
      else if (received[0]==0x43) { // Read TDC
        rlen=99;
        reply[hlen]=0x22; // reply code
        for (i=0;i<98;++i)
          reply[hlen+1+i]=0; // reset all

        reply[hlen+98]=0x84; // TDC ID Code
        reply[hlen+97]=0x70; // TDC ID Code
        reply[hlen+96]=0xDA; // TDC ID Code
        reply[hlen+95]=0xCE; // TDC ID Code
// Bytes 87-94 are the status
//        reply[hlen+87]=255; // BIT0
//        reply[hlen+88]=255; // BIT1
//        reply[hlen+89]=255; // BIT2
//        reply[hlen+90]=255; // BIT3
//        reply[hlen+91]=255;// BIT4
//        reply[hlen+92]=255;// BIT5
//        reply[hlen+93]=255;// BIT6
        reply[hlen+94]=0x10; // // BIT7 (DLL in lock)
      }
      else if (received[0]==0x27){ // WrTTCrx
        rlen=3;
        reply[hlen]=0xfc;
        reply[hlen+1]=0x27;
        reply[hlen+2]=0;
      }
      else if (received[0]==0x28){ // RdTTCrx
        rlen=3;
        reply[hlen]=0x29;
        reply[hlen+1]=0;
        reply[hlen+2]=0;
      }
      else if (received[0]==0xe7){ // Auto LINK set (boot)
        rlen=3;
        reply[hlen]=0xFC;
        reply[hlen+1]=0xE7;
        reply[hlen+2]=1;
      }
      else if (received[0]==0xe4){ // LINK test (boot)
        rlen=0;
      }
      else if (received[0]==0xe6){ // Reset SEU (boot)
        rlen=2;
        reply[hlen]=0xFC;
        reply[hlen+1]=0xE6;
      }
      else if (received[0]==0xe5){ // Link DAC (boot)
        rlen=2;
        reply[hlen]=0xFC;
        reply[hlen+1]=0xE5;
      }
      else if (received[0]==0xe3){ // TTC write (boot)
        rlen=3;
        reply[hlen]=0xFC;
        reply[hlen+1]=0xE3;
        reply[hlen+2]=0;
      }
      else if (received[0]==0xe2){ // TTC read (boot)
        rlen=3;
        reply[hlen]=0xE1;
        reply[hlen+1]=0xFF;
        reply[hlen+2]=0;
      }
      else if (received[0]==0xe0){ // TTC find (boot)
        rlen=3;
        reply[hlen]=0xFC;
        reply[hlen+1]=0xE0;
        reply[hlen+2]=0;
      }
      else if (received[0]==0xF7){ // HD Watchdog restart
        rlen=2;
        reply[hlen]=0xFC;
        reply[hlen+1]=0xF7;
      }
      else if (received[0]==0xA3){ // GetConfigCRC
        rlen=3;
        reply[hlen]=0xA4;
        reply[hlen+1]=reply[hlen+2]=0xFF;
      }
      else if (received[0]==0x40){ // save_MC_conf
//        rlen=3; // until v1.16
//        reply[hlen]=0xfc;
//        reply[hlen+1]=0x40;
//        reply[hlen+2]=0xFF;
//
        rlen=4; // from v1.17
        reply[hlen]=0xfc;
        reply[hlen+1]=0x40;
        reply[hlen+2]=0xFF;
        reply[hlen+3]=0xFF;
      }
      else if (received[0]==0x61){ // save_MC_conf
//        rlen=3; // until v1.16
//        reply[hlen]=0xfc;
//        reply[hlen+1]=0x61;
//        reply[hlen+2]=0xff;
//
        rlen=4; // from v1.17
        reply[hlen]=0xfc;
        reply[hlen+1]=0x61;
        reply[hlen+2]=0xFF;
        reply[hlen+3]=0xFF;
      }
      else if (received[0]==0xA6){ // find_sensors
        rlen=169;
        reply[hlen]=0xA7;
        for (i=0;i<168;++i)
          reply[hlen+1+i]=0x00;
      }
      else if (received[0]==0x8F){ // read_crate_sensor_id
        rlen=169;
        reply[hlen]=0x90;
        for (i=0;i<168;++i)
          reply[hlen+1+i]=0x00;
      }
      else if (received[0]==0x38 || received[0]==0x3a || received[0]==0x3b){
// read_FE_mask, set_FE_mask, set_FE_mask_ch
        rlen=217;
        reply[hlen]=0x39;
        for (i=0;i<216;++i)
          reply[hlen+1+i]=0x00;
      }
      else if (received[0]==0x9a){ // set_SL_mask
        rlen=3;
        reply[hlen]=0xfc;
        reply[hlen+1]=0x9a;
        reply[hlen+2]=0;
      }
      else if (received[0]==0x19) { // read_BTI
        reply[hlen]=0x1A;

        for (i=0;i<37;++i)
          reply[hlen+i+1]=i;

        rlen=38;
      }
      else if (received[0]==0x59) { // read_BTI_6b
        reply[hlen]=0x5A;

        for (i=0;i<48;++i)
          reply[hlen+i+1]=i;

        rlen=49;
      }
      else if (received[0]==0x14) { // write_BTI
        reply[hlen]=0xFC;
        reply[hlen+1]=0x14;
        reply[hlen+2]=0x1;
        rlen=3;
      }
      else if (received[0]==0x54) { // write_BTI_6b
        reply[hlen]=0xFC;
        reply[hlen+1]=0x54;
        reply[hlen+2]=0x1;
        rlen=3;
      }
      else if (received[0]==0x51) { // enable_BTI
        reply[hlen]=0xFC;
        reply[hlen+1]=0x51;
        reply[hlen+2]=0x1;
        rlen=3;
      }
      else if (received[0]==0x52) { // load_BTI_emul
        reply[hlen]=0xFC;
        reply[hlen+1]=0x52;
        reply[hlen+2]=0x1;
        rlen=3;
      }
      else if (received[0]==0x53) { // start_BTI_emul
        reply[hlen]=0xFC;
        reply[hlen+1]=0x53;
        reply[hlen+2]=0x1;
        rlen=3;
      }
      else if (received[0]==0x79) { // new_start_BTI_emul
        reply[hlen]=0xFC;
        reply[hlen+1]=0x79;
        reply[hlen+2]=0x1;
        rlen=3;
      }
      else if (received[0]==0x9C) { // compare_BTI_register
        reply[hlen]=0x9D;
        for (i=0;i<24;++i)
          reply[hlen+i+1]=i;

        rlen=25;
      }
      else if (received[0]==0x2F) { // PWR_on
        reply[hlen]=0xFC;
        reply[hlen+1]=0x2F;
        reply[hlen+2]=0x1;
        rlen=3;
      }
      else if (received[0]==0x9E) { // clear_V_monitor
        reply[hlen]=0xFC;
        reply[hlen+1]=0x9E;
        rlen=2;
      }
      else if (received[0]==0x3C) { // MC_temp
        reply[hlen]=0x3D;
        for (i=0;i<=7;++i) {
          reply[hlen+1+4*i]=0x78;
          reply[hlen+2+4*i]=0;
          reply[hlen+3+4*i]=0;
          reply[hlen+4+4*i]=0x5;
        }
        for (i=8;i<=14;++i) {
          reply[hlen+1+4*i]=0x40;
          reply[hlen+2+4*i]=0;
          reply[hlen+3+4*i]=0;
          reply[hlen+4+4*i]=0x6;
        }
        for (i=15;i<=20;++i) {
          reply[hlen+1+4*i]=0x48;
          reply[hlen+2+4*i]=0;
          reply[hlen+3+4*i]=0;
          reply[hlen+4+4*i]=0x6;
        }

        rlen=85;
      }
      else if (received[0]==0x44) { // write_TDC
        reply[hlen]=0x45;
        for (i=0;i<21;++i)
          reply[hlen+1+i]=0;
        rlen=9;
      }
      else if (received[0]==0x18) { // write_TDC_control
        reply[hlen]=0xFC;
        reply[hlen+1]=0x18;
        reply[hlen+2]=0x1;
        rlen=3;
      }
      else if (received[0]==0x64) { // rpc_I2C
        reply[hlen]=0x65;
        rlen=qlen-hlen;
        for (i=0;i<rlen;++i)
          reply[hlen+1+i]=received[1+i];
      }
      else if (received[0]==0x66) { // led_I2C
        unsigned char padcreply[]={0x00,0x6A,0x00,0xAA,0x00,0x13,0x00,0x6B,0x00,0xFE,0x00,0x61,0x00,0xFE,0x00,0x60,0x00,0xFE,0x00,0x63,0x00,0xFE,0x00,0x36,0x00,0xFE,0x00,0x61,0x00,0xFE,0x00,0x61,0x00,0xFE,0x00,0x63,0x00,0xFE,0x00,0x34,0x00,0xFE,0x00,0x39, 0x00,0xFE,0x00,0x9B};
        reply[hlen]=0x67;
        rlen=qlen-hlen;
        if (rlen==49 && received[1]==0x01 && received[2]==0x6A) // Presumably PADC reading
          for (i=0;i<48;++i)
            reply[hlen+1+i]=padcreply[i];
        else
          for (i=0;i<rlen;++i)
            reply[hlen+1+i]=received[1+i];
      }
      else if (received[0]==0x69) { // read_ADC
        reply[hlen]=0x6A;
        for (i=0;i<64;++i)
          reply[hlen+1+i]=0;
        rlen=65;
      }
      else if (received[0]==0x2b) { // SYNC
        reply[hlen]=0xFC;
        reply[hlen+1]=0x2b;
        rlen=2;
      }
      else if (received[0]==0x2c) { // SNAP_reset
        reply[hlen]=0xFC;
        reply[hlen+1]=0x2c;
        rlen=2;
      }
      else if (received[0]==0x2d) { // SOFT_reset
        reply[hlen]=0xFC;
        reply[hlen+1]=0x2d;
        rlen=2;
      }
      else if (received[0]==0x9f) { // select_L1A_veto
        reply[hlen]=0xFC;
        reply[hlen+1]=0x9f;
        rlen=2;
      }
      else if (received[0]==0x1d) { // read_TSS
        reply[hlen]=0x1e;
        for (i=0;i<59;++i)
          reply[hlen+1+i]=i;
        rlen=60;
      }
      else if (received[0]==0x16) { // write_TSS
        reply[hlen]=0xfc;
        reply[hlen+1]=0x16;
        reply[hlen+2]=0x1;
        rlen=3;
      }
      else if (received[0]==0x1f) { // read_TSM
        reply[hlen]=0x20;
        for (i=0;i<6;++i)
          reply[hlen+1+i]=i;
        rlen=7;
      }
      else if (received[0]==0x17) { // write_TSM
        reply[hlen]=0xfc;
        reply[hlen+1]=0x17;
        reply[hlen+2]=0x1;
        rlen=3;
      }
      else if (received[0]==0x49) { // TrgOut_select
        reply[hlen]=0xfc;
        reply[hlen+1]=0x49;
        rlen=2;
      }
      else if (received[0]==0x5b) { // read_ROB_pwr
        reply[hlen]=0x5c;
        for (i=0;i<7;++i) {
          reply[hlen+1+4*i]=0x50;
          reply[hlen+2+4*i]=reply[hlen+3+4*i]=0;
          reply[hlen+4+4*i]=0x2;
        }
        for (i=0;i<7;++i) {
          reply[hlen+29+4*i]=0x69;
          reply[hlen+30+4*i]=0x99;
          reply[hlen+31+4*i]=0;
          reply[hlen+32+4*i]=0x2;
        }
        for (i=0;i<7;++i) {
          reply[hlen+57+4*i]=0x40;
          reply[hlen+58+4*i]=0;
          reply[hlen+59+4*i]=0;
          reply[hlen+60+4*i]=0x1;
        }

        rlen=85;
      }
      else if (received[0]==0x32) { // reset_ROB
        reply[hlen]=0xfc;
        reply[hlen+1]=0x32;
        rlen=2;
      }
      else if (received[0]==0x33) { // Read ROB error
        rlen=8;
        reply[hlen]=0x34; // ID code
        for (i=0;i<7;++i)
          reply[hlen+1+i]=0xFF; // ROB ok 
      }
      else if (received[0]==0x41) { // run_BIST_test
        rlen=9;
        reply[hlen]=0x42; // ID code
        for (i=0;i<8;++i)
          reply[hlen+1+i]=i;
      }
      else if (received[0]==0x6c) { // read_out_ROB
        rlen=6;
        reply[hlen]=0x6d;
        reply[hlen+1]=0;
        for (i=0;i<4;++i)
          reply[hlen+2+i]=i;
      }
      else if (received[0]==0x26) { // set_CPU_ck_delay
        rlen=2;
        reply[hlen]=0xfc;
        reply[hlen+1]=0x26;
      }
      else if (received[0]==0x83) { // disable_temp_test
        rlen=2;
        reply[hlen]=0xfc;
        reply[hlen+1]=0x83;
      }
      else if (received[0]==0x5f) { // TRB PI test
        rlen=38;
        reply[hlen]=0x60;
        for (i=0;i<37;++i)
          reply[hlen+1+i]=i;
      }
      else if (received[0]==0x93) { // disable_TRB_CK
        rlen=2;
        reply[hlen]=0xfc;
        reply[hlen+1]=0x93;
      }
      else if (received[0]==0xa0) { // TRB_PI_test_0xA0
        rlen=3;
        reply[hlen]=0xfc;
        reply[hlen+1]=0xa0;
        reply[hlen+2]=received[6];
      }
      else if (received[0]==0xa1) { // clear_TRB_SEU_counter
        rlen=2;
        reply[hlen]=0xfc;
        reply[hlen+1]=0xa1;
      }
      else if (received[0]==0xa2) { // test_IR
        rlen=2;
        reply[hlen]=0xfc;
        reply[hlen+1]=0xa2;
      }
      else if (received[0]==0x1b) { // read_TRACO
        rlen=61;
        reply[hlen]=0x1c;
        for (i=0;i<60;++i)
          reply[hlen+1+i]=i;
      }
      else if (received[0]==0x15) { // write_TRACO
        rlen=3;
        reply[hlen]=0xfc;
        reply[hlen+1]=0x15;
        reply[hlen+2]=1;
      }
      else if (received[0]==0x3e) { // write_TRACO_LUTS
        rlen=3;
        reply[hlen]=0xfc;
        reply[hlen+1]=0x3e;
        reply[hlen+2]=1;
      }
      else if (received[0]==0xa8) { // write_MC_LUTS
        rlen=3;
        reply[hlen]=0xfc;
        reply[hlen+1]=0xa8;
        reply[hlen+2]=0;
      }
      else if (received[0]==0x4d) { // preload_LUTS
        rlen=3;
        reply[hlen]=0xfc;
        reply[hlen+1]=0x4d;
        reply[hlen+2]=1;
      }
      else if (received[0]==0x4e) { // load_LUTS
        rlen=3;
        reply[hlen]=0xfc;
        reply[hlen+1]=0x4e;
        reply[hlen+2]=1;
      }
      else if (received[0]==0x86) { // read_LUTS
        rlen=1+2*received[6];
        reply[hlen]=0x87;
        for (i=0;i<rlen-1;++i)
          reply[hlen+2+2*i]=i;
      }
      else if (received[0]==0x88) { // read_LUTS_param
        rlen=17;
        reply[hlen]=0x89;
        reply[hlen+1]=0;
        reply[hlen+2]=0xFF;
        reply[hlen+3]=0x0;
        reply[hlen+4]=0x3;
        reply[hlen+5]=0x40;
        reply[hlen+6]=0x0f;
        reply[hlen+7]=0x00;
        reply[hlen+8]=0x0a;
        reply[hlen+9]=0xa5;
        reply[hlen+10]=0x74;
        reply[hlen+11]=0x00;
        reply[hlen+12]=0x07;
        reply[hlen+13]=0x5d;
        reply[hlen+14]=0xfe;
        reply[hlen+15]=0x00;
        reply[hlen+16]=0x08;
      }
      else if (received[0]==0xa9) { // read_MC_LUTS_param
        rlen=13;
        reply[hlen]=0xaa;
        reply[hlen+1]=0;
        reply[hlen+2]=0x2;
        reply[hlen+3]=0x40;
        reply[hlen+4]=0x0f;
        reply[hlen+5]=0x00;
        reply[hlen+6]=0x0a;
        reply[hlen+7]=0xa5;
        reply[hlen+8]=0x74;
        reply[hlen+9]=0x00;
        reply[hlen+10]=0x07;
        reply[hlen+11]=0;
        reply[hlen+12]=0x1;
      }
      else if (received[0]==0x96) { // restore_MC_conf
        rlen=3;
        reply[hlen]=0xfc;
        reply[hlen+1]=0x96;
        reply[hlen+2]=0x1;
      }
      else if (received[0]==0xa5) { // exit_MC
        rlen=2;
        reply[hlen]=0xfc;
        reply[hlen+1]=0xa5;
      }
      else if (received[0]==0xf8) { // restart_CCB
        rlen=2;
        reply[hlen]=0xfc;
        reply[hlen+1]=0xf8;
      }
      else if (received[0]==0xf5) { // read_CPU_addr
        rlen=4+(received[5]<<8)+received[6];
        reply[hlen]=0xf4;
        reply[hlen+1]=received[1];
        reply[hlen+2]=received[2];
        reply[hlen+3]=received[3];
        reply[hlen+4]=0x11;
//
        reply[hlen+rlen-1]=0xFF;
      }
      else if (received[0]==0x46) { // run_in_progress
        rlen=2;
        reply[hlen]=0xfc;
        reply[hlen+1]=0x46;
      }
      else if (received[0]==0xf0) { // read_comm_err
        rlen=4;
        reply[hlen]=0xf0;
        reply[hlen+1]=0x1;
        reply[hlen+2]=0x0;
        reply[hlen+3]=0x0;
      }
      else if (received[0]==0x74 || received[0]==0x76) { // auto_set_LINK, read_LINK
        rlen=9;
        reply[hlen]=0x75;
        reply[hlen+1]=0x01;
        reply[hlen+2]=0x90;
        reply[hlen+3]=0x00;
        reply[hlen+4]=0x0E;
        reply[hlen+5]=0x0;
        reply[hlen+6]=0x14;
        reply[hlen+7]=0x0;
        reply[hlen+8]=0x10;
      }
      else if (received[0]==0x77) { // disable_LINK_monitor
        rlen=2;
        reply[hlen]=0xfc;
        reply[hlen+1]=0x77;
      }
      else if (received[0]==0xee) { // script
        rlen=2;
        reply[hlen]=0xee;
        reply[hlen+1]=0x1;
      }
      else if (received[0]==0x70) { // auto_trigger
        rlen=2;
        reply[hlen]=0xfc;
        reply[hlen+1]=0x70;
      }
      else if (received[0]==0x91) { // CCBrdy_pulse
        rlen=2;
        reply[hlen]=0xfc;
        reply[hlen+1]=0x91;
      }
      else if (received[0]==0x92) { // pirw
        rlen=2;
        reply[hlen]=0xfc;
        reply[hlen+1]=0x92;
      }
      else if (received[0]==0x94) { // disable_SB_CK
        rlen=2;
        reply[hlen]=0xfc;
        reply[hlen+1]=0x94;
      }
      else if (received[0]==0x95) { // disable_OSC_CK
        rlen=2;
        reply[hlen]=0xfc;
        reply[hlen+1]=0x95;
      }
      else if (received[0]==0x97) { // write_custom_data
        rlen=2;
        reply[hlen]=0xfc;
        reply[hlen+1]=0x97;
      }
      else if (received[0]==0x98) { // read_custom_data
        rlen=10;
        reply[hlen]=0x99;
        for (i=0;i<9;++i)
          reply[hlen+1+i]=1+i;
      }
      else if (received[0]==0x35) { // set_FE_thr
        rlen=17;
        unsigned char rply35[]="\x60\xF\x0\x1\x51\x8B\xFF\xFB\x60\xF\x0\1\x51\x8B\xFF\xFB";
        reply[hlen]=0x25;
        for (i=0;i<16;++i)
          reply[hlen+1+i]=rply35[i];
      }
      else if (received[0]==0x47) { // set_FE_thr
        rlen=9;
        unsigned char rply47[]="\x46\x77\0\2\x46\x77\0\2";
        reply[hlen]=0x24;
        for (i=0;i<8;++i)
          reply[hlen+1+i]=rply47[i];
        }
      else if (received[0]==0x36) { // read_FE_temp
        rlen=197;
        reply[hlen]=0x37;
        for (i=0;i<98;++i) {
          reply[hlen+1+2*i]=0x01;
          reply[hlen+2+2*i]=0x40;
        }
      }
      else if (received[0]==0x71 || received[0]==0x62) { // read_FE_Tmask, set_FE_Tmask
        rlen=73;
        reply[hlen]=0x63;
        for (i=0;i<72;++i) {
          reply[hlen+1+i]=0;
        }
      }
      else if (received[0]==0x9b) { // set_SL_Tmask
        rlen=3;
        reply[hlen]=0xfc;
        reply[hlen+1]=0x9b;
        reply[hlen+2]=0;
      }
      else if (received[0]==0x4b) { // FE_test
        rlen=37;
        reply[hlen]=0x4c;
        for (i=i;i<36;++i)
          reply[hlen+1+i]=0;
      }
      else if (received[0]==0x4a) { // set_TP
        rlen=2;
        reply[hlen]=0xfc;
        reply[hlen+1]=0x4a;
      }
      else if (received[0]==0x78) { // set_rel_TP
        rlen=2;
        reply[hlen]=0xfc;
        reply[hlen+1]=0x78;
      }
      else if (received[0]==0x72) { // read_TP
        unsigned char rply72[]="\1\2\3\4\5\x51\xeb\xff\xfa\x51\xeb\xff\xfa\1\2\3";
        rlen=17;
        reply[hlen]=0x73;
        for (i=0;i<16;++i)
          reply[hlen+1+i]=rply72[i];
      }
      else if (received[0]==0x82) { // test_TP_finedelay
        rlen=3;
        reply[hlen]=0xfc;
        reply[hlen+1]=0x82;
        reply[hlen+2]=0x7;
      }
      else if (received[0]==0x8b) { // set_TP_offset
        rlen=2;
        reply[hlen]=0xfc;
        reply[hlen+1]=0x8b;
      }
      else if (received[0]==0x68) { // test_DAC
        rlen=4;
        reply[hlen]=0xfc;
        reply[hlen+1]=0x68;
        reply[hlen+2]=0x3;
        reply[hlen+3]=0x7f;
      }
      else if (received[0]==0x6b) { // set_calib_DAC
        rlen=2;
        reply[hlen]=0xfc;
        reply[hlen+1]=0x6b;
      }
      else if (received[0]==0x6e) { // read_calib_DAC
        unsigned char rply6e[]="\x66\x66\xff\xfd\x66\x66\xff\xfd\x66\x66\xff\xfd\x66\x66\xff\xfd\x66\x66\xff\xfd\x66\x66\xff\xfd\x66\x66\xff\xfd\x66\x66\xff\xfd\x66\x66\xff\xfd\x66\x66\xff\xfd\x66\x66\xff\xfd\x66\x66\xff\xfd\x66\x66\xff\xfd\x66\x66\xff\xfd";
        rlen=57;
        reply[hlen]=0x6f;
        for (i=0;i<56;++i)
          reply[hlen+1+i]=rply6e[i];
      }
      else if (received[0]==0x48) { // TTC_skew
        rlen=3;
        reply[hlen]=0xfc;
        reply[hlen+1]=0x48;
        reply[hlen+2]=0;
      }
      else if (received[0]==0x50) { // emul_TTC
        rlen=2;
        reply[hlen]=0xfc;
        reply[hlen+1]=0x50;
      }
      else if (received[0]==0x2a) { // sel_TTC_ck
        rlen=2;
        reply[hlen]=0xfc;
        reply[hlen+1]=0x2a;
      }
      else if (received[0]==0x8a) { // reset_lose_lock_counter
        rlen=2;
        reply[hlen]=0xfc;
        reply[hlen+1]=0x8a;
      }
      else if (received[0]==0x8e) { // set_TTC_fine_delay
        rlen=3;
        reply[hlen]=0xfc;
        reply[hlen+1]=0x8e;
        reply[hlen+2]=0;
      }
      else if (received[0]==0xAD) { // clear_TRIGGER_histogram
        rlen=2;
        reply[hlen]=0xfc;
        reply[hlen+1]=0xAD;
      }
      else if (received[0]==0xAF) { // PIPROG
        rlen=2;
        reply[hlen]=0xfc;
        reply[hlen+1]=0xAF;
      }
      else if (received[0]==0xB0) { // read_TRIGGER_histogram
//        rlen=3;
//        reply[hlen]=0xfc;
//        reply[hlen+1]=0xAE;
//        reply[hlen+2]=0xFF;
//
        rlen=201;
        reply[hlen]=0xb1;
        for (i=0;i<100;++i) {
          reply[hlen+1+2*i]=0;
          reply[hlen+2+2*i]=1;
        }
      }
      else if (received[0]==0xB2) { // TRIGGER_frequency
        rlen=3;
        reply[hlen]=0xB3;
        reply[hlen+1]=0;
        reply[hlen+2]=115;
      }
      else if (received[0]==0xB4) { // chamber_map
        rlen=3;
        reply[hlen]=0xFC;
        reply[hlen+1]=0xB4;
        reply[hlen+2]=0;
      }
      else if (received[0]==0xED) { // protect_FLASH
        rlen=2;
        reply[hlen]=0xFC;
        reply[hlen+1]=0xED;
      }
      else if (received[0]==0xEB) { // erase_FLASH
        rlen=2;
        reply[hlen]=0xFC;
        reply[hlen+1]=0xEB;
      }
      else if (received[0]==0xF3) { // write_FLASH
        rlen=3;
        reply[hlen]=0xF2;
        reply[hlen+1]=0x00;
        reply[hlen+2]=0x00;
      }
      else {
//        rlen=1;
//        reply[hlen]=0x3F; // "ccb busy" reply
//        reply[hlen]=0x13; // "mc status" reply
//        reply[hlen]=0x99; // "Wrong" reply
        rlen=2;
        reply[hlen]=0xFC; // ccb unknown command
        reply[hlen+1]=0x00; // ccb unknown command
      }

// reply lenght
      reply[4]=reply[5]=0;
      reply[6]=((hlen+rlen)>>8)&0xFF;
      reply[7]=(hlen+rlen)&0xFF;

      client->write(reply,hlen+rlen);
      client->flush();

      printf("Sent back %d chars: 0x",hlen+rlen);
      for (i=0;i<rlen+hlen;++i)
        printf("%02x",reply[i]);
      std::cout << std::endl << std::endl;

    }
  }
  catch(estream* e) {
    perr.putf("Error: %s\n", pconst(e->get_message()));
    delete e;
  }
  client->close();
}


void servermain(ipstmserver& svr) {
  int i;
  ipstream client[maxclients], sclient;
  pthread_t thrid[maxclients];

  printf("Dummy server (ver. Dec07 2007) is ready on port %d\n\n", srvport);

  int client_status;
  while (1) {
    for (i=0; (client_status=client[i].get_status())!=IO_CREATED && client_status!=IO_CLOSED && i<maxclients; ++i) {} // Look for the first available stream
    if (i<maxclients) {
// serve() will wait for a connection request and will prepare
// the supplied ipstream object for talking to the peer.
// note that (unlikely) exceptions thrown in serve() will be 
// caught in main()
      svr.serve(client[i]);

      if (client[i].get_active()) {
        pthread_create(thrid+i,NULL,clnthread,(void*)(client+i));
        pthread_detach(thrid[i]);
      }

    } else {
      printf("Server: max number of connections (%d) reached\n\n", i);
      sleep(1);
    }
  }

// i=0;
//  while(i < maxclients) {
//// serve() will wait for a connection request and will prepare
//// the supplied ipstream object for talking to the peer.
//// note that (unlikely) exceptions thrown in serve() will be 
//// caught in main()
//    svr.serve(client[i]);
//
//    if (client[i].get_active())
//      pthread_create(thrid+i,NULL,clnthread,(void*)(client+i));
//
//    ++i;
//  }
//
//  printf("Server: max number of connections (%d) reached\n\n", i);
//
//// From now on the connections are serialised
//  while(true) {
//    svr.serve(sclient);
//
//    if (sclient.get_active())
//      clnthread((void*)(&sclient));
}

int main() {
  ipstmserver svr;

  try {
// bind to all local addresses on port <srvport>
    svr.bindall(srvport);

// enter a loooong loop of serving requests
    servermain(svr);
  }
  catch(estream* e) {
    perr.putf("FATAL: %s\n", pconst(e->get_message()));
    delete e;
  }

  return 0;
}
