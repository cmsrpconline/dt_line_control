#include <ccbcmds/apfunctions.h>
#include <ccbcmds/Clog.h>
#include <ccbcmds/Ccommand.h>
#include <ccbcmds/Ccmn_cmd.h>
#include <ccbcmds/Cdef.h>

#include <iostream>
#include <stdio.h>


// Author: A.Parenti, Jun29 2006
// Modified: AP, Jan26 2007
int main(int argc, char *argv[]) {
  short pwrid, pon;
  short ccbport;
  Clog *myLog;
  Ccommand *myCmd;
  Ccmn_cmd *myCmn;

  if (argc < 4) {
    printf("*** Usage: mc_pwr [ccbport] [pwr_id] [off/on]\n");
    printf("*** pwr_id: 1=flash 2=analog 3=ccb_ck 4=rpc 5=led 6=tsms 7=tsmdu\n");
    printf("            8=tsmdd 9=sb_theta 10=sb_ck 11=buff_trb 12=vcc_trb\n");
    printf("            13-20=trb0-trb7 21-27=rob0-rob6\n");
    printf("*** off/on: 0=off 1=on\n");

    printf("ccbport? ");
    scanf("%d",&ccbport);
    printf("pwr_id? ");
    scanf("%d",&pwrid);
    printf("off/on (0/1)? ");
    scanf("%d",&pon);
  }  else {
    sscanf(argv[1],"%d",&ccbport);
    sscanf(argv[3],"%d",&pon);
    sscanf(argv[2],"%d",&pwrid);
  }
  printf("got port %d pwrid %d on %d\n",ccbport,pwrid,pon);

#ifdef LOGTODB
  myLog = new Clog(); // Log to DB
#else
  myLog = new Clog("log.txt"); // Log to file
#endif
  myCmd = new Ccommand(ccbport,CCBSERVER,CCBPORT,myLog);
  myCmn = new Ccmn_cmd(myCmd); // New CCB object

  myCmd->set_write_log(true); // Switch on logger

  printf("Using server %s:%d\n",myCmd->read_server_name(),myCmd->read_server_port());

  printf("\n\n\n");
  printf("*** Power on/off boards***\n");
  printf("*** Actual time: %s\n",get_time());
  myCmn->PWR_on(pwrid,pon); // 0x2F
  myCmn->PWR_on_print();

}
