#include <iostream>
#include <stdio.h>

#ifndef __USE_CCBDB__ // No DB access
int main() {
  std::cout << "Nothing done. Please compile with DB support.\n";
}
#else

#include <ccbdb/Cdtpartition.h>
#include <ccbcmds/apfunctions.h>

// Author: A. Parenti, Mar23 2006
// Modified: AP, May25 2007
// Tested on MySQL/Oracle
int main(int argc, char *argv[]) {
  dtpartition mydtpart;

  bool first=true;
  char filename[LEN1]="", partname[LEN1]="", content[1024]="", c,
    *pcont, *pcontmax, strccbid[64];
  int ii, ccbid;

  printf("*** This program insert an entry in the dtrelations table.\n");
  printf("*** Usage: %s [filename]\n",argv[0]);

  printf("Partition Name? ");
  while ((c=getchar()) != '\n')
    sprintf(partname,"%s%c",partname,c);

  if (argc != 2) {
    printf("Structure of the file must be: \"(int)ccbid[0] (int)ccbid[1] ...\"\n");

    printf("Filename? ");
    while ((c=getchar()) != '\n')
      sprintf(filename,"%s%c",filename,c);
  } else
    strcpy(filename,argv[1]);

  strcpy(content,read_file(filename));
  if (strlen(content)==0) {
    printf("File not existing or empty.\n");
    return -1;
  }

  pcontmax=(pcont=content)+strlen(content);
  while(pcont<pcontmax) {

    if (first) {
      first=false;
      mydtpart.select(partname);
      mydtpart.newdtpart(partname,0);
    }

    sscanf(pcont,"%s",strccbid); // read ccbid
    sscanf(strccbid,"%d",&ccbid);
    pcont+=(strlen(strccbid)+1);

    printf("Inserting ccb=%d into %s. Result: %s\n",ccbid,partname,(mydtpart.insert(ccbid)==0)?"ok":"error");
  }
  return 0;
}

#endif
