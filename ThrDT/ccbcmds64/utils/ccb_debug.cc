#include <ccbcmds/apfunctions.h>
#include <ccbcmds/Clog.h>
#include <ccbcmds/Ccommand.h>
#include <ccbcmds/Cccb.h>
#include <ccbcmds/Cdef.h>


#include <iostream>
#include <stdio.h>
#include <stdlib.h>

// Author: A.Parenti, Jul01 2006
// Modified: AP, Aug30 2007
int main(int argc, char *argv[]) {
  bool mc_program;
  char message[1024];
  long pippo;
  short ccbport, ccbid;
  Emcstatus mc_phys_st, mc_logic_st;

  Clog *myLog;
  Ccommand *myCmd;
  Cccb *myCcb;
  Cref *myRef;
  char mccbsrv[64];
  int errcod;
  char *errstr;
  unsigned long mycmd,mycmd3,mycmd4,mycmd5,mycmd6,mycmd7;
 unsigned short optoid;

  if (argc>=3) {
    sscanf(argv[1],"%lx",&pippo);
    mycmd=pippo;
	 printf("cmd %x\n",pippo);
    sscanf(argv[2],"%x",&pippo);
    ccbport=pippo;

    if (argc>=4) sscanf(argv[3],"%x",&mycmd3);
    if (argc>=5) sscanf(argv[4],"%x",&mycmd4);
    if (argc>=6) sscanf(argv[5],"%x",&mycmd5);
    if (argc>=7) sscanf(argv[6],"%x",&mycmd6);
    //if (argc>=8) sscanf(argv[7],"%x",&mycmd7);
    if (argc>=8)  sscanf(argv[7],"%s",mccbsrv); 
	 else strcpy(mccbsrv,"127.0.0.1");
  } else {
    printf("Usage: ccb_debug cmd port [host]\n");

   exit(-1);
  }

  printf("Using ccbserver %s\n",mccbsrv);
  
#ifdef LOGTODB
  myLog = new Clog(); // Log to DB
#else
  myLog = new Clog("log.txt"); // Log to file
#endif
  //myCmd = new Ccommand(1,ccbport,mccbsrv,CCBPORT,myLog);
  //optoid = 0xff00+ccbport;
  myCmd = new Ccommand(ccbport,mccbsrv,18889,myLog);
 // myCmd = new Ccommand(optoid,mccbsrv,18885,myLog);


  printf("Using server %s:%d\n",myCmd->read_server_name(),myCmd->read_server_port());
  printf("cmd3  %x\n",(mycmd3&0xff000000)>>24);
  printf("cmd2  %x\n",(mycmd3&0xff0000)>>16);
  printf("cmd1  %x\n",(mycmd3&0xff00)>>8);
  printf("cmd0  %x\n",mycmd3&0xff);
  printf("cmd3  %x\n",(mycmd4&0xff000000)>>24);
  printf("cmd2  %x\n",(mycmd4&0xff0000)>>16);
  printf("cmd1  %x\n",(mycmd4&0xff00)>>8);
  printf("cmd0  %x\n",mycmd4&0xff);
  printf("port  %x\n",ccbport);

  printf("*** Actual time: %s\n",get_time());

  unsigned char cmdline[256];
  cmdline[0]=mycmd&0xff;
  cmdline[4]=mycmd3&0xff;
  cmdline[3]=(mycmd3&0xff00)>>8;
  cmdline[2]=(mycmd3&0xff0000)>>16;
  cmdline[1]=(mycmd3&0xff000000)>>24;
  cmdline[8]=mycmd4&0xff;
  cmdline[7]=(mycmd4&0xff00)>>8;
  cmdline[6]=(mycmd4&0xff0000)>>16;
  cmdline[5]=(mycmd4&0xff000000)>>24;
  cmdline[12]=mycmd5&0xff;
  cmdline[11]=(mycmd5&0xff00)>>8;
  cmdline[10]=(mycmd5&0xff0000)>>16;
  cmdline[9]=(mycmd5&0xff000000)>>24;
  cmdline[16]=mycmd6&0xff;
  cmdline[15]=(mycmd6&0xff00)>>8;
  cmdline[14]=(mycmd6&0xff0000)>>16;
  cmdline[13]=(mycmd6&0xff000000)>>24;
  //myCmd->use_secondary_port();
  errcod=myCmd->send_command(cmdline,17,2000);
  errstr=myCmd->read_error_message(errcod);

   
   printf("CCB reply:");
  for (int i=0; i<(myCmd->data_read_len); ++i)
    printf(" %02X",myCmd->data_read[i]);
  putchar('\n');
  printf("Error code: %s\n",errstr);



}

