#include <ccbcmds/apfunctions.h>
#include <ccbcmds/Clog.h>
#include <ccbcmds/Ccommand.h>
#include <ccbcmds/Cccb.h>
#include <ccbcmds/Cfe.h>
#include <ccbcmds/Cdef.h>

#include <iostream>
#include <stdio.h>

// Author: A.Parenti, Jul14 2006
// Modified: AP, Jan26 2007
int main(int argc, char *argv[]) {
  int ccbport, SL, channel, status;
  Clog *myLog;
  Ccommand *myCmd;
  Cfe *myFe;


  if (argc < 5) {
    printf("*** Usage: mc_mask_fe_channel [ccbport] [SL] [channel] [0/1]\n");

    printf("ccbport? ");
    scanf("%d",&ccbport);
    printf("SL [0-2]? ");
    scanf("%d",&SL);
    printf("channel? ");
    scanf("%d",&channel);
    printf("mask (1) / unmask (0)? ");
    scanf("%d",&status);
  }  else {
    sscanf(argv[1],"%d",&ccbport);
    sscanf(argv[2],"%d",&SL);
    sscanf(argv[3],"%d",&channel);
    sscanf(argv[4],"%d",&status);
  }

#ifdef LOGTODB
  myLog = new Clog(); // Log to DB
#else
  myLog = new Clog("log.txt"); // Log to file
#endif
  myCmd = new Ccommand(ccbport,CCBSERVER,CCBPORT,myLog);
  myFe = new Cfe(myCmd); // New FE object

 myCmd->set_write_log(true); // Switch on logger

  printf("Using server %s:%d\n",myCmd->read_server_name(),myCmd->read_server_port());

  printf("\n\n\n");
  printf("*** Mask FE channel ccbport: %d\n",ccbport);
  printf("*** Actual time: %s\n",get_time());
  myFe->set_FE_mask_ch(SL,channel,status); // 0x3B
  myFe->set_FE_mask_print();
}
