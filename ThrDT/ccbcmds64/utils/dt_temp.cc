#include <iostream>
#include <fstream>
#include <ctime>

#ifndef __USE_CCBDB__ // No DB access
int main() {
  std::cout << "Nothing done. Please compile with DB support.\n";
}
#else

#include <ccbcmds/apfunctions.h>
#include <ccbcmds/Cdef.h>
#include <ccbcmds/Clog.h>
#include <ccbcmds/Ccommand.h>
#include <ccbcmds/Ccmn_cmd.h>
#include <ccbcmds/Cdef.h>

#define LOWT  15 // Lower temperature limit
#define HIGHT 38 // Higher temperature limit
#define MONITINT  60 // temperature monitoring interval, sec
#define WRITEINT 300 // temperature logging interval, sec
#define NMAX 250

#define FILEROOT "/home/dtdqm/temperature/dttemp" // filename for logging

// Author: A.Parenti, Aug10 2006
int main(int argc, char *argv[]) {

  int i;
  char TLogFile[256], CcbFName[256]; // Filenames: log4temperatures, CCB list
  std::string CcbFCont; // CcbFName content
  std::fstream TFile; // Temperatures file

// Datetime
  time_t currenttime=WRITEINT, writetime=0;
  struct tm *timeinfo;
  char datetime[256], temp[256];

// CCB list and status
  int nccb, ccbid[NMAX], port[NMAX];
  float maxtemp[NMAX];
  Clog myLog("/dev/null"); // No ccbcmds log
  Ccommand *myCmd[NMAX];
  Ccmn_cmd *myCmn[NMAX];

  if (argc <2) {
    printf("*** Usage: dt_temp <ccblist_file>\n");
    return -1;
  }  else {
    sscanf(argv[1],"%s",&CcbFName);
  }

// Read CCB list
  std::string::size_type pos1=0, pos2=0;
  std::string pippo;

  CcbFCont=read_file((std::string)CcbFName);

  for (nccb=0; (pos2=CcbFCont.find ("\n",pos1))!=std::string::npos &&
	 nccb<NMAX; nccb++) {
    pippo = CcbFCont.substr(pos1,pos2-pos1);

    sscanf(pippo.c_str(),"%d %d",ccbid+nccb,port+nccb);

    myCmd[nccb] = new Ccommand(port[nccb],CCBSERVER,CCBPORT,&myLog);
    myCmn[nccb] = new Ccmn_cmd(myCmd[nccb]); // New CCB object

    pos1=pos2+1;
  }

// Get datetime & create TLogFile
  time(&currenttime);
  timeinfo = localtime(&currenttime);
  sprintf(TLogFile,"%s%d%02d%02d.txt",FILEROOT,timeinfo->tm_year+1900,
	  timeinfo->tm_mon+1,timeinfo->tm_mday);


// Open file for logging
  TFile.open(TLogFile,std::ios::out | std::ios::app); 

  if (!TFile.is_open()) {
    printf("Cannot open file %s for logging.\n",TLogFile);
    return -2;
  }


  printf("Starting temperature monitor. Please wait.\n");
  while(1) {
    for (i=0;i<nccb;++i) {
      myCmn[i]->MC_read_status(); // 0xEA
      maxtemp[i] = myCmn[i]->mc_status.BrdMaxtemp;
    }

    printf("%s",CLRSCR); // Clear screen
    printf("%s",STDBKG); // Standard BKG
    printf("%s\n",get_time());
    printf("CCBID   MAXTEMP\n");
    printf("***************\n");
    for (i=0;i<nccb;++i) {
      if (maxtemp[i]>HIGHT) printf("%s",REDBKG); // Red BKG
      else if (maxtemp[i]<=LOWT) printf("%s",BLUBKG); // Blue BKG
      else printf("%s",GRNBKG); // Green BKG
      printf("%5d    %6.2f\n",ccbid[i],maxtemp[i]);
    }

    printf("%s\n\n",STDBKG); // Standard BKG

// Get datetime
    time(&currenttime);
    timeinfo = localtime(&currenttime);
    sprintf(datetime,"%d/%02d/%02d\t%02d:%02d",timeinfo->tm_year+1900,
            timeinfo->tm_mon+1,timeinfo->tm_mday,timeinfo->tm_hour,
            timeinfo->tm_min);

    if (timeinfo->tm_mday != localtime(&writetime)->tm_mday){// Day has changed
      TFile.close(); // Close logging file
      sprintf(TLogFile,"%s%d%02d%02d.txt",FILEROOT,timeinfo->tm_year+1900,
              timeinfo->tm_mon+1,timeinfo->tm_mday);

// Re-open a new file for logging
      TFile.open(TLogFile,std::ios::out | std::ios::app); 
    }

    if (currenttime-writetime>=WRITEINT) { // Write temp every 300 sec
      writetime=currenttime;
      TFile << datetime;

      strcpy(temp,"");
      for (i=0;i<nccb;++i)
	sprintf(temp,"%s\t%6.2f",temp,maxtemp[i]);

      TFile << temp << '\n';
      TFile.flush();
    }

    apsleep(500*MONITINT);
  }

  TFile.close();
  return 0;
}

#endif
