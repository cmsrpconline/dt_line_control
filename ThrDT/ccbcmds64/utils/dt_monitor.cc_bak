#include <ccbcmds/apfunctions.h>
#include <ccbcmds/Cdef.h>
#include <ccbcmds/Cdtdcs.h>

#include <cstdio>
#include <iostream>
#include <ctime>

#define LOWT  15 // Lower temperature limit (C)
#define HIGHT 38 // Higher temperature limit
#define LOWP  10 // Lower pressure limit (mbar)
#define HIGHP 50 // Higher pressure limit (mbar)

// Author: A.Parenti
// Modified: AP, Jul03 2007
int main(int argc, char *argv[]) {

  int i, j;
  char partname[PARTNAMESIZE]; // Partition name

// Datetime
  time_t atime, wtime=0;

// CCB list and status
  int nccb;
  int *ccbid = new int[NMAXCCB];
  int wheel[NMAXCCB], sector[NMAXCCB], station[NMAXCCB];
  float *MaxTempRob = new float[NMAXCCB];
  float *MaxTempTrb = new float[NMAXCCB];
  float *MaxTempExt = new float[NMAXCCB];
  float *MaxTempFe  = new float[NMAXCCB];
  float *TempExt = new float[7];
  float *pressure_hv = new float[NMAXCCB];
  float *pressure_fe = new float[NMAXCCB];
  Emcstatus *dtphysst = new Emcstatus[NMAXCCB];
  Emcstatus *dtlogicst = new Emcstatus[NMAXCCB];

// DTDCS class
  dtdcs mydtdcs;

  if (argc <2) {
    printf("*** Usage: dt_temp <partition name>\n");

// List available partitions
    mydtdcs.partShowAvailable();

    return -1;
  }  else {
    sscanf(argv[1],"%s",&partname);
  }

  printf("Starting temperature/pressure monitor. Please wait.\n");
  mydtdcs.partInitialize(partname);
  mydtdcs.disableAutoRecover(); // Disable ccb auto-recover

// CCB list
  nccb = mydtdcs.partGetNCcb(); // number of ccbs
  ccbid = mydtdcs.partGetCcbList(); // ccb list
  for (i=0;i<nccb;++i)
    mydtdcs.ccbGetWhSecSt(ccbid[i],wheel+i,sector+i,station+i);

  while(1) {
// Get datetime
    time(&atime);

// Minicrate status
    dtphysst = mydtdcs.partGetPhysStatus(); // Status
    dtlogicst = mydtdcs.partGetLogicStatus(); // Status
    MaxTempRob = mydtdcs.partGetRobMaxTemp(); // ROB max temps
    MaxTempTrb = mydtdcs.partGetTrbMaxTemp(); // TRB max temps
    MaxTempFe = mydtdcs.partGetFeMaxTemp(); // FE max temps
    MaxTempExt = mydtdcs.partGetExtMaxTemp(); // EXT max temps
    pressure_hv = mydtdcs.partGetHvPressure(); // PADC HV side
    pressure_fe = mydtdcs.partGetFePressure(); // PADC FE side

// Print to stdout
    printf("%s",CLRSCR); // Clear screen
    printf("%s",STDBKG); // Standard BKG
    printf("Current time:            %s\n",ctime(&atime));
    printf("*****************************************************************************************************\n");
    printf(" Wh/Se/St CCBID   MAXTEMP   MAXTEMP   MAXTEMP TEMP_EXT1 TEMP_EXT2 TEMP_EXT3   SENS100_HV   SENS100_FE\n");
    printf("                 (ROB,°C)  (TRB,°C)   (FE,°C)      (°C)      (°C)      (°C)       (mbar)       (mbar)\n");
    printf("*****************************************************************************************************\n");
    for (i=0;i<nccb;++i) {
      printf(" %+2d/%2d/%2d",wheel[i],sector[i],station[i]);
      printf(" %5d ",ccbid[i]);

// Print max Temperature, ROB
      if (MaxTempRob[i]>HIGHT) printf("%s",REDBKG); // Red BKG
      else if (MaxTempRob[i]<=LOWT) printf("%s",BLUBKG); // Blue BKG
      else printf("%s",GRNBKG); // Green BKG
      printf("   %6.2f ",MaxTempRob[i]);
      printf("%s",STDBKG); // Standard BKG

// Print max Temperature, TRB
      if (MaxTempTrb[i]>HIGHT) printf("%s",REDBKG); // Red BKG
      else if (MaxTempTrb[i]<=LOWT) printf("%s",BLUBKG); // Blue BKG
      else printf("%s",GRNBKG); // Green BKG
      printf("   %6.2f ",MaxTempTrb[i]);
      printf("%s",STDBKG); // Standard BKG

// Print max Temperature, FE
      if (MaxTempFe[i]>HIGHT) printf("%s",REDBKG); // Red BKG
      else if (MaxTempFe[i]<=LOWT) printf("%s",BLUBKG); // Blue BKG
      else printf("%s",GRNBKG); // Green BKG
      printf("   %6.2f ",MaxTempFe[i]);
      printf("%s",STDBKG); // Standard BKG


// Print EXT Temperature (three sensors)
      TempExt=mydtdcs.ccbGetExtTemp(ccbid[i]);
      for (j=0;j<3; ++j) {
        if (TempExt[j]>HIGHT) printf("%s",REDBKG); // Red BKG
        else if (TempExt[j]<=LOWT) printf("%s",BLUBKG); // Blue BKG
        else printf("%s",GRNBKG); // Green BKG
        printf("   %6.2f ",TempExt[j]);
        printf("%s",STDBKG); // Standard BKG
      }

// Print Pressure
      if (1000*pressure_hv[i]>HIGHP) printf("%s",REDBKG); // Red BKG
      else if (1000*pressure_hv[i]<=LOWP) printf("%s",BLUBKG); // Blue BKG
      else printf("%s",GRNBKG); // Green BKG
      printf("      %+3.3f",1000*pressure_hv[i]);
      printf("%s",STDBKG); // Standard BKG

      if (1000*pressure_fe[i]>HIGHP) printf("%s",REDBKG); // Red BKG
      else if (1000*pressure_fe[i]<=LOWP) printf("%s",BLUBKG); // Blue BKG
      else printf("%s",GRNBKG); // Green BKG
      printf("      %+3.3f",1000*pressure_fe[i]);
      printf("%s",STDBKG); // Standard BKG

      printf("\n");
    }

    apsleep(1000);
  }

  return 0;
}
