#include <ccbcmds/apfunctions.h>
#include <ccbcmds/Clog.h>
#include <ccbcmds/Ccommand.h>
#include <ccbcmds/Cccb.h>
#include <ccbcmds/Cdef.h>
#include "../ccbdb/Cccbdata.h"
#include <ccbcmds/Cdtdcs.h>

#ifdef __USE_CCBDB__ // DB access enabled
  #include <ccbdb/Ccmsdtdb.h>
#endif

#include <iostream>
#include <stdio.h>


typedef struct {
  short int ccbid;
  short int port;
  short int wheel;
  short int sector;
  short int station;
} ccb;


char CCBSERVERm2[15]="10.176.60.236";  
char CCBSERVER0[15]="10.176.60.232";  
char CCBSERVERm1[15]="10.176.60.234";  
char CCBSERVER2[15]="10.176.60.228";  
char CCBSERVER1[15]="10.176.60.230";



// void getCcbList( cmsdtdb & mydtdtb, ccb ccbList[264] )
// {

//   char * query = "SELECT CCBID, PORT, WHEEL, SECTOR, STATION FROM CCBMAP ORDER BY WHEEL, SECTOR, STATION";

//   char tmp[20]
//   int reso = mydtdb->sqlquery(query_, &rows, &fields);
//   if ( reso != 0 )
//     return -1;
  
//   while ( mydtdb->fetchrow() == SQL_SUCCESS ) {
//     mydtdb->returnfield(0, tmp);
//     ccbList[counter].ccbid = atoi(tmp);
//     mydtdb->returnfield(1, tmp);
//     ccbList[counter].port = atoi(tmp);
//     mydtdb->returnfield(2, tmp);
//     ccbList[counter].wheel = atoi(tmp);
//     mydtdb->returnfield(3, tmp);
//     ccbList[counter].sector = atoi(tmp);
//     mydtdb->returnfield(4, tmp);
//     ccbList[counter].station = atoi(tmp);
//     ++counter;
//   }
// }



// Author: A.Parenti, Jul01 2006
// Modified: AP, Aug30 2007
#ifdef __USE_CCBDB__ // DB access enabled
int main(int argc, char *argv[]) {

  char partname[PARTNAMESIZE]; // Partition name
// DTDCS class
  dtdcs mydtdcs;

  if (argc <2) {
    printf("*** Usage: mc_checkTDCsStatus <partition name>\n");

// List available partitions
    mydtdcs.partShowAvailable();

    return -1;
  }  else {
    sscanf(argv[1],"%s",&partname);
  }

  mydtdcs.partInitialize((char *)partname, false);
  mydtdcs.partSendCommand(cmd_checktdcs_status);

}

#else
int main() {
  std::cout << "Nothing done. Please compile with DB support.\n";
}
#endif
