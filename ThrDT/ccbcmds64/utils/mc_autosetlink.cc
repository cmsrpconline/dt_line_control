#include <ccbcmds/apfunctions.h>
#include <ccbcmds/Clog.h>
#include <ccbcmds/Ccommand.h>
#include <ccbcmds/Cccb.h>
#include <ccbcmds/Cdef.h>

#include <iostream>
#include <stdio.h>
#include <stdlib.h>

// Author: S.Ventura, Jul13 2006
// Modified: A.Parenti, Jan26 2007
int main(int argc, char *argv[]) {
  short ccbport;
  Clog *myLog;
  Ccommand *myCmd;
  Cccb *myCcb;
  char ccbserver[256]=CCBSERVER;
  int srvport=CCBPORT;

  
  if (argc < 2) {
    printf("*** Usage: mc_autosetlink [ccbport [ccbserver ccbport]\n");

    printf("ccbport? ");
    scanf("%d",&ccbport);
  }  else 
  if (argc==2) {
    sscanf(argv[1],"%d",&ccbport);
  } else
  if (argc==4) {
    sscanf(argv[1],"%d",&ccbport);
    sscanf(argv[2],"%s",&ccbserver);
    sscanf(argv[3],"%d",&srvport);


  } else 
  {printf("*** Usage: mc_autosetlink [ccbport [ccbserver ccbport]\n"); exit(-1);}
	

#ifdef LOGTODB
  myLog = new Clog(); // Log to DB
#else
  myLog = new Clog("log.txt"); // Log to file
#endif
  //myCmd = new Ccommand(ccbport,CCBSERVER,CCBPORT,myLog);
  myCmd = new Ccommand(ccbport,ccbserver,srvport,myLog);
  myCcb = new Cccb(myCmd); // New CCB object

  myCmd->set_write_log(true); // Switch on logger

  printf("Using server %s:%d\n",myCmd->read_server_name(),myCmd->read_server_port());

  printf("\n\n\n");
  printf("*** auto set link threshold ccbport: %d\n",ccbport);
  printf("*** Actual time: %s\n",get_time());
  myCcb->auto_set_LINK(); // 0x74
  myCcb->auto_set_LINK_print();
}
