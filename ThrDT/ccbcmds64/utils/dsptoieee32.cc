#include <ccbcmds/apfunctions.h>
#include <iostream>
#include <cstdio>

// Author: A.Parenti, Aug01 2006
int main(int argc, char *argv[]) {
  unsigned char array[4];
  int i;
  float ieee32_float;

  if (argc < 5) {
    printf("*** Usage: dsptoieee32 [byte0 byte1 byte2 byte3]\n");

    printf("DSP  (HEX)?\n");
    for (i=0;i<4;++i) {
      printf("Byte %d? ",i);
      scanf("%x",&array[i]);
    }
  }
  else {
    sscanf(argv[1],"%x",&array[0]);
    sscanf(argv[2],"%x",&array[1]);
    sscanf(argv[3],"%x",&array[2]);
    sscanf(argv[4],"%x",&array[3]);
  }

  ccb_to_host(array,&ieee32_float);

  printf("Conversion to IEEE32: %02hhx %02hhx %02hhx %02hhx ---> %f\n",array[0],array[1],array[2],array[3],ieee32_float);

}
