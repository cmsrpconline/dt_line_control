#include <ccbcmds/apfunctions.h>
#include <ccbcmds/Clog.h>
#include <ccbcmds/Ccommand.h>
#include <ccbcmds/Cccb.h>
#include <ccbcmds/Cdef.h>

#ifdef __USE_CCBDB__ // DB access enabled
  #include <ccbdb/Ccmsdtdb.h>
#endif

#include <iostream>
#include <stdio.h>

// Author: A.Parenti, Jul01 2006
// Modified: AP, Aug30 2007
#ifdef __USE_CCBDB__ // DB access enabled
int main(int argc, char *argv[]) {
  bool mc_program;
  char message[1024];
  short pippo, ccbport, ccbid;
  Emcstatus mc_phys_st, mc_logic_st;

  Clog *myLog;
  Ccommand *myCmd;
  Cccb *myCcb;
  Cref *myRef;
  char mccbsrv[64];
  int srvport=CCBPORT;

  if (argc==3) {
    sscanf(argv[1],"%d",&pippo);
    ccbport=pippo;

    sscanf(argv[2],"%d",&pippo);
    ccbid=pippo;

    strcpy(mccbsrv,CCBSERVER);
  } else 
  if (argc==4) {
    sscanf(argv[1],"%d",&pippo);
    ccbport=pippo;

    sscanf(argv[2],"%d",&pippo);
    ccbid=pippo;

    sscanf(argv[3],"%s",&mccbsrv);
  } else 
  if (argc==5) {
    sscanf(argv[1],"%d",&pippo);
    ccbport=pippo;

    sscanf(argv[2],"%d",&pippo);
    ccbid=pippo;

    sscanf(argv[3],"%s",&mccbsrv);
    sscanf(argv[4],"%d",&srvport);
  } else {
    printf("Usage: mc_status [port] [ccbid]\n");

    printf("port? ");
    scanf("%d",&pippo);
    ccbport=pippo;

    printf("ccbid? ");
    scanf("%d",&pippo);
    ccbid=pippo;
  }

  cmsdtdb* mydtdtb= new(cmsdtdb);
  //cmsdtdb mydtdtb;
  myLog = new Clog("log.txt"); // Log to file
  //myCmd = new Ccommand(ccbid,ccbport,mccbsrv,CCBPORT,myLog);
  myCmd = new Ccommand(ccbid,ccbport,1000+ccbid,mccbsrv,srvport,myLog);
  myCcb = new Cccb(myCmd); // New CCB object
  //myRef = new Cref(ccbid,&mydtdtb); // read reference from DB
  myRef = new Cref(ccbid,mydtdtb); // read reference from DB

  printf("Using server %s:%d\n",myCmd->read_server_name(),myCmd->read_server_port());

  printf("*** Actual time: %s\n",get_time());
  myCcb->MC_check_status(myRef,false,&mc_program, &mc_phys_st, &mc_logic_st, message); // QPLL check enabled

  myCcb->MC_check_status_print();

}

#else
int main() {
  std::cout << "Nothing done. Please compile with DB support.\n";
}
#endif
