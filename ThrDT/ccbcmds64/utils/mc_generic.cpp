#include <ccbcmds/apfunctions.h>
#include <ccbcmds/Clog.h>
#include <ccbcmds/Ccommand.h>
#include <ccbcmds/Cdef.h>

#include <iostream>
#include <stdio.h>
#include <string>
#include <fstream>
#include <time.h>
#include <iostream.h>
#include <iomanip.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <string>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include <termios.h>
#include <stropts.h>

using namespace std;
// Author:C.F. Bedoya May07 2008
//*************************
int main(int argc, char *argv[]) {
 
	char c, cmdstr[3*WRITE_DIM]="", *errstr;
  short ccbport;
  int i, errcod;
  char file_command[64];
  unsigned char sntline[WRITE_DIM]; 
  unsigned char rdline[READ_DIM];
  int sntlinelen, rdlinelen;
  
  Clog *myLog;
  Ccommand *myCmd;

  int user_wh;
  int port=0;
  int wheel;
char command[10];

FILE * file_in;
 
  char CCBSERVER0[30]="10.176.60.232";  
 char CCBSERVER1[30]="10.176.60.230";  
 char CCBSERVER2[30]="10.176.60.228";  
 char CCBSERVERm1[30]="10.176.60.234";  
 char CCBSERVERm2[30]="10.176.60.236";  
 char miCCBSERVER[30]; 

  if (argc < 4) {
    printf("*** Usage: mc_generic {wheel} {port} {file_command} \n");
 	printf("Exiting...");
    return 1;
  } 
   if (argc > 3){
     sscanf(argv[1],"%d",&user_wh);
     sscanf(argv[2],"%d",&port);
     sscanf(argv[3],"%s",file_command);
 }

file_in=fopen(file_command,"r");
//fscanf(file_in,"%s",command);
fgets(command,300,file_in);
cout<<"command: "<<command<<endl;

  if (user_wh==2) strcpy(miCCBSERVER,CCBSERVER2);
  if (user_wh==1) strcpy(miCCBSERVER,CCBSERVER1);
  if (user_wh==0) strcpy(miCCBSERVER,CCBSERVER0);
  if (user_wh==-1) strcpy(miCCBSERVER,CCBSERVERm1);
  if (user_wh==-2) strcpy(miCCBSERVER,CCBSERVERm2);

 wheel=user_wh;
 ccbport = port;

errstr=new char[1024];
myLog=new Clog("log.txt"); 
myCmd=new Ccommand(ccbport,miCCBSERVER,CCBPORT,myLog);
myCmd->set_write_log(true);


sprintf(cmdstr,"%s",command);
 printf("string %s\n",command);
string_to_array(cmdstr,16,sntline,&sntlinelen);
sntline[sntlinelen]=0;
printf("%s",sntline);

	printf("Using server %s:%d\n",myCmd->read_server_name(),myCmd->read_server_port());
	printf("*** Actual time: %s\n",get_time());

	errcod=myCmd->send_command(sntline,sntlinelen,TIMEOUT_DEFAULT);
errstr=myCmd->read_error_message(errcod);

 for (int hh=0; hh<(myCmd->data_read_len); ++hh){
   printf(" %d -- 0x%x \n",hh,myCmd->data_read[hh]); 
};

 printf("Error code: %s\n",errstr);  
fclose(file_in);

  return 0;  
};
