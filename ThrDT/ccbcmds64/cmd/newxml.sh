#!/bin/tcsh -f

set virg='"'

# Status: min
#set nomi = (code HVersion LVersion McType flags1 PwrTrb PwrRob  flags2 AlrmPwrTrb AlrmPwrRob AlrmTempTrb AlrmTempRob LoseLockCountTTC LoseLockCountQPLL1 LoseLockCountQPLL2 SeuRam SeuIntRam SeuBTI SeuTRACO SeuLUT SeuTSS VccinMin VddinMin VccMin VddMin Tp1LMin Tp1HMin Tp2LMin Tp2HMin Fe_Vcc_1Min Fe_Vcc_2Min Fe_Vcc_3Min Fe_Vdd_1Min Fe_Vdd_2Min Fe_Vdd_3Min Sp_VccMin Sp_VddMin in_TmaxMin in_TmedMin th_TmaxMin th_TmedMin out_TmaxMin out_TmedMin BrdMaxtempMin)
#set valori = (0x13 1 11 1 0xEFFF 0xCF 0x3F 0 0 0 0 0 0 0 0 0 0 0 0 0 0 5.4 3.7 4.75 3.135 1.8612 3.069 1.8612 3.069 4.75 4.75 4.75 2.375 2.375 2.375 4.75 2.5 150 150 150 150 150 150 15.0)

# Status: max
#set nomi = (VccinMax VddinMax VccMax VddMax Tp1LMax Tp1HMax Tp2LMax Tp2HMax Fe_Vcc_1Max Fe_Vcc_2Max Fe_Vcc_3Max Fe_Vdd_1Max Fe_Vdd_2Max Fe_Vdd_3Max Sp_VccMax Sp_VddMax in_TmaxMax in_TmedMax th_TmaxMax th_TmedMax out_TmaxMax out_TmedMax BrdMaxtempMax)
#set valori = (6.0 4.0 5.25 3.465 1.8988 3.131 1.8988 3.131 5.25 5.25 5.25 2.675 2.675 2.675 5.25 2.7 500 350 500 350 500 350 50.0)

# Test: min
#set nomi = (code flags Dac SbTestJtag SbTestPi TrbPwr TrbBadJtagAddr RobPwr RobBadJtagAddr RobOverlapAddr McType)
#set valori = (0x11 0xFFFF 0x37F 0x403 0 0xDE 0 0x3F 0 0 1)
# Test: max


#@ i=1
#while ($i<$#valori)
#  echo '<'$nomi[$i]'>'$valori[$i]'</'$nomi[$i]'>' >> prova.txt
#  @ i++
#end
#echo '<'$nomi[$i]'>'$valori[$i]'</'$nomi[$i]'>' >> prova.txt

# Test: min (arrays)
set valori = (100 100 100 100 100 100 65535)
@ i=0
@ j=1
while ($i < $#valori)
  echo "<idx=${virg}$i${virg}>$valori[$j]</idx>" >> prova.txt
  @ i++
  @ j++
end
