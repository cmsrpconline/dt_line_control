#ifndef __APFUNCTIONS_H__
#define __APFUNCTIONS_H__
// include apfunctions.h only once

#include <string>

#define SEPARATOR ' ' // space
//#define SEPARATOR '\n' // carriage return

/***************************************/
/* Additional functions of general use */
/***************************************/

// Copy *exactly* <len> characters from str2 to str1.
// Return the No. of copied char.
// Inputs: str1, str2, len.
// Type = "char*" or "unsigned char*"
template <class Type> int mystrcpy(Type, Type, int);


// Write to standard output a string of hex integers
// Inputs: string, string_length.
// Type = "char*" or "unsigned char*"
template <class Type> void strprint(Type, int);

// Convert an array of chars in a text string of hex
// Type = "char*", "unsigned char*"
// Arguments: array_in, array_length, string_out.
template <class Type> void array_to_string(Type, int, char*);
template <class Type> void array_to_string(Type, int, std::string*);

// Convert a text string of hex in an array of chars
// Type = "char*", "unsigned char*"
// Arguments: string_in, base, array_out, array_len
template <class Type> void string_to_array(char*,int,Type,int*);

// Reset a vector
// Type = char*, short*, int* (and unsigned), bool*, float*, double*
// Arguments: vector, vector_size
template <class Type> void vector_reset(Type,int);

// Get the local time, put it in string[26]
char* get_time(void);

// Wait for a key to be pressed
void mywait();

// Sleep for <timewait> msec's
void apsleep(int timewait);

// Conversion from DSP float to IEEE32 format
// Inputs: mantissa, exponent. Output: IEEE32 float.
void DSPtoIEEE32(short, short, float *);

// Conversion from IEEE32 float to DSP float
// Input: IEEE32 float. Outputs: mantissa, exponent.
void IEEE32toDSP(float, short *, short *);

// Return CRC
unsigned short crc(unsigned char *data, int len); // Input: data, data_length

// Convert a number from host notation to ccb (= char array) notation,
// and viceversa
void host_to_ccb(char,unsigned char*);
void host_to_ccb(unsigned char,unsigned char*);
void host_to_ccb(short,unsigned char*);
void host_to_ccb(unsigned short,unsigned char*);
void host_to_ccb(int,unsigned char*);
void host_to_ccb(unsigned int,unsigned char*);
void host_to_ccb(float,unsigned char*);

void ccb_to_host(unsigned char*,char*);
void ccb_to_host(unsigned char*,unsigned char*);
void ccb_to_host(unsigned char*,short*);
void ccb_to_host(unsigned char*,unsigned short*);
void ccb_to_host(unsigned char*,int*);
void ccb_to_host(unsigned char*,unsigned int*);
void ccb_to_host(unsigned char*,float*);

// Read a bit from a char/short/int
bool takebit(unsigned char, int);


// Read a variable from a unsigned char string
// Type = "char*", "unsigned char*", "short*", "unsigned short*",
//        "int*", "unsigned int*", "float*"
template <class Type> int rstring(unsigned char*,int,Type);

// Write a variable to a unsigned char string
// Type = "char", "unsigned char", "short", "unsigned short",
//        "int", "unsigned int", "float"
template <class Type> int wstring(Type,int,unsigned char*);

// Read a file and return the content as a string of char
char* read_file(char *filename);
std::string read_file(std::string filename);

// XML parser. Uses libxml2 from gnome
std::string xml_get_root(std::string); // Recover root tag
std::string xml_get(std::string, char*); // Recover tag content. Input: xml buffer, tag
std::string xml_get(std::string, char*, unsigned int); // Recover tag content. Input: xml buffer, tag, idx

// Get a variable from an xml buffer. Input: xml_buffer, tag. Output: read_variable
void read_from_xml(std::string, char*, unsigned char*);
void read_from_xml(std::string, char*, unsigned short*);
void read_from_xml(std::string, char*, unsigned int*);
void read_from_xml(std::string, char*, char*);
void read_from_xml(std::string, char*, short*);
void read_from_xml(std::string, char*, int*);
void read_from_xml(std::string, char*, float*);
// Get a variable from an xml buffer. Input: xml_buffer, tag, array_idx. Output: read_variable
void read_from_xml(std::string, char*, int, unsigned char*);
void read_from_xml(std::string, char*, int, unsigned short*);
void read_from_xml(std::string, char*, int, unsigned int*);
void read_from_xml(std::string, char*, int, char*);
void read_from_xml(std::string, char*, int, short*);
void read_from_xml(std::string, char*, int, int*);
void read_from_xml(std::string, char*, int, float*);


// Define an array with some additional methods
class flarray {
 public:
  void init(int i); // Initialize the array with dimension <i>
  int size(); // Number of filled elements
  void insert(float x); // Insert a new element <x>
  void clear(); // Clear array content
  float mean(); // Mean over all elements

// Constructor/destructor
  flarray(); // Input: array dimension
  ~flarray();

 private:
  bool initialized;
  int len; // Number of filled elements in the array
  int nmax; // Max number of elements in the array
  float *array; // The array itself
};

#endif // __APFUNCTIONS_H__
