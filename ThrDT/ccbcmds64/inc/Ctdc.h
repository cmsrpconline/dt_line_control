// include Ctdc.h only once
#ifndef __CTDC_H__
#define __CTDC_H__

// defines classes Ccmn_cmd
#include <ccbcmds/Ccmn_cmd.h>

// configurations class
#include <ccbcmds/Cconf.h>


/*****************************************************************************/
// TDC data structures
/*****************************************************************************/

// Configuration results
struct Sconfig_TDC {
  short sent, bad; // Sent and unsuccessful cmds
  short crc;
  bool error;
};

// Sread_TDC and Sstatus_TDC are defined in Ccmn_cmd.h

// Format of return data from "write TDC" (0x44) command
struct Swrite_TDC {
  unsigned char code1, status[8];
  unsigned char code2;
  char narg;
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "write TDC control" (0x18) command
struct Swrite_TDC_control {
  unsigned char code1, code2, result;
  int ccbserver_code;
  std::string msg;
};


/*****************************************************************************/
// TDC class
/*****************************************************************************/

class Ctdc : public Ccmn_cmd {
 public:
// Class constructor; arguments: pointer_to_command_object
  Ctdc(Ccommand*);


/* HIGH LEVEL COMMANDS */

// Configure the TDC. Input: Cconf_object, brd, chip
  Sconfig_TDC config_TDC_result;
  short config_TDC(Cconf*,char,char);
  void config_TDC_print();

//  Retrive TDC config from CCB, dump it to file
  void read_TDC_config(char *filename);

// Check TDC status: defined in Ccmn_cmd

/* LOW LEVEL COMMANDS */

// "read TDC" (0x43) command: defined in Ccmn_cmd

// "write TDC" (0x44) command
  Swrite_TDC write_tdc; // Contains command results
  void write_TDC(char, char, unsigned char*, unsigned char*); // Send command and decode reply. Input: brd, chip, setup[81], control[5]
  void write_TDC_reset(); // Reset write_tdc struct
  int write_TDC_decode(unsigned char*); // Decode reply. Input: data_read
  void write_TDC_print(); // Print reply to standard output

// "write TDC control" (0x18) command
  Swrite_TDC_control write_tdc_control; // Contains command results
  void write_TDC_control(char, char, unsigned char*); // Send command and decode reply. Input: brd, chip, control[5]
  void write_TDC_control_reset(); // Reset write_tdc_control struct
  int write_TDC_control_decode(unsigned char*); // Decode reply. Input: data_read
  void write_TDC_control_print(); // Print reply to standard output

};

#endif // __CTDC_H__
