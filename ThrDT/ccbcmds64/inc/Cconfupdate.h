#ifndef __CCONFUPDATE_H__
#define __CCONFUPDATE_H__
// include Cconfupdate.h only once

/*****************************************************************************/
// Cconfupdate structures
/*****************************************************************************/

// Parameters for theta-BTI configuration
struct Sbti_theta_param {
  short ccbid; // Needed to extract y,z from DB
  float R, z; // When ccbid<=0, the program use these instead of DB values
  int sign; // When ccbid<=0, the program use this instead of DB values
  unsigned int tolerance;
};

// Parameters for FE masking
struct Sfe_mask_param {
  short ccbid;

  short mbtype; // 0-7
  bool chimney; // true if chamber is of "chimney" type
  bool mb4_8; // true if chamber is "mb4_8"

  bool femask[3][4][96];
};

// Parameters for TDC masking
struct Stdc_mask_param {
  short ccbid;

  short mbtype; // 0-7
  bool chimney; // true if chamber is of "chimney" type
  bool mb4_8; // true if chamber is "mb4_8"

  bool tdcmask[3][4][96];
};


// Parameters for BTI masking
struct Sbti_mask_param {
  short ccbid;

  short mbtype; // 0-7
  bool chimney; // true if chamber is of "chimney" type
  bool mb4_8; // true if chamber is "mb4_8"

  bool btimask[3][4][96];
};


/*****************************************************************************/
// Cconfupdate functions
/*****************************************************************************/

// Decode/update command lines for theta-BTI configuration (0x54 code)
// Return 0 if ok, !=0 if command code is not correct
// Parameters read with decode: tolerance
// Parameters needed for update: ccbid or (if ccb<=0) R+z+sign, tolerance
int bti_theta_decode(unsigned char *cmdline, Sbti_theta_param *parameters);
int bti_theta_update(unsigned char *cmdline, Sbti_theta_param parameters);


// Generic mask commands
int mbtype_from_db(short ccbid,short *mbtype,bool *chimney, bool *mb4_8); // Return mbtype, chimney, mb4_8 from DB. Input: ccbid.
bool dt_channel_exist(int ch, short SLayer, short mbtype, bool chimney, bool mb4_8); // Returns true if <ch> exists in the given chamber type
void mask_reset(bool mask[3][4][96]); // Reset a mask
void mask_print(bool mask[3][4][96], short mbtype, bool chimney, bool mb4_8); // Print a mask to stout

// Decode/update command lines for FE mask configuration (0x3A/0x3B codes)
// Return 0 if ok, !=0 if command code is not correct
// Parameters read with decode: femask[][][]
// Parameters needed for decode/update: mbtype, chimney, mb4_8
// Parameters needed for update: femask[][][]
//
int fe_mask_decode(unsigned char *cmdline, Sfe_mask_param *parameters);
int fe_mask_update(unsigned char *cmdline, Sfe_mask_param parameters);
void fe_mask_print(Sfe_mask_param parameters); // Print mask to stout

// Decode/update command lines for TDC mask configuration (0x18/0x44 codes)
// Return 0 if ok, !=0 if command code is not correct
// Parameters read with decode: tdcmask[][][]
// Parameters needed for decode/update: mbtype, chimney, mb4_8
// Parameters needed for update: tdcmask[][][]
//
int tdc_mask_decode(unsigned char *cmdline, Stdc_mask_param *parameters);
int tdc_mask_update(unsigned char *cmdline, Stdc_mask_param parameters);
void tdc_mask_print(Stdc_mask_param parameters); // Print mask to stout

// Decode/update command lines for BTI mask configuration (0x54 code)
// Return 0 if ok, !=0 if command code is not correct
// Parameters read with decode: tdcmask[][][]
// Parameters needed for decode/update: mbtype, chimney, mb4_8
// Parameters needed for update: tdcmask[][][]
//
int bti_mask_decode(unsigned char *cmdline, Sbti_mask_param *parameters);
int bti_mask_update(unsigned char *cmdline, Sbti_mask_param parameters);
void bti_mask_print(Sbti_mask_param parameters); // Print mask to stout



#endif // __CCONFUPDATE_H__
