// include Ctraco.h only once
#ifndef __CTRACO_H__
#define __CTRACO_H__

// defines classes Ccmn_cmd
#include <ccbcmds/Ccmn_cmd.h>

// configurations class
#include <ccbcmds/Cconf.h>

#define RD_TRACO_LUT_MAX 64
#define RD_TRACO_LUT_MIN  1

// default timeouts
#define READ_TRACO_TIMEOUT        5000
#define READ_LUTS_TIMEOUT         5000


/*****************************************************************************/
// TRACO data structures
/*****************************************************************************/

// Configuration results

struct Sconfig_TRACO {
  short sent, bad; // Sent and unsuccessful cmds
  short crc;
  bool error;
};

struct Sconfig_LUT {
  short sent, bad; // Sent and unsuccessful cmds
  short crc;
  bool error;
};

// Format of return data from "Read TRACO" (0x1B) command
struct Sread_TRACO {
  unsigned char code1, conf[10], testin[28], testout[22];
  unsigned char code2;
  char narg;
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "Write TRACO" (0x15) command
// Also used for "Write TRACO LUTS" (0x3E), "preload LUT" (0x4D)
// and "Load LUT" (0x4E), "Write minicrate LUTS" (0xA8)
struct Swrite_TRACO {
  unsigned char code1, code2;
  char result;
  int ccbserver_code;
  std::string msg;
};


// Format of return data from "Read TRACO LUT" (0x86) command
struct Sread_LUT {
  char size;
  unsigned char code1;
  short data[64];
  unsigned char code2, error;
  int ccbserver_code;
  std::string msg;
};


// Format of return data from "Read TRACO LUT parameters" (0x88) command
struct Sread_LUT_param {
  unsigned char code1;
  short shift, ST;
  float d, Xc, Xn;
  unsigned char code2, error;
  int ccbserver_code;
  std::string msg;
};


// Format of return data from "Read minicrate LUT parameters" (0xA9) command
struct Sread_mc_lut_param {
  unsigned char code1;
  short ST;
  float d, Xcn;
  short wheel_sign;
  int ccbserver_code;
  std::string msg;
};


/*****************************************************************************/
// TRACO class
/*****************************************************************************/

class Ctraco : public Ccmn_cmd {
 public:
// Class constructor; arguments: pointer_to_ccb_object
  Ctraco(Ccommand*);

/* Override ROB/TDC methods */
  void read_TDC(char,char);
  int read_TDC_decode(unsigned char*);
  void read_TDC_print();

  void status_TDC(char,char);
  void status_TDC_print();

  void read_ROB_error();
  int read_ROB_error_decode(unsigned char*);
  void read_ROB_error_print();

/* HIGH LEVEL COMMANDS */

// Configure the TRACO. Input: Cconf_object, brd, chip
  Sconfig_TRACO config_TRACO_result;
  short config_TRACO(Cconf*,char,char);
  void config_TRACO_print();

//  Retrive TRACO config from CCB, dump it to file
  void read_TRACO_config(char *filename);

// Configure the LUT. Input: Cconf_object, brd, chip
  Sconfig_LUT config_LUT_result;
  short config_LUT(Cconf*,char,char);
  void config_LUT_print();

//  Retrive LUT config from CCB, dump it to file
  void read_LUT_config(char *filename);

/* LOW LEVEL COMMANDS */

// "Read TRACO" (0x1B) command
  Sread_TRACO read_traco; // Contains command results
  void read_TRACO(char, char); // Send command and decode reply. Input: brd, chip.
  void read_TRACO_reset(); // Reset read_traco struct
  int read_TRACO_decode(unsigned char*); // Decode reply. Input: data_read
  void read_TRACO_print(); // print reply to stdout

// "Write TRACO" (0x15) command
  Swrite_TRACO write_traco; // Contains command results
  void write_TRACO(char, char, unsigned char*, unsigned char*); // Send command and decode reply. Inputs: brd, chip, conf[10], testin[28]
  void write_TRACO_reset(); // Reset write_traco struct
  int write_TRACO_decode(unsigned char*); // Decode reply. Input: data_read
  void write_TRACO_print(); // Print reply to standard output

// "Write TRACO LUTS" (0x3E) command
  Swrite_TRACO write_traco_luts; // Contains command results
  void write_TRACO_LUTS(char, char, short, short, float, float, float); // Send command and decode reply. Inputs: brd, chip, shift, ST, d, Xc, Xn
  void write_TRACO_LUTS_reset(); // Reset write_traco_luts struct
  int write_TRACO_LUTS_decode(unsigned char*); // Decode reply. Inputs: data_read
  void write_TRACO_LUTS_print(); // Print reply to standard output

// "Write minicrate LUTS" (0xA8) command
  Swrite_TRACO write_mc_luts; // Contains command results
  void write_MC_LUTS(short,float,float,short); // Send command and decode reply. Inputs: ST, d, Xcn, wheel_sign (-1/+1)
  void write_MC_LUTS_reset(); // Reset write_traco_luts struct
  int write_MC_LUTS_decode(unsigned char*); // Decode reply. Inputs: data_read
  void write_MC_LUTS_print(); // Print reply to standard output

// "preload LUTS" (0x4D) command
  Swrite_TRACO preload_luts; // Contains command results
  void preload_LUTS(char, char, char, short, short*); // Send command and decode reply. Inputs: brd, chip, ang, addr, data[64]
  void preload_LUTS_reset(); // Reset preload_luts struct
  int preload_LUTS_decode(unsigned char*); // Decode reply. Input:data_read
  void preload_LUTS_print(); // Print reply to standard output

// "load LUTS" (0x4E) command
  Swrite_TRACO load_luts; // Contains command results
  void load_LUTS(char, char); // Send command and decode reply. Input: brd, chip
  void load_LUTS_reset(); // Reset load_luts struct
  int load_LUTS_decode(unsigned char*); // Decode reply. Input: data_read
  void load_LUTS_print(); // Print reply to standard output

// "Read TRACO LUT" (0x86) command
  Sread_LUT read_luts; // Contains command results
  void read_LUTS(char, char, char,short,char); // Send command and decode reply. Inputs: brd, chip, ang (0 or 1), addr, size.
  void read_LUTS_reset(); // Reset read_luts struct
  int read_LUTS_decode(unsigned char*); // Decode reply. Input: data_read
  void read_LUTS_print(); // Print reply to standard output

// "Read TRACO LUT parameters" (0x88) command
  Sread_LUT_param read_luts_param; // Contains command results
  void read_LUTS_param(char, char); // Send command and decode reply. Input: brd, chip.
  void read_LUTS_param_reset(); // Reset read_luts_param struct
  int read_LUTS_param_decode(unsigned char*); // Decode reply. Input: data_read
  void read_LUTS_param_print(); // Print reply to standard output

// "Read minicrate LUT parameters" (0xA9) command
  Sread_mc_lut_param read_mc_lut_param; // Contains command results
  void read_MC_LUTS_param(); // Send command and decode reply.
  void read_MC_LUTS_param_reset(); // Reset read_luts_param struct
  int read_MC_LUTS_param_decode(unsigned char*); // Decode reply. Input: data_read
  void read_MC_LUTS_param_print(); // Print reply to standard output

};

#endif // __CTRACO_H__
