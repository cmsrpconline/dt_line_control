// Include Cmisc.h only once
#ifndef __CMISC_H__
#define __CMISC_H__

// defines classes Ccmn_cmd
#include <ccbcmds/Ccmn_cmd.h>

// some string dimensions
#define RD_TRG_HISTO_DATA_DIM 100

// misc timeouts
#define SYNC_TIMEOUT            15000
#define SNAP_RESET_TIMEOUT      15000
#define SOFT_RESET_TIMEOUT      15000
#define SELECT_L1A_VETO_TIMEOUT 15000
#define CLEAR_TRG_HISTO_TIMEOUT 15000
#define READ_TRG_HISTO_TIMEOUT   5000
#define TRG_FREQ_TIMEOUT         5000

/*****************************************************************************/
// MISC data structures
/*****************************************************************************/

// Format of return data from "SYNC" (0x2B) command, also
// "Snap Reset" (0x2C), "Soft Reset" (0x2D), "Select L1A/veto mode" (0x9F),
// "Clear Trigger Histogram" (0xAD)
struct Smisc_1 {
  unsigned char code1, code2;
  int ccbserver_code;
  std::string msg;
};

// Format of return data from "Read Trigger Histogram" (0xB0) command
struct Srd_trg_histo {
  unsigned char code1, code2;
  char error;
  short data[RD_TRG_HISTO_DATA_DIM];
  int ccbserver_code;
  std::string msg;
};


// Format of return data from "Trigger Frequency" (0xB2) command
struct Strg_freq {
  unsigned char code;
  unsigned short frequency;
  int ccbserver_code;
  std::string msg;
};


/*****************************************************************************/
// MISC class
/*****************************************************************************/

class Cmisc : public Ccmn_cmd {
 public:
// Class constructor; arguments: pointer_to_command_object
  Cmisc(Ccommand* ppp);

/* Override ROB/TDC methods */
  void read_TDC(char,char);
  int read_TDC_decode(unsigned char*);
  void read_TDC_print();

  void status_TDC(char,char);
  void status_TDC_print();

  void read_ROB_error();
  int read_ROB_error_decode(unsigned char*);
  void read_ROB_error_print();

// "SYNC" (0x2B) command
  Smisc_1 sync; // contains command results
  void SYNC(); // Send command and decode reply
  void SYNC_reset(); // Reset sync struct
  int SYNC_decode(unsigned char*); // Decode reply. Input: data_read
  void SYNC_print(); // print reply to stdout

// "Snap Reset" (0x2C) command
  Smisc_1 snap_reset; // contains command results
  void SNAP_reset(); // Send command and decode reply
  void SNAP_reset_reset(); // Reset snap_reset struct
  int SNAP_reset_decode(unsigned char*); // Decode reply. Input: data_read
  void SNAP_reset_print(); // print reply to stdout

// "Soft Reset" (0x2D) command
  Smisc_1 soft_reset; // contains command results
  void SOFT_reset(); // Send command and decode reply
  void SOFT_reset_reset(); // Reset soft_reset struct
  int SOFT_reset_decode(unsigned char*); // Decode reply
  void SOFT_reset_print(); // print reply to stdout

// "Select L1A/veto mode" (0x9F) command
  Smisc_1 select_l1a_veto; // contains command results.
  void select_L1A_veto(char mode); // Send command and decode reply. Input: mode (1=L1A, 0=veto)
  void select_L1A_veto_reset(); // Reset select_l1a_veto struct
  int select_L1A_veto_decode(unsigned char*); // Decode reply
  void select_L1A_veto_print(); // print reply to stdout

// "Clear Trigger Histogram" (0xAD) command
Smisc_1 clear_trg_histo; // contains command results.
  void clear_TRIGGER_histogram(); // Send command and decode reply.
  void clear_TRIGGER_histogram_reset(); // Reset clear_trg_histo struct
  int clear_TRIGGER_histogram_decode(unsigned char*); // Decode reply
  void clear_TRIGGER_histogram_print(); // print reply to stdout

// "Read Trigger Histogram" (0xB0) command
Srd_trg_histo rd_trg_histo; // contains command results.
  void read_TRIGGER_histogram(short); // Send command and decode reply. Input: offset (0-3996)
  void read_TRIGGER_histogram_reset(); // Reset rd_trg_histo struct.
  int read_TRIGGER_histogram_decode(unsigned char*); // Decode reply
  void read_TRIGGER_histogram_print(); // print reply to stdout

// "Trigger Frequency" (0xB2) command
Strg_freq trg_freq; // contains command results.
  void TRIGGER_frequency(); // Send command and decode reply.
  void TRIGGER_frequency_reset(); // Reset trg_freq struct.
  int TRIGGER_frequency_decode(unsigned char*); // Decode reply
  void TRIGGER_frequency_print(); // print reply to stdout

};

#endif // __CMISC_H__
