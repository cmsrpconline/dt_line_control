#! /bin/tcsh -f
# set the correct environmnet and run "main"

# base path
set base_path = `cd ..; pwd`

# set user limits
limit stacksize 2048

# execute program "main"
env LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${base_path}/lib ${base_path}/exe/main $*
#env LD_LIBRARY_PATH=${base_path}/lib ${base_path}/exe/main $*

