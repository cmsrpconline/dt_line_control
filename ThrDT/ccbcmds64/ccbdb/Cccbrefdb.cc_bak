#include "Cccbrefdb.h"
#include <cstring>

#define CLOB_DIM 4000 // max dimension of clob

char strreftype[][7]={"boot","status","test"};

ccbrefdb::ccbrefdb() {
  dtdbobj = new cmsdtdb;
}

ccbrefdb::ccbrefdb(cmsdtdb* thisobj) {
  dtdbobj=thisobj;
}

// Insert a new record in ccbref
// Author: A. Parenti, Apr28 2006
// Modified: AP, Apr18 2007
int ccbrefdb::insert(short mctype, reftype rtype, char *refxml, char *cmnt) {
  char mquer[FIELD_MAX_LENGTH];
  int rows=0, fields=0, resu=0;

  int refid = dtdbobj->get_counter("refid","ccbref")+1; // Set refid to MAX(refid)+1;

  dtdbobj->dblock(); // lock the mutex

  if (refid>0) { // Write reference to DB
    char tmpxml[CLOB_DIM+1];
    int lenxml = strlen(refxml);

    sprintf(mquer,"INSERT INTO ccbref (refid,reftype,cmnt) VALUES ('%d','%s','%s')",refid,strreftype[rtype],cmnt);
    resu=dtdbobj->sqlquery(mquer,&rows,&fields);
    if (resu!=0) {
      dtdbobj->dbunlock(); // unlock the mutex
      return resu; // return if error
    }

    if (rtype!=boot) { // Status or test reference: write mctype
      sprintf(mquer,"UPDATE ccbref SET mctype='%d' WHERE refid='%d'",mctype,refid);
      resu=dtdbobj->sqlquery(mquer,&rows,&fields);

      if (resu!=0) {
        dtdbobj->dbunlock(); // unlock the mutex
        return resu; // return if error
      }
    }

// Write first chunk of xml
    strncpy(tmpxml,refxml,CLOB_DIM);
    tmpxml[CLOB_DIM]='\0';
    sprintf(mquer,"UPDATE ccbref SET refxml1='%s' WHERE refid='%d'",tmpxml,refid);
    resu=dtdbobj->sqlquery(mquer,&rows,&fields);

    if (resu!=0) {
      dtdbobj->dbunlock(); // unlock the mutex
      return resu; // return if error
    }

// Eventually write second chunk of xml
    if (lenxml>CLOB_DIM) {
      strncpy(tmpxml,refxml+CLOB_DIM,CLOB_DIM);
      tmpxml[CLOB_DIM]='\0';
      sprintf(mquer,"UPDATE ccbref SET refxml2='%s' WHERE refid='%d'",tmpxml,refid);
      resu=dtdbobj->sqlquery(mquer,&rows,&fields);

      if (resu!=0) {
        dtdbobj->dbunlock(); // unlock the mutex
        return resu; // return if error
      }
    }

// Eventually write third chunk of xml
    if (lenxml>2*CLOB_DIM) {
      strncpy(tmpxml,refxml+2*CLOB_DIM,CLOB_DIM);
      tmpxml[CLOB_DIM]='\0';
      sprintf(mquer,"UPDATE ccbref SET refxml3='%s' WHERE refid='%d'",tmpxml,refid);
      resu=dtdbobj->sqlquery(mquer,&rows,&fields);

      if (resu!=0) {
        dtdbobj->dbunlock(); // unlock the mutex
        return resu; // return if error
      }
    }

    dtdbobj->dbunlock(); // unlock the mutex
    return 0;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return -1;
}


// Read a record from ccbref. Input: ccbid, rtype.
// Author: A. Parenti, Jan19 2006
// Modified: AP, Apr04 2007 (Tested on MySQL/Oracle)
int ccbrefdb::retrieveref(int ccbid,reftype rtype,char *refxml,char *cmnt) {
  int rows=0, fields=0, resu=0;
  int refid=0;
  char mquer[LEN2], myfie[FIELD_MAX_LENGTH];

  dtdbobj->dblock(); // lock the mutex

// Reset fields
  strcpy(refxml,"");
  strcpy(cmnt,"");

// Find RefId
  sprintf(mquer,"SELECT %srefid FROM refrelations WHERE ccbid='%d'",strreftype[rtype],ccbid);
  resu = dtdbobj->sqlquery(mquer,&rows,&fields);

  if (resu==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {
    dtdbobj->returnfield(0,myfie);   
    sscanf(myfie,"%d",&refid);
  }

  if (refid<=0) {
    printf("Reference for ccbid=%d not found.\n",ccbid);

    dtdbobj->dbunlock(); // unlock the mutex
    return -1;
  }

  sprintf(mquer,"SELECT refxml1,refxml2,refxml3,cmnt FROM ccbref WHERE refid='%d'",refid);
  resu = dtdbobj->sqlquery(mquer,&rows,&fields);

  if (resu==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {

    dtdbobj->returnfield(0,myfie);
    strcpy(refxml,myfie);

    dtdbobj->returnfield(1,myfie);
    strcat(refxml,myfie);

    dtdbobj->returnfield(2,myfie);
    strcat(refxml,myfie);

    dtdbobj->returnfield(3,myfie);
    strcpy(cmnt,myfie);


    dtdbobj->dbunlock(); // unlock the mutex
    return 0;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return -1;
}

// Read a record from ccbref. Input: refid.
// Author: A. Parenti, Jan17 2006
// Modified: AP, Apr04 2007 (Tested on MySQL/Oracle)
int ccbrefdb::retrieveref(int refid,short *mctype,reftype *rtype,char *refxml,char *cmnt) {
  int rows=0, fields=0, resu=0;
  char mquer[LEN2];

  dtdbobj->dblock(); // lock the mutex

// Reset fields
  *mctype = 0;
  strcpy(refxml,"");
  strcpy(cmnt,"");

// Query
  sprintf(mquer,"SELECT mctype,reftype,refxml1,refxml2,refxml3,cmnt FROM ccbref WHERE refid='%d'",refid);
  resu = dtdbobj->sqlquery(mquer,&rows,&fields);

  if (resu==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {
    char myfie[FIELD_MAX_LENGTH];

    dtdbobj->returnfield(0,myfie);
    sscanf(myfie,"%d",mctype);

    dtdbobj->returnfield(1,myfie);
    if (strstr(myfie,"boot")!=NULL)
      *rtype=boot;
    else if (strstr(myfie,"status")!=NULL)
      *rtype=status;
    else if (strstr(myfie,"test")!=NULL)
      *rtype=test;

    dtdbobj->returnfield(2,myfie);   
    strcpy(refxml,myfie);

    dtdbobj->returnfield(3,myfie);
    strcat(refxml,myfie);

    dtdbobj->returnfield(4,myfie);
    strcat(refxml,myfie);

    dtdbobj->returnfield(5,myfie);
    strcpy(cmnt,myfie);

    dtdbobj->dbunlock(); // unlock the mutex
    return 0;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return -1;
}

// Retrieve records from ccbref. Input: mctype, rtype.
// Author: A. Parenti, Jan18 2006
// Modified: AP, Apr04 2007 (Tested on MySQL/Oracle)
int ccbrefdb::retrieveref(short mctype, reftype rtype, int *howmany, int *refid, char cmnt[][LEN2]) {
  int rows=0, fields=0, resu=0;
  char mquer[LEN2];

  dtdbobj->dblock(); // lock the mutex

// Reset variables
  *howmany = 0;

  if (rtype==boot)
    sprintf(mquer,"SELECT refid,cmnt FROM ccbref WHERE reftype='%s' ORDER BY refid",strreftype[rtype]);
  else
    sprintf(mquer,"SELECT refid,cmnt FROM ccbref WHERE reftype='%s' AND mctype='%d' ORDER BY refid",strreftype[rtype],mctype);

  resu=dtdbobj->sqlquery(mquer,&rows,&fields);

  if (resu==0) {
    char myfie[LEN1];
    int kk;

    for (kk=0;dtdbobj->fetchrow()==SQL_SUCCESS;++kk) {
      dtdbobj->returnfield(0,myfie);
      sscanf(myfie,"%d",&refid[*howmany]);

      dtdbobj->returnfield(1,cmnt[*howmany]);

      *howmany+=1;
    }

    dtdbobj->dbunlock(); // unlock the mutex
    return 0;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return -1;
}

// Retrieve last record from ccbref. Input: mctype, rtype.
// Author: A. Parenti, Jan18 2006
// Modified: AP, Apr04 2007
int ccbrefdb::retrievelastref(short mctype, reftype rtype, int *refid, char cmnt[LEN2]) {
  int rows=0, fields=0, resu=0;
  char mquer[LEN2];

  dtdbobj->dblock(); // lock the mutex

// Reset values
  *refid = 0;
  strcpy(cmnt,"");

  if (rtype==boot)
    sprintf(mquer,"SELECT refid,cmnt FROM ccbref WHERE reftype='%s' ORDER BY refid DESC",strreftype[rtype]);
  else
    sprintf(mquer,"SELECT refid,cmnt FROM ccbref WHERE reftype='%s' AND mctype='%d' ORDER BY refid DESC",strreftype[rtype],mctype);

  resu=dtdbobj->sqlquery(mquer,&rows,&fields);

  if (resu==0 && dtdbobj->fetchrow()==SQL_SUCCESS) {
    char myfie[FIELD_MAX_LENGTH];

    dtdbobj->returnfield(0,myfie);
    sscanf(myfie,"%d",refid);

    dtdbobj->returnfield(1,myfie);
    strcpy(cmnt,myfie);

    dtdbobj->dbunlock(); // unlock the mutex
    return 1;
  }

  dtdbobj->dbunlock(); // unlock the mutex
  return -1;
}
