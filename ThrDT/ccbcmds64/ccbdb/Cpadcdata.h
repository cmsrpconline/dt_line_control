#ifndef _padcdata_
#define _padcdata_
#include "Ccmsdtdb.h"

class padcdata {
 public:
   
  padcdata(); // Create a new connection to DB
  padcdata(cmsdtdb* dtdbobj); // Use an existing connection to DB
  ~padcdata();

  int select(int thisccb); // Select a PADC and recover info

// Print PADC info
  void padcinfo();
  void padcinfo(int thisccb);

// Retrieve PADC data
  int retrievepadcdata(short adc_count[10],float data_read[10]);
  int retrievepadcdata(int thisccb,short adc_count[10],float data_read[10]);

 private:
  bool newconn; // True if a new connection to DB is open
  char Datetime[32];
  int PadcDataId; // ID of data in padcdata table
  int PadcId, ManifoldFeId, ManifoldHvId; // PADC IDs
  int ccbid, wheel, sector, station; // Chamber IDs

  void reset(); // Reset the class

  cmsdtdb* dtdbobj;
};

#endif
