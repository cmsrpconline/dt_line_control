#ifndef _ccbmap_
#define _ccbmap_
#include "Ccmsdtdb.h"

class ccbmap {

 public:
  int mId;
  int wheel,sector,station,chamber,minicrate;
  int online;
  int port, secport; // Primary and secondary port
  char ccbserver[LEN1], comment[LEN1];

  ccbmap(); // Create a new connection to DB
  ccbmap(cmsdtdb* dtdbobj); // Use an existing connection to DB
  ~ccbmap();

  int insert(int ccbid, int wh, int sec, int sta, int cha, int min,
             int onl, int prt, int sprt, char *serv, char *comm);
  int insert();
  int setrefs(int ccbid,reftype rtype,int refid);
  int setrefs(int ccbid,int bootrefid,int statusrefid,int testrefid);
  int retrieve(int ccbID);
  int retrieve(int wh,int sec,int sta); // Retrieve ccb info using wheel, sector, station
  int isonline(int ccbID);
  int setonline();
  int setonline(int ccbID);
  int setoffline();
  int setoffline(int ccbID);
  int getport();
  int getport(int ccbID);
  int setport(int mport);
  int setport(int ccbID,int mport);
  int getsecport();
  int getsecport(int ccbID);
  int setsecport(int mport);
  int setsecport(int ccbID,int mport);

// Get/set <ccbmap.ccbserver>
  int getserver(char *ccbserver);
  int getserver(int ccbID,char *ccbserver);
  int setserver(char *ccbserver);
  int setserver(int ccbID,char *ccbserver);

// Retrieve/insert <ccbgeom.y> and <ccbgeom.z>
  int retrieve_BTItheta(int ccbID, float *R, float *z, int *sign);
  int insert_BTItheta(int ccbID, float R, float z, int sign);

 private:
  bool newconn; // True if a new connection to DB is open
  cmsdtdb* dtdbobj;
};

#endif
