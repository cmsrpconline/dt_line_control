-- WHEEL -2 --
-- AP, last updated Oct10 2007 --

-- SECTOR 1 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 84,-2,1,1,0,1084,13);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (122,-2,1,2,0,1122,12);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 52,-2,1,3,0,1152,57);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (330,-2,1,4,0,1330, 8);

-- SECTOR 2 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 88,-2,2,1,0,1088,12);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (370,-2,2,2,0,1370,19);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (341,-2,2,3,0,1341,60);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (367,-2,2,4,0,1367, 6);

-- SECTOR 3 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (384,-2,3,1,0,1384,29);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (375,-2,3,2,0,1375,27);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (377,-2,3,3,0,1377,50);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (227,-2,3,4,0,1227, 4);

-- SECTOR 4 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (127,-2,4,1,0,1127,14);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (366,-2,4,2,0,1366,22);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 96,-2,4,3,0,1096,36);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (369,-2,4,4,0,1369, 5);

-- SECTOR 5 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (125,-2,5,1,0,1125,15);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (226,-2,5,2,0,1226,12);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (234,-2,5,3,0,1234,21);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (131,-2,5,4,0,1131, 5);

-- SECTOR 6 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (343,-2,6,1,0,1343,26);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (326,-2,6,2,0,1326,28);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (328,-2,6,3,0,1328,58);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (365,-2,6,4,0,1365, 6);

-- SECTOR 7 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (338,-2,7,1,0,2338,25);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (130,-2,7,2,0,2130, 9);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 86,-2,7,3,0,2086,52);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (381,-2,7,4,0,2381, 7);

-- SECTOR 8 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (363,-2,8,1,0,2363,24);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (142,-2,8,2,0,2142,13);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 69,-2,8,3,0,2069, 4);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (115,-2,8,4,0,2115, 3);

-- SECTOR 9 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (364,-2,9,1,0,2364,30);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (211,-2,9,2,0,2211, 6);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (175,-2,9,3,0,2175,56);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (379,-2,9,4,0,2379, 5);

-- SECTOR 10 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (241,-2,10,1,0,2241, 3);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (111,-2,10,2,0,2111,10);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (129,-2,10,3,0,2129,37);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 87,-2,10,4,0,2087, 2);

-- SECTOR 11 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 33,-2,11,1,0,2033, 8);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (116,-2,11,2,0,2116,11);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 47,-2,11,3,0,2047,41);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (356,-2,11,4,0,2356, 3);

-- SECTOR 12 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (239,-2,12,1,0,2239, 7);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (325,-2,12,2,0,2325,25);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (152,-2,12,3,0,2152,30);
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (123,-2,12,4,0,2123, 3);

-- SECTOR 13 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES (362,-2,13,4,0,1362, 6);

-- SECTOR 14 --
INSERT INTO ccbmap (ccbid,wheel,sector,station,port,secport,minicrate) VALUES ( 91,-2,14,4,0,2091, 2);

