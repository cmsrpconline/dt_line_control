#include <ccbcmds/Cmisc.h>
#include <ccbcmds/apfunctions.h>

#include <iostream>
#include <cstdio>

/*****************************************************************************/
// MISC class
/*****************************************************************************/

// Class constructor; arguments: pointer_to_command_object
// Author: A.Parenti, Dec01 2006
// Modified: AP, Oct09 2007
Cmisc::Cmisc(Ccommand* ppp) : Ccmn_cmd(ppp) {
  SYNC_reset(); // Reset sync struct
  SNAP_reset_reset(); // Reset snap_reset struct
  SOFT_reset_reset(); // Reset soft_reset struct
  select_L1A_veto_reset(); // Reset select_l1a_veto struct
  clear_TRIGGER_histogram_reset(); // Reset clear_trg_histo struct
  read_TRIGGER_histogram_reset(); // Reset rd_trg_histo struct
  TRIGGER_frequency_reset(); // Reset trg_freq struct
}

/****************************/
/* Override ROB/TDC methods */
/****************************/

void Cmisc::read_TDC(char tdc_brd, char tdc_chip) {}
int Cmisc::read_TDC_decode(unsigned char *rdstr) {return 0;}
void Cmisc::read_TDC_print() {}

void Cmisc::status_TDC(char tdc_brd, char tdc_chip) {}
void Cmisc::status_TDC_print() {}

void Cmisc::read_ROB_error() {}
int Cmisc::read_ROB_error_decode(unsigned char *rdstr) {return 0;}
void Cmisc::read_ROB_error_print() {}

/*************************/
/* "SYNC" (0x2B) command */
/*************************/

// Send "SYNC" (0x2B) command and decode reply
// Author: A. Parenti, Jan16 2005
// Modified: AP, Mar24 2005
// Tested at PD, Mar29 2005
void Cmisc::SYNC() {
  const unsigned char command_code=0x2B;
  unsigned char command_line[1];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;

  SYNC_reset(); // Reset sync struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1,SYNC_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = SYNC_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    sync.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nSYNC (0x2B): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset sync struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec07 2006
void Cmisc::SYNC_reset() {
  if (this==NULL) return; // Object deleted -> return

  sync.code1=sync.code2=0;

  sync.ccbserver_code=-5;
  sync.msg="";
}


// Decode "SYNC" (0x2B) reply
// Author: A. Parenti, Feb03 2005
// Modified: AP, Dec07 2006
int Cmisc::SYNC_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x2B, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure

  idx = rstring(rdstr,idx,&sync.code1);
  idx = rstring(rdstr,idx,&sync.code2);

// Fill-up the error code
  if (sync.code1==CCB_BUSY_CODE) // ccb is busy
    sync.ccbserver_code = -1;
  else if (sync.code1==CCB_UNKNOWN1 && sync.code2==CCB_UNKNOWN2) // ccb: unknown command
    sync.ccbserver_code = 1;
  else if (sync.code1==right_reply && sync.code2==command_code) // ccb: right reply
    sync.ccbserver_code = 0;
  else // wrong ccb reply
    sync.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  sync.msg="\nSYNC (0x2B)\n";
  if (sync.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(sync.ccbserver_code)); sync.msg+=tmpstr;
  } else {

    sprintf(tmpstr,"code1: %#2X\n", sync.code1); sync.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n", sync.code2); sync.msg+=tmpstr;
  }

  return idx;
}

// Print result of "SYNC" (0x2B) to stdout
// Author: A. Parenti, Jan16 2005
// Modified: AP, Dec07 2006
void Cmisc::SYNC_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",sync.msg.c_str());
}


/*******************************/
/* "SNAP reset" (0x2C) command */
/*******************************/

// Send "SNAP reset" (0x2C) command and decode reply
// Author: A. Parenti, Jan16 2005
// Modified: AP, Mar24 2005
// Tested at PD, Mar29 2005
void Cmisc::SNAP_reset() {
  const unsigned char command_code=0x2C;
  unsigned char command_line[1];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;

  SNAP_reset_reset(); // Reset snap_reset struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1,SNAP_RESET_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = SNAP_reset_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    snap_reset.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nSNAP reset (0x2C): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset snap_reset struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec07 2006
void Cmisc::SNAP_reset_reset() {
  if (this==NULL) return; // Object deleted -> return

  snap_reset.code1=snap_reset.code2=0;

  snap_reset.ccbserver_code=-5;
  snap_reset.msg="";
}


// Decode "SNAP reset" (0x2C) reply
// Author: A. Parenti, Feb03 2005
// Modified: AP, Dec07 2006
int Cmisc::SNAP_reset_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x2C, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure

  idx = rstring(rdstr,idx,&snap_reset.code1);
  idx = rstring(rdstr,idx,&snap_reset.code2);

// Fill-up the error code
  if (snap_reset.code1==CCB_BUSY_CODE) // ccb is busy
    snap_reset.ccbserver_code = -1;
  else if (snap_reset.code1==CCB_UNKNOWN1 && snap_reset.code2==CCB_UNKNOWN2) // ccb: unknown command
    snap_reset.ccbserver_code = 1;
  else if (snap_reset.code1==right_reply && snap_reset.code2==command_code) // ccb: right reply
    snap_reset.ccbserver_code = 0;
  else // wrong ccb reply
    snap_reset.ccbserver_code = 2;

  char tmpstr[100];

  snap_reset.msg="\nSNAP reset (0x2C)\n";

  if (snap_reset.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(snap_reset.ccbserver_code)); snap_reset.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n", snap_reset.code1); snap_reset.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n", snap_reset.code2); snap_reset.msg+=tmpstr;
  }

  return idx;
}


// Print result of "SNAP reset" (0x2C) to stdout
// Author: A. Parenti, Jan16 2005
// Modified: AP, Dec07 2006
void Cmisc::SNAP_reset_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",snap_reset.msg.c_str());
}



/*******************************/
/* "SOFT reset" (0x2D) command */
/*******************************/

// Send "SOFT reset" (0x2D) command and decode reply
// Author: A. Parenti, Jan16 2005
// Modified: AP, Mar24 2005
// Tested at Pd, Mar29 2005
void Cmisc::SOFT_reset() {
  const unsigned char command_code=0x2D;
  unsigned char command_line[1];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;

  SOFT_reset_reset(); // Reset soft_reset struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1,SOFT_RESET_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = SOFT_reset_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    soft_reset.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nSOFT reset (0x2D): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset soft_reset struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec07 2006
void Cmisc::SOFT_reset_reset() {
  if (this==NULL) return; // Object deleted -> return

  soft_reset.code1=soft_reset.code2=0;

  soft_reset.ccbserver_code=-5;
  soft_reset.msg="";
}


// Decode "SOFT reset" (0x2D) reply
// Author: A. Parenti, Feb03 2005
// Modified: AP, Dec07 2006
int Cmisc::SOFT_reset_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x2D, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure

  idx = rstring(rdstr,idx,&soft_reset.code1);
  idx = rstring(rdstr,idx,&soft_reset.code2);

// Fill-up the error code
  if (soft_reset.code1==CCB_BUSY_CODE) // ccb is busy
    soft_reset.ccbserver_code = -1;
  else if (soft_reset.code1==CCB_UNKNOWN1 && soft_reset.code2==CCB_UNKNOWN2) // ccb: unknown command
    soft_reset.ccbserver_code = 1;
  else if (soft_reset.code1==right_reply && soft_reset.code2==command_code) // ccb: right reply
    soft_reset.ccbserver_code = 0;
  else // wrong ccb reply
    soft_reset.ccbserver_code = 2;

// Fill-up message
 char tmpstr[100];

  soft_reset.msg="\nSOFT reset (0x2D)\n";

  if (soft_reset.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(soft_reset.ccbserver_code)); soft_reset.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n", soft_reset.code1); soft_reset.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n", soft_reset.code2); soft_reset.msg+=tmpstr;
  }

  return idx;
}


// Print result of "SOFT reset" (0x2D) to stdout
// Author: A. Parenti, Jan16 2005
// Modified: AP, Dec07 2006
void Cmisc::SOFT_reset_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",soft_reset.msg.c_str());
}



/*****************************************/
/* "Select L1A/veto mode" (0x9F) command */
/*****************************************/

// Send "Select L1A/veto mode" (0x9F) command and decode reply
// Author: A. Parenti, Jul20 2006
// Modified: AP, Dec07 2006
void Cmisc::select_L1A_veto(char mode) {
  const unsigned char command_code=0x9F;
  unsigned char command_line[2];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

  mc_firmware_version(); // Get firmware version
  select_L1A_veto_reset(); // Reset select_l1a_veto struct
  if (HVer<1 || (HVer==1 && LVer<13)) { // Firmware < v1.13
    char tmpstr[100];
    sprintf(tmpstr,"Attention: command %#X supported after v1.13 (actual: v%d.%d)\n",command_code,HVer,LVer);
    select_l1a_veto.msg=tmpstr;
    std::cout << tmpstr;
    return;
  }

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  command_line[1]=mode;

// Send the command line
  send_command_result = cmd_obj->send_command(command_line,2,SELECT_L1A_VETO_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = select_L1A_veto_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    select_l1a_veto.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nSelect L1A/veto mode (0x9F): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}

// Reset select_l1a_veto struct
// Author: A.Parenti, Aug08 2006
// Modified: AP, Dec07 2006
void Cmisc::select_L1A_veto_reset() {
  if (this==NULL) return; // Object deleted -> return

  select_l1a_veto.code1=select_l1a_veto.code2=0;

  select_l1a_veto.ccbserver_code=-5;
  select_l1a_veto.msg="";
}


// Decode "Select L1A/veto mode" (0x9F) reply
// Author: A. Parenti, Jul20 2006
// Modified: AP, Dec07 2006
int Cmisc::select_L1A_veto_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x9F, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure

  idx = rstring(rdstr,idx,&select_l1a_veto.code1);

// Fill-up the "result" structure

  idx = rstring(rdstr,idx,&select_l1a_veto.code2);

// Fill-up the error code
  if (select_l1a_veto.code1==CCB_BUSY_CODE) // ccb is busy
    select_l1a_veto.ccbserver_code = -1;
  else if (select_l1a_veto.code1==CCB_UNKNOWN1 && select_l1a_veto.code2==CCB_UNKNOWN2) // ccb: unknown command
    select_l1a_veto.ccbserver_code = 1;
  else if (select_l1a_veto.code1==right_reply && select_l1a_veto.code2==command_code) // ccb: right reply
    select_l1a_veto.ccbserver_code = 0;
  else // wrong ccb reply
    select_l1a_veto.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  select_l1a_veto.msg="\nSelect L1A/veto mode (0x9F)\n";

  if (select_l1a_veto.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(select_l1a_veto.ccbserver_code)); select_l1a_veto.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",select_l1a_veto.code1); select_l1a_veto.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",select_l1a_veto.code2); select_l1a_veto.msg+=tmpstr;
  }

  return idx;
}

// Print result of "Select L1A/veto mode" (0x9F) to stdout
// Author: A. Parenti, Jul20 2006
// Modified: AP, Dec07 2006
void Cmisc::select_L1A_veto_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",select_l1a_veto.msg.c_str());
}


/********************************************/
/* "Clear Trigger Histogram" (0xAD) command */
/********************************************/

// Send "Clear Trigger Histogram" (0xAD) command and decode reply
// Author: A. Parenti, Oct09 2007
void Cmisc::clear_TRIGGER_histogram() {
  const unsigned char command_code=0xAD;
  unsigned char command_line[1];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

  mc_firmware_version(); // Get firmware version
  clear_TRIGGER_histogram_reset(); // Reset clear_trg_histo struct
  if (HVer<1 || (HVer==1 && LVer<20)) { // Firmware < v1.20
    char tmpstr[100];
    sprintf(tmpstr,"Attention: command %#X supported after v1.20 (actual: v%d.%d)\n",command_code,HVer,LVer);
    clear_trg_histo.msg=tmpstr;
    std::cout << tmpstr;
    return;
  }

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;

// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1,CLEAR_TRG_HISTO_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = clear_TRIGGER_histogram_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    clear_trg_histo.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nClear Trigger Histogram (0xAD): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}

// Reset clear_trg_histo struct
// Author: A.Parenti, Oct09 2007
void Cmisc::clear_TRIGGER_histogram_reset() {
  if (this==NULL) return; // Object deleted -> return

  clear_trg_histo.code1=clear_trg_histo.code2=0;

  clear_trg_histo.ccbserver_code=-5;
  clear_trg_histo.msg="";
}


// Decode "Clear Trigger Histogram" (0xAD) reply
// Author: A. Parenti, Oct09 2007
int Cmisc::clear_TRIGGER_histogram_decode(unsigned char *rdstr) {
  const unsigned char command_code=0xAD, right_reply=CCB_OXFC;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure

  idx = rstring(rdstr,idx,&clear_trg_histo.code1);

// Fill-up the "result" structure

  idx = rstring(rdstr,idx,&clear_trg_histo.code2);

// Fill-up the error code
  if (clear_trg_histo.code1==CCB_BUSY_CODE) // ccb is busy
    clear_trg_histo.ccbserver_code = -1;
  else if (clear_trg_histo.code1==CCB_UNKNOWN1 && clear_trg_histo.code2==CCB_UNKNOWN2) // ccb: unknown command
    clear_trg_histo.ccbserver_code = 1;
  else if (clear_trg_histo.code1==right_reply && clear_trg_histo.code2==command_code) // ccb: right reply
    clear_trg_histo.ccbserver_code = 0;
  else // wrong ccb reply
    clear_trg_histo.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  clear_trg_histo.msg="\nClear Trigger Histogram (0xAD)\n";

  if (clear_trg_histo.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(clear_trg_histo.ccbserver_code)); clear_trg_histo.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",clear_trg_histo.code1); clear_trg_histo.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",clear_trg_histo.code2); clear_trg_histo.msg+=tmpstr;
  }

  return idx;
}

// Print result of "Clear Trigger Histogram" (0xAD) to stdout
// Author: A. Parenti, Oct09 2007
void Cmisc::clear_TRIGGER_histogram_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",clear_trg_histo.msg.c_str());
}


/*******************************************/
/* "Read Trigger Histogram" (0xB0) command */
/*******************************************/

// Send "Read Trigger Histogram" (0xB0) command and decode reply
// Author: A. Parenti, Oct09 2007
void Cmisc::read_TRIGGER_histogram(short offset) {
  const unsigned char command_code=0xB0;
  unsigned char command_line[3];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

  mc_firmware_version(); // Get firmware version
  read_TRIGGER_histogram_reset(); // Reset rd_trg_histo struct
  if (HVer<1 || (HVer==1 && LVer<20)) { // Firmware < v1.20
    char tmpstr[100];
    sprintf(tmpstr,"Attention: command %#X supported after v1.20 (actual: v%d.%d)\n",command_code,HVer,LVer);
    rd_trg_histo.msg=tmpstr;
    std::cout << tmpstr;
    return;
  }

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;
  wstring(offset,1,command_line);

// Send the command line
  send_command_result = cmd_obj->send_command(command_line,3,READ_TRG_HISTO_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = read_TRIGGER_histogram_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    rd_trg_histo.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nRead Trigger Histogram (0xB0): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}

// Reset rd_trg_histo struct
// Author: A.Parenti, Oct09 2007
void Cmisc::read_TRIGGER_histogram_reset() {
  if (this==NULL) return; // Object deleted -> return

  int i;

  rd_trg_histo.code1=rd_trg_histo.code2=0;
  rd_trg_histo.error=0;
  for (i=0;i<RD_TRG_HISTO_DATA_DIM;++i)
    rd_trg_histo.data[i]=0;

  rd_trg_histo.ccbserver_code=-5;
  rd_trg_histo.msg="";
}


// Decode "Read Trigger Histogram" (0xB0) reply
// Author: A. Parenti, Oct09 2007
int Cmisc::read_TRIGGER_histogram_decode(unsigned char *rdstr) {
  bool on_error;
  const unsigned char right_reply=0xB1, err_reply=0xAE;
  int i, idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&rd_trg_histo.code1);
  rd_trg_histo.code2=rdstr[1];
  rd_trg_histo.error=rdstr[2];

// Fill-up the "result" structure
  for (i=0;i<RD_TRG_HISTO_DATA_DIM;++i)
    idx=rstring(rdstr,idx,&rd_trg_histo.data[i]);

// Fill-up the error code
  if (rd_trg_histo.code1==CCB_BUSY_CODE) // ccb is busy
    rd_trg_histo.ccbserver_code = -1;
  else if (rd_trg_histo.code1==CCB_UNKNOWN1 && rd_trg_histo.code2==CCB_UNKNOWN2) // ccb: unknown command
    rd_trg_histo.ccbserver_code = 1;
  else if (rd_trg_histo.code1==right_reply) { // ccb: right reply
    rd_trg_histo.ccbserver_code = 0;
    on_error=false;
  }
  else if (rd_trg_histo.code1==CCB_OXFC && rd_trg_histo.code2==err_reply) {// ccb: right reply (but error returned)
    rd_trg_histo.ccbserver_code = 0;
    on_error=true;
  }
  else // wrong ccb reply
    rd_trg_histo.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  rd_trg_histo.msg="\nRead Trigger Histogram (0xB0)\n";

  if (rd_trg_histo.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(rd_trg_histo.ccbserver_code)); rd_trg_histo.msg+=tmpstr;
  } else if (!on_error) {
    sprintf(tmpstr,"code: %#2X\n",rd_trg_histo.code1); rd_trg_histo.msg+=tmpstr;
    for (i=0;i<RD_TRG_HISTO_DATA_DIM;++i) {
      sprintf(tmpstr,"data[%d]: %d\n",i,rd_trg_histo.data[i]); rd_trg_histo.msg+=tmpstr;
    }
  } else {
    sprintf(tmpstr,"code1: %#2X\n",rd_trg_histo.code1); rd_trg_histo.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",rd_trg_histo.code2); rd_trg_histo.msg+=tmpstr;
    sprintf(tmpstr,"error: %d\n",rd_trg_histo.error); rd_trg_histo.msg+=tmpstr;
  }

  return idx;
}

// Print result of "Read Trigger Histogram" (0xB0) to stdout
// Author: A. Parenti, Oct09 2007
void Cmisc::read_TRIGGER_histogram_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",rd_trg_histo.msg.c_str());
}


/*******************************************/
/* "Trigger Frequency" (0xB2) command */
/*******************************************/

// Send "Trigger Frequency" (0xB2) command and decode reply
// Author: A. Parenti, Oct09 2007
void Cmisc::TRIGGER_frequency() {
  const unsigned char command_code=0xB2;
  unsigned char command_line[1];
  int send_command_result, idx=0;

  if (this==NULL) return; // Object deleted -> return

  mc_firmware_version(); // Get firmware version
  TRIGGER_frequency_reset(); // Reset trg_freq struct
  if (HVer<1 || (HVer==1 && LVer<20)) { // Firmware < v1.20
    char tmpstr[100];
    sprintf(tmpstr,"Attention: command %#X supported after v1.20 (actual: v%d.%d)\n",command_code,HVer,LVer);
    trg_freq.msg=tmpstr;
    std::cout << tmpstr;
    return;
  }

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]=command_code;

// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1,TRG_FREQ_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = TRIGGER_frequency_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    trg_freq.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nTrigger Frequency (0xB2): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}

// Reset trg_freq struct
// Author: A.Parenti, Oct09 2007
void Cmisc::TRIGGER_frequency_reset() {
  if (this==NULL) return; // Object deleted -> return

  trg_freq.code=0;
  trg_freq.frequency=0;

  trg_freq.ccbserver_code=-5;
  trg_freq.msg="";
}


// Decode "Trigger Frequency" (0xB2) reply
// Author: A. Parenti, Oct09 2007
int Cmisc::TRIGGER_frequency_decode(unsigned char *rdstr) {
  const unsigned char right_reply=0xB3;
  unsigned char code2;
  int idx=0;

  if (this==NULL) return 0; // Object deleted -> return

// identification code of the structure
  idx = rstring(rdstr,idx,&trg_freq.code);
  code2 = rdstr[idx];

// Fill-up the "result" structure
  idx = rstring(rdstr,idx,&trg_freq.frequency);

// Fill-up the error code
  if (trg_freq.code==CCB_BUSY_CODE) // ccb is busy
    trg_freq.ccbserver_code = -1;
  else if (trg_freq.code==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    trg_freq.ccbserver_code = 1;
  else if (trg_freq.code==right_reply) // ccb: right reply
    trg_freq.ccbserver_code = 0;
  else // wrong ccb reply
    trg_freq.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  trg_freq.msg="\nTrigger Frequency (0xB2)\n";

  if (trg_freq.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(trg_freq.ccbserver_code)); trg_freq.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code: %#2X\n",trg_freq.code); trg_freq.msg+=tmpstr;
    sprintf(tmpstr,"frequency: %d\n",trg_freq.frequency); trg_freq.msg+=tmpstr;
  }

  return idx;
}

// Print result of "Trigger Frequency" (0xB2) to stdout
// Author: A. Parenti, Oct09 2007
void Cmisc::TRIGGER_frequency_print() {
  if (this==NULL) return; // Object deleted -> return

  printf("%s",trg_freq.msg.c_str());
}
