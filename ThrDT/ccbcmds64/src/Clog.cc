#include <ccbcmds/Clog.h>
#include <ccbcmds/apfunctions.h>

#include <iostream>
#include <cstdio>
#include <exception>
#include <string.h>

// class constructor
Clog::Clog(char *this_filename) {
  fname = this_filename;
  logto = UFILE;

  pthread_mutex_init(&log_mutex2,NULL); // initialize log_mutex2
}

// Author: A.Parenti
// Modified: AP, Dec04 2006
Clog::Clog() {
  logto = UDATABASE;
#ifdef __USE_CCBDB__ // DB access enabled
  myLog = new dtlogger; // datasource from /etc/ccbdb.conf
#endif

  pthread_mutex_init(&log_mutex2,NULL); // initialize log_mutex2
}

// Author: A.Parenti
// Modified: AP, Jun18 2007
Clog::~Clog() {
#ifdef __USE_CCBDB__ // DB access enabled
  if (logto==UDATABASE)
    delete myLog; // Disconnect from DB
  else
    logfile.close(); // Close log file
#endif
}

// Create an entry in the log.
// Author: A.Parenti, Jan20 2005
// Modified/Tested: AP, Dec04 2006
int Clog::write_log(char* string, msglevel msg_type) {
  int rt;

  if (this==NULL) return -1; // Object deleted -> return

  if (logto==UDATABASE) { // Log to DB
#ifdef __USE_CCBDB__ // DB access enabled
    if (ENABLE_LOG_MUTEX)
      pthread_mutex_lock(&log_mutex2); // lock "log_mutex2"

    if (msg_type==mesg)
      myLog->Tell(string);
    else if (msg_type==warn)
      myLog->Warn(string);
    else if (msg_type==erro)
      myLog->Erro(string);

    rt = 0;

    if (ENABLE_LOG_MUTEX)
      pthread_mutex_unlock(&log_mutex2); // unlock "log_mutex2"
#else
    rt = 1;
    nodbmsg();
#endif
  } else if (logto==UFILE) { // Log to file

    if (ENABLE_LOG_MUTEX)
      pthread_mutex_lock(&log_mutex2); // lock "log_mutex2"

    try {
      char msgtypetxt[5], tempo[26];

      if (!logfile.is_open()) { // File not open: open it        
        logfile.open(fname.c_str(),std::ios::out | std::ios::app);
      }

      switch (msg_type) {
      case mesg:
        strcpy(msgtypetxt,"mesg");
        break;
      case warn:
        strcpy(msgtypetxt,"warn");
        break;
      case erro:
        strcpy(msgtypetxt,"erro");
        break;
      }

      if (logfile.is_open()) {
        strcpy(tempo,get_time());

        logfile << "*** Time: " << get_time() << std::endl;
        logfile << "*** Msg Level: " << msgtypetxt << std::endl;
        logfile << "*** Begin Msg:\n" << string << std::endl;
        logfile << "*** End Msg\n\n";
        logfile.close();
        rt = 0;
      }
      else {
        std::cout << "Clog: error in logging\n";
        rt = 1;
      }
    }
    catch (std::exception& e) {
      std::cout << "Clog exception: " << e.what();
    }

    if (ENABLE_LOG_MUTEX)
      pthread_mutex_unlock(&log_mutex2); // unlock "log_mutex2"
  }

  return rt;
}
