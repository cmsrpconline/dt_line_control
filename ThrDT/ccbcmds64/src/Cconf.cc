#include <ccbcmds/Cconf.h>
#include <ccbcmds/apfunctions.h>

#include <libxml/parser.h>
#include <libxml/tree.h>

#ifdef __USE_CCBDB__ // DB access enabled
  #include <ccbdb/Cccbconfdb.h> // support for cmsdtdb DB
#endif

#include <iostream>
#include <cstdio>
#include <string.h>

// class constructor
Cconf::Cconf(char *this_name) {

  _verbose_ = VERBOSE_DEFAULT; // Set verbose level to default

  ConfigurationType = UFILE;
  fname = this_name;
  cname = "";
  reset_confmask();
#ifdef __USE_CCBDB__ // DB access enabled
  dtdbobj=NULL;
#endif
}


#ifdef __USE_CCBDB__ // DB access enabled
Cconf::Cconf(char *this_name,cmsdtdb *thisdtdb) {

  _verbose_ = VERBOSE_DEFAULT; // Set verbose level to default

  ConfigurationType = UDATABASE;
  fname = "";
  cname = this_name;
  reset_confmask();

// Set the DB connector
  dtdbobj = thisdtdb;
}
#endif

// Go to verbose mode (in=true) or not
void Cconf::verbose_mode(bool in) {
  if (this==NULL) return; // Object deleted -> return

  _verbose_ = in;
}

// Get config_name or config_file_name
// Author: A.Parenti, Apr06 2006
// Modified: AP, May03 2006
char* Cconf::get_conf_name() {
  if (this==NULL) return ""; // Object deleted -> return

  if (ConfigurationType==UFILE)
    return (char*)fname.c_str();
  else if (ConfigurationType==UDATABASE)
    return (char*)cname.c_str();
  else
    return "";
}

// Look for a command in the configuration
// Author: A.Parenti, Feb17 2005
// Modified/Tested: AP, Oct28 2007
int Cconf::read_conf(short ccbid,unsigned char cmd_code, char brd, char chip, unsigned char cmd_line[][CMD_LINE_DIM], int line_len[]) {
  char tmpstr[5*CMD_LINE_DIM+1];
  unsigned char tmpcmd[CMD_LINE_DIM];
  int tmplen;
  int i, j, lines=0; // number of command lines
  int offset; // 0 if there's not HEADER, header length if there is
  std::string sss="";

  if (this==NULL) return 0; // Object deleted -> return

  if (ConfigurationType == UFILE) {

// Open the file
    std::ifstream conffile(fname.c_str());

    if (conffile.is_open()) {
      while (!conffile.eof()) {
        sprintf(tmpstr,"%c",conffile.get()); // Read a byte from file
        sss+=tmpstr;
      }

      conffile.close(); // Close file

      while ((i=sss.find("\r\n",0))!=std::string::npos)
        sss.erase(i,1); // Replace "\r\n" (Windows newline) with "\n"

      while ((i=sss.find("\r"))!=std::string::npos)
        sss.replace(i,1,"\n"); // Replace "\r" (MAC newline) with "\n"

      for (lines=j=i=0; i<sss.length() && lines<CMD_MAXLINES; ++i) {
        if (sss[i]=='\n') { // Newline character
          tmpstr[j] = '\0'; // Add string terminator
          string_to_array(tmpstr,16,tmpcmd,&tmplen); // Convert text to numbers

          offset = 0; // Configuration files don't have header within

          if ((tmplen>0) && // Non-empty line
              (cmd_code<=0 || tmpcmd[offset]==cmd_code) && // Desired command_code (-1 for all)
              (brd<0 || tmpcmd[offset+1]==brd) && // Desired board (-1 for all)
              (chip<0 || tmpcmd[offset+2]==chip)) { // Desired chip (-1 for all)
            line_len[lines] = tmplen; // Store line length
            mystrcpy(cmd_line[lines],tmpcmd,tmplen); // Store command line
            ++lines; // A new command line has been found
            if (lines>=CMD_MAXLINES)
              printf("Cconf::read_conf -> maximun number of lines read (ignoring following)\n");
          }
          strcpy(tmpstr,""); // Blank tmpstr
          j = 0; // Reset j
        }
        else
          tmpstr[j++]=sss[i];
      }
      return lines; // Number of read lines
    }
    else // File is not open
      return -1;
  }
  else if (ConfigurationType == UDATABASE) {
#ifdef __USE_CCBDB__ // DB access enabled
    char tmp_cmd_lines[CMD_MAXLINES][CMD_LINE_DIM];
    int tmp_lines;

    ccbconfdb *myConf;
    if (dtdbobj!=NULL)
      myConf = new ccbconfdb(dtdbobj); // Use an existing connection
    else 
      myConf = new ccbconfdb(); // Create a new connection to DB

     myConf->confselect((char *)cname.c_str()); // Select configuration from DB

    lines=0;
    for (int ii=0;ii<3;ii++)
    {
      int bricktype;
      tmp_lines=0;
     switch (ii) {

      case 0:
        //retrieve pre conf commands
        bricktype=20;
        myConf->retrievecmds(ccbid,bricktype, &tmp_lines,(char (*)[CMD_LINE_DIM])tmp_cmd_lines,CMD_MAXLINES);
        printf("Cconf: retrieved %d preconf lines\n",tmp_lines);
        break;
      case 1:
        myConf->retrievecmds(ccbid,&tmp_lines, (char (*)[CMD_LINE_DIM])tmp_cmd_lines,CMD_MAXLINES);
        printf("Cconf: retrieved %d conf lines\n",tmp_lines);
        break;
      case 2:
        //retrieve post conf commands
        bricktype=21;
        myConf->retrievecmds(ccbid,bricktype, &tmp_lines,(char (*)[CMD_LINE_DIM])tmp_cmd_lines,CMD_MAXLINES);
        printf("Cconf: retrieved %d postconf lines\n",tmp_lines);
        break;
        }

    offset = CCB_HDR_LEN; // Commands in DB have CCB-style header

//      printf("Cconf: adding %d %d-conf lines\n",tmp_lines,ii);
//    for (lines=i=0; i<tmp_lines; ++i) {
      for (i=0; i<tmp_lines && lines<CMD_MAXLINES; ++i) {

// Add white spaces in between exadecimal numbers, strip away the leading "0x"
       strcpy(tmpstr,"");
       for (j=0;(2*j+2)<strlen(tmp_cmd_lines[i]);++j) {
        tmpstr[3*j]   = tmp_cmd_lines[i][2*j+2];
        tmpstr[3*j+1] = tmp_cmd_lines[i][2*j+3];
        tmpstr[3*j+2] = ' ';
        } 
        tmpstr[3*j-1]='\0'; // Add string terminator

        string_to_array(tmpstr,16,tmpcmd,&tmplen); // Convert text to numbers
        if (tmplen<=CCB_HDR_LEN) continue; // Empty line -> skip to next

//      printf("*  %s\n",tmp_cmd_lines[i]);
//      printf("** %s\n",tmpstr);
//      strprint(tmpcmd+offset,tmplen);

        if ((cmd_code<=0 || tmpcmd[offset]==cmd_code) && // Desired command_code (-1 for all)
          (brd<0 || tmpcmd[offset+1]==brd) && // Desired board (-1 for all)
          (chip<0 || tmpcmd[offset+2]==chip)) { // Desired chip (-1 for all)
           line_len[lines] = tmplen - CCB_HDR_LEN;
           mystrcpy(cmd_line[lines],tmpcmd+CCB_HDR_LEN,line_len[lines]); // Store command line (strip away header)
           ++lines; // A new command line has been found
            if (lines>=CMD_MAXLINES)
              printf("Cconf::read_conf -> maximun number of lines read (ignoring following)\n");
           }
        }
    }
    delete myConf;
    return lines;
#else
    nodbmsg();
    return -1;
#endif
  }
  return -1;
}


// Write the current configuration to a file
// Author: A. Parenti, Jun23 2005
// Modified: AP, Feb28 2007 (Tested)
int Cconf::write_conf_to_file(short ccbid,char *filename) {
  unsigned char cmd_line[CMD_MAXLINES][CMD_LINE_DIM];
  char tmpstr[5*CMD_LINE_DIM+1];
  int i, lines, line_len[CMD_MAXLINES];

  if (this==NULL) return 0; // Object deleted -> return

// Open the file
  std::ofstream conffile(filename);

    if (conffile.is_open()) {
      lines = read_conf(ccbid,(unsigned char)0,(char)-1,(char)-1,cmd_line,line_len); // Read all command lines
      if (_verbose_)
        printf("write_conf_to_file: %d lines written.\n",lines);
      for (i=0;i<lines;++i) {
        array_to_string(cmd_line[i],line_len[i],tmpstr); // Convert cmd line to text
        conffile.write(tmpstr,strlen(tmpstr)); // Write string to file
        conffile.put('\n'); // Add a newline character
      }
      return lines;
    }
    else
      return 0;
}


// Reset components to be masked in configuration
// Author: A.Parenti, Oct26 2007
void Cconf::reset_confmask() {
  int i, j;

  for (i=0;i<8;++i)
    for (j=0;j<32;++j)
      BTImask[i][j]=false; // Unmask all BTIs

  for (i=0;i<6;++i)
    for (j=0;j<4;++j)
      TRACOmask[i][j]=false; // Unmask all TRACOs

  for (i=0;i<6;++i)
    TSSmask[i]=false; // Unmask all TSSs

  TSMmask=false; // Unmask TSM

  for (i=0;i<7;++i)
    for (j=0;j<4;++j)
      TDCmask[i][j]=false; // Unmask all TDCs

  for (i=0;i<3;++i)
    for (j=0;j<24;++j)
      FEmask[i][j]=false; // Unmask all FEs
}


// Read the list of components to be masked in configuration
// Read mask from file
// Author: A.Parenti, Oct28 2007
int Cconf::read_confmask(char *filename) {
  std::string fname, strin="";

  fname=filename;
  strin=read_file(fname); // Read file content

  return read_confmask(strin);
}


// Read the list of components to be masked in configuration
// Read mask from DB
// Author: A.Parenti, Oct28 2007
int Cconf::read_confmask(short ccbID) {
  char confmask[2048];
  std::string strin="";

#ifdef __USE_CCBDB__ // DB access enabled
  ccbconfdb *myConf;

  if (dtdbobj!=NULL)
    myConf = new ccbconfdb(dtdbobj); // Use an existing connection
  else 
    myConf = new ccbconfdb(); // Create a new connection to DB

  myConf->retrieve_confmask(ccbID,confmask);
  strin=confmask;

  delete myConf;
#else
    nodbmsg();
#endif

  return read_confmask(strin);
}


// Read the list of components to be masked in configuration
// Read mask from a string
// Author: A.Parenti, Oct26 2007
int Cconf::read_confmask(std::string strin) {
  int i, j;

  xmlDocPtr doc=NULL; // Document pointer
  xmlNodePtr cur=NULL; // Node pointer
  xmlChar *attr1, *attr2; // Element attributes

  reset_confmask();


  if (strin.length()<=0) return -1;

  if (xml_get_root(strin).find(MASK_XML_ROOT,0)==std::string::npos)
    return -2; // Root tag not found: return

  LIBXML_TEST_VERSION; // Check libxml version

// Declare document pointer
  doc = xmlParseMemory(strin.c_str(),strin.length());
  if (doc == NULL)
    return -3;

// Declare node pointer, point to root element
  cur = xmlDocGetRootElement(doc);
  if (cur == NULL) {
    xmlFreeDoc(doc); // Free pointer
    return -3;
  }

  cur = cur->xmlChildrenNode; // Point to first child of root
  while (cur!=NULL) {
    i=j=-1;
    if ((!xmlStrcmp(cur->name, (const xmlChar *)"bti"))){
      attr1= xmlGetProp(cur, (const xmlChar *)"brd");
      attr2= xmlGetProp(cur, (const xmlChar *)"chip");
      if (attr1!=NULL) i=strtol((const char*)attr1,NULL,0);
      if (attr2!=NULL) j=strtol((const char*)attr2,NULL,0);
      if (i>=0 && i<8 && j>=0 && j<32)
        BTImask[i][j]=true;
    } else if ((!xmlStrcmp(cur->name, (const xmlChar *)"traco"))){
      attr1= xmlGetProp(cur, (const xmlChar *)"brd");
      attr2= xmlGetProp(cur, (const xmlChar *)"chip");
      if (attr1!=NULL) i=strtol((const char*)attr1,NULL,0);
      if (attr2!=NULL) j=strtol((const char*)attr2,NULL,0);
      if (i>=0 && i<6 && j>=0 && j<4)
        TRACOmask[i][j]=true;
    } else if ((!xmlStrcmp(cur->name, (const xmlChar *)"tss"))){
      attr1= xmlGetProp(cur, (const xmlChar *)"brd");
      if (attr1!=NULL) i=strtol((const char*)attr1,NULL,0);
      if (i>=0 && i<6)
        TSSmask[i]=true;
    } else if ((!xmlStrcmp(cur->name, (const xmlChar *)"tsm"))){
      TSMmask=true;
    } else if ((!xmlStrcmp(cur->name, (const xmlChar *)"tdc"))){
      attr1= xmlGetProp(cur, (const xmlChar *)"brd");
      attr2= xmlGetProp(cur, (const xmlChar *)"chip");
      if (attr1!=NULL) i=strtol((const char*)attr1,NULL,0);
      if (attr2!=NULL) j=strtol((const char*)attr2,NULL,0);
      j = strtol((const char*)attr2,NULL,0);
      if (i>=0 && i<7 && j>=0 && j<4)
        TDCmask[i][j]=true;
    } else if ((!xmlStrcmp(cur->name, (const xmlChar *)"fe"))){
      attr1= xmlGetProp(cur, (const xmlChar *)"SL");
      attr2= xmlGetProp(cur, (const xmlChar *)"chip");
      if (attr1!=NULL) i=strtol((const char*)attr1,NULL,0);
      if (attr2!=NULL) j=strtol((const char*)attr2,NULL,0);
      if (i>=0 && i<3 && j>=0 && j<24)
        FEmask[i][j]=true;
    }

    cur = cur->next; // Next element
  }

  xmlFreeDoc(doc); // Free pointer. All memory can be sucked by xml2 otherwise.
  return 0;
}


// Print components to be masked in configuration
// Author: A.Parenti, Oct26 2007
void Cconf::print_confmask() {
  int i, j;

  for (i=0;i<8;++i)
    for (j=0;j<32;++j)
      if (BTImask[i][j]) printf("BTI brd=%d chip=%d masked\n",i,j);
      

  for (i=0;i<6;++i)
    for (j=0;j<4;++j)
      if(TRACOmask[i][j]) printf("TRACO brd=%d chip=%d masked\n",i,j);

  for (i=0;i<6;++i)
    if (TSSmask[i]) printf("TSS brd=%d masked\n",i);

  if (TSMmask) printf("TSM masked\n");

  for (i=0;i<7;++i)
    for (j=0;j<4;++j)
      if (TDCmask[i][j]) printf("TDC brd=%d chip=%d masked\n",i,j);

  for (i=0;i<3;++i)
    for (j=0;j<24;++j)
      if (FEmask[i][j]) printf("FE SL=%d brd=%d masked\n",i,j);
}


// Associate RunNr to configuration (ie set configsets.Run)
// Author: A.Parenti, Jan18 2007
// Modified/Tested: AP, Oct28 2007
int Cconf::set_run(int runnr) {
  if (this==NULL) return -1; // Object deleted -> return

  if (ConfigurationType==UDATABASE && cname.length()>0 && runnr>0) {
#ifdef __USE_CCBDB__ // DB access enabled
    int resu;
    ccbconfdb *myConf;

    if (dtdbobj!=NULL)
      myConf = new ccbconfdb(dtdbobj); // Use an existing connection
    else 
      myConf = new ccbconfdb(); // Create a new connection to DB

    myConf->confselect((char *)cname.c_str());
    resu = myConf->updaterun(runnr);

    delete myConf;
    return resu;
#else
    nodbmsg();
    return -1;
#endif
  }
  return -1;
}
