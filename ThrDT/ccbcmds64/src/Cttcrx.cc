#include <ccbcmds/Cttcrx.h>
#include <ccbcmds/apfunctions.h>

#include <iostream>
#include <cstdio>

/*****************************************************************************/
// TTCrx class
/*****************************************************************************/

// Class constructor; arguments: pointer_to_command_object
// Author: A.Parenti, Dec01 2006
// Modified: AP, Dec11 2006
Cttcrx::Cttcrx(Ccommand* ppp) : Ccmn_cmd(ppp) {
// High level commands
  get_TTCRX_id_reset(); // Reset ttcrx_id struct
  get_TTCRX_fine_delay_reset(); // Reset get_ttcrx_fine_delay struct
  get_TTCRX_coarse_delay_reset(); // Reset get_ttcrx_coarse_delay struct
  set_TTCRX_coarse_delay_reset(); // Reset set_ttcrx_coarse_delay struct
  get_TTCRX_control_register_reset(); // Reset get_ttcrx_control_reg struct
  set_TTCRX_control_register_reset(); // Reset set_ttcrx_control_reg struct
  reset_TTCRX_control_register_reset(); // Reset reset_ttcrx_control_reg struct
  get_TTCRX_error_counter_reset(); // Reset get_err_count struct
  reset_TTCRX_error_counter_reset(); // Reset reset_err_count struct
  get_TTCRX_status_register_reset(); // Reset get_ttcrx_status_reg struct
  reset_TTCRX_status_register_reset(); // Reset reset_ttcrx_status_reg struct
  get_TTCRX_bunch_counter_reset(); // Reset get_b_counter struct
  reset_TTCRX_bunch_counter_reset(); // Reset reset_b_counter struct
  get_TTCRX_event_counter_reset(); // Reset get_e_counter struct
  reset_TTCRX_event_counter_reset(); // Reset reset_e_counter struct
  get_TTCRX_config_reset(); // Reset get_ttcrx_config struct
  set_TTCRX_config_reset(); // Reset set_ttcrx_config struct
  reset_TTCRX_config_reset(); // Reset reset_ttcrx_config struct

// Low level commands
  read_TTCRX_reset(); // Reset read_ttcrx struct
  write_TTCRX_reset(); // Reset write_ttcrx struct
  TTC_skew_reset(); // Reset ttc_skew struct
  emul_TTC_reset(); // Reset emul_ttc struct
  sel_TTC_ck_reset(); // Reset sel_ttc_ck struct
  reset_lose_lock_counter_reset(); // Reset reset_llc struct
  set_TTC_fine_delay_reset(); // Reset ttc_fine_delay struct
  write_TTC_boot_reset(); // Reset write_ttc_boot struct
  read_TTC_boot_reset(); // Reset read_ttc_boot struct
  find_TTC_boot_reset(); // Reset find_ttc_boot struct
}


/************************/
/* Override TDC methods */
/************************/

void Cttcrx::read_TDC(char tdc_brd, char tdc_chip) {}
int Cttcrx::read_TDC_decode(unsigned char *rdstr) {return 0;}
void Cttcrx::read_TDC_print() {}

void Cttcrx::status_TDC(char tdc_brd, char tdc_chip) {}
void Cttcrx::status_TDC_print() {}

/********************/
/* Get the TTCrx ID */
/********************/
// Author: A.Parenti, Feb23 2005
// Modified: AP, Dec11 2006
void Cttcrx::get_TTCRX_id() {
  unsigned char addr[3]={16,17,18}, tmp[3], err[3];
  int i;

  get_TTCRX_id_reset();

  for (i=0;i<3;++i) {
    read_TTCRX(addr[i]); // read TTCrx register
    err[i] = read_ttcrx.ccbserver_code; // read error if !=0
    tmp[i] = read_ttcrx.data; // copy data
  }

  if (err[0]==0 && err[1]==0 && err[2]==0) {
    ttcrx_id.id     = tmp[0]+((tmp[1]&0x3F)<<8);
    ttcrx_id.mma    = (tmp[1]>>6)&0x3;
    ttcrx_id.mmb    = (tmp[2]>>6)&0x3;
    ttcrx_id.id_i2c = tmp[2]&0x3F;
    ttcrx_id.read_err = 0; // no errors
  }
  else {
    ttcrx_id.read_err = 1; // ERROR!
  }

// Fill-up message
  char tmpstr[100];

  ttcrx_id.msg="\nGet the TTCrx ID\n";
  sprintf(tmpstr,"ID: %#4X\n", ttcrx_id.id); ttcrx_id.msg+=tmpstr;
  sprintf(tmpstr,"Master Mode A: %#2X\n",ttcrx_id.mma); ttcrx_id.msg+=tmpstr;
  sprintf(tmpstr,"Master Mode B: %#2X\n",ttcrx_id.mmb); ttcrx_id.msg+=tmpstr;
  sprintf(tmpstr,"ID_I2C: %#2X\n",ttcrx_id.id_i2c); ttcrx_id.msg+=tmpstr;

// Print the error message
  if (ttcrx_id.read_err==0)
    ttcrx_id.msg+="Error message: No error\n";
  else
    ttcrx_id.msg+="Error message: Read Error\n";
}

// Reset ttcrx_id struct
// Author: A.Parenti, Dec11 2006
void Cttcrx::get_TTCRX_id_reset() {
  ttcrx_id.id=0;
  ttcrx_id.mma=0;
  ttcrx_id.mmb=0;
  ttcrx_id.id_i2c=0;
  ttcrx_id.read_err=0;
  ttcrx_id.msg="";
}


// Print result of "Get the TTCrx ID" to stdout
// Author: A. Parenti, Feb23 2005
// Modified: AP, Dec11, 2006
void Cttcrx::get_TTCRX_id_print() {
  printf("%s",ttcrx_id.msg.c_str());
}


/************************/
/* Get TTCrx fine delay */
/************************/
// Author: A.Parenti, Feb26 2005
// Modified: AP, Dec11 2006
void Cttcrx::get_TTCRX_fine_delay(char ch) {
  unsigned char addr;

  if (ch==1)
    addr=1;
  else
    addr=0;

  get_TTCRX_fine_delay_reset(); // Reset get_ttcrx_fine_delay struct
  read_TTCRX(addr); // read TTCrx register, address 0 or 1

// copy data
  get_ttcrx_fine_delay.code   = read_ttcrx.code;
  get_ttcrx_fine_delay.data   = read_ttcrx.data;
  get_ttcrx_fine_delay.result = read_ttcrx.result;
  get_ttcrx_fine_delay.ccbserver_code = read_ttcrx.ccbserver_code;

// Fill-up message
  char tmpstr[100];
  int n, m;
  float dT=0.10417, dly;

  get_ttcrx_fine_delay.msg="\nGet TTCrx fine delay\n";

  if (get_ttcrx_fine_delay.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(get_ttcrx_fine_delay.ccbserver_code)); get_ttcrx_fine_delay.msg+=tmpstr;
  } else {
    n = (get_ttcrx_fine_delay.data>>4)&0xF;
    m = get_ttcrx_fine_delay.data&0xF;
    dly = dT*(float)((15*m+16*n+30)%240); // delay in ns

    sprintf(tmpstr,"code: %#2X\n", get_ttcrx_fine_delay.code); get_ttcrx_fine_delay.msg+=tmpstr;
//    sprintf(tmpstr,"data: %#2X\n", get_ttcrx_fine_delay.data); get_ttcrx_fine_delay.msg+=tmpstr;
    sprintf(tmpstr,"delay (ns): %.2f\n", dly); get_ttcrx_fine_delay.msg+=tmpstr;
    sprintf(tmpstr,"Result: %s\n", TTCRX_result_to_str(get_ttcrx_fine_delay.result)); get_ttcrx_fine_delay.msg+=tmpstr;
  }
}

// Reset get_ttcrx_fine_delay struct
// Author: A.Parenti, Dec11 2006
void Cttcrx::get_TTCRX_fine_delay_reset() {
  get_ttcrx_fine_delay.code=0;
  get_ttcrx_fine_delay.data=0;
  get_ttcrx_fine_delay.result=0;
  get_ttcrx_fine_delay.ccbserver_code=-5;
  get_ttcrx_fine_delay.msg="";
}

// Print result of "Get TTCrx fine delay" to stdout
// Author: A. Parenti, Feb26 2005
// Modified: AP, Dec11 2006
void Cttcrx::get_TTCRX_fine_delay_print() {
  printf("%s",get_ttcrx_fine_delay.msg.c_str());
}


/**************************/
/* Get TTCrx coarse delay */
/**************************/
// Author: A.Parenti, Feb26 2005
// Modified: AP, Dec11 2006
void Cttcrx::get_TTCRX_coarse_delay() {
  unsigned char addr=2;

  get_TTCRX_coarse_delay_reset(); // Reset get_ttcrx_coarse_delay struct
  read_TTCRX(addr); // read TTCrx register, address 2

// copy data
  get_ttcrx_coarse_delay.code   = read_ttcrx.code;
  get_ttcrx_coarse_delay.data   = read_ttcrx.data;
  get_ttcrx_coarse_delay.result = read_ttcrx.result;
  get_ttcrx_coarse_delay.ccbserver_code = read_ttcrx.ccbserver_code;

// Fill-up message
  char tmpstr[100];
  int n, m;
  float dly1, dly2;

  get_ttcrx_coarse_delay.msg="\nGet TTCrx coarse delay\n";

  if (get_ttcrx_coarse_delay.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(get_ttcrx_coarse_delay.ccbserver_code)); get_ttcrx_coarse_delay.msg+=tmpstr;
  } else {
    n=get_ttcrx_coarse_delay.data&0xF;
    m=(get_ttcrx_coarse_delay.data>>4)&0xF;

    dly1=(float)n*25.0; // delay 1 (ns)
    dly2=(float)m*25.0; // delay 2 (ns)

    sprintf(tmpstr,"code: %#2X\n", get_ttcrx_coarse_delay.code); get_ttcrx_coarse_delay.msg+=tmpstr;
//    sprintf(tmpstr,"data: %#2X\n", get_ttcrx_coarse_delay.data); get_ttcrx_coarse_delay.msg+=tmpstr;
    sprintf(tmpstr,"delay 1 (ns): %.2f\n", dly1); get_ttcrx_coarse_delay.msg+=tmpstr;
    sprintf(tmpstr,"delay 2 (ns): %.2f\n", dly2); get_ttcrx_coarse_delay.msg+=tmpstr;
    sprintf(tmpstr,"Result: %s\n", TTCRX_result_to_str(get_ttcrx_coarse_delay.result)); get_ttcrx_coarse_delay.msg+=tmpstr;
  }
}


// Reset get_ttcrx_coarse_delay struct
// Author: A.Parenti, Dec11 2006
void Cttcrx::get_TTCRX_coarse_delay_reset() {
  get_ttcrx_coarse_delay.code=0;
  get_ttcrx_coarse_delay.data=0;
  get_ttcrx_coarse_delay.result=0;
  get_ttcrx_coarse_delay.ccbserver_code=-5;
  get_ttcrx_coarse_delay.msg="";
}

// Print result of "Get TTCrx coarse delay" to stdout
// Author: A. Parenti, Feb26 2005
// Modified: AP, Dec11 2006
void Cttcrx::get_TTCRX_coarse_delay_print() {
  printf("%s",get_ttcrx_coarse_delay.msg.c_str());
}


/**************************/
/* Set TTCrx coarse delay */
/**************************/
// Author: A.Parenti, Feb26 2005
// Modified: AP, Dec12 2006
void Cttcrx::set_TTCRX_coarse_delay(float delay1, float delay2) {
  unsigned char addr=2, this_data;

  this_data = ((unsigned char)(delay1/25+0.5)&0xF)
    + (((unsigned char)(delay2/25+0.5)&0xF)<<4);

  set_TTCRX_coarse_delay_reset(); // Reset set_ttcrx_coarse_delay struct
  write_TTCRX(addr,this_data); // read TTCrx register, address 2

// copy data
  set_ttcrx_coarse_delay.code1  = write_ttcrx.code1;
  set_ttcrx_coarse_delay.code2  = write_ttcrx.code2;
  set_ttcrx_coarse_delay.result = write_ttcrx.result;
  set_ttcrx_coarse_delay.ccbserver_code = write_ttcrx.ccbserver_code;

// Fill-up message
  char tmpstr[100];

  set_ttcrx_coarse_delay.msg="\nSet TTCrx coarse delay\n";

  if (set_ttcrx_coarse_delay.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(set_ttcrx_coarse_delay.ccbserver_code)); set_ttcrx_coarse_delay.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n", set_ttcrx_coarse_delay.code1); set_ttcrx_coarse_delay.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n", set_ttcrx_coarse_delay.code2); set_ttcrx_coarse_delay.msg+=tmpstr;
    sprintf(tmpstr,"Result: %s\n", TTCRX_result_to_str(set_ttcrx_coarse_delay.result)); set_ttcrx_coarse_delay.msg+=tmpstr;
  }
}

// Reset set_ttcrx_coarse_delay struct
// Author: A.Parenti, Dec12 2006
void Cttcrx::set_TTCRX_coarse_delay_reset() {
  set_ttcrx_coarse_delay.code1=0;
  set_ttcrx_coarse_delay.code2=0;
  set_ttcrx_coarse_delay.result=0;
  set_ttcrx_coarse_delay.ccbserver_code=-5;
  set_ttcrx_coarse_delay.msg="";
}

// Print result of "Set TTCrx coarse delay" to stdout
// Author: A. Parenti, Feb26 2005
// Modified: AP, Dec12 2006
void Cttcrx::set_TTCRX_coarse_delay_print() {
  printf("%s",set_ttcrx_coarse_delay.msg.c_str());
}


/******************************/
/* Get TTCrx Control Register */
/******************************/
// Author: A.Parenti, Feb28 2005
// Modified: AP, Dec12 2006
void Cttcrx::get_TTCRX_control_register() {
  unsigned char addr=3;

  get_TTCRX_control_register_reset(); // Reset get_ttcrx_control_reg struct
  read_TTCRX(addr); // read TTCrx register, address 3

// copy data
  get_ttcrx_control_reg.code   = read_ttcrx.code;
  get_ttcrx_control_reg.data   = read_ttcrx.data;
  get_ttcrx_control_reg.result = read_ttcrx.result;
  get_ttcrx_control_reg.ccbserver_code = read_ttcrx.ccbserver_code;

// Fill-up message
  char tmpstr[100];

  get_ttcrx_control_reg.msg="\nGet TTCrx Control Register\n";

  if (get_ttcrx_control_reg.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(get_ttcrx_control_reg.ccbserver_code)); get_ttcrx_control_reg.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code: %#2X\n", get_ttcrx_control_reg.code); get_ttcrx_control_reg.msg+=tmpstr;
//    sprintf(tmpstr,"control register: %#2X\n", get_ttcrx_control_reg.data); get_ttcrx_control_reg.msg+=tmpstr;
    sprintf(tmpstr,"Enable Bunch Counter operation: %d\n",takebit(get_ttcrx_control_reg.data,0)); get_ttcrx_control_reg.msg+=tmpstr;
    sprintf(tmpstr,"Enable Event Counter operation: %d\n",takebit(get_ttcrx_control_reg.data,1)); get_ttcrx_control_reg.msg+=tmpstr;
    sprintf(tmpstr,"SelClock40Des2: %d\n",takebit(get_ttcrx_control_reg.data,2)); get_ttcrx_control_reg.msg+=tmpstr;
    sprintf(tmpstr,"Enable Clock40Des2 output: %d\n",takebit(get_ttcrx_control_reg.data,3)); get_ttcrx_control_reg.msg+=tmpstr;
    sprintf(tmpstr,"Enable ClockL1Accept output: %d\n",takebit(get_ttcrx_control_reg.data,4)); get_ttcrx_control_reg.msg+=tmpstr;
    sprintf(tmpstr,"Enable Parallel output bus: %d\n",takebit(get_ttcrx_control_reg.data,5)); get_ttcrx_control_reg.msg+=tmpstr;
    sprintf(tmpstr,"Enable Serial B output: %d\n",takebit(get_ttcrx_control_reg.data,6)); get_ttcrx_control_reg.msg+=tmpstr;
    sprintf(tmpstr,"Enable (non-deskewed) Clock40 output: %d\n",takebit(get_ttcrx_control_reg.data,7)); get_ttcrx_control_reg.msg+=tmpstr;
    sprintf(tmpstr,"Result: %s\n", TTCRX_result_to_str(get_ttcrx_control_reg.result)); get_ttcrx_control_reg.msg+=tmpstr;
  }
}

// Reset get_ttcrx_control_reg struct
// Author: A.Parenti, Dec12 2006
void Cttcrx::get_TTCRX_control_register_reset() {
  get_ttcrx_control_reg.code=0;
  get_ttcrx_control_reg.data=0;
  get_ttcrx_control_reg.result=0;
  get_ttcrx_control_reg.ccbserver_code=-5;
  get_ttcrx_control_reg.msg="";
}

// Print result of "Get Control Register TTCrx " to stdout
// Author: A. Parenti, Feb28 2005
// Modified: AP, Dec12 2006
void Cttcrx::get_TTCRX_control_register_print() {
  printf("%s",get_ttcrx_control_reg.msg.c_str());
}


/******************************/
/* Set TTCrx Control Register */
/******************************/
// Author: A.Parenti, Jun17 2005
// Modified: AP, Dec12 2006
void Cttcrx::set_TTCRX_control_register(unsigned char data) {
  unsigned char addr=3;

  set_TTCRX_control_register_reset(); // Reset set_ttcrx_control_reg struct
  write_TTCRX(addr,data); // write TTCrx register, address 3

// copy data
  set_ttcrx_control_reg.code1  = write_ttcrx.code1;
  set_ttcrx_control_reg.code2  = write_ttcrx.code2;
  set_ttcrx_control_reg.result = write_ttcrx.result;
  set_ttcrx_control_reg.ccbserver_code = write_ttcrx.ccbserver_code;

// Fill-up message
  char tmpstr[100];

  set_ttcrx_control_reg.msg="Set TTCrx Control Register\n";

  if (set_ttcrx_control_reg.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(set_ttcrx_control_reg.ccbserver_code)); set_ttcrx_control_reg.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n", set_ttcrx_control_reg.code1); set_ttcrx_control_reg.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n", set_ttcrx_control_reg.code2); set_ttcrx_control_reg.msg+=tmpstr;
    sprintf(tmpstr,"Result: %s\n", TTCRX_result_to_str(set_ttcrx_control_reg.result)); set_ttcrx_control_reg.msg+=tmpstr;
  }
}

// Reset set_ttcrx_control_reg struct
// Author: A.Parenti, Dec12 2006
void Cttcrx::set_TTCRX_control_register_reset() {
  set_ttcrx_control_reg.code1=0;
  set_ttcrx_control_reg.code2=0;
  set_ttcrx_control_reg.result=0;
  set_ttcrx_control_reg.ccbserver_code=-5;
  set_ttcrx_control_reg.msg="";
}

// Print result of "Set Control Register TTCrx " to stdout
// Author: A. Parenti, Jun17 2005
// Modified: AP, Dec12 2006
void Cttcrx::set_TTCRX_control_register_print() {
  printf("%s",set_ttcrx_control_reg.msg.c_str());
}


/********************************/
/* Reset TTCrx Control Register */
/********************************/
// Author: A.Parenti, Jun17 2005
// Modified: AP, Dec12 2006
void Cttcrx::reset_TTCRX_control_register() {
  unsigned char addr=3, data=0x93;

  reset_TTCRX_control_register_reset(); // Reset reset_ttcrx_control_reg struct
  write_TTCRX(addr,data); // write TTCrx register, address 3

// copy data
  reset_ttcrx_control_reg.code1  = write_ttcrx.code1;
  reset_ttcrx_control_reg.code2  = write_ttcrx.code2;
  reset_ttcrx_control_reg.result = write_ttcrx.result;
  reset_ttcrx_control_reg.ccbserver_code = write_ttcrx.ccbserver_code;

// Fill-up message
  char tmpstr[100];

  reset_ttcrx_control_reg.msg="Reset TTCrx Control Register\n";

  if (reset_ttcrx_control_reg.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(reset_ttcrx_control_reg.ccbserver_code)); reset_ttcrx_control_reg.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",reset_ttcrx_control_reg.code1); reset_ttcrx_control_reg.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",reset_ttcrx_control_reg.code2); reset_ttcrx_control_reg.msg+=tmpstr;
    sprintf(tmpstr,"Result: %s\n",TTCRX_result_to_str(reset_ttcrx_control_reg.result)); reset_ttcrx_control_reg.msg+=tmpstr;
  }
}

// Reset reset_ttcrx_control_reg struct
// Author: A.Parenti, Dec12 2006
void Cttcrx::reset_TTCRX_control_register_reset() {
  reset_ttcrx_control_reg.code1=0;
  reset_ttcrx_control_reg.code2=0;
  reset_ttcrx_control_reg.result=0;
  reset_ttcrx_control_reg.ccbserver_code=-5;
  reset_ttcrx_control_reg.msg="";
}

// Print result of "Reset Control Register TTCrx " to stdout
// Author: A. Parenti, Jun17 2005
// Modified: AP, Dec12 2006
void Cttcrx::reset_TTCRX_control_register_print() {
  printf("%s",reset_ttcrx_control_reg.msg.c_str());
}


/****************************************/
/* Get the TTCrx error counter register */
/****************************************/
// Author: A.Parenti, Feb28 2005
// Modified: AP, Dec12 2006
void Cttcrx::get_TTCRX_error_counter() {
  unsigned char addr[4]={8,9,10,11}, tmp[4], err[4];
  int i;

  get_TTCRX_error_counter_reset(); // Reset get_err_count struct

  for (i=0;i<4;++i) {
    read_TTCRX(addr[i]); // read TTCrx register
    err[i] = read_ttcrx.ccbserver_code; // read error if !=0
    tmp[i] = read_ttcrx.data; // copy data
  }

  if (err[0]==0 && err[1]==0 && err[2]==0 && err[4]==0) {
    get_err_count.single_err_count = (tmp[1]<<8) + tmp[0];
    get_err_count.double_err_count = tmp[2];
    get_err_count.seu_err_count = tmp[3];
    get_err_count.read_err = 0; // no errors
  } else {
    get_err_count.read_err = 1; // READ ERROR!
  }

// Fill-up message
  char tmpstr[100];

  get_err_count.msg="\nGet the TTCrx error counter register\n";

  sprintf(tmpstr,"TTCrx single_err_count: %d\n", get_err_count.single_err_count); get_err_count.msg+=tmpstr;
  sprintf(tmpstr,"TTCrx double_err_count: %d\n", get_err_count.double_err_count); get_err_count.msg+=tmpstr;
  sprintf(tmpstr,"TTCrx seu_err_count: %d\n", get_err_count.seu_err_count); get_err_count.msg+=tmpstr;

// Print the error message
  if (get_err_count.read_err==0)
    get_err_count.msg+="Error message: No error\n";
  else
    get_err_count.msg+="Error message: Read Error\n";
}


// Reset get_err_count struct
// Author: A.Parenti, Dec12 2006
void Cttcrx::get_TTCRX_error_counter_reset() {
  get_err_count.single_err_count = 0;
  get_err_count.double_err_count = 0;
  get_err_count.seu_err_count = 0;
  get_err_count.read_err = 0;
  get_err_count.msg="";
}

// Print result of "Get the TTCrx ID" to stdout
// Author: A. Parenti, Feb23 2005
// Modified: AP, Dec12 2006
void Cttcrx::get_TTCRX_error_counter_print() {
  printf("%s",get_err_count.msg.c_str());
}


/************************************/
/* Reset the Error Counter Register */
/************************************/
// Author: A.Parenti, Feb26 2005
// Modified: AP, Dec12 2006
void Cttcrx::reset_TTCRX_error_counter() {
  unsigned char addr[4]={8,9,10,11},err[4];
  int i;

  reset_TTCRX_error_counter_reset(); // Reset reset_err_count struct
  for (i=0;i<4;++i) {
    write_TTCRX(addr[i],0); // write TTCrx register
    err[i] = write_ttcrx.ccbserver_code; // write error if !=0
  }

  if (err[0]==0 && err[1]==0 && err[2]==0 && err[3]==0) { // no errors
    reset_err_count.write_err = 0;
  }
  else {
    reset_err_count.write_err = 1; // READ ERROR!
  }

  reset_err_count.msg="\nReset Error Counter Register\n";

// Print the error message
  if (reset_err_count.write_err==0)
    reset_err_count.msg+="Error message: No error\n";
  else
    reset_err_count.msg+="Error message: Reset Error\n";
}


// Reset reset_err_count struct
// Author: A.Parenti, Dec12 2006
void Cttcrx::reset_TTCRX_error_counter_reset() {
  reset_err_count.write_err = 0;
  reset_err_count.msg="";
}


// Print result of "Reset Error Counter" to stdout
// Author: A. Parenti, Feb26 2005
// Modified: AP, Dec12 2006
void Cttcrx::reset_TTCRX_error_counter_print() {
  printf("%s",reset_err_count.msg.c_str());
}


/******************************/
/* Get TTCrx Status Register */
/******************************/
// Author: A.Parenti, Feb28 2005
// Modified: AP, Dec12 2006
void Cttcrx::get_TTCRX_status_register() {
  unsigned char addr=22;

  get_TTCRX_status_register_reset(); // Reset get_ttcrx_status_reg struct
  read_TTCRX(addr); // read TTCrx register, address 22

// copy data
  get_ttcrx_status_reg.code   = read_ttcrx.code;
  get_ttcrx_status_reg.data   = read_ttcrx.data;
  get_ttcrx_status_reg.result = read_ttcrx.result;
  get_ttcrx_status_reg.ccbserver_code = read_ttcrx.ccbserver_code;

// Fill-up message
  char tmpstr[100];

  get_ttcrx_status_reg.msg="\nGet TTCrx Status Register\n";

  if (get_ttcrx_status_reg.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(get_ttcrx_status_reg.ccbserver_code)); get_ttcrx_status_reg.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code: %#2X\n", get_ttcrx_status_reg.code); get_ttcrx_status_reg.msg+=tmpstr;
//    sprintf(tmpstr,"status register: %#2X\n", ttcrx_status_reg.data); get_ttcrx_status_reg.msg+=tmpstr;
    sprintf(tmpstr,"auto_reset_flag:%d\n",takebit(get_ttcrx_status_reg.data,4)); get_ttcrx_status_reg.msg+=tmpstr;
    sprintf(tmpstr,"frame_synch:%d\n",takebit(get_ttcrx_status_reg.data,5)); get_ttcrx_status_reg.msg+=tmpstr;
    sprintf(tmpstr,"dll_ready:%d\n",takebit(get_ttcrx_status_reg.data,6)); get_ttcrx_status_reg.msg+=tmpstr;
    sprintf(tmpstr,"pll_ready:%d\n",takebit(get_ttcrx_status_reg.data,7)); get_ttcrx_status_reg.msg+=tmpstr;
    sprintf(tmpstr,"Result: %s\n", TTCRX_result_to_str(get_ttcrx_status_reg.result)); get_ttcrx_status_reg.msg+=tmpstr;
  }
}


// Reset get_ttcrx_status_reg struct
// Author: A.Parenti, Dec12 2006
void Cttcrx::get_TTCRX_status_register_reset() {
  get_ttcrx_status_reg.code=0;
  get_ttcrx_status_reg.data=0;
  get_ttcrx_status_reg.result=0;
  get_ttcrx_status_reg.ccbserver_code=-5;
  get_ttcrx_status_reg.msg="";
}


// Print result of "Get Status Register TTCrx " to stdout
// Author: A. Parenti, Feb28 2005
// Modified: AP, Dec12 2006
void Cttcrx::get_TTCRX_status_register_print() {
  printf("%s",get_ttcrx_status_reg.msg.c_str());
}


/*******************************/
/* Reset TTCrx Status Register */
/*******************************/
// Author: A.Parenti, Jul09 2005
// Modified: AP, Dec12 2006
void Cttcrx::reset_TTCRX_status_register(bool rst_status, bool rst_watchdog) {
  unsigned char addr=22, err[2]={0,0};

  reset_TTCRX_status_register_reset(); // Reset reset_ttcrx_status_reg struct
  if (rst_status) { // reset status register
    write_TTCRX(addr,5); // write TTCrx register, address 22
    err[0] = write_ttcrx.ccbserver_code; // read error if !=0
    reset_ttcrx_status_reg.data1 = write_ttcrx.code2;
    reset_ttcrx_status_reg.result1 = write_ttcrx.result;
  }

  if (rst_watchdog) { // reset watchdog-reset flag
    write_TTCRX(addr,0); // write TTCrx register, address 22
    err[1] = write_ttcrx.ccbserver_code; // read error if !=0
    reset_ttcrx_status_reg.data2 = write_ttcrx.code2;
    reset_ttcrx_status_reg.result2 = write_ttcrx.result;
  }

  if (err[0]==0 && err[1]==0)
    reset_ttcrx_status_reg.write_err = 0;
  else
    reset_ttcrx_status_reg.write_err = 1;

// Fill-up message
  char tmpstr[100];

  reset_ttcrx_status_reg.msg="\nReset TTCrx Status Register\n";
  if(rst_status && err[0]==0) {
    sprintf(tmpstr,"Register reset. Data: %#2X\n",reset_ttcrx_status_reg.data1); reset_ttcrx_status_reg.msg+=tmpstr;
    sprintf(tmpstr,"                Result: %s\n",TTCRX_result_to_str(reset_ttcrx_status_reg.result1)); reset_ttcrx_status_reg.msg+=tmpstr;
  }
  if (rst_watchdog && err[1]==0) {
    sprintf(tmpstr,"Watchdog reset. Data: %#2X\n",reset_ttcrx_status_reg.data2); reset_ttcrx_status_reg.msg+=tmpstr;
    sprintf(tmpstr,"                Result: %s\n",TTCRX_result_to_str(reset_ttcrx_status_reg.result2)); reset_ttcrx_status_reg.msg+=tmpstr;
  }

// Print the error message
  if (reset_ttcrx_status_reg.write_err==0)
    reset_ttcrx_status_reg.msg+="Error message: No error\n";
  else
    reset_ttcrx_status_reg.msg+="Error message: Reset Error\n";
}


// Reset reset_ttcrx_status_reg struct
// Author: A.Parenti, Dec12 2006
void Cttcrx::reset_TTCRX_status_register_reset() {
  reset_ttcrx_status_reg.data1=0;
  reset_ttcrx_status_reg.result1=0;
  reset_ttcrx_status_reg.data2=0;
  reset_ttcrx_status_reg.result2=0;
  reset_ttcrx_status_reg.write_err=0;
  reset_ttcrx_status_reg.msg="";
}


// Print result of "Reset Status Register TTCrx " to stdout
// Author: A. Parenti, Jun17 2005
// Modified: AP, Dec12 2006
void Cttcrx::reset_TTCRX_status_register_print() {
  printf("%s",reset_ttcrx_status_reg.msg.c_str());
}



/*********************/
/* Get Bunch Counter */
/*********************/
// Author: A.Parenti, Feb26 2005
// Modified: AP, Dec12 2006
void Cttcrx::get_TTCRX_bunch_counter() {
  unsigned char addr[2]={24,25},tmp[2],err[2];
  int i;

  get_TTCRX_bunch_counter_reset(); // Reset get_b_counter struct
  for (i=0;i<2;++i) {
    read_TTCRX(addr[i]); // read TTCrx register
    err[i] = read_ttcrx.ccbserver_code; // read error if !=0
    tmp[i] = read_ttcrx.data; // copy data
  }

  if (err[0]==0 && err[1]==0) {
    get_b_counter.counter  = tmp[0] + (tmp[1]<<8);
    get_b_counter.read_err = 0; // no errors
  }
  else {
    get_b_counter.counter  = 0;
    get_b_counter.read_err = 1; // READ ERROR!
  }

// Fill-up message
  char tmpstr[100];

  get_b_counter.msg="\nGet Bunch Counter\n";

// Print the error message
  if (get_b_counter.read_err==0) {
    sprintf(tmpstr,"Bunch counter: %#2X\n", get_b_counter.counter); get_b_counter.msg+=tmpstr;
    get_b_counter.msg+="Error message: No error\n";
  } else {
    get_b_counter.msg+="Error message: Read Error\n";
  }
}


// Reset get_b_counter struct
// Author: A.Parenti, Dec12 2006
void Cttcrx::get_TTCRX_bunch_counter_reset() {
  get_b_counter.counter=0;
  get_b_counter.read_err=0;
  get_b_counter.msg="";
}

// Print result of "Get Bunch Counter" to stdout
// Author: A. Parenti, Feb26 2005
// Modified: AP, Dec12 2006
void Cttcrx::get_TTCRX_bunch_counter_print() {
  printf("%s",get_b_counter.msg.c_str());
}


/***********************/
/* Reset Bunch Counter */
/***********************/
// Author: A.Parenti, Feb26 2005
// Modified: AP, Dec12 2006
void Cttcrx::reset_TTCRX_bunch_counter() {
  unsigned char addr[2]={24,25},err[2];
  int i;

  reset_TTCRX_bunch_counter_reset(); // Reset reset_b_counter struct
  for (i=0;i<2;++i) {
    write_TTCRX(addr[i],0); // read TTCrx register
    err[i] = write_ttcrx.ccbserver_code; // write error if !=0
  }

  if (err[0]==0 && err[1]==0) {
    reset_b_counter.write_err = 0; // no errors
  } else {
    reset_b_counter.write_err = 1; // READ ERROR!
  }

// Fill-up message

  reset_b_counter.msg="\nReset Bunch Counter\n";

// Print the error message
  if (reset_b_counter.write_err==0)
    reset_b_counter.msg+="Error message: No error\n";
  else
    reset_b_counter.msg+="Error message: Reset Error\n";
}

// Reset reset_b_counter struct
// Author: A.Parenti, Dec12 2006
void Cttcrx::reset_TTCRX_bunch_counter_reset() {
  reset_b_counter.write_err=0;
  reset_b_counter.msg="";
}

// Print result of "Reset Bunch Counter" to stdout
// Author: A. Parenti, Feb26 2005
// Modified: AP, Dec12 2006
void Cttcrx::reset_TTCRX_bunch_counter_print() {
  printf("%s",reset_b_counter.msg.c_str());
}


/*********************/
/* Get Event Counter */
/*********************/
// Author: A.Parenti, Feb26 2005
// Modified: AP, Dec12 2006
void Cttcrx::get_TTCRX_event_counter() {
  unsigned char addr[3]={26,27,28},tmp[3],err[3];
  int i;

  get_TTCRX_event_counter_reset(); // Reset get_e_counter struct
  for (i=0;i<3;++i) {
    read_TTCRX(addr[i]); // read TTCrx register
    err[i] = read_ttcrx.ccbserver_code; // read error if !=0
    tmp[i] = read_ttcrx.data; // copy data
  }

  if (err[0]==0 && err[1]==0 && err[2]==0) {
    get_e_counter.counter  = tmp[0] + (tmp[1]<<8) + (tmp[2]<<16);
    get_e_counter.read_err = 0; // no errors
  } else {
    get_e_counter.counter  = 0;
    get_e_counter.read_err = 1; // READ ERROR!
  }

// Fill-up message
  char tmpstr[100];

  get_e_counter.msg="\nGet Event Counter\n";

// Print the error message
  if (get_e_counter.read_err==0) {
    sprintf(tmpstr,"Event counter: %#2X\n", get_e_counter.counter); get_e_counter.msg+=tmpstr;
    get_e_counter.msg+="Error message: No error\n";
  } else {
    get_e_counter.msg+="Error message: Read Error\n";
  }
}


// Reset get_e_counter struct
// Author: A.Parenti, Dec12 2006
void Cttcrx::get_TTCRX_event_counter_reset() {
  get_e_counter.counter=0;
  get_e_counter.read_err=0;
  get_e_counter.msg="";
}


// Print result of "Get Event Counter" to stdout
// Author: A. Parenti, Feb26 2005
// Modified: AP, Dec12 2006
void Cttcrx::get_TTCRX_event_counter_print() {
  printf("%s",get_e_counter.msg.c_str());
}


/***********************/
/* Reset Event Counter */
/***********************/
// Author: A.Parenti, Feb26 2005
// Modified: AP, Dec12 2006
void Cttcrx::reset_TTCRX_event_counter() {
  unsigned char addr[3]={26,27,28},err[3];
  int i;

  reset_TTCRX_event_counter_reset(); // Reset reset_e_counter struct
  for (i=0;i<3;++i) {
    write_TTCRX(addr[i],0); // read TTCrx register
    err[i] = write_ttcrx.ccbserver_code; // write error if !=0
  }

  if (err[0]==0 && err[1]==0 && err[2]==0) {
    reset_e_counter.write_err = 0; // no errors
  } else {
    reset_e_counter.write_err = 1; // READ ERROR!
  }

// Fill-up message

  reset_e_counter.msg="\nReset Event Counter\n";

// Print the error message
  if (reset_e_counter.write_err==0)
    reset_e_counter.msg+="Error message: No error\n";
  else
    reset_e_counter.msg+="Error message: Reset Error\n";


}


// Reset reset_e_counter struct
// Author: A.Parenti, Dec12 2006
void Cttcrx::reset_TTCRX_event_counter_reset() {
  reset_e_counter.write_err=0;
  reset_e_counter.msg="";
}

// Print result of "Reset Event Counter" to stdout
// Author: A. Parenti, Feb26 2005
// Modified: AP, Dec12 2006
void Cttcrx::reset_TTCRX_event_counter_print() {
  printf("%s",reset_e_counter.msg.c_str());
}


/********************/
/* Get TTCrx Config */
/********************/
// Author: A.Parenti, Jun17 2005
// Modified: AP, Dec12 2006
void Cttcrx::get_TTCRX_config() {
  unsigned char addr[3]={19,20,21},tmp[3],err[3];
  int i;

  get_TTCRX_config_reset(); // Reset get_ttcrx_config struct
  for (i=0;i<3;++i) {
    read_TTCRX(addr[i]); // read TTCrx register
    err[i] = read_ttcrx.ccbserver_code; // read error?if !=0
    tmp[i] = read_ttcrx.data; // copy data
  }

  if (err[0]==0 && err[1]==0 && err[2]==0) {
    get_ttcrx_config.dll_isel=(tmp[0])&0x7; // config 1
    get_ttcrx_config.pll_isel=(tmp[0]>>3)&0x7;
    get_ttcrx_config.dll_sel_aux_1=(tmp[0]>>6)&0x1;
    get_ttcrx_config.dll_sel_aux_2=(tmp[0]>>7)&0x1;
    get_ttcrx_config.mux_select=(tmp[1])&0x7; // config2
    get_ttcrx_config.cf_sel_test_PD=(tmp[1]>>3)&0x1;
    get_ttcrx_config.cf_sel_inputA=(tmp[1]>>4)&0x1;
    get_ttcrx_config.cf_PLL_aux_reset=(tmp[1]>>5)&0x1;
    get_ttcrx_config.cf_DLL_aux_reset=(tmp[1]>>6)&0x1;
    get_ttcrx_config.cf_en_check_machineA=(tmp[1]>>7)&0x1;
    get_ttcrx_config.frequ_check_period=(tmp[2])&0x7;  // config3
    get_ttcrx_config.cf_dis_INITfaster=(tmp[2]>>3)&0x1;
    get_ttcrx_config.cf_dis_watchdog=(tmp[2]>>4)&0x1;
    get_ttcrx_config.cf_en_Hamming=(tmp[2]>>5)&0x1;
    get_ttcrx_config.cf_en_testIO=(tmp[2]>>6)&0x1;
    get_ttcrx_config.cf_en_check_machineB=(tmp[2]>>7)&0x1;
    get_ttcrx_config.read_err = 0; // no errors
  }
  else {
    get_ttcrx_config.read_err = 1; // READ ERROR!
  }

// Fill-up message
  char tmpstr[100];

  get_ttcrx_config.msg="\nGet TTCrx config\n";

  if (get_ttcrx_config.read_err==0) {
    sprintf(tmpstr,"dll_isel: %#2X\n",get_ttcrx_config.dll_isel); get_ttcrx_config.msg+=tmpstr;
    sprintf(tmpstr,"pll_isel: %#2X\n",get_ttcrx_config.pll_isel); get_ttcrx_config.msg+=tmpstr;
    sprintf(tmpstr,"dll_sel_aux_1: %#2X\n",get_ttcrx_config.dll_sel_aux_1); get_ttcrx_config.msg+=tmpstr;
    sprintf(tmpstr,"dll_sel_aux_2: %#2X\n",get_ttcrx_config.dll_sel_aux_2); get_ttcrx_config.msg+=tmpstr;
    sprintf(tmpstr,"mux_select: %#2X\n",get_ttcrx_config.mux_select); get_ttcrx_config.msg+=tmpstr;
    sprintf(tmpstr,"cf_sel_test_PD: %#2X\n",get_ttcrx_config.cf_sel_test_PD); get_ttcrx_config.msg+=tmpstr;
    sprintf(tmpstr,"cf_sel_inputA: %#2X\n",get_ttcrx_config.cf_sel_inputA); get_ttcrx_config.msg+=tmpstr;
    sprintf(tmpstr,"cf_PLL_aux_reset: %#2X\n",get_ttcrx_config.cf_PLL_aux_reset); get_ttcrx_config.msg+=tmpstr;
    sprintf(tmpstr,"cf_DLL_aux_reset: %#2X\n",get_ttcrx_config.cf_DLL_aux_reset); get_ttcrx_config.msg+=tmpstr;
    sprintf(tmpstr,"cf_en_check_machineA: %#2X\n",get_ttcrx_config.cf_en_check_machineA); get_ttcrx_config.msg+=tmpstr;
    sprintf(tmpstr,"frequ_check_period: %#2X\n",get_ttcrx_config.frequ_check_period); get_ttcrx_config.msg+=tmpstr;
    sprintf(tmpstr,"cf_dis_INITfaster: %#2X\n",get_ttcrx_config.cf_dis_INITfaster); get_ttcrx_config.msg+=tmpstr;
    sprintf(tmpstr,"cf_dis_watchdog: %#2X\n",get_ttcrx_config.cf_dis_watchdog); get_ttcrx_config.msg+=tmpstr;
    sprintf(tmpstr,"cf_en_Hamming: %#2X\n",get_ttcrx_config.cf_en_Hamming); get_ttcrx_config.msg+=tmpstr;
    sprintf(tmpstr,"cf_en_testIO: %#2X\n",get_ttcrx_config.cf_en_testIO); get_ttcrx_config.msg+=tmpstr;
    sprintf(tmpstr,"cf_en_check_machineB: %#2X\n",get_ttcrx_config.cf_en_check_machineB); get_ttcrx_config.msg+=tmpstr;
    get_ttcrx_config.msg+="Error message: No error\n";
  } else {
    get_ttcrx_config.msg+="Error message: Read Error\n";
  }
}


// Reset get_ttcrx_config struct
// Author: A.Parenti, Dec12 2006
void Cttcrx::get_TTCRX_config_reset() {
  get_ttcrx_config.dll_isel=0; // config 1
  get_ttcrx_config.pll_isel=0;
  get_ttcrx_config.dll_sel_aux_1=0;
  get_ttcrx_config.dll_sel_aux_2=0;
  get_ttcrx_config.mux_select=0; // config2
  get_ttcrx_config.cf_sel_test_PD=0;
  get_ttcrx_config.cf_sel_inputA=0;
  get_ttcrx_config.cf_PLL_aux_reset=0;
  get_ttcrx_config.cf_DLL_aux_reset=0;
  get_ttcrx_config.cf_en_check_machineA=0;
  get_ttcrx_config.frequ_check_period=0;  // config3
  get_ttcrx_config.cf_dis_INITfaster=0;
  get_ttcrx_config.cf_dis_watchdog=0;
  get_ttcrx_config.cf_en_Hamming=0;
  get_ttcrx_config.cf_en_testIO=0;
  get_ttcrx_config.cf_en_check_machineB=0;
  get_ttcrx_config.read_err=0;
  get_ttcrx_config.msg="";
}

// Print result of "Get TTCrx Config" to stdout
// Author: A. Parenti, Jun17 2005
// Modified: AP, Dec12 2006
void Cttcrx::get_TTCRX_config_print() {
  printf("%s",get_ttcrx_config.msg.c_str());
}


/*****************************/
/* Set TTCrx Config Register */
/*****************************/
// Author: A.Parenti, Jun17 2005
// Modified: AP, Dec12 2006
void Cttcrx::set_TTCRX_config(unsigned char data[3]) {
  unsigned char addr[3]={19,20,21},err[3];
  int i;

  set_TTCRX_config_reset(); // Reset set_ttcrx_config struct
  for (i=0;i<3;++i) {
    write_TTCRX(addr[i],data[i]); // write TTCrx register
    err[i] = write_ttcrx.ccbserver_code; // read error if !=0
  }

 if (err[0]==0 && err[1]==0 && err[2]==0) {
    set_ttcrx_config.write_err = 0; // no errors
  }
  else {
    set_ttcrx_config.write_err = 1; // READ ERROR!
  }

// Fill-up message

 set_ttcrx_config.msg="Set TTCrx Config Register\n";

// Print the error message
  if (set_ttcrx_config.write_err==0)
    set_ttcrx_config.msg+="Error message: No error\n";
  else
    set_ttcrx_config.msg+="Error message: Set Error\n";
}


// Reset set_ttcrx_config struct
// Author: A.Parenti, Dec12 2006
void Cttcrx::set_TTCRX_config_reset() {
  set_ttcrx_config.write_err=0;
  set_ttcrx_config.msg="";
}


// Print result of "Set Control Register TTCrx " to stdout
// Author: A. Parenti, Jun17 2005
// Modified: AP, Dec12 2006
void Cttcrx::set_TTCRX_config_print() {
  printf("%s",set_ttcrx_config.msg.c_str());
}


/*******************************/
/* Reset TTCrx Config Register */
/*******************************/
// Author: A.Parenti, Jun17 2005
// Modified: AP, Dec12 2006
void Cttcrx::reset_TTCRX_config() {
  unsigned char addr[3]={19,20,21},err[3];
  unsigned char data[3]={0x1A,0x85,0xA7};
  int i;

  reset_TTCRX_config_reset(); // Reset reset_ttcrx_config struct
  for (i=0;i<3;++i) {
    write_TTCRX(addr[i],data[i]); // write TTCrx register
    err[i] = write_ttcrx.ccbserver_code; // read error if !=0
  }

  if (err[0]==0 && err[1]==0 && err[2]==0) {
    reset_ttcrx_config.write_err = 0; // no errors
  } else {
    reset_ttcrx_config.write_err = 1; // READ ERROR!
  }

// Fill-up message

  reset_ttcrx_config.msg="Reset TTCrx Config Register\n";

// Print the error message
  if (reset_ttcrx_config.write_err==0)
    reset_ttcrx_config.msg+="Error message: No error\n";
  else
    reset_ttcrx_config.msg+="Error message: Reset Error\n";
}


// Reset reset_ttcrx_config struct
// Author: A.Parenti, Dec12 2006
void Cttcrx::reset_TTCRX_config_reset() {
  reset_ttcrx_config.write_err=0;
  reset_ttcrx_config.msg="";
}

// Print result of "Reset Control Register TTCrx " to stdout
// Author: A. Parenti, Jun17 2005
// Modified: AP, Dec12 2006
void Cttcrx::reset_TTCRX_config_print() {
  printf("%s",reset_ttcrx_config.msg.c_str());
}



/*******************************/
/* "read TTCrx" (0x28) command */
/*******************************/

// Send "read TTCrx" (0x28) command and decode reply
// Author: A. Parenti, Jan06 2005
// Modified: AP, Mar24 2005
// Note: Tested at Cessy, Jul09 2005
void Cttcrx::read_TTCRX(unsigned char addr) {

  const unsigned char command_code=0x28;
  unsigned char command_line[2];
  int send_command_result, idx=0;

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]= command_code;
  command_line[1]= addr;

  read_TTCRX_reset(); // Reset read_ttcrx struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,2,READ_TTCRX_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = read_TTCRX_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    read_ttcrx.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nRead TTCrx (0x28): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset read_ttcrx struct
// Author: A.Parenti, Aug09 2006
// Modified: AP, Dec12 2006
void Cttcrx::read_TTCRX_reset() {
  read_ttcrx.code=read_ttcrx.data=read_ttcrx.result=0;

  read_ttcrx.ccbserver_code=-5;
  read_ttcrx.msg="";
}


// Decode "read TTCrx" (0x28) reply
// Author: A. Parenti, Feb03 2005
// Modified: AP, Dec12 2006
int Cttcrx::read_TTCRX_decode(unsigned char *rdstr) {
  const unsigned char right_reply=0x29;
  unsigned char code2;
  int idx=0;

// identification code of the structure
  idx = rstring(rdstr,idx,&read_ttcrx.code);
  code2 = rdstr[idx];

// Fill-up the "result" structure
  idx = rstring(rdstr,idx,&read_ttcrx.data);
  idx = rstring(rdstr,idx,&read_ttcrx.result);

// Fill-up the error code
  if (read_ttcrx.code==CCB_BUSY_CODE) // ccb is busy
    read_ttcrx.ccbserver_code = -1;
  else if (read_ttcrx.code==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    read_ttcrx.ccbserver_code = 1;
  else if (read_ttcrx.code==right_reply) // ccb: right reply
    read_ttcrx.ccbserver_code = 0;
  else // wrong ccb reply
    read_ttcrx.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  read_ttcrx.msg="\nRead TTCrx (0x28)\n";

  if (read_ttcrx.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(read_ttcrx.ccbserver_code)); read_ttcrx.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code: %#2X\n",read_ttcrx.code); read_ttcrx.msg+=tmpstr;
    sprintf(tmpstr,"data: %#2X\n",read_ttcrx.data); read_ttcrx.msg+=tmpstr;
    sprintf(tmpstr,"Result: %s\n",TTCRX_result_to_str(read_ttcrx.result)); read_ttcrx.msg+=tmpstr;
  }

  return idx;
}


// Print result of "read TTCrx" (0x28) to stdout
// Author: A. Parenti, Jan06 2005
// Modified: AP, Dec12 2006
void Cttcrx::read_TTCRX_print() {
  printf("%s",read_ttcrx.msg.c_str());
}


/********************************/
/* "Write TTCrx" (0x27) command */
/********************************/

// Send "Write TTCrx" (0x27) command and decode reply
// Author: A. Parenti, Jan06 2005
// Modified: AP, Mar24 2005
// Note: Tested at Cessy, Jul09 2005
void Cttcrx::write_TTCRX(unsigned char addr, unsigned char data) {

  const unsigned char command_code=0x27;
  unsigned char command_line[3];
  int send_command_result, idx=0;

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]= command_code;
  command_line[1]= addr;
  command_line[2]= data;

  write_TTCRX_reset(); // Reset write_ttcrx struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,3,WRITE_TTCRX_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = write_TTCRX_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    write_ttcrx.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nWrite TTCrx (0x27): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset write_ttcrx struct
// Author: A.Parenti, Aug09 2006
// Modified: AP, Dec12 2006
void Cttcrx::write_TTCRX_reset() {
  write_ttcrx.code1=write_ttcrx.code2=write_ttcrx.result=0;

  write_ttcrx.ccbserver_code=-5;
  write_ttcrx.msg="";
}


// Decode "write TTCrx" (0x27) reply
// Author: A. Parenti, Feb03 2005
// Modified: AP, Dec12 2006
int Cttcrx::write_TTCRX_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x27, right_reply=CCB_OXFC;
  int idx=0;

// identification code of the structure
  idx = rstring(rdstr,idx,&write_ttcrx.code1);
  idx = rstring(rdstr,idx,&write_ttcrx.code2);

// Fill-up the "result" structure
  idx = rstring(rdstr,idx,&write_ttcrx.result);

// Fill-up the error code
  if (write_ttcrx.code1==CCB_BUSY_CODE) // ccb is busy
    write_ttcrx.ccbserver_code = -1;
  else if (write_ttcrx.code1==CCB_UNKNOWN1 && write_ttcrx.code2==CCB_UNKNOWN2) // ccb: unknown command
    write_ttcrx.ccbserver_code = 1;
  else if (write_ttcrx.code1==right_reply  && write_ttcrx.code2==command_code) // ccb: right reply
    write_ttcrx.ccbserver_code = 0;
  else // wrong ccb reply
    write_ttcrx.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  write_ttcrx.msg="\nWrite TTCrx (0x27)\n";

  if (write_ttcrx.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(write_ttcrx.ccbserver_code)); write_ttcrx.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",write_ttcrx.code1); write_ttcrx.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",write_ttcrx.code2); write_ttcrx.msg+=tmpstr;
    sprintf(tmpstr,"Result: %s\n",TTCRX_result_to_str(write_ttcrx.result)); write_ttcrx.msg+=tmpstr;
  }

  return idx;
}


// Print result of "write TTCrx" (0x27) to stdout
// Author: A. Parenti, Jan06 2005
// Modified: AP, Dec12 2006
void Cttcrx::write_TTCRX_print() {
  printf("%s",write_ttcrx.msg.c_str());
}


/*****************************/
/* "TTC skew" (0x48) command */
/*****************************/

// Send "TTC skew" (0x48) command and decode reply
// Author: A. Parenti, Jan 12 2005
// Modified: AP, Mar24 2005
// Tested at PD, Mar29 2005
void Cttcrx::TTC_skew(char delay) {

  const unsigned char command_code=0x48;
  unsigned char command_line[2];
  int send_command_result, idx=0;

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]= command_code;
  command_line[1]= delay;

  TTC_skew_reset(); // Reset ttc_skew struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,2,TTC_SKEW_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = TTC_skew_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    ttc_skew.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nTTC skew (0x48): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset ttc_skew struct
// Author: A.Parenti Aug09 2006
// Modified: AP, Dec12 2006
void Cttcrx::TTC_skew_reset() {
  ttc_skew.code1=ttc_skew.code2=ttc_skew.result=0;

  ttc_skew.ccbserver_code=-5;
  ttc_skew.msg="";
}


// Decode "TTC skew" (0x48) reply
// Author: A. Parenti, Feb03 2005
// Modified: AP, Dec12 2006
int Cttcrx::TTC_skew_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x48, right_reply=CCB_OXFC;
  int idx=0;

// identification code of the structure
  idx = rstring(rdstr,idx,&ttc_skew.code1);
  idx = rstring(rdstr,idx,&ttc_skew.code2);

// Fill-up the "result" structure
  idx = rstring(rdstr,idx,&ttc_skew.result);

// Fill-up the error code
  if (ttc_skew.code1==CCB_BUSY_CODE) // ccb is busy
    ttc_skew.ccbserver_code = -1;
  else if (ttc_skew.code1==CCB_UNKNOWN1 && ttc_skew.code2==CCB_UNKNOWN2) // ccb: unknown command
    ttc_skew.ccbserver_code = 1;
  else if (ttc_skew.code1==right_reply  && ttc_skew.code2==command_code) // ccb: right reply
    ttc_skew.ccbserver_code = 0;
  else // wrong ccb reply
    ttc_skew.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  ttc_skew.msg="\nTTC skew (0x48)\n";

  if (ttc_skew.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(ttc_skew.ccbserver_code)); ttc_skew.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",ttc_skew.code1); ttc_skew.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",ttc_skew.code2); ttc_skew.msg+=tmpstr;
    sprintf(tmpstr,"Result: %s\n",TTCRX_result_to_str(ttc_skew.result)); ttc_skew.msg+=tmpstr;
  }

  return idx;
}


// Print result of "TTC skew" (0x48) to stdout
// Author: A. Parenti, Jan12 2005
// Modified: AP, Dec12 200606
void Cttcrx::TTC_skew_print() {
  printf("%s",ttc_skew.msg.c_str());
}

/*****************************/
/* "Emul TTC" (0x50) command */
/*****************************/

// Send "Emul TTC" (0x50) command and decode reply
// Author: A. Parenti, Jan12 2005
// Modified: AP, Mar24 2005
// Tested at PD, Mar29 2005
void Cttcrx::emul_TTC(char cmd) {

  const unsigned char command_code=0x50;
  unsigned char command_line[2];
  int send_command_result, idx=0;

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]= command_code;
  command_line[1]= cmd;

  emul_TTC_reset(); // Reset emul_ttc struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,2,EMUL_TTC_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = emul_TTC_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    emul_ttc.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nEmul TTC (0x50): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset emul_ttc struct
// Author: A.Parenti, Aug09 2006
// Modified: AP, Dec12 2006
void Cttcrx::emul_TTC_reset() {
  emul_ttc.code1=emul_ttc.code2=0;

  emul_ttc.ccbserver_code=-5;
  emul_ttc.msg="";
}


// Decode "Emul TTC" (0x50) reply
// Author: A. Parenti, Feb03 2005
// Modified: AP, Dec12 2006
int Cttcrx::emul_TTC_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x50, right_reply=CCB_OXFC;
  int idx=0;

// identification code of the structure
  idx = rstring(rdstr,idx,&emul_ttc.code1);
  idx = rstring(rdstr,idx,&emul_ttc.code2);

// Fill-up the error code
  if (emul_ttc.code1==CCB_BUSY_CODE) // ccb is busy
    emul_ttc.ccbserver_code = -1;
  else if (emul_ttc.code1==CCB_UNKNOWN1 && emul_ttc.code2==CCB_UNKNOWN2) // ccb: unknown command
    emul_ttc.ccbserver_code = 1;
  else if (emul_ttc.code1==right_reply  && emul_ttc.code2==command_code) // ccb: right reply
    emul_ttc.ccbserver_code = 0;
  else // wrong ccb reply
    emul_ttc.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  emul_ttc.msg="\nEmul TTC (0x50)\n";

  if (emul_ttc.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(emul_ttc.ccbserver_code)); emul_ttc.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",emul_ttc.code1); emul_ttc.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",emul_ttc.code2); emul_ttc.msg+=tmpstr;
  }
  return idx;
}



// Print result of "Emul TTC" (0x50) to stdout
// Author: A. Parenti, Jan12 2005
// Modified: AP, Dec12 2006
void Cttcrx::emul_TTC_print() {
  printf("%s",emul_ttc.msg.c_str());
}


/*******************************/
/* "sel TTC Ck" (0x2A) command */
/*******************************/

// Send "sel TTC Ck" (0x2A) command and decode reply
// Author: A. Parenti, Jan 12 2005
// Modified: AP, Mar24 2005
// Tested at PD, Mar29 2005
void Cttcrx::sel_TTC_ck(char ck_type) {

  const unsigned char command_code=0x2A;
  unsigned char command_line[2];
  int send_command_result, idx=0;

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]= command_code;
  command_line[1]= ck_type;

  sel_TTC_ck_reset(); // Reset sel_ttc_ck struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,2,SEL_TTC_CK_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = sel_TTC_ck_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    sel_ttc_ck.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nsel TTC Ck (0x2A): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset sel_ttc_ck struct
// Author: A.Parenti, Aug09 2006
// Modified: AP, Dec12 2006
void Cttcrx::sel_TTC_ck_reset() {
  sel_ttc_ck.code1=sel_ttc_ck.code2=0;

  sel_ttc_ck.ccbserver_code=-5;
  sel_ttc_ck.msg="";
}


// Decode "sel TTC Ck" (0x2A) reply
// Author: A. Parenti, Feb03 2005
// Modified: AP, Dec12 2006
int Cttcrx::sel_TTC_ck_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x2A, right_reply=CCB_OXFC;
  int idx=0;

// identification code of the structure
  idx = rstring(rdstr,idx,&sel_ttc_ck.code1);
  idx = rstring(rdstr,idx,&sel_ttc_ck.code2);

// Fill-up the error code
  if (sel_ttc_ck.code1==CCB_BUSY_CODE) // ccb is busy
    sel_ttc_ck.ccbserver_code = -1;
  else if (sel_ttc_ck.code1==CCB_UNKNOWN1 && sel_ttc_ck.code2==CCB_UNKNOWN2) // ccb: unknown command
    sel_ttc_ck.ccbserver_code = 1;
  else if (sel_ttc_ck.code1==right_reply  && sel_ttc_ck.code2==command_code) // ccb: right reply
    sel_ttc_ck.ccbserver_code = 0;
  else // wrong ccb reply
    sel_ttc_ck.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  sel_ttc_ck.msg="\nsel TTC Ck (0x2A)\n";

  if (sel_ttc_ck.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(sel_ttc_ck.ccbserver_code)); sel_ttc_ck.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",sel_ttc_ck.code1); sel_ttc_ck.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",sel_ttc_ck.code2); sel_ttc_ck.msg+=tmpstr;
  }

  return idx;
}



// Print result of "sel TTC Ck" (0x2A) to stdout
// Author: A. Parenti, Jan12 2005
// Modified: AP, Dec12 2006
void Cttcrx::sel_TTC_ck_print() {
  printf("%s",sel_ttc_ck.msg.c_str());
}



/********************************************/
/* "reset lose lock counter" (0x8A) command */
/********************************************/

// Send "reset lose lock counter" (0x8A) command and decode reply
// Author: A. Parenti, Jan10 2005
// Modified: AP, Mar24 2005
// Note: Tested at Cessy, Jul09 2005
void Cttcrx::reset_lose_lock_counter() {

  const unsigned char command_code=0x8A;
  unsigned char command_line[1];
  int send_command_result, idx=0;

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]= command_code;

  reset_lose_lock_counter_reset(); // Reset reset_llc struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1,RESET_LOSE_LOCK_COUNTER_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = reset_lose_lock_counter_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    reset_llc.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nReset Lose Lock Counter (0x8A): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset reset_llc struct
// Author: A.Parenti, Aug09 2006
// Modified: AP, Dec12 2006
void Cttcrx::reset_lose_lock_counter_reset() {
  reset_llc.code1=reset_llc.code2=0;

  reset_llc.ccbserver_code=-5;
  reset_llc.msg="";
}


// Decode "reset lose lock counter" (0x8A) reply
// Author: A. Parenti, Feb03 2005
// Modified: AP, Dec12 2006
int Cttcrx::reset_lose_lock_counter_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x8A, right_reply=CCB_OXFC;
  int idx=0;

// identification code of the structure
  idx = rstring(rdstr,idx,&reset_llc.code1);
  idx = rstring(rdstr,idx,&reset_llc.code2);

// Fill-up the error code
  if (reset_llc.code1==CCB_BUSY_CODE) // ccb is busy
    reset_llc.ccbserver_code = -1;
  else if (reset_llc.code1==CCB_UNKNOWN1 && reset_llc.code2==CCB_UNKNOWN2) // ccb: unknown command
    reset_llc.ccbserver_code = 1;
  else if (reset_llc.code1==right_reply  && reset_llc.code2==command_code) // ccb: right reply
    reset_llc.ccbserver_code = 0;
  else // wrong ccb reply
    reset_llc.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  reset_llc.msg="\nReset Lose Lock Counter (0x8A)\n";

  if (reset_llc.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(reset_llc.ccbserver_code)); reset_llc.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",reset_llc.code1); reset_llc.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",reset_llc.code2); reset_llc.msg+=tmpstr;
  }

  return idx;
}


// Print result of "reset lose lock counter" (0x8A) to stdout
// Author: A. Parenti, Jan10 2005
// Modified: AP, Dec12 2006
void Cttcrx::reset_lose_lock_counter_print() {
  printf("%s",reset_llc.msg.c_str());
}


/***************************************/
/* "set TTC fine delay" (0x8E) command */
/***************************************/

// Send "TTC fine delay" (0x8E) command and decode reply
// Author: A. Parenti, Feb26 2005
// Modified: AP, Dec11 2006
void Cttcrx::set_TTC_fine_delay(char ch, float delay) {

  const unsigned char command_code=0x8E;
  unsigned char command_line[6];
  int send_command_result, idx=0;

  mc_firmware_version(); // Get firmware version
  set_TTC_fine_delay_reset(); // Reset ttc_fine_delay struct
  if (HVer<1 || (HVer==1 && LVer<6)) { // Firmware < v1.6
    char tmpstr[200];
    sprintf(tmpstr,"Attention: command %#X supported after v1.6 (actual: v%d.%d)\n",command_code,HVer,LVer);
    ttc_fine_delay.msg=tmpstr;
    std::cout << tmpstr;
    return;
  }

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]= command_code;
  command_line[1]= ch;
  wstring(delay,2,command_line);

// Send the command line
  send_command_result = cmd_obj->send_command(command_line,6,TTC_FINE_DELAY_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = set_TTC_fine_delay_decode(cmd_obj->data_read); // Decode reply
  }
  else // copy error code
    ttc_fine_delay.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nTTC Fine Delay (0x8E): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset ttc_fine_delay struct
// Author: A.Parenti, Aug09 2006
// Modified: AP, Dec12 2006
void Cttcrx::set_TTC_fine_delay_reset() {
  ttc_fine_delay.code1=ttc_fine_delay.code2=ttc_fine_delay.result=0;

  ttc_fine_delay.ccbserver_code=-5;
  ttc_fine_delay.msg="";
}


// Decode "TTC fine delay" (0x8E) reply
// Author: A. Parenti, Feb26 2005
// Modified: AP, Dec12 2006
int Cttcrx::set_TTC_fine_delay_decode(unsigned char *rdstr) {
  const unsigned char command_code=0x8E, right_reply=CCB_OXFC;
  int idx=0;

// identification code of the structure
  idx = rstring(rdstr,idx,&ttc_fine_delay.code1);
  idx = rstring(rdstr,idx,&ttc_fine_delay.code2);

// Fill-up the "result" structure
  idx = rstring(rdstr,idx,&ttc_fine_delay.result);

// Fill-up the error code
  if (ttc_fine_delay.code1==CCB_BUSY_CODE) // ccb is busy
    ttc_fine_delay.ccbserver_code = -1;
  else if (ttc_fine_delay.code1==CCB_UNKNOWN1 && ttc_fine_delay.code2==CCB_UNKNOWN2) // ccb: unknown command
    ttc_fine_delay.ccbserver_code = 1;
  else if (ttc_fine_delay.code1==right_reply  && ttc_fine_delay.code2==command_code) // ccb: right reply
    ttc_fine_delay.ccbserver_code = 0;
  else // wrong ccb reply
    ttc_fine_delay.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  ttc_fine_delay.msg="\nTTC Fine Delay (0x8E)\n";

  if (ttc_fine_delay.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(ttc_fine_delay.ccbserver_code)); ttc_fine_delay.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n",ttc_fine_delay.code1);  ttc_fine_delay.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n",ttc_fine_delay.code2); ttc_fine_delay.msg+=tmpstr;
    sprintf(tmpstr,"Result: %s\n",TTCRX_result_to_str(ttc_fine_delay.result)); ttc_fine_delay.msg+=tmpstr;
  }

  return idx;
}


// Print result of "TTC fine delay" (0x8E) to stdout
// Author: A. Parenti, Feb26 2005
// Modified: AP, Dec12 2006
void Cttcrx::set_TTC_fine_delay_print() {
  printf("%s",ttc_fine_delay.msg.c_str());
}


// Decode RdTTCrx/WrTTCrx result.
// Author: A.Parenti, Aug24 2006
char* Cttcrx::TTCRX_result_to_str(char result) {
  static char replystr[20];

  switch (result) {
  case 0:
    strcpy(replystr,"OK");
    break;
  case 1:
    strcpy(replystr,"No ack");
    break;
  case -1:
    strcpy(replystr,"SDA error");
    break;
  case -2:
    strcpy(replystr,"SCL error");
    break;
  default:
    strcpy(replystr,"");
  }

  return replystr;
}

///////////////////////
//// Boot Commands ////
///////////////////////


/*************************************/
/* "TTC Write (boot)" (0xE3) command */
/*************************************/

// Send "TTC Write (boot)" (0xE3) command and decode reply
// Author: A. Parenti, Sep07 2006
void Cttcrx::write_TTC_boot(unsigned char addr, unsigned char data) {

  const unsigned char command_code=0xE3;
  unsigned char command_line[3];
  int send_command_result, idx=0;

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]= command_code;
  command_line[1]= addr;
  command_line[2]= data;

  write_TTC_boot_reset(); // Reset write_ttc_boot struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,3,WRITE_TTCRX_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = write_TTC_boot_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    write_ttc_boot.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nTTC Write (boot) (0xE3): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset write_ttc_boot struct
// Author: A.Parenti, Sep07 2006
// Modified: AP, Dec12 2006
void Cttcrx::write_TTC_boot_reset() {
  write_ttc_boot.code1=write_ttc_boot.code2=write_ttc_boot.error=0;

  write_ttc_boot.ccbserver_code=-5;
  write_ttc_boot.msg="";
}


// Decode "write TTCrx" (0xE3) reply
// Author: A. Parenti, Sep07 2006
// Modified: AP, Dec12 2006
int Cttcrx::write_TTC_boot_decode(unsigned char *rdstr) {
  const unsigned char command_code=0xE3, right_reply=CCB_OXFC;
  int idx=0;

// identification code of the structure
  idx = rstring(rdstr,idx,&write_ttc_boot.code1);
  idx = rstring(rdstr,idx,&write_ttc_boot.code2);

// Fill-up the "result" structure
  idx = rstring(rdstr,idx,&write_ttc_boot.error);

// Fill-up the error code
  if (write_ttc_boot.code1==CCB_BUSY_CODE) // ccb is busy
    write_ttc_boot.ccbserver_code = -1;
  else if (write_ttc_boot.code1==CCB_UNKNOWN1 && write_ttc_boot.code2==CCB_UNKNOWN2) // ccb: unknown command
    write_ttc_boot.ccbserver_code = 1;
  else if (write_ttc_boot.code1==right_reply  && write_ttc_boot.code2==command_code) // ccb: right reply
    write_ttc_boot.ccbserver_code = 0;
  else // wrong ccb reply
    write_ttc_boot.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  write_ttc_boot.msg="\nTTC Write (boot) (0xE3)\n";

  if (write_ttc_boot.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(write_ttc_boot.ccbserver_code)); write_ttc_boot.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n", write_ttc_boot.code1); write_ttc_boot.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n", write_ttc_boot.code2); write_ttc_boot.msg+=tmpstr;
    sprintf(tmpstr,"Result: %s\n", TTCRX_result_to_str(write_ttc_boot.error)); write_ttc_boot.msg+=tmpstr;
  }

  return idx;
}


// Print result of "write TTCrx" (0xE3) to stdout
// Author: A. Parenti, Sep07 2006
// Modified: AP, Dec12 2006
void Cttcrx::write_TTC_boot_print() {
  printf("%s",write_ttc_boot.msg.c_str());
}


/************************************/
/* "TTC Read (boot)" (0xE2) command */
/************************************/

// Send "TTC Read (boot)" (0xE2) command and decode reply
// Author: A. Parenti, Sep07 2006
void Cttcrx::read_TTC_boot(unsigned char addr) {

  const unsigned char command_code=0xE2;
  unsigned char command_line[2];
  int send_command_result, idx=0;

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]= command_code;
  command_line[1]= addr;

  read_TTC_boot_reset(); // Reset read_ttc_boot struct
  send_command_result = cmd_obj->send_command(command_line,2,READ_TTCRX_TIMEOUT);


  if (send_command_result>=0) { // alright
    idx = read_TTC_boot_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    read_ttc_boot.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nTTC Read (boot) (0xE2): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset read_ttc_boot struct
// Author: A.Parenti, Sep07 2006
// Modified: AP, Dec12 2006
void Cttcrx::read_TTC_boot_reset() {
  read_ttc_boot.code=read_ttc_boot.data=read_ttc_boot.result=0;

  read_ttc_boot.ccbserver_code=-5;
  read_ttc_boot.msg="";
}


// Decode "TTC Read (boot)" (0xE2) reply
// Author: A. Parenti, Sep07 2006
// Modified: AP, Dec12 2006
int Cttcrx::read_TTC_boot_decode(unsigned char *rdstr) {
  const unsigned char right_reply=0xE1;
  unsigned char code2;
  int idx=0;

// identification code of the structure
  idx = rstring(rdstr,idx,&read_ttc_boot.code);
  code2 = rdstr[idx];

// Fill-up the "result" structure
  idx = rstring(rdstr,idx,&read_ttc_boot.data);
  idx = rstring(rdstr,idx,&read_ttc_boot.result);

// Fill-up the error code
  if (read_ttc_boot.code==CCB_BUSY_CODE) // ccb is busy
    read_ttc_boot.ccbserver_code = -1;
  else if (read_ttc_boot.code==CCB_UNKNOWN1 && code2==CCB_UNKNOWN2) // ccb: unknown command
    read_ttc_boot.ccbserver_code = 1;
  else if (read_ttc_boot.code==right_reply) // ccb: right reply
    read_ttc_boot.ccbserver_code = 0;
  else // wrong ccb reply
    read_ttc_boot.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  read_ttc_boot.msg="\nTTC Read (boot) (0xE2)\n";

  if (read_ttc_boot.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(read_ttc_boot.ccbserver_code)); read_ttc_boot.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code: %#2X\n", read_ttc_boot.code); read_ttc_boot.msg+=tmpstr;
    sprintf(tmpstr,"data: %#2X\n", read_ttc_boot.data); read_ttc_boot.msg+=tmpstr;
    sprintf(tmpstr,"Result: %s\n", TTCRX_result_to_str(read_ttc_boot.result)); read_ttc_boot.msg+=tmpstr;
  }

  return idx;
}


// Print result of "TTC Read (boot)" (0xE2) to stdout
// Author: A. Parenti, Sep07 2006
// Modified: AP, Dec12 2006
void Cttcrx::read_TTC_boot_print() {
  printf("%s",read_ttc_boot.msg.c_str());
}



/************************************/
/* "TTC Find (boot)" (0xE0) command */
/************************************/

// Send "TTC Find (boot)" (0xE0) command and decode reply
// Author: A. Parenti, Sep07 2006
void Cttcrx::find_TTC_boot() {

  const unsigned char command_code=0xE0;
  unsigned char command_line[1];
  int send_command_result, idx=0;

// lock "cmd_mutex"
  cmd_obj->cmdlock();

// create the command line
  command_line[0]= command_code;

  find_TTC_boot_reset(); // Reset find_ttc_boot struct
// Send the command line
  send_command_result = cmd_obj->send_command(command_line,1,FIND_TTC_TIMEOUT);

  if (send_command_result>=0) { // alright
    idx = find_TTC_boot_decode(cmd_obj->data_read); // Decode the ccb reply
  }
  else // copy error code
    find_ttc_boot.ccbserver_code = send_command_result;

  if (_verbose_)
    printf("\nTTC Find (boot) (0xE0): %d (%d) bytes read (decoded)\n",cmd_obj->data_read_len,idx);

// unlock "cmd_mutex"
  cmd_obj->cmdunlock();
}


// Reset find_ttc_boot struct
// Author: A.Parenti, Sep07 2006
// Modified: AP, Dec12 2006
void Cttcrx::find_TTC_boot_reset() {
  find_ttc_boot.code1=find_ttc_boot.code2=find_ttc_boot.error=0;

  find_ttc_boot.ccbserver_code=-5;
  find_ttc_boot.msg="";
}


// Decode "TTC Find (boot)" (0xE0) reply
// Author: A. Parenti, Sep07 2006
// Modified: AP, Dec12 2006
int Cttcrx::find_TTC_boot_decode(unsigned char *rdstr) {
  const unsigned char command_code=0xE0, right_reply=CCB_OXFC;
  int idx=0;

// identification code of the structure
  idx = rstring(rdstr,idx,&find_ttc_boot.code1);
  idx = rstring(rdstr,idx,&find_ttc_boot.code2);

// Fill-up the "result" structure
  idx = rstring(rdstr,idx,&find_ttc_boot.error);

// Fill-up the error code
  if (find_ttc_boot.code1==CCB_BUSY_CODE) // ccb is busy
    find_ttc_boot.ccbserver_code = -1;
  else if (find_ttc_boot.code1==CCB_UNKNOWN1 && find_ttc_boot.code2==CCB_UNKNOWN2) // ccb: unknown command
    find_ttc_boot.ccbserver_code = 1;
  else if (find_ttc_boot.code1==right_reply && find_ttc_boot.code2==command_code) // ccb: right reply
    find_ttc_boot.ccbserver_code = 0;
  else // wrong ccb reply
    find_ttc_boot.ccbserver_code = 2;

// Fill-up message
  char tmpstr[100];

  find_ttc_boot.msg="\nTTC Find (boot) (0xE0)\n";

  if (find_ttc_boot.ccbserver_code!=0) { // Read error
    sprintf(tmpstr,"Error message: %s\n",cmd_obj->read_error_message(find_ttc_boot.ccbserver_code)); find_ttc_boot.msg+=tmpstr;
  } else {
    sprintf(tmpstr,"code1: %#2X\n", find_ttc_boot.code1); find_ttc_boot.msg+=tmpstr;
    sprintf(tmpstr,"code2: %#2X\n", find_ttc_boot.code2); find_ttc_boot.msg+=tmpstr;

    switch (find_ttc_boot.error) {
    case 0:
      find_ttc_boot.msg+="Result: OK\n";
      break;
    case 1:
      find_ttc_boot.msg+="Result: Not Found\n";
      break;
    case -1:
      find_ttc_boot.msg+="Result: SDA error\n";
      break;
    case -2:
      find_ttc_boot.msg+="Result: SCL error\n";
      break;
    default:
      find_ttc_boot.msg+="Result: \n";
    }
  }

  return idx;
}


// Print result of "TTC Find (boot)" (0xE0) to stdout
// Author: A. Parenti, Sep07 2006
// Modified: AP, Dec12 2006
void Cttcrx::find_TTC_boot_print() {
  printf("%s",find_ttc_boot.msg.c_str());
}
