#include <ccbcmds/apfunctions.h>
#include <ccbcmds/Cdef.h>
#include <ccbcmds/Ccommand.h>
#include <ccbcmds/Cccb.h>
#include <ccbcmds/Ci2c.h>
#include <ccbcmds/Clog.h>

#include <cstdio>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <ctime>

#define PCA9544 0x70 //01110000
#define AD7417 0x28  //01010000

#define server "10.176.60.232"
#define ccbid 34
#define RX 6
#define THR 0x20 //VTH1
#define ROLL 0x04

int main(int argc, char **argv)
{
    char ciccio[14];

    std::stringstream inps;
    std::string cs;
    short ccbport;
    short INOUT;
    short VTH;
    short FWBW;
    std::cout << "NUmber of Argument used" << argc << std::endl;

    if (argc == 6)
    {
        cs = argv[1];
        inps << argv[2] << " " << argv[3] << " " << argv[4] << " " << argv[5];
        inps >> ccbport >> INOUT >> std::hex >> VTH >> FWBW;
    }
    else
    {
        cs = server;
        ccbport = ccbid;
        INOUT = RX;
        VTH = THR;
        FWBW = ROLL;
    }

    short ctrl_data[I2C_DATA_MAX];
    short err_data[I2C_DATA_MAX];

    // I need to select the Minicrate via Wheel Sector Station

    Clog *myLog = new Clog("ciccio.txt"); // No ccbcmds log

    std::cout << "Open COnnection on " << ciccio << ":" << CCBPORT
              << " for ccb id " << ccbport
              << " INOUT " << INOUT << " TH " << VTH << " FWBW " << FWBW
              << std::endl;
    Ccommand *myCmd = new Ccommand(ccbport, const_cast<char *>(cs.c_str()), CCBPORT, myLog);
    if (!myCmd->connect())
    {
        std::cout << " No connection " << std::endl;
        return -1;
    }

    std::cout << " done it!" << myCmd << std::endl;
    Cccb *myCcb = new Cccb(myCmd); // CCB object
    Ci2c *myI2c = new Ci2c(myCmd); // I2C object
    short size;
    // enable mux
    // write on PCA9544 + 7 RB1,2_in RB3,RB4
    // write on PCA9544 + 6 RB1,2_out
    //Enabling Sleow-Ctrl
    ctrl_data[0] = 0x0100 + ((PCA9544 + INOUT) << 1); //start I2C sequence on Distr. Board U10
    ctrl_data[1] = 0x0200 + 0x4;                      // wrI2C:Enable U10/Ch0
    size = 2;
    myI2c->rpc_I2C(ctrl_data, size);
    Si2c_1 res = myI2c->rpc_i2c;
    //myI2c->rpc_I2C_print();

    // only one partition at the time
    ctrl_data[0] = 0x0100 + ((PCA9544 + 1) << 1); //start I2C sequence on Distr. Board U4
    ctrl_data[1] = 0x0200 + FWBW;                 //  wrI2C:  Enable U4 Forward Eta Partition
    size = 2;
    myI2c->rpc_I2C(ctrl_data, size);

    for (unsigned char ifeb = 0; ifeb < 6; ifeb++)
    {

        // Read VTH1 (0x20) and VTH2 (0x040) of the first feb (ifeb=0)
        ctrl_data[0] = 0x0100 + ((AD7417 + ifeb) << 1); //start I2C sequence on first FEB
        ctrl_data[1] = 0x0000 + 0x01;                   // Write Addr. Point CONF_REG
        ctrl_data[2] = 0x0200 + VTH;                    //Prepare for ADC Read
        size = 3;
        myI2c->rpc_I2C(ctrl_data, size);
        //myI2c->rpc_I2C_print();

        ctrl_data[0] = 0x0100 + ((AD7417 + ifeb) << 1); //start I2C sequence on first FEB
        ctrl_data[1] = 0x0200 + 0x04;                   // Write Addr. Point CONF_REG
        size = 2;
        myI2c->rpc_I2C(ctrl_data, size);
        res = myI2c->rpc_i2c;
        //myI2c->rpc_I2C_print();

        ctrl_data[0] = 0x0100 + ((AD7417 + ifeb) << 1) + 1; //start I2C sequence on first FEB
        ctrl_data[1] = 0x0400;                              //read
        ctrl_data[2] = 0x0600;                              //read
        size = 3;
        myI2c->rpc_I2C(ctrl_data, size);
        res = myI2c->rpc_i2c;
        //myI2c->rpc_I2C_print();
        //
        short res1 = (res.err_data[1] & 0x00FF);
        short res2 = (res.err_data[2] & 0x00FF);
        int result = (res2 + (res1 << 8));
        /*
  std::cout <<"result = 0x"
	    <<std::setw(4)<<std::setfill('0')<<std::hex
	    <<res1<<std::endl;
  std::cout <<"result = 0x"
	    <<std::setw(4)<<std::setfill('0')<<std::hex
	    <<res2<<std::endl;
  std::cout <<"result = 0x"
	    <<std::setw(4)<<std::setfill('0')<<std::hex
	    <<result<<std::endl;
  */
        int nout = (result & 0xFFC0) >> 6;
        /*  std::cout 
  */
        float vdac = nout * 2.5 / 1.024; /* convert in mV*/
        std::cout << "Thr = " << vdac << " mVolts"
                  << "adc count "
                  << std::setw(4) << std::setfill('0') << std::hex
                  << nout << " r " << result
                  << std::endl;
    }
    // disable mux
    // only one partition at the time
    ctrl_data[0] = 0x0100 + ((PCA9544 + 1) << 1); //start I2C sequence on Distr. Board U4
    ctrl_data[1] = 0x0200 + 0x0;                  //  wrI2C:  Disable U4 Forward Eta Partition
    size = 2;
    myI2c->rpc_I2C(ctrl_data, size);

    // write on PCA9544 + 7 RB1,2_in RB3,RB4
    // write on PCA9544 + 6 RB1,2_out
    //Disabling Slow-Ctrl
    ctrl_data[0] = 0x0100 + ((PCA9544 + INOUT) << 1); //start I2C sequence on Distr. Board U10
    ctrl_data[1] = 0x0200 + 0x0;                      // wrI2C:Enable U10/Ch0
    size = 2;
    myI2c->rpc_I2C(ctrl_data, size);

    return 0;
}
