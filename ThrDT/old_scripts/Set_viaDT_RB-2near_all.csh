#!/bin/bash -x
export  LD_LIBRARY_PATH=LD_LIBRARY_PATH_LD:~/DT/ThrDT/ccbcmds2/lib:~/DT/ThrDT/ccbcmds2/ccbdb
# g++ -o rpc_write rpc_write.cc -D'__USE_PTYPES__' -D'__USE_CCBDB__'   -I ccbcmds2/inc -L ccbcmds2/lib -lccbcmds -lpthread -lxml2
# g++ -o rpc_read rpc_read.cc  -D'__USE_PTYPES__' -D'__USE_CCBDB__' -I ccbcmds2/inc -L ccbcmds2/lib -lccbcmds -lpthread -lxml2

#if ( $1 == "" ) then
#echo " You should write also the threshold value (example: ./Set_viaDT_RB-2near_all.csh 230 ) "
#exit 0
#endif

#### Change log ############################################
############################################################
# 2014-03-10 :: PC controlling Mini Crates has changed     #
#               only YB-2 changed so far, rest will follow # 
#               new IP: 10.176.60.236 --> 10.176.10.16     #
############################################################
 

export s1="on"
export s2="on"
export s3="on"
export s10="on"
export s10rb12="off"
export s10rb34="on"
export s11="on"
export s12="on"

if [ $s10 = "on" ]
then
    if [ $s10rb12 = "on" ]
    then 
	echo "W-2/S10/RB1in"
	./rpc_write 10.176.10.16 42 7 0x20 0x04 $1
	./rpc_write 10.176.10.16 42 7 0x40 0x04 $1
	./rpc_write 10.176.10.16 42 7 0x20 0x05 $1
	./rpc_write 10.176.10.16 42 7 0x40 0x05 $1
	
	echo "W-2/S10/RB1out"
	./rpc_write 10.176.10.16 42 6 0x20 0x04 $1
	./rpc_write 10.176.10.16 42 6 0x40 0x04 $1
	./rpc_write 10.176.10.16 42 6 0x20 0x05 $1
	./rpc_write 10.176.10.16 42 6 0x40 0x05 $1

	echo "W-2/S10/RB2in"
	./rpc_write 10.176.10.16 43 7 0x20 0x04 $1
	./rpc_write 10.176.10.16 43 7 0x40 0x04 $1
	./rpc_write 10.176.10.16 43 7 0x20 0x05 $1
	./rpc_write 10.176.10.16 43 7 0x40 0x05 $1
	
	echo "W-2/S10/RB2out"
	./rpc_write 10.176.10.16 43 6 0x20 0x04 $1
	./rpc_write 10.176.10.16 43 6 0x40 0x04 $1
	./rpc_write 10.176.10.16 43 6 0x20 0x05 $1
	./rpc_write 10.176.10.16 43 6 0x40 0x05 $1
	./rpc_write 10.176.10.16 43 6 0x20 0x06 $1
	./rpc_write 10.176.10.16 43 6 0x40 0x06 $1
    fi
    if [ $s10rb34 = "on" ]
    then 
	echo "W-2/S10/RB3"
	./rpc_write 10.176.10.16 44 7 0x20 0x04 $1
	./rpc_write 10.176.10.16 44 7 0x40 0x04 $1
	./rpc_write 10.176.10.16 44 7 0x20 0x05 $1
	./rpc_write 10.176.10.16 44 7 0x40 0x05 $1
	
	echo "W-2/S10/RB4- ??? to be checked"
	./rpc_write 10.176.10.16 45 7 0x20 0x04 $1
	./rpc_write 10.176.10.16 45 7 0x40 0x04 $1
	./rpc_write 10.176.10.16 45 7 0x20 0x05 $1
	./rpc_write 10.176.10.16 45 7 0x40 0x05 $1
	
	echo "W-2/S10/RB4+ ??? to be checked"
	./rpc_write 10.176.10.16 46 7 0x20 0x04 $1
	./rpc_write 10.176.10.16 46 7 0x40 0x04 $1
	./rpc_write 10.176.10.16 46 7 0x20 0x05 $1
	./rpc_write 10.176.10.16 46 7 0x40 0x05 $1
    fi
fi

if [ $s11 = "on" ]
then
    echo "W-2/S11/RB1in"
    ./rpc_write 10.176.10.16 47 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 47 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 47 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 47 7 0x40 0x05 $1
    
    echo "W-2/S11/RB1out"
    ./rpc_write 10.176.10.16 47 6 0x20 0x04 $1
    ./rpc_write 10.176.10.16 47 6 0x40 0x04 $1
    ./rpc_write 10.176.10.16 47 6 0x20 0x05 $1
    ./rpc_write 10.176.10.16 47 6 0x40 0x05 $1
    
    echo "W-2/S11/RB2in"
    ./rpc_write 10.176.10.16 48 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 48 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 48 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 48 7 0x40 0x05 $1
    
    echo "W-2/S11/RB2out"
    ./rpc_write 10.176.10.16 48 6 0x20 0x04 $1
    ./rpc_write 10.176.10.16 48 6 0x40 0x04 $1
    ./rpc_write 10.176.10.16 48 6 0x20 0x05 $1
    ./rpc_write 10.176.10.16 48 6 0x40 0x05 $1
    ./rpc_write 10.176.10.16 48 6 0x20 0x06 $1
    ./rpc_write 10.176.10.16 48 6 0x40 0x06 $1
    
    echo "W-2/S11/RB3"
    ./rpc_write 10.176.10.16 49 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 49 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 49 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 49 7 0x40 0x05 $1
    
    echo "W-2/S11/RB4 (only one RB4 in S11)"
    ./rpc_write 10.176.10.16 50 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 50 7 0x40 0x04 $1
fi

if [ $s12 = "on" ]
then
    echo "W-2/S12/RB1in"
    ./rpc_write 10.176.10.16 51 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 51 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 51 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 51 7 0x40 0x05 $1
    
    echo "W-2/S12/RB1out"
    ./rpc_write 10.176.10.16 51 6 0x20 0x04 $1
    ./rpc_write 10.176.10.16 51 6 0x40 0x04 $1
    ./rpc_write 10.176.10.16 51 6 0x20 0x05 $1
    ./rpc_write 10.176.10.16 51 6 0x40 0x05 $1

    echo "W-2/S12/RB2in"
    ./rpc_write 10.176.10.16 52 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 52 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 52 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 52 7 0x40 0x05 $1
    
    echo "W-2/S12/RB2out"
    ./rpc_write 10.176.10.16 52 6 0x20 0x04 $1
    ./rpc_write 10.176.10.16 52 6 0x40 0x04 $1
    ./rpc_write 10.176.10.16 52 6 0x20 0x05 $1
    ./rpc_write 10.176.10.16 52 6 0x40 0x05 $1
    ./rpc_write 10.176.10.16 52 6 0x20 0x06 $1
    ./rpc_write 10.176.10.16 52 6 0x40 0x06 $1
    
    echo "W-2/S12/RB3"
    ./rpc_write 10.176.10.16 53 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 53 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 53 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 53 7 0x40 0x05 $1
    
    echo "W-2/S12/RB4"
    ./rpc_write 10.176.10.16 54 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 54 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 54 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 54 7 0x40 0x05 $1
fi

if [ $s1 = "on" ]
then
    echo "W-2/S1/RB1in"
    ./rpc_write 10.176.10.16 1 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 1 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 1 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 1 7 0x40 0x05 $1
    
    echo "W-2/S1/RB1out"
    ./rpc_write 10.176.10.16 1 6 0x20 0x04 $1
    ./rpc_write 10.176.10.16 1 6 0x40 0x04 $1
    ./rpc_write 10.176.10.16 1 6 0x20 0x05 $1
    ./rpc_write 10.176.10.16 1 6 0x40 0x05 $1
    
    echo "W-2/S1/RB2in"
    ./rpc_write 10.176.10.16 2 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 2 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 2 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 2 7 0x40 0x05 $1
    
    echo "W-2/S1/RB2out"
    ./rpc_write 10.176.10.16 2 6 0x20 0x04 $1
    ./rpc_write 10.176.10.16 2 6 0x40 0x04 $1
    ./rpc_write 10.176.10.16 2 6 0x20 0x05 $1
    ./rpc_write 10.176.10.16 2 6 0x40 0x05 $1
    ./rpc_write 10.176.10.16 2 6 0x20 0x06 $1
    ./rpc_write 10.176.10.16 2 6 0x40 0x06 $1

    echo "W-2/S1/RB3"
    ./rpc_write 10.176.10.16 3 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 3 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 3 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 3 7 0x40 0x05 $1
    
    echo "W-2/S1/RB4"
    ./rpc_write 10.176.10.16 4 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 4 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 4 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 4 7 0x40 0x05 $1
fi

if [ $s2 = "on" ]
then        
    echo "W-2/S2/RB1in"
    ./rpc_write 10.176.10.16 5 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 5 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 5 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 5 7 0x40 0x05 $1

    echo "W-2/S2/RB1out"
    ./rpc_write 10.176.10.16 5 6 0x20 0x04 $1
    ./rpc_write 10.176.10.16 5 6 0x40 0x04 $1
    ./rpc_write 10.176.10.16 5 6 0x20 0x05 $1
    ./rpc_write 10.176.10.16 5 6 0x40 0x05 $1
    
    echo "W-2/S2/RB2in"
    ./rpc_write 10.176.10.16 6 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 6 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 6 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 6 7 0x40 0x05 $1
    
    echo "W-2/S2/RB2out"
    ./rpc_write 10.176.10.16 6 6 0x20 0x04 $1
    ./rpc_write 10.176.10.16 6 6 0x40 0x04 $1
    ./rpc_write 10.176.10.16 6 6 0x20 0x05 $1
    ./rpc_write 10.176.10.16 6 6 0x40 0x05 $1
    ./rpc_write 10.176.10.16 6 6 0x20 0x06 $1
    ./rpc_write 10.176.10.16 6 6 0x40 0x06 $1
    
    echo "W-2/S2/RB3"
    ./rpc_write 10.176.10.16 8 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 8 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 8 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 8 7 0x40 0x05 $1
    
    echo "W-2/S2/RB4"
    ./rpc_write 10.176.10.16 9 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 9 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 9 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 9 7 0x40 0x05 $1
fi

if [ $s3 = "on" ]
then
    echo "W-2/S3/RB1in"
    ./rpc_write 10.176.10.16 10 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 10 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 10 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 10 7 0x40 0x05 $1
    
    echo "W-2/S3/RB1out"
    ./rpc_write 10.176.10.16 10 6 0x20 0x04 $1
    ./rpc_write 10.176.10.16 10 6 0x40 0x04 $1
    ./rpc_write 10.176.10.16 10 6 0x20 0x05 $1
    ./rpc_write 10.176.10.16 10 6 0x40 0x05 $1
    
    echo "W-2/S3/RB2in"
    ./rpc_write 10.176.10.16 11 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 11 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 11 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 11 7 0x40 0x05 $1
    
    echo "W-2/S3/RB2out"
    ./rpc_write 10.176.10.16 11 6 0x20 0x04 $1
    ./rpc_write 10.176.10.16 11 6 0x40 0x04 $1
    ./rpc_write 10.176.10.16 11 6 0x20 0x05 $1
    ./rpc_write 10.176.10.16 11 6 0x40 0x05 $1
    ./rpc_write 10.176.10.16 11 6 0x20 0x06 $1
    ./rpc_write 10.176.10.16 11 6 0x40 0x06 $1
    
    echo "W-2/S3/RB3"
    ./rpc_write 10.176.10.16 12 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 12 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 12 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 12 7 0x40 0x05 $1
    
    echo "W-2/S3/RB4"
    ./rpc_write 10.176.10.16 13 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 13 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 13 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 13 7 0x40 0x05 $1
fi