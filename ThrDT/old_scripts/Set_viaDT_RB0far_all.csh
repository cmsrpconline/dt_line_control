#!/bin/bash -x
export  LD_LIBRARY_PATH=LD_LIBRARY_PATH_LD:~/DT/ThrDT/ccbcmds2/lib:~/DT/ThrDT/ccbcmds2/ccbdb
#g++ -o rpc_write rpc_write.cc -D'__USE_PTYPES__' -D'__USE_CCBDB__'   -I ccbcmds2/inc -L ccbcmds2/lib -lccbcmds -lpthread -lxml2
#g++ -o rpc_read rpc_read.cc  -D'__USE_PTYPES__' -D'__USE_CCBDB__' -I ccbcmds2/inc -L ccbcmds2/lib -lccbcmds -lpthread -lxml2

#if ( $1 == "" ) then
#echo " You should write also the threshold value (example: ./Set_viaDT_RB0far_all.csh 230 ) "
#exit 0
#endif

### Wheel 0 ####################################
# Positive Sectors:   2,3,    6,7,    10,11,   # 
# Negative Sectors: 1,    4,5,    8,9,      12 #
################################################

export s4="off"
export s5="off"
export s6="on"
export s7="on"
export s8="on"
export s9="on"

if [ $s4 = "on" ]
then
    echo "W0/S4/RB1in"
    ./rpc_write 10.176.60.232 14 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 14 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 14 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 14 7 0x40 0x05 $1
    
    echo "W0/S4/RB1out"
    ./rpc_write 10.176.60.232 14 6 0x20 0x04 $1
    ./rpc_write 10.176.60.232 14 6 0x40 0x04 $1
    ./rpc_write 10.176.60.232 14 6 0x20 0x05 $1
    ./rpc_write 10.176.60.232 14 6 0x40 0x05 $1
    
    echo "W0/S4/RB2in"
    ./rpc_write 10.176.60.232 15 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 15 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 15 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 15 7 0x40 0x05 $1
    ./rpc_write 10.176.60.232 15 7 0x20 0x06 $1
    ./rpc_write 10.176.60.232 15 7 0x40 0x06 $1
    
    echo "W0/S4/RB2out"
    ./rpc_write 10.176.60.232 15 6 0x20 0x04 $1
    ./rpc_write 10.176.60.232 15 6 0x40 0x04 $1
    ./rpc_write 10.176.60.232 15 6 0x20 0x05 $1
    ./rpc_write 10.176.60.232 15 6 0x40 0x05 $1
    
    echo "W0/S4/RB3"
    ./rpc_write 10.176.60.232 16 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 16 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 16 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 16 7 0x40 0x05 $1
    
    echo "W0/S4/RB4 (RB4-- and RB4-)"
    ./rpc_write 10.176.60.232 17 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 17 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 17 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 17 7 0x40 0x05 $1
    
    echo "W0/S4/RB4 (RB4+ and RB4++)"
    ./rpc_write 10.176.60.232 18 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 18 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 18 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 18 7 0x40 0x05 $1
fi
    
if [ $s5 = "on" ]
then
    echo "W0/S5/RB1in"
    ./rpc_write 10.176.60.232 19 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 19 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 19 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 19 7 0x40 0x05 $1
    
    echo "W0/S5/RB1out"
    ./rpc_write 10.176.60.232 19 6 0x20 0x04 $1
    ./rpc_write 10.176.60.232 19 6 0x40 0x04 $1
    ./rpc_write 10.176.60.232 19 6 0x20 0x05 $1
    ./rpc_write 10.176.60.232 19 6 0x40 0x05 $1

    echo "W0/S5/RB2in"
    ./rpc_write 10.176.60.232 20 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 20 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 20 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 20 7 0x40 0x05 $1
    ./rpc_write 10.176.60.232 20 7 0x20 0x06 $1
    ./rpc_write 10.176.60.232 20 7 0x40 0x06 $1
    
    echo "W0/S5/RB2out"
    ./rpc_write 10.176.60.232 20 6 0x20 0x04 $1
    ./rpc_write 10.176.60.232 20 6 0x40 0x04 $1
    ./rpc_write 10.176.60.232 20 6 0x20 0x05 $1
    ./rpc_write 10.176.60.232 20 6 0x40 0x05 $1
    
    echo "W0/S5/RB3"
    ./rpc_write 10.176.60.232 21 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 21 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 21 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 21 7 0x40 0x05 $1
    
    echo "W0/S5/RB4"
    ./rpc_write 10.176.60.232 22 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 22 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 22 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 22 7 0x40 0x05 $1
fi

if [ $s6 = "on" ]
then
    echo "W0/S6/RB1in"
    ./rpc_write 10.176.60.232 24 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 24 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 24 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 24 7 0x40 0x05 $1
    
    echo "W0/S6/RB1out"
    ./rpc_write 10.176.60.232 24 6 0x20 0x04 $1
    ./rpc_write 10.176.60.232 24 6 0x40 0x04 $1
    ./rpc_write 10.176.60.232 24 6 0x20 0x05 $1
    ./rpc_write 10.176.60.232 24 6 0x40 0x05 $1
    
    echo "W0/S6/RB2in"
    ./rpc_write 10.176.60.232 25 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 25 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 25 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 25 7 0x40 0x05 $1
    ./rpc_write 10.176.60.232 25 7 0x20 0x06 $1
    ./rpc_write 10.176.60.232 25 7 0x40 0x06 $1
    
    echo "W0/S6/RB2out"
    ./rpc_write 10.176.60.232 25 6 0x20 0x04 $1
    ./rpc_write 10.176.60.232 25 6 0x40 0x04 $1
    ./rpc_write 10.176.60.232 25 6 0x20 0x05 $1
    ./rpc_write 10.176.60.232 25 6 0x40 0x05 $1
    
    echo "W0/S6/RB3"
    ./rpc_write 10.176.60.232 39 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 39 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 39 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 39 7 0x40 0x05 $1
    
    echo "W0/S6/RB4"
    ./rpc_write 10.176.60.232 27 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 27 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 27 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 27 7 0x40 0x05 $1
fi

if [ $s7 = "on" ]
then
    echo "W0/S7/RB1in"
    ./rpc_write 10.176.60.232 28 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 28 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 28 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 28 7 0x40 0x05 $1
    
    echo "W0/S7/RB1out"
    ./rpc_write 10.176.60.232 28 6 0x20 0x04 $1
    ./rpc_write 10.176.60.232 28 6 0x40 0x04 $1
    ./rpc_write 10.176.60.232 28 6 0x20 0x05 $1
    ./rpc_write 10.176.60.232 28 6 0x40 0x05 $1
    
    echo "W0/S7/RB2in"
    ./rpc_write 10.176.60.232 29 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 29 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 29 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 29 7 0x40 0x05 $1
    ./rpc_write 10.176.60.232 29 7 0x20 0x06 $1
    ./rpc_write 10.176.60.232 29 7 0x40 0x06 $1
    
    echo "W0/S7/RB2out"
    ./rpc_write 10.176.60.232 29 6 0x20 0x04 $1
    ./rpc_write 10.176.60.232 29 6 0x40 0x04 $1
    ./rpc_write 10.176.60.232 29 6 0x20 0x05 $1
    ./rpc_write 10.176.60.232 29 6 0x40 0x05 $1
    
    echo "W0/S7/RB3"
    ./rpc_write 10.176.60.232 30 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 30 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 30 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 30 7 0x40 0x05 $1
    
    echo "W0/S7/RB4"
    ./rpc_write 10.176.60.232 31 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 31 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 31 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 31 7 0x40 0x05 $1
fi

if [ $s8 = "on" ]
then
    echo "W0/S8/RB1in"
    ./rpc_write 10.176.60.232 33 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 33 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 33 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 33 7 0x40 0x05 $1
    
    echo "W0/S8/RB1out"
    ./rpc_write 10.176.60.232 33 6 0x20 0x04 $1
    ./rpc_write 10.176.60.232 33 6 0x40 0x04 $1
    ./rpc_write 10.176.60.232 33 6 0x20 0x05 $1
    ./rpc_write 10.176.60.232 33 6 0x40 0x05 $1
    
    echo "W0/S8/RB2in"
    ./rpc_write 10.176.60.232 34 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 34 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 34 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 34 7 0x40 0x05 $1
    ./rpc_write 10.176.60.232 34 7 0x20 0x06 $1
    ./rpc_write 10.176.60.232 34 7 0x40 0x06 $1
    
    echo "W0/S8/RB2out"
    ./rpc_write 10.176.60.232 34 6 0x20 0x04 $1
    ./rpc_write 10.176.60.232 34 6 0x40 0x04 $1
    ./rpc_write 10.176.60.232 34 6 0x20 0x05 $1
    ./rpc_write 10.176.60.232 34 6 0x40 0x05 $1
    
    echo "W0/S8/RB3"
    ./rpc_write 10.176.60.232 35 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 35 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 35 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 35 7 0x40 0x05 $1
    
    echo "W0/S8/RB4"
    ./rpc_write 10.176.60.232 36 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 36 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 36 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 36 7 0x40 0x05 $1
fi

if [ $s9 = "on" ]
then
    echo "W0/S9/RB1in"
    ./rpc_write 10.176.60.232 37 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 37 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 37 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 37 7 0x40 0x05 $1
    
    echo "W0/S9/RB1out"
    ./rpc_write 10.176.60.232 37 6 0x20 0x04 $1
    ./rpc_write 10.176.60.232 37 6 0x40 0x04 $1
    ./rpc_write 10.176.60.232 37 6 0x20 0x05 $1
    ./rpc_write 10.176.60.232 37 6 0x40 0x05 $1
    
    echo "W0/S9/RB2in"
    ./rpc_write 10.176.60.232 38 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 38 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 38 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 38 7 0x40 0x05 $1
    ./rpc_write 10.176.60.232 38 7 0x20 0x06 $1
    ./rpc_write 10.176.60.232 38 7 0x40 0x06 $1
    
    echo "W0/S9/RB2out"
    ./rpc_write 10.176.60.232 38 6 0x20 0x04 $1
    ./rpc_write 10.176.60.232 38 6 0x40 0x04 $1
    ./rpc_write 10.176.60.232 38 6 0x20 0x05 $1
    ./rpc_write 10.176.60.232 38 6 0x40 0x05 $1
    
    echo "W0/S9/RB3"
    ./rpc_write 10.176.60.232 40 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 40 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 40 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 40 7 0x40 0x05 $1
    
    echo "W0/S9/RB4 (only one RB4 in S9)"
    ./rpc_write 10.176.60.232 41 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 41 7 0x40 0x04 $1
fi
