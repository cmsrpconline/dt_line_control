export  LD_LIBRARY_PATH=LD_LIBRARY_PATH_LD:~/DT/ThrDT/ccbcmds2/lib:~/DT/ThrDT/ccbcmds2/ccbdb
g++ -o rpc_write rpc_write.cc -D'__USE_PTYPES__' -D'__USE_CCBDB__'   -I ccbcmds2/inc -L ccbcmds2/lib -lccbcmds -lpthread -lxml2
g++ -o rpc_read rpc_read.cc  -D'__USE_PTYPES__' -D'__USE_CCBDB__' -I ccbcmds2/inc -L ccbcmds2/lib -lccbcmds -lpthread -lxml2

echo "W+2 RB4-- sect4 (LV block is close to S3, NO EXIT) "
./rpc_write 10.176.60.228 17 7 0x20 0x04 $1
./rpc_write 10.176.60.228 17 7 0x40 0x04 $1

echo "W+2 RB3- sect6 (LV block is close to S5) "
./rpc_write 10.176.60.228 26 7 0x20 0x04 $1
./rpc_write 10.176.60.228 26 7 0x40 0x04 $1

echo "W+2 RB1in sect12 (I2C arbitration lost in RPC line, 21.03.2012)"
./rpc_write 10.176.60.228 51 7 0x20 0x05 $1
./rpc_write 10.176.60.228 51 7 0x40 0x05 $1

echo "W+2 RB4- Sector12 BW FEB5 chip2 Vmon1=4995mV (through DT line seems Ok, 27.03.2012)"
./rpc_write 10.176.60.228 54 7 0x40 0x04 $1

echo "W0 RB2 out 1 BW"
./rpc_write 10.176.60.232 2 6 0x20 0x05 $1
./rpc_write 10.176.60.232 2 6 0x40 0x05 $1

echo "W0 RB2 out 7 FW"
./rpc_write 10.176.60.232 29 6 0x20 0x04 $1
./rpc_write 10.176.60.232 29 6 0x40 0x04 $1

echo "W0 RB2 out 8 BW + FW"
./rpc_write 10.176.60.232 34 6 0x20 0x05 $1
./rpc_write 10.176.60.232 34 6 0x40 0x05 $1
./rpc_write 10.176.60.232 34 6 0x20 0x04 $1
./rpc_write 10.176.60.232 34 6 0x40 0x04 $1

#echo "W-1 RB1 in Sect3 Back"
#./rpc_write 10.176.60.234 10 7 0x20 0x05 $1
#./rpc_write 10.176.60.234 10 7 0x40 0x05 $1

#echo "W-1 RB3- Sect9 (RB3- is without exit therefore it holds the DB so it is 0x04) "
#./rpc_write 10.176.60.234 40 7 0x20 0x04 $1
#./rpc_write 10.176.60.234 40 7 0x40 0x04 $1

#echo "W-2 RB1 out sect5 Forw"
#./rpc_write 10.176.60.236 19 6 0x20 0x04 $1  
#./rpc_write 10.176.60.236 19 6 0x40 0x04 $1 
