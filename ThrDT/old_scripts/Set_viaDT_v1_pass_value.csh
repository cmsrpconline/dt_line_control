export  LD_LIBRARY_PATH=LD_LIBRARY_PATH_LD:~/DT/ThrDT/ccbcmds2/lib:~/DT/ThrDT/ccbcmds2/ccbdb
g++ -o rpc_write rpc_write.cc -D'__USE_PTYPES__' -D'__USE_CCBDB__'   -I ccbcmds2/inc -L ccbcmds2/lib -lccbcmds -lpthread -lxml2
g++ -o rpc_read rpc_read.cc  -D'__USE_PTYPES__' -D'__USE_CCBDB__' -I ccbcmds2/inc -L ccbcmds2/lib -lccbcmds -lpthread -lxml2

if ( $1 == "" ) then
echo " You should write also the threshold value (example:  ./Set_viaDT_vpv.csh 230 ) "
exit 0
endif

echo "W+2 RB3 sect6 (RB3+ and RB3- just in case) "
./rpc_write 10.176.60.228 26 7 0x20 0x04 $1
./rpc_write 10.176.60.228 26 7 0x40 0x04 $1
./rpc_write 10.176.60.228 26 7 0x20 0x05 $1
./rpc_write 10.176.60.228 26 7 0x40 0x05 $1

#echo "W+2 RB4 sect8 (RB4+(0x05) and RB4-(0x04) just in case, however not controlled even by DTline) "
#./rpc_write 10.176.60.228 36 7 0x20 0x04 $1
#./rpc_write 10.176.60.228 36 7 0x40 0x04 $1
#./rpc_write 10.176.60.228 36 7 0x20 0x05 $1
#./rpc_write 10.176.60.228 36 7 0x40 0x05 $1

# echo " W+2 RB1out sect11 FW (not controlled even by DT line) "
#./rpc_write 10.176.60.228 47 6 0x20 0x04 $1
#./rpc_write 10.176.60.228 47 6 0x40 0x04 $1

#echo "W+2 RB2out sect6 FW (due to FEB3, Vth2=0/ updated to 22/11/09 it is OK) "
#./rpc_write 10.176.60.228 25 6 0x20 0x04 $1
#./rpc_write 10.176.60.228 25 6 0x40 0x04 $1

echo " W+2 RB1in sect9 BW  "  
./rpc_write 10.176.60.228 37 7 0x20 0x05 $1                                                                                                                                                       
./rpc_write 10.176.60.228 37 7 0x40 0x05 $1

#echo " W+1 RB4- Sect10 FW&BW " (not controlled even by DTline, updated to 22/11/09 BW is OK)
#./rpc_write 10.176.60.230 45 7 0x20 0x04 $1
#./rpc_write 10.176.60.230 45 7 0x40 0x04 $1
#./rpc_write 10.176.60.230 45 7 0x20 0x05 $1
#./rpc_write 10.176.60.230 45 7 0x40 0x05 $1

echo "W0 RB2 out 1 BW"
./rpc_write 10.176.60.232 2 6 0x20 0x05 $1
./rpc_write 10.176.60.232 2 6 0x40 0x05 $1

#echo "W0 RB2 in 11 Centr and BW (not working even through DT line) "
#./rpc_write 10.176.60.232 48 7 0x20 0x05 $1
#./rpc_write 10.176.60.232 48 7 0x40 0x05 $1
#./rpc_write 10.176.60.232 48 7 0x20 0x06 $1
#./rpc_write 10.176.60.232 48 7 0x40 0x06 $1

#echo " W0 RB2 out 12 FW&BW (not controlled even by DTline) "
#./rpc_write 10.176.60.232 52 6 0x20 0x04 250
#./rpc_write 10.176.60.232 52 6 0x40 0x04 250
#./rpc_write 10.176.60.232 52 6 0x20 0x05 250
#./rpc_write 10.176.60.232 52 6 0x40 0x05 250

echo "W0 RB2 out 7 FW"
./rpc_write 10.176.60.232 29 6 0x20 0x04 $1
./rpc_write 10.176.60.232 29 6 0x40 0x04 $1

#echo "W0 RB2 in 3 FW (due to FEB1, Vth2=0, FEB problems not solved via DTline) "
#./rpc_write 10.176.60.232 11 7 0x20 0x04 $1
#./rpc_write 10.176.60.232 11 7 0x40 0x04 $1

echo "W-1 RB1 in Sect3 Back"
./rpc_write 10.176.60.234 10 7 0x20 0x05 $1
./rpc_write 10.176.60.234 10 7 0x40 0x05 $1

echo " W-1 RB2 in Sect4 Centr "
./rpc_write 10.176.60.234 15 7 0x20 0x05 $1
./rpc_write 10.176.60.234 15 7 0x40 0x05 $1

#echo "W-1 RB1 out Sect4 FW (not working even via DT line)"
#./rpc_write 10.176.60.234 14 6 0x20 0x04 $1
#./rpc_write 10.176.60.234 14 6 0x40 0x04 $1

echo "W-1 RB3- Sect9 (RB3+ just in case) "
./rpc_write 10.176.60.234 40 7 0x20 0x04 $1
./rpc_write 10.176.60.234 40 7 0x40 0x04 $1
./rpc_write 10.176.60.234 40 7 0x20 0x05 $1
./rpc_write 10.176.60.234 40 7 0x40 0x05 $1

echo "W-1 RB2 in Sect11 Back "
./rpc_write 10.176.60.234 48 7 0x20 0x06 $1
./rpc_write 10.176.60.234 48 7 0x40 0x06 $1

#echo "W-1 RB3- Sect5 (RB3+ just in case, not working even via DT line) "
#./rpc_write 10.176.60.234 21 7 0x20 0x04 $1
#./rpc_write 10.176.60.234 21 7 0x40 0x04 $1
#./rpc_write 10.176.60.234 21 7 0x20 0x05 $1
#./rpc_write 10.176.60.234 21 7 0x40 0x05 $1

echo "W-2 RB1 out sect5 Forw "
./rpc_write 10.176.60.236 19 6 0x20 0x04 $1  
./rpc_write 10.176.60.236 19 6 0x40 0x04 $1 

echo "W-2 RB2 in Sect 5 Back "
./rpc_write 10.176.60.236 20 7 0x20 0x05 $1
./rpc_write 10.176.60.236 20 7 0x40 0x05 $1

#echo "W-2 RB2 in Sect 2 FW (due to FEB3, single FEB problems are not solved via DT line)"
#./rpc_write 10.176.60.236 6 7 0x20 0x04 $1
#./rpc_write 10.176.60.236 6 7 0x40 0x04 $1

#echo "W-2 RB4++ sect4  (RB4+ just in case) now is OK "
#./rpc_write 10.176.60.236 18 7 0x20 0x04 $1
#./rpc_write 10.176.60.236 18 7 0x40 0x04 $1
#./rpc_write 10.176.60.236 18 7 0x20 0x05 $1
#./rpc_write 10.176.60.236 18 7 0x40 0x05 $1
