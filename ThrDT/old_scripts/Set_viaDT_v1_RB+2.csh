export  LD_LIBRARY_PATH=LD_LIBRARY_PATH_LD:~/DT/ThrDT/ccbcmds2/lib:~/DT/ThrDT/ccbcmds2/ccbdb
g++ -o rpc_write rpc_write.cc -D'__USE_PTYPES__' -D'__USE_CCBDB__'   -I ccbcmds2/inc -L ccbcmds2/lib -lccbcmds -lpthread -lxml2
g++ -o rpc_read rpc_read.cc  -D'__USE_PTYPES__' -D'__USE_CCBDB__' -I ccbcmds2/inc -L ccbcmds2/lib -lccbcmds -lpthread -lxml2

echo "W+2 RB3 sect6 (RB3+ and RB3- just in case) "
./rpc_write 10.176.60.228 26 7 0x20 0x04 220
./rpc_write 10.176.60.228 26 7 0x40 0x04 220
./rpc_write 10.176.60.228 26 7 0x20 0x05 220
./rpc_write 10.176.60.228 26 7 0x40 0x05 220

 echo " Not controlled W+2 RB1out sect11 FW "
#./rpc_write 10.176.60.228 47 6 0x20 0x04 220
#./rpc_write 10.176.60.228 47 6 0x40 0x04 220

echo "W+2 RB2out sect6 FW (due to FEB3, Vth2=0) "
#./rpc_write 10.176.60.228 25 6 0x20 0x04 220
#./rpc_write 10.176.60.228 25 6 0x40 0x04 220
