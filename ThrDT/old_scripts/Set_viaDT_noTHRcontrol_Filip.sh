#! /bin/sh

# Default values
vth_value=220
vmon_value=3500
selected_wheels="01234"
selected_sides="nf"
output_dir=`date +%Y%m%d`
output_file=`date +%H%M%S`

##################################################
## usage                                        ##
##################################################
usage(){
    cat <<EOF

  Usage: ${0##*/} [<option> ...]

  Set selected FEB Thresholds for via DT Line

  Options:
    -t, --vth VALUE             set the VTh value
                                default: 220
    -m, --vmon VALUE            set the VMon value
                                default: 3500
    -w, --wheel WHEEL           select wheel WHEEL
                                default: use all wheels
    -n, --near                  select near side
                                default: use both sides
    -f, --far                   select far side
                                default: use both sides

    -c, --compile               compile before running
    -p, --parallel              handle the wheels parallelly
                                default: one after the other

    -d, --output-dir FOLDER     when combined with parallel, send
                                output to results/FOLDER/FILE-WHEEL.txt
                                defaults to date: YYYYMMDD
    -o, --output-file FILE      when combined with parallel, send
                                output to results/FOLDER/FILE-WHEEL.txt
                                defaults to time: HHMMSS

    -h, --help                  show this message

EOF
}

##################################################
## parse options                                ##
##################################################
args=`getopt -o "t:m:w:nfcpd:o:h" -l "vth:,vmon:,wheel:,near,far,compile,parallel,output-dir:,output-file:,help" -n "${0}" -- "$@"`
declare -a jobs
while [ $# -gt 0 ] ; do
    case "${1}" in
        -t|--vth) shift; vth_value=${1} ; shift ;;
        -m|--vmon) shift; vmon_value=${1} ; shift ;;
        -w|--wheel) shift; let "wheel_idx = ${1} + 2"; selected_wheels=${selected_wheels/${wheel_idx}/s} ; shift ;;
        -n|--near) selected_sides=${selected_sides/n/s} ; shift ;;
        -f|--far) selected_sides=${selected_sides/f/s} ; shift ;;
        -c|--compile) compile=1 ; shift ;;
        -p|--parallel) parallel=1 ; shift ;;
	-d|--output-dir) shift; output_dir=${1} ; shift ;;
	-o|--output-file) shift; output_file=${1} ; shift ;;
        -h|--help) usage; exit 0; shift ;;
        --) shift ;;
        -*) echo "unknown option '${1}'" ; usage; exit 1 ;;
    esac
done

##################################################
## print options                                ##
##################################################
echo "--------------------------------------------------"
echo " VTh  = ${vth_value}"
echo " VMon = ${vmon_value}"

if [ ${selected_wheels} = "01234" ]
    then
    selected_wheels="sssss"
else
    echo -n " Selected wheels:"
    for wheel_idx in `seq 0 4`
      do
      if [ ${selected_wheels:${wheel_idx}:1} = "s" ]
          then
          let "wheel=${wheel_idx} - 2"
	  [ ${wheel} -le 0 ] || wheel="+${wheel}"
          echo -n " W${wheel}"
      fi
    done
    echo ""
fi

if [ ${selected_sides} = "nf" ]
    then
    selected_sides="ss"
else
    echo -n " Selected sides:"
    if [ ${selected_wheels:0:1} = "s" ]
	then
	echo -n " near"
    fi
    if [ ${selected_wheels:1:1} = "s" ]
	then
	echo -n " far"
    fi
    echo ""
fi

##################################################
## environment and possible compilation         ##
##################################################
export LD_LIBRARY_PATH=LD_LIBRARY_PATH_LD:~/DT/ThrDT/ccbcmds2/lib:~/DT/ThrDT/ccbcmds2/ccbdb
if [ ${compile} ]
    then
    echo "Compiling rpc_write"
    g++ -o rpc_write rpc_write.cc -D'__USE_PTYPES__' -D'__USE_CCBDB__'   -I ccbcmds2/inc -L ccbcmds2/lib -lccbcmds -lpthread -lxml2
    g++ -o rpc_read rpc_read.cc  -D'__USE_PTYPES__' -D'__USE_CCBDB__' -I ccbcmds2/inc -L ccbcmds2/lib -lccbcmds -lpthread -lxml2
fi

ccb_ybn2="vmepc-s2g16-06-01"
ccb_ybn1="vmepc-s2g16-07-01"
ccb_yb0="vmepc-s2g16-08-01"
ccb_ybp1="vmepc-s2g16-09-01"
ccb_ybp2="vmepc-s2g16-10-01"

#echo "echo \"do nothing\"" >rpc_write
chmod u+x rpc_write

##################################################
## process the wheels                           ##
##################################################
if [ ${parallel} ]
    then
    for wheel_idx in `seq 0 4`
      do
      mkdir -p results/${output_dir}
      if [ ${selected_wheels:${wheel_idx}:1} = "s" ]
          then
          let "wheel=${wheel_idx} - 2"
	  [ ${wheel} -le 0 ] || wheel="+${wheel}"
	  ./${0} -t ${vth_value} -m ${vmon_value} -w ${wheel} > results/${output_dir}/${output_file}-W${wheel}.txt 2>&1 &
	  echo "use \"tail -F results/${output_dir}/${output_file}-W${wheel}.txt\" to follow the progress for wheel W${wheel}."
      fi
    done
    exit 0
fi

if [ ${selected_wheels:0:1} = "s" ]
    then
    if [ ${selected_sides:0:1} = "s" ]
	then
	echo "--------------------------------------------------"
	echo -e " Processing W-2 Near Side\n--------------------------------------------------" # 1-3, 10-12
##################################################
## W-2 near                                     ##
##################################################

	# W-2/RB1in/11_Backward (error) ; to be tested
	echo "RB1in/W-2/S11:Bwd"
	./rpc_write ${ccb_ybn2} 47 7 0x20 0x05 ${vth_value} ${vmon_value}
	./rpc_write ${ccb_ybn2} 47 7 0x40 0x05 ${vth_value} ${vmon_value}

##################################################
## end W-2 near                                 ##
##################################################
    fi
    if [ ${selected_sides:1:1} = "s" ]
	then
	echo "--------------------------------------------------"
	echo -e " Processing W-2 Far Side\n--------------------------------------------------" # 4-9
##################################################
## W-2 far                                      ##
##################################################

        # W-2/RB2in/2_Forward 2 and 3 (not 0, 1, 4, 5) (disabled) ; to be tested
        # echo "RB2in/W-2/S02:Fwd 2,3"
	# ./rpc_write ${ccb_ybn2} 34 7 0x20 0x04 ${vth_value} ${vmon_value}
	# ./rpc_write ${ccb_ybn2} 34 7 0x40 0x04 ${vth_value} ${vmon_value}

##################################################
## end W-2 far                                  ##
##################################################
    fi
    echo "--------------------------------------------------"
    echo -e " Finished setting thresholds for W-2\n--------------------------------------------------"
fi

if [ ${selected_wheels:1:1} = "s" ]
    then
    if [ ${selected_sides:0:1} = "s" ]
	then
	echo "--------------------------------------------------"
	echo -e " Processing W-1 Near Side\n--------------------------------------------------" # 1-3, 10-12
##################################################
## W-1 near                                     ##
##################################################

##################################################
## end W-1 near                                 ##
##################################################
    fi
    if [ ${selected_sides:1:1} = "s" ]
	then
	echo "--------------------------------------------------"
	echo -e " Processing W-1 Far Side\n--------------------------------------------------" # 4-9
##################################################
## W-1 far                                      ##
##################################################
	
	# W-1/RB1in/8_Backward (error) ; to be tested
	echo "RB1in/W-1/S08:Bwd"
	./rpc_write ${ccb_ybn1} 33 7 0x20 0x05 ${vth_value} ${vmon_value}
	./rpc_write ${ccb_ybn1} 33 7 0x40 0x05 ${vth_value} ${vmon_value}

        # W-1/RB4/6- (disabled) ; to be tested
	echo "RB4/W-1/S06-"
        # verify if it's 0x04 for -, not 0x05
	./rpc_write ${ccb_ybn1} 27 7 0x20 0x04 ${vth_value} ${vmon_value}
	./rpc_write ${ccb_ybn1} 27 7 0x40 0x04 ${vth_value} ${vmon_value}

##################################################
## end W-1 far                                  ##
##################################################
    fi
    echo "--------------------------------------------------"
    echo -e " Finished setting thresholds for W-1\n--------------------------------------------------"
fi

if [ ${selected_wheels:2:1} = "s" ]
    then
    if [ ${selected_sides:0:1} = "s" ]
	then
	echo "--------------------------------------------------"
	echo -e " Processing W0 Near Side\n--------------------------------------------------" # 1-3, 10-12
##################################################
## W0 near                                      ##
##################################################

        #W0/RB2in/3_Forward:1.1#1 (LV OK, check offsets)

##################################################
## end W0 near                                  ##
##################################################
    fi
    if [ ${selected_sides:1:1} = "s" ]
	then
	echo "--------------------------------------------------"
	echo -e " Processing W0 Far Side\n--------------------------------------------------" # 4-9
##################################################
## W0 far                                       ##
##################################################
	
	# W0/RB2in/5_Forward (error)
	echo "RB2in/W0/S05:Fwd"
	./rpc_write ${ccb_yb0} 20 7 0x20 0x04 ${vth_value} ${vmon_value}
	./rpc_write ${ccb_yb0} 20 7 0x40 0x04 ${vth_value} ${vmon_value}

	# W0/RB2out/8 (error)
	echo "RB2out/W0/S08"
	./rpc_write ${ccb_yb0} 34 6 0x20 0x04 ${vth_value} ${vmon_value}
	./rpc_write ${ccb_yb0} 34 6 0x40 0x04 ${vth_value} ${vmon_value}
	./rpc_write ${ccb_yb0} 34 6 0x20 0x05 ${vth_value} ${vmon_value}
	./rpc_write ${ccb_yb0} 34 6 0x40 0x05 ${vth_value} ${vmon_value}

##################################################
## end W0 far                                   ##
##################################################
    fi
    echo "--------------------------------------------------"
    echo -e "Finished setting thresholds for W0\n--------------------------------------------------"
fi

if [ ${selected_wheels:3:1} = "s" ]
    then
    if [ ${selected_sides:0:1} = "s" ]
	then
	echo "--------------------------------------------------"
	echo -e " Processing W+1 Near Side\n--------------------------------------------------" # 1-3, 10-12
##################################################
## W+1 near                                     ##
##################################################

	# W+1/RB3/2- (error) ; to be tested
	echo "RB3/W+1/S02-"
	./rpc_write ${ccb_ybp1} 8 7 0x20 0x04 ${vth_value} ${vmon_value}
	./rpc_write ${ccb_ybp1} 8 7 0x40 0x04 ${vth_value} ${vmon_value}

	# W+1/RB4/10-_Forward (error)
	echo "RB4/W+1/S10-:Fwd"
	./rpc_write ${ccb_ybp1} 45 7 0x20 0x04 ${vth_value} ${vmon_value}
	./rpc_write ${ccb_ybp1} 45 7 0x40 0x04 ${vth_value} ${vmon_value}

##################################################
## end W+1 near                                 ##
##################################################
    fi
    if [ ${selected_sides:1:1} = "s" ]
	then
	echo "--------------------------------------------------"
	echo -e " Processing W+1 Far Side\n--------------------------------------------------" # 4-9
##################################################
## W+1 far                                      ##
##################################################

##################################################
## end W+1 far                                  ##
##################################################
    fi
    echo "--------------------------------------------------"
    echo -e " Finished setting thresholds for W+1\n--------------------------------------------------"
fi

if [ ${selected_wheels:4:1} = "s" ]
    then
    if [ ${selected_sides:0:1} = "s" ]
	then
	echo "--------------------------------------------------"
	echo -e " Processing W+2 Near Side\n--------------------------------------------------" # 1-3, 10-12
##################################################
## W+2 near                                     ##
##################################################

	# W+2/RB3/1 (error)
	echo "RB3/W+2/S01"
	./rpc_write ${ccb_ybp2} 3 7 0x20 0x04 ${vth_value} ${vmon_value}
	./rpc_write ${ccb_ybp2} 3 7 0x40 0x04 ${vth_value} ${vmon_value}
	./rpc_write ${ccb_ybp2} 3 7 0x20 0x05 ${vth_value} ${vmon_value}
	./rpc_write ${ccb_ybp2} 3 7 0x40 0x05 ${vth_value} ${vmon_value}

##################################################
## end W+2 near                                 ##
##################################################
    fi
    if [ ${selected_sides:1:1} = "s" ]
	then
	echo "--------------------------------------------------"
	echo -e " Processing W+2 Far Side\n--------------------------------------------------" # 4-9
##################################################
## W+2 far                                      ##
##################################################

	# W+2/RB2out/8_Backward (error) ; old file says Bwd=0x05?
	echo "RB2out/W+2/S08:Bwd"
	./rpc_write ${ccb_ybp2} 34 6 0x20 0x06 ${vth_value} ${vmon_value}
	./rpc_write ${ccb_ybp2} 34 6 0x40 0x06 ${vth_value} ${vmon_value}

	# W+2/RB3/6 (error)
	echo "RB3/W+2/S06"
	./rpc_write ${ccb_ybp2} 26 7 0x20 0x04 ${vth_value} ${vmon_value}
	./rpc_write ${ccb_ybp2} 26 7 0x40 0x04 ${vth_value} ${vmon_value}
	./rpc_write ${ccb_ybp2} 26 7 0x20 0x05 ${vth_value} ${vmon_value}
	./rpc_write ${ccb_ybp2} 26 7 0x40 0x05 ${vth_value} ${vmon_value}

	# W+2/RB4/8+ (error)
	./rpc_write ${ccb_ybp2} 36 7 0x20 0x04 ${vth_value} ${vmon_value}
	./rpc_write ${ccb_ybp2} 36 7 0x40 0x04 ${vth_value} ${vmon_value}
	./rpc_write ${ccb_ybp2} 36 7 0x20 0x05 ${vth_value} ${vmon_value}
	./rpc_write ${ccb_ybp2} 36 7 0x40 0x05 ${vth_value} ${vmon_value}

##################################################
## end W+2 far                                  ##
##################################################
    fi
    echo "--------------------------------------------------"
    echo -e " Finished setting thresholds for W+2\n--------------------------------------------------"
fi
