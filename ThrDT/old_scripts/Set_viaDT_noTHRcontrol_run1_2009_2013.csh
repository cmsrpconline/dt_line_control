export  LD_LIBRARY_PATH=LD_LIBRARY_PATH_LD:~/DT/ThrDT/ccbcmds2/lib:~/DT/ThrDT/ccbcmds2/ccbdb
g++ -o rpc_write rpc_write.cc -D'__USE_PTYPES__' -D'__USE_CCBDB__'   -I ccbcmds2/inc -L ccbcmds2/lib -lccbcmds -lpthread -lxml2
g++ -o rpc_read rpc_read.cc  -D'__USE_PTYPES__' -D'__USE_CCBDB__' -I ccbcmds2/inc -L ccbcmds2/lib -lccbcmds -lpthread -lxml2

echo "W+2 RB4-- sect4 (LV block is close to S3, NO EXIT) "
./rpc_write 10.176.60.228 17 7 0x20 0x04 $1
./rpc_write 10.176.60.228 17 7 0x40 0x04 $1

echo "W+2 RB3- sect6 (LV block is close to S5) "
./rpc_write 10.176.60.228 26 7 0x20 0x04 $1
./rpc_write 10.176.60.228 26 7 0x40 0x04 $1

echo "W+2 RB1in sect12 (I2C arbitration lost in RPC line, 21.03.2012)"
./rpc_write 10.176.60.228 51 7 0x20 0x05 $1
./rpc_write 10.176.60.228 51 7 0x40 0x05 $1

echo "W+2 RB4- Sector12 BW FEB5 chip2 Vmon1=4995mV (through DT line seems Ok, 27.03.2012)"
./rpc_write 10.176.60.228 54 7 0x40 0x04 $1

echo "W0 RB2 out 1 BW"
./rpc_write 10.176.60.232 2 6 0x20 0x05 $1
./rpc_write 10.176.60.232 2 6 0x40 0x05 $1

echo "W0 RB2 out 7 FW"
./rpc_write 10.176.60.232 29 6 0x20 0x04 $1
./rpc_write 10.176.60.232 29 6 0x40 0x04 $1

echo "W-1 RB1 in Sect3 Back"
./rpc_write 10.176.60.234 10 7 0x20 0x05 $1
./rpc_write 10.176.60.234 10 7 0x40 0x05 $1

echo "W-1 RB1 out 5 FW (I2C arbitration lost in RPC line, 21.03.2012)"
./rpc_write 10.176.60.234 19 6 0x20 0x04 $1
./rpc_write 10.176.60.234 19 6 0x40 0x04 $1

echo "W-1 RB1 in Sect8 Back (I2C arbitration lost in RPC line, 21.03.2012)"
./rpc_write 10.176.60.234 33 7 0x20 0x05 $1
./rpc_write 10.176.60.234 33 7 0x40 0x05 $1

echo "W-1 RB3- Sect9 (RB3- is without exit therefore it holds the DB so it is 0x04) "
./rpc_write 10.176.60.234 40 7 0x20 0x04 $1
./rpc_write 10.176.60.234 40 7 0x40 0x04 $1
#./rpc_write 10.176.60.234 40 7 0x20 0x05 $1
#./rpc_write 10.176.60.234 40 7 0x40 0x05 $1

echo "W-1 RB2 in Sect11 Back "
./rpc_write 10.176.60.234 48 7 0x20 0x06 $1
./rpc_write 10.176.60.234 48 7 0x40 0x06 $1

echo "W-2 RB1 in sect11 BW (I2C arbitration lost in RPC line, 21.03.2012)"
./rpc_write 10.176.60.236 47 7 0x20 0x05 $1
./rpc_write 10.176.60.236 47 7 0x40 0x05 $1

echo "W-2 RB1 out sect5 Forw"
./rpc_write 10.176.60.236 19 6 0x20 0x04 $1  
./rpc_write 10.176.60.236 19 6 0x40 0x04 $1 

#***************************************************************************************************************
# OLD CASS
#***************************************************************************************************************
#echo "W+2 RB1out sect1 BW (01.07.2010 however DT line is not working) "
#./rpc_write 10.176.60.228 1 6 0x20 0x05 $1
#./rpc_write 10.176.60.228 1 6 0x40 0x05 $1

#echo "W+2 RB4+ sect8 (RB4+(0x05) and RB4-(0x04) just in case, however RB4+ not controlled even by DTline. 26.03.2012 RPC and DT lines are both working now. FEC enabled in db) "
#./rpc_write 10.176.60.228 36 7 0x20 0x05 $1
#./rpc_write 10.176.60.228 36 7 0x40 0x05 $1 

# echo " W+2 RB1out sect11 FW (not controlled even by DT line, also the BW is not working via DT. And also the RB1in is not working via DT.) "
#./rpc_write 10.176.60.228 47 6 0x20 0x04 $1
#./rpc_write 10.176.60.228 47 6 0x40 0x04 $1

#echo "W+2/S9/RB1in -first chip 250, second chip 220, however since 10.08.2010 all backward not working in the normal RPC line (DTline is working). Also there is an inversion between RB1in and RB1out, I do not know at which level... Since 10.08.2010 also viaDT line we are not able to set those thresholds. Only readin is working."
#./rpc_write 10.176.60.228 37 7 0x20 0x05 250
#./rpc_write 10.176.60.228 37 7 0x40 0x05 $1

#echo " W+1 RB4- Sect10 FW (not controlled even by DTline (both FW and BW), updated to 22/11/09 BW is OK) "
#./rpc_write 10.176.60.230 45 7 0x20 0x04 $1                                                                                                                                                       
#./rpc_write 10.176.60.230 45 7 0x40 0x04 $1                                                                                                                                                       

#echo "W0 RB2in sect3 FW FEB1 Vth2=0 (chamber masked in FebConfig in order not to enable DAC, here we set only chip1, though to all FEBs...check this out)"                                       
#./rpc_write 10.176.60.232 11 7 0x20 0x04 $1 

#echo "W0 RB2 in sector 5 FW "                                                                                                                                                                     
#./rpc_write 10.176.60.232 20 7 0x20 0x04 $1                                                                                                                                                       
#./rpc_write 10.176.60.232 20 7 0x40 0x04 $1                                                                                                                                                       

#echo "W0 RB2 out 8 FW (not working even through DT line (both FW and BW)) "                                                                                                                       
#./rpc_write 10.176.60.232 34 6 0x20 0x04 $1                                                                                                                                                       
#./rpc_write 10.176.60.232 34 6 0x40 0x04 $1                                                                                                                                                       

#echo "W0 RB2 in 11 Centr and BW (not working even through DT line) "                                                                                                                              
#./rpc_write 10.176.60.232 48 7 0x20 0x05 $1                                                                                                                                                       
#./rpc_write 10.176.60.232 48 7 0x40 0x05 $1                                                                                                                                                       
#./rpc_write 10.176.60.232 48 7 0x20 0x06 $1                                                                                                                                                       
#./rpc_write 10.176.60.232 48 7 0x40 0x06 $1                                                                                                                                                       

#echo " W0 RB2 out 12 FW&BW (not controlled even by DTline) "                                                                                                                                      
#./rpc_write 10.176.60.232 52 6 0x20 0x04 250                                                                                                                                                      
#./rpc_write 10.176.60.232 52 6 0x40 0x04 250                                                                                                                                                      
#./rpc_write 10.176.60.232 52 6 0x20 0x05 250                                                                                                                                                      
#./rpc_write 10.176.60.232 52 6 0x40 0x05 250         

#echo " W-1 RB2 in Sect4 Centr (not working via DT anymore) "                                                                                                                                      
#./rpc_write 10.176.60.234 15 7 0x20 0x05 $1                                                                                                                                                       
#./rpc_write 10.176.60.234 15 7 0x40 0x05 $1                                                                                                                                                       

#echo "W-1 RB1 out Sect4 FW (not working even via DT line, CH switched OFF)"                                                                                                                       
#./rpc_write 10.176.60.234 14 6 0x20 0x04 $1                                                                                                                                                       
#./rpc_write 10.176.60.234 14 6 0x40 0x04 $1 

#echo "W-1 RB3+ in Sector 11 Forward and Backward (04.11.2011), CH Left, DT line not working (15.11.2011)"                                                                                         
#./rpc_write 10.176.60.234 49 7 0x20 0x05 $1                                                                                                                                                       
#./rpc_write 10.176.60.234 49 7 0x40 0x05 $1                                                                                                                                                      

#echo "W-1 RB4+ Sect7 new case 28.Mar.2011 (RB4+ moved to the problematic script)"                                                                                                                
#./rpc_write 10.176.60.234 31 7 0x20 0x04 $1                                                                                                                                                   
#./rpc_write 10.176.60.234 31 7 0x40 0x04 $1                                               

#echo "W-1 RB3- Sect5 (not working even via DT line, LV OFF, disconnected from LV board) "                                                                                                         
#./rpc_write 10.176.60.234 21 7 0x20 0x04 $1                                                                                                                                                       
#./rpc_write 10.176.60.234 21 7 0x40 0x04 $1 

#echo "W-2 RB2 in Sect 5 Back (not working via DTline anymore) "                                                                                                                                   
#./rpc_write 10.176.60.236 20 7 0x20 0x05 $1                                                                                                                                                       
#./rpc_write 10.176.60.236 20 7 0x40 0x05 $1