export  LD_LIBRARY_PATH=LD_LIBRARY_PATH_LD:~/DT/ThrDT/ccbcmds2/lib:~/DT/ThrDT/ccbcmds2/ccbdb
g++ -o rpc_write rpc_write.cc -D'__USE_PTYPES__' -D'__USE_CCBDB__'   -I ccbcmds2/inc -L ccbcmds2/lib -lccbcmds -lpthread -lxml2
g++ -o rpc_read rpc_read.cc  -D'__USE_PTYPES__' -D'__USE_CCBDB__' -I ccbcmds2/inc -L ccbcmds2/lib -lccbcmds -lpthread -lxml2

###############################################################
### Rare Cases or Tests
### Check whether the RPC line is working
###############################################################


##############################################
#### LS-1 Update :: new pcs for DT control ###
##############################################
## YB-2: vmepc-s2g16-06-01 (10.176.10.48)   ##
## YB-1: vmepc-s2g16-07-01 (10.176.10.46)   ##
## YB0: vmepc-s2g16-08-01 (10.176.10.44)    ##
## YB1: vmepc-s2g16-09-01 (10.176.10.42)    ##
## YB2: vmepc-s2g16-10-01 (10.176.10.40)    ##
##############################################


echo "W+2/S3/RB4- forward backward 15 April 2011 (CH Left, LV service close to sector 4, therefore RB4- is 0x05). 11.Feb.2013 thr changed from 235 to 230."
# old ./rpc_write 10.176.60.228 13 7 0x20 0x05 230
# old ./rpc_write 10.176.60.228 13 7 0x40 0x05 230
./rpc_write 10.176.10.40 13 7 0x20 0x05 230
./rpc_write 10.176.10.40 13 7 0x40 0x05 230


echo "W-2/S5/RB4+ Forw/Back, both RPC and DT lines are working. On 11.Feb.2013 thr changed from 240 to 235." 
# old ./rpc_write 10.176.60.236 22 7 0x20 0x04 240
# old ./rpc_write 10.176.60.236 22 7 0x40 0x04 240
./rpc_write 10.176.10.48 22 7 0x20 0x04 240
./rpc_write 10.176.10.46 22 7 0x40 0x04 240

### all old from here ###############################


#echo "W-1 Sector 2 RB4- trying to set higher threshold. RPC line is working, but I am not sure how they are set. Setting threshold via DT line improve the picture on the LB, The chamber is Right with exit. "
#./rpc_write 10.176.60.234 9 7 0x20 0x05 250
#./rpc_write 10.176.60.234 9 7 0x40 0x05 250

#echo "W+2/RB3/1+ Forw&Back 5.11.2011 all chips disabled. Chips 3.5.0, 3.5.1, 3.0.0 and 3.0.1 have I2C arbitration lost. The rest of the chips are ok, but at 250mV, sometimes the I2C arbitration is not stable (appearing and disappearing). Today 27.02.2012 I experienced problem only from FEB5 and FEB3. FEB0 now was reachable. Then when I monitored I received more I2C errors from FEB4 and FEB2. So I disabled all the chips again for RB3+. Now trying to set first 220 to check the performance, before it was 240. If 220 ok move to script1"  
#./rpc_write 10.176.60.228 3 7 0x20 0x04 220
#./rpc_write 10.176.60.228 3 7 0x40 0x04 220

