export  LD_LIBRARY_PATH=LD_LIBRARY_PATH_LD:~/DT/ThrDT/ccbcmds2/lib:~/DT/ThrDT/ccbcmds2/ccbdb
g++ -o rpc_write rpc_write.cc -D'__USE_PTYPES__' -D'__USE_CCBDB__'   -I ccbcmds2/inc -L ccbcmds2/lib -lccbcmds -lpthread -lxml2
g++ -o rpc_read rpc_read.cc  -D'__USE_PTYPES__' -D'__USE_CCBDB__' -I ccbcmds2/inc -L ccbcmds2/lib -lccbcmds -lpthread -lxml2

#***********************************************
#           Rare Cases or Tests/ Check whether the RPC line is working
#***********************************************

echo "W-2 Sector 12 RB2in Backward, 13.04.2011, in the DB the chips are disabled because there is I2C error during writing. 23.03.2012: The I2C in fact is OK, but after moinitoring the values are far off the values in the database. I enabled the chips in the dataabse. Let's monitor again the values"
./rpc_write 10.176.60.236 52 7 0x20 0x05 240
./rpc_write 10.176.60.236 52 7 0x40 0x05 240

echo "W+2/S3/RB4- forward backward 15 April 2011 (CH Left, LV service close to sector 4, therefore RB4- is 0x05"
./rpc_write 10.176.60.228 13 7 0x20 0x05 235
./rpc_write 10.176.60.228 13 7 0x40 0x05 235

echo "W-2/S5/RB4+ Forw/Back, both RPC and DT lines are working" 
./rpc_write 10.176.60.236 22 7 0x20 0x04 240
./rpc_write 10.176.60.236 22 7 0x40 0x04 240

#echo "W-1 Sector 2 RB4- trying to set higher threshold. RPC line is working, but I am not sure how they are set. Setting threshold via DT line improve the picture on the LB, The chamber is Right with exit. "
#./rpc_write 10.176.60.234 9 7 0x20 0x05 250
#./rpc_write 10.176.60.234 9 7 0x40 0x05 250

echo "W+2/RB3/1+ Forw&Back 5.11.2011 all chips disabled. Chips 3.5.0, 3.5.1, 3.0.0 and 3.0.1 have I2C arbitration lost. The rest of the chips are ok, but at 250mV, sometimes the I2C arbitration is not stable (appearing and disappearing). Today 27.02.2012 I experienced problem only from FEB5 and FEB3. FEB0 now was reachable. Then when I monitored I received more I2C errors from FEB4 and FEB2. So I disabled all the chips again for RB3+. Now trying to set first 220 to check the performance, before it was 240. If 220 ok move to script1"  
./rpc_write 10.176.60.228 3 7 0x20 0x04 220
./rpc_write 10.176.60.228 3 7 0x40 0x04 220