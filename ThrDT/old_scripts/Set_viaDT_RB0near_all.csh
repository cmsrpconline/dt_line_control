#!/bin/bash -x
export  LD_LIBRARY_PATH=LD_LIBRARY_PATH_LD:~/DT/ThrDT/ccbcmds2/lib:~/DT/ThrDT/ccbcmds2/ccbdb
#g++ -o rpc_write rpc_write.cc -D'__USE_PTYPES__' -D'__USE_CCBDB__'   -I ccbcmds2/inc -L ccbcmds2/lib -lccbcmds -lpthread -lxml2
#g++ -o rpc_read rpc_read.cc  -D'__USE_PTYPES__' -D'__USE_CCBDB__' -I ccbcmds2/inc -L ccbcmds2/lib -lccbcmds -lpthread -lxml2

#if ( $1 == "" ) then
#echo " You should write also the threshold value (example: ./Set_viaDT_RB0near_all.csh 230 ) "
#exit 0
#endif

### Wheel 0 ####################################
# Positive Sectors:   2,3,    6,7,    10,11,   #
# Negative Sectors: 1,    4,5,    8,9,      12 #
################################################
# Near Sectors:     1,2,3,            10,11,12 #
# Far  Sectors:           4,5,6,7,8,9,         #
################################################

export s10="on"
export s11="on"
export s12="on"
export s01="on"
export s02="on"
export s03="on"

if [ $s10 = "on" ]
then
    echo "W0/S10/RB1in"
    ./rpc_write 10.176.60.232 42 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 42 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 42 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 42 7 0x40 0x05 $1
    
    echo "W0/S10/RB1out"
    ./rpc_write 10.176.60.232 42 6 0x20 0x04 $1
    ./rpc_write 10.176.60.232 42 6 0x40 0x04 $1
    ./rpc_write 10.176.60.232 42 6 0x20 0x05 $1
    ./rpc_write 10.176.60.232 42 6 0x40 0x05 $1
    
    echo "W0/S10/RB2in"
    ./rpc_write 10.176.60.232 43 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 43 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 43 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 43 7 0x40 0x05 $1
    ./rpc_write 10.176.60.232 43 7 0x20 0x06 $1
    ./rpc_write 10.176.60.232 43 7 0x40 0x06 $1
    
    echo "W0/S10/RB2out"
    ./rpc_write 10.176.60.232 43 6 0x20 0x04 $1
    ./rpc_write 10.176.60.232 43 6 0x40 0x04 $1
    ./rpc_write 10.176.60.232 43 6 0x20 0x05 $1
    ./rpc_write 10.176.60.232 43 6 0x40 0x05 $1
    
    echo "W0/S10/RB3"
    ./rpc_write 10.176.60.232 44 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 44 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 44 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 44 7 0x40 0x05 $1
    
    echo "W0/S10/RB4-"
    ./rpc_write 10.176.60.232 45 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 45 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 45 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 45 7 0x40 0x05 $1
    
    echo "W0/S10/RB4+"
    ./rpc_write 10.176.60.232 46 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 46 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 46 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 46 7 0x40 0x05 $1
fi

if [ $s11 = "on" ]
then
    echo "W0/S11/RB1in"
    ./rpc_write 10.176.60.232 47 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 47 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 47 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 47 7 0x40 0x05 $1
    
    echo "W0/S11/RB1out"
    ./rpc_write 10.176.60.232 47 6 0x20 0x04 $1
    ./rpc_write 10.176.60.232 47 6 0x40 0x04 $1
    ./rpc_write 10.176.60.232 47 6 0x20 0x05 $1
    ./rpc_write 10.176.60.232 47 6 0x40 0x05 $1
    
    echo "W0/S11/RB2in"
    ./rpc_write 10.176.60.232 48 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 48 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 48 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 48 7 0x40 0x05 $1
    ./rpc_write 10.176.60.232 48 7 0x20 0x06 $1
    ./rpc_write 10.176.60.232 48 7 0x40 0x06 $1
    
    echo "W0/S11/RB2out"
    ./rpc_write 10.176.60.232 48 6 0x20 0x04 $1
    ./rpc_write 10.176.60.232 48 6 0x40 0x04 $1
    ./rpc_write 10.176.60.232 48 6 0x20 0x05 $1
    ./rpc_write 10.176.60.232 48 6 0x40 0x05 $1
    
    echo "W0/S11/RB3"
    ./rpc_write 10.176.60.232 49 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 49 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 49 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 49 7 0x40 0x05 $1
    
    echo "W0/S11/RB4 (only one RB4 in S11)"
    ./rpc_write 10.176.60.232 50 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 50 7 0x40 0x04 $1
fi

if [ $s12 = "on" ]
then
    echo "W0/S12/RB1in"
    ./rpc_write 10.176.60.232 51 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 51 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 51 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 51 7 0x40 0x05 $1
    
    echo "W0/S12/RB1out"
    ./rpc_write 10.176.60.232 51 6 0x20 0x04 $1
    ./rpc_write 10.176.60.232 51 6 0x40 0x04 $1
    ./rpc_write 10.176.60.232 51 6 0x20 0x05 $1
    ./rpc_write 10.176.60.232 51 6 0x40 0x05 $1
    
    echo "W0/S12/RB2in"
    ./rpc_write 10.176.60.232 52 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 52 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 52 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 52 7 0x40 0x05 $1
    ./rpc_write 10.176.60.232 52 7 0x20 0x06 $1
    ./rpc_write 10.176.60.232 52 7 0x40 0x06 $1
    
    echo "W0/S12/RB2out"
    ./rpc_write 10.176.60.232 52 6 0x20 0x04 $1
    ./rpc_write 10.176.60.232 52 6 0x40 0x04 $1
    ./rpc_write 10.176.60.232 52 6 0x20 0x05 $1
    ./rpc_write 10.176.60.232 52 6 0x40 0x05 $1
    
    echo "W0/S12/RB3"
    ./rpc_write 10.176.60.232 53 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 53 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 53 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 53 7 0x40 0x05 $1
    
    echo "W0/S12/RB4"
    ./rpc_write 10.176.60.232 54 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 54 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 54 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 54 7 0x40 0x05 $1
fi

if [ $s01 = "on" ]
then
    echo "W0/S1/RB1in"
    ./rpc_write 10.176.60.232 1 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 1 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 1 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 1 7 0x40 0x05 $1
    
    echo "W0/S1/RB1out"
    ./rpc_write 10.176.60.232 1 6 0x20 0x04 $1
    ./rpc_write 10.176.60.232 1 6 0x40 0x04 $1
    ./rpc_write 10.176.60.232 1 6 0x20 0x05 $1
    ./rpc_write 10.176.60.232 1 6 0x40 0x05 $1
    
    echo "W0/S1/RB2in"
    ./rpc_write 10.176.60.232 2 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 2 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 2 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 2 7 0x40 0x05 $1
    ./rpc_write 10.176.60.232 2 7 0x20 0x06 $1
    ./rpc_write 10.176.60.232 2 7 0x40 0x06 $1
    
    echo "W0/S1/RB2out"
    ./rpc_write 10.176.60.232 2 6 0x20 0x04 $1
    ./rpc_write 10.176.60.232 2 6 0x40 0x04 $1
    ./rpc_write 10.176.60.232 2 6 0x20 0x05 $1
    ./rpc_write 10.176.60.232 2 6 0x40 0x05 $1
    
    echo "W0/S1/RB3"
    ./rpc_write 10.176.60.232 3 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 3 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 3 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 3 7 0x40 0x05 $1
    
    echo "W0/S1/RB4"
    ./rpc_write 10.176.60.232 4 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 4 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 4 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 4 7 0x40 0x05 $1
fi

if [ $s02 = "on" ]
then
    echo "W0/S2/RB1in"
    ./rpc_write 10.176.60.232 5 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 5 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 5 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 5 7 0x40 0x05 $1
    
    echo "W0/S2/RB1out"
    ./rpc_write 10.176.60.232 5 6 0x20 0x04 $1
    ./rpc_write 10.176.60.232 5 6 0x40 0x04 $1
    ./rpc_write 10.176.60.232 5 6 0x20 0x05 $1
    ./rpc_write 10.176.60.232 5 6 0x40 0x05 $1
    
    echo "W0/S2/RB2in"
    ./rpc_write 10.176.60.232 6 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 6 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 6 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 6 7 0x40 0x05 $1
    ./rpc_write 10.176.60.232 6 7 0x20 0x06 $1
    ./rpc_write 10.176.60.232 6 7 0x40 0x06 $1
    
    echo "W0/S2/RB2out"
    ./rpc_write 10.176.60.232 6 6 0x20 0x04 $1
    ./rpc_write 10.176.60.232 6 6 0x40 0x04 $1
    ./rpc_write 10.176.60.232 6 6 0x20 0x05 $1
    ./rpc_write 10.176.60.232 6 6 0x40 0x05 $1
    
    echo "W0/S2/RB3"
    ./rpc_write 10.176.60.232 8 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 8 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 8 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 8 7 0x40 0x05 $1
    
    echo "W0/S2/RB4"
    ./rpc_write 10.176.60.232 9 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 9 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 9 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 9 7 0x40 0x05 $1
fi

if [ $s03 = "on" ]
then
    echo "W0/S3/RB1in"
    ./rpc_write 10.176.60.232 10 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 10 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 10 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 10 7 0x40 0x05 $1
    
    echo "W0/S3/RB1out"
    ./rpc_write 10.176.60.232 10 6 0x20 0x04 $1
    ./rpc_write 10.176.60.232 10 6 0x40 0x04 $1
    ./rpc_write 10.176.60.232 10 6 0x20 0x05 $1
    ./rpc_write 10.176.60.232 10 6 0x40 0x05 $1
    
    echo "W0/S3/RB2in"
    ./rpc_write 10.176.60.232 11 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 11 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 11 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 11 7 0x40 0x05 $1
    ./rpc_write 10.176.60.232 11 7 0x20 0x06 $1
    ./rpc_write 10.176.60.232 11 7 0x40 0x06 $1
    
    echo "W0/S3/RB2out"
    ./rpc_write 10.176.60.232 11 6 0x20 0x04 $1
    ./rpc_write 10.176.60.232 11 6 0x40 0x04 $1
    ./rpc_write 10.176.60.232 11 6 0x20 0x05 $1
    ./rpc_write 10.176.60.232 11 6 0x40 0x05 $1
    
    echo "W0/S3/RB3"
    ./rpc_write 10.176.60.232 12 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 12 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 12 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 12 7 0x40 0x05 $1
    
    echo "W0/S3/RB4"
    ./rpc_write 10.176.60.232 13 7 0x20 0x04 $1
    ./rpc_write 10.176.60.232 13 7 0x40 0x04 $1
    ./rpc_write 10.176.60.232 13 7 0x20 0x05 $1
    ./rpc_write 10.176.60.232 13 7 0x40 0x05 $1
fi