#!/bin/bash -x 
export  LD_LIBRARY_PATH=LD_LIBRARY_PATH_LD:~/DT/ThrDT/ccbcmds2/lib:~/DT/ThrDT/ccbcmds2/ccbdb
#g++ -o rpc_write rpc_write.cc -D'__USE_PTYPES__' -D'__USE_CCBDB__'   -I ccbcmds2/inc -L ccbcmds2/lib -lccbcmds -lpthread -lxml2
#g++ -o rpc_read rpc_read.cc  -D'__USE_PTYPES__' -D'__USE_CCBDB__' -I ccbcmds2/inc -L ccbcmds2/lib -lccbcmds -lpthread -lxml2

#if ( $1 == "" ) then
#echo " You should write also the threshold value (example: ./Set_viaDT_RB+2near_all.csh 230 ) "
#exit 0
#endif

echo "W+2/S10/RB1in"
./rpc_write 10.176.60.228 42 7 0x20 0x04 $1
./rpc_write 10.176.60.228 42 7 0x40 0x04 $1
./rpc_write 10.176.60.228 42 7 0x20 0x05 $1
./rpc_write 10.176.60.228 42 7 0x40 0x05 $1

echo "W+2/S10/RB1out"
./rpc_write 10.176.60.228 42 6 0x20 0x04 $1
./rpc_write 10.176.60.228 42 6 0x40 0x04 $1
./rpc_write 10.176.60.228 42 6 0x20 0x05 $1
./rpc_write 10.176.60.228 42 6 0x40 0x05 $1

echo "W+2/S10/RB2in"
./rpc_write 10.176.60.228 43 7 0x20 0x04 $1
./rpc_write 10.176.60.228 43 7 0x40 0x04 $1
./rpc_write 10.176.60.228 43 7 0x20 0x05 $1
./rpc_write 10.176.60.228 43 7 0x40 0x05 $1

echo "W+2/S10/RB2out"
./rpc_write 10.176.60.228 43 6 0x20 0x04 $1
./rpc_write 10.176.60.228 43 6 0x40 0x04 $1
./rpc_write 10.176.60.228 43 6 0x20 0x05 $1
./rpc_write 10.176.60.228 43 6 0x40 0x05 $1
./rpc_write 10.176.60.228 43 6 0x20 0x06 $1
./rpc_write 10.176.60.228 43 6 0x40 0x06 $1

echo "W+2/S10/RB3"
./rpc_write 10.176.60.228 44 7 0x20 0x04 $1
./rpc_write 10.176.60.228 44 7 0x40 0x04 $1
./rpc_write 10.176.60.228 44 7 0x20 0x05 $1
./rpc_write 10.176.60.228 44 7 0x40 0x05 $1

echo "W+2/S10/RB4- ??? to be checked"
./rpc_write 10.176.60.228 45 7 0x20 0x04 $1
./rpc_write 10.176.60.228 45 7 0x40 0x04 $1
./rpc_write 10.176.60.228 45 7 0x20 0x05 $1
./rpc_write 10.176.60.228 45 7 0x40 0x05 $1

echo "W+2/S10/RB4+ ??? to be checked"
./rpc_write 10.176.60.228 46 7 0x20 0x04 $1
./rpc_write 10.176.60.228 46 7 0x40 0x04 $1
./rpc_write 10.176.60.228 46 7 0x20 0x05 $1
./rpc_write 10.176.60.228 46 7 0x40 0x05 $1

echo "W+2/S11/RB1in"
./rpc_write 10.176.60.228 47 7 0x20 0x04 $1
./rpc_write 10.176.60.228 47 7 0x40 0x04 $1
./rpc_write 10.176.60.228 47 7 0x20 0x05 $1
./rpc_write 10.176.60.228 47 7 0x40 0x05 $1

echo "W+2/S11/RB1out"
./rpc_write 10.176.60.228 47 6 0x20 0x04 $1
./rpc_write 10.176.60.228 47 6 0x40 0x04 $1
./rpc_write 10.176.60.228 47 6 0x20 0x05 $1
./rpc_write 10.176.60.228 47 6 0x40 0x05 $1

echo "W+2/S11/RB2in"
./rpc_write 10.176.60.228 48 7 0x20 0x04 $1
./rpc_write 10.176.60.228 48 7 0x40 0x04 $1
./rpc_write 10.176.60.228 48 7 0x20 0x05 $1
./rpc_write 10.176.60.228 48 7 0x40 0x05 $1

echo "W+2/S11/RB2out"
./rpc_write 10.176.60.228 48 6 0x20 0x04 $1
./rpc_write 10.176.60.228 48 6 0x40 0x04 $1
./rpc_write 10.176.60.228 48 6 0x20 0x05 $1
./rpc_write 10.176.60.228 48 6 0x40 0x05 $1
./rpc_write 10.176.60.228 48 6 0x20 0x06 $1
./rpc_write 10.176.60.228 48 6 0x40 0x06 $1

echo "W+2/S11/RB3"
./rpc_write 10.176.60.228 49 7 0x20 0x04 $1
./rpc_write 10.176.60.228 49 7 0x40 0x04 $1
./rpc_write 10.176.60.228 49 7 0x20 0x05 $1
./rpc_write 10.176.60.228 49 7 0x40 0x05 $1

echo "W+2/S11/RB4 (only one RB4 in S11)"
./rpc_write 10.176.60.228 50 7 0x20 0x04 $1
./rpc_write 10.176.60.228 50 7 0x40 0x04 $1

echo "W+2/S12/RB1in"
./rpc_write 10.176.60.228 51 7 0x20 0x04 $1
./rpc_write 10.176.60.228 51 7 0x40 0x04 $1
./rpc_write 10.176.60.228 51 7 0x20 0x05 $1
./rpc_write 10.176.60.228 51 7 0x40 0x05 $1

echo "W+2/S12/RB1out"
./rpc_write 10.176.60.228 51 6 0x20 0x04 $1
./rpc_write 10.176.60.228 51 6 0x40 0x04 $1
./rpc_write 10.176.60.228 51 6 0x20 0x05 $1
./rpc_write 10.176.60.228 51 6 0x40 0x05 $1

echo "W+2/S12/RB2in"
./rpc_write 10.176.60.228 52 7 0x20 0x04 $1
./rpc_write 10.176.60.228 52 7 0x40 0x04 $1
./rpc_write 10.176.60.228 52 7 0x20 0x05 $1
./rpc_write 10.176.60.228 52 7 0x40 0x05 $1

echo "W+2/S12/RB2out"
./rpc_write 10.176.60.228 52 6 0x20 0x04 $1
./rpc_write 10.176.60.228 52 6 0x40 0x04 $1
./rpc_write 10.176.60.228 52 6 0x20 0x05 $1
./rpc_write 10.176.60.228 52 6 0x40 0x05 $1
./rpc_write 10.176.60.228 52 6 0x20 0x06 $1
./rpc_write 10.176.60.228 52 6 0x40 0x06 $1

echo "W+2/S12/RB3"
./rpc_write 10.176.60.228 53 7 0x20 0x04 $1
./rpc_write 10.176.60.228 53 7 0x40 0x04 $1
./rpc_write 10.176.60.228 53 7 0x20 0x05 $1
./rpc_write 10.176.60.228 53 7 0x40 0x05 $1

echo "W+2/S12/RB4"
./rpc_write 10.176.60.228 54 7 0x20 0x04 $1
./rpc_write 10.176.60.228 54 7 0x40 0x04 $1
./rpc_write 10.176.60.228 54 7 0x20 0x05 $1
./rpc_write 10.176.60.228 54 7 0x40 0x05 $1

echo "W+2/S1/RB1in"
./rpc_write 10.176.60.228 1 7 0x20 0x04 $1
./rpc_write 10.176.60.228 1 7 0x40 0x04 $1
./rpc_write 10.176.60.228 1 7 0x20 0x05 $1
./rpc_write 10.176.60.228 1 7 0x40 0x05 $1

echo "W+2/S1/RB1out"
./rpc_write 10.176.60.228 1 6 0x20 0x04 $1
./rpc_write 10.176.60.228 1 6 0x40 0x04 $1
./rpc_write 10.176.60.228 1 6 0x20 0x05 $1
./rpc_write 10.176.60.228 1 6 0x40 0x05 $1

echo "W+2/S1/RB2in"
./rpc_write 10.176.60.228 2 7 0x20 0x04 $1
./rpc_write 10.176.60.228 2 7 0x40 0x04 $1
./rpc_write 10.176.60.228 2 7 0x20 0x05 $1
./rpc_write 10.176.60.228 2 7 0x40 0x05 $1

echo "W+2/S1/RB2out"
./rpc_write 10.176.60.228 2 6 0x20 0x04 $1
./rpc_write 10.176.60.228 2 6 0x40 0x04 $1
./rpc_write 10.176.60.228 2 6 0x20 0x05 $1
./rpc_write 10.176.60.228 2 6 0x40 0x05 $1
./rpc_write 10.176.60.228 2 6 0x20 0x06 $1
./rpc_write 10.176.60.228 2 6 0x40 0x06 $1

echo "W+2/S1/RB3"
./rpc_write 10.176.60.228 3 7 0x20 0x04 $1
./rpc_write 10.176.60.228 3 7 0x40 0x04 $1
./rpc_write 10.176.60.228 3 7 0x20 0x05 $1
./rpc_write 10.176.60.228 3 7 0x40 0x05 $1

echo "W+2/S1/RB4"
./rpc_write 10.176.60.228 4 7 0x20 0x04 $1
./rpc_write 10.176.60.228 4 7 0x40 0x04 $1
./rpc_write 10.176.60.228 4 7 0x20 0x05 $1
./rpc_write 10.176.60.228 4 7 0x40 0x05 $1

echo "W+2/S2/RB1in"
./rpc_write 10.176.60.228 5 7 0x20 0x04 $1
./rpc_write 10.176.60.228 5 7 0x40 0x04 $1
./rpc_write 10.176.60.228 5 7 0x20 0x05 $1
./rpc_write 10.176.60.228 5 7 0x40 0x05 $1

echo "W+2/S2/RB1out"
./rpc_write 10.176.60.228 5 6 0x20 0x04 $1
./rpc_write 10.176.60.228 5 6 0x40 0x04 $1
./rpc_write 10.176.60.228 5 6 0x20 0x05 $1
./rpc_write 10.176.60.228 5 6 0x40 0x05 $1

echo "W+2/S2/RB2in"
./rpc_write 10.176.60.228 6 7 0x20 0x04 $1
./rpc_write 10.176.60.228 6 7 0x40 0x04 $1
./rpc_write 10.176.60.228 6 7 0x20 0x05 $1
./rpc_write 10.176.60.228 6 7 0x40 0x05 $1

echo "W+2/S2/RB2out"
./rpc_write 10.176.60.228 6 6 0x20 0x04 $1
./rpc_write 10.176.60.228 6 6 0x40 0x04 $1
./rpc_write 10.176.60.228 6 6 0x20 0x05 $1
./rpc_write 10.176.60.228 6 6 0x40 0x05 $1
./rpc_write 10.176.60.228 6 6 0x20 0x06 $1
./rpc_write 10.176.60.228 6 6 0x40 0x06 $1

echo "W+2/S2/RB3"
./rpc_write 10.176.60.228 8 7 0x20 0x04 $1
./rpc_write 10.176.60.228 8 7 0x40 0x04 $1
./rpc_write 10.176.60.228 8 7 0x20 0x05 $1
./rpc_write 10.176.60.228 8 7 0x40 0x05 $1

echo "W+2/S2/RB4"
./rpc_write 10.176.60.228 9 7 0x20 0x04 $1
./rpc_write 10.176.60.228 9 7 0x40 0x04 $1
./rpc_write 10.176.60.228 9 7 0x20 0x05 $1
./rpc_write 10.176.60.228 9 7 0x40 0x05 $1

echo "W+2/S3/RB1in"
./rpc_write 10.176.60.228 10 7 0x20 0x04 $1
./rpc_write 10.176.60.228 10 7 0x40 0x04 $1
./rpc_write 10.176.60.228 10 7 0x20 0x05 $1
./rpc_write 10.176.60.228 10 7 0x40 0x05 $1

echo "W+2/S3/RB1out"
./rpc_write 10.176.60.228 10 6 0x20 0x04 $1
./rpc_write 10.176.60.228 10 6 0x40 0x04 $1
./rpc_write 10.176.60.228 10 6 0x20 0x05 $1
./rpc_write 10.176.60.228 10 6 0x40 0x05 $1

echo "W+2/S3/RB2in"
./rpc_write 10.176.60.228 11 7 0x20 0x04 $1
./rpc_write 10.176.60.228 11 7 0x40 0x04 $1
./rpc_write 10.176.60.228 11 7 0x20 0x05 $1
./rpc_write 10.176.60.228 11 7 0x40 0x05 $1

echo "W+2/S3/RB2out"
./rpc_write 10.176.60.228 11 6 0x20 0x04 $1
./rpc_write 10.176.60.228 11 6 0x40 0x04 $1
./rpc_write 10.176.60.228 11 6 0x20 0x05 $1
./rpc_write 10.176.60.228 11 6 0x40 0x05 $1
./rpc_write 10.176.60.228 11 6 0x20 0x06 $1
./rpc_write 10.176.60.228 11 6 0x40 0x06 $1

echo "W+2/S3/RB3"
./rpc_write 10.176.60.228 12 7 0x20 0x04 $1
./rpc_write 10.176.60.228 12 7 0x40 0x04 $1
./rpc_write 10.176.60.228 12 7 0x20 0x05 $1
./rpc_write 10.176.60.228 12 7 0x40 0x05 $1

echo "W+2/S3/RB4"
./rpc_write 10.176.60.228 13 7 0x20 0x04 $1
./rpc_write 10.176.60.228 13 7 0x40 0x04 $1
./rpc_write 10.176.60.228 13 7 0x20 0x05 $1
./rpc_write 10.176.60.228 13 7 0x40 0x05 $1
