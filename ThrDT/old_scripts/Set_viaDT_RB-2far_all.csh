#!/bin/bash -x
export  LD_LIBRARY_PATH=LD_LIBRARY_PATH_LD:~/DT/ThrDT/ccbcmds2/lib:~/DT/ThrDT/ccbcmds2/ccbdb
# g++ -o rpc_write rpc_write.cc -D'__USE_PTYPES__' -D'__USE_CCBDB__'   -I ccbcmds2/inc -L ccbcmds2/lib -lccbcmds -lpthread -lxml2
# g++ -o rpc_read rpc_read.cc  -D'__USE_PTYPES__' -D'__USE_CCBDB__' -I ccbcmds2/inc -L ccbcmds2/lib -lccbcmds -lpthread -lxml2

#if ( $1 == "" ) then
#echo " You should write also the threshold value (example:  ./Set_viaDT_RB-2far_all.csh 230 ) "
#exit 0
#endif

#### Change log ############################################
############################################################
# 2014-03-10 :: PC controlling Mini Crates has changed     #
#               only YB-2 changed so far, rest will follow #
#               new IP: 10.176.60.236 --> 10.176.10.16     #
############################################################

export s4="on"
export s5="on"
export s6="on"
export s7="on"
export s8="on"
export s9="on"

export s4rb3rb4="on"
export s5rb3rb4="on"



if [ $s4 = "on" ]
then
    echo "W-2/S4/RB1in"
    ./rpc_write 10.176.10.16 14 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 14 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 14 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 14 7 0x40 0x05 $1

    echo "W-2/S4/RB1out"
    ./rpc_write 10.176.10.16 14 6 0x20 0x04 $1
    ./rpc_write 10.176.10.16 14 6 0x40 0x04 $1
    ./rpc_write 10.176.10.16 14 6 0x20 0x05 $1
    ./rpc_write 10.176.10.16 14 6 0x40 0x05 $1
    
    echo "W-2/S4/RB2in"
    ./rpc_write 10.176.10.16 15 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 15 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 15 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 15 7 0x40 0x05 $1
    
    echo "W-2/S4/RB2out"
    ./rpc_write 10.176.10.16 15 6 0x20 0x04 $1
    ./rpc_write 10.176.10.16 15 6 0x40 0x04 $1
    ./rpc_write 10.176.10.16 15 6 0x20 0x05 $1
    ./rpc_write 10.176.10.16 15 6 0x40 0x05 $1
    ./rpc_write 10.176.10.16 15 6 0x20 0x06 $1
    ./rpc_write 10.176.10.16 15 6 0x40 0x06 $1
fi 
if [ $s4rb3rb4 = "on" ]
then
    echo "W-2/S4/RB3"
    ./rpc_write 10.176.10.16 16 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 16 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 16 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 16 7 0x40 0x05 $1
    
    echo "W-2/S4/RB4 (RB4-- and RB4-)"
    ./rpc_write 10.176.10.16 17 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 17 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 17 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 17 7 0x40 0x05 $1
    
    echo "W-2/S4/RB4 (RB4+ and RB4++)"
    ./rpc_write 10.176.10.16 18 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 18 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 18 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 18 7 0x40 0x05 $1
fi

if [ $s5 = "on" ]
then
    echo "W-2/S5/RB1in"
    ./rpc_write 10.176.10.16 19 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 19 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 19 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 19 7 0x40 0x05 $1
    
    echo "W-2/S5/RB1out"
    ./rpc_write 10.176.10.16 19 6 0x20 0x04 $1
    ./rpc_write 10.176.10.16 19 6 0x40 0x04 $1
    ./rpc_write 10.176.10.16 19 6 0x20 0x05 $1
    ./rpc_write 10.176.10.16 19 6 0x40 0x05 $1
    
    echo "W-2/S5/RB2in"
    ./rpc_write 10.176.10.16 20 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 20 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 20 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 20 7 0x40 0x05 $1
    
    echo "W-2/S5/RB2out"
    ./rpc_write 10.176.10.16 20 6 0x20 0x04 $1
    ./rpc_write 10.176.10.16 20 6 0x40 0x04 $1
    ./rpc_write 10.176.10.16 20 6 0x20 0x05 $1
    ./rpc_write 10.176.10.16 20 6 0x40 0x05 $1
    ./rpc_write 10.176.10.16 20 6 0x20 0x06 $1
    ./rpc_write 10.176.10.16 20 6 0x40 0x06 $1
fi 
if [ $s5rb3rb4 = "on" ]
then    
    echo "W-2/S5/RB3"
    ./rpc_write 10.176.10.16 21 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 21 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 21 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 21 7 0x40 0x05 $1
    
    echo "W-2/S5/RB4"
    ./rpc_write 10.176.10.16 22 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 22 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 22 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 22 7 0x40 0x05 $1
fi

if [ $s6 = "on" ]
then
    echo "W-2/S6/RB1in"
    ./rpc_write 10.176.10.16 7 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 7 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 7 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 7 7 0x40 0x05 $1
    
    echo "W-2/S6/RB1out"
    ./rpc_write 10.176.10.16 7 6 0x20 0x04 $1
    ./rpc_write 10.176.10.16 7 6 0x40 0x04 $1
    ./rpc_write 10.176.10.16 7 6 0x20 0x05 $1
    ./rpc_write 10.176.10.16 7 6 0x40 0x05 $1
    
    echo "W-2/S6/RB2in"
    ./rpc_write 10.176.10.16 23 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 23 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 23 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 23 7 0x40 0x05 $1
    
    echo "W-2/S6/RB2out"
    ./rpc_write 10.176.10.16 23 6 0x20 0x04 $1
    ./rpc_write 10.176.10.16 23 6 0x40 0x04 $1
    ./rpc_write 10.176.10.16 23 6 0x20 0x05 $1
    ./rpc_write 10.176.10.16 23 6 0x40 0x05 $1
    ./rpc_write 10.176.10.16 23 6 0x20 0x06 $1
    ./rpc_write 10.176.10.16 23 6 0x40 0x06 $1
    
    echo "W-2/S6/RB3"
    ./rpc_write 10.176.10.16 39 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 39 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 39 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 39 7 0x40 0x05 $1
    
    echo "W-2/S6/RB4"
    ./rpc_write 10.176.10.16 55 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 55 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 55 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 55 7 0x40 0x05 $1
fi

if [ $s7 = "on" ]
then    
    echo "W-2/S7/RB1in"
    ./rpc_write 10.176.10.16 28 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 28 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 28 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 28 7 0x40 0x05 $1
    
    echo "W-2/S7/RB1out"
    ./rpc_write 10.176.10.16 28 6 0x20 0x04 $1
    ./rpc_write 10.176.10.16 28 6 0x40 0x04 $1
    ./rpc_write 10.176.10.16 28 6 0x20 0x05 $1
    ./rpc_write 10.176.10.16 28 6 0x40 0x05 $1
    
    echo "W-2/S7/RB2in"
    ./rpc_write 10.176.10.16 29 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 29 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 29 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 29 7 0x40 0x05 $1
    
    echo "W-2/S7/RB2out"
    ./rpc_write 10.176.10.16 29 6 0x20 0x04 $1
    ./rpc_write 10.176.10.16 29 6 0x40 0x04 $1
    ./rpc_write 10.176.10.16 29 6 0x20 0x05 $1
    ./rpc_write 10.176.10.16 29 6 0x40 0x05 $1
    ./rpc_write 10.176.10.16 29 6 0x20 0x06 $1
    ./rpc_write 10.176.10.16 29 6 0x40 0x06 $1
    
    echo "W-2/S7/RB3"
    ./rpc_write 10.176.10.16 30 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 30 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 30 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 30 7 0x40 0x05 $1
    
    echo "W-2/S7/RB4"
    ./rpc_write 10.176.10.16 31 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 31 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 31 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 31 7 0x40 0x05 $1
fi

if [ $s8 = "on" ]
then
    echo "W-2/S8/RB1in"
    ./rpc_write 10.176.10.16 33 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 33 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 33 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 33 7 0x40 0x05 $1
    
    echo "W-2/S8/RB1out"
    ./rpc_write 10.176.10.16 33 6 0x20 0x04 $1
    ./rpc_write 10.176.10.16 33 6 0x40 0x04 $1
    ./rpc_write 10.176.10.16 33 6 0x20 0x05 $1
    ./rpc_write 10.176.10.16 33 6 0x40 0x05 $1
    
    echo "W-2/S8/RB2in"
    ./rpc_write 10.176.10.16 34 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 34 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 34 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 34 7 0x40 0x05 $1
    
    echo "W-2/S8/RB2out"
    ./rpc_write 10.176.10.16 34 6 0x20 0x04 $1
    ./rpc_write 10.176.10.16 34 6 0x40 0x04 $1
    ./rpc_write 10.176.10.16 34 6 0x20 0x05 $1
    ./rpc_write 10.176.10.16 34 6 0x40 0x05 $1
    ./rpc_write 10.176.10.16 34 6 0x20 0x06 $1
    ./rpc_write 10.176.10.16 34 6 0x40 0x06 $1
    
    echo "W-2/S8/RB3"
    ./rpc_write 10.176.10.16 35 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 35 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 35 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 35 7 0x40 0x05 $1
    
    echo "W-2/S8/RB4"
    ./rpc_write 10.176.10.16 36 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 36 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 36 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 36 7 0x40 0x05 $1
fi

if [ $s9 = "on" ]
then
    echo "W-2/S9/RB1in"
    ./rpc_write 10.176.10.16 37 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 37 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 37 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 37 7 0x40 0x05 $1
    
    echo "W-2/S9/RB1out"
    ./rpc_write 10.176.10.16 37 6 0x20 0x04 $1
    ./rpc_write 10.176.10.16 37 6 0x40 0x04 $1
    ./rpc_write 10.176.10.16 37 6 0x20 0x05 $1
    ./rpc_write 10.176.10.16 37 6 0x40 0x05 $1
    
    echo "W-2/S9/RB2in"
    ./rpc_write 10.176.10.16 38 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 38 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 38 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 38 7 0x40 0x05 $1
    
    echo "W-2/S9/RB2out"
    ./rpc_write 10.176.10.16 38 6 0x20 0x04 $1
    ./rpc_write 10.176.10.16 38 6 0x40 0x04 $1
    ./rpc_write 10.176.10.16 38 6 0x20 0x05 $1
    ./rpc_write 10.176.10.16 38 6 0x40 0x05 $1
    ./rpc_write 10.176.10.16 38 6 0x20 0x06 $1
    ./rpc_write 10.176.10.16 38 6 0x40 0x06 $1
    
    echo "W-2/S9/RB3"
    ./rpc_write 10.176.10.16 40 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 40 7 0x40 0x04 $1
    ./rpc_write 10.176.10.16 40 7 0x20 0x05 $1
    ./rpc_write 10.176.10.16 40 7 0x40 0x05 $1
    
    echo "W-2/S9/RB4 (only one RB4 in S9)"
    ./rpc_write 10.176.10.16 41 7 0x20 0x04 $1
    ./rpc_write 10.176.10.16 41 7 0x40 0x04 $1
fi