#! /bin/sh

vth_value=$1
if ([ -z "$vth_value" ]) ; then
    vth_value=220
fi

vmon_value=$2
if ([ -z "$vmon_value" ]) ; then
    vmon_value=3500
fi

correct=$3
if ([ -z "correct" ]) ; then
    correct=1
fi

# The threshold are set via the CCB servers of the DT
# W-2 CCB SERVER Ip address 10.176.10.48
# W-1 CCB SERVER Ip address 10.176.10.46
# W0  CCB SERVER Ip address 10.176.10.44
# W+1 CCB SERVER Ip address 10.176.10.42
# W+2 CCB SERVER Ip address 10.176.10.40

##############################################
#### LS-1 Update :: new pcs for DT control ###
##############################################
## YB-2: vmepc-s2g16-06-01 (10.176.10.48)   ##
## YB-1: vmepc-s2g16-07-01 (10.176.10.46)   ##
## YB0: vmepc-s2g16-08-01 (10.176.10.44)    ##
## YB1: vmepc-s2g16-09-01 (10.176.10.42)    ##
## YB2: vmepc-s2g16-10-01 (10.176.10.40)    ##
##############################################  

set echo on

export  LD_LIBRARY_PATH=LD_LIBRARY_PATH_LD:~/DT/ThrDT/ccbcmds2/lib:~/DT/ThrDT/ccbcmds2/ccbdb
g++ -o rpc_write rpc_write.cc -D'__USE_PTYPES__' -D'__USE_CCBDB__'   -I ccbcmds2/inc -L ccbcmds2/lib -lccbcmds -lpthread -lxml2
g++ -o rpc_read rpc_read.cc  -D'__USE_PTYPES__' -D'__USE_CCBDB__' -I ccbcmds2/inc -L ccbcmds2/lib -lccbcmds -lpthread -lxml2

echo "${vth_value} ${vmon_value} ${correct}"
echo "----------------------------------------------------"
echo "W+2 RB2out sect 3 BW (since 04.Dec.2015) "
echo "----------------------------------------------------"
./rpc_write 10.176.10.40 11 6 0x20 0x06 $vth_value $vmon_value $correct
./rpc_write 10.176.10.40 11 6 0x40 0x06 $vth_value $vmon_value $correct
echo "----------------------------------------------------"
echo ""
echo "W+2 RB3- sect 6 (LV block is close to S5) "
echo "----------------------------------------------------"
./rpc_write 10.176.10.40 26 7 0x20 0x04 $vth_value $vmon_value $correct
./rpc_write 10.176.10.40 26 7 0x40 0x04 $vth_value $vmon_value $correct
./rpc_write 10.176.10.40 26 7 0x20 0x05 $vth_value $vmon_value $correct
./rpc_write 10.176.10.40 26 7 0x40 0x05 $vth_value $vmon_value $correct
#echo "----------------------------------------------------"
#echo "W+2 RB4+ sect 8 FW + BW (LV block is close to 7, Chamber disconnected from the gas service on 02.Feb.2016)"
#./rpc_write 10.176.10.40 36 7 0x20 0x04 $vth_value $vmon_value $correct
#./rpc_write 10.176.10.40 36 7 0x40 0x04 $vth_value $vmon_value $correct
#./rpc_write 10.176.10.40 36 7 0x20 0x05 $vth_value $vmon_value $correct
#./rpc_write 10.176.10.40 36 7 0x40 0x05 $vth_value $vmon_value $correct
#echo "----------------------------------------------------"
echo ""
echo ""
echo "W+1 RB4- sect 10 FW (DT line OK) "
echo "----------------------------------------------------"
./rpc_write 10.176.10.42 45 7 0x20 0x04 $vth_value $vmon_value $correct
./rpc_write 10.176.10.42 45 7 0x40 0x04 $vth_value $vmon_value $correct
echo "----------------------------------------------------"
echo ""
#echo "W+1 RB2in sect 4 BW - DT line not working"
# echo "----------------------------------------------------"
# ./rpc_write 10.176.10.42 23 7 0x20 0x06 $vth_value $vmon_value 0
# ./rpc_write 10.176.10.42 23 7 0x40 0x06 $vth_value $vmon_value 0
# echo "----------------------------------------------------"
echo ""
#echo "W0 RB2 in sect 5 FW - DT line not working "
# echo "----------------------------------------------------"
# ./rpc_write 10.176.10.44 20 7 0x20 0x04 $vth_value $vmon_value 0
# ./rpc_write 10.176.10.44 20 7 0x40 0x04 $vth_value $vmon_value 0
# echo "----------------------------------------------------"
echo ""
echo "W0 RB4- sect 7 FW & BW (check which of those is RB4-)"
echo "----------------------------------------------------"
./rpc_write 10.176.10.44 31 7 0x20 0x04 $vth_value $vmon_value $correct
./rpc_write 10.176.10.44 31 7 0x20 0x04 $vth_value $vmon_value $correct
./rpc_write 10.176.10.44 31 7 0x20 0x05 $vth_value $vmon_value $correct
./rpc_write 10.176.10.44 31 7 0x40 0x05 $vth_value $vmon_value $correct
echo "----------------------------------------------------"
echo ""
echo "W0 RB2 out sect 8 BW "
echo "----------------------------------------------------"
./rpc_write 10.176.10.44 34 6 0x20 0x05 $vth_value $vmon_value $correct
./rpc_write 10.176.10.44 34 6 0x40 0x05 $vth_value $vmon_value $correct
echo "----------------------------------------------------"
echo "W0 RB3- sect 11 FW only FEB2 with I2C error "
echo "----------------------------------------------------"
./rpc_write 10.176.10.44 49 7 0x20 0x04 300 3500
./rpc_write 10.176.10.44 49 7 0x40 0x04 300 3500
echo "----------------------------------------------------"
echo ""
echo "W-1 RB1 in sect 8 BW "
echo "----------------------------------------------------"
./rpc_write 10.176.10.46 33 7 0x20 0x05 $vth_value $vmon_value $correct
./rpc_write 10.176.10.46 33 7 0x40 0x05 $vth_value $vmon_value $correct
echo "----------------------------------------------------"
echo ""
echo "W-1 RB3+ sect 11 BW (reconfigure in DB, check which of those is RB3+)"
echo "----------------------------------------------------"
./rpc_write 10.176.10.46 47 7 0x20 0x04 $vth_value $vmon_value $correct
./rpc_write 10.176.10.46 47 7 0x40 0x04 $vth_value $vmon_value $correct
./rpc_write 10.176.10.46 47 7 0x20 0x05 $vth_value $vmon_value $correct
./rpc_write 10.176.10.46 47 7 0x40 0x05 $vth_value $vmon_value $correct
echo "----------------------------------------------------"
echo ""
echo ""
echo "W-2 RB1 in sect 11 BW "
echo "----------------------------------------------------"
./rpc_write 10.176.10.48 47 7 0x20 0x05 $vth_value $vmon_value $correct
./rpc_write 10.176.10.48 47 7 0x40 0x05 $vth_value $vmon_value $correct
echo "----------------------------------------------------"
echo ""
