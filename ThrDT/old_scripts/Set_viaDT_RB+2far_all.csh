#!/bin/bash -x
export  LD_LIBRARY_PATH=LD_LIBRARY_PATH_LD:~/DT/ThrDT/ccbcmds2/lib:~/DT/ThrDT/ccbcmds2/ccbdb
#g++ -o rpc_write rpc_write.cc -D'__USE_PTYPES__' -D'__USE_CCBDB__'   -I ccbcmds2/inc -L ccbcmds2/lib -lccbcmds -lpthread -lxml2
#g++ -o rpc_read rpc_read.cc  -D'__USE_PTYPES__' -D'__USE_CCBDB__' -I ccbcmds2/inc -L ccbcmds2/lib -lccbcmds -lpthread -lxml2

#if ( $1 == "" ) then
#echo " You should write also the threshold value (example:  ./Set_viaDT_RB+2far_all.csh 230 ) "
#exit 0
#endif

echo "W+2/S4/RB1in"
./rpc_write 10.176.60.228 14 7 0x20 0x04 $1
./rpc_write 10.176.60.228 14 7 0x40 0x04 $1
./rpc_write 10.176.60.228 14 7 0x20 0x05 $1
./rpc_write 10.176.60.228 14 7 0x40 0x05 $1

echo "W+2/S4/RB1out"
./rpc_write 10.176.60.228 14 6 0x20 0x04 $1
./rpc_write 10.176.60.228 14 6 0x40 0x04 $1
./rpc_write 10.176.60.228 14 6 0x20 0x05 $1
./rpc_write 10.176.60.228 14 6 0x40 0x05 $1

echo "W+2/S4/RB2in"
./rpc_write 10.176.60.228 15 7 0x20 0x04 $1
./rpc_write 10.176.60.228 15 7 0x40 0x04 $1
./rpc_write 10.176.60.228 15 7 0x20 0x05 $1
./rpc_write 10.176.60.228 15 7 0x40 0x05 $1

echo "W+2/S4/RB2out"
./rpc_write 10.176.60.228 15 6 0x20 0x04 $1
./rpc_write 10.176.60.228 15 6 0x40 0x04 $1
./rpc_write 10.176.60.228 15 6 0x20 0x05 $1
./rpc_write 10.176.60.228 15 6 0x40 0x05 $1
./rpc_write 10.176.60.228 15 6 0x20 0x06 $1
./rpc_write 10.176.60.228 15 6 0x40 0x06 $1

echo "W+2/S4/RB3"
./rpc_write 10.176.60.228 16 7 0x20 0x04 $1
./rpc_write 10.176.60.228 16 7 0x40 0x04 $1
./rpc_write 10.176.60.228 16 7 0x20 0x05 $1
./rpc_write 10.176.60.228 16 7 0x40 0x05 $1

echo "W+2/S4/RB4 (RB4-- and RB4-)"
./rpc_write 10.176.60.228 17 7 0x20 0x04 $1
./rpc_write 10.176.60.228 17 7 0x40 0x04 $1
./rpc_write 10.176.60.228 17 7 0x20 0x05 $1
./rpc_write 10.176.60.228 17 7 0x40 0x05 $1

echo "W+2/S4/RB4 (RB4+ and RB4++)"
./rpc_write 10.176.60.228 18 7 0x20 0x04 $1
./rpc_write 10.176.60.228 18 7 0x40 0x04 $1
./rpc_write 10.176.60.228 18 7 0x20 0x05 $1
./rpc_write 10.176.60.228 18 7 0x40 0x05 $1

echo "W+2/S5/RB1in"
./rpc_write 10.176.60.228 19 7 0x20 0x04 $1
./rpc_write 10.176.60.228 19 7 0x40 0x04 $1
./rpc_write 10.176.60.228 19 7 0x20 0x05 $1
./rpc_write 10.176.60.228 19 7 0x40 0x05 $1

echo "W+2/S5/RB1out"
./rpc_write 10.176.60.228 19 6 0x20 0x04 $1
./rpc_write 10.176.60.228 19 6 0x40 0x04 $1
./rpc_write 10.176.60.228 19 6 0x20 0x05 $1
./rpc_write 10.176.60.228 19 6 0x40 0x05 $1

echo "W+2/S5/RB2in"
./rpc_write 10.176.60.228 20 7 0x20 0x04 $1
./rpc_write 10.176.60.228 20 7 0x40 0x04 $1
./rpc_write 10.176.60.228 20 7 0x20 0x05 $1
./rpc_write 10.176.60.228 20 7 0x40 0x05 $1

echo "W+2/S5/RB2out"
./rpc_write 10.176.60.228 20 6 0x20 0x04 $1
./rpc_write 10.176.60.228 20 6 0x40 0x04 $1
./rpc_write 10.176.60.228 20 6 0x20 0x05 $1
./rpc_write 10.176.60.228 20 6 0x40 0x05 $1
./rpc_write 10.176.60.228 20 6 0x20 0x06 $1
./rpc_write 10.176.60.228 20 6 0x40 0x06 $1

echo "W+2/S5/RB3"
./rpc_write 10.176.60.228 21 7 0x20 0x04 $1
./rpc_write 10.176.60.228 21 7 0x40 0x04 $1
./rpc_write 10.176.60.228 21 7 0x20 0x05 $1
./rpc_write 10.176.60.228 21 7 0x40 0x05 $1

echo "W+2/S5/RB4"
./rpc_write 10.176.60.228 22 7 0x20 0x04 $1
./rpc_write 10.176.60.228 22 7 0x40 0x04 $1
./rpc_write 10.176.60.228 22 7 0x20 0x05 $1
./rpc_write 10.176.60.228 22 7 0x40 0x05 $1

echo "W+2/S6/RB1in"
./rpc_write 10.176.60.228 24 7 0x20 0x04 $1
./rpc_write 10.176.60.228 24 7 0x40 0x04 $1
./rpc_write 10.176.60.228 24 7 0x20 0x05 $1
./rpc_write 10.176.60.228 24 7 0x40 0x05 $1

echo "W+2/S6/RB1out"
./rpc_write 10.176.60.228 24 6 0x20 0x04 $1
./rpc_write 10.176.60.228 24 6 0x40 0x04 $1
./rpc_write 10.176.60.228 24 6 0x20 0x05 $1
./rpc_write 10.176.60.228 24 6 0x40 0x05 $1

echo "W+2/S6/RB2in"
./rpc_write 10.176.60.228 25 7 0x20 0x04 $1
./rpc_write 10.176.60.228 25 7 0x40 0x04 $1
./rpc_write 10.176.60.228 25 7 0x20 0x05 $1
./rpc_write 10.176.60.228 25 7 0x40 0x05 $1

echo "W+2/S6/RB2out"
./rpc_write 10.176.60.228 25 6 0x20 0x04 $1
./rpc_write 10.176.60.228 25 6 0x40 0x04 $1
./rpc_write 10.176.60.228 25 6 0x20 0x05 $1
./rpc_write 10.176.60.228 25 6 0x40 0x05 $1
./rpc_write 10.176.60.228 25 6 0x20 0x06 $1
./rpc_write 10.176.60.228 25 6 0x40 0x06 $1

echo "W+2/S6/RB3"
./rpc_write 10.176.60.228 26 7 0x20 0x04 $1
./rpc_write 10.176.60.228 26 7 0x40 0x04 $1
./rpc_write 10.176.60.228 26 7 0x20 0x05 $1
./rpc_write 10.176.60.228 26 7 0x40 0x05 $1

echo "W+2/S6/RB4"
./rpc_write 10.176.60.228 27 7 0x20 0x04 $1
./rpc_write 10.176.60.228 27 7 0x40 0x04 $1
./rpc_write 10.176.60.228 27 7 0x20 0x05 $1
./rpc_write 10.176.60.228 27 7 0x40 0x05 $1

echo "W+2/S7/RB1in"
./rpc_write 10.176.60.228 28 7 0x20 0x04 $1
./rpc_write 10.176.60.228 28 7 0x40 0x04 $1
./rpc_write 10.176.60.228 28 7 0x20 0x05 $1
./rpc_write 10.176.60.228 28 7 0x40 0x05 $1

echo "W+2/S7/RB1out"
./rpc_write 10.176.60.228 28 6 0x20 0x04 $1
./rpc_write 10.176.60.228 28 6 0x40 0x04 $1
./rpc_write 10.176.60.228 28 6 0x20 0x05 $1
./rpc_write 10.176.60.228 28 6 0x40 0x05 $1

echo "W+2/S7/RB2in"
./rpc_write 10.176.60.228 29 7 0x20 0x04 $1
./rpc_write 10.176.60.228 29 7 0x40 0x04 $1
./rpc_write 10.176.60.228 29 7 0x20 0x05 $1
./rpc_write 10.176.60.228 29 7 0x40 0x05 $1

echo "W+2/S7/RB2out"
./rpc_write 10.176.60.228 29 6 0x20 0x04 $1
./rpc_write 10.176.60.228 29 6 0x40 0x04 $1
./rpc_write 10.176.60.228 29 6 0x20 0x05 $1
./rpc_write 10.176.60.228 29 6 0x40 0x05 $1
./rpc_write 10.176.60.228 29 6 0x20 0x06 $1
./rpc_write 10.176.60.228 29 6 0x40 0x06 $1

echo "W+2/S7/RB3"
./rpc_write 10.176.60.228 30 7 0x20 0x04 $1
./rpc_write 10.176.60.228 30 7 0x40 0x04 $1
./rpc_write 10.176.60.228 30 7 0x20 0x05 $1
./rpc_write 10.176.60.228 30 7 0x40 0x05 $1

echo "W+2/S7/RB4"
./rpc_write 10.176.60.228 31 7 0x20 0x04 $1
./rpc_write 10.176.60.228 31 7 0x40 0x04 $1
./rpc_write 10.176.60.228 31 7 0x20 0x05 $1
./rpc_write 10.176.60.228 31 7 0x40 0x05 $1

echo "W+2/S8/RB1in"
./rpc_write 10.176.60.228 33 7 0x20 0x04 $1
./rpc_write 10.176.60.228 33 7 0x40 0x04 $1
./rpc_write 10.176.60.228 33 7 0x20 0x05 $1
./rpc_write 10.176.60.228 33 7 0x40 0x05 $1

echo "W+2/S8/RB1out"
./rpc_write 10.176.60.228 33 6 0x20 0x04 $1
./rpc_write 10.176.60.228 33 6 0x40 0x04 $1
./rpc_write 10.176.60.228 33 6 0x20 0x05 $1
./rpc_write 10.176.60.228 33 6 0x40 0x05 $1

echo "W+2/S8/RB2in"
./rpc_write 10.176.60.228 34 7 0x20 0x04 $1
./rpc_write 10.176.60.228 34 7 0x40 0x04 $1
./rpc_write 10.176.60.228 34 7 0x20 0x05 $1
./rpc_write 10.176.60.228 34 7 0x40 0x05 $1

echo "W+2/S8/RB2out"
./rpc_write 10.176.60.228 34 6 0x20 0x04 $1
./rpc_write 10.176.60.228 34 6 0x40 0x04 $1
./rpc_write 10.176.60.228 34 6 0x20 0x05 $1
./rpc_write 10.176.60.228 34 6 0x40 0x05 $1
./rpc_write 10.176.60.228 34 6 0x20 0x06 $1
./rpc_write 10.176.60.228 34 6 0x40 0x06 $1

echo "W+2/S8/RB3"
./rpc_write 10.176.60.228 35 7 0x20 0x04 $1
./rpc_write 10.176.60.228 35 7 0x40 0x04 $1
./rpc_write 10.176.60.228 35 7 0x20 0x05 $1
./rpc_write 10.176.60.228 35 7 0x40 0x05 $1

echo "W+2/S8/RB4"
./rpc_write 10.176.60.228 36 7 0x20 0x04 $1
./rpc_write 10.176.60.228 36 7 0x40 0x04 $1
./rpc_write 10.176.60.228 36 7 0x20 0x05 $1
./rpc_write 10.176.60.228 36 7 0x40 0x05 $1

echo "W+2/S9/RB1in" ---> this is RB1out
./rpc_write 10.176.60.228 37 7 0x20 0x04 $1
./rpc_write 10.176.60.228 37 7 0x40 0x04 $1
./rpc_write 10.176.60.228 37 7 0x20 0x05 $1
./rpc_write 10.176.60.228 37 7 0x40 0x05 $1

echo "W+2/S9/RB1out"---> this is RB1in
./rpc_write 10.176.60.228 37 6 0x20 0x04 $1
./rpc_write 10.176.60.228 37 6 0x40 0x04 $1
./rpc_write 10.176.60.228 37 6 0x20 0x05 $1
./rpc_write 10.176.60.228 37 6 0x40 0x05 $1

echo "W+2/S9/RB2in"
./rpc_write 10.176.60.228 38 7 0x20 0x04 $1
./rpc_write 10.176.60.228 38 7 0x40 0x04 $1
./rpc_write 10.176.60.228 38 7 0x20 0x05 $1
./rpc_write 10.176.60.228 38 7 0x40 0x05 $1

echo "W+2/S9/RB2out"
./rpc_write 10.176.60.228 38 6 0x20 0x04 $1
./rpc_write 10.176.60.228 38 6 0x40 0x04 $1
./rpc_write 10.176.60.228 38 6 0x20 0x05 $1
./rpc_write 10.176.60.228 38 6 0x40 0x05 $1
./rpc_write 10.176.60.228 38 6 0x20 0x06 $1
./rpc_write 10.176.60.228 38 6 0x40 0x06 $1

echo "W+2/S9/RB3"
./rpc_write 10.176.60.228 40 7 0x20 0x04 $1
./rpc_write 10.176.60.228 40 7 0x40 0x04 $1
./rpc_write 10.176.60.228 40 7 0x20 0x05 $1
./rpc_write 10.176.60.228 40 7 0x40 0x05 $1

echo "W+2/S9/RB4 (only one RB4 in S9)"
./rpc_write 10.176.60.228 41 7 0x20 0x04 $1
./rpc_write 10.176.60.228 41 7 0x40 0x04 $1

