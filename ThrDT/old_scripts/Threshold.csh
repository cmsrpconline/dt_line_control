#!/bin/csh

clear

echo -n " Setting thresholds via DT ...."
echo " "
echo " "
echo -n " Type the threshold value (in mV) you would like to write via DTs... "
set thr_value = $<

echo " The value " ${thr_value} " mV will be wrote ...."

# ./pirla.csh ${thr_value}

./Set_viaDT_vpv.csh ${thr_value} > output.txt

echo " ok...output.txt created "

exit 0 
