/* File rpc_slowc.c   06 Dec. 2004
Created by F. Loddo
Isituto Nazionale di Fisica Nucleare  Sez. di BARI

This code contains the main Routines to drive I2C lines 
on RPC FEB 
*/

/* I2C-port */
#define AD7417   0x28 	/* Address of ADC AD7417 = 0101000 */
#define AD5316   0x0c  	/* Address of DAC AD5316 = 0001100 */
#define PCF8574A 0x38  	/* Address of PCF8574A   = 0111000 */
#define PCA9544  0x70  	/* Address of PCA9544    = 1110000 */

unsigned int ack_bit=0;

/*****************************************************************/
/*********  Routine: Write of n bytes ************/
void wr_i2c(unsigned char nbyte, unsigned char addr_i2c, unsigned long din);

/******  Routine: Read 1 or 2 bytes *****/
unsigned int rd_i2c_nb(unsigned char nbyte, unsigned char addr_i2c);

/**********************************************************************/
/****  Routine: Enable Mux on Distr. Board ****************************/
void en_mux(unsigned char addr,unsigned char chan)  
{
  unsigned char chan_sel=0;

  switch (chan)
    {
    case 0: chan_sel=0x4; /* Chan 0 */
      break;
    case 1: chan_sel=0x5;
		break;
    case 2: chan_sel=0x6;
      break;
    case 3: chan_sel=0x7;
      break;
    case 4: chan_sel=0x0; /* Switch off */
      break;
    default:chan_sel=0x0;
    } /* end switch */
  
  wr_i2c(1,PCA9544+addr, chan_sel); /* Address Mux for writing */
} /* end en_mux */

/***************************************************************/
/****  Routine: Enable channel *********************************/
void en_chan(unsigned char nsez, unsigned char rbx_out)  
{
  /* nsez=0 -> Forward
     nsez=1 -> Middle in RB2/3, Backward in all other chambers 
     nsez=2 -> Backward in RB2/3
  */
  
  en_mux(7-rbx_out,0);  	/* Enable Chan0 of U10 on Dist. Board */
  en_mux(1,nsez); 		/* Enable Chan(nsez) of U4 on Dist. Board */
  
} /* end en_chan */
/**********************************************************/

/****  Routine: Disable chan  *****************************/
void disable_chan(unsigned char rbx_out)  
{
  en_mux(nchan,1,4); 		/* Disable U4 on Dist. Board */
  en_mux(nchan,7-rbx_out,4);    /* Disable U10 on Dist. Board */
  
} /* end disable_chan */
/***************************************************************************/

/*******  Routine: Reading PCF8574A on FEB for Control_Reg operations  *****/
unsigned int rd_controlreg(unsigned char nboard) 
{
  /* 0 <= nboard <= 5 */
  unsigned int control_reg;
  
  control_reg = rd_i2c_nb(1,PCF8574A+nboard) & 0xFF; /* Read 1 byte */
  
  return(control_reg);
} /*end rd_controlreg */
/**************************************************************************/

/**** Routine: Enable-Disable DAC on PCF8574A on FEB **********************/
void en_dis_dac(unsigned char nboard, unsigned char en_dac)  
{
  unsigned int control_reg;

  /* 0 <= nboard <= 5 */
  
  /************   PCF8574A  BIT Definition        **************************/
  /***   P7  P6  P5  P4    P3        P2      P1         P0    **************/
  /***   --  --  --  --    --        --    EN_DAC    SEL_DAC  **************/

  /* en_dac.  0 -> disable ; 1 -> enable   */

  switch(en_dac)
    {
    case 0: contr_reg = 0xFF; /* Disable DAC */
      break;
    case 1: contr_reg = 0xFD; /* Enable  DAC for Barrel*/
      break;
    default: break;   
    } /* end switch */
  
  wr_i2c(1, PCF8574A+nboard, contr_reg); /* Write PCF8574A */
  
} /* end en_dis_dac */
/***************************************************************************/

/****  ROUTINE: select/deselect DAC of PCF8574A on FEB *********************/
void sel_desel_dac(unsigned char nboard, unsigned char sel_dac) 
{
  unsigned int control_reg;  /* Content of PCF8574A on FEB */

/************   PCF8574A  BIT Definition        **************************/
/***   P7  P6  P5  P4    P3        P2      P1         P0    **************/
/***   --  --  --  --    --        --    EN_DAC    SEL_DAC  **************/


/*  sel_dac:0 = Deselect DAC
		1 = Select   DAC 

control_reg = rd_controlreg(nboard); /* Load previous control_reg configuration */

	switch (sel_dac)
	{
	case 0: control_reg = control_reg | 0x1;  /* Deselect DAC  */
		break;
	case 1: control_reg = control_reg & 0xFE;  /* Select DAC  */
		break;
	default: break;       
	} /* end switch */

wr_i2c(1, PCF8574A+nboard, control_reg); /* Write PCF8574A */ 

}  /* end sel_desel_DAC */
/******************************************************************/

/****  ROUTINE: read status on EN_DAC of PCF8574A on FEB *********************/
unsigned int read_endac(unsigned char nboard) 
{
unsigned int control_reg,endac;  

/************   PCF8574A  BIT Definition        **************************/
/***   P7  P6  P5  P4    P3        P2      P1         P0    **************/
/***   --  --  --  --    --        --    EN_DAC    SEL_DAC  **************/

/* For the Barrel:
	endac =  0 -> DAC Enabled  
 	endac =  1 -> DAC Disabled 
*/

control_reg = rd_controlreg(nboard); /* Read control_reg */
endac = (control_reg & 0x2) >> 1;

return(endac);
}  /* end read_endac */
/******************************************************************/

/******** Routine: Reading ADC from AD7417 ************************/
unsigned int AD7417_RADC(unsigned char nboard, unsigned char ndac) 

/* ndac = 0  VTH1
   ndac = 1  VTH2
   ndac = 2  VMON1
   ndac = 3  VMON2
 */
{
unsigned int nout=0,vdac=0;
unsigned char chan_sel=0;

switch (ndac ) {
		case 0: chan_sel=0x20;
			break;
		case 1: chan_sel=0x40;
			break;
		case 2: chan_sel=0x60;
        		break;
		case 3: chan_sel=0x80;
        		break;
		default: chan_sel=0x20;
		} /* end switch */

/* Write Address point Reg, for CONF_REG Write &  Chan Select */
wr_i2c(2,AD7417+nboard,(0x01 + (chan_sel << 8))); 

/* ADC Channel Selection: Write Address point Reg, for ADC-READ */
wr_i2c(1,AD7417+nboard,0x04); 

/* Read ADC (2 Bytes) from AD7417 */
nout  = (rd_i2c_nb(2,AD7417+nboard) & 0xFFC0) >> 6;
vdac = nout * 2.5/1.024; /* convert in mV*/

if((ndac == 2) || (ndac == 3)) vdac=vdac*2; /* VMON must be multiplied by 2
                                               for voltage division on FEB */
return(vdac);
}  /* end AD7417_RADC */
/**********************************************************************/

/*******  Routine: Reading Temperature from AD7417 ********************/
float AD7417_RT(unsigned char nboard) 

{
float t=0;

/* Write Address point Reg, for CONF_REG Write &  T Select */
wr_i2c(2,AD7417+nboard,0x01); 

/* ADC Channel Selection: Write Address point Reg, for T-READ */
wr_i2c(1,AD7417+nboard, 0); 

/* Read T (2 Bytes) from AD7417 and convert into degrees */ 
t  = ((rd_i2c_nb(2,AD7417+nboard) & 0xFFC0) >> 6)*0.25;
return(t);
} /*end AD7417_RT */
/************************************************************************/

/******* ROUTINE Writing Threshold to AD5316 ****************************/
void AD5316_WDAC(unsigned char ndac,unsigned int v0)  
{
unsigned int nvth,wthmsb=0,wthpointer=0,wthlsb=0;

switch(ndac)
 {
   case 0: wthpointer=0; /* Nothing */ 
           break;
   case 1: wthpointer=1; /* DAC_A (VTH1) */
           break;
   case 2: wthpointer=2; /* DAC_B (VTH2) */
           break;
   case 3: wthpointer=4; /* DAC_C (VMON1)*/
           break;
   case 4: wthpointer=8; /* DAC_D (VMON2)*/
           break;
   case 5: wthpointer=3; /* DAC_A & DAC_B*/
           break;
   case 6: wthpointer=12;/* DAC_C & DAC_D*/
           break;
  default: wthpointer=0;	  
	     break;
  }

nvth = v0*1.024/5; /* Digital word for Threshold */

wthmsb = ((nvth & 0x3c0) >> 6) + 0x70;  /*  Gain=0,BUF=1,CLR_=1,PD=1 */
wthlsb = (nvth & 0x3f) << 2;

wr_i2c(nchan,3,AD5316,(wthpointer + (wthmsb << 8) + (wthlsb << 16))); 
}  /* end AD5316_WDAC */
/***************************************************************************/

/******* ROUTINE Writing Threshold to AD5316 with FEEDBACK *****************/
void AD5316_WDAC_FB(unsigned char nboard, unsigned char ndac,unsigned int v0)  
{
unsigned int vth_read=0;

AD5316_WDAC(ndac,v0);  

/* Feedback */
vth_read = AD7417_RADC(nboard, ndac-1); /* Readback DAC */ 
AD5316_WDAC(ndac,(2*v0-vth_read));

}  /* end AD5316_WDAC_FB */
/***************************************************************************/

int main()  /* Puramente indicativo, sintatticamente errato!!!!!!! */
{
/*  0<= nsez <= 2 e' la Bi-Gap (FW,Mid,BW) selezionata quando si usa la linea DT
    rbx_out -> Va messo a 1 quando si indirizza la camera RB1_OUT o RB2_OUT
*/

switch(mode)
 {
	/* write barrel (whole Bigap) */
	case 0: en_chan(nsez,rbx_out); /* Enable slow-ctrl on the selected Bi-Gap */
		for(i=0; i<6; i++)  /* i<5 if RB4_2500 */
		{
		sel_desel_dac(i,1); 		/* Select DAC on board i */
		AD5316_WDAC_FB(i,1,vth1); 	/* Write Vth1 (mV) */
		AD5316_WDAC_FB(i,2,vth2); 	/* Write Vth2 (mV)*/
		AD5316_WDAC_FB(i,3,vmon1); 	/* Write Vmon1 (mV)*/
		AD5316_WDAC_FB(i,4,vmon2); 	/* Write Vmon2 (mV)*/
		sel_desel_dac(i,0); 		/* Deselect DAC on board i */
		}
		disable_chan(rbx_out); /* Disable slow-ctrl on the selected Bi-Gap, if finished */
           	break;

	/* Read barrel (whole Bigap) */
	case 1: en_chan(nsez, rbx_out); /* Enable slow-ctrl on the selected Bi-Gap */
		for(i=0; i<6; i++)  /* i<5 if RB4_2500 */
		{
		Temp = AD7417_RT(i);	      /* Read Temperature */
		VTH1 = AD7417_RADC(i,0);	/* Read VTH1 */
		VTH2 = AD7417_RADC(i,1);	/* Read VTH2 */
		VMON1= AD7417_RADC(i,2);	/* Read VMON1*/
		VMON2= AD7417_RADC(i,3);	/* Read VMON2*/
		endac= read_endac(i); 		/* 0/1 -> DAC enabled/disabled*/
		}
		disable_chan(rbx_out); /* Disable slow-ctrl on the selected Bi-Gap*/
           	break;


	/* enable DAC (whole Bigap) */
	case 3: en_chan(nsez, rbx_out); /* Enable slow-ctrl on the selected Bi-Gap */
		for(i=0; i<6; i++)  	  /* i<5 if RB4_2500  */
			en_dis_dac(i,1);    /* Enable DAC on the i board*/
		disable_chan(rbx_out);    /* Disable slow-ctrl on the selected Bi-Gap */
           	break;

	/* Disable DAC (whole Bigap) */
	case 4: en_chan(nsez, rbx_out); /* Enable slow-ctrl on the selected Bi-Gap */
		for(i=0; i<6; i++)  	  /* i<5 if RB4_2500 */
			en_dis_dac(i,0);    /* Diable DAC on the i board*/

		disable_chan(rbx_out); /* Disable slow-ctrl on the selected Bi-Gap */
           	break;
 	default: break;
  }

return 0;
}



