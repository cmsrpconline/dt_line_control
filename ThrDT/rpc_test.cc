#include <ccbcmds/apfunctions.h>
#include <ccbcmds/Cdef.h>
#include <ccbcmds/Ccommand.h>
#include <ccbcmds/Cccb.h>
#include <ccbcmds/Ci2c.h>
#include <ccbcmds/Clog.h>

#include <cstdio>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <ctime>


#define PCA9544  0x70 //01110000
#define PCF8574A 0x38 //00111000
#define AD7417   0x28 //00101000
#define AD5316   0x0c //00001100

#define ciccio "10.176.60.232"

int main() {

  short ccbid;
  short ctrl_data[I2C_DATA_MAX];
  short err_data[I2C_DATA_MAX];
  

  // I need to select the Minicrate via Wheel Sector Station

  Clog *myLog=new Clog("ciccio.txt"); // No ccbcmds log

  //  ccbid = 28;
  int ccbport=28;
  std::cout << "Open COnnection on "<<ciccio<<":"<<CCBPORT
	    <<" for ccb id "<<ccbid
	    <<std::endl;
  Ccommand *myCmd =new Ccommand(ccbport,ciccio,CCBPORT,myLog);
  if(!myCmd->connect()){
    std::cout <<" No connection "<<std::endl;
    return -1;
  }
  
  std::cout << " done it!" <<myCmd<<std::endl;
  Cccb *myCcb=new Cccb(myCmd); // CCB object
  Ci2c *myI2c=new Ci2c(myCmd); // I2C object
  short size;
  // enable mux
  // write on PCA9544 + 7 RB1,2_in RB3,RB4
  // write on PCA9544 + 6 RB1,2_out
  //Enabling Slow-Ctrl
  ctrl_data[0]=0x0100+((PCA9544+6)<<1);//start I2C sequence on Distr. Board U10
  ctrl_data[1]=0x0200 + 0x4; // wrI2C:Enable U10/Ch0 
  size = 2;
  myI2c->rpc_I2C(ctrl_data, size);
  Si2c_1 res = myI2c->rpc_i2c;
  //myI2c->rpc_I2C_print();
  // only one partition at the time
  ctrl_data[0]=0x0100+((PCA9544+1)<<1);//start I2C sequence on Distr. Board U4
  ctrl_data[1]=0x0200 + 0x4;//  wrI2C:  Enable U4 Forward Eta Partition 
  size = 2;
  myI2c->rpc_I2C(ctrl_data, size);
  //myI2c->rpc_I2C_print();

  for (unsigned char ifeb=0; ifeb<6; ifeb++){
    std::cout <<" ifeb = "<<(int) ifeb<<std::endl;
    // enable dac
    ctrl_data[0]=0x0100+((PCF8574A+ifeb)<<1);
    ctrl_data[1]=0x0200+0xFC;
    size=2;
    myI2c->rpc_I2C(ctrl_data, size);
    myI2c->rpc_I2C_print();

    
    //write VTH1 wthp = 1(220 mV)
    int wthp = 1;
    int nvth   =  (int)(250* 1.024/5);
    std::cout <<"nvth " <<std::hex<<nvth<<std::endl;
    int wthmsb = ((nvth & 0x03c0)>>6)+0x70;
    int wthlsb = (nvth & 0x3f) <<2;
    ctrl_data[0]=0x0100+((AD5316)<<1);//+ifeb??
    ctrl_data[1]=0x0000+wthp;
    ctrl_data[2]=0x0000+wthmsb;
    ctrl_data[3]=0x0200+wthlsb;
    size=4;
    myI2c->rpc_I2C(ctrl_data, size);    
    myI2c->rpc_I2C_print();

  // Read VTH1 (0x20) and VTH2 (0x040) of the first feb (ifeb=0)
    ctrl_data[ 0]=0x0100+((AD7417+ifeb)<<1);//start I2C sequence on first FEB
    ctrl_data[ 1]=0x0000+ 0x01; // Write Addr. Point CONF_REG
    ctrl_data[ 2]=0x0200+ 0x20; //Prepare for ADC Read
    size=3;
    myI2c->rpc_I2C(ctrl_data, size);
    myI2c->rpc_I2C_print();

    ctrl_data[ 0]=0x0100 + ((AD7417+ifeb)<<1);//start I2C sequence on first FEB
    ctrl_data[ 1]=0x0200 + 0x04; // Write Addr. Point CONF_REG
    size = 2;
    myI2c->rpc_I2C(ctrl_data, size);
    res = myI2c->rpc_i2c;
    myI2c->rpc_I2C_print();
    
    ctrl_data[ 0]=0x0100 + ((AD7417+ifeb)<<1)+1; //start I2C sequence on first FEB
    ctrl_data[ 1]=0x0400; //read
    ctrl_data[ 2]=0x0600; //read 
    size = 3;
    myI2c->rpc_I2C(ctrl_data, size);
    res = myI2c->rpc_i2c;
    myI2c->rpc_I2C_print();
    
    short res1 = (res.err_data[1]&0x00FF);
    short res2 = (res.err_data[2]&0x00FF);
    short result = (res2+(res1<<8));
    int nout  = (result & 0xFFC0) >> 6;
    float vdac = nout * 2.5/1.024; /* convert in mV*/
    std::cout <<"Thr = "<<vdac<<" mVolts"<<std::endl;


    //disable dac
    ctrl_data[0]=0x0100+((PCF8574A+ifeb)<<1);
    ctrl_data[1]=0x0200+0xFD;
    size=2;
    myI2c->rpc_I2C(ctrl_data, size);
    myI2c->rpc_I2C_print();

  }
  // disabling mux
  // only one partition at the time
  ctrl_data[0]=0x0100+((PCA9544+1)<<1);//start I2C sequence on Distr. Board U4
  ctrl_data[1]=0x0200 + 0x0;//  wrI2C:  Disable U4 Forward Eta Partition 
  size = 2;
  myI2c->rpc_I2C(ctrl_data, size);

  // write on PCA9544 + 7 RB1,2_in RB3,RB4
  // write on PCA9544 + 6 RB1,2_out
  //Enabling Slow-Ctrl
  ctrl_data[0]=0x0100+((PCA9544+6)<<1);//start I2C sequence on Distr. Board U10
  ctrl_data[1]=0x0200 + 0x0; // wrI2C:Enable U10/Ch0 
  size = 2;
  myI2c->rpc_I2C(ctrl_data, size);
  return 0;
}
